#define AppTitle "@APP_TITLE@"
#define AppInstallerBasename "@INSTALLER_BASENAME@"
#define AppExeFileName "@EXE_FILENAME@"
#define AppVersion "@VERSION@"
#define AppMaintainerName "@MAINTAINER_NAME@"
#define AppMaintainerEmail "@MAINTAINER_EMAIL@"
#define AppWebsiteUrl "@WEBSITE_URL@"
#define AppDefaultFileExtension "@DEFAULT_FILE_EXTENSION@"

[Setup]
AppName={#AppTitle}
AppVersion={#AppVersion}
AppContact={#AppMaintainerName} <{#AppMaintainerEmail}>
AppPublisher={#AppMaintainerName}
AppPublisherURL={#AppWebsiteUrl}
ChangesAssociations=yes
DefaultDirName={pf}\{#AppTitle}
DefaultGroupName={#AppTitle}
SetupIconFile="files\app.ico"
UninstallDisplayIcon={app}\app.ico
OutputDir=.
OutputBaseFilename={#AppInstallerBasename}

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl"
Name: "hu"; MessagesFile: "compiler:Languages\Hungarian.isl"

[Files]
Source: "files\{#AppExeFileName}"; DestDir: "{app}"
Source: "files\file.svg"; DestDir: "{app}"
Source: "files\app.ico"; DestDir: "{app}"
Source: "files\lib\*"; DestDir: "{app}\lib"

[Icons]
Name: "{group}\{#AppTitle}"; Filename: "{app}\{#AppExeFileName}"; IconFilename: "{app}\app.ico"
Name: "{userdesktop}\{#AppTitle}"; Filename: "{app}\{#AppExeFileName}"; IconFilename: "{app}\app.ico"

[Registry]
Root: HKCR; Subkey: ".{#AppDefaultFileExtension}"; ValueData: "{#AppTitle}"; Flags: uninsdeletevalue; ValueType: string; ValueName: ""
Root: HKCR; Subkey: "{#AppTitle}"; ValueData: "Program {#AppTitle}";  Flags: uninsdeletekey; ValueType: string; ValueName: ""
Root: HKCR; Subkey: "{#AppTitle}\DefaultIcon"; ValueData: "{app}\file.svg,0"; ValueType: string; ValueName: ""
Root: HKCR; Subkey: "{#AppTitle}\shell\open\command"; ValueData: """{app}\{#AppExeFileName}"" ""%1"""; ValueType: string; ValueName: ""

[UninstallRun]
Filename: "{app}\{#AppExeFileName}"; Parameters: "--run-operation=prerm"

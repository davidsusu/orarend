#!/bin/bash

set -e

update-mime
update-mime-database /usr/share/mime
gtk-update-icon-cache /usr/share/icons/hicolor

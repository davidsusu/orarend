#!/bin/bash

set -e

JAVA_ROOT="@JAVA_ROOT@";
MAIN_JAR="${JAVA_ROOT}/@JAR_FILENAME@";
LIB_DIR="${JAVA_ROOT}/lib";
MAIN_CLASS="@MAIN_CLASS@"

getent passwd | while IFS=: read -r name password uid gid gecos home shell; do
  top=${home#/}; top=${top%%/*}
  case $top in
    bin|dev|etc|lib*|no*|proc|sbin|usr|var) ;;
    *)
      if [ -d "$home" ] && [ "$(stat -c %u "$home")" = "$uid" ]; then
        if [ -d "$home/.aurora-timetable" ]; then
          echo "Clean settings for $name:"
           su $name -c 'java -cp "@JAVA_ROOT@/@JAR_FILENAME@:@JAVA_ROOT@/lib/*" "@MAIN_CLASS@" --run-operation=prerm'
        fi
      fi
    ;;
  esac
done

update-mime
update-mime-database /usr/share/mime
gtk-update-icon-cache /usr/share/icons/hicolor

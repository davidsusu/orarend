# TODO

Ezek az 1.0-hoz szükséges funkciók, átalakítások.

A GitHub-ra (?...) az eddigi history nélkül kell átköltözni!

Faliújságot, regisztrációt eltüntetni, vagy jobb alternatívát találni...

## Egyéb

- core maximális leegyszerűsítése
- a kód teljes rendbetétele
- pluginrendszer
    - pluginok beolvasása
    - egyelőre minimálisan kivitelezett plugin repository
    - plugin manager

## Általános vizuális

- mini timelimit-nézet
- megjelenítés:
    - az erőforrások listájában
    - az erőforrás-szerkesztőben

## Szűrők

- mindenbenkereső doboz az áttekintő fülön
    - esetleg új ablakot nyisson, ami menüből is elérhető?
- egyszerű szöveges szűrő a címkékhez és az erőforrásokhoz
    - hogyan áll vissza könnyen alaphelyzetbe? (onblur?)
- szofisztikáltabb blokk-szűrő

## Tábla-néző

- rugalmasabb keretrendszer interaktív és statikus nézetekhez
- kompakt (nagytáblás) nézet

## Generálás

- generáló ablak átszervezése
    - ne a progress-ablakon alapuljon
    - több paraméter
    - menthető log (bővebb kimenettel)
- állítható mértékű előellenőrzés (nincs, alap*, részletes)
- kikapcsolható feltételek (RuleTable)
- feltétel-próbálgató (sikertelen generálás után)
- eredmény előnézete
    - ad hoc táblanéző új ablakban
    - újrapróbálási/-paraméterezési lehetőség
    - alkalmazás új táblára

-----------------------------------------------------------

# Mégsem kell most:

## Generált blokkok (talán csak pluginok számára?)

- generált blokkok támogatása
    - a blokk módosítása/törlése esetén mi lesz a generált blokkokkal?
    - a generált blokk nem szerkeszthető?
- tábla-entry ablak (kattintásra)
- blokk-szerkesztőbe plusz fül a különleges paramétereknek
- read-only blokk-ablak (pl. a generált blokk nézegetéséhez) (ha nem szerkeszthető...)
    
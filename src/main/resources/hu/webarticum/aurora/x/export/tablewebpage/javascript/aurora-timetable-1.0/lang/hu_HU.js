if (!("auroraTimetableLang" in window)) {
    window.auroraTimetableLang = {};
}

window.auroraTimetableLang["hu_HU"] = {
    "softwareName": "AURORA Órarend Webexport",
    "weekDay0": "Hétfő",
    "weekDay1": "Kedd",
    "weekDay2": "Szerda",
    "weekDay3": "Csütörtök",
    "weekDay4": "Péntek",
    "weekDay5": "Szombat",
    "weekDay6": "Vasárnap",
    "labelNameAttribute": "Nézet",
    "labelNameClass": "Osztály",
    "labelNamePerson": "Tanár",
    "labelNameLocale": "Helyszín",
    "labelNamePeriod": "Periódus"
    
};

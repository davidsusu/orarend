
(function($){
    
    var defaultSettings = {
        "data": {
            "selects": [],
            "days": []
        },
        "autoinit": true,
        "autoload": true,
        "onready": null,
        "onload": null,
        "skin": "default",
        "lang": "en_US"
    };
    
    var version = "1.0";
    var methodName = "auroraTimetable";
    var activityPrefix = "timetable";
    var cssClassPrefix = "aurora-timetable-webexport-";
    var secondsPerPixel = 45;
    
    var funcCreate = function (element, userSettings) {
        var $container = $(element);
        var $headerContainer = $("<div />");
        var $selects = $([]);
        var $daysContainer = $("<div />");
        var $footerContainer = $("<div />");
        var settings = $.extend({}, defaultSettings, userSettings);
        var texts = window.auroraTimetableLang[settings["lang"]];
        
        var aligner = new IntervalFlowAligner();
        
        var inited = false;
        
        var funcGetLabel = function (item) {
            if (item.label) {
                return item.label;
            } else if (item.labelName && texts[item.labelName]) {
                return texts[item.labelName];
            } else {
                return "XXX";
            }
        }
        
        var funcInit = function () {
            $container.empty();
            $container.addClass(cssClassPrefix + "container " + cssClassPrefix + "skin-" + settings["skin"]);
            
            $headerContainer.appendTo($container);
            $headerContainer.addClass(cssClassPrefix + "header");
            
            for (var s = 0; s < settings["data"].selects.length; s++) {
                var selectData = settings["data"].selects[s];
                var $selectContainer = $("<div /" + ">").addClass(cssClassPrefix + "select-container").appendTo($headerContainer);
                
                var selectLabel = funcGetLabel(selectData);
                $("<span /" + ">").addClass(cssClassPrefix + "select-label").text(selectLabel + ":").appendTo($selectContainer);
                var $select = $("<select /" + ">").addClass(cssClassPrefix + "select").appendTo($selectContainer);
                $select.on("change", function () {
                    $container.trigger(activityPrefix + "load");
                });
                $selects = $selects.add($select);
                
                for (var g = 0; g < selectData.groups.length; g++) {
                    var groupData = selectData.groups[g];
                    var groupLabel = funcGetLabel(groupData);
                    var $optgroup = $("<optgroup /" + ">").attr("label", groupLabel).appendTo($select);
                    
                    for (var i = 0; i < groupData.items.length; i++) {
                        var item = groupData.items[i];
                        var itemLabel = funcGetLabel(item);
                        $("<option />").text(itemLabel).val(item.name).appendTo($optgroup);
                    }
                }
            }
            
            $daysContainer.appendTo($container);
            $daysContainer.addClass(cssClassPrefix + "days");
            
            $footerContainer.appendTo($container);
            $footerContainer.addClass(cssClassPrefix + "footer");
            
            var $signature = $("<div /" + ">").addClass(cssClassPrefix + "signature").appendTo($footerContainer);
            $signature.text(texts["softwareName"] + " " + version);
            
            inited = true;
        };
        
        var funcGetFilteredDays = function () {
            var daysData = settings["data"].days;
            var filteredDaysData = [];
            var searchTags = [];
            
            $selects.each(function () {
                searchTags.push($(this).val());
            });
            
            for (var day = 0; day < daysData.length; day++) {
                var dayData = daysData[day];
                var filteredDayData = $.extend({}, dayData);
                filteredDaysData.push(filteredDayData);
                filteredDayData["activities"] = [];
                for (var i = 0; i < dayData["activities"].length; i++) {
                    var activityData = dayData["activities"][i];
                    var allTagsFound = true;
                    for (var t = 0; t < searchTags.length; t++) {
                        if ($.inArray(searchTags[t], activityData["tags"]) == (-1)) {
                            allTagsFound = false;
                            break;
                        }
                    }
                    if (allTagsFound) {
                        filteredDayData["activities"].push(activityData);
                    }
                }
            }
            return filteredDaysData;
        };
        
        var funcFormatTimeNumber = function (timeNumber) {
            return ((timeNumber > 9) ? "" : "0") + timeNumber;
        };
        
        var funcSecondsToTimeString = function (seconds) {
            var hoursPart = seconds % 86400;
            var secondsPart = hoursPart % 60;
            hoursPart -= secondsPart;
            var minutesPart = hoursPart % 3600;
            hoursPart -= minutesPart;
            var result = (hoursPart / 3600) + ":" + funcFormatTimeNumber(minutesPart / 60);
            if (secondsPart > 0) {
                result += ":" + funcFormatTimeNumber(secondsPart);
            }
            return result;
        };
        
        var funcDisplayActivityFlow = function ($dayItemsContainer, alignData, startSeconds, secondsPerPixel) {
            $dayItemsContainer.width(alignData.width);
            
            $.each(alignData.aligns, function (i, alignedItem) {
                var top = Math.round((alignedItem.item.start - startSeconds) / secondsPerPixel);
                var height = Math.round((alignedItem.item.end - alignedItem.item.start) / secondsPerPixel);
                var timeString = funcSecondsToTimeString(alignedItem.item.start);
                var $activity = $("<div /"+">").addClass(cssClassPrefix + "activity").css({
                    "left": alignedItem.left + "px",
                    "top": top + "px",
                    "width": alignedItem.width + "px",
                    "height": height + "px"
                }).appendTo($dayItemsContainer);
                $("<div /" + ">").addClass(cssClassPrefix + "activity-time").text(timeString).appendTo($activity);
                $("<div /" + ">").addClass(cssClassPrefix + "activity-label").text(alignedItem.item.label).appendTo($activity);
            });
        }
        
        var funcGetDayLabel = function (day) {
            var weekDay = day % 7;
            var week = (day - weekDay) / 7;
            var result = texts["weekDay" + weekDay];
            if (week > 0) {
                result += "(" + (week + 1) + ")";
            }
            return result;
        }
        
        var funcReload = function () {
            if (!inited) {
                $container.trigger(activityPrefix + "ready");
            }
            
            var filteredDays = funcGetFilteredDays();
            
            $daysContainer.empty();
            
            var minStartSeconds = (-1);
            var maxEndSeconds = 0;
            
            for (var day = 0; day < filteredDays.length; day++) {
                var activities = filteredDays[day].activities;
                if (activities.length > 0) {
                    var firstActivity = activities[0];
                    if (minStartSeconds < 0 || firstActivity.start < minStartSeconds) {
                        minStartSeconds = firstActivity.start;
                    }
                    
                    var lastActivity = activities[activities.length - 1];
                    if (lastActivity.end > maxEndSeconds) {
                        maxEndSeconds = lastActivity.end;
                    }
                }
            }
            
            if (minStartSeconds > 0) {
                for (var day = 0; day < filteredDays.length; day++) {
                    var filteredDayData = filteredDays[day];
                    
                    var alignData = aligner.alignItems(filteredDayData.activities);
                    
                    var activityFlowLength = maxEndSeconds - minStartSeconds;
                    var columnHeight = Math.ceil(activityFlowLength / secondsPerPixel);
                    
                    var $day = $("<div />").addClass(cssClassPrefix + "day").appendTo($daysContainer);
                    
                    var $dayTitle = $("<div />").addClass(cssClassPrefix + "day-title").text(funcGetDayLabel(day)).appendTo($day);
                    
                    var $activityFlow = $("<div />").addClass(cssClassPrefix + "activityflow").height(columnHeight).appendTo($day);
                    funcDisplayActivityFlow($activityFlow, alignData, minStartSeconds, secondsPerPixel);
                }
            } else {
                // XXX
            }
        };
        
        $container.on(activityPrefix + "ready", funcInit);
        if (settings["onready"]) {
            $container.on(activityPrefix + "ready", settings["onready"]);
        }
        
        $container.on(activityPrefix + "load", funcReload);
        if (settings["onload"]) {
            $container.on(activityPrefix + "load", settings["onload"]);
        }
        
        if (settings["autoinit"]) {
            $container.trigger(activityPrefix + "ready");
            if (settings["autoload"]) {
                $container.trigger(activityPrefix + "load");
            }
        }
    }
    
    
    var IntervalFlowAligner = function (userSettings) {
        this.settings = $.extend({}, IntervalFlowAligner.defaultSettings, userSettings || {});
    };
    
    IntervalFlowAligner.defaultSettings = {
        "optimalItemWidth": 100,
        "minimumWidth": 150,
        "maximumWidth": 500,
        "innerSpace": 10
    };
    
    IntervalFlowAligner.prototype.alignItems = function (items) {
        var settings = this.settings;
        
        var sortedItems = items.slice();
        sortedItems.sort(IntervalFlowAligner.compareIntervals);
        
        var maximumLevelCount = 0;
        
        var crowds = [];
        var crowd = null;
        $.each(sortedItems, function (i, item) {
            if (crowd === null) {
                crowd = [];
                crowds.push(crowd);
                var levelItems = [];
                crowd.push(levelItems);
                levelItems.push(item);
                maximumLevelCount = 1;
            } else {
                var added = false;
                var levelCount = crowd.length;
                for (var level = 0; level < levelCount; level++) {
                    var levelItems = crowd[level];
                    var previousItem = levelItems[levelItems.length - 1];
                    if (item.start >= previousItem.end) {
                        if (level == 0) {
                            crowd = [];
                            crowds.push(crowd);
                            levelItems = [];
                            crowd.push(levelItems);
                        }
                        levelItems.push(item);
                        added = true;
                        break;
                    }
                }
                if (!added) {
                    var levelItems = [];
                    crowd.push(levelItems);
                    levelItems.push(item);
                    if (levelCount + 1 > maximumLevelCount) {
                        maximumLevelCount = levelCount + 1;
                    }
                }
            }
        });
        
        var optimalWidth = settings.optimalItemWidth * maximumLevelCount;
        var width = IntervalFlowAligner.calculateRestrictedSize(optimalWidth, settings.minimumWidth, settings.maximumWidth);
        var aligns = [];
        
        $.each(crowds, function (i, crowd) {
            var levelCount = crowd.length;
            var itemWidth = Math.max(1, ((width + settings.innerSpace) / levelCount) - settings.innerSpace);
            for (var level = 0; level < levelCount; level++) {
                var levelItems = crowd[level];
                $.each(levelItems, function (j, item) {
                    aligns.push({"left": (itemWidth + settings.innerSpace) * level, "width": itemWidth, "item": item});
                });
            }
        });
        
        var result = {
            "width": width,
            "aligns": aligns
        };
        
        return result;
    };
    
    IntervalFlowAligner.compareIntervals = function (interval1, interval2) {
        if (interval1.start > interval2.start) {
            return 1;
        } else if (interval1.start < interval2.start) {
            return -1;
        } else if (interval1.end > interval2.end) {
            return 1;
        } else if (interval1.end < interval2.end) {
            return -1;
        } else {
            return 0;
        }
    }
    
    IntervalFlowAligner.calculateRestrictedSize = function (optimal, minimum, maximum) {
        minimum = Math.max(0, minimum);
        maximum = Math.max(0, maximum);
        if (optimal == 0 && minimum == 0 && maximum == 0) {
            return 1;
        }
        if (maximum > 0 && maximum <= minimum) {
            maximum = minimum;
        }
        if (minimum == 0 && maximum == 0) {
            return optimal;
        } else if (minimum == 0) {
            if (optimal > 0) {
                return Math.min(maximum, optimal);
            } else {
                return 1;
            }
        } else if (maximum == 0) {
            if (optimal > 0) {
                return Math.max(minimum, optimal);
            } else {
                return minimum;
            }
        } else if (optimal < maximum) {
            return Math.max(minimum, optimal);
        } else {
            return maximum;
        }
    };
    
    
    var extendObject = {};
    extendObject[methodName] = function (userSettings) {
        this.each(function () {
            funcCreate(this, userSettings || {});
        });
        return this;
    };
    $.fn.extend(extendObject);
    
})(window.jQuery);


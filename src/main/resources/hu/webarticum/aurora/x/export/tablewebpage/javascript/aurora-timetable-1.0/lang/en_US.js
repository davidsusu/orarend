if (!("auroraTimetableLang" in window)) {
    window.auroraTimetableLang = {};
}

window.auroraTimetableLang["en_US"] = {
    "softwareName": "AURORA Timetable Webexport",
    "weekDay0": "Monday",
    "weekDay1": "Tuesday",
    "weekDay2": "Wednesday",
    "weekDay3": "Thursday",
    "weekDay4": "Friday",
    "weekDay5": "Saturday",
    "weekDay6": "Sunday",
    "labelNameAttribute": "View",
    "labelNameClass": "Class",
    "labelNamePerson": "Teacher",
    "labelNameLocale": "Locale",
    "labelNamePeriod": "Period"
    
};

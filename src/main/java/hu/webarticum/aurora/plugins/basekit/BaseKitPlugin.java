package hu.webarticum.aurora.plugins.basekit;

import static hu.webarticum.aurora.app.Shortcut.text;

import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.util.DocumentFactory;
import hu.webarticum.aurora.app.util.UrlDocumentFactory;
import hu.webarticum.aurora.app.util.autoput.AutoPutterFactory;
import hu.webarticum.aurora.plugins.basekit.autoput.sat.SatAutoPutterFactory;
import hu.webarticum.jpluginmanager.core.AbstractSimplePlugin;
import hu.webarticum.jpluginmanager.core.Version;

public class BaseKitPlugin extends AbstractSimplePlugin {

    private static final String DOCUMENT_TEMPLATE_PATH = Info.RESOURCE_PATH + "/plugins/basekit/sample/template";

    private static final String DOCUMENT_DEMO_PATH = Info.RESOURCE_PATH + "/plugins/basekit/sample/demo";
    
    public BaseKitPlugin() {
        extensionBinder.bind(CommonExtensions.Algorithm.class, SatAlgorithm.class);

        extensionBinder.bind(CommonExtensions.Template.class, DefaultTemplate.class);
        
        extensionBinder.bind(CommonExtensions.Demo.class, LargeDemo.class);
        extensionBinder.bind(CommonExtensions.Demo.class, ReducedDemo.class);
    }
    
    @Override
    public String getName() {
        return "xxx.basekit";
    }

    @Override
    public Version getVersion() {
        return new Version("0.1.0");
    }

    @Override
    public String getLabel() {
        return "Base KIT";
    }

    public static class SatAlgorithm implements CommonExtensions.Algorithm {

        @Override
        public AutoPutterFactory getAutoPutterFactory() {
            return new SatAutoPutterFactory();
        }
        
    }

    public static class DefaultTemplate implements CommonExtensions.Template {

        @Override
        public String getLabel() {
            return text("app.newdocument.template.default");
        }
        
        @Override
        public DocumentFactory getDocumentFactory() {
            return new UrlDocumentFactory(getClass().getResource(DOCUMENT_TEMPLATE_PATH + "/default.xml"));
        }
        
    }

    public static class LargeDemo implements CommonExtensions.Demo {

        @Override
        public String getLabel() {
            return text("app.newdocument.demo.large");
        }
        
        @Override
        public DocumentFactory getDocumentFactory() {
            return new UrlDocumentFactory(getClass().getResource(DOCUMENT_DEMO_PATH + "/large.xml"));
        }
        
    }

    public static class ReducedDemo implements CommonExtensions.Demo {

        @Override
        public String getLabel() {
            return text("app.newdocument.demo.reduced");
        }
        
        @Override
        public DocumentFactory getDocumentFactory() {
            return new UrlDocumentFactory(getClass().getResource(DOCUMENT_DEMO_PATH + "/reduced.xml"));
        }
        
    }
    
}

package hu.webarticum.aurora.plugins.basekit.autoput.sat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.autoput.AbstractAutoPutter;
import hu.webarticum.aurora.app.util.autoput.AutoPutter;
import hu.webarticum.aurora.app.util.autoput.AutoPutterAspectStructure;
import hu.webarticum.aurora.app.util.autoput.AutoPutterFactory;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;

public class SatAutoPutterFactory implements AutoPutterFactory {

    private static final long serialVersionUID = 1L;

    public static final String name = "sat";
    
    public static final Priority priority = Priority.HIGH;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getLabel() {
        return "Simple SAT";
    }

    @Override
    public Priority getPriority() {
        return priority;
    }
    
    @Override
    public AutoPutterAspectStructure collectAspectStructure(Document document, BlockList blocks, Board board) {
        return AutoPutterAspectStructure.EMPTY;
    }
    
    @Override
    public AutoPutter createAutoPutter(Document document, BlockList blocks, Board board, List<String> enabledAspects) {
        return new SatAutoPutter(document, blocks, board, enabledAspects);
    }

    public static class SatAutoPutter extends AbstractAutoPutter {

        protected int nextItemIndex = 1;
        
        public SatAutoPutter(Document document, BlockList blocks, Board board, List<String> enabledAspects) {
            init(document, blocks, board, enabledAspects);
        }
        
        @Override
        protected void _run() throws CancelledException {
            out.setLabel("START...");
            
            BlockList generatedBlocks = new BlockList();
            onBeforeSolverStarted(generatedBlocks);
            
            blocks.addAll(generatedBlocks);
            
            final Map<Block, List<Item>> basicClauses = new HashMap<Block, List<Item>>();
            for (Block block: blocks) {
                basicClauses.put(block, collectItems(block));
            }
            
            final ISolver solver = createSolver();
            solver.setTimeout(300);
            solver.newVar(nextItemIndex-1);
            
            final GeneralWrapper<Boolean> finished = new GeneralWrapper<Boolean>(false);
            final GeneralWrapper<Boolean> contradictionDetected = new GeneralWrapper<Boolean>(false);
            final GeneralWrapper<Boolean> timedOut = new GeneralWrapper<Boolean>(false);
            final GeneralWrapper<Boolean> satisfiable = new GeneralWrapper<Boolean>(false);

            Thread solverThread = (new Thread(new Runnable() {
                
                @Override
                public void run() {
                    try {
                        fillSolver(solver, blocks, basicClauses);
                    
                        for (Map.Entry<Block, List<Item>> clauseEntry: basicClauses.entrySet()) {
                            List<Item> items = clauseEntry.getValue();
                            List<Literal> literals = new ArrayList<Literal>(items.size());
                            for (Item item: items) {
                                literals.add(new Literal(item, true));
                            }
                            addClause(solver, literals);
                        }
                    } catch (ContradictionException e1) {
                        contradictionDetected.set(true);
                        finished.set(true);
                        return;
                    }
                    
                    try {
                        boolean isSat = solver.isSatisfiable();
                        satisfiable.set(isSat);
                        finished.set(true);
                    } catch (TimeoutException e) {
                        timedOut.set(true);
                        finished.set(true);
                    }
                }
                
            }));
            solverThread.start();
            
            while (true) {
                try {
                    out.tick();
                    this.sleep(500);
                } catch (CancelledException e) {
                    solverThread.interrupt();
                    resultState = ResultState.ABORTED;
                    throw e;
                }
                if (finished.get()) {
                    break;
                }
            }
            
            if (contradictionDetected.get()) {
                resultState = ResultState.UNSOLVED;
                out.setLabel("Failed!");
                out.log("Contradiction!");
                out.finish(false);
                return;
            }
            if (timedOut.get()) {
                resultState = ResultState.UNSOLVED;
                out.setLabel("Failed!");
                out.log("Timeout!");
                out.finish(false);
                return;
            }

            // FIXME
            onAfterSolverFinished(generatedBlocks);
            
            if (satisfiable.get()) {
                resultState = ResultState.SOLVED;
                for (Map.Entry<Block, List<Item>> clauseEntry: basicClauses.entrySet()) {
                    Block block = clauseEntry.getKey();
                    if (!generatedBlocks.contains(block)) {
                        List<Item> items = clauseEntry.getValue();
                        for (Item item: items) {
                            if (solver.model(item.getIndex())) {
                                board.add(block, item.getTime());
                                break;
                            }
                        }
                    }
                }
                out.setLabel("Success!");
                out.finish(true);
            } else {
                resultState = ResultState.UNSOLVED;
                out.setLabel("Failed!");
                out.log("Unsolvable!");
                out.finish(false);
            }
        }
        
        protected List<Item> collectItems(Block block) {
            TimingSet blockTimingSet = block.getCalculatedTimingSet(board);
            List<Item> blockItems = new ArrayList<Item>();
            for (TimingSet.TimeEntry timeEntry: blockTimingSet) {
                blockItems.add(new Item(block, timeEntry.getTime()));
            }
            return blockItems;
        }
        
        protected void onBeforeSolverStarted(BlockList generatedBlocks) {
        }
        
        // FIXME
        protected void onAfterSolverFinished(BlockList generatedBlocks) {
        }
        
        protected void fillSolver(ISolver solver, BlockList blocks, Map<Block, List<Item>> basicClauses) throws ContradictionException {
            int blockCount = blocks.size();
            
            for (int i=0; i<blockCount; i++) {
                Block block1 = blocks.get(i);
                List<Item> items1 = basicClauses.get(block1);
                for (int j=i+1; j<blockCount; j++) {
                    Block block2 = blocks.get(j);
                    List<Item> items2 = basicClauses.get(block2);
                    if (block1.conflictsWith(block2)) {
                        for (Item item1: items1) {
                            Time time1 = item1.getTime();
                            Interval interval1 = new Interval(time1, block1.getLength());
                            for (Item item2: items2) {
                                Time time2 = item2.getTime();
                                Interval interval2 = new Interval(time2, block2.getLength());
                                if (interval1.intersects(interval2)) {
                                    List<Literal> literals = new ArrayList<Literal>(2);
                                    literals.add(new Literal(item1, false));
                                    literals.add(new Literal(item2, false));
                                    addClause(solver, literals);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        protected void addClause(ISolver solver, Collection<Literal> literals) throws ContradictionException {
            int[] literalArray = new int[literals.size()];
            int i = 0;
            for (Literal literal: literals) {
                int value = literal.getItem().getIndex();
                if (!literal.isPositive()) {
                    value = -value;
                }
                literalArray[i] = value;
                i++;
            }
            VecInt clause = new VecInt(literalArray);
            solver.addClause(clause);
        }

        protected ISolver createSolver() {
            return SolverFactory.newSAT();
        }
        
        protected class Item {
            
            protected final int index;
            
            protected final Block block;
            
            protected final Time time;
            
            public Item(Block block, Time time) {
                this.index = nextItemIndex;
                this.block = block;
                this.time = time;
                nextItemIndex++; // store all in static list
            }
            
            public int getIndex() {
                return index;
            }
            
            public Block getBlock() {
                return block;
            }
            
            public Time getTime() {
                return time;
            }
            
        }
        
        protected class Literal {

            protected final Item item;
            
            protected final boolean positive;
            
            public Literal(Item item, boolean positive) {
                this.item = item;
                this.positive = positive;
            }

            public Item getItem() {
                return item;
            }
            
            public boolean isPositive() {
                return positive;
            }
            
        }
        
    }
    
    
}

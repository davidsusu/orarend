package hu.webarticum.aurora.plugins.defaultautoput;

import static hu.webarticum.aurora.app.Shortcut.text;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.util.autoput.AutoPutterFactory;
import hu.webarticum.aurora.app.util.i18n.CombinedMultiLanguageTextRepository;
import hu.webarticum.aurora.app.util.i18n.PropertiesLanguageTextRepository;
import hu.webarticum.jpluginmanager.core.AbstractSimplePlugin;
import hu.webarticum.jpluginmanager.core.Version;

public class DefaultAutoPutPlugin extends AbstractSimplePlugin {

    @Override
    public String getName() {
        return "defaultautoput";
    }

    @Override
    public Version getVersion() {
        return new Version("0.1.0");
    }

    @Override
    public String getLabel() {
        return text("plugin.defaultautoput.label");
    }
    
    @Override
    public boolean start() {
        if (!super.start()) {
            return false;
        }

        extensionBinder.bind(CommonExtensions.Algorithm.class, DefaultAlgorithm.class);

        CombinedMultiLanguageTextRepository langRepository = new CombinedMultiLanguageTextRepository();
        langRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH + "/plugins/defaultautoput/lang/defaultautoput.hu_HU.properties", "hu_HU"));
        langRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH + "/plugins/defaultautoput/lang/defaultautoput.en_US.properties", "en_US"));
        Application.instance().getTextRepository().mount("plugin.defaultautoput", langRepository);
        
        return true;
    }
    
    @Override
    public void stop() {

        extensionBinder.unbind(CommonExtensions.Algorithm.class, DefaultAlgorithm.class);
        
        Application.instance().getTextRepository().unmount("plugin.defaultautoput");
        
        super.stop();
    }

    public static class DefaultAlgorithm implements CommonExtensions.Algorithm {

        @Override
        public AutoPutterFactory getAutoPutterFactory() {
            return new DefaultAutoPutterFactory();
        }
        
    }
    
}

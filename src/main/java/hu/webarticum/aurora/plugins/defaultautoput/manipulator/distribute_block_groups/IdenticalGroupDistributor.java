package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;

public class IdenticalGroupDistributor {

    public boolean success = false;
    
    public final BlockDayTable blockDayTable;
    
    public IdenticalGroupDistributor(Scaffold scaffold, IdenticalGroup identicalGroup) {
        this(scaffold, identicalGroup, identicalGroup.getDays());
    }
    
    public IdenticalGroupDistributor(Scaffold scaffold, IdenticalGroup identicalGroup, Collection<Time> days) {
        this(scaffold, new BlockList(identicalGroup), days);
    }

    public IdenticalGroupDistributor(Scaffold scaffold, Collection<Block> blocks, Collection<Time> days) {
        blockDayTable = new BlockDayTable(scaffold);
        
        List<Block> blockList = new ArrayList<Block>(blocks);
        List<Time> dayList = new ArrayList<Time>(days);

        int blockCount = blockList.size();
        int dayCount = dayList.size();
        
        if (dayCount<blockCount) {
            return;
        }
        
        int blockDayCount = dayCount-blockCount+1;
        
        for (int i = 0; i<blockCount; i++) {
            Block block = blockList.get(i);
            TreeSet<Time> blockDays = new TreeSet<Time>();
            for (int j = 0; j<blockDayCount; j++) {
                int dayIndex = i + j;
                Time day = dayList.get(dayIndex);
                blockDays.add(day);
            }
            blockDayTable.add(block, blockDays);
        }
        
        success = true;
    }
    
}

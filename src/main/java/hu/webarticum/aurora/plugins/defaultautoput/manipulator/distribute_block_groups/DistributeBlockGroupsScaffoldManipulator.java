package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.common.CollectionUtil;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.CustomTimeLimit;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.InvertedTimeLimit;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.aurora.plugins.defaultautoput.ScaffoldManipulator;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;
import hu.webarticum.jsatbuilder.builder.core.Definition;
import hu.webarticum.jsatbuilder.builder.core.Variable;
import hu.webarticum.jsatbuilder.solver.core.Solver;
import hu.webarticum.jsatbuilder.solver.sat4j.EnumerableSat4jSolver;
import hu.webarticum.jsatbuilder.solver.sat4j.WeightedSat4jSolver;

public class DistributeBlockGroupsScaffoldManipulator implements ScaffoldManipulator {

    protected Scaffold scaffold;

    @Override
    public void manipulate(Scaffold scaffold) throws CancelledException, CollapseException {
        this.scaffold = scaffold;
        
        double diff;
        int p;
        
        // TODO
        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.distribute_block_groups.collect_block_groups"));
        scaffold.out.setProgress(null);
        
        List<SimilarGroup> similarGroups = new BlockGrouper(scaffold).createStructure();
        
        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.distribute_block_groups.filter_block_groups"));
        scaffold.out.setProgress(0d);
        diff = 1d / similarGroups.size();
        p = 0;


        for (Iterator<SimilarGroup> it = similarGroups.iterator(); it.hasNext(); ) {
            SimilarGroup similarGroup = it.next();
            if (!validateSimilarGroup(similarGroup)) {
                similarGroup.unshare();
                it.remove();
            }
            scaffold.out.setProgress((++p) * diff);
            scaffold.out.tick();
        }

        int similarGroupCount = similarGroups.size();

        // FIXME/TODO
        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.distribute_block_groups.process_block_groups"));
        scaffold.out.setProgress(0d);
        diff = 1d / similarGroupCount;
        p = 0;

        Set<SimilarGroup> unsolvableSimilarGroups = new HashSet<SimilarGroup>();
        Set<SimilarGroup> unsolvableSharedSimilarGroups = new HashSet<SimilarGroup>();
        Set<SimilarGroup> solvableSimilarGroups = new HashSet<SimilarGroup>();
        Set<SimilarGroup> solvable2SimilarGroups = new HashSet<SimilarGroup>();

        List<SimilarGroup> sharedSimilarGroups = new ArrayList<SimilarGroup>();

        for (SimilarGroup similarGroup: similarGroups) {
            if (similarGroup.isShared()) {
                sharedSimilarGroups.add(similarGroup);
            } else {
                boolean tryNormal = true;
                if (similarGroup.getBlockCount() == 2) {
                    if (restrictSimilarGroupDays(similarGroup, true)) {
                        solvable2SimilarGroups.add(similarGroup);
                        tryNormal = false;
                    }
                }
                if (tryNormal) {
                    if (restrictSimilarGroupDays(similarGroup, false)) {
                        solvableSimilarGroups.add(similarGroup);
                    } else {
                        unsolvableSimilarGroups.add(similarGroup);
                    }
                }
            }
            scaffold.out.setProgress((++p) * diff);
            scaffold.out.tick();
        }

        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.distribute_block_groups.reduce_block_days"));
        scaffold.out.setProgress(0d);
        diff = 0.5d / similarGroupCount;
        p = 0;
        
        if (!sharedSimilarGroups.isEmpty()) {
            SolvableStatusResult solvableStatusResult = maximizeSolvableSimilarGroups(sharedSimilarGroups);
            for (SimilarGroup similarGroup: solvableStatusResult.solvedSimilarGroups) {
                restrictSimilarGroupDays(similarGroup, false);
                scaffold.out.setProgress((++p) * diff);
            }
            for (SimilarGroup similarGroup: solvableStatusResult.solved2SimilarGroups) {
                restrictSimilarGroupDays(similarGroup, true);
                scaffold.out.setProgress((++p)*diff);
            }
            unsolvableSharedSimilarGroups.addAll(solvableStatusResult.unsolvedSimilarGroups);
            solvableSimilarGroups.addAll(solvableStatusResult.solvedSimilarGroups);
            solvable2SimilarGroups.addAll(solvableStatusResult.solved2SimilarGroups);
        }

        scaffold.out.tick();
        
        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.distribute_block_groups.process_shared_block_groups"));
        scaffold.out.setProgress(null);
        
        DistributionSolverFiller solverFiller = new DistributionSolverFiller(scaffold);
        for (SimilarGroup similarGroup: solvableSimilarGroups) {
            solverFiller.addSimilarGroup(similarGroup, false);
        }
        for (SimilarGroup similarGroup: solvable2SimilarGroups) {
            solverFiller.addSimilarGroup(similarGroup, true);
        }

        scaffold.out.tick();

        solverFiller.fillScaffold();
        
        scaffold.out.tick();

        List<SimilarGroup> secondTrySimilarGroups = new ArrayList<SimilarGroup>(unsolvableSimilarGroups);

        for (SimilarGroup similarGroup : unsolvableSimilarGroups) {
            logSimilarGroupAsProblematic(similarGroup);
        }
        
        for (SimilarGroup similarGroup: unsolvableSharedSimilarGroups) {
            if (similarGroup.getKind() == SimilarGroup.Kind.SIMILAR) {
                logSimilarGroupAsProblematic(similarGroup);
                continue;
            }
            boolean equalGroupsOk = true;
            for (EqualGroup equalGroup: similarGroup) {
                int referenceCount = equalGroup.getReferenceCount();
                if (referenceCount > 2 || (referenceCount == 1 && equalGroup.getBlockCount() == 2)) {
                    equalGroupsOk = false;
                    break;
                }
            }
            if (equalGroupsOk) {
                secondTrySimilarGroups.add(similarGroup);
            } else {
                logSimilarGroupAsProblematic(similarGroup);
            }
        }

        scaffold.out.tick();

        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.distribute_block_groups.process_hard_block_groups"));
        scaffold.out.setProgress(0d);
        diff = 1d / secondTrySimilarGroups.size();
        p = 0;
        
        for (SimilarGroup similarGroup: secondTrySimilarGroups) {
            boolean success = handleUnsolvedSimilarGroup(similarGroup);

            if (!success) {
                logSimilarGroupAsProblematic(similarGroup);
            }
            
            scaffold.out.setProgress((++p)*diff);
            scaffold.out.tick();
        }

        scaffold.out.log(text("plugin.defaultautoput.manipulator.distribute_block_groups.finished"));
    }
    
    private boolean validateSimilarGroup(SimilarGroup similarGroup) {
        SimilarGroup.Kind kind = similarGroup.getKind();
        
        if (kind == SimilarGroup.Kind.EMPTY || kind == SimilarGroup.Kind.SINGLE) {
            return false;
        }

        BlockList blocks = similarGroup.getBlocks();
        
        boolean allFixed = true;
        for (Block block: blocks) {
            if (!scaffold.isFixed(block)) {
                allFixed = false;
                break;
            }
        }

        if (allFixed) {
            return false;
        }

        boolean shared = similarGroup.isShared();
        int blockCount = similarGroup.getBlockCount();
        
        if (shared && kind != SimilarGroup.Kind.SIMILAR && blockCount > 2) {
            return false;
        }
        
        return true;
    }
    
    private void logSimilarGroupAsProblematic(SimilarGroup similarGroup) {
        scaffold.out.log(String.format(
            text("plugin.defaultautoput.manipulator.distribute_block_groups.skip_problematic_group"),
            similarGroup.getBlockCount(), new Labeled.ListWrapper(similarGroup.getBlocks())
        ));
    }
    
    protected SolvableStatusResult maximizeSolvableSimilarGroups(List<SimilarGroup> similarGroups) {
        Map<Variable, SimilarGroup> solvedMap = new IdentityHashMap<Variable, SimilarGroup>();
        Map<Variable, SimilarGroup> solved2Map = new IdentityHashMap<Variable, SimilarGroup>();
        
        Map<SimilarGroup, Integer> solvedStatuses = new IdentityHashMap<SimilarGroup, Integer>();
        
        WeightedSat4jSolver solver = new WeightedSat4jSolver();
        DistributionSolverFiller solverFiller = new DistributionSolverFiller(scaffold);
        for (SimilarGroup similarGroup: similarGroups) {
            solvedStatuses.put(similarGroup, 0);
            Variable solvedVariable = new Variable();
            solvedMap.put(solvedVariable, similarGroup);
            if (similarGroup.size() == 2) {
                Variable solved2Variable = new Variable();
                solved2Map.put(solved2Variable, similarGroup);
                solverFiller.addSimilarGroup(similarGroup, true, solvedVariable, solved2Variable);
            } else {
                solverFiller.addSimilarGroup(similarGroup, false, solvedVariable, null);
            }
        }
        solverFiller.fillSolver(solver);
        if (solver.run()) {
            for (Solver.Literal literal: solver.getModel()) {
                if (literal.isPositive()) {
                    Variable variable = (Variable)literal.getVariable();
                    if (solvedMap.containsKey(variable)) {
                        SimilarGroup similarGroup = solvedMap.get(variable);
                        int currentStatus = solvedStatuses.get(similarGroup);
                        if (currentStatus == 0) {
                            solvedStatuses.put(similarGroup, 1);
                        }
                    } else if (solved2Map.containsKey(variable)) {
                        SimilarGroup similarGroup = solved2Map.get(variable);
                        solvedStatuses.put(similarGroup, 2);
                    }
                }
            }
        }
        
        return new SolvableStatusResult(solvedStatuses);
    }
    
    protected boolean restrictSimilarGroupDays(SimilarGroup similarGroup, boolean allowed2) {
        Map<Block, IdenticalGroup> blockIdenticalGroupMap = new IdentityHashMap<Block, IdenticalGroup>();
        for (EqualGroup equalGroup: similarGroup) {
            for (IdenticalGroup identicalGroup: equalGroup) {
                for (Block block: identicalGroup) {
                    blockIdenticalGroupMap.put(block, identicalGroup);
                }
            }
        }
        
        EnumerableSat4jSolver solver = new EnumerableSat4jSolver();
        DistributionSolverFiller solverFiller = new DistributionSolverFiller(scaffold);
        if (!solverFiller.addSimilarGroup(similarGroup, allowed2)) {
            return false;
        }
        
        solverFiller.fillSolver(solver);
        Set<Variable> variables = new HashSet<Variable>();
        boolean solved = false;
        while (solver.run()) {
            solved = true;
            for (Solver.Literal literal: solver.getModel()) {
                if (literal.isPositive()) {
                    variables.add((Variable)literal.getVariable());
                }
            }
        }
        if (!solved) {
            return false;
        }
        
        Map<IdenticalGroup, TreeSet<Time>> identicalGroupDaysMap = new IdentityHashMap<IdenticalGroup, TreeSet<Time>>();
        for (Map.Entry<Block, TreeMap<Time, BlockDayTable.BlockDayItem>> entry: solverFiller.blockDayTable.blockDayItemMap.entrySet()) {
            Block block = entry.getKey();
            IdenticalGroup identicalGroup = blockIdenticalGroupMap.get(block);
            TreeMap<Time, BlockDayTable.BlockDayItem> dayItemMap = entry.getValue();
            TreeSet<Time> identicalGroupDays;
            if (identicalGroupDaysMap.containsKey(identicalGroup)) {
                identicalGroupDays = identicalGroupDaysMap.get(identicalGroup);
            } else {
                identicalGroupDays = new TreeSet<Time>();
                identicalGroupDaysMap.put(identicalGroup, identicalGroupDays);
            }
            for (Map.Entry<Time, BlockDayTable.BlockDayItem> _entry: dayItemMap.entrySet()) {
                Time day = _entry.getKey();
                BlockDayTable.BlockDayItem item = _entry.getValue();
                if (variables.contains(item.getVariable())) {
                    identicalGroupDays.add(day);
                }
            }
        }
        
        for (Map.Entry<IdenticalGroup, TreeSet<Time>> entry: identicalGroupDaysMap.entrySet()) {
            IdenticalGroup identicalGroup = entry.getKey();
            TreeSet<Time> days = entry.getValue();
            identicalGroup.restrictDays(days);
        }
        
        return true;
    }
    
    protected boolean handleUnsolvedSimilarGroup(SimilarGroup similarGroup) throws CollapseException {
        Set<IdenticalGroup> identicalGroups = similarGroup.getIdenticalGroups();

        Set<IdenticalGroup> fixedGroups = new HashSet<IdenticalGroup>();
        Set<IdenticalGroup> oneDayGroups = new HashSet<IdenticalGroup>();
        Set<IdenticalGroup> fixedAndOneDayGroups = new HashSet<IdenticalGroup>();
        Set<IdenticalGroup> normalGroups = new HashSet<IdenticalGroup>();
        for (IdenticalGroup identicalGroup: identicalGroups) {
            if (identicalGroup.size() == 1) {
                Block block = identicalGroup.getSampleBlock();
                if (scaffold.isFixed(block)) {
                    fixedGroups.add(identicalGroup);
                } else {
                    oneDayGroups.add(identicalGroup);
                }
            }
        }
        normalGroups.addAll(identicalGroups);
        normalGroups.removeAll(fixedGroups);
        normalGroups.removeAll(oneDayGroups);
        fixedAndOneDayGroups.addAll(fixedGroups);
        fixedAndOneDayGroups.addAll(oneDayGroups);

        int normalGroupCount = normalGroups.size();
        
        IdenticalGroup normalIdenticalGroup = null;
        if (normalGroupCount == 1) {
            normalIdenticalGroup = CollectionUtil.getFirst(normalGroups);
        } else if (normalGroupCount > 1) {
            for (IdenticalGroup identicalGroup: identicalGroups) {
                if (identicalGroup.size() > identicalGroup.getDays().size()) {
                    return false;
                }
                
                EqualGroup forcedEqualGroup = new EqualGroup();
                forcedEqualGroup.add(identicalGroup);
                
                SimilarGroup forcedSimilarGroup = new SimilarGroup();
                forcedSimilarGroup.add(forcedEqualGroup);
                
                DistributionSolverFiller solverFiller = new DistributionSolverFiller(scaffold);
                solverFiller.addSimilarGroup(forcedSimilarGroup, false);
                solverFiller.fillScaffold();
            }
            return true;
        }
        
        TreeMap<Time, Set<IdenticalGroup>> dayIdenticalGroupsMap = new TreeMap<Time, Set<IdenticalGroup>>();
        for (IdenticalGroup identicalGroup: fixedAndOneDayGroups) {
            TreeSet<Time> identicalGroupDays = identicalGroup.getDays();
            if (!identicalGroupDays.isEmpty()) {
                Time day = identicalGroupDays.first();
                Set<IdenticalGroup> dayIdenticalGroups;
                if (dayIdenticalGroupsMap.containsKey(day)) {
                    dayIdenticalGroups = dayIdenticalGroupsMap.get(day);
                } else {
                    dayIdenticalGroups = new HashSet<IdenticalGroup>();
                    dayIdenticalGroupsMap.put(day, dayIdenticalGroups);
                }
                dayIdenticalGroups.add(identicalGroup);
            }
        }
        
        List<Pair<IdenticalGroup, IdenticalGroup>> sameDayGroups = new ArrayList<Pair<IdenticalGroup, IdenticalGroup>>();
        TreeMap<Time, IdenticalGroup> dayGroupMap = new TreeMap<Time, IdenticalGroup>();
        TreeSet<Time> reservedDays = new TreeSet<Time>();
        
        for (Map.Entry<Time, Set<IdenticalGroup>> entry: dayIdenticalGroupsMap.entrySet()) {
            Time day = entry.getKey();
            Set<IdenticalGroup> _identicalGroups = entry.getValue();
            int groupCount = _identicalGroups.size();
            if (groupCount == 1) {
                dayGroupMap.put(day, CollectionUtil.getFirst(_identicalGroups));
            } else if (groupCount >= 2) {
                if (groupCount == 2) {
                    IdenticalGroup identicalGroup1 = CollectionUtil.getNth(_identicalGroups, 0);
                    IdenticalGroup identicalGroup2 = CollectionUtil.getNth(_identicalGroups, 1);
                    sameDayGroups.add(new Pair<IdenticalGroup, IdenticalGroup>(identicalGroup1, identicalGroup2));
                }
                reservedDays.add(day);
            }
        }
        
        if (normalIdenticalGroup != null) {
            TreeSet<Time> groupDays = normalIdenticalGroup.getDays();

            TreeSet<Time> freeSecondaryDays = new TreeSet<Time>(groupDays);
            freeSecondaryDays.removeAll(reservedDays);
            
            TreeSet<Time> freePrimaryDays = new TreeSet<Time>(freeSecondaryDays);
            freePrimaryDays.removeAll(dayGroupMap.keySet());

            int freePrimaryDayCount = freePrimaryDays.size();
            int freeSecondaryDayCount = freeSecondaryDays.size();
            
            int blockCount = normalIdenticalGroup.size();
            
            if (freePrimaryDayCount + freeSecondaryDayCount<blockCount) {
                return false;
            } else if (blockCount<freePrimaryDayCount) {
                
                // FIXME: ez az eset ellentmondas
                return false;
                
            }

            BlockList primaryBlocks = new BlockList();
            BlockList secondaryBlocks = new BlockList(normalIdenticalGroup);
            CollectionUtil.moveElements(secondaryBlocks, primaryBlocks, freePrimaryDayCount);
            
            TreeMap<Time, Pair<Block, Boolean>> dayBaseBlockMap = new TreeMap<Time, Pair<Block, Boolean>>();
            for (Map.Entry<Time, IdenticalGroup> entry: dayGroupMap.entrySet()) {
                Time day = entry.getKey();
                IdenticalGroup identicalGroup = entry.getValue();
                boolean isFixed = fixedGroups.contains(identicalGroup);
                dayBaseBlockMap.put(day, new Pair<Block, Boolean>(identicalGroup.getSampleBlock(), isFixed));
            }
            Iterator<Time> primaryDayIterator = freePrimaryDays.iterator();
            for (Block block: primaryBlocks) {
                Time day = primaryDayIterator.next();
                TreeSet<Time> days = new TreeSet<Time>();
                days.add(day);
                restrictBlockDays(block, days);
                dayBaseBlockMap.put(day, new Pair<Block, Boolean>(block, false));
            }
            
            if (freePrimaryDayCount == secondaryBlocks.size()) {
                Iterator<Time> secondaryDayIterator = freeSecondaryDays.iterator();
                for (Block block: secondaryBlocks) {
                    Time day = secondaryDayIterator.next();
                    TreeSet<Time> days = new TreeSet<Time>();
                    days.add(day);
                    restrictBlockDays(block, days);
                    Pair<Block, Boolean> baseBlockFixedPair = dayBaseBlockMap.get(day);
                    if (baseBlockFixedPair!=null) {
                        Block baseBlock = baseBlockFixedPair.getLeft();
                        boolean baseIsFixed = baseBlockFixedPair.getRight();
                        if (baseIsFixed) {
                            scaffold.registerConstraint(scaffold.new BlockFollowUpConstraint(baseBlock, block, false));
                        } else {
                            scaffold.registerConstraint(scaffold.new BlockFollowUpConstraint(baseBlock, block, true));
                        }
                    }
                }
            } else {
                IdenticalGroupDistributor distributor = new IdenticalGroupDistributor(scaffold, secondaryBlocks, freeSecondaryDays);
                for (Map.Entry<Block, TreeMap<Time, BlockDayTable.BlockDayItem>> entry: distributor.blockDayTable.blockDayItemMap.entrySet()) {
                    Block block = entry.getKey();
                    TreeMap<Time, BlockDayTable.BlockDayItem> blockDayItems = entry.getValue();
                    TreeSet<Time> days = new TreeSet<Time>(blockDayItems.keySet());
                    restrictBlockDays(block, days);
                    for (Map.Entry<Time, BlockDayTable.BlockDayItem> _entry: blockDayItems.entrySet()) {
                        Time blockDay = _entry.getKey();
                        BlockDayTable.BlockDayItem blockDayItem = _entry.getValue();
                        Definition definition = blockDayItem.getHelper();
                        Pair<Block, Boolean> baseBlockFixedPair = dayBaseBlockMap.get(blockDay);
                        if (baseBlockFixedPair!=null) {
                            Block baseBlock = baseBlockFixedPair.getLeft();
                            boolean baseIsFixed = baseBlockFixedPair.getRight();
                            if (baseIsFixed) {
                                scaffold.registerConstraint(scaffold.new BlockFollowUpConstraint(baseBlock, block, false, definition));
                            } else {
                                scaffold.registerConstraint(scaffold.new BlockFollowUpConstraint(baseBlock, block, true, definition));
                            }
                        }
                    }
                    
                }
            }
        }
        
        for (Pair<IdenticalGroup, IdenticalGroup> pair: sameDayGroups) {
            IdenticalGroup identicalGroup1 = pair.getLeft();
            IdenticalGroup identicalGroup2 = pair.getRight();
            Block block1 = identicalGroup1.getSampleBlock();
            Block block2 = identicalGroup2.getSampleBlock();
            boolean isFixed1 = fixedGroups.contains(identicalGroup1);
            boolean isFixed2 = fixedGroups.contains(identicalGroup2);
            if (!isFixed1 && !isFixed2) {
                scaffold.registerConstraint(scaffold.new BlockFollowUpConstraint(block1, block2, true));
            } else if (isFixed1) {
                scaffold.registerConstraint(scaffold.new BlockFollowUpConstraint(block1, block2, false));
            }
        }
        
        return true;
    }
    
    protected void restrictBlockDays(Block block, TreeSet<Time> days) throws CollapseException {
        TimingSet timingSet = scaffold.getTimingSet(block);
        CustomTimeLimit daysTimeLimit = new CustomTimeLimit(false);
        for (Time day: days) {
            Interval dayTimeLimit = new Interval(day, Time.DAY);
            daysTimeLimit = daysTimeLimit.unionWith(new CustomTimeLimit(dayTimeLimit));
        }
        TimingSet removedTimingSet = timingSet.getLimited(new InvertedTimeLimit(daysTimeLimit));
        for (Time time: removedTimingSet.getTimes()) {
            Variable variable = scaffold.blockTimeVariableMap.get(block).get(time);
            variable.remove();
        }
    }
    
    protected class SolvableStatusResult {

        public final List<SimilarGroup> unsolvedSimilarGroups = new ArrayList<SimilarGroup>();
        public final List<SimilarGroup> solvedSimilarGroups = new ArrayList<SimilarGroup>();
        public final List<SimilarGroup> solved2SimilarGroups = new ArrayList<SimilarGroup>();
        
        public SolvableStatusResult(Map<SimilarGroup, Integer> solvedStatuses) {
            for (Map.Entry<SimilarGroup, Integer> entry: solvedStatuses.entrySet()) {
                SimilarGroup similarGroup = entry.getKey();
                int status = entry.getValue();
                switch (status) {
                    case 2:
                        solved2SimilarGroups.add(similarGroup);
                    break;
                    case 1:
                        solvedSimilarGroups.add(similarGroup);
                    break;
                    case 0:
                    default:
                        unsolvedSimilarGroups.add(similarGroup);
                }
            }
        }
        
    }

}

package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import java.util.HashSet;
import java.util.TreeSet;

import hu.webarticum.aurora.app.util.common.CollectionUtil;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;

public class IdenticalGroup extends HashSet<Block> {

    private static final long serialVersionUID = 1L;
    
    protected Scaffold scaffold;
    
    protected TreeSet<Time> days = null;
    
    public IdenticalGroup(Scaffold scaffold) {
        this.scaffold = scaffold;
    }

    public Block getSampleBlock() {
        return CollectionUtil.getFirst(this);
    }
    
    public TreeSet<Time> getDays() {
        if (this.days == null) {
            if (this.isEmpty()) {
                this.days = new TreeSet<Time>();
            } else {
                Block sampleBlock = CollectionUtil.getFirst(this);
                TimingSet timingSet = scaffold.getTimingSet(sampleBlock);
                this.days = new TreeSet<Time>(timingSet.split(Time.DAY).keySet());
            }
        }
        return this.days;
    }
    
    public void restrictDays(TreeSet<Time> days) {
        if (this.days == null) {
            this.days = new TreeSet<Time>(days);
        } else {
            this.days.retainAll(days);
        }
    }
    
    @Override
    public String toString() {
        return super.toString() + "/" + this.days;
    }

    public static class DisruptException extends Exception {
        
        private static final long serialVersionUID = 1L;
        
    }

}

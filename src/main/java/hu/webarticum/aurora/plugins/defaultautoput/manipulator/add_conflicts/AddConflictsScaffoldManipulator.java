package hu.webarticum.aurora.plugins.defaultautoput.manipulator.add_conflicts;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.common.IntervalUtil;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Intervalable;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.aurora.plugins.defaultautoput.ScaffoldManipulator;
import hu.webarticum.jsatbuilder.builder.common.BoundConstraint;
import hu.webarticum.jsatbuilder.builder.common.ConflictConstraint;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;
import hu.webarticum.jsatbuilder.builder.core.Variable;

public class AddConflictsScaffoldManipulator implements ScaffoldManipulator {
    
    @Override
    public void manipulate(Scaffold scaffold) throws CancelledException, CollapseException {
        addSimpleConflicts(scaffold);
        addComplexConflicts(scaffold);
    }
    
    private void addSimpleConflicts(Scaffold scaffold) throws CancelledException {
        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.add_conflicts.add_conflicts"));
        scaffold.out.setProgress(0d);
        
        BlockList blocks = new BlockList(scaffold.blockTimeVariableMap.keySet());

        double progress = 0;
        double progressDiff = 1d / blocks.size();
        
        int blockCount = blocks.size();
        
        for (int i = 0; i < blockCount; i++) {
            Block block1 = blocks.get(i);
            Map<Time, Variable> timeVariableMap1 = scaffold.blockTimeVariableMap.get(block1);
            
            for (int j = i + 1; j < blockCount; j++) {
                Block block2 = blocks.get(j);
                
                if (scaffold.areInConflict(block1, block2)) {
                    Map<Time, Variable> timeVariableMap2 = scaffold.blockTimeVariableMap.get(block2);
                    
                    for (Map.Entry<Time, Variable> innerEntry1: timeVariableMap1.entrySet()) {
                        Time time1 = innerEntry1.getKey();
                        Variable variable1 = innerEntry1.getValue();
                        Interval interval1 = new Interval(time1, block1.getLength());
                        
                        for (Map.Entry<Time, Variable> innerEntry2: timeVariableMap2.entrySet()) {
                            Time time2 = innerEntry2.getKey();
                            Variable variable2 = innerEntry2.getValue();
                            Interval interval2 = new Interval(time2, block2.getLength());
                            
                            if (interval1.intersects(interval2)) {
                                scaffold.registerConstraint(new ConflictConstraint(variable1, variable2));
                            }
                            
                        }
                    }
                    
                }
            }
            
            scaffold.out.setProgress(progress += progressDiff);
            scaffold.out.tick();
        }

        scaffold.out.log(text("plugin.defaultautoput.manipulator.add_conflicts.finished"));
    }

    private void addComplexConflicts(Scaffold scaffold) throws CancelledException, CollapseException {
        List<Resource> multiResources = new ArrayList<Resource>();
        BlockList placeableBlocks = new BlockList(scaffold.blockTimeVariableMap.keySet());
        for (Resource resource: placeableBlocks.getActivities().getResources()) {
            if (resource.getQuantity() > 1) {
                multiResources.add(resource);
            }
        }
        
        if (multiResources.isEmpty()) {
            return;
        }

        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.add_complex_conflicts.add_conflicts"));
        scaffold.out.setProgress(0d);

        double progress = 0;
        double progressDiff = 1d / multiResources.size();
        
        for (Resource multiResource: multiResources) {
            handleMultiResource(scaffold, multiResource);
            scaffold.out.setProgress(progress += progressDiff);
            scaffold.out.tick();
        }

        scaffold.out.log(text("plugin.defaultautoput.manipulator.add_complex_conflicts.finished"));
    }
    
    private void handleMultiResource(Scaffold scaffold, Resource resource)
        throws CancelledException, CollapseException
    {
        Set<Set<BlockSlotIntervalable>> groups = new HashSet<Set<BlockSlotIntervalable>>();
        
        PeriodSet periods = scaffold.blocks.getPeriods();
        for (Period period: periods) {
            ActivityFilter activityFilter = new ActivityFilter.HasResource(resource);
            BlockFilter blockFilter = new BlockFilter.PeriodActivityBlockFilter(activityFilter, period);
            BlockList periodBlocks = scaffold.blocks.filter(blockFilter);
            List<BlockSlotIntervalable> intervalables = new ArrayList<BlockSlotIntervalable>();
            for (Block periodBlock: periodBlocks) {
                ActivityList periodActivities = periodBlock.getActivityManager().getActivities(period);
                TimingSet periodBlockTimingSet = scaffold.getTimingSet(periodBlock);
                for (TimingSet.TimeEntry timeEntry: periodBlockTimingSet) {
                    Interval interval = new Interval(timeEntry.getTime(), periodBlock.getLength());
                    intervalables.add(new BlockSlotIntervalable(periodBlock, periodActivities, interval));
                }
            }
            
            SortedMap<Interval, List<BlockSlotIntervalable>> sections =
                IntervalUtil.breakIntervalablesToSections(intervalables)
            ;
            
            for (Map.Entry<Interval, List<BlockSlotIntervalable>> entry: sections.entrySet()) {
                List<BlockSlotIntervalable> sectionSlots = entry.getValue();
                if (sectionSlots.size() > 1) {
                    groups.add(new HashSet<BlockSlotIntervalable>(sectionSlots));
                }
            }
        }
        
        for (Set<BlockSlotIntervalable> group: groups) {
            handleMultiResourceBlockGroup(scaffold, resource, group);
        }
    }
    
    // TODO: handle non-whole resource subsets
    private static void handleMultiResourceBlockGroup(
        Scaffold scaffold, Resource resource, Set<BlockSlotIntervalable> group
    ) throws CancelledException, CollapseException {
        List<Variable> repeatedVariables = new ArrayList<Variable>();
        for (BlockSlotIntervalable blockSlotIntervalable: group) {
            Block block = blockSlotIntervalable.block;
            ActivityList activities = new ActivityList(blockSlotIntervalable.activities);
            int count = activities.getResourceSubsets().getMaximumCount(resource);
            Map<Time, Variable> blockVariables = scaffold.blockTimeVariableMap.get(block);
            Variable variable = blockVariables.get(blockSlotIntervalable.interval.getStart());
            for (int i = 0; i < count; i++) {
                repeatedVariables.add(variable);
            }
        }
        int quantity = resource.getQuantity();
        BoundConstraint constraint = new BoundConstraint(0, quantity, repeatedVariables);
        // TODO: lang
        constraint.setLabel(resource.getLabel() + " max(" + quantity + ")/" + repeatedVariables.size());
        scaffold.registerConstraint(constraint);
    }
    
    
    private static class BlockSlotIntervalable implements Intervalable {
        
        final Block block;
        
        final Set<Activity> activities;
        
        final Interval interval;
        
        
        BlockSlotIntervalable(Block block, Collection<Activity> activities, Interval interval) {
            this.block = block;
            this.activities = new HashSet<Activity>(activities);
            this.interval = interval;
        }
        
        public Interval getInterval() {
            return interval;
        }
        
        @Override
        public String toString() {
            return block + "[" + activities + "] at " + interval;
        }
        
        @Override
        public boolean equals(Object object) {
            if (!(object instanceof BlockSlotIntervalable)) {
                return false;
            }
            
            BlockSlotIntervalable other = (BlockSlotIntervalable)object;
            return (
                this.activities.equals(other.activities) &&
                this.interval.equals(other.getInterval())
            );
        }
        
    }
    
}

package hu.webarticum.aurora.plugins.defaultautoput.manipulator.add_block_followings;

import static hu.webarticum.aurora.app.Shortcut.text;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.aurora.plugins.defaultautoput.ScaffoldManipulator;
import hu.webarticum.aurora.plugins.defaultautoput.UserBlockFollowingDefinition;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;

public class AddBlockFollowingsScaffoldManipulator implements ScaffoldManipulator {
    
    @Override
    public void manipulate(Scaffold scaffold) throws CancelledException, CollapseException {
        if (scaffold.userBlockFollowings.isEmpty()) {
            return;
        }
        
        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.add_block_followups.adding_block_followings"));
        scaffold.out.setProgress(null);
        
        for (UserBlockFollowingDefinition following : scaffold.userBlockFollowings) {
            scaffold.registerConstraint(scaffold.new BlockFollowUpConstraint(
                following.getBlock1(),
                following.getBlock2(),
                following.isOrdered()
            ));
        }

        scaffold.out.log(text("plugin.defaultautoput.manipulator.add_block_followups.block_followings_added"));
    }
    
}

package hu.webarticum.aurora.plugins.defaultautoput;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.autoput.AbstractAutoPutter;
import hu.webarticum.aurora.app.util.autoput.AutoPutter;
import hu.webarticum.aurora.app.util.autoput.AutoPutterAspectStructure;
import hu.webarticum.aurora.app.util.autoput.AutoPutterFactory;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.defaultautoput.manipulator.add_block_followings.AddBlockFollowingsScaffoldManipulator;
import hu.webarticum.aurora.plugins.defaultautoput.manipulator.add_conflicts.AddConflictsScaffoldManipulator;
import hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups.DistributeBlockGroupsScaffoldManipulator;
import hu.webarticum.aurora.plugins.defaultautoput.manipulator.eliminate_teacher_holes.EliminateTeacherHolesScaffoldManipulator;
import hu.webarticum.aurora.plugins.defaultautoput.manipulator.ensure_teacher_daymin.EnsureTeacherDayMinScaffoldManipulator;
import hu.webarticum.aurora.plugins.defaultautoput.manipulator.gravitate_classes.GravitateClassesScaffoldManipulator;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;
import hu.webarticum.jsatbuilder.solver.core.Solver;
import hu.webarticum.jsatbuilder.solver.sat4j.DefaultSat4jSolver;

public class DefaultAutoPutterFactory implements AutoPutterFactory {

    private static final long serialVersionUID = 1L;

    public static final String name = "school";
    
    public static final Priority priority = Priority.PRIMARY;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getLabel() {
        return text("plugin.defaultautoput.autoputter.label");
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public AutoPutterAspectStructure collectAspectStructure(Document document, BlockList blocks, Board board) {
        return AutoPutterAspectStructure.EMPTY;
    }
    
    @Override
    public AutoPutter createAutoPutter(Document document, BlockList blocks, Board board, List<String> enabledAspects) {
        return new DefaultAutoPutter(document, blocks, board, enabledAspects);
    }

    public static class DefaultAutoPutter extends AbstractAutoPutter {
        
        public DefaultAutoPutter(Document document, BlockList blocks, Board board, List<String> enabledAspects) {
            init(document, blocks, board, enabledAspects);
        }
        
        @Override
        protected void _run() throws CancelledException {
            try {
                _runUnwrapped();
            } catch (CancelledException e) {
                resultState = ResultState.ABORTED;
                throw e;
            }
        }
        
        protected void _runUnwrapped() throws CancelledException {
            final Scaffold scaffold;
            
            displaySetupDialog();
            
            out.log(text("plugin.defaultautoput.autoputter.started"));
            
            try {
                scaffold = buildScaffold();
            } catch (CollapseException e) {
                resultState = ResultState.UNSOLVED;
                out.setLabel(text("plugin.defaultautoput.autoputter.collapse"));
                out.log(String.format(
                    text("plugin.defaultautoput.autoputter.collapsed"),
                    e.getMessage()
                ));
                out.finish(false);
                return;
            }
            
            out.log(text("plugin.defaultautoput.autoputter.scaffold_built"));
            
            out.tick();
            
            out.setLabel(text("plugin.defaultautoput.autoputter.fill_solver"));
            
            Solver solver = new DefaultSat4jSolver();
            
            scaffold.fillSolver(solver);
            
            out.log(text("plugin.defaultautoput.autoputter.solver_filled"));
            
            out.tick();
            
            out.setLabel(text("plugin.defaultautoput.autoputter.solve_constraints"));
            
            boolean success = false;
            try {
                success = solver.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            if (success) {
                resultState = ResultState.SOLVED;
                scaffold.applySolverModel(solver.getModel());
                out.setLabel(text("plugin.defaultautoput.autoputter.success"));
                out.log(text("plugin.defaultautoput.autoputter.solved"));
                out.finish(true);
            } else {
                resultState = ResultState.UNSOLVED;
                out.setLabel(text("plugin.defaultautoput.autoputter.fail"));
                out.log(text("plugin.defaultautoput.autoputter.unsolvable"));
                for (String tip: scaffold.tips) {
                    out.log("Tipp: " + tip); // XXX
                }
                out.finish(false);
            }
        }
        
        private void displaySetupDialog() throws CancelledException {
            
            // XXX
            
            out.setLabel(text("plugin.defaultautoput.setup.setup"));
            
            try {
                Class<?> setupDialogClass = Class.forName("hu.webarticum.orarend.plugins.basekit.schoolautoputsetup.SchoolAutoputSetupDialog");
                Class<?> jFrameClass = Class.forName("javax.swing.JFrame");
                Constructor<?> setupWindowConstructor = setupDialogClass.getConstructor(jFrameClass, Document.class);
                Object setupDialog = setupWindowConstructor.newInstance(null, document);
                Method runMethod = setupDialogClass.getMethod("run");
                runMethod.invoke(setupDialog);
                Method getResultMethod = setupDialogClass.getMethod("getResult");
                Object resultObject = getResultMethod.invoke(setupDialog);
                int result = (int)(Integer)resultObject;
                Class<?> editDialogClass = Class.forName("hu.webarticum.orarend.plugins.defaultswingui.component.dialog.EditDialog");
                Field resultOkField = editDialogClass.getDeclaredField("RESULT_OK");
                int resultOk = resultOkField.getInt(setupDialog);
                if (result != resultOk) {
                    throw new CancelledException();
                }
            } catch (ClassNotFoundException e) {
            } catch (NoSuchMethodException e) {
            } catch (SecurityException e) {
            } catch (InvocationTargetException e) {
            } catch (IllegalAccessException e) {
            } catch (InstantiationException e) {
            } catch (NoSuchFieldException e) {
            }
        }
        
        private Scaffold buildScaffold() throws CancelledException, CollapseException {
            Scaffold scaffold = new Scaffold(out, document, blocks, board, enabledAspects);
            //scaffold.simplify();

            out.tick();
            out.setLabel("");
            out.setProgress(null);
            
            List<ScaffoldManipulator> manipulators = new ArrayList<ScaffoldManipulator>();
            
            manipulators.add(new DistributeBlockGroupsScaffoldManipulator());
            manipulators.add(new GravitateClassesScaffoldManipulator());
            manipulators.add(new EnsureTeacherDayMinScaffoldManipulator());
            manipulators.add(new EliminateTeacherHolesScaffoldManipulator());
            manipulators.add(new AddConflictsScaffoldManipulator());
            manipulators.add(new AddBlockFollowingsScaffoldManipulator());
            
            for (ScaffoldManipulator manipulator: manipulators) {
                manipulator.manipulate(scaffold);
                //scaffold.simplify();
                out.tick();
                out.setLabel("");
                out.setProgress(null);
            }

            scaffold.simplify();
            
            return scaffold;
        }
        
    }

}

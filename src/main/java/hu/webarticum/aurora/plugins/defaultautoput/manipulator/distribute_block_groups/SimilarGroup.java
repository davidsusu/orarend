package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import hu.webarticum.aurora.app.util.common.CollectionUtil;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.time.Time;

public class SimilarGroup extends HashSet<EqualGroup> {

    private static final long serialVersionUID = 1L;

    public enum Kind {
        EMPTY, SINGLE, IDENTICAL, EQUAL, SIMILAR
    }

    public Set<IdenticalGroup> getIdenticalGroups() {
        Set<IdenticalGroup> identicalGroups = new HashSet<IdenticalGroup>();
        for (EqualGroup equalGroup: this) {
            identicalGroups.addAll(equalGroup);
        }
        return identicalGroups;
    }

    public Block getSampleBlock() {
        for (EqualGroup equalGroup: this) {
            return equalGroup.getSampleBlock();
        }
        return null;
    }
    
    public BlockList getBlocks() {
        BlockList blocks = new BlockList();
        for (EqualGroup equalGroup: this) {
            blocks.addAll(equalGroup.getBlocks());
        }
        return blocks;
    }

    public int getBlockCount() {
        int blockCount = 0;
        for (EqualGroup equalGroup: this) {
            blockCount += equalGroup.getBlockCount();
        }
        return blockCount;
    }
    
    public TreeSet<Time> getDays() {
        TreeSet<Time> days = new TreeSet<Time>();
        for (EqualGroup equalGroup: this) {
            days.addAll(equalGroup.getDays());
        }
        return days;
    }
    
    public Kind getKind() {
        int size = this.size();
        if (size == 0) {
            return Kind.EMPTY;
        } else if (size > 1) {
            return Kind.SIMILAR;
        } else {
            switch (CollectionUtil.getFirst(this).getKind()) {
                case EMPTY:
                    return Kind.EMPTY;
                case SINGLE:
                    return Kind.SINGLE;
                case IDENTICAL:
                    return Kind.IDENTICAL;
                case EQUAL:
                default:
                    return Kind.EQUAL;
            }
        }
    }

    public boolean isShared() {
        for (EqualGroup equalGroup: this) {
            if (equalGroup.getReferenceCount() > 1) {
                return true;
            }
        }
        return false;
    }

    public void unshare() {
        for (EqualGroup equalGroup: this) {
            if (equalGroup.getReferenceCount() > 1) {
                equalGroup.decreaseReferenceCount();
            }
        }
    }
    
}

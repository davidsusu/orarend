package hu.webarticum.aurora.plugins.defaultautoput;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;

public interface ScaffoldManipulator {
    
    public void manipulate(Scaffold scaffold) throws CancelledException, CollapseException;
    
}

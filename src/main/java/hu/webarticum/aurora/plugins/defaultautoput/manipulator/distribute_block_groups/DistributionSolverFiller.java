package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.CustomTimeLimit;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.InvertedTimeLimit;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.core.model.time.TimeLimit;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.jsatbuilder.builder.common.AllViability;
import hu.webarticum.jsatbuilder.builder.common.DefinitionLiteral;
import hu.webarticum.jsatbuilder.builder.common.DefinitionLiteralList;
import hu.webarticum.jsatbuilder.builder.common.OptionalOrConstraint;
import hu.webarticum.jsatbuilder.builder.common.OrConstraint;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;
import hu.webarticum.jsatbuilder.builder.core.Definition;
import hu.webarticum.jsatbuilder.builder.core.SolverFiller;
import hu.webarticum.jsatbuilder.builder.core.Variable;
import hu.webarticum.jsatbuilder.builder.core.Viability;
import hu.webarticum.jsatbuilder.solver.core.Solver;

// TODO: ConflictSet

public class DistributionSolverFiller implements SolverFiller {
    
    protected final Map<EqualGroup, EqualGroupData> equalGroupDataMap = new IdentityHashMap<EqualGroup, EqualGroupData>();

    protected final Map<SimilarGroup, SimilarGroupData> similarGroupDataMap = new IdentityHashMap<SimilarGroup, SimilarGroupData>();
    
    protected final BlockSimilarityTable blockSimilarityTable = new BlockSimilarityTable();

    public final Scaffold scaffold;

    public final BlockDayTable blockDayTable;
    
    
    public DistributionSolverFiller(Scaffold scaffold) {
        this.scaffold = scaffold;
        this.blockDayTable = new BlockDayTable(scaffold);
    }
    
    
    public boolean addSimilarGroup(SimilarGroup similarGroup, boolean allowed2) {
        return addSimilarGroup(similarGroup, allowed2, null, null);
    }
    
    public boolean addSimilarGroup(SimilarGroup similarGroup, boolean allowed2, Variable conditionVariable, Variable conditionVariable2) {
        blockSimilarityTable.addSimilarGroup(similarGroup);
        SimilarGroupData similarGroupData = new SimilarGroupData(conditionVariable);
        similarGroupDataMap.put(similarGroup, similarGroupData);
        for (EqualGroup equalGroup: similarGroup) {
            EqualGroupData equalGroupData;
            try {
                equalGroupData = getEqualGroupData(equalGroup);
            } catch (CommonContradictionException e) {
                scaffold.out.log(String.format(
                    text("plugin.defaultautoput.manipulator.distribute_block_groups.skip_problematic_group"),
                    similarGroup.getBlockCount(),
                    similarGroup.getSampleBlock()
                ));
                return false;
            }
            similarGroupData.addEqualGroupData(equalGroupData);
            if (conditionVariable == null) {
                equalGroupData.setUnconditional();
            } else {
                equalGroupData.addConditionVariable(conditionVariable);
            }
        }

        blockDayTable.merge(similarGroupData.blockDayTable);
        
        if (allowed2 && similarGroup.getBlockCount() == 2) {
            BlockList blocks = similarGroup.getBlocks();
            Block firstBlock = blocks.get(0);
            Block secondBlock = blocks.get(1);

            TreeMap<Time, BlockDayTable.BlockDayItem> firstDayVariableMap = blockDayTable.blockDayItemMap.get(firstBlock);
            TreeMap<Time, BlockDayTable.BlockDayItem> secondDayVariableMap = blockDayTable.blockDayItemMap.get(secondBlock);
            
            boolean foundDayPair = false;
            List<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>> pairConflicts = new ArrayList<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>>();
            
            for (Map.Entry<Time, BlockDayTable.BlockDayItem> firstEntry: firstDayVariableMap.entrySet()) {
                Time firstDay = firstEntry.getKey();
                BlockDayTable.BlockDayItem firstItem = firstEntry.getValue();
                for (Map.Entry<Time, BlockDayTable.BlockDayItem> secondEntry: secondDayVariableMap.entrySet()) {
                    Time secondDay = secondEntry.getKey();
                    BlockDayTable.BlockDayItem secondItem = secondEntry.getValue();
                    if (!firstDay.equals(secondDay)) {
                        if (areDaysPair(firstDay, secondDay)) {
                            foundDayPair = true;
                        } else {
                            pairConflicts.add(new Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>(firstItem, secondItem));
                        }
                    }
                }
            }
            
            if (foundDayPair) {
                similarGroupData.addPairConflicts(pairConflicts, conditionVariable2);
            }
        }
        
        return true;
    }

    @Override
    public void fillSolver(Solver solver) {
        fillSolver(solver, false);
    }
    
    public void fillSolver(Solver solver, boolean useHelpers) {
        fillSolverOrScaffoldWithCommons(solver, useHelpers);
        
        for (Map.Entry<Block, TreeMap<Time, BlockDayTable.BlockDayItem>> entry: blockDayTable.blockDayItemMap.entrySet()) {
            Solver.Clause clause = new Solver.Clause();
            for (BlockDayTable.BlockDayItem item: entry.getValue().values()) {
                Definition itemDefinition = useHelpers ? item.getHelper() : item.getVariable();
                clause.addLiteral(new Solver.Literal(itemDefinition, true));
            }
            solver.addCardinality(clause, 1, 1);
        }
    }

    public void fillScaffold() throws CollapseException {
        fillScaffold(true);
    }
    
    public void fillScaffold(boolean useHelpers) throws CollapseException {
        fillSolverOrScaffoldWithCommons(scaffold, useHelpers);
        
        for (Map.Entry<Block, TreeMap<Time, BlockDayTable.BlockDayItem>> entry: blockDayTable.blockDayItemMap.entrySet()) {
            Block block = entry.getKey();
            TimingSet timingSet = scaffold.getTimingSet(block);
            TreeMap<Time, BlockDayTable.BlockDayItem> dayItemMap = entry.getValue();

            CustomTimeLimit daysTimeLimit = new CustomTimeLimit(false);
            for (Map.Entry<Time, BlockDayTable.BlockDayItem> _entry: dayItemMap.entrySet()) {
                Time day = _entry.getKey();
                TimeLimit dayTimeLimit = new Interval(day, Time.DAY);
                daysTimeLimit = daysTimeLimit.unionWith(new CustomTimeLimit(dayTimeLimit));
            }
            TimingSet removedTimingSet = timingSet.getLimited(new InvertedTimeLimit(daysTimeLimit));
            for (Time time: removedTimingSet.getTimes()) {
                Variable variable = scaffold.blockTimeVariableMap.get(block).get(time);
                variable.remove();
            }
        }
    }

    protected void fillSolverOrScaffoldWithCommons(Object solverOrSolverScaffold, boolean useHelpers) {
        for (Map.Entry<SimilarGroup, SimilarGroupData> entry: similarGroupDataMap.entrySet()) {
            SimilarGroupData similarGroupData = entry.getValue();
            if (similarGroupData.conditionVariable != null) {
                DefinitionLiteralList conditionClause = new DefinitionLiteralList(similarGroupData.conditionVariable);
                // XXX
                //addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conditionClause, true, true);
                addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conditionClause, true, false);
            }
            if (similarGroupData.conditionVariable2 != null) {
                DefinitionLiteralList conditionClause2 = new DefinitionLiteralList(similarGroupData.conditionVariable2);
                
                // XXX
                //addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conditionClause2, true, true);
                addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conditionClause2, true, false);
            }
            for (Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem> conflict: similarGroupData.blockDayTable.getConflicts()) {
                BlockDayTable.BlockDayItem item1 = conflict.getLeft();
                Block block1 = item1.getBlock();
                Definition definition1 = useHelpers ? item1.getHelper() : item1.getVariable();
                BlockDayTable.BlockDayItem item2 = conflict.getRight();
                Block block2 = item2.getBlock();
                Definition definition2 = useHelpers ? item2.getHelper() : item2.getVariable();
                if (!blockSimilarityTable.areBlocksEqual(block1, block2)) {
                    DefinitionLiteralList conflictClause = new DefinitionLiteralList();
                    conflictClause.add(new DefinitionLiteral(definition1, false));
                    conflictClause.add(new DefinitionLiteral(definition2, false));
                    if (similarGroupData.conditionVariable != null) {
                        conflictClause.add(new DefinitionLiteral(similarGroupData.conditionVariable, false));
                    }
                    
                    // XXX: required?
                    addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, false);
                }
            }
            if (!similarGroupData.pairConflicts.isEmpty()) {
                for (Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem> pair: similarGroupData.pairConflicts) {
                    BlockDayTable.BlockDayItem item1 = pair.getLeft();
                    BlockDayTable.BlockDayItem item2 = pair.getRight();
                    Definition definition1 = useHelpers ? item1.getHelper() : item1.getVariable();
                    Definition definition2 = useHelpers ? item2.getHelper() : item2.getVariable();
                    DefinitionLiteralList conflictClause = new DefinitionLiteralList();
                    conflictClause.add(new DefinitionLiteral(definition1, false));
                    conflictClause.add(new DefinitionLiteral(definition2, false));
                    if (similarGroupData.conditionVariable2 != null) {
                        conflictClause.add(new DefinitionLiteral(similarGroupData.conditionVariable2, false));
                    }
                    
                    // XXX
                    //addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, true);
                    addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, false);
                }
                if (similarGroupData.conditionVariable != null && similarGroupData.conditionVariable2 != null) {
                    DefinitionLiteralList conditionsClause = new DefinitionLiteralList();
                    conditionsClause.add(new DefinitionLiteral(similarGroupData.conditionVariable, true));
                    conditionsClause.add(new DefinitionLiteral(similarGroupData.conditionVariable2, false));

                    // XXX
                    //addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conditionsClause, false, true);
                    addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conditionsClause, false, false);
                }
            }
        }
        
        for (Map.Entry<EqualGroup, EqualGroupData> entry: equalGroupDataMap.entrySet()) {
            EqualGroupData equalGroupData = entry.getValue();
            for (Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem> pair: equalGroupData.equalConflicts) {
                BlockDayTable.BlockDayItem item1 = pair.getLeft();
                BlockDayTable.BlockDayItem item2 = pair.getRight();
                Definition definition1 = useHelpers ? item1.getHelper() : item1.getVariable();
                Definition definition2 = useHelpers ? item2.getHelper() : item2.getVariable();
                if (equalGroupData.isUnconditional) {
                    DefinitionLiteralList conflictClause = new DefinitionLiteralList();
                    conflictClause.add(new DefinitionLiteral(definition1, false));
                    conflictClause.add(new DefinitionLiteral(definition2, false));

                    // FIXME: what is here?
                    //addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, true);
                    addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, false);
                } else {
                    for (Variable conditionVariable: equalGroupData.conditionVariables) {
                        DefinitionLiteralList conflictClause = new DefinitionLiteralList();
                        conflictClause.add(new DefinitionLiteral(definition1, false));
                        conflictClause.add(new DefinitionLiteral(definition2, false));
                        conflictClause.add(new DefinitionLiteral(conditionVariable, false));

                        // FIXME: what is here?
                        //addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, true);
                        addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, false);
                    }
                }
            }

            for (Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem> pair: equalGroupData.identicalConflicts) {
                BlockDayTable.BlockDayItem item1 = pair.getLeft();
                BlockDayTable.BlockDayItem item2 = pair.getRight();
                Definition definition1 = useHelpers ? item1.getHelper() : item1.getVariable();
                Definition definition2 = useHelpers ? item2.getHelper() : item2.getVariable();
                DefinitionLiteralList conflictClause = new DefinitionLiteralList();
                conflictClause.add(new DefinitionLiteral(definition1, false));
                conflictClause.add(new DefinitionLiteral(definition2, false));

                // FIXME: what is here?
                //addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, true);
                addSimpleClauseToSolverOrScaffold(solverOrSolverScaffold, conflictClause, false, false);
            }
        }
    }

    protected void addSimpleClauseToSolverOrScaffold(Object solverOrSolverScaffold, DefinitionLiteralList literals, boolean isOptional, boolean required) {
        if (solverOrSolverScaffold instanceof Solver) {
            Solver solver = (Solver)solverOrSolverScaffold;
            Solver.Clause clause = new Solver.Clause();
            for (DefinitionLiteral literal: literals) {
                clause.addLiteral(literal.toSolverLiteral());
            }
            if (isOptional) {
                solver.addOptional(clause, 100);
            } else {
                solver.add(clause);
            }
        } else if (solverOrSolverScaffold instanceof Scaffold) {
            Scaffold scaffold = (Scaffold)solverOrSolverScaffold;
            if (isOptional) {
                
                // XXX
                class EOptionalOrConstraint extends OptionalOrConstraint {
                    private final Viability viability;
                    private final DefinitionLiteralList originalLiterals;
                    public EOptionalOrConstraint(DefinitionLiteralList literals, int weight) {
                        super(literals, weight);
                        this.originalLiterals = literals;
                        this.viability = new AllViability(literals.stream().map(DefinitionLiteral::getDefinition).collect(Collectors.toList()));
                    }
                    @Override
                    public String toString() {
                        return super.toString() + " // original: " +
                                originalLiterals.stream()
                                    .map(d -> d.getDefinition() + ":" + d.isPositive())
                                    .collect(Collectors.toList());
                    }
                    @Override
                    public Viability getViability() {
                        return viability;
                    }
                }

                // XXX
                //scaffold.registerConstraint(new OptionalOrConstraint(literals, 100));
                scaffold.registerConstraint(new EOptionalOrConstraint(literals, 100).setRequired(required));
            } else {
                
                // XXX
                class EOrConstraint extends OrConstraint {
                    private final Viability viability;
                    private final DefinitionLiteralList originalLiterals;
                    public EOrConstraint(DefinitionLiteralList literals) {
                        super(literals);
                        this.originalLiterals = literals;
                        this.viability = new AllViability(literals.stream().map(DefinitionLiteral::getDefinition).collect(Collectors.toList()));
                    }
                    @Override
                    public String toString() {
                        return super.toString() + " // original: " +
                                originalLiterals.stream()
                                    .map(d -> d.getDefinition() + ":" + d.isPositive())
                                    .collect(Collectors.toList());
                    }
                    @Override
                    public Viability getViability() {
                        return viability;
                    }
                }

                // XXX
                //scaffold.registerConstraint(new OrConstraint(literals).setRequired(required));
                scaffold.registerConstraint(new EOrConstraint(literals).setRequired(required));
            }
        }
    }
    
    protected boolean areDaysPair(Time day1, Time day2) {
        // FIXME
        int weekCount = 1;
        int weekWeekdayCount = 5;

        long weekNo1 = day1.getPart(Time.PART.WEEKS);
        long weekNo2 = day2.getPart(Time.PART.WEEKS);

        long dayNo1 = day1.getPart(Time.PART.DAYS);
        long dayNo2 = day2.getPart(Time.PART.DAYS);
        
        if (weekNo1 == weekNo2) {
            if (Math.abs(dayNo2-dayNo1) < 2) {
                return false;
            }
        }

        boolean isWeeks12 = (weekNo2 == weekNo1 + 1);
        if (weekNo1 == weekCount-1 && weekNo2 == 0) {
            isWeeks12 = true;
        }
        
        boolean isWeeks21 = (weekNo1 == weekNo2 + 1);
        if (weekNo2 == weekCount-1 && weekNo1 == 0) {
            isWeeks21 = true;
        }
        
        if (isWeeks12 && dayNo1 >= weekWeekdayCount-1 && dayNo2 == 0) {
            return false;
        }

        if (isWeeks21 && dayNo2 >= weekWeekdayCount-1 && dayNo1 == 0) {
            return false;
        }
        
        return true;
    }
    
    protected EqualGroupData getEqualGroupData(EqualGroup equalGroup) throws CommonContradictionException {
        if (equalGroupDataMap.containsKey(equalGroup)) {
            return equalGroupDataMap.get(equalGroup);
        } else {
            EqualGroupData equalGroupData = new EqualGroupData(equalGroup);
            equalGroupDataMap.put(equalGroup, equalGroupData);
            return equalGroupData;
        }
    }
    
    public class EqualGroupData {
        
        public final BlockDayTable blockDayTable = new BlockDayTable(scaffold);
        
        public final List<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>> identicalConflicts = new ArrayList<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>>();
        
        public final List<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>> equalConflicts = new ArrayList<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>>();
        
        public final Set<Variable> conditionVariables = new HashSet<Variable>();
        
        public boolean isUnconditional = false;
        
        public EqualGroupData(EqualGroup equalGroup) throws CommonContradictionException {
            for (IdenticalGroup identicalGroup: equalGroup) {
                processIdenticalGroup(identicalGroup);
            }
            
            equalConflicts.addAll(blockDayTable.getConflicts());
            
            for (Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem> pair: identicalConflicts) {
                equalConflicts.remove(pair);
                equalConflicts.remove(pair.getReversed());
            }
        }
        
        public void processIdenticalGroup(IdenticalGroup identicalGroup) throws CommonContradictionException {
            IdenticalGroupDistributor distributor = new IdenticalGroupDistributor(scaffold, identicalGroup);
            if (!distributor.success) {
                throw new CommonContradictionException();
            }
            blockDayTable.merge(distributor.blockDayTable);
            this.identicalConflicts.addAll(distributor.blockDayTable.getConflicts());
        }

        public void addConditionVariable(Variable variable) {
            conditionVariables.add(variable);
        }

        public void setUnconditional() {
            isUnconditional = true;
        }
        
    }
    
    protected class SimilarGroupData {
        
        public final List<EqualGroupData> equalGroupDatas = new ArrayList<EqualGroupData>();

        public final BlockDayTable blockDayTable = new BlockDayTable(scaffold);

        public final Variable conditionVariable;

        public Variable conditionVariable2 = null;

        public final List<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>> pairConflicts = new ArrayList<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>>();

        public SimilarGroupData() {
            this(null);
        }

        public SimilarGroupData(Variable conditionVariable) {
            this.conditionVariable = conditionVariable;
        }
        
        public void addEqualGroupData(EqualGroupData equalGroupData) {
            equalGroupDatas.add(equalGroupData);
            blockDayTable.merge(equalGroupData.blockDayTable);
        }
        
        public void addPairConflicts(List<Pair<BlockDayTable.BlockDayItem, BlockDayTable.BlockDayItem>> pairConflicts, Variable conditionVariable2) {
            // FIXME: identicalConflicts...
            this.conditionVariable2 = conditionVariable2;
            this.pairConflicts.clear();
            this.pairConflicts.addAll(pairConflicts);
        }
        
    }
    
    protected class CommonContradictionException extends Exception {
        
        private static final long serialVersionUID = 1L;
        
    }
    
}

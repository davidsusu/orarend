package hu.webarticum.aurora.plugins.defaultautoput;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.autoput.AutoPutter.AutoPutOutput;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.jsatbuilder.builder.common.AnyHelper;
import hu.webarticum.jsatbuilder.builder.common.HelperMap;
import hu.webarticum.jsatbuilder.builder.common.OneConstraint;
import hu.webarticum.jsatbuilder.builder.common.OrConstraint;
import hu.webarticum.jsatbuilder.builder.core.AbstractConstraint;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;
import hu.webarticum.jsatbuilder.builder.core.Constraint;
import hu.webarticum.jsatbuilder.builder.core.ConstraintSetSolverFiller;
import hu.webarticum.jsatbuilder.builder.core.Definition;
import hu.webarticum.jsatbuilder.builder.core.Helper;
import hu.webarticum.jsatbuilder.builder.core.RemovalListener;
import hu.webarticum.jsatbuilder.builder.core.SolverFiller;
import hu.webarticum.jsatbuilder.builder.core.Variable;
import hu.webarticum.jsatbuilder.builder.core.Viability;
import hu.webarticum.jsatbuilder.solver.core.Solver;

public class Scaffold implements SolverFiller {
    
    public final AutoPutOutput out;
    public final Document document;
    public final Board board;
    
    public final BlockList blocks;
    
    public final Map<Block, SortedMap<Time, Variable>> blockTimeVariableMap = new HashMap<Block, SortedMap<Time, Variable>>();
    
    public final Map<Variable, Pair<Block, Time>> variableBlockTimeMap = new HashMap<Variable, Pair<Block, Time>>();
    
    public final Map<Block, HelperMap<Long>> blockDayHelperMap = new HashMap<Block, HelperMap<Long>>();
    
    
    private final Map<Block, Set<Block>> blockConflictMap = new HashMap<Block, Set<Block>>();
    
    private final ConstraintSetSolverFiller constraintSet = new ConstraintSetSolverFiller();
    
    private final Set<Block> foundFixedBlocks = new HashSet<Block>();
    
    private final BlockTimeVariableRemovalListener blockTimeVariableRemovalListener = new BlockTimeVariableRemovalListener();
    
    
    // XXX
    public final List<UserBlockFollowingDefinition> userBlockFollowings = new ArrayList<UserBlockFollowingDefinition>();
    
    // XXX
    public final List<String> tips = new ArrayList<String>();
    
    
    public Scaffold(AutoPutOutput out, Document document, Collection<Block> blocks, Board board, List<String> enabledAspects) throws CancelledException, CollapseException {
        this.out = out;
        this.document = document;
        this.board = board;
        
        Value extraData = document.getExtraData();
        
        this.blocks = new BlockList(blocks);
        this.blocks.addAll(board.getBlocks());
        

        Value.ValueList followingData = extraData.getAccess("autoput.school.setup.followings").get().getAsList();

        for (Value followingItem : followingData) {
            UserBlockFollowingDefinition followingDefinition = new UserBlockFollowingDefinition(
                followingItem.getAccess("ordered").get().getAsBoolean(),
                followingItem.getAccess("block1").get().getAsBlock(),
                followingItem.getAccess("block2").get().getAsBlock(),
                followingItem.getAccess("comment").get().getAsString()
            );
            userBlockFollowings.add(followingDefinition);
        }
        
        out.setLabel(text("plugin.defaultautoput.scaffold.preprocess_blocks.preprocess_blocks"));
        out.setProgress(0d);

        double progress = 0;
        double progressDiff = 1d / this.blocks.size();
        
        int blockCount = this.blocks.size();
        for (int i = 0; i < blockCount; i++) {
            Block block = this.blocks.get(i);
            
            Set<Block> conflictBlockSet = blockConflictMap.get(block);
            if (conflictBlockSet == null) {
                conflictBlockSet = new HashSet<Block>();
                blockConflictMap.put(block, conflictBlockSet);
            }
            blockConflictMap.put(block, conflictBlockSet);
            for (int j = i + 1; j < blockCount; j++) {
                Block otherBlock = this.blocks.get(j);
                Set<Block> otherConflictBlockSet = blockConflictMap.get(otherBlock);
                if (otherConflictBlockSet == null) {
                    otherConflictBlockSet = new HashSet<Block>();
                    blockConflictMap.put(otherBlock, otherConflictBlockSet);
                }
                if (block.conflictsWith(otherBlock)) {
                    conflictBlockSet.add(otherBlock);
                    otherConflictBlockSet.add(block);
                }
            }
            
            SortedMap<Time, Variable> timeVariableMap = new TreeMap<Time, Variable>();
            Board.Entry boardEntry = board.search(block);
            if (boardEntry != null) {
                Time time = boardEntry.getTime();
                Variable variable = new Variable(String.format(
                    text("plugin.defaultautoput.scaffold.variable.block_at_predefined"),
                    block.getLabel(), time.toReadableString()
                ));
                registerBlockTimeVariable(variable);
                timeVariableMap.put(time, variable);
                blockTimeVariableMap.put(block, timeVariableMap);
                variableBlockTimeMap.put(variable, new Pair<Block, Time>(block, time));
                foundFixedBlocks.add(block);
                registerConstraint(new OrConstraint(variable).setLabel(String.format(
                    text("plugin.defaultautoput.scaffold.constraint.fix_block_at"),
                    block.getLabel(), time
                )));
            } else {
                TimingSet limitedTimingSet = block.getCalculatedTimingSet(true);
                blockTimeVariableMap.put(block, timeVariableMap);
                List<Variable> blockVariables = new ArrayList<Variable>();
                for (TimingSet.TimeEntry timeEntry: limitedTimingSet) {
                    Time time = timeEntry.getTime();
                    if (!board.conflictsWithBlockAt(block, time)) {
                        Variable variable = new Variable(String.format(
                            text("plugin.defaultautoput.scaffold.variable.block_at"),
                            block.getLabel(), time.toReadableString()
                        ));
                        registerBlockTimeVariable(variable);
                        blockVariables.add(variable);
                        timeVariableMap.put(time, variable);
                        variableBlockTimeMap.put(variable, new Pair<Block, Time>(block, time));
                    }
                }
                if (timeVariableMap.isEmpty()) {
                    throw new CollapseException(String.format(
                        text("plugin.defaultautoput.scaffold.preprocess_blocks.block_has_no_place"),
                        block.getLabel()
                    ));
                } else if (timeVariableMap.size() == 1) {
                    foundFixedBlocks.add(block);
                    Time onlyTime = variableBlockTimeMap.get(blockVariables.get(0)).getRight();
                    registerConstraint(new OrConstraint(blockVariables).setLabel(String.format(
                        text("plugin.defaultautoput.scaffold.constraint.fix_block_at"),
                        block.getLabel(), onlyTime.toReadableString()
                    )));
                } else {
                    registerConstraint(new OneConstraint(blockVariables).setLabel(String.format(
                        text("plugin.defaultautoput.scaffold.constraint.place_block"),
                        block.getLabel()
                    )));
                }
            }
            
            if (!timeVariableMap.isEmpty()) {
                HelperMap<Long> dayHelperMap = new HelperMap<Long>();
                List<Variable> dayVariables = null;
                long previousDay = -1;
                for (Map.Entry<Time, Variable> entry: timeVariableMap.entrySet()) {
                    Time time = entry.getKey();
                    Variable variable = entry.getValue();
                    
                    long day = time.getPart(Time.PART.FULLDAYS);
                    
                    if (dayVariables == null) {
                        dayVariables = new ArrayList<Variable>();
                    } else if (day != previousDay) {
                        Time previousDayTime = new Time(previousDay * Time.DAY);
                        Helper dayHelper = new AnyHelper(dayVariables).setLabel(String.format(
                            text("plugin.defaultautoput.scaffold.helper.block_day"),
                            block.getLabel(), previousDayTime.toReadableString()
                        ));
                        dayHelperMap.put(previousDay, dayHelper);
                        dayVariables = new ArrayList<Variable>();
                    }
                    
                    dayVariables.add(variable);
                    
                    previousDay = day;
                }
                if (dayVariables != null && !dayVariables.isEmpty()) {
                    Time previousDayTime = new Time(previousDay * Time.DAY);
                    Helper dayHelper = new AnyHelper(dayVariables).setLabel(String.format(
                        text("plugin.defaultautoput.scaffold.helper.block_day"),
                        block.getLabel(), previousDayTime.toReadableString()
                    ));
                    dayHelperMap.put(previousDay, dayHelper);
                }
                blockDayHelperMap.put(block, dayHelperMap);
            }

            out.setProgress(progress += progressDiff);
            out.tick();
        }
        
        out.log(text("plugin.defaultautoput.scaffold.preprocess_blocks.finished"));

        out.setLabel("");
        out.setProgress(null);
    }
    
    public boolean areInConflict(Block block1, Block block2) {
        Set<Block> conflictingBlocks = blockConflictMap.get(block1);
        return (conflictingBlocks != null && conflictingBlocks.contains(block2));
    }

    // TODO: improve Scaffold as a data structure, and move this to SimplifyScaffoldManipulator
    public void simplify() throws CollapseException {
        boolean findMoreFixedBlock = true;
        while (findMoreFixedBlock) {
            findMoreFixedBlock = false;
            for (Map.Entry<Block, SortedMap<Time, Variable>> entry: blockTimeVariableMap.entrySet()) {
                Block block = entry.getKey();
                Map<Time, Variable> timeVariableMap = entry.getValue();
                if (timeVariableMap.size() == 1 && !foundFixedBlocks.contains(block)) {
                    Time onlyTime = timeVariableMap.keySet().iterator().next();
                    Interval onlyInterval = new Interval(onlyTime, block.getLength());
                    List<Variable> variablesToRemove = new ArrayList<Variable>();
                    for (Block conflictingBlock: blockConflictMap.get(block)) {
                        Iterator<Map.Entry<Time, Variable>> checkTimeIterator = blockTimeVariableMap.get(conflictingBlock).entrySet().iterator();
                        while (checkTimeIterator.hasNext()) {
                            Map.Entry<Time, Variable> timeVariableEntry = checkTimeIterator.next();
                            Time checkTime = timeVariableEntry.getKey();
                            Variable variable = timeVariableEntry.getValue();
                            Interval checkInterval = new Interval(checkTime, conflictingBlock.getLength());
                            if (onlyInterval.intersects(checkInterval)) {
                                variablesToRemove.add(variable);
                            }
                        }
                    }

                    for (Variable variable: variablesToRemove) {
                        variable.remove();
                    }
                    
                    foundFixedBlocks.add(block);
                    findMoreFixedBlock = true;
                    break;
                }
            }
        }
    }
    
    public TimingSet getTimingSet(Block block) {
        Map<Time, Variable> timeVariableMap = blockTimeVariableMap.get(block);
        
        if (timeVariableMap == null) {
            return block.getCalculatedTimingSet();
        }
        
        TimingSet currentTimingSet = new TimingSet();
        for (Map.Entry<Time, Variable> entry: timeVariableMap.entrySet()) {
            currentTimingSet.add(entry.getKey());
        }
        
        return currentTimingSet;
    }
    
    public void registerConstraint(Constraint constraint) {
        constraintSet.add(constraint);
    }
    
    public boolean isFixed(Block block) {
        if (foundFixedBlocks.contains(block)) {
            return true;
        }
        
        if (!blockTimeVariableMap.containsKey(block)) {
            return false;
        }
        
        if (blockTimeVariableMap.get(block).size() == 1) {
            foundFixedBlocks.add(block);
            return true;
        }
        
        return false;
    }
    
    @Override
    public void fillSolver(Solver solver) {
        constraintSet.fillSolver(solver);
    }

    public void applySolverModel(Solver.Model solverModel) {
        for (Solver.Literal literal: solverModel) {
            if (literal.isPositive()) {
                Object positiveObject = literal.getVariable();
                if (variableBlockTimeMap.containsKey(positiveObject)) {
                    Pair<Block, Time> blockTimePair = variableBlockTimeMap.get(positiveObject);
                    Block block = blockTimePair.getLeft();
                    Time time = blockTimePair.getRight();
                    board.add(block, time);
                }
            }
        }
    }
    
    private void registerBlockTimeVariable(Variable variable) {
        variable.addRemovalListener(blockTimeVariableRemovalListener);
    }
    
    public class BlockFollowUpConstraint extends AbstractConstraint {

        public final long MIN_REST_TIME = 0;
        // public final long MAX_REST_TIME = Time.MINUTE * 25;
        
        // FIXME SZIGNUM 2020
        public final long MAX_REST_TIME = Time.MINUTE * 42;
        
        private final Block block1;
        private final Block block2;
        private final boolean directed;
        private final Definition condition;
        
        private final Viability viability = new BlockFollowUpViability();
        
        private final List<FollowItem> followItems = new ArrayList<FollowItem>();

        public BlockFollowUpConstraint(Block block1, Block block2) {
            this(block1, block2, true);
        }

        public BlockFollowUpConstraint(Block block1, Block block2, boolean directed) {
            this(block1, block2, directed, null);
        }

        public BlockFollowUpConstraint(Block block1, Block block2, boolean directed, Definition condition) {
            super(false);
            this.block1 = block1;
            this.block2 = block2;
            this.directed = directed;
            this.condition = condition;
            
            try {
                init();
            } catch (CollapseException e) {
                throw new RuntimeException(e);
            }
        }

        public BlockFollowUpConstraint(Block block1, Block block2, boolean directed, boolean required) throws CollapseException {
            this(block1, block2, directed, null, required);
        }

        public BlockFollowUpConstraint(Block block1, Block block2, boolean directed, Definition condition, boolean required) throws CollapseException {
            super(required);
            this.block1 = block1;
            this.block2 = block2;
            this.directed = directed;
            this.condition = condition;
            
            init();
        }
        
        public void init() throws CollapseException {
            if (
                !blockTimeVariableMap.containsKey(block1) ||
                !blockTimeVariableMap.containsKey(block2)
            ) {
                remove();
                return;
            }
            
            long length2 = block2.getLength();
            
            SortedMap<Time, Variable> timeVariableMap1 = blockTimeVariableMap.get(block1);
            SortedMap<Time, Variable> timeVariableMap2 = blockTimeVariableMap.get(block2);
            
            for (Map.Entry<Time, Variable> entry: timeVariableMap1.entrySet()) {
                Time baseTime = entry.getKey();
                Interval baseInterval = new Interval(baseTime, block1.getLength());
                Variable baseVariable = entry.getValue();
                
                Variable previousVariable = null;
                if (!directed) {
                    long beforeSeconds = baseTime.getSeconds() - length2;
                    Time beforeFromTime = new Time(beforeSeconds - MAX_REST_TIME);
                    Time beforeToTime = new Time(beforeSeconds - MIN_REST_TIME + 1);
                    SortedMap<Time, Variable> beforeSubMap = timeVariableMap2.subMap(beforeFromTime, beforeToTime);
                    if (!beforeSubMap.isEmpty()) {
                        previousVariable = beforeSubMap.get(beforeSubMap.lastKey());
                    }
                }

                Variable nextVariable = null;
                long afterSeconds = baseInterval.getEnd().getSeconds();
                Time afterFromTime = new Time(afterSeconds + MIN_REST_TIME);
                Time afterToTime = new Time(afterSeconds + MAX_REST_TIME + 1);
                SortedMap<Time, Variable> afterSubMap = timeVariableMap2.subMap(afterFromTime, afterToTime);
                if (!afterSubMap.isEmpty()) {
                    nextVariable = afterSubMap.get(afterSubMap.firstKey());
                }
                
                if (previousVariable != null || nextVariable != null) {
                    FollowItem followItem = new FollowItem();
                    followItem.previousVariable = previousVariable;
                    followItem.baseVariable = baseVariable;
                    followItem.nextVariable = nextVariable;
                    followItems.add(followItem);
                }
            }
            
            if (!viability.isViable()) {
                remove();
            }
        }
        
        @Override
        public List<Definition> getDependencies() {
            Set<Definition> dependencies = new HashSet<Definition>();
            if (condition != null) {
                dependencies.add(condition);
            }
            for (FollowItem followItem: followItems) {
                if (followItem.previousVariable != null) {
                    dependencies.add(followItem.previousVariable);
                }
                dependencies.add(followItem.baseVariable);
                if (followItem.nextVariable != null) {
                    dependencies.add(followItem.nextVariable);
                }
            }
            return new ArrayList<Definition>(dependencies);
        }

        @Override
        public Viability getViability() {
            return viability;
        }

        @Override
        public void fillSolver(Solver solver) {
            Solver.Clause possibilitiesClause = new Solver.Clause();
            
            for (FollowItem followItem: followItems) {
                Solver.Clause itemClause = new Solver.Clause();
                itemClause.addLiteral(new Solver.Literal(followItem.baseVariable, false));
                if (followItem.previousVariable != null) {
                    itemClause.addLiteral(new Solver.Literal(followItem.previousVariable, true));
                }
                if (followItem.nextVariable != null) {
                    itemClause.addLiteral(new Solver.Literal(followItem.nextVariable, true));
                }
                if (condition != null) {
                    itemClause.addLiteral(new Solver.Literal(condition, false));
                }
                solver.add(itemClause);
                
                possibilitiesClause.addLiteral(new Solver.Literal(followItem.baseVariable, true));
            }

            solver.addCardinality(possibilitiesClause, 1, 1);
        }

        @Override
        protected void freeDefinition(Definition definition) {
        }
        
        @Override
        public String toString() {
            return
                (directed ? "Directed" : "Undirected") +
                " block follow-up: " + block1 + " and " + block2 +
                ((condition != null) ? "; condition: " + condition : "")
            ;
        }
        
        private class FollowItem {

            Variable previousVariable = null;
            Variable baseVariable = null;
            Variable nextVariable = null;
            
        }
        
        private class BlockFollowUpViability implements Viability {
            
            @Override
            public boolean isViable() {
                if (followItems.isEmpty()) {
                    return false;
                }
                if (condition != null && condition.isRemoved()) {
                    return false;
                }
                return true;
            }

            @Override
            public void removeDefinition(Definition definition) {
                Iterator<FollowItem> iterator = followItems.iterator();
                while (iterator.hasNext()) {
                    FollowItem followItem = iterator.next();
                    if (followItem.baseVariable == definition) {
                        iterator.remove();
                    } else {
                        if (followItem.previousVariable == definition) {
                            followItem.previousVariable = null;
                        } else if (followItem.nextVariable == definition) {
                            followItem.nextVariable = null;
                        }
                        if (followItem.previousVariable == null && followItem.nextVariable == null) {
                            iterator.remove();
                        }
                    }
                }
            }
            
        }
        
    }
    
    private class BlockTimeVariableRemovalListener implements RemovalListener {

        @Override
        public void definitionRemoved(Definition definition) throws CollapseException {
            Pair<Block, Time> blockTimePair = variableBlockTimeMap.get(definition);
            if (blockTimePair != null) {
                variableBlockTimeMap.remove(definition);
                Block block = blockTimePair.getLeft();
                Time time = blockTimePair.getRight();
                blockTimeVariableMap.get(block).remove(time);
            }
        }
        
    }
    
}

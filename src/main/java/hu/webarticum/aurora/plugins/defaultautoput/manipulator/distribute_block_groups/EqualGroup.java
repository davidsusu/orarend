package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import java.util.HashSet;
import java.util.TreeSet;

import hu.webarticum.aurora.app.util.common.CollectionUtil;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.time.Time;

public class EqualGroup extends HashSet<IdenticalGroup> {

    private static final long serialVersionUID = 1L;
    
    protected int referenceCount = 1;
    
    public enum Kind {
        EMPTY, SINGLE, IDENTICAL, EQUAL
    }
    
    // TODO
    // isHandled
    // getDayXYs ...
    // ...
    
    public Block getSampleBlock() {
        for (IdenticalGroup identicalGroup: this) {
            return identicalGroup.getSampleBlock();
        }
        return null;
    }
    
    public BlockList getBlocks() {
        BlockList blocks = new BlockList();
        for (IdenticalGroup identicalGroup: this) {
            blocks.addAll(identicalGroup);
        }
        return blocks;
    }
    
    public int getBlockCount() {
        int blockCount = 0;
        for (IdenticalGroup identicalGroup: this) {
            blockCount += identicalGroup.size();
        }
        return blockCount;
    }
    
    public TreeSet<Time> getDays() {
        TreeSet<Time> days = new TreeSet<Time>();
        for (IdenticalGroup identicalGroup: this) {
            days.addAll(identicalGroup.getDays());
        }
        return days;
    }
    
    public Kind getKind() {
        int size = this.size();
        if (size == 0) {
            return Kind.EMPTY;
        } else if (size > 1) {
            return Kind.EQUAL;
        } else {
            int identicalSize = CollectionUtil.getFirst(this).size();
            if (identicalSize == 0) {
                return Kind.EMPTY;
            } else if (identicalSize == 1) {
                return Kind.SINGLE;
            } else {
                return Kind.IDENTICAL;
            }
        }
    }
    
    public void increaseReferenceCount() {
        referenceCount++;
    }

    public void decreaseReferenceCount() {
        referenceCount++;
    }
    
    public int getReferenceCount() {
        return referenceCount;
    }
    
    
}

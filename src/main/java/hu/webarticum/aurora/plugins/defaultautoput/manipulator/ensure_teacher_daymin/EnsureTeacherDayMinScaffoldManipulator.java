package hu.webarticum.aurora.plugins.defaultautoput.manipulator.ensure_teacher_daymin;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.aurora.plugins.defaultautoput.ScaffoldManipulator;
import hu.webarticum.jsatbuilder.builder.common.AnyHelper;
import hu.webarticum.jsatbuilder.builder.common.DefinitionLiteral;
import hu.webarticum.jsatbuilder.builder.common.OrConstraint;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;
import hu.webarticum.jsatbuilder.builder.core.Helper;
import hu.webarticum.jsatbuilder.builder.core.Variable;


public class EnsureTeacherDayMinScaffoldManipulator implements ScaffoldManipulator {

    @Override
    public void manipulate(Scaffold scaffold) throws CancelledException, CollapseException {
        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.ensure_teacher_daymin.ensure_teacher_daymin"));
        scaffold.out.setProgress(0d);

        Value.ValueSet reqDay2TeachersData = scaffold.document.getExtraData().getAccess("autoput.school.setup.req_day_2_teachers").get().getAsSet();

        double progress = 0;
        double progressDiff = 1d / reqDay2TeachersData.size();
        
        for (Value value: reqDay2TeachersData) {
            Resource teacher = value.getAsResource();
            handleTeacher(scaffold, teacher);

            scaffold.out.setProgress(progress += progressDiff);
            scaffold.out.tick();
        }

        scaffold.out.log(text("plugin.defaultautoput.manipulator.ensure_teacher_daymin.finished"));
    }

    private void handleTeacher(Scaffold scaffold, Resource teacher) throws CollapseException {
        TreeMap<Time, List<Variable>> dayVariablesMap = new TreeMap<Time, List<Variable>>();
        
        int totalPeriodCount = scaffold.blocks.getPeriods().size();
        
        for (Block block: scaffold.blocks) {
            Block.ActivityManager activityManager = block.getActivityManager();
            ActivityList activities = activityManager.getActivities();
            boolean contains = false;
            for (Resource resource: activities.getResources()) {
                if (resource.equals(teacher)) {
                    contains = true;
                    break;
                }
            }

            if (contains) {
                int periodCount = activityManager.getPeriods().size();
                
                boolean containsSubject = false;
                for (Tag tag: activities.getTags()) {
                    if (tag.getType() == Tag.Type.SUBJECT) {
                        containsSubject = true;
                        break;
                    }
                }
                
                if (containsSubject && periodCount == totalPeriodCount) {
                    TimingSet timingSet = scaffold.getTimingSet(block);
                    TreeMap<Time, TimingSet> dayTimingSetMap = timingSet.split(Time.DAY);
                    for (Map.Entry<Time, TimingSet> entry: dayTimingSetMap.entrySet()) {
                        Time day = entry.getKey();
                        for (Time time: entry.getValue().getTimes()) {
                            Variable variable = scaffold.blockTimeVariableMap.get(block).get(time);
                            List<Variable> variables;
                            if (dayVariablesMap.containsKey(day)) {
                                variables = dayVariablesMap.get(day);
                            } else {
                                variables = new ArrayList<Variable>();
                                dayVariablesMap.put(day, variables);
                            }
                            variables.add(variable);
                        }
                    }
                    
                }
            }
        }
        
        for (Map.Entry<Time, List<Variable>> entry: dayVariablesMap.entrySet()) {
            Time dayTime = entry.getKey();
            List<Variable> variables = entry.getValue();
            
            if (variables.size()==1) {
                Variable onlyVariable = variables.get(0);
                onlyVariable.remove();
                continue;
            }
            
            Helper teacherDayHelper = new AnyHelper(variables).setLabel(teacher + " at day " + dayTime.toReadableString());
            
            for (Variable outVariable: variables) {
                List<Variable> remainVariables = new ArrayList<Variable>(variables);
                remainVariables.remove(outVariable);
                
                List<DefinitionLiteral> literals = new ArrayList<DefinitionLiteral>();
                literals.add(new DefinitionLiteral(teacherDayHelper, false));
                for (Variable remainVariable: remainVariables) {
                    literals.add(new DefinitionLiteral(remainVariable, true));
                }
                
                scaffold.registerConstraint(new OrConstraint(literals));
            }
            
        }
        
    }
    
}

package hu.webarticum.aurora.plugins.defaultautoput;

import hu.webarticum.aurora.core.model.Block;

public class UserBlockFollowingDefinition {

    private final boolean ordered;
    
    private final Block block1;
    
    private final Block block2;
    
    private final String comment;
    
    
    public UserBlockFollowingDefinition(boolean ordered, Block block1, Block block2, String comment) {
        this.ordered = ordered;
        this.block1 = block1;
        this.block2 = block2;
        this.comment = comment;
    }


    public boolean isOrdered() {
        return ordered;
    }
    
    public Block getBlock1() {
        return block1;
    }

    public Block getBlock2() {
        return block2;
    }

    public String getComment() {
        return comment;
    }

}

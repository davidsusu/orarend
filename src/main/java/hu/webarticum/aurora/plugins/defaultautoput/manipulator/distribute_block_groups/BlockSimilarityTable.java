package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;

public class BlockSimilarityTable {

    protected Map<Block, BlockData> blockDataMap = new IdentityHashMap<Block, BlockData>();
    
    public void addSimilarGroup(SimilarGroup similarGroup) {
        BlockList blocks = similarGroup.getBlocks();
        for (Block block: blocks) {
            getBlockData(block).similarBlocks.addAll(blocks);
        }
        for (EqualGroup equalGroup: similarGroup) {
            addEqualGroup(equalGroup);
        }
    }
    
    public void addEqualGroup(EqualGroup equalGroup) {
        BlockList blocks = equalGroup.getBlocks();
        for (Block block: blocks) {
            getBlockData(block).equalBlocks.addAll(blocks);
        }
        for (IdenticalGroup identicalGroup: equalGroup) {
            addIdenticalGroup(identicalGroup);
        }
    }
    
    public void addIdenticalGroup(IdenticalGroup identicalGroup) {
        for (Block block: identicalGroup) {
            getBlockData(block).identicalBlocks.addAll(identicalGroup);
        }
    }

    public boolean areBlocksSimilar(Block block1, Block block2) {
        if (blockDataMap.containsKey(block1)) {
            return blockDataMap.get(block1).similarBlocks.contains(block2);
        } else {
            return false;
        }
    }

    public boolean areBlocksEqual(Block block1, Block block2) {
        if (blockDataMap.containsKey(block1)) {
            return blockDataMap.get(block1).equalBlocks.contains(block2);
        } else {
            return false;
        }
    }

    public boolean areBlocksIdentical(Block block1, Block block2) {
        if (blockDataMap.containsKey(block1)) {
            return blockDataMap.get(block1).identicalBlocks.contains(block2);
        } else {
            return false;
        }
    }
    
    protected BlockData getBlockData(Block block) {
        BlockData blockData;
        if (blockDataMap.containsKey(block)) {
            blockData = blockDataMap.get(block);
        } else {
            blockData = new BlockData();
            blockDataMap.put(block, blockData);
        }
        return blockData;
    }
    
    protected class BlockData {

        public final Set<Block> similarBlocks = new HashSet<Block>();

        public final Set<Block> equalBlocks = new HashSet<Block>();

        public final Set<Block> identicalBlocks = new HashSet<Block>();
        
    }
    
}

package hu.webarticum.aurora.plugins.defaultautoput.manipulator.eliminate_teacher_holes;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.aurora.plugins.defaultautoput.ScaffoldManipulator;
import hu.webarticum.jsatbuilder.builder.common.AllViability;
import hu.webarticum.jsatbuilder.builder.common.AnyHelper;
import hu.webarticum.jsatbuilder.builder.common.ConflictConstraint;
import hu.webarticum.jsatbuilder.builder.common.DefinitionLiteral;
import hu.webarticum.jsatbuilder.builder.common.OrConstraint;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;
import hu.webarticum.jsatbuilder.builder.core.Constraint;
import hu.webarticum.jsatbuilder.builder.core.Definition;
import hu.webarticum.jsatbuilder.builder.core.Variable;
import hu.webarticum.jsatbuilder.builder.core.Viability;

public class EliminateTeacherHolesScaffoldManipulator implements ScaffoldManipulator {

    public static final long NO_HOLE_MAX_PAD_TIME = Time.MINUTE * 25;
    public static final long NO_DOUBLE_HOLE_MAX_PAD_TIME = Time.MINUTE * 75;
    
    @Override
    public void manipulate(Scaffold scaffold) throws CancelledException, CollapseException {
        scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.eliminate_teacher_holes.eliminate_teacher_holes"));
        scaffold.out.setProgress(null);
        
        Set<Resource> noHoleTeachers = getNoHoleTeachers(scaffold);
        Set<Resource> noDoubleHoleTeachers = getNoDoubleHoleTeachers(scaffold);
        
        scaffold.out.tick();
        
        Map<Block, TreeMap<Time, TimingSet>> blockDayTimingSetMap = new HashMap<>();
        for (Block block: scaffold.blocks) {
            TimingSet timingSet = scaffold.getTimingSet(block);
            blockDayTimingSetMap.put(block, timingSet.split(Time.DAY));
        }
        
        scaffold.out.tick();

        Map<Resource, Map<Period, BlockList>> teacherPeriodBlocksMap = new HashMap<>();
        {
            scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.eliminate_teacher_holes.collect_structure"));
            scaffold.out.setProgress(0d);
            
            double progress = 0;
            double progressDiff = 1d / scaffold.blocks.size();
            
            for (Block block: scaffold.blocks) {
                Block.ActivityManager activityManager = block.getActivityManager();
                PeriodSet periods = activityManager.getPeriods();
                for (Period period: periods) {
                    for (Resource teacher: activityManager.getActivities(period).getResources(Resource.Type.PERSON)) {
                        Map<Period, BlockList> periodBlocksMap;
                        if (teacherPeriodBlocksMap.containsKey(teacher)) {
                            periodBlocksMap = teacherPeriodBlocksMap.get(teacher);
                        } else {
                            periodBlocksMap = new HashMap<>();
                            teacherPeriodBlocksMap.put(teacher, periodBlocksMap);
                        }
                        BlockList blocks;
                        if (periodBlocksMap.containsKey(period)) {
                            blocks = periodBlocksMap.get(period);
                        } else {
                            blocks = new BlockList();
                            periodBlocksMap.put(period, blocks);
                        }
                        blocks.add(block);
                    }
                }
                
                scaffold.out.setProgress(progress += progressDiff);
                scaffold.out.tick();
            }
        }
        
        {
            scaffold.out.setLabel(text("plugin.defaultautoput.manipulator.eliminate_teacher_holes.process_structure"));
            scaffold.out.setProgress(0d);
            
            double progress = 0;
            double progressDiff = 1d / scaffold.blocks.size();
            
            for (Map.Entry<Resource, Map<Period, BlockList>> entry: teacherPeriodBlocksMap.entrySet()) {
                Resource teacher = entry.getKey();
                long maximumTimeDiff = -1;
                
                if (noHoleTeachers.contains(teacher)) {
                    maximumTimeDiff = NO_HOLE_MAX_PAD_TIME;
                } else if (noDoubleHoleTeachers.contains(teacher)) {
                    maximumTimeDiff = NO_DOUBLE_HOLE_MAX_PAD_TIME;
                }
                
                if (maximumTimeDiff < 0) {
                    continue;
                }
                
                Map<Period, BlockList> periodBlocksMap = entry.getValue();
                
                TreeMap<Time, Set<Set<Block>>> dayBlockSetsMap = new TreeMap<>();
                for (Map.Entry<Period, BlockList> _entry: periodBlocksMap.entrySet()) {
                    BlockList blocks = _entry.getValue();
                    boolean hasNonFixed = false;
                    for (Block block : blocks) {
                        if (!scaffold.isFixed(block)) {
                            hasNonFixed = true;
                            break;
                        }
                    }
                    if (!hasNonFixed) {
                        continue;
                    }
                    
                    TreeMap<Time, Set<Block>> dayBlocksMap = new TreeMap<>();
                    for (Block block: blocks) {
                        for (Time day: blockDayTimingSetMap.get(block).keySet()) {
                            Set<Block> dayBlocks;
                            if (dayBlocksMap.containsKey(day)) {
                                dayBlocks = dayBlocksMap.get(day);
                            } else {
                                dayBlocks = new HashSet<>();
                                dayBlocksMap.put(day, dayBlocks);
                            }
                            dayBlocks.add(block);
                        }
                    }
                    for (Map.Entry<Time, Set<Block>> __entry: dayBlocksMap.entrySet()) {
                        Time day = __entry.getKey();
                        Set<Block> dayBlocks = __entry.getValue();
                        Set<Set<Block>> dayBlocksSet;
                        if (dayBlockSetsMap.containsKey(day)) {
                            dayBlocksSet = dayBlockSetsMap.get(day);
                        } else {
                            dayBlocksSet = new HashSet<>();
                            dayBlockSetsMap.put(day, dayBlocksSet);
                        }
                        dayBlocksSet.add(dayBlocks);
                    }
                }
                
                for (Map.Entry<Time, Set<Set<Block>>> _entry: dayBlockSetsMap.entrySet()) {
                    Time day = _entry.getKey();
                    Set<Set<Block>> dayBlocksSet = _entry.getValue();
                    for (Set<Block> blockSet: dayBlocksSet) {
                        handleDayBlocks(scaffold, blockDayTimingSetMap, day, blockSet, maximumTimeDiff);
                    }
                }

                scaffold.out.setProgress(progress += progressDiff);
                scaffold.out.tick();
            }
        }
        
        scaffold.out.log(text("plugin.defaultautoput.manipulator.eliminate_teacher_holes.finished"));
    }

    private void handleDayBlocks(
        Scaffold scaffold,
        Map<Block, TreeMap<Time, TimingSet>> blockDayTimingSetMap,
        Time day,
        Set<Block> blockSet,
        long maximumTimeDiff
    ) {
        NavigableMap<Time, Set<Variable>> variablesByEndTime = new TreeMap<>();
        NavigableMap<Time, Set<Variable>> variablesByStartTime = new TreeMap<>();
        Map<Set<Variable>, Definition> aggregateHelpers = new HashMap<>();
        
        for (Block block : blockSet) {
            long blockLength = block.getLength();
            TimingSet blockDayTimingSet = blockDayTimingSetMap.get(block).get(day);
            for (TimingSet.TimeEntry timeEntry : blockDayTimingSet) {
                Time startTime = timeEntry.getTime();
                Time endTime = startTime.getMoved(blockLength);
                Set<Variable> endingVariables = variablesByEndTime.computeIfAbsent(endTime, k -> new HashSet<>());
                Set<Variable> startingVariables = variablesByStartTime.computeIfAbsent(startTime, k -> new HashSet<>());
                Variable placementVariable = scaffold.blockTimeVariableMap.get(block).get(startTime);
                endingVariables.add(placementVariable);
                startingVariables.add(placementVariable);
            }
        }
        
        for (Map.Entry<Time, Set<Variable>> endEntry : variablesByEndTime.entrySet()) {
            Time endingTime = endEntry.getKey();
            Set<Variable> endingVariables = endEntry.getValue();
            Definition endingDefinition =
                aggregateHelpers.computeIfAbsent(endingVariables, k -> toAggregateDefinition(endingVariables))
            ;
            
            Time borderTime = endingTime.getMoved(maximumTimeDiff);

            Set<Variable> farVariables = new HashSet<>();
            NavigableMap<Time, Set<Variable>> farMap = variablesByStartTime.tailMap(borderTime, false);
            farMap.forEach((k, vs) -> farVariables.addAll(vs));
            if (farVariables.isEmpty()) {
                continue;
            }

            Definition farDefinition =
                aggregateHelpers.computeIfAbsent(farVariables, k -> toAggregateDefinition(farVariables))
            ;

            Set<Variable> nearVariables = new HashSet<>();
            variablesByStartTime.subMap(endingTime, true, borderTime, true).forEach((k, vs) -> nearVariables.addAll(vs));

            if (nearVariables.isEmpty()) {
                Constraint conflictConstraint = new ConflictConstraint(endingDefinition, farDefinition);
                scaffold.registerConstraint(conflictConstraint);
                return;
            }
            
            Definition nearDefinition =
                aggregateHelpers.computeIfAbsent(nearVariables, k -> toAggregateDefinition(nearVariables))
            ;

            Constraint constraint = new OrConstraint(
                new DefinitionLiteral(endingDefinition, false),
                new DefinitionLiteral(farDefinition, false),
                new DefinitionLiteral(nearDefinition, true)
            ) {
                private final Viability viability = new AllViability(endingDefinition, farDefinition, nearDefinition);
                
                @Override
                public Viability getViability() {
                    return viability;
                }

                @Override
                public void dependencyRemoved(Definition definition) throws CollapseException {
                    System.out.println("REM: " + definition);
                    try {
                    super.dependencyRemoved(nearDefinition);
                    } catch (CollapseException e) {
                        System.out.println("EXC: " + e.getMessage());
                        throw e;
                    }
                }
            };
            
            scaffold.registerConstraint(constraint);
        }
    }
    
    private Definition toAggregateDefinition(Collection<Variable> variables) {
        if (variables.size() == 1) {
            return variables.iterator().next();
        } else {
            return new AnyHelper(variables);
        }
    }

    private Set<Resource> getNoHoleTeachers(Scaffold scaffold) {
        Set<Resource> result = new HashSet<>();
        
        Value extraData = scaffold.document.getExtraData();
        
        Value.ValueSet noHoleTeachersData = extraData.getAccess("autoput.school.setup.no_hole_teachers").get().getAsSet();
        for (Value value: noHoleTeachersData) {
            Resource teacher = value.getAsResource();
            result.add(teacher);
        }
        
        return result;
    }

    private Set<Resource> getNoDoubleHoleTeachers(Scaffold scaffold) {
        Set<Resource> result = new HashSet<>();
        
        Value extraData = scaffold.document.getExtraData();

        Value.ValueSet noDoubleHoleTeachersData = extraData.getAccess("autoput.school.setup.no_double_hole_teachers").get().getAsSet();
        for (Value value: noDoubleHoleTeachersData) {
            Resource teacher = value.getAsResource();
            result.add(teacher);
        }
        
        return result;
    }
    
}

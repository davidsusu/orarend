package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.jsatbuilder.builder.common.AnyHelper;
import hu.webarticum.jsatbuilder.builder.core.Helper;
import hu.webarticum.jsatbuilder.builder.core.Variable;

public class BlockDayTable {
    
    
    public final Map<Block, TreeMap<Time, BlockDayItem>> blockDayItemMap = new IdentityHashMap<Block, TreeMap<Time, BlockDayItem>>();
    
    public final TreeMap<Time, Map<Block, BlockDayItem>> dayBlockItemMap = new TreeMap<Time, Map<Block, BlockDayItem>>();
    
    
    private final Scaffold scaffold;
    
    
    public BlockDayTable(Scaffold scaffold) {
        this.scaffold = scaffold;
    }
    
    
    public void add(Block block, TreeSet<Time> days) {
        if (blockDayItemMap.containsKey(block)) {
            return;
        }
        TreeMap<Time, BlockDayItem> dayItemMap = new TreeMap<Time, BlockDayItem>();
        for (Time day: days) {
            BlockDayItem item = new BlockDayItem(block, day);
            dayItemMap.put(day, item);
            Map<Block, BlockDayItem> blockVariableMap;
            if (dayBlockItemMap.containsKey(day)) {
                blockVariableMap = dayBlockItemMap.get(day);
            } else {
                blockVariableMap = new IdentityHashMap<Block, BlockDayItem>();
                dayBlockItemMap.put(day, blockVariableMap);
            }
            blockVariableMap.put(block, item);
        }
        blockDayItemMap.put(block, dayItemMap);
    }
    
    public void merge(BlockDayTable other) {
        for (Map.Entry<Block, TreeMap<Time, BlockDayItem>> entry: other.blockDayItemMap.entrySet()) {
            Block block = entry.getKey();
            if (blockDayItemMap.containsKey(block)) {
                continue;
            }
            TreeMap<Time, BlockDayItem> dayItemMap = entry.getValue();
            TreeMap<Time, BlockDayItem> copyOfDayItemMap = new TreeMap<Time, BlockDayItem>(dayItemMap);
            blockDayItemMap.put(block, copyOfDayItemMap);
            for (Map.Entry<Time, BlockDayItem>_entry: dayItemMap.entrySet()) {
                Time day = _entry.getKey();
                BlockDayItem item = _entry.getValue();
                Map<Block, BlockDayItem> blockItemMap;
                if (dayBlockItemMap.containsKey(day)) {
                    blockItemMap = dayBlockItemMap.get(day);
                } else {
                    blockItemMap = new IdentityHashMap<Block, BlockDayItem>();
                    dayBlockItemMap.put(day, blockItemMap);
                }
                blockItemMap.put(block, item);
            }
        }
    }
    
    public BlockDayItem getBlockDayItem(Block block, Time day) {
        if (blockDayItemMap.containsKey(block)) {
            return blockDayItemMap.get(block).get(day);
        } else {
            return null;
        }
    }
    
    public List<Pair<BlockDayItem, BlockDayItem>> getConflicts() {
        List<Pair<BlockDayItem, BlockDayItem>> conflicts = new ArrayList<Pair<BlockDayItem, BlockDayItem>>();
        for (Map.Entry<Time, Map<Block, BlockDayItem>> entry: dayBlockItemMap.entrySet()) {
            List<BlockDayItem> dayItems = new ArrayList<BlockDayItem>(entry.getValue().values());
            int dayItemCount = dayItems.size();
            for (int i = 0; i < dayItemCount-1; i++) {
                BlockDayItem item1 = dayItems.get(i);
                for (int j = i + 1; j < dayItemCount; j++) {
                    BlockDayItem item2 = dayItems.get(j);
                    conflicts.add(new Pair<BlockDayItem, BlockDayItem>(item1, item2));
                }
            }
        }
        return conflicts;
    }
    
    public class BlockDayItem {

        private final Block block;
        private final Time dayTime;

        private Variable variable = null;
        private Helper helper = null;
        
        BlockDayItem(Block block, Time dayTime) {
            this.block = block;
            this.dayTime = dayTime;
        }

        public Block getBlock() {
            return block;
        }

        public Time getDayTime() {
            return dayTime;
        }

        public long getDayIndex() {
            return dayTime.getPart(Time.PART.FULLDAYS);
        }

        public Variable getVariable() {
            if (variable == null) {
                variable = new Variable(block + " at day " + dayTime.toReadableString());
            }
            return variable;
        }

        public Helper getHelper() {
            if (helper == null) {
                if (scaffold.blockDayHelperMap.containsKey(block)) {
                    helper = scaffold.blockDayHelperMap.get(block).get(getDayIndex());
                }
                if (helper == null) {
                    helper = new AnyHelper(getVariable()).setLabel("Fallback for " + block + " at day " + dayTime.toReadableString());
                }
            }
            return helper;
        }
        
    }
    
}

package hu.webarticum.aurora.plugins.defaultautoput.manipulator.distribute_block_groups;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.common.CollectionUtil;
import hu.webarticum.aurora.app.util.common.Triplet;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.aurora.plugins.defaultautoput.UserBlockFollowingDefinition;

public class BlockGrouper {
    
    private final Scaffold scaffold;
    
    private final Set<Block> normalFollowingBlocks = new HashSet<Block>();
    
    private final Set<Block> similarFollowingBlocks = new HashSet<Block>();
    
    private final Set<Tag> teacherDistributorTags =  new HashSet<Tag>();
    
    
    public BlockGrouper(Scaffold scaffold) {
        this.scaffold = scaffold;
        for (UserBlockFollowingDefinition following : scaffold.userBlockFollowings) {
            Block block1 = following.getBlock1();
            Block block2 = following.getBlock2();
            normalFollowingBlocks.add(block1);
            if (areBlocksSimilar(block1, block2)) {
                similarFollowingBlocks.add(block1);
            } else {
                normalFollowingBlocks.add(block1);
            }
        }
        Value.ValueSet teacherDistributorTagsData = scaffold.document.getExtraData().getAccess("autoput.school.setup.teacher_distributor_tags").get().getAsSet();
        for (Value tagValue: teacherDistributorTagsData) {
            Tag tag = tagValue.getAsTag();
            teacherDistributorTags.add(tag);
        }
    }
    
    public List<SimilarGroup> createStructure() throws CancelledException {
        List<EqualGroup> equalGroups = collectEqualGroups();

        Set<SimilarGroup> similarGroups = new LinkedHashSet<SimilarGroup>();
        similarGroups.addAll(collectClassSubjectGroups(equalGroups));
        similarGroups.addAll(collectTeacherTagGroups(equalGroups));

        Set<EqualGroup> handledEqualGroups = new HashSet<EqualGroup>();
        for (SimilarGroup currentEqualGroups: similarGroups) {
            for (EqualGroup equalGroup: currentEqualGroups) {
                if (handledEqualGroups.contains(equalGroup)) {
                    equalGroup.increaseReferenceCount();
                } else {
                    handledEqualGroups.add(equalGroup);
                }
            }
            scaffold.out.tick();
        }
        
        return new ArrayList<SimilarGroup>(similarGroups);
    }
    
    private Collection<SimilarGroup> collectClassSubjectGroups(List<EqualGroup> equalGroups) throws CancelledException {
        Map<Triplet<Period, Resource, Tag>, SimilarGroup> periodClassSubjectEqualGroupsMap = new HashMap<Triplet<Period, Resource, Tag>, SimilarGroup>();
        
        for (EqualGroup equalGroup: equalGroups) {
            Block block = equalGroup.getSampleBlock();
            
            Set<Triplet<Period, Resource, Tag>> periodClassSubjectTriplets = new HashSet<Triplet<Period, Resource, Tag>>();
            Block.ActivityManager activityManager = block.getActivityManager();
            for (Period period: activityManager.getPeriods()) {
                for (Activity activity: activityManager.getActivities(period)) {
                    List<Resource> classes = activity.getResourceManager().getResources(Resource.Type.CLASS);
                    
                    // XXX
                    if (activity.getTagManager().hasType(Tag.Type.LANGUAGE)) {
                        continue;
                    }
                    
                    Set<Tag> subjects = activity.getTagManager().getTags(Tag.Type.SUBJECT);
                    for (Resource _class: classes) {
                        for (Tag subject: subjects) {
                            periodClassSubjectTriplets.add(new Triplet<Period, Resource, Tag>(period, _class, subject));
                        }
                    }
                }
            }
            for (Triplet<Period, Resource, Tag> triplet: periodClassSubjectTriplets) {
                SimilarGroup similarGroup;
                if (periodClassSubjectEqualGroupsMap.containsKey(triplet)) {
                    similarGroup = periodClassSubjectEqualGroupsMap.get(triplet);
                } else {
                    similarGroup = new SimilarGroup();
                    periodClassSubjectEqualGroupsMap.put(triplet, similarGroup);
                }
                similarGroup.add(equalGroup);
            }
            
            scaffold.out.tick();
        }
        
        return periodClassSubjectEqualGroupsMap.values();
    }
    
    private Collection<SimilarGroup> collectTeacherTagGroups(List<EqualGroup> equalGroups) {
        Map<Triplet<Period, Resource, Tag>, SimilarGroup> groupMap = new HashMap<Triplet<Period, Resource, Tag>, SimilarGroup>();
        
        for (Tag tag : teacherDistributorTags) {
            for (EqualGroup equalGroup : equalGroups) {
                Block block = equalGroup.getSampleBlock();
                Block.ActivityManager activityManager = block.getActivityManager();
                for (Period period: activityManager.getPeriods()) {
                    for (Activity activity: activityManager.getActivities(period)) {
                        if (activity.getTagManager().has(tag)) {
                            for (Resource teacher: activity.getResourceManager().getResources(Resource.Type.PERSON)) {
                                Triplet<Period, Resource, Tag> key = new Triplet<Period, Resource, Tag>(period, teacher, tag);
                                SimilarGroup similarGroup;
                                if (groupMap.containsKey(key)) {
                                    similarGroup = groupMap.get(key);
                                } else {
                                    similarGroup = new SimilarGroup();
                                    groupMap.put(key, similarGroup);
                                }
                                similarGroup.add(equalGroup);
                            }
                        }
                    }
                }
            }
        }

        return groupMap.values();
    }
    
    private List<EqualGroup> collectEqualGroups() throws CancelledException {
        List<EqualGroup> equalGroups = new ArrayList<EqualGroup>();

        for (Block block: scaffold.blocks) {
            if (similarFollowingBlocks.contains(block)) {
                continue;
            }
            boolean foundEqual = false;
            for (EqualGroup equalGroup: equalGroups) {
                if (areBlocksEqual(block, equalGroup.getSampleBlock(), true)) {
                    boolean foundIdentical = false;
                    for (IdenticalGroup identicalGroup: equalGroup) {
                        if (areBlockTimingSetsEqual(block, identicalGroup.getSampleBlock())) {
                            identicalGroup.add(block);
                            foundIdentical = true;
                        }
                    }
                    if (!foundIdentical) {
                        IdenticalGroup identicalGroup = new IdenticalGroup(scaffold);
                        identicalGroup.add(block);
                        equalGroup.add(identicalGroup);
                    }
                    foundEqual = true;
                }
            }
            if (!foundEqual) {
                IdenticalGroup identicalGroup = new IdenticalGroup(scaffold);
                identicalGroup.add(block);
                EqualGroup equalGroup = new EqualGroup();
                equalGroup.add(identicalGroup);
                equalGroups.add(equalGroup);
            }
            
            scaffold.out.tick();
        }
        
        return equalGroups;
    }

    private boolean areBlocksSimilar(Block block1, Block block2) {
        Block.ActivityManager activityManager1 = block1.getActivityManager();
        Block.ActivityManager activityManager2 = block2.getActivityManager();
        
        Set<Resource> classes1 = activityManager1.getActivities().getResources(Resource.Type.CLASS);
        Set<Resource> commonClasses = activityManager2.getActivities().getResources(Resource.Type.CLASS);
        commonClasses.retainAll(classes1);
        
        Set<Resource> teachers1 = activityManager1.getActivities().getResources(Resource.Type.PERSON);
        Set<Resource> commonTeachers = activityManager2.getActivities().getResources(Resource.Type.PERSON);
        commonTeachers.retainAll(teachers1);
        
        if (commonClasses.isEmpty() && commonTeachers.isEmpty()) {
            return false;
        }
        
        PeriodSet periods1 = activityManager1.getPeriods();

        for (Period period : periods1) {
            ActivityList periodActivities1 = activityManager1.getActivities(period);
            ActivityList periodActivities2 = activityManager2.getActivities(period);
            
            for (Resource clazz : commonClasses) {
                Set<Tag> subjects1 = periodActivities1.filter(new ActivityFilter.HasResource(clazz)).getTags(Tag.Type.SUBJECT);
                Set<Tag> commonSubjects = periodActivities2.filter(new ActivityFilter.HasResource(clazz)).getTags(Tag.Type.SUBJECT);
                commonSubjects.retainAll(subjects1);
                if (!commonSubjects.isEmpty()) {
                    return true;
                }
            }
            
            if (!teacherDistributorTags.isEmpty()) {
                for (Resource teacher : commonTeachers) {
                    Set<Tag> tags1 = periodActivities1.filter(new ActivityFilter.HasResource(teacher)).getTags();
                    Set<Tag> commonDistributors = periodActivities2.filter(new ActivityFilter.HasResource(teacher)).getTags();
                    commonDistributors.retainAll(teacherDistributorTags);
                    commonDistributors.retainAll(tags1);
                    if (!commonDistributors.isEmpty()) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    private boolean areBlockTimingSetsEqual(Block block1, Block block2) {
        TimingSet timingSet1 = scaffold.getTimingSet(block1);
        TimingSet timingSet2 = scaffold.getTimingSet(block2);
        return timingSet1.getTimes().equals(timingSet2.getTimes());
    }
    
    private boolean areBlocksEqual(Block block1, Block block2, boolean checkTag) {
        // FIXME
        if (checkTag && (isBlockDistributedByTeacherTag(block1) || isBlockDistributedByTeacherTag(block2))) {
            return false;
        }
        
        if (normalFollowingBlocks.contains(block1) || normalFollowingBlocks.contains(block2)) {
            return false;
        }

        if (!scaffold.areInConflict(block1, block2)) {
            return false;
        }
        
        Block.ActivityManager activityManager1 = block1.getActivityManager();
        Block.ActivityManager activityManager2 = block2.getActivityManager();
        PeriodSet periods1 = activityManager1.getPeriods();
        PeriodSet periods2 = activityManager2.getPeriods();
        
        if (!periods1.equals(periods2)) {
            return false;
        }

        for (Period period: periods1) {
            if (!areActivityListsEqual(activityManager1.getActivities(period), activityManager2.getActivities(period))) {
                return false;
            }
        }
        return true;
    }
    
    private boolean areActivityListsEqual(ActivityList activities1, ActivityList activities2) {
        ActivityList activities2Copy = activities2.copy();
        for (Activity activity1: activities1) {
            boolean found = false;
            Iterator<Activity> activities2Iterator = activities2Copy.iterator();
            while (activities2Iterator.hasNext()) {
                Activity activity2 = activities2Iterator.next();
                if (areActivitiesEqual(activity1, activity2)) {
                    activities2Iterator.remove();
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }
        }
        return activities2Copy.isEmpty();
    }
    
    private boolean areActivitiesEqual(Activity activity1, Activity activity2) {
        if (!CollectionUtil.containSame(activity1.getResourceManager().getResourceSubsets(), activity2.getResourceManager().getResourceSubsets())) {
            return false;
        }
        if (!activity1.getTagManager().getTags(Tag.Type.SUBJECT).equals(activity2.getTagManager().getTags(Tag.Type.SUBJECT))) {
            return false;
        }
        return true;
    }
    
    private boolean isBlockDistributedByTeacherTag(Block block) {
        Set<Tag> tags = new BlockList(block).getActivities().getTags();
        tags.retainAll(teacherDistributorTags);
        return !tags.isEmpty();
    }
    
}

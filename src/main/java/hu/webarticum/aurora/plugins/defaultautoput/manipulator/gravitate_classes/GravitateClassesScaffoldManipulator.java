package hu.webarticum.aurora.plugins.defaultautoput.manipulator.gravitate_classes;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.BlockUtil;
import hu.webarticum.aurora.app.util.TimingSetDisjoiner;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultautoput.Scaffold;
import hu.webarticum.aurora.plugins.defaultautoput.ScaffoldManipulator;
import hu.webarticum.jsatbuilder.builder.common.AnyHelper;
import hu.webarticum.jsatbuilder.builder.common.BoundConstraint;
import hu.webarticum.jsatbuilder.builder.core.CollapseException;
import hu.webarticum.jsatbuilder.builder.core.Helper;
import hu.webarticum.jsatbuilder.builder.core.Variable;

// TODO preferredTimes in last row

public class GravitateClassesScaffoldManipulator implements ScaffoldManipulator {

    @Override
    public void manipulate(Scaffold scaffold) throws CancelledException, CollapseException {
        for (Map.Entry<Resource, Map<Period, Set<Block>>> classEntry: collectBlockMap(scaffold).entrySet()) {
            Resource clazz = classEntry.getKey();
            Set<Set<Block>> blockSets = new HashSet<Set<Block>>(classEntry.getValue().values());
            for (Set<Block> blockSet: blockSets) {
                processBlocks(scaffold, clazz, new BlockList(blockSet));
            }
        }
    }
    
    private Map<Resource, Map<Period, Set<Block>>> collectBlockMap(Scaffold scaffold) {
        Map<Resource, Map<Period, Set<Block>>> blockMap = new HashMap<>();
        
        for (Block block: scaffold.blocks) {
            for (Resource _class: block.getActivityManager().getActivities().getResources(Resource.Type.CLASS)) {
                Map<Period, Set<Block>> blockSetsByPeriod;
                if (blockMap.containsKey(_class)) {
                    blockSetsByPeriod = blockMap.get(_class);
                } else {
                    blockSetsByPeriod = new HashMap<Period, Set<Block>>();
                    blockMap.put(_class, blockSetsByPeriod);
                }
                for (Period period: block.getActivityManager().getResourcePeriods(_class)) {
                    Set<Block> blockSet;
                    if (blockSetsByPeriod.containsKey(period)) {
                        blockSet = blockSetsByPeriod.get(period);
                    } else {
                        blockSet = new HashSet<Block>();
                        blockSetsByPeriod.put(period, blockSet);
                    }
                    blockSet.add(block);
                }
            }
        }

        return blockMap;
    }
    
    private void processBlocks(Scaffold scaffold, Resource clazz, BlockList blocks) throws CancelledException, CollapseException {
        BlockList movingBlocks = new BlockList();
        BlockList fixedBlocks = new BlockList();
        for (Block block: blocks) {
            if (scaffold.isFixed(block)) {
                fixedBlocks.add(block);
            } else {
                movingBlocks.add(block);
            }
        }
        
        scaffold.out.tick();

        int requiredFreeSlotCount = BlockUtil.getShrinkedCount(blocks, clazz) - fixedBlocks.size();
        long blockLength = BlockUtil.detectMedianBlockLength(blocks);
        
        TimingSet mergedTimingSet = new TimingSet();
        for (Block block: blocks) {
            mergedTimingSet.addAll(scaffold.getTimingSet(block));
        }
        TimingSetDisjoiner disjoiner = new TimingSetDisjoiner(mergedTimingSet);
        TimingSet disjointTimingSet =  disjoiner.calculateMaximalDisjoint(blockLength);

        TreeMap<Time, TimingSet> daySplitMap = disjointTimingSet.split(Time.DAY);
        List<Iterator<Time>> dayIterators = new ArrayList<Iterator<Time>>();
        for (Map.Entry<Time, TimingSet> entry: daySplitMap.entrySet()) {
            Iterator<Time> iterator = entry.getValue().getTimes().iterator();
            if (iterator.hasNext()) {
                dayIterators.add(iterator);
            }
        }

        scaffold.out.tick();
        
        int counter = 0;
        
        List<Time> lastRowTimes = new ArrayList<Time>();
        List<Time> freeLastRowTimes = new ArrayList<Time>();
        while (!dayIterators.isEmpty()) {
            lastRowTimes.clear();
            freeLastRowTimes.clear();
            List<Iterator<Time>> removableIterators = new ArrayList<Iterator<Time>>();
            for (Iterator<Time> iterator: dayIterators) {
                Time time = iterator.next();
                if (!iterator.hasNext()) {
                    removableIterators.add(iterator);
                }
                lastRowTimes.add(time);
                if (isFree(scaffold, fixedBlocks, time, blockLength)) {
                    freeLastRowTimes.add(time);
                    counter++;
                }
            }
            dayIterators.removeAll(removableIterators);
            if (counter >= requiredFreeSlotCount) {
                break;
            }
        }
        
        if (counter < requiredFreeSlotCount) {
            throw new CollapseException(String.format(
                text("plugin.defaultautoput.manipulator.gravitate_classes.not_enough_space"),
                clazz.getLabel()
            ));
        }

        scaffold.out.tick();
        
        banRemainingTimes(scaffold, lastRowTimes, movingBlocks);

        scaffold.out.tick();
        
        if (counter > requiredFreeSlotCount) {
            int bannedCount = counter - requiredFreeSlotCount;
            int allowedCount = freeLastRowTimes.size() - bannedCount;
            if (movingBlocks.size() == requiredFreeSlotCount) {
                limitRowSimple(scaffold, clazz, freeLastRowTimes, movingBlocks, allowedCount);
            } else {
                limitRowComplex(scaffold, clazz, freeLastRowTimes, movingBlocks, allowedCount);
            }
        }
        
        scaffold.out.tick();
    }
    
    private boolean isFree(Scaffold scaffold, BlockList fixedBlocks, Time time, long blockLength) {
        long start = time.getSeconds();
        long end = start + blockLength;
        for (Block fixedBlock: fixedBlocks) {
            Time fixedTime = scaffold.getTimingSet(fixedBlock).first().getTime();
            long fixedStart = fixedTime.getSeconds();
            long fixedEnd = fixedStart + fixedBlock.getLength();
            if (fixedStart < end && fixedEnd > start) {
                return false;
            }
        }
        return true;
    }

    private void banRemainingTimes(Scaffold scaffold, List<Time> lastRowTimes, BlockList blocks) throws CollapseException {
        for (Time lastRowTime: lastRowTimes) {
            Time afterTime = new Time(lastRowTime.getSeconds() + 1);
            Time nextDay = new Time(lastRowTime.getDateOnly().getSeconds() + Time.DAY);
            for (Block block: blocks) {
                SortedMap<Time, Variable> dayTimes = scaffold.blockTimeVariableMap.get(block);
                List<Time> timesToBan = new ArrayList<Time>(dayTimes.subMap(afterTime, nextDay).keySet());
                for (Time timeToBan : timesToBan) {
                    dayTimes.get(timeToBan).remove();
                    dayTimes.remove(timeToBan);
                }
            }
        }
        
    }
    
    private void limitRowSimple(
        Scaffold scaffold, Resource clazz, List<Time> rowTimes, BlockList blocks, int count
    ) {
        List<Variable> affectedVariables = new ArrayList<Variable>();
        for (Time time: rowTimes) {
            for (Block block: blocks) {
                Variable variable = scaffold.blockTimeVariableMap.get(block).get(time);
                if (variable != null) {
                    affectedVariables.add(variable);
                }
            }
        }
        
        scaffold.registerConstraint(new BoundConstraint(count, affectedVariables).setLabel(
            String.format("Last row of gravitation for class: %s", clazz)
        ));
    }

    private void limitRowComplex(
        Scaffold scaffold, Resource clazz, List<Time> rowTimes, BlockList blocks, int count
    ) {
        List<Helper> helpers = new ArrayList<Helper>();
        for (Time time: rowTimes) {
            List<Variable> variables = new ArrayList<Variable>();
            for (Block block: blocks) {
                Variable variable = scaffold.blockTimeVariableMap.get(block).get(time);
                if (variable != null) {
                    variables.add(variable);
                }
            }
            if (!variables.isEmpty()) {
                helpers.add(new AnyHelper(variables));
            }
        }
        
        scaffold.registerConstraint(new BoundConstraint(count, helpers).setLabel(
            String.format(
                "Complex last row of gravitation for class: %s",
                clazz
            )
        ));
    }
    
}

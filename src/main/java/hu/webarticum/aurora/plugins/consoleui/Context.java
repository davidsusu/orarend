package hu.webarticum.aurora.plugins.consoleui;

import java.util.List;

public interface Context {
    
    public String getLabel();

    public Context executeCommand(String command, List<String> arguments);

    public boolean leave();
    
    public String getValue(String name);
    
    public Object getContextObject();
    
}

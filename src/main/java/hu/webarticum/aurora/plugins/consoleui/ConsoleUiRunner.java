package hu.webarticum.aurora.plugins.consoleui;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.util.FileUtil;
import hu.webarticum.aurora.core.io.DocumentIo;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.consoleui.context.DefaultContext;
import hu.webarticum.aurora.plugins.consoleui.context.DocumentContext;
import hu.webarticum.aurora.plugins.consoleui.util.ArgumentsParser;

public class ConsoleUiRunner implements CommonExtensions.Runner {

    LinkedList<Context> contextStack = new LinkedList<Context>();
    
    Map<String, String> variables = new HashMap<String, String>();
    
    @Override
    public Type getType() {
        return Type.UI;
    }
    
    @Override
    public boolean isApplicable() {
        return true;
    }
    
    @Override
    public void run() {
        contextStack.clear();
        contextStack.add(new DefaultContext());

        String inputFilePath = Application.instance().getArgumentMap().get("input-file");
        if (inputFilePath!=null && !inputFilePath.isEmpty()) {
            do {
                File file = new File(inputFilePath);
                
                if (!file.exists()) {
                    System.out.println("File not found: "+inputFilePath);
                    break;
                }
                
                String extension = FileUtil.getExtension(file);
                if (extension.isEmpty()) {
                    extension = Info.DEFAULT_FILE_EXTENSION;
                }
                
                DocumentIo documentIo = Application.instance().getDocumentIoQueue().getByExtension(extension);
                if (documentIo==null) {
                    System.out.println("Unsupported format");
                    break;
                }
                
                Document document = null;
                
                try {
                    document = documentIo.load(file);
                } catch (Exception e) {
                    System.out.println("Load error");
                    break;
                }
                
                contextStack.add(new DocumentContext(document, file));
            } while (false);
        }
        
        while (true) {
            Context currentContext = contextStack.getLast();
            StringBuilder promptBuilder = new StringBuilder();
            for (Context context: contextStack) {
                promptBuilder.append(" :: "+context.getLabel());
            }
            String prompt = promptBuilder.toString();
            int promptLength = prompt.length();
            if (promptLength>25) {
                prompt = " ..."+prompt.substring(promptLength-21);
            }
            prompt = "(O)"+prompt+" & ";
            System.out.print(prompt);
            String fullCommand = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try {
                fullCommand = reader.readLine();
            } catch (IOException e) {
                System.out.println("Read error");
                break;
            }
            fullCommand = (fullCommand == null) ? "" : fullCommand.trim();
            if (fullCommand.isEmpty()) {
                continue;
            }
            if (fullCommand.charAt(0)==':') {
                execScript(fullCommand.substring(1));
                continue;
            }
            int spacePos = fullCommand.indexOf(' ');
            String command;
            String argumentsStr;
            if (spacePos>=0) {
                command = fullCommand.substring(0, spacePos);
                argumentsStr = fullCommand.substring(spacePos).trim();
            } else {
                command = fullCommand;
                argumentsStr = "";
            }
            ArgumentsParser argumentsParser = new ArgumentsParser(argumentsStr, currentContext, variables);
            List<String> arguments = new ArrayList<String>();
            for (String argument: argumentsParser) {
                arguments.add(argument);
            }
            if (command.equals("leave") || command.equals("back")) {
                if (currentContext.leave()) {
                    contextStack.removeLast();
                    if (contextStack.isEmpty()) {
                        break;
                    }
                }
            } else if (command.equals("quit") || command.equals("exit")) {
                break;
            } else if (command.equals("set")) {
                if (arguments.size()>1) {
                    variables.put(arguments.get(0), arguments.get(1));
                }
            } else if (command.equals("unset")) {
                if (!arguments.isEmpty()) {
                    variables.remove(arguments.get(0));
                }
            } else if (command.equals("echo")) {
                for (String argument: arguments) {
                    System.out.println(argument);
                }
            } else {
                Context subContext = currentContext.executeCommand(command, arguments);
                if (subContext!=null) {
                    contextStack.add(subContext);
                }
            }
        }
    }

    protected List<String> parseArguments(String argumentsStr) {
        return new ArrayList<String>();
    }
    
    protected void execScript(String script) {
        // FIXME/TODO
        /*
        Context currentContext = contextStack.getLast();
        Object contextObject = currentContext.getContextObject();
        sun.org.mozilla.javascript.Context scriptContext = sun.org.mozilla.javascript.Context.enter();
        Scriptable scope = scriptContext.initStandardObjects();
        Object contextObjectWrapper = sun.org.mozilla.javascript.Context.javaToJS(contextObject, scope);
        ScriptableObject.putProperty(scope, "current", contextObjectWrapper);
        try {
            Object result = scriptContext.evaluateString(scope, script, "<jsCommand>", 1, null);
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("Futtatási hiba:");
            System.out.println(e.getMessage());
        }*/
    }

}

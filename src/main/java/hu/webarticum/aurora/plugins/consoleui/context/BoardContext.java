package hu.webarticum.aurora.plugins.consoleui.context;

import static hu.webarticum.aurora.app.Shortcut.extensions;

import java.util.ArrayList;
import java.util.List;

import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.autoput.AutoPutter;
import hu.webarticum.aurora.app.util.autoput.AutoPutterAspectStructure;
import hu.webarticum.aurora.app.util.autoput.AutoPutterFactory;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.consoleui.Context;

public class BoardContext implements Context {

    protected final DocumentContext documentContext;

    protected final Board board;
    
    public BoardContext(DocumentContext documentContext, Board board) {
        this.documentContext = documentContext;
        this.board = board;
    }
    
    @Override
    public String getLabel() {
        return board.getLabel();
    }

    @Override
    public Context executeCommand(String command, List<String> arguments) {
        // FIXME
        Document document = documentContext.getDocument();
        if (command.equals("autoput")) {
            List<AutoPutterFactory> autoPutterFactoryList = new ArrayList<AutoPutterFactory>();
            for (CommonExtensions.Algorithm algorithm: extensions(CommonExtensions.Algorithm.class)) {
                autoPutterFactoryList.add(algorithm.getAutoPutterFactory());
            }
            if (arguments.isEmpty()) {
                System.out.println();
                System.out.println("----------------------");
                System.out.println("Válasszon algoritmust:");
                System.out.println("----------------------");
                System.out.println();
                for (AutoPutterFactory autoPutterFactory: autoPutterFactoryList) {
                    System.out.println(autoPutterFactory.getName()+" ("+autoPutterFactory.getLabel()+")");
                }
                System.out.println();
                return null;
            }
            
            String autoPutterName = arguments.get(0);

            AutoPutterFactory factory = null;
            for (AutoPutterFactory _autoPutterFactory: autoPutterFactoryList) {
                if (_autoPutterFactory.getName().equals(autoPutterName)) {
                    factory = _autoPutterFactory;
                    break;
                }
            }
            
            if (factory==null) {
                System.out.println("Ismeretlen algoritmus: "+autoPutterName);
                return null;
            }
            
            BlockList blocks = document.getBlockStore().getAll();
            AutoPutterAspectStructure ascpectStructure =  factory.collectAspectStructure(document, blocks, board);
            AutoPutter autoPutter = factory.createAutoPutter(document, blocks, board, ascpectStructure.getAllAspects());
            
            // FIXME/TODO: cancel: create other thread!
            
            System.out.println("Autoput...");
            autoPutter.run();
            autoPutter.setOutput(new TextAutoPutOutput());
            System.out.println("Finished...");
            
            return null;
        } else {
            return null;
        }
    }

    @Override
    public boolean leave() {
        // TODO
        return true;
    }

    @Override
    public String getValue(String name) {
        if (name.equals("label")) {
            return board.getLabel();
        } else {
            return "";
        }
    }

    @Override
    public Object getContextObject() {
        return board;
    }

    public Board getBoard() {
        return board;
    }
    
    protected class TextAutoPutOutput implements AutoPutter.AutoPutOutput {
        
        protected String label = "";
        
        private volatile long startTime = -1;
        
        @Override
        public void tick() throws CancelledException {
            // TODO
        }
        
        @Override
        public void setProgress(Double progress) {
            // TODO
        }
        
        @Override
        public void setLabel(String label) {
            System.out.println(" ### "+label+" ###");
            this.label = label;
        }
        
        @Override
        public void log(String line) {
            System.out.println(label+": "+line);
        }

        @Override
        public void trackTime() {
            this.startTime = System.currentTimeMillis();
        }

        @Override
        public void logTime(String comment) {
            if (startTime >= 0) {
                long currentTime = System.currentTimeMillis();
                long elapsedTime = currentTime - startTime;
                log(String.format(" %.3f | %s", elapsedTime / 1000d, comment));
            }
        }
        
        @Override
        public void finish(boolean success) {
            // TODO
        }
        
    }

}

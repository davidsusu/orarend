package hu.webarticum.aurora.plugins.consoleui.context;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.core.io.DocumentIo;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.consoleui.Context;

public class DocumentContext implements Context {
    
    protected final Document document;

    protected File associatedFile;
    
    public DocumentContext(Document document, File associatedFile) {
        this.document = document;
        this.associatedFile = associatedFile;
    }

    @Override
    public String getLabel() {
        return document.getLabel();
    }

    @Override
    public Context executeCommand(String command, List<String> arguments) {
        Document.BoardStore boardStore = document.getBoardStore();
        if (command.equals("create")) {
            if (arguments.isEmpty()) {
                System.out.println("Adja meg az objektum típusát!");
                return null;
            }
            String type = arguments.get(0);
            String name = null;
            if (arguments.size()>1) {
                name = arguments.get(1);
            }
            if (type.equals("board")) {
                if (name==null) {
                    name = "Új tábla";
                }
                Board board = new Board(name);
                boardStore.register(board);
                return new BoardContext(this, board);
            } else {
                System.out.println("Ismeretlen objektumtípus: "+type);
                return null;
            }
        } else if (command.equals("use")) {
            if (arguments.isEmpty()) {
                System.out.println("Adja meg az objektum típusát!");
                return null;
            }
            String type = arguments.get(0);
            Integer index = null;
            if (arguments.size()>1) {
                try {
                    index = Integer.parseInt(arguments.get(1))-1;
                } catch (NumberFormatException e) {
                    System.out.println("Érvénytelen szám");
                    return null;
                }
            }
            if (type.equals("board")) {
                List<Board> boards = boardStore.getAll();
                if (index==null) {
                    listObjects("board");
                    return null;
                }
                if (index<0 || index>=boards.size()) {
                    System.out.println("Érvénytelen index");
                    return null;
                } else {
                    Board board = boards.get(index);
                    return new BoardContext(this, board);
                }
            } else {
                System.out.println("Ismeretlen objektumtípus: "+type);
                return null;
            }
        } else if (command.equals("duplicate")) {
            if (arguments.isEmpty()) {
                System.out.println("Adja meg az objektum típusát!");
                return null;
            }
            String type = arguments.get(0);
            Integer index = null;
            if (arguments.size()>1) {
                try {
                    index = Integer.parseInt(arguments.get(1))-1;
                } catch (NumberFormatException e) {
                    System.out.println("Érvénytelen szám");
                    return null;
                }
            }
            if (type.equals("board")) {
                List<Board> boards = boardStore.getAll();
                if (index==null) {
                    listObjects("board");
                    return null;
                }
                if (index<0 || index>=boards.size()) {
                    System.out.println("Érvénytelen index");
                    return null;
                } else {
                    Board board = new Board(boards.get(index));
                    if (arguments.size()>2) {
                        board.setLabel(arguments.get(2));
                    } else {
                        board.setLabel(board.getLabel()+" (másolat)");
                    }
                    boardStore.register(board);
                    return new BoardContext(this, board);
                }
            } else {
                System.out.println("Ismeretlen objektumtípus: "+type);
                return null;
            }
        } else if (command.equals("list")) {
            if (arguments.isEmpty()) {
                System.out.println("Adja meg az objektum típusát!");
                return null;
            }
            String type = arguments.get(0);
            Map<String, String> pluralMap = new HashMap<String, String>();
            pluralMap.put("boards", "board");
            if (pluralMap.containsKey(type)) {
                type = pluralMap.get(type);
            }
            listObjects(type);
            return null;
        } else if (command.equals("save")) {
            File saveFile = associatedFile;
            
            if (arguments.isEmpty() && saveFile==null) {
                System.out.print("Fájl: ");
                String filepath = "";
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                try {
                    filepath = reader.readLine();
                } catch (IOException e) {
                    System.out.println("Olvasási hiba");
                    return null;
                }
                if (filepath == null) {
                    System.out.println("Olvasási hiba (null)");
                    return null;
                }
                saveFile = new File(filepath);
            }
            
            String extension = getExtension(saveFile);
            DocumentIo documentIo = Application.instance().getDocumentIoQueue().getByExtension(extension);
            try {
                documentIo.save(document, associatedFile);
            } catch (Exception e) {
                System.out.println("Sikertelen mentés: "+e.getMessage());
                return null;
            }
            System.out.println("Sikeres mentés!");
            return null;
        } else {
            return null;
        }
    }
    
    public void listObjects(String type) {
        if (type.equals("board")) {
            List<Board> boards = document.getBoardStore().getAll();
            if (boards.isEmpty()) {
                System.out.println("Nincs létrehozva tábla");
            } else {
                System.out.println();
                System.out.println("----------------------");
                System.out.println("Elérhető táblák:");
                System.out.println("----------------------");
                System.out.println();
                // TODO
                int i=1;
                for (Board board: boards) {
                    System.out.println(i+": "+board.getLabel());
                    i++;
                }
                System.out.println();
            }
        }
    }

    @Override
    public boolean leave() {
        // TODO
        return true;
    }

    @Override
    public String getValue(String name) {
        if (name.equals("label")) {
            return document.getLabel();
        } else {
            return "";
        }
    }

    @Override
    public Object getContextObject() {
        return document;
    }

    public Document getDocument() {
        return document;
    }

    // TODO: global file handler and document loader utils
    private String getExtension(File file) {
        String name = file.getName();
        int dotIndex = name.lastIndexOf('.');
        if (dotIndex>=0) {
            return name.substring(dotIndex+1);
        } else {
            return "";
        }
    }

}

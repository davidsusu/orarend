package hu.webarticum.aurora.plugins.consoleui.util;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hu.webarticum.aurora.plugins.consoleui.Context;

public class ArgumentsParser implements Iterable<String> {
    
    final String argumentsStr;
    
    final Context context;
    
    final Map<String, String> variables;
    
    public ArgumentsParser(String argumentsStr, Context context, Map<String, String> variables) {
        this.argumentsStr = argumentsStr.trim();
        this.context = context;
        this.variables = variables;
    }

    @Override
    public Iterator<String> iterator() {
        return new ArgumentIterator();
    }

    public class ArgumentIterator implements Iterator<String> {
    
        int position = 0;
        
        @Override
        public boolean hasNext() {
            return (position<argumentsStr.length());
        }
    
        @Override
        public String next() {
            int length = argumentsStr.length();
            if (position>=length) {
                throw new NoSuchElementException();
            }
            while (argumentsStr.charAt(position)==' ') {
                position++;
            }
            if (argumentsStr.charAt(position)=='@') {
                position++;
                int spacePosition = argumentsStr.indexOf(' ', position);
                String contextValueName;
                if (spacePosition==(-1)) {
                    contextValueName = argumentsStr.substring(position);
                    position = length;
                } else {
                    contextValueName = argumentsStr.substring(position, spacePosition);
                    position = spacePosition;
                }
                return context.getValue(contextValueName);
            } else if (argumentsStr.charAt(position)=='$') {
                position++;
                if (length>position && argumentsStr.charAt(position)=='{') {
                    String rawStr = getEscapedUntil(argumentsStr, position, '}');
                    String variableName = unescape(rawStr);
                    position = position+rawStr.length()+1;
                    return getValue(variableName);
                } else {
                    int spacePosition = argumentsStr.indexOf(' ', position);
                    String variableName;
                    if (spacePosition==(-1)) {
                        variableName = argumentsStr.substring(position);
                        position = length;
                    } else {
                        variableName = argumentsStr.substring(position, spacePosition);
                        position = spacePosition;
                    }
                    return getValue(variableName);
                }
            } else if (argumentsStr.charAt(position)=='{') {
                if (length>position+1 && argumentsStr.charAt(position+1)=='$') {
                    position += 2;
                    String rawStr = getEscapedUntil(argumentsStr, position, '}');
                    String variableName = unescape(rawStr);
                    position = position+rawStr.length()+1;
                    return getValue(variableName);
                } else {
                    int spacePosition = argumentsStr.indexOf(' ', position);
                    String result;
                    if (spacePosition==(-1)) {
                        result = argumentsStr.substring(position);
                        position = length;
                    } else {
                        result = argumentsStr.substring(position, spacePosition);
                        position = spacePosition;
                    }
                    return result;
                }
            } else if (argumentsStr.charAt(position)=='"') {
                position++;
                String stringStr = getStringFrom(argumentsStr, position);
                position += stringStr.length();
                if (position<length && argumentsStr.charAt(position)=='"') {
                    position++;
                }
                return parseString(stringStr);
            } else {
                if (argumentsStr.charAt(position)=='\\') {
                    position++;
                    if (position==length) {
                        return "";
                    }
                }
                int spacePosition = argumentsStr.indexOf(' ', position);
                String result;
                if (spacePosition==(-1)) {
                    result = argumentsStr.substring(position);
                    position = length;
                } else {
                    result = argumentsStr.substring(position, spacePosition);
                    position = spacePosition;
                }
                return result;
            }
        }
    
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    
        public void rewind() {
            position = 0;
        }
        
        protected String getEscapedUntil(String context, int startPosition, char stopChar) {
            int findPosition = startPosition;
            while (true) {
                int stopCharPosition = context.indexOf(stopChar, findPosition);
                if (stopCharPosition==(-1)) {
                    return context.substring(startPosition);
                }
                int escapePosition = context.indexOf('\\', findPosition);
                if (escapePosition==(-1) || stopCharPosition<escapePosition) {
                    return context.substring(startPosition, stopCharPosition);
                }
                findPosition = escapePosition+2;
            }
        }
        
        protected String getStringFrom(String context, int startPosition) {
            Pattern pattern = Pattern.compile("((\\\\.|[^\"])*)(\"|$)");
            Matcher matcher = pattern.matcher(context);
            if (matcher.find(startPosition)) {
                return matcher.group(1);
            } else {
                return context.substring(startPosition);
            }
        }
        
        protected String parseString(String stringStr) {
            StringBuilder resultBuilder = new StringBuilder();
            Pattern pattern = Pattern.compile("(\\\\(.)|\\$(\\w+)|((\\{\\$|\\$\\{)((\\\\.|[^}])*)\\}))");
            Matcher matcher = pattern.matcher(stringStr);
            int position = 0;
            while (matcher.find()) {
                int matchPosition = matcher.start();
                if (matchPosition>position) {
                    resultBuilder.append(stringStr.substring(position, matchPosition));
                }
                String escapeCharMatch = matcher.group(2);
                String variableMatch = matcher.group(3);
                String bracketVariableMatch = matcher.group(6);
                if (escapeCharMatch!=null) {
                    resultBuilder.append(escapeCharMatch);
                } else if (variableMatch!=null) {
                    resultBuilder.append(getValue(variableMatch));
                } else if (bracketVariableMatch!=null) {
                    resultBuilder.append(getValue(unescape(bracketVariableMatch)));
                }
                position = matcher.end();
            }
            if (position<stringStr.length()) {
                resultBuilder.append(stringStr.substring(position));
            }
            return resultBuilder.toString();
        }
        
        protected String unescape(String escaped) {
            return escaped.replaceAll("\\\\(.)", "$1");
        }
        
        protected String getValue(String variableName) {
            if (variables.containsKey(variableName)) {
                return variables.get(variableName);
            } else {
                return "";
            }
        }

    }

}

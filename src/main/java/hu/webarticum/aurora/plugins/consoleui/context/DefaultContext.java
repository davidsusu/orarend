package hu.webarticum.aurora.plugins.consoleui.context;

import java.io.File;
import java.util.List;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.core.io.DocumentIo;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.consoleui.Context;

public class DefaultContext implements Context {
    
    public DefaultContext() {
    }

    @Override
    public String getLabel() {
        return "S";
    }

    // XXX
    @SuppressWarnings("unused")
    @Override
    public Context executeCommand(String command, List<String> arguments) {
        if (command.equals("new")) {
            return new DocumentContext(new Document("Névtelen dokumentum"), null);
        } else if (command.equals("sample")) {
            if (arguments.isEmpty()) {
                // TODO: list sample documents
                return null;
            }
            
            Document document = null; // TODO
            
            if (document==null) {
                System.out.println("Nem lehet betölteni a példadokumentumot!");
                return null;
            } else {
                return new DocumentContext(document, null);
            }
        } else if (command.equals("load")) {
            if (arguments.isEmpty()) {
                System.out.println("Adja meg a fájl elérési útvonalát!");
                return null;
            }
            String filepath = arguments.get(0);
            File file = new File(filepath);
            

            String extension = getExtension(file);
            if (extension.isEmpty()) {
                extension = Info.DEFAULT_FILE_EXTENSION;
            }
            
            DocumentIo documentIo = Application.instance().getDocumentIoQueue().getByExtension(extension);
            if (documentIo==null) {
                System.out.println("Nem támogatott formátum");
                return null;
            }
            
            final Document document;
            
            try {
                document = documentIo.load(file);
            } catch (Exception e) {
                System.out.println("Sikertelen feldolgozás");
                return null;
            }
            
            return new DocumentContext(document, file);
        } else {
            return null;
        }
    }

    @Override
    public boolean leave() {
        return true;
    }

    @Override
    public String getValue(String name) {
        return "";
    }
    
    @Override
    public Object getContextObject() {
        return new Object();
    }

    // TODO: global file handler and document loader utils
    private String getExtension(File file) {
        String name = file.getName();
        int dotIndex = name.lastIndexOf('.');
        if (dotIndex>=0) {
            return name.substring(dotIndex+1);
        } else {
            return "";
        }
    }

}

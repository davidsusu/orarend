package hu.webarticum.aurora.plugins.consoleui;

import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.jpluginmanager.core.AbstractSimplePlugin;
import hu.webarticum.jpluginmanager.core.Version;

public class ConsoleUiPlugin extends AbstractSimplePlugin {
    
    public ConsoleUiPlugin() {
        extensionBinder.bind(CommonExtensions.Runner.class, ConsoleUiRunner.class);
    }
    
    @Override
    public String getName() {
        return "xxx.consoleui";
    }

    @Override
    public Version getVersion() {
        return new Version("0.1.0");
    }

    @Override
    public String getLabel() {
        return "Console UI";
    }

}

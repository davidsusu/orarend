package hu.webarticum.aurora.plugins.defaultswingui.util;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class RunnableChangeListener implements ChangeListener {

    private final Runnable runnable;
    
    public RunnableChangeListener(Runnable runnable) {
        this.runnable = runnable;
    }
    
    @Override
    public void stateChanged(ChangeEvent ev) {
        runnable.run();
    }

}

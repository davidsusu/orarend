package hu.webarticum.aurora.plugins.defaultswingui.util;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListModel;

// XXX
public class JListUtil {

    @SuppressWarnings("unchecked")
    public static <E> void addItem(JList<E> listComponent, Object item) {
        try {
            DefaultListModel<E> model = (DefaultListModel<E>)listComponent.getModel();
            model.addElement((E)item);
        } catch (Exception e) {
        }
    }

    @SuppressWarnings("unchecked")
    public static <E> void replaceItem(JList<E> listComponent, Object item, int index) {
        if (index>=0) {
            try {
                DefaultListModel<E> model = (DefaultListModel<E>)listComponent.getModel();
                model.set(index, (E)item);
            } catch (Exception e) {
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static <E> void insertItem(JList<E> listComponent, Object item, int index) {
        if (index>=0) {
            try {
                DefaultListModel<E> model = (DefaultListModel<E>)listComponent.getModel();
                model.insertElementAt((E)item, index);
            } catch (Exception e) {
            }
        }
    }

    public static <E> void replaceOrInsertItem(JList<?> listComponent, Object item, int index, boolean insert) {
        if (insert) {
            insertItem(listComponent, item, index);
        } else {
            replaceItem(listComponent, item, index);
        }
    }

    public static void removeItem(JList<?> listComponent, Object item) {
        try {
            DefaultListModel<?> model = (DefaultListModel<?>)listComponent.getModel();
            int itemCount = model.getSize();
            for (int i=0; i<itemCount; i++) {
                if (model.get(i).equals(item)) {
                    model.remove(i);
                    break;
                }
            }
        } catch (Exception e) {
        }
    }
    
    public static void remove(JList<?> listComponent, int index) {
        try {
            DefaultListModel<?> model = (DefaultListModel<?>)listComponent.getModel();
            model.remove(index);
        } catch (Exception e) {
        }
    }
    
    public static boolean isModelSupported(ListModel<?> listModel) {
        return (listModel instanceof DefaultListModel);
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.Painter;

import hu.webarticum.aurora.app.util.common.CollectionUtil;
import hu.webarticum.aurora.core.model.Color;

public class ColorUtil {

    public static java.awt.Color toAwtColor(Color color) {
        return new java.awt.Color(
            color.getRed(), color.getGreen(), color.getBlue()
        );
    }

    public static Color fromAwtColor(java.awt.Color color) {
        return new Color(
            color.getRed(), color.getGreen(), color.getBlue()
        );
    }

    public static Painter<Component> createStripPainter(Collection<Color> colors) {
        if (colors.size() > 1) {
            final List<java.awt.Color> awtColors = new ArrayList<java.awt.Color>();
            for (Color color: colors) {
                awtColors.add(toAwtColor(color));
            }
            return new Painter<Component>() {

                @Override
                public void paint(Graphics2D g, Component object, int width, int height) {
                    int stripWidth = 30;
                    int colorCount = awtColors.size();
                    int stripCount = (width + height) / stripWidth;
                    for (int i = 0; i < stripCount; i++) {
                        java.awt.Color stripcolor = awtColors.get(i % colorCount);
                        g.setColor(stripcolor);
                        Polygon polygon = new Polygon();
                        polygon.addPoint((i * stripWidth), 0);
                        polygon.addPoint((i * stripWidth) + stripWidth + 10, 0);
                        polygon.addPoint((i * stripWidth) + stripWidth + 10, height);
                        polygon.addPoint((i * stripWidth) - height, height);
                        g.fillPolygon(polygon);
                    }
                }
                
            };
        } else {
            final java.awt.Color awtColor = colors.isEmpty() ?
                java.awt.Color.WHITE :
                toAwtColor(CollectionUtil.getFirst(colors))
            ;
            return new Painter<Component>() {

                @Override
                public void paint(Graphics2D g, Component object, int width, int height) {
                    g.setColor(awtColor);
                    g.fillRect(0, 0, width, height);
                }
                
            };
        }
    }
    
}

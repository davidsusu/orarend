package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.rawText;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JFrame;

import hu.webarticum.aurora.app.util.autoput.AutoPutterAspectStructure;
import hu.webarticum.aurora.core.model.GeneralWrapper;


public class AutoPutAspectSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;
    

    private AutoPutterAspectStructure aspectStructure;
    
    private GeneralWrapper<List<String>> enabledAspectsWrapper;
    
    private Map<String, JCheckBox> checkBoxesByAspect = new LinkedHashMap<String, JCheckBox>();
    
    
    public AutoPutAspectSelectDialog(
        JFrame parent, AutoPutterAspectStructure aspectStructure, GeneralWrapper<List<String>> enabledAspectsWrapper
    ) {
        super(parent);
        this.title = rawText("Edit aspect");
        this.aspectStructure = aspectStructure;
        this.enabledAspectsWrapper = enabledAspectsWrapper;
        init();
    }
    
    @Override
    protected void build() {
        for (String aspect : aspectStructure.getAllAspects()) {
            JCheckBox checkBox = new JCheckBox(aspect, false);
            checkBoxesByAspect.put(aspect, checkBox);
            addRow(checkBox);
        }
    }

    @Override
    protected void load() {
        for (String enabledAspect : enabledAspectsWrapper.get()) {
            JCheckBox checkBox = checkBoxesByAspect.get(enabledAspect);
            if (checkBox != null) {
                checkBox.setSelected(true);
            }
        }
    }

    @Override
    protected void save() {
        List<String> newEnabledAspects = new ArrayList<String>();
        for (Map.Entry<String, JCheckBox> entry : checkBoxesByAspect.entrySet()) {
            if (entry.getValue().isSelected()) {
                newEnabledAspects.add(entry.getKey());
            }
        }
        enabledAspectsWrapper.set(newEnabledAspects);
    }

}

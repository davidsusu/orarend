package hu.webarticum.aurora.plugins.defaultswingui.util;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

public abstract class FieldSynchronizer {
    
    private DocumentListener listener = null;
    
    private Document sourceDocument = null;
    
    private JTextComponent sourceField = null;
    
    private JTextComponent targetField = null;
    
    private String previousSourceValue = null;
    
    public abstract String generateSecondaryValue(String primaryValue);
    
    public void bind(JTextComponent _sourceField, JTextComponent _targetField) {
        this.sourceField = _sourceField;
        this.sourceDocument = _sourceField.getDocument();
        this.previousSourceValue = _sourceField.getText();
        this.targetField = _targetField;
        this.listener = new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent ev) {
                handleChange();
            }

            @Override
            public void removeUpdate(DocumentEvent ev) {
                handleChange();
            }

            @Override
            public void changedUpdate(DocumentEvent ev) {
            }
            
            private void handleChange() {
                String currentValue = sourceField.getText();
                doSync(previousSourceValue, currentValue);
                previousSourceValue = currentValue;
            }
            
            private void doSync(String originalValue, final String currentValue) {
                String previousGeneratedText = generateSecondaryValue(originalValue);
                if (targetField.getText().equals(previousGeneratedText)) {
                    targetField.setText(generateSecondaryValue(currentValue));
                }
            }
            
        };
        this.sourceDocument.addDocumentListener(this.listener);
    }
    
    public void unbind() {
        if (listener!=null) {
            sourceDocument.removeDocumentListener(listener);
            listener = null;
            sourceDocument = null;
            sourceField = null;
            targetField = null;
        }
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class PurposeButton extends JButton {

    private static final long serialVersionUID = 1L;

    private final Border originalBorder;
    
    private final Color originalBackground;
    
    private final Color originalForeground;
    
    private final Font originalFont;

    public PurposeButton(ButtonPurpose purpose) {
        originalBorder = getBorder();
        originalBackground = getBackground();
        originalForeground = getForeground();
        originalFont = getFont();
        if (purpose != ButtonPurpose.NONE) {
            setPurpose(purpose);
        }
    }

    public void setPurpose(ButtonPurpose purpose) {
        if (purpose == ButtonPurpose.NONE) {
            setBackground(originalBackground);
            setBorder(originalBorder);
            setForeground(originalForeground);
            setFont(originalFont);
            putClientProperty("Nimbus.Overrides", new UIDefaults());
        } else {
            if (UIManager.getLookAndFeel().getName().equals("Nimbus")) {
                UIDefaults overrides = new UIDefaults();
                overrides.put("Button.background", ColorUtil.toAwtColor(purpose.getBackgroundColor()));
                // TODO: border
                putClientProperty("Nimbus.Overrides", overrides);
            } else {
                setBackground(ColorUtil.toAwtColor(purpose.getBackgroundColor()));
                setBorder(new LineBorder(ColorUtil.toAwtColor(purpose.getBackgroundColor())));
            }
            setForeground(ColorUtil.toAwtColor(purpose.getFontColor()));
            setFont(originalFont.deriveFont(purpose.isFontBold() ? Font.BOLD : Font.PLAIN));
        }
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;


public class JListReferenceSorter<E> {
    
    private List<JList<E>> lists;
    
    private List<E> items = new ArrayList<>();
    
    private Map<E, Integer> itemIndexMap = new HashMap<>();
    
    private E copiedObject = null;
    

    protected JComponent lastInsertComponent = null;
    protected int lastInsertIndex = -1;
    
    
    @SafeVarargs
    public JListReferenceSorter(JList<E>... lists) {
        this(Arrays.asList(lists));
    }

    public JListReferenceSorter(Collection<JList<E>> lists) {
        this.lists = new ArrayList<>(lists);
    }
    
    public E getItemByIndex(int i) {
        return items.get(i);
    }

    public int getItemIndex(Object item) {
        return items.indexOf(item);
    }
    
    public E getCopiedObject() {
        return copiedObject;
    }
    
    public void apply() {
        Iterator<JList<E>> iterator = lists.iterator();
        while (iterator.hasNext()) {
            final JList<E> listComponent = iterator.next();
            if (!JListUtil.isModelSupported(listComponent.getModel())) {
                iterator.remove();
                continue;
            }
            
            ListModel<E> listModel = listComponent.getModel();
            int itemCount = listModel.getSize();
            for (int i=0; i < itemCount; i++) {
                E item = listModel.getElementAt(i);
                itemIndexMap.put(item, items.size());
                items.add(item);
            }
            listComponent.putClientProperty(JListReferenceSorter.class, this);
            
            listComponent.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            listComponent.setDragEnabled(true);
            listComponent.setDropMode(DropMode.INSERT);
            listComponent.setTransferHandler(new ReferenceTransferHandler(this));
            
            listComponent.addMouseListener(new MouseAdapter() {
                
                @Override
                public void mousePressed(MouseEvent ev) {
                    listComponent.requestFocusInWindow();
                }
                
            });

            listComponent.addKeyListener(new KeyAdapter() {
                
                @Override
                public void keyPressed(KeyEvent ev) {
                    onKeyPressed(ev, listComponent);
                }
                
            });
        }
    }
    
    private void onKeyPressed(KeyEvent ev, JList<E> listComponent) {
        if (!ev.isControlDown()) {
            return;
        }
        
        int keyCode = ev.getKeyCode();
        if (keyCode == KeyEvent.VK_C) {
            ev.consume();
        } else if (keyCode == KeyEvent.VK_V) {
            ev.consume();
            if (copiedObject!=null) {
                E currentObject = copiedObject;
                copiedObject = null;
                removeFromLists(currentObject);
                int index = listComponent.getSelectedIndex();
                if (index==(-1)) {
                    index = listComponent.getModel().getSize();
                }
                JListUtil.insertItem(listComponent, currentObject, index);
                JListReferenceSorter.this.lastInsertComponent = listComponent;
                JListReferenceSorter.this.lastInsertIndex = index;
                listComponent.setSelectedIndex(index);
            }
        } else if (keyCode == KeyEvent.VK_X) {
            ev.consume();
            E object = listComponent.getSelectedValue();
            if (object!=null) {
                copiedObject = object;
                listComponent.repaint();
            }
        }
    }
    
    public void remove(Object item) {
        removeFromLists(item);
    }
    
    protected void removeFromLists(Object item) {
        for (JList<E> listComponent: lists) {
            JListUtil.removeItem(listComponent, item);
        }
    }
    
    public static class ReferenceTransferHandler extends TransferHandler {

        private static final long serialVersionUID = 1L;
        
        private JListReferenceSorter<?> handler;
        

        public ReferenceTransferHandler(JListReferenceSorter<?> handler) {
            this.handler = handler;
        }
        
        @Override
        public int getSourceActions(JComponent component) {
            return MOVE;
        }

        @Override
        protected Transferable createTransferable(JComponent component) {
            JList<?> listComponent = (JList<?>)component;
            Object selectedItem = listComponent.getSelectedValue();
            JListReferenceSorter<?> handler = (JListReferenceSorter<?>)listComponent.getClientProperty(JListReferenceSorter.class);
            int index = handler.getItemIndex(selectedItem);
            return new ReferenceDataHandler(index);
        }

        @Override
        public void exportDone(JComponent component, Transferable transferable, int action) {
            if (action!=TransferHandler.MOVE) {
                return;
            }
            try {
                JListReferenceSorter<?> handler = (JListReferenceSorter<?>)component.getClientProperty(JListReferenceSorter.class);
                if (handler!=null) {
                    int sourceIndex = (int)(Integer)transferable.getTransferData(ReferenceDataFlavor.instance());
                    Object sourceItem = handler.getItemByIndex(sourceIndex);
                    boolean internal = (handler.lastInsertComponent==component);
                    JList<?> listComponent = (JList<?>)component;
                    ListModel<?> listModel = listComponent.getModel();
                    int itemCount = listModel.getSize();
                    for (int i=0; i<itemCount; i++) {
                        Object item = listModel.getElementAt(i);
                        if (item==sourceItem && !(internal && handler.lastInsertIndex==i)) {
                            JListUtil.remove(listComponent, i);
                            break;
                        }
                    }
                }
            } catch (Exception e) {
            }
        }

        @Override
        public boolean canImport(TransferHandler.TransferSupport transferSupport) {
            try {
                if (!transferSupport.isDataFlavorSupported(ReferenceDataFlavor.instance())) {
                    return false;
                }
                Component component = transferSupport.getComponent();
                if (!(component instanceof JList)) {
                    return false;
                }
                JList<?> listComponent = (JList<?>)component;
                if (listComponent.getClientProperty(JListReferenceSorter.class)!=handler) {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
            return true;
        }

        @Override
        public boolean importData(TransferHandler.TransferSupport transferSupport) {
            ReferenceDataFlavor referenceDataFlavor = ReferenceDataFlavor.instance();
            if (canImport(transferSupport)) {
                try {
                    Integer index = (Integer)transferSupport.getTransferable().getTransferData(referenceDataFlavor);
                    Object item = handler.getItemByIndex(index);
                    Component component = transferSupport.getComponent();
                    JList<?> listComponent = (JList<?>)component;
                    JListReferenceSorter<?> handler = (JListReferenceSorter<?>)listComponent.getClientProperty(JListReferenceSorter.class);
                    if (handler!=null) {
                        JList.DropLocation listDropLocation = (JList.DropLocation)transferSupport.getDropLocation();
                        int insertedIndex = listDropLocation.getIndex();
                        JListUtil.replaceOrInsertItem(listComponent, item, insertedIndex, listDropLocation.isInsert());
                        handler.lastInsertComponent = listComponent;
                        handler.lastInsertIndex = insertedIndex;
                        listComponent.setSelectedIndex(insertedIndex);
                        if (handler.copiedObject!=null) {
                            handler.copiedObject = null;
                            for (JList<?> list: handler.lists) {
                                list.repaint();
                            }
                        }
                        listComponent.requestFocusInWindow();
                        return true;
                    }
                } catch (Exception e) {
                }
            }
            return false;
        }
        
    }
    
    public static class ReferenceDataHandler implements Transferable, Serializable {

        private static final long serialVersionUID = 1L;
        
        private int index;
        
        
        public ReferenceDataHandler(int index) {
            this.index = index;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor dataFlavor) {
            return dataFlavor.equals(ReferenceDataFlavor.instance());
        }
        
        
        @Override
        public Object getTransferData(DataFlavor dataFlavor) {
            if (dataFlavor.equals(ReferenceDataFlavor.instance())) {
                return Integer.valueOf(index);
            } else {
                return null;
            }
        }

        
        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[]{ReferenceDataFlavor.instance()};
        }
        
    }
    
    static class ReferenceDataFlavor extends DataFlavor {
        
        private static ReferenceDataFlavor instance = null;
        
        private ReferenceDataFlavor() {
            super(Object.class, "Reference");
        }
        
        public static ReferenceDataFlavor instance() {
            if (instance==null) {
                instance = new ReferenceDataFlavor();
            }
            return instance;
        }
        
    }
    
}

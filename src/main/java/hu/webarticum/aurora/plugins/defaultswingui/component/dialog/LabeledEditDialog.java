package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;

import hu.webarticum.aurora.plugins.defaultswingui.util.DocumentChangeListener;

public abstract class LabeledEditDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    protected JTextField labelField;
    
    public LabeledEditDialog(JFrame parent) {
        super(parent);
        // TODO
    }

    @Override
    protected String generateTitle() {
        String label = labelField.getText();
        if (label.length()>25) {
            label = label.substring(0, 24)+"…";
        }
        return title+" ("+label+")";
    }

    @Override
    protected void afterLoad() {
        super.afterLoad();
        
        labelField.getDocument().addDocumentListener(new DocumentChangeListener() {
        
            @Override
            public void changed(DocumentEvent ev) {
                modified = true;
                refreshTitle();
            }
            
        });
    }

    @Override
    protected boolean canSave() {
        if (!super.canSave()) {
            return false;
        }
        
        if (labelField.getText().isEmpty()) {
            OptionPaneUtil.showMessageDialog(parent, text("ui.swing.dialog.labelededit.specify_label"));
            labelField.requestFocusInWindow();
            return false;
        }
        
        return true;
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Objects;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.EmptyBorder;

import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.aurora.plugins.defaultswingui.util.PurposeButton;

public class AbstractOptionDialog extends Dialog {

    private static final long serialVersionUID = 1L;

    protected JPanel contentPanel;
    
    protected JPanel mainPanel;
    
    protected JPanel returnButtonPanel;

    private volatile JButton buttonToFocus = null;

    private volatile int result = -1;
    
    public AbstractOptionDialog(JFrame parent) {
        super(parent);
    }

    @Override
    protected void build() {
        contentPanel = new JPanel(new BorderLayout());
        containerPanel.add(contentPanel, BorderLayout.CENTER);

        returnButtonPanel = new JPanel();
        getContentPane().add(returnButtonPanel, BorderLayout.SOUTH);
    }
    
    protected void applyMessageType(int messageType) {
        getRootPane().setWindowDecorationStyle(styleFromMessageType(messageType));
        applyContentIcon(contentIconFromMessageType(messageType));
    }

    private static Icon contentIconFromMessageType(int messageType) {
        String contentIconName = contentIconNameFromMessageType(messageType);
        if (contentIconName == null) {
            return null;
        }

        return IconLoader.loadIcon(contentIconName);
    }
    
    private static String contentIconNameFromMessageType(int messageType) {
        switch (messageType) {
            case JOptionPane.ERROR_MESSAGE:
                return "dialog-error";
            case JOptionPane.QUESTION_MESSAGE:
                return "dialog-question";
            case JOptionPane.WARNING_MESSAGE:
                return "dialog-warning";
            case JOptionPane.INFORMATION_MESSAGE:
                return "dialog-info";
            case JOptionPane.PLAIN_MESSAGE:
            default:
                return null;
        }
    }
    
    private void applyContentIcon(Icon contentIcon) {
        Component component = ((BorderLayout)contentPanel.getLayout()).getLayoutComponent(BorderLayout.LINE_START);
        if (component != null) {
            contentPanel.remove(component);
        }
        
        if (contentIcon == null) {
            return;
        }
        
        JLabel iconLabel = new JLabel(contentIcon);
        iconLabel.setBorder(new EmptyBorder(15, 15, 15, 15));
        contentPanel.add(BorderLayout.LINE_START, iconLabel);
    }
    
    private static int styleFromMessageType(int messageType) {
        switch (messageType) {
            case JOptionPane.ERROR_MESSAGE:
                return JRootPane.ERROR_DIALOG;
            case JOptionPane.QUESTION_MESSAGE:
                return JRootPane.QUESTION_DIALOG;
            case JOptionPane.WARNING_MESSAGE:
                return JRootPane.WARNING_DIALOG;
            case JOptionPane.INFORMATION_MESSAGE:
                return JRootPane.INFORMATION_DIALOG;
            case JOptionPane.PLAIN_MESSAGE:
            default:
                return JRootPane.PLAIN_DIALOG;
        }
    }

    protected void addButtons(Object[] labels, ButtonPurpose[] purposes, Object defaultLabel) {
        purposes = ensurePurposes(purposes, labels.length);
        for (int i = 0; i < labels.length; i++) {
            JButton button = new PurposeButton(purposes[i]);
            Object value = labels[i];
            button.setText(Objects.toString(value));
            returnButtonPanel.add(button);
            if (Objects.equals(value, defaultLabel)) {
                buttonToFocus = button;
            }
            int index = i;
            button.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    result = index;
                    AbstractOptionDialog.this.setVisible(false);
                }
                
            });
            addWindowListener(new WindowAdapter() {
                
                @Override
                public void windowOpened(WindowEvent ev) {
                    if (buttonToFocus != null) {
                        buttonToFocus.requestFocus();
                    }
                }
                
            });
        }
    }

    private static ButtonPurpose[] ensurePurposes(ButtonPurpose[] optionPurposes, int buttonCount) {
        ButtonPurpose[] result = new ButtonPurpose[buttonCount];
        int length = optionPurposes != null ? optionPurposes.length : 0;
        for (int i = 0; i < length; i++) {
            result[i] = optionPurposes[i];
        }
        for (int i = length; i < buttonCount; i++) {
            result[i] = ButtonPurpose.NONE;
        }
        return result;
    }

    public int getResult() {
        return result;
    }

}

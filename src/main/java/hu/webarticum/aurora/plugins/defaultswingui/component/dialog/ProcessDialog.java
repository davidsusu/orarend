package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;

// FIXME: extends Dialog?
public abstract class ProcessDialog extends JDialog {

    private static final long serialVersionUID = 1L;
    
    protected JFrame parent;
    
    protected JLabel labelPlace;
    
    protected JProgressBar progressBar;
    
    protected JTextArea logTextArea;
    
    protected JScrollPane logScrollPane;
    
    protected JButton button;

    protected boolean running;

    protected boolean open = false;

    protected boolean cancelled = false;
    
    public ProcessDialog(JFrame parent) {
        this(parent, text("ui.swing.dialog.processdialog.title"), false);
    }

    public ProcessDialog(final JFrame parent, String title) {
        this(parent, title, false);
    }

    public ProcessDialog(final JFrame parent, boolean compact) {
        this(parent, text("ui.swing.dialog.processdialog.title"), compact);
    }
    
    public ProcessDialog(final JFrame parent, String title, boolean compact) {
        super(parent, title, true);
        
        this.parent = parent;
        
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.PAGE_AXIS));
        topPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
        contentPane.add(topPanel, BorderLayout.PAGE_START);
        
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new BorderLayout());
        
        if (!compact) {
            topPanel.add(labelPanel);
        }
        
        labelPlace = new JLabel("");
        labelPlace.setPreferredSize(new Dimension(400, 30));
        labelPlace.putClientProperty("html.disable", true);
        labelPanel.add(labelPlace, BorderLayout.LINE_START);
        
        progressBar = new JProgressBar(0, 100);
        progressBar.setPreferredSize(new Dimension(400, 30));
        progressBar.setIndeterminate(true);
        topPanel.add(progressBar);
        
        logTextArea = new JTextArea();
        logTextArea.setEditable(false);
        logScrollPane = new JScrollPane(logTextArea);
        logScrollPane.setPreferredSize(new Dimension(400, 170));

        if (!compact) {
            JPanel middlePanel = new JPanel(new BorderLayout());
            middlePanel.setBorder(new EmptyBorder(0, 10, 0, 10));
            contentPane.add(middlePanel, BorderLayout.CENTER);
            
            middlePanel.add(logScrollPane, BorderLayout.CENTER);
        }
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        contentPane.add(buttonPanel, BorderLayout.PAGE_END);
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            
            @Override
            public void windowClosing(WindowEvent ev) {
                if (running) {
                    int answer = OptionPaneUtil.showConfirmDialog(
                        parent, text("ui.swing.dialog.processdialog.stop_process"), ButtonPurpose.ABORT
                    );
                    if (answer!=JOptionPane.OK_OPTION) {
                        return;
                    }
                }
                _cancel();
                closeDialog();
            }
            
        });
        
        button = new MultilingualButton("ui.swing.dialog.processdialog.close");

        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ev) {
                if (running) {
                    _cancel();
                } else {
                    closeDialog();
                }
            }
          
        });
        
        if (!compact) {
            buttonPanel.add(button);
        }
        
        setRunning(false);
        
        if (compact) {
            setResizable(false);
        }

        setModal(true);
        pack();
    }
    
    public void run() {
        setRunning(true);
        _start();

        if (parent!=null) {
            Dimension parentSize = parent.getSize();
            Point parentLocation = parent.getLocation();
            setLocation(parentLocation.x+(parentSize.width/5), parentLocation.y+(parentSize.height/10));
        }
        open = true;

        int xCenter = 300;
        int yCenter = 200;
        if (parent!=null) {
            xCenter = parent.getX()+(parent.getWidth()/2);
            yCenter = parent.getY()+(parent.getHeight()/2);
        } else {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            xCenter = screenSize.width/2;
            yCenter = screenSize.height/2;
        }
        int x = xCenter-(getWidth()/2);
        int y = yCenter-(getHeight()/2);
        setLocation(x, y);
        
        setVisible(true);
    }
    
    public void setRunning(boolean running) {
        this.running = running;
        button.setText(
            running?
            text("ui.swing.dialog.processdialog.stop"):
            text("ui.swing.dialog.processdialog.finish")
        );
        progressBar.setEnabled(running);
        if (!running) {
            progressBar.setIndeterminate(false);
            progressBar.setValue(0);
        }
    }
    
    public void closeDialog() {
        if (open) {
            open = false;
            setVisible(false);
            dispose();
        }
    }
    
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
    
    public boolean isCancelled() {
        return cancelled;
    }
    
    protected abstract void _start();
    
    protected abstract void _cancel();

}
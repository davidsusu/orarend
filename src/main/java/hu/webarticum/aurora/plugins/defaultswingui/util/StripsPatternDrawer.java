package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.List;

public class StripsPatternDrawer {

    public void drawStripes(Graphics2D g, Rectangle rectangle, List<Color> colors, int stripWidth) {
        drawStripes(g, rectangle, colors, stripWidth, 255);
    }
    
    public void drawStripes(Graphics2D g, Rectangle rectangle, List<Color> colors, int stripWidth, int alpha) {
        BufferedImage image = new BufferedImage(rectangle.width, rectangle.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D sg = (Graphics2D)image.getGraphics();
        sg.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        int numOfColors = colors.size();
        int numOfStrips = (rectangle.width + rectangle.height) / stripWidth;
        for (int i = 0; i < numOfStrips; i++) {
            Color color = colors.get(i % numOfColors);
            sg.setColor(color);
            Polygon polygon = new Polygon();
            polygon.addPoint((i * stripWidth), 0);
            polygon.addPoint((i * stripWidth) + stripWidth + 10, 0);
            polygon.addPoint((i * stripWidth) + stripWidth + 10, rectangle.height);
            polygon.addPoint((i * stripWidth) - rectangle.height, rectangle.height);
            sg.fillPolygon(polygon);
        }
        
        Composite currentComposite = g.getComposite();
        if (alpha < 255) {
            AlphaComposite alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha / 256f);
            g.setComposite(alphaComposite);
        }
        
        g.drawImage(
            image,
            rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height,
            0, 0, rectangle.width, rectangle.height,
            null
        );
        
        g.setComposite(currentComposite); 
    }

    public void drawStripes(Graphics2D g, Rectangle rectangle, Color color, int fillWidth, int gapWidth) {
        drawStripes(g, rectangle, color, fillWidth, gapWidth, 255);
    }

    public void drawStripes(Graphics2D g, Rectangle rectangle, Color color, int fillWidth, int gapWidth, int alpha) {
        BufferedImage image = new BufferedImage(rectangle.width, rectangle.height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D sg = (Graphics2D)image.getGraphics();
        sg.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        int stepWidth = fillWidth + gapWidth;
        int to = rectangle.width + rectangle.height;
        
        sg.setColor(color);
        for (int start = 0; start < to; start += stepWidth) {
            Polygon polygon = new Polygon();
            polygon.addPoint(start + fillWidth, 0);
            polygon.addPoint(start, 0);
            polygon.addPoint(start - rectangle.height, rectangle.height);
            polygon.addPoint(start - rectangle.height + fillWidth, rectangle.height);
            sg.fillPolygon(polygon);
        }

        Composite currentComposite = g.getComposite();
        AlphaComposite alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha / 256f);
        g.setComposite(alphaComposite);
        
        g.drawImage(
            image,
            rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height,
            0, 0, rectangle.width, rectangle.height,
            null
        );
        
        g.setComposite(currentComposite);
    }

}

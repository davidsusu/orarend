package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JPanel;
import javax.swing.Scrollable;

public class VerticallyScrollablePanel extends JPanel implements Scrollable {

    private static final long serialVersionUID = 1L;

    @Override
    public int getScrollableBlockIncrement(Rectangle visibleArea, int orientation, int direction) {
        return 16;
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleArea, int orientation, int direction) {
        return 1;
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return true;
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        // FIXME
        return (getParent().getHeight()>getPreferredSize().getHeight());
    }

}

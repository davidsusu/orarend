package hu.webarticum.aurora.plugins.defaultswingui.i18n;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.MutableComboBoxModel;

public class MultilingualComboBox<T> extends JComboBox<MultilingualComboBox.Item<T>> implements MultilingualComponent {

    private static final long serialVersionUID = 1L;

    protected MultilingualContent tooltipContent;

    public MultilingualComboBox() {
        this("");
    }

    public MultilingualComboBox(String tooltipPath) {
        this(new PathMultilingualContent(tooltipPath));
    }

    public MultilingualComboBox(MultilingualContent tooltipContent) {
        this.tooltipContent = tooltipContent;
    }

    @Override
    public void reloadLanguageTexts() {
        setToolTipText(tooltipContent.toStringOrNull());
    }

    public void addMultilingualItem(String labelPath, T data) {
        addItem(new Item<T>(labelPath, data));
    }

    public void addItem(String label, T data) {
        addItem(new Item<T>(false, label, data));
    }
    
    @SuppressWarnings("unchecked")
    public T getSelectedData() {
        Object selectedItem = getSelectedItem();
        if (selectedItem==null) {
            return null;
        } else {
            return ((Item<T>)selectedItem).getData();
        }
    }

    public void setSelectedData(T data) {
        int count = getItemCount();
        for (int i = 0; i < count; i++) {
            Item<T> item = getItemAt(i);
            if (item.getData().equals(data)) {
                setSelectedIndex(i);
                break;
            }
        }
    }
    
    public static class Item<T> implements Serializable {

        private static final long serialVersionUID = 1L;

        
        protected final boolean isMultilingual;

        protected final String labelPath;

        protected final String label;

        protected final T data;
        
        public Item(String labelPath, T data) {
            this.isMultilingual = true;
            this.labelPath = labelPath;
            this.label = null;
            this.data = data;
        }

        public Item(boolean isMultilingual, String labelOrlabelPath, T data) {
            this.isMultilingual = isMultilingual;
            if (isMultilingual) {
                this.labelPath = labelOrlabelPath;
                this.label = null;
            } else {
                this.labelPath = null;
                this.label = labelOrlabelPath;
            }
            this.data = data;
        }
        
        public T getData() {
            return data;
        }
        
        @Override
        public String toString() {
            if (isMultilingual) {
                return text(this.labelPath);
            } else {
                return this.label;
            }
        }
        
    }
    
    public class Model extends AbstractListModel<Item<T>> implements MutableComboBoxModel<Item<T>>, Serializable {

        private static final long serialVersionUID = 1L;

        
        protected List<Item<T>> items = new LinkedList<Item<T>>();
        
        protected int index = 0;
        
        @Override
        public void setSelectedItem(Object item) {
            index = items.indexOf(item);
        }

        @Override
        public Object getSelectedItem() {
            if (index==(-1)) {
                return null;
            } else {
                return items.get(index);
            }
        }

        @Override
        public int getSize() {
            return items.size();
        }

        @Override
        public Item<T> getElementAt(int index) {
            return items.get(index);
        }

        @Override
        public void addElement(Item<T> item) {
            items.add(item);
        }

        @Override
        public void removeElement(Object item) {
            items.remove(item);
        }

        @Override
        public void insertElementAt(Item<T> item, int index) {
            items.add(index, item);
        }

        @Override
        public void removeElementAt(int index) {
            items.remove(index);
        }
        
    }

}

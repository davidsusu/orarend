package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JTextField;

import hu.webarticum.aurora.app.memento.BoardLightMemento;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.chm.Command;


public class BoardEditDialog extends LabeledEditDialog {

    private static final long serialVersionUID = 1L;

    private Board board;
    
    private Document document;
    
    public BoardEditDialog(JFrame parent, Document document, Board board) {
        super(parent);
        this.title = text("ui.swing.dialog.boardedit.title");
        this.board = board;
        this.document = document;
        init();
    }
    
    @Override
    protected void build() {
        labelField = new JTextField();
        labelField.setMaximumSize(new Dimension(700, 25));
        addRow(new MultilingualLabel("ui.swing.dialog.boardedit.label"), labelField);
    }

    @Override
    protected void load() {
        labelField.setText(board.getLabel());
    }

    @Override
    protected void save() {
        if (!modified) {
            return;
        }
        
        BoardLightMemento newMemento = new BoardLightMemento(labelField.getText());
        
        Command command;
        Document.BoardStore boardStore = document.getBoardStore();
        
        if (boardStore.contains(board)) {
            BoardLightMemento oldMemento = new BoardLightMemento(board);
            command = new StoreItemEditCommand<Board>(
                "ui.swing.dialog.boardedit.command.edit",
                boardStore, board, oldMemento, newMemento
            );
        } else {
            newMemento.apply(board);
            command = new StoreItemRegisterCommand<Board>(
                "ui.swing.dialog.boardedit.command.register",
                boardStore, board
            );
        }
        
        history().addAndExecute(command);
    }
    
}
package hu.webarticum.aurora.plugins.defaultswingui.i18n;


public interface MultilingualContent {
    
    public String toStringOrNull();
    
}

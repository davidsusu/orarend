package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Labeled.Wrapper;
import hu.webarticum.aurora.core.model.MultiComparator;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.BlockFilterPanel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;

public class BlockMultiSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private Collection<Block> loadedBlocks;

    private Set<Block> markedBlocks = new HashSet<Block>();
    
    private BlockFilterPanel blockFilterPanel;
    
    private JList<Labeled.Wrapper> filteredBlockListDisplay;
    
    private JList<Labeled.Wrapper> selectedBlockListDisplay;
    
    private TreeSet<Block> selectedBlocks = new TreeSet<Block>(new MultiComparator<Block>(new Labeled.LabeledComparator()));

    public BlockMultiSelectDialog(JFrame parent, Document document, Collection<Block> loadedBlocks) {
        this(parent, document, loadedBlocks, null);
    }
    
    public BlockMultiSelectDialog(
        JFrame parent, Document document, Collection<Block> loadedBlocks,
        Collection<Block> markedBlocks
    ) {
        super(parent);
        this.title = text("ui.swing.dialog.blockmultiselect.title");
        this.document = document;
        this.loadedBlocks = loadedBlocks;
        this.selectedBlocks.addAll(loadedBlocks);
        if (markedBlocks != null) {
            this.markedBlocks.addAll(markedBlocks);
        }
        init(false, true);
    }
    
    @Override
    protected void build() {
        JPanel outerPanel = new JPanel();
        outerPanel.setLayout(new BoxLayout(outerPanel, BoxLayout.LINE_AXIS));
        containerPanel.add(outerPanel, BorderLayout.CENTER);
        
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BorderLayout());
        leftPanel.setBorder(new CompoundBorder(
            new EmptyBorder(0, 0, 0, 7),
            new CompoundBorder(
                new LineBorder(java.awt.Color.GRAY),
                new EmptyBorder(10, 10, 10, 10)
            )
        ));
        outerPanel.add(leftPanel);
        
        blockFilterPanel = new BlockFilterPanel(parent, document);
        blockFilterPanel.setColorAspectVisible(false);
        blockFilterPanel.addChangeListener(new BlockFilterPanel.BlockFilterChangeListener() {
            
            @Override
            public void changed(BlockFilterPanel.BlockFilterChangeEvent ev) {
                reloadFilteredList();
            }
        });
        leftPanel.add(blockFilterPanel, BorderLayout.PAGE_START);
        
        filteredBlockListDisplay = new JList<Labeled.Wrapper>();
        filteredBlockListDisplay.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JScrollPane filteredBlockListScrollPane = new JScrollPane(filteredBlockListDisplay);
        filteredBlockListScrollPane.setPreferredSize(new Dimension(200, 230));

        final ListCellRenderer<? super Labeled.Wrapper> originalRenderer = filteredBlockListDisplay.getCellRenderer();
        filteredBlockListDisplay.setCellRenderer(new ListCellRenderer<Labeled.Wrapper>() {

            @Override
            public Component getListCellRendererComponent(
                JList<? extends Wrapper> list, Wrapper value, int index,
                boolean isSelected, boolean cellHasFocus
            ) {
                Component component = originalRenderer.getListCellRendererComponent(
                    list, value, index, isSelected, cellHasFocus
                );
                if (component instanceof JLabel) {
                    JLabel label = (JLabel)component;
                    if (markedBlocks.contains(value.get())) {
                        Font font = label.getFont();
                        Map<TextAttribute, Object> fontAttributes = new HashMap<TextAttribute, Object>(
                            font.getAttributes()
                        );
                        fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                        fontAttributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
                        label.setFont(font.deriveFont(fontAttributes));
                    }
                }
                return component;
            }
            
        });
        
        leftPanel.add(filteredBlockListScrollPane, BorderLayout.CENTER);
        
        JPanel leftButtonsPanel = new JPanel();
        leftButtonsPanel.setLayout(new BorderLayout());
        leftPanel.add(leftButtonsPanel, BorderLayout.PAGE_END);

        JButton selectAllButton = new JButton(text("ui.swing.dialog.blockmultiselect.select_all"));
        selectAllButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                int size = filteredBlockListDisplay.getModel().getSize();
                int[] indices = new int[size];
                for (int i = 0; i < size; i++) {
                    indices[i] = i;
                }
                filteredBlockListDisplay.setSelectedIndices(indices);
            }
            
        });
        leftButtonsPanel.add(selectAllButton, BorderLayout.LINE_START);
        
        JButton addButton = new JButton(text("ui.swing.dialog.blockmultiselect.add_selection")+" →");
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                for (Labeled.Wrapper wrapper: filteredBlockListDisplay.getSelectedValuesList()) {
                    selectedBlocks.add((Block)wrapper.get());
                }
                reloadSelectedList();
            }
            
        });
        leftButtonsPanel.add(addButton, BorderLayout.LINE_END);
        
        
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
        rightPanel.setBorder(new CompoundBorder(
            new EmptyBorder(0, 7, 0, 0),
            new CompoundBorder(
                new LineBorder(java.awt.Color.GRAY),
                new EmptyBorder(10, 10, 10, 10)
            )
        ));
        outerPanel.add(rightPanel);

        rightPanel.add(new MultilingualLabel("ui.swing.dialog.blockmultiselect.list_content"));
        
        selectedBlockListDisplay = new JList<Labeled.Wrapper>();
        selectedBlockListDisplay.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        final Runnable removeAction = new Runnable() {
            
            @Override
            public void run() {
                Labeled.Wrapper wrapper = selectedBlockListDisplay.getSelectedValue();
                if (wrapper!=null) {
                    selectedBlocks.remove((Block)wrapper.get());
                    reloadSelectedList();
                }
            }
            
        };
        selectedBlockListDisplay.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getButton()==MouseEvent.BUTTON3) {
                    removeAction.run();
                }
            }
        });
        selectedBlockListDisplay.addKeyListener(new KeyAdapter() {
            
            @Override
            public void keyPressed(KeyEvent ev) {
                if (ev.getKeyCode()==KeyEvent.VK_DELETE) {
                    removeAction.run();
                }
            }
            
        });
        JScrollPane selectedBlockListScrollPane = new JScrollPane(selectedBlockListDisplay);
        selectedBlockListScrollPane.setPreferredSize(new Dimension(200, 300));
        rightPanel.add(selectedBlockListScrollPane);
    }

    @Override
    protected void load() {
        reloadFilteredList();
        reloadSelectedList();
    }

    @Override
    protected void save() {
        loadedBlocks.clear();
        loadedBlocks.addAll(selectedBlocks);
    }
    
    private void reloadFilteredList() {
        DefaultComboBoxModel<Labeled.Wrapper> model = new DefaultComboBoxModel<Labeled.Wrapper>();
        List<Block> filteredBlocks = document.getBlockStore().getAll().filter(blockFilterPanel.getBlockFilter());
        for (Block block : filteredBlocks) {
            model.addElement(new Labeled.Wrapper(block));
        }
        filteredBlockListDisplay.setModel(model);
    }
    
    private void reloadSelectedList() {
        DefaultComboBoxModel<Labeled.Wrapper> model = new DefaultComboBoxModel<Labeled.Wrapper>();
        for (Block block: selectedBlocks) {
            model.addElement(new Labeled.Wrapper(block));
        }
        selectedBlockListDisplay.setModel(model);
    }

}

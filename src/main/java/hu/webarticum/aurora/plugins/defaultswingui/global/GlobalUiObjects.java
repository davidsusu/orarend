package hu.webarticum.aurora.plugins.defaultswingui.global;

import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Window;

import javax.swing.JFrame;

import hu.webarticum.aurora.plugins.defaultswingui.util.NotificationQueue;

public class GlobalUiObjects {

    private static volatile JFrame mainFrame = null;

    private static volatile NotificationQueue notificationQueue = null;
    
    private static Dimension cachedScreenSize;
    
    private static Insets cachedScreenInsets;
    
    private static Object lock = new Object();
    
    public static void setMainFrame(JFrame mainFrame) {
        GlobalUiObjects.mainFrame = mainFrame;
    }

    public static JFrame getMainFrame() {
        return mainFrame;
    }

    public static Dimension getScreenSize() {
        return getScreenSize(null);
    }
    
    public static Dimension getScreenSize(Window temporaryContext) {
        Dimension screenSize;
        synchronized (lock) {
            if (cachedScreenSize != null) {
                screenSize = cachedScreenSize;
            } else {
                Window window;
                boolean dispose = false;
                if (mainFrame != null) {
                    window = mainFrame;
                } else if (temporaryContext != null) {
                    window = temporaryContext;
                } else {
                    window = createTemporaryFrame();
                    dispose = true;
                }
                GraphicsDevice device = window.getGraphicsConfiguration().getDevice();
                DisplayMode displayMode = device.getDisplayMode();
                screenSize = new Dimension(
                    displayMode.getWidth(),
                    displayMode.getHeight()
                );
                if (dispose) {
                    window.setVisible(false);
                    window.dispose();
                }
                if (mainFrame != null) {
                    cachedScreenSize = screenSize;
                }
            }
        }
        return screenSize;
    }

    public static Insets getScreenInsets() {
        return getScreenInsets(null);
    }
    
    public static Insets getScreenInsets(Window temporaryContext) {
        Insets screenInsets;
        synchronized (lock) {
            if (cachedScreenInsets != null) {
                screenInsets = cachedScreenInsets;
            } else {
                Window window;
                boolean dispose = false;
                if (mainFrame != null) {
                    window = mainFrame;
                } else if (temporaryContext != null) {
                    window = temporaryContext;
                } else {
                    window = createTemporaryFrame();
                    dispose = true;
                }
                screenInsets = java.awt.Toolkit.getDefaultToolkit().getScreenInsets(window.getGraphicsConfiguration());
                if (dispose) {
                    window.setVisible(false);
                    window.dispose();
                }
                if (mainFrame != null) {
                    cachedScreenInsets = screenInsets;
                }
            }
        }
        return screenInsets;
    }
    
    private static JFrame createTemporaryFrame() {
        JFrame frame = new JFrame();
        frame.setSize(new Dimension(10, 10));
        frame.setLocation(new Point(0, 0));
        frame.setVisible(true);
        return frame;
    }
    
    public static NotificationQueue getNotificationQueue() {
        if (notificationQueue == null) {
            notificationQueue = new NotificationQueue.DefaultNotificationQueue();
        }
        return notificationQueue;
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.FileUtil;
import hu.webarticum.aurora.app.util.IntervalFlowAligner;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.app.util.common.StringUtil;
import hu.webarticum.aurora.app.util.email.Attachment;
import hu.webarticum.aurora.app.util.email.ImageAttachment;
import hu.webarticum.aurora.app.util.email.SimpleMessage;
import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.ActivityFlow;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.MultiComparator;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.BlockFilterPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.ColorButton;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.FileChooserField;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.FontButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualCheckBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualComboBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualRadioButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualTabbedPane;
import hu.webarticum.aurora.plugins.defaultswingui.util.ActivityFlowImageRenderer;
import hu.webarticum.aurora.plugins.defaultswingui.util.AutoExtensionFileChooser;
import hu.webarticum.aurora.plugins.defaultswingui.util.BoardImageColorSchemeListBuilder;
import hu.webarticum.aurora.plugins.defaultswingui.util.ColorUtil;
import hu.webarticum.aurora.plugins.defaultswingui.util.RunnableChangeListener;
import hu.webarticum.aurora.plugins.defaultswingui.util.RunnableItemListener;


public class ImageExportDialog extends AbstractExportDialog {

    private static final long serialVersionUID = 1L;

    
    private Board board;
    
    private BlockFilterPanel sourceBlockFilterPanel;
    
    private ActivityFlowImageRenderer.ColorScheme sourceColorScheme;


    private MultilingualTabbedPane tabbedPane = new MultilingualTabbedPane();
    
    
    private JPanel commonTabPanel = new JPanel();
    
    private JTextField titleField = new JTextField();

    private MultilingualComboBox<String> titleAlignComboBox = new MultilingualComboBox<String>();
    
    private AutoExtensionFileChooser titleImageFileChooser = new AutoExtensionFileChooser();
    
    private FileChooserField titleImageFileField = new FileChooserField();

    private MultilingualComboBox<String> titleImageAlignComboBox = new MultilingualComboBox<String>();

    private MultilingualCheckBox titleImageSizingCheckBox = new MultilingualCheckBox();

    private JSpinner titleImageScaleSpinner = createDecimalSpinner(new BigDecimal("0.01"), new BigDecimal("9.99"), 2);
    
    private JSpinner startDayWeekSpinner;

    private JComboBox<Labeled.PairWrapper<Time>> startDayCombo;

    private JSpinner endDayWeekSpinner;

    private JComboBox<Labeled.PairWrapper<Time>> endDayCombo;
    
    private JSpinner startTimeHourSpinner;
    
    private JSpinner startTimeMinuteSpinner;
    
    private JSpinner startTimeSecondSpinner;
    
    private JSpinner endTimeHourSpinner;
    
    private JSpinner endTimeMinuteSpinner;
    
    private JSpinner endTimeSecondSpinner;
    
    
    private JPanel layoutTabPanel = new JPanel();

    private JSpinner topMarginSpinner = createIntegerSpinner(0, 999);

    private JSpinner leftMarginSpinner = createIntegerSpinner(0, 999);

    private JSpinner bottomMarginSpinner = createIntegerSpinner(0, 999);

    private JSpinner rightMarginSpinner = createIntegerSpinner(0, 999);

    private JSpinner titleImageSpaceSpinner = createIntegerSpinner(0, 999);

    private JSpinner titleBelowSpaceSpinner = createIntegerSpinner(0, 999);

    private JSpinner leftLabelsWidthSpinner = createIntegerSpinner(1, 999);

    private JSpinner leftLabelAboveSpaceSpinner = createIntegerSpinner(0, 999);

    private JSpinner secondsPerPixelSpinner = createIntegerSpinner(1, 999);

    private JSpinner activityTopPadSpinner = createIntegerSpinner(0, 999);

    private JSpinner activityLeftPadSpinner = createIntegerSpinner(0, 999);
    
    private JSpinner headerHeightSpinner = createIntegerSpinner(1, 999);

    private JSpinner headerBelowSpaceSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnsSpaceSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnTopPadSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnLeftPadSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnBottomPadSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnRightPadSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnMinimumInnerWidthSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnMaximuminnerWidthSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnActivityOptimalWidthSpinner = createIntegerSpinner(0, 999);

    private JSpinner columnActivitySpaceSpinner = createIntegerSpinner(0, 999);

    private JPanel fontsTabPanel = new JPanel();
    
    private FontButton titleFontButton;
    
    private FontButton headerFontButton;
    
    private FontButton leftLabelFontButton;
    
    private FontButton activityLabelFontButton;

    
    private JPanel colorsTabPanel = new JPanel();

    private MultilingualRadioButton whiteBlackColorsRadioButton = new MultilingualRadioButton();
    
    private MultilingualRadioButton customColorsRadioButton = new MultilingualRadioButton();
    
    private MultilingualCheckBox applyActivityColorsCheckbox = new MultilingualCheckBox();
    
    private List<JLabel> moreColorsLabels = new ArrayList<JLabel>();

    private MultilingualComboBox<ActivityFlowImageRenderer.ColorScheme> colorSchemeComboBox = new MultilingualComboBox<ActivityFlowImageRenderer.ColorScheme>();
    
    private MultilingualButton colorSchemeButton = new MultilingualButton();
    
    private ColorButton backgroundColorButton;
    
    private ColorButton titleColorButton;
    
    private ColorButton headerBorderColorButton;
    
    private ColorButton headerBackgroundColorButton;
    
    private ColorButton headerLabelColorButton;
    
    private ColorButton columnBorderColorButton;
    
    private ColorButton columnBackgroundColorButton;
    
    private ColorButton startLineColorButton;
    
    private ColorButton startLabelColorButton;
    
    private ColorButton activityBorderColorButton;
    
    private ColorButton activityBackgroundColorButton;
    
    private ColorButton activityLabelColorButton;
    
    private ColorButton activityLabelBackgroundColorButton;

    private MultilingualLabel colorAspectLabel = new MultilingualLabel();
    
    private MultilingualComboBox<Object> colorAspectComboBox = new MultilingualComboBox<Object>();

    
    private JPanel filterTabPanel = new JPanel();

    private BlockFilterPanel blockFilterPanel;
    
    
    private JPanel saveTabPanel = new JPanel();

    private MultilingualRadioButton saveSingleFileRadioButton = new MultilingualRadioButton();

    private MultilingualLabel singleFileLabel = new MultilingualLabel();
    
    private AutoExtensionFileChooser singleFileChooser = new AutoExtensionFileChooser();
    
    private FileChooserField singleFileField = new FileChooserField();

    private MultilingualRadioButton saveMultiFileRadioButton = new MultilingualRadioButton();
    
    private MultilingualLabel aspectLabel = new MultilingualLabel();
    
    private MultilingualCheckBox splitByPeriodsCheckBox = new MultilingualCheckBox();

    private JFileChooser multiFileFolderFileChooser = new JFileChooser();
    
    private MultilingualCheckBox extractClassesCheckBox = new MultilingualCheckBox();
    private MultilingualCheckBox extractPersonCheckBox = new MultilingualCheckBox();
    private MultilingualCheckBox extractLocaleCheckBox = new MultilingualCheckBox();
    
    private MultilingualLabel filenameTemplateLabel = new MultilingualLabel();

    private MultilingualRadioButton saveMultiTargetFolderRadioButton = new MultilingualRadioButton();

    private MultilingualLabel multiFileFolderLabel = new MultilingualLabel();
    
    private FileChooserField multiFileFolderFileField = new FileChooserField();

    private JTextField filenameTemplateField = new JTextField();

    private MultilingualRadioButton saveMultiTargetEmailRadioButton = new MultilingualRadioButton();

    
    private JPanel emailTabPanel = new JPanel();
    
    private JTextField emailSubjectField = new JTextField();
    
    private JTextArea emailContentTextArea = new JTextArea();
    
    private MultilingualCheckBox emailRenderAllImagesCheckBox = new MultilingualCheckBox();
    
    
    private PreviewPanel previewTabPanel = new PreviewPanel();
    
    
    public ImageExportDialog(
        JFrame parent, Board board, Document document,
        BlockFilterPanel sourceBlockFilterPanel,
        ActivityFlowImageRenderer.ColorScheme sourceColorScheme
    ) {
        super(parent);
        this.title = text("ui.swing.dialog.imageexport.title");
        this.board = board;
        this.sourceBlockFilterPanel = sourceBlockFilterPanel;
        this.sourceColorScheme = sourceColorScheme;
        this.formWidth = 500;
        
        this.titleFontButton = new FontButton(parent);
        this.headerFontButton = new FontButton(parent);
        this.leftLabelFontButton = new FontButton(parent);
        this.activityLabelFontButton = new FontButton(parent);
        this.backgroundColorButton = new ColorButton(parent);
        this.titleColorButton = new ColorButton(parent);
        this.headerBorderColorButton = new ColorButton(parent);
        this.headerBackgroundColorButton = new ColorButton(parent);
        this.headerLabelColorButton = new ColorButton(parent);
        this.columnBorderColorButton = new ColorButton(parent);
        this.columnBackgroundColorButton = new ColorButton(parent);
        this.startLineColorButton = new ColorButton(parent);
        this.startLabelColorButton = new ColorButton(parent);
        this.activityBorderColorButton = new ColorButton(parent);
        this.activityBackgroundColorButton = new ColorButton(parent);
        this.activityLabelColorButton = new ColorButton(parent);
        this.activityLabelBackgroundColorButton = new ColorButton(parent);
        this.blockFilterPanel = new BlockFilterPanel(parent, document);
        
        init(true, true, false);
    }

    
    @Override
    protected void build() {
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(tabbedPane, BorderLayout.CENTER);
        
        int fullWidth = formWidth - 50;
        
        {
            commonTabPanel.setLayout(new BorderLayout());
            
            // XXX
            commonTabPanel.setPreferredSize(new Dimension(fullWidth - 30, 100));
            
            tabbedPane.addMultilingualTab("ui.swing.dialog.imageexport.tab.basic", commonTabPanel);
            
            JPanel commonInnerPanel = new JPanel();
            commonInnerPanel.setLayout(new BoxLayout(commonInnerPanel, BoxLayout.PAGE_AXIS));
            commonTabPanel.add(commonInnerPanel, BorderLayout.PAGE_START);
            
            addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.title"), titleField, fullWidth, 140);

            titleAlignComboBox.addMultilingualItem("ui.swing.dialog.imageexport.tab.basic.titlealign.left", "left");
            titleAlignComboBox.addMultilingualItem("ui.swing.dialog.imageexport.tab.basic.titlealign.center", "center");
            titleAlignComboBox.addMultilingualItem("ui.swing.dialog.imageexport.tab.basic.titlealign.right", "right");
            addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.titlealign"), titleAlignComboBox, fullWidth, 140);
            
            FileFilter titleImageFileChooserImageFilter = new AutoExtensionFileChooser.ImageFilter();
            titleImageFileChooser.setAcceptAllFileFilterUsed(true);
            titleImageFileChooser.addChoosableFileFilter(titleImageFileChooserImageFilter);
            titleImageFileChooser.setFileFilter(titleImageFileChooserImageFilter);
            titleImageFileField.setFileChooser(titleImageFileChooser);
            titleImageFileField.setDialogType(JFileChooser.OPEN_DIALOG);
            addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.titleimage"), titleImageFileField, fullWidth, 140);
            
            titleImageAlignComboBox.addMultilingualItem("ui.swing.dialog.imageexport.tab.basic.titleimagealign.left", "left");
            titleImageAlignComboBox.addMultilingualItem("ui.swing.dialog.imageexport.tab.basic.titleimagealign.right", "right");
            addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.titleimagealign"), titleImageAlignComboBox, fullWidth, 140);
            
            titleImageSizingCheckBox.setLanguagePath("ui.swing.dialog.imageexport.tab.basic.titleimagesizing.fit_to_text");
            addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.titleimagesizing"), titleImageSizingCheckBox, fullWidth, 140);
            
            titleImageScaleSpinner.setPreferredSize(new Dimension(70, 25));
            JPanel titleImageScaleSpinnerPanel = new JPanel(new BorderLayout());
            titleImageScaleSpinnerPanel.add(titleImageScaleSpinner, BorderLayout.LINE_START);
            addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.titleimagescale"), titleImageScaleSpinnerPanel, fullWidth, 140);
            
            @SuppressWarnings("unchecked")
            Labeled.PairWrapper<Time>[] dayLabeledWrappers = new Labeled.PairWrapper[]{
                new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.monday")), new Time(Time.MONDAY)),
                new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.tuesday")), new Time(Time.TUESDAY)),
                new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.wednesday")), new Time(Time.WEDNESDAY)),
                new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.thursday")), new Time(Time.THURSDAY)),
                new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.friday")), new Time(Time.FRIDAY)),
                new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.saturday")), new Time(Time.SATURDAY)),
                new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.sunday")), new Time(Time.SUNDAY)),
            };
            
            {
                JPanel startDayOuterPanel = new JPanel(new BorderLayout());
                
                JPanel startDayPanel = new JPanel();
                startDayPanel.setLayout(new BoxLayout(startDayPanel, BoxLayout.LINE_AXIS));
                startDayOuterPanel.add(startDayPanel, BorderLayout.LINE_START);
                
                startDayWeekSpinner = new JSpinner();
                SpinnerNumberModel weekSpinnerModel = new SpinnerNumberModel();
                weekSpinnerModel.setMinimum(1);
                startDayWeekSpinner.setModel(weekSpinnerModel);
                JSpinner.NumberEditor weekSpinnerEditor = new JSpinner.NumberEditor(startDayWeekSpinner, text("ui.swing.dialog.timeedit.week_format"));
                startDayWeekSpinner.setEditor(weekSpinnerEditor);
                startDayPanel.add(startDayWeekSpinner);
                
                startDayCombo = new JComboBox<Labeled.PairWrapper<Time>>(dayLabeledWrappers);
                startDayPanel.add(startDayCombo);
                
                addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.requiredfirstday"), startDayOuterPanel, fullWidth, 140);
            }

            {
                JPanel endDayOuterPanel = new JPanel(new BorderLayout());
                
                JPanel endDayPanel = new JPanel();
                endDayPanel.setLayout(new BoxLayout(endDayPanel, BoxLayout.LINE_AXIS));
                endDayOuterPanel.add(endDayPanel, BorderLayout.LINE_START);
                
                endDayWeekSpinner = new JSpinner();
                SpinnerNumberModel weekSpinnerModel = new SpinnerNumberModel();
                weekSpinnerModel.setMinimum(1);
                endDayWeekSpinner.setModel(weekSpinnerModel);
                JSpinner.NumberEditor weekSpinnerEditor = new JSpinner.NumberEditor(endDayWeekSpinner, text("ui.swing.dialog.timeedit.week_format"));
                endDayWeekSpinner.setEditor(weekSpinnerEditor);
                endDayPanel.add(endDayWeekSpinner);
                
                endDayCombo = new JComboBox<Labeled.PairWrapper<Time>>(dayLabeledWrappers);
                endDayPanel.add(endDayCombo);
                
                addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.requiredlasttday"), endDayOuterPanel, fullWidth, 140);
            }
            
            {
                JPanel startTimeOuterPanel = new JPanel(new BorderLayout());
                
                JPanel startTimePanel = new JPanel();
                startTimePanel.setLayout(new BoxLayout(startTimePanel, BoxLayout.LINE_AXIS));
                startTimeOuterPanel.add(startTimePanel, BorderLayout.LINE_START);

                startTimeHourSpinner = createTimeSpinner(0, 23);
                startTimePanel.add(startTimeHourSpinner);

                startTimePanel.add(new JLabel(":"));
                
                startTimeMinuteSpinner = createTimeSpinner(0, 59);
                startTimePanel.add(startTimeMinuteSpinner);

                startTimePanel.add(new JLabel(":"));
                
                startTimeSecondSpinner = createTimeSpinner(0, 59);
                startTimePanel.add(startTimeSecondSpinner);
                
                addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.requiredstarttime"), startTimePanel, fullWidth, 140);
            }

            {
                JPanel endTimeOuterPanel = new JPanel(new BorderLayout());
                
                JPanel endTimePanel = new JPanel();
                endTimePanel.setLayout(new BoxLayout(endTimePanel, BoxLayout.LINE_AXIS));
                endTimeOuterPanel.add(endTimePanel, BorderLayout.LINE_START);

                endTimeHourSpinner = createTimeSpinner(0, 23);
                endTimePanel.add(endTimeHourSpinner);
                
                endTimePanel.add(new JLabel(":"));
                
                endTimeMinuteSpinner = createTimeSpinner(0, 59);
                endTimePanel.add(endTimeMinuteSpinner);

                endTimePanel.add(new JLabel(":"));
                
                endTimeSecondSpinner = createTimeSpinner(0, 59);
                endTimePanel.add(endTimeSecondSpinner);
                
                addRowTo(commonInnerPanel, text("ui.swing.dialog.imageexport.tab.basic.requiredendtime"), endTimePanel, fullWidth, 140);
            }
        }
        
        {
            int moreLayoutSettingsLabelWidth = 130;
            int moreLayoutSettingsColumnWidth = (fullWidth / 2) - 30;
            int moreLayoutSettingsColumnSpace = 20;
            
            layoutTabPanel.setLayout(new BorderLayout());
            
            // XXX
            layoutTabPanel.setPreferredSize(new Dimension(fullWidth - 30, 100));

            tabbedPane.addMultilingualTab("ui.swing.dialog.imageexport.tab.sizes", layoutTabPanel);
            
            JPanel layoutInnerPanel = new JPanel();
            layoutInnerPanel.setLayout(new BoxLayout(layoutInnerPanel, BoxLayout.PAGE_AXIS));
            layoutTabPanel.add(layoutInnerPanel, BorderLayout.PAGE_START);
            
            final JPanel moreLayoutSettingsPanel = createSubPanel();
            moreLayoutSettingsPanel.setLayout(new BoxLayout(moreLayoutSettingsPanel, BoxLayout.LINE_AXIS));
            addRowTo(layoutInnerPanel, moreLayoutSettingsPanel, fullWidth - 50);
            
            JPanel moreLayoutSettingsLeftPanel = new JPanel();
            moreLayoutSettingsLeftPanel.setLayout(new BoxLayout(moreLayoutSettingsLeftPanel, BoxLayout.PAGE_AXIS));
            moreLayoutSettingsPanel.add(moreLayoutSettingsLeftPanel);
            
            JPanel moreLayoutSettingsRightPanel = new JPanel();
            moreLayoutSettingsRightPanel.setLayout(new BoxLayout(moreLayoutSettingsRightPanel, BoxLayout.PAGE_AXIS));
            moreLayoutSettingsRightPanel.setBorder(new EmptyBorder(0, moreLayoutSettingsColumnSpace, 0, 0));
            moreLayoutSettingsPanel.add(moreLayoutSettingsRightPanel);
            
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.topmargin"), topMarginSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.leftmargin"), leftMarginSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.bottommargin"), bottomMarginSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.rightmargin"), rightMarginSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.titleimagespace"), titleImageSpaceSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.titlebelowspace"), titleBelowSpaceSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.leftlabelswidth"), leftLabelsWidthSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.leftlabelabovespace"), leftLabelAboveSpaceSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.secondsperpixel"), secondsPerPixelSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.activitytoppad"), activityTopPadSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsLeftPanel, text("ui.swing.dialog.imageexport.tab.sizes.activityleftpad"), activityLeftPadSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);

            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.headerheight"), headerHeightSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.headerbelowspace"), headerBelowSpaceSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columnsspace"), columnsSpaceSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columntoppad"), columnTopPadSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columnleftpad"), columnLeftPadSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columnbottompad"), columnBottomPadSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columnrightpad"), columnRightPadSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columnminimuminnerwidth"), columnMinimumInnerWidthSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columnmaximuminnerwidth"), columnMaximuminnerWidthSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columnactivityoptimalwidth"), columnActivityOptimalWidthSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
            addRowTo(moreLayoutSettingsRightPanel, text("ui.swing.dialog.imageexport.tab.sizes.columnactivitiesspace"), columnActivitySpaceSpinner, moreLayoutSettingsColumnWidth, moreLayoutSettingsLabelWidth);
        }
        
        {
            fontsTabPanel.setLayout(new BorderLayout());
            
            tabbedPane.addMultilingualTab("ui.swing.dialog.imageexport.tab.fonts", fontsTabPanel);

            JPanel fontsInnerPanel = new JPanel();
            fontsInnerPanel.setLayout(new BoxLayout(fontsInnerPanel, BoxLayout.PAGE_AXIS));
            fontsTabPanel.add(fontsInnerPanel, BorderLayout.PAGE_START);

            // XXX
            Dimension buttonSize = new Dimension(250, 30);
            
            titleFontButton.setMinimumSize(buttonSize);
            titleFontButton.setPreferredSize(buttonSize);
            titleFontButton.setMaximumSize(buttonSize);
            addRowTo(fontsInnerPanel, text("ui.swing.dialog.imageexport.tab.fonts.title"), titleFontButton, fullWidth, 170);

            headerFontButton.setMinimumSize(buttonSize);
            headerFontButton.setPreferredSize(buttonSize);
            headerFontButton.setMaximumSize(buttonSize);
            addRowTo(fontsInnerPanel, text("ui.swing.dialog.imageexport.tab.fonts.header"), headerFontButton, fullWidth, 170);

            leftLabelFontButton.setMinimumSize(buttonSize);
            leftLabelFontButton.setPreferredSize(buttonSize);
            leftLabelFontButton.setMaximumSize(buttonSize);
            addRowTo(fontsInnerPanel, text("ui.swing.dialog.imageexport.tab.fonts.leftlabels"), leftLabelFontButton, fullWidth, 170);

            activityLabelFontButton.setMinimumSize(buttonSize);
            activityLabelFontButton.setPreferredSize(buttonSize);
            activityLabelFontButton.setMaximumSize(buttonSize);
            addRowTo(fontsInnerPanel, text("ui.swing.dialog.imageexport.tab.fonts.activitylabels"), activityLabelFontButton, fullWidth, 170);
        }
        
        {
            colorsTabPanel.setLayout(new BorderLayout());
            
            tabbedPane.addMultilingualTab("ui.swing.dialog.imageexport.tab.colors", colorsTabPanel);
            
            JPanel colorsInnerPanel = new JPanel();
            colorsInnerPanel.setLayout(new BoxLayout(colorsInnerPanel, BoxLayout.PAGE_AXIS));
            colorsTabPanel.add(colorsInnerPanel, BorderLayout.PAGE_START);

            ButtonGroup colorsButtonGroup = new ButtonGroup();
            colorsButtonGroup.add(whiteBlackColorsRadioButton);
            colorsButtonGroup.add(customColorsRadioButton);
            
            whiteBlackColorsRadioButton.setLanguagePath("ui.swing.dialog.imageexport.tab.colors.whiteblack");
            addRowTo(colorsInnerPanel, whiteBlackColorsRadioButton, fullWidth);
            
            customColorsRadioButton.setLanguagePath("ui.swing.dialog.imageexport.tab.colors.custom");
            addRowTo(colorsInnerPanel, customColorsRadioButton, fullWidth);

            {
                JPanel customColorsSubPanel = createSubPanel();
                addRowTo(colorsInnerPanel, customColorsSubPanel, fullWidth);

                JPanel loadColorSchemePanel = new JPanel();
                loadColorSchemePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
                addRowTo(customColorsSubPanel, loadColorSchemePanel, fullWidth - 10);
                
                for (Pair<String, ActivityFlowImageRenderer.ColorScheme> pair: new BoardImageColorSchemeListBuilder().getColorSchemes()) {
                    colorSchemeComboBox.addMultilingualItem(pair.getLeft(), pair.getRight());
                }
                loadColorSchemePanel.add(colorSchemeComboBox);

                colorSchemeButton.setLanguagePath("ui.swing.dialog.imageexport.tab.colors.custom.colorschemebutton");
                loadColorSchemePanel.add(colorSchemeButton);
                
                colorSchemeButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        ActivityFlowImageRenderer.ColorScheme colorScheme = colorSchemeComboBox.getSelectedData();
                        backgroundColorButton.setSelectedAwtColor(colorScheme.background);
                        titleColorButton.setSelectedColor(ColorUtil.fromAwtColor(colorScheme.background).getBlackWhiteContrastColor());
                        headerBorderColorButton.setSelectedAwtColor(colorScheme.headerBorder);
                        headerBackgroundColorButton.setSelectedAwtColor(colorScheme.headerBackground);
                        headerLabelColorButton.setSelectedAwtColor(colorScheme.headerLabel);
                        columnBorderColorButton.setSelectedAwtColor(colorScheme.columnBorder);
                        columnBackgroundColorButton.setSelectedAwtColor(colorScheme.columnBackground);
                        startLineColorButton.setSelectedAwtColor(colorScheme.startLine);
                        startLabelColorButton.setSelectedAwtColor(colorScheme.startLabel);
                        activityBorderColorButton.setSelectedAwtColor(colorScheme.activityBorder);
                        activityBackgroundColorButton.setSelectedAwtColor(colorScheme.activityBackground);
                        activityLabelColorButton.setSelectedAwtColor(colorScheme.activityLabel);
                        activityLabelBackgroundColorButton.setSelectedAwtColor(colorScheme.activityLabelBackground);
                        applyActivityColorsCheckbox.setSelected(colorScheme.applyActivityColors);
                        colorAspectComboBox.setEnabled(colorScheme.applyActivityColors);
                    }
                    
                });
                
                {
                    final JPanel moreColorsPanel = new JPanel();
                    moreColorsPanel.setLayout(new BoxLayout(moreColorsPanel, BoxLayout.LINE_AXIS));
                    addRowTo(customColorsSubPanel, moreColorsPanel, fullWidth - 50);
                    
                    JPanel moreColorsLeftPanel = new JPanel();
                    moreColorsLeftPanel.setLayout(new BoxLayout(moreColorsLeftPanel, BoxLayout.PAGE_AXIS));
                    moreColorsPanel.add(moreColorsLeftPanel);
                    
                    JPanel moreColorsRightPanel = new JPanel();
                    moreColorsRightPanel.setLayout(new BoxLayout(moreColorsRightPanel, BoxLayout.PAGE_AXIS));
                    moreColorsPanel.add(moreColorsRightPanel);
                    
                    int moreColorsLabelWidth = 140;
                    int moreColorsColumnWidth = (fullWidth / 2) - 30;
                    
                    JLabel backgroundLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.background");
                    moreColorsLabels.add(backgroundLabel);
                    addRowTo(moreColorsLeftPanel, backgroundLabel, backgroundColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel titleLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.title");
                    moreColorsLabels.add(titleLabel);
                    addRowTo(moreColorsLeftPanel, titleLabel, titleColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel headerBorderLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.headerborder");
                    moreColorsLabels.add(headerBorderLabel);
                    addRowTo(moreColorsLeftPanel, headerBorderLabel, headerBorderColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel headerBackgroundLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.headerbackground");
                    moreColorsLabels.add(headerBackgroundLabel);
                    addRowTo(moreColorsLeftPanel, headerBackgroundLabel, headerBackgroundColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel headerLabelLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.headerlabel");
                    moreColorsLabels.add(headerLabelLabel);
                    addRowTo(moreColorsLeftPanel, headerLabelLabel, headerLabelColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel columnBorderLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.columnborder");
                    moreColorsLabels.add(columnBorderLabel);
                    addRowTo(moreColorsLeftPanel, columnBorderLabel, columnBorderColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel columnBackgroundLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.columnbackground");
                    moreColorsLabels.add(columnBackgroundLabel);
                    addRowTo(moreColorsLeftPanel, columnBackgroundLabel, columnBackgroundColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    

                    
                    JLabel startLineLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.startline");
                    moreColorsLabels.add(startLineLabel);
                    addRowTo(moreColorsRightPanel, startLineLabel, startLineColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel startLabelLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.startlabel");
                    moreColorsLabels.add(startLabelLabel);
                    addRowTo(moreColorsRightPanel, startLabelLabel, startLabelColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel activityBorderLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.activityborder");
                    moreColorsLabels.add(activityBorderLabel);
                    addRowTo(moreColorsRightPanel, activityBorderLabel, activityBorderColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel activityBackgroundLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.activitybackground");
                    moreColorsLabels.add(activityBackgroundLabel);
                    addRowTo(moreColorsRightPanel, activityBackgroundLabel, activityBackgroundColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel activityLabelLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.activitylabel");
                    moreColorsLabels.add(activityLabelLabel);
                    addRowTo(moreColorsRightPanel, activityLabelLabel, activityLabelColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    JLabel activityLabelBackgroundLabel = new MultilingualLabel("ui.swing.dialog.imageexport.tab.colors.custom.activitylabelbackground");
                    moreColorsLabels.add(activityLabelBackgroundLabel);
                    addRowTo(moreColorsRightPanel, activityLabelBackgroundLabel, activityLabelBackgroundColorButton, moreColorsColumnWidth, moreColorsLabelWidth);
                    
                    addRowTo(moreColorsRightPanel, "", new JLabel(""), moreColorsColumnWidth, moreColorsLabelWidth);
                }

                colorAspectLabel.setLanguagePath("ui.swing.dialog.imageexport.tab.colors.custom.applyactivitycolors.aspect");
                
                applyActivityColorsCheckbox.setLanguagePath("ui.swing.dialog.imageexport.tab.colors.custom.applyactivitycolors");
                addRowTo(customColorsSubPanel, applyActivityColorsCheckbox, fullWidth - 50);
                
                JPanel activityColorAspectSubPanel = createSubPanel();
                addRowTo(customColorsSubPanel, activityColorAspectSubPanel, fullWidth - 50);
                
                colorAspectComboBox.addMultilingualItem("core.aspect.type.subject", Tag.Type.SUBJECT);
                colorAspectComboBox.addMultilingualItem("core.aspect.type.language", Tag.Type.LANGUAGE);
                colorAspectComboBox.addMultilingualItem("core.aspect.type.other_tag", Tag.Type.OTHER);
                colorAspectComboBox.addMultilingualItem("core.aspect.type.class", Resource.Type.CLASS);
                colorAspectComboBox.addMultilingualItem("core.aspect.type.person", Resource.Type.PERSON);
                colorAspectComboBox.addMultilingualItem("core.aspect.type.locale", Resource.Type.LOCALE);
                colorAspectComboBox.addMultilingualItem("core.aspect.type.object", Resource.Type.OBJECT);
                colorAspectComboBox.addMultilingualItem("core.aspect.type.other_resource", Resource.Type.OTHER);
                addRowTo(activityColorAspectSubPanel, colorAspectLabel, colorAspectComboBox, fullWidth - 50, 100);
                
            }
            
        }

        {
            filterTabPanel.setLayout(new BorderLayout());
            
            tabbedPane.addMultilingualTab("ui.swing.dialog.imageexport.tab.filter", filterTabPanel);
            
            JPanel filterInnerPanel = new JPanel();
            filterInnerPanel.setLayout(new BoxLayout(filterInnerPanel, BoxLayout.PAGE_AXIS));
            filterTabPanel.add(filterInnerPanel, BorderLayout.PAGE_START);
            
            blockFilterPanel.setColorAspectVisible(false);
            addRowTo(filterInnerPanel, blockFilterPanel, fullWidth);
        }

        {
            ButtonGroup saveTypeButtonGroup = new ButtonGroup();
            saveTypeButtonGroup.add(saveSingleFileRadioButton);
            saveTypeButtonGroup.add(saveMultiFileRadioButton);
            
            saveTabPanel.setLayout(new BorderLayout());
            
            tabbedPane.addMultilingualTab("ui.swing.dialog.imageexport.tab.save", saveTabPanel);
            
            JPanel saveInnerPanel = new JPanel();
            saveInnerPanel.setLayout(new BoxLayout(saveInnerPanel, BoxLayout.PAGE_AXIS));
            saveTabPanel.add(saveInnerPanel, BorderLayout.PAGE_START);
            

            {
                saveSingleFileRadioButton.setLanguagePath("ui.swing.dialog.imageexport.tab.save.singlefile");
                addRowTo(saveInnerPanel, saveSingleFileRadioButton, fullWidth);

                final JPanel singleFileSubPanel = createSubPanel();
                addRowTo(saveInnerPanel, singleFileSubPanel, fullWidth);
                
                singleFileLabel.setLanguagePath("ui.swing.dialog.imageexport.tab.save.singlefile.file");
                
                FileFilter singleFileChooserFilter = new AutoExtensionFileChooser.ImageFilter();
                singleFileChooser.addChoosableFileFilter(singleFileChooserFilter);
                singleFileChooser.setFileFilter(singleFileChooserFilter);
                singleFileField.setFileChooser(singleFileChooser);
                singleFileField.setDialogType(JFileChooser.SAVE_DIALOG);
                addRowTo(singleFileSubPanel, singleFileLabel, singleFileField, fullWidth, 120);
            }

            {
                saveMultiFileRadioButton.setLanguagePath("ui.swing.dialog.imageexport.tab.save.multifile");
                addRowTo(saveInnerPanel, saveMultiFileRadioButton, fullWidth);

                final JPanel multiFileSubPanel = createSubPanel();
                addRowTo(saveInnerPanel, multiFileSubPanel, fullWidth);

                {
                    aspectLabel.setLanguagePath("ui.swing.dialog.imageexport.tab.save.multifile.aspects");
                    addRowTo(multiFileSubPanel, aspectLabel, fullWidth);
                    JPanel aspectSubPanel = createSubPanel();
                    
                    addRowTo(multiFileSubPanel, aspectSubPanel, fullWidth);
                    
                    extractClassesCheckBox.setLanguagePath("core.resource.type.class");
                    addRowTo(aspectSubPanel, extractClassesCheckBox, fullWidth);
                    
                    extractPersonCheckBox.setLanguagePath("core.resource.type.person");
                    addRowTo(aspectSubPanel, extractPersonCheckBox, fullWidth);
                    
                    extractLocaleCheckBox.setLanguagePath("core.resource.type.locale");
                    addRowTo(aspectSubPanel, extractLocaleCheckBox, fullWidth);
                }
                
                splitByPeriodsCheckBox.setLanguagePath("ui.swing.dialog.imageexport.tab.save.multifile.perperiod");
                addRowTo(multiFileSubPanel, splitByPeriodsCheckBox, fullWidth);

                filenameTemplateLabel.setLanguagePath("ui.swing.dialog.imageexport.tab.save.multifile.filenametemplate");
                addRowTo(multiFileSubPanel, filenameTemplateLabel, filenameTemplateField, fullWidth, 130);

                ButtonGroup saveMultiTargetButtonGroup = new ButtonGroup();
                saveMultiTargetButtonGroup.add(saveMultiTargetFolderRadioButton);
                saveMultiTargetButtonGroup.add(saveMultiTargetEmailRadioButton);
                
                saveMultiTargetFolderRadioButton.setLanguagePath("ui.swing.dialog.imageexport.tab.save.multifile.into_folder");
                
                addRowTo(multiFileSubPanel, saveMultiTargetFolderRadioButton, fullWidth);
                final JPanel multiFileToFolderSubPanel = createSubPanel();

                addRowTo(multiFileSubPanel, multiFileToFolderSubPanel, fullWidth);

                multiFileFolderLabel.setLanguagePath("ui.swing.dialog.imageexport.tab.save.multifile.folder");
                
                multiFileFolderFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                multiFileFolderFileChooser.setAcceptAllFileFilterUsed(false);
                multiFileFolderFileField.setFileChooser(multiFileFolderFileChooser);
                multiFileFolderFileField.setDialogType(JFileChooser.SAVE_DIALOG);
                addRowTo(multiFileToFolderSubPanel, multiFileFolderLabel, multiFileFolderFileField, fullWidth, 130);

                saveMultiTargetEmailRadioButton.setLanguagePath("ui.swing.dialog.imageexport.tab.save.multifile.into_email");
                addRowTo(multiFileSubPanel, saveMultiTargetEmailRadioButton, fullWidth);
            }
        }
        
        {
            emailTabPanel.setLayout(new BorderLayout());
            tabbedPane.addMultilingualTab("ui.swing.dialog.imageexport.tab.email", emailTabPanel);

            JPanel emailInnerPanel = new JPanel();
            emailInnerPanel.setLayout(new BoxLayout(emailInnerPanel, BoxLayout.PAGE_AXIS));
            emailTabPanel.add(emailInnerPanel, BorderLayout.PAGE_START);
            
            addRowTo(emailInnerPanel, text("ui.swing.dialog.imageexport.tab.email.subject"), emailSubjectField, fullWidth, leftWidth);
            
            emailContentTextArea.setLineWrap(true);
            JScrollPane emailContentTextAreaScrollPane = new JScrollPane(emailContentTextArea);
            emailContentTextAreaScrollPane.setPreferredSize(new Dimension(100, 400));
            addRowTo(emailInnerPanel, text("ui.swing.dialog.imageexport.tab.email.message"), emailContentTextAreaScrollPane, fullWidth, leftWidth);
            
            emailRenderAllImagesCheckBox.setLanguagePath("ui.swing.dialog.imageexport.tab.email.render_all_images");
            addRowTo(emailInnerPanel, emailRenderAllImagesCheckBox, fullWidth);
        }

        {
            previewTabPanel.setLayout(new BorderLayout());
            
            tabbedPane.addMultilingualTab("ui.swing.dialog.imageexport.tab.preview", previewTabPanel);
        }
        
        pack();
    }

    @Override
    protected void load() {
        {
            titleField.setText(board.getLabel());
            titleImageAlignComboBox.setSelectedData("left");
            titleImageSizingCheckBox.setSelected(true);
            titleImageScaleSpinner.setValue(new BigDecimal("1"));
            startDayWeekSpinner.setValue(1);
            startDayCombo.setSelectedIndex(0);
            endDayWeekSpinner.setValue(1);
            endDayCombo.setSelectedIndex(4);
            startTimeHourSpinner.setValue(8);
            startTimeMinuteSpinner.setValue(0);
            startTimeSecondSpinner.setValue(0);
            endTimeHourSpinner.setValue(15);
            endTimeMinuteSpinner.setValue(0);
            endTimeSecondSpinner.setValue(0);
        }
        
        {
            topMarginSpinner.setValue(50);
            leftMarginSpinner.setValue(50);
            bottomMarginSpinner.setValue(50);
            rightMarginSpinner.setValue(50);
            titleImageSpaceSpinner.setValue(10);
            titleBelowSpaceSpinner.setValue(40);
            leftLabelsWidthSpinner.setValue(50);
            leftLabelAboveSpaceSpinner.setValue(1);
            secondsPerPixelSpinner.setValue(60);
            activityTopPadSpinner.setValue(3);
            activityLeftPadSpinner.setValue(3);
            headerHeightSpinner.setValue(50);
            headerBelowSpaceSpinner.setValue(10);
            columnsSpaceSpinner.setValue(10);
            columnTopPadSpinner.setValue(10);
            columnLeftPadSpinner.setValue(10);
            columnBottomPadSpinner.setValue(10);
            columnRightPadSpinner.setValue(10);
            columnMinimumInnerWidthSpinner.setValue(150);
            columnMaximuminnerWidthSpinner.setValue(0);
            columnActivityOptimalWidthSpinner.setValue(100);
            columnActivitySpaceSpinner.setValue(10);
        }
        
        {
            titleFontButton.setSelectedFont(new Font(Font.SANS_SERIF, Font.BOLD, 48));
            headerFontButton.setSelectedFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
            leftLabelFontButton.setSelectedFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
            activityLabelFontButton.setSelectedFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
        }
        
        {
            whiteBlackColorsRadioButton.setSelected(true);
            applyActivityColorsCheckbox.setSelected(sourceColorScheme.applyActivityColors);
            colorAspectComboBox.setSelectedData(sourceBlockFilterPanel.getColorAspect());
            backgroundColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.background));
            titleColorButton.setSelectedColor(Color.BLACK);
            headerBorderColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.headerBorder));
            headerBackgroundColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.headerBackground));
            headerLabelColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.headerLabel));
            columnBorderColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.columnBorder));
            columnBackgroundColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.columnBackground));
            startLineColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.startLine));
            startLabelColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.startLabel));
            activityBorderColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.activityBorder));
            activityBackgroundColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.activityBackground));
            activityLabelColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.activityLabel));
            activityLabelBackgroundColorButton.setSelectedColor(ColorUtil.fromAwtColor(sourceColorScheme.activityLabelBackground));
        }
        
        {
            blockFilterPanel.loadFrom(sourceBlockFilterPanel);
        }
        
        {
            saveSingleFileRadioButton.setSelected(true);
            splitByPeriodsCheckBox.setSelected(true);
            titleAlignComboBox.setSelectedData("center");
            titleImageAlignComboBox.setSelectedData("left");
            extractClassesCheckBox.setSelected(true);
            saveMultiTargetFolderRadioButton.setSelected(true);
            filenameTemplateField.setText("%s.png");
        }
        
        {
            emailSubjectField.setText(text("ui.swing.dialog.imageexport.tab.email.subject.default"));
            emailContentTextArea.setText(text("ui.swing.dialog.imageexport.tab.email.message.default"));
        }
    }

    @Override
    protected void afterLoad() {
        Runnable applyActivityColorsCheckboxAction = new Runnable() {

            @Override
            public void run() {
                boolean enabled = customColorsRadioButton.isSelected() && applyActivityColorsCheckbox.isSelected();
                colorAspectLabel.setEnabled(enabled);
                colorAspectComboBox.setEnabled(enabled);
            }
            
        };
        applyActivityColorsCheckbox.addItemListener(new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                applyActivityColorsCheckboxAction.run();
            }
            
        });
        applyActivityColorsCheckboxAction.run();

        
        Runnable customColorsRadioButtonAction = new Runnable() {
            
            @Override
            public void run() {
                boolean enabled = customColorsRadioButton.isSelected();
                for (JLabel moreColorsLabel: moreColorsLabels) {
                    moreColorsLabel.setEnabled(enabled);
                }
                colorSchemeComboBox.setEnabled(enabled);
                colorSchemeButton.setEnabled(enabled);
                colorAspectLabel.setEnabled(enabled && applyActivityColorsCheckbox.isSelected());
                colorAspectComboBox.setEnabled(enabled && applyActivityColorsCheckbox.isSelected());
                backgroundColorButton.setEnabled(enabled);
                titleColorButton.setEnabled(enabled);
                headerBorderColorButton.setEnabled(enabled);
                headerBackgroundColorButton.setEnabled(enabled);
                headerLabelColorButton.setEnabled(enabled);
                columnBorderColorButton.setEnabled(enabled);
                columnBackgroundColorButton.setEnabled(enabled);
                startLineColorButton.setEnabled(enabled);
                startLabelColorButton.setEnabled(enabled);
                activityBorderColorButton.setEnabled(enabled);
                activityBackgroundColorButton.setEnabled(enabled);
                activityLabelColorButton.setEnabled(enabled);
                activityLabelBackgroundColorButton.setEnabled(enabled);
                applyActivityColorsCheckbox.setEnabled(enabled);
            }
            
        };
        customColorsRadioButton.addItemListener(new RunnableItemListener(customColorsRadioButtonAction));
        customColorsRadioButtonAction.run();
        

        Runnable saveSingleFileRadioButtonAction = new Runnable() {
            
            @Override
            public void run() {
                boolean enabled = saveSingleFileRadioButton.isSelected();
                singleFileLabel.setEnabled(enabled);
                singleFileField.setEnabled(enabled);
            }
            
        };
        saveSingleFileRadioButton.addItemListener(new RunnableItemListener(saveSingleFileRadioButtonAction));
        saveSingleFileRadioButtonAction.run();
        
        
        Runnable saveMultiTargetFolderRadioButtonAction = new Runnable() {
            
            @Override
            public void run() {
                boolean enabled = saveMultiFileRadioButton.isSelected();
                boolean saveToFolder = saveMultiTargetFolderRadioButton.isSelected();
                multiFileFolderLabel.setEnabled(enabled && saveToFolder);
                multiFileFolderFileField.setEnabled(enabled && saveToFolder);
            }
            
        };
        saveMultiTargetFolderRadioButton.addItemListener(new RunnableItemListener(saveMultiTargetFolderRadioButtonAction));
        
        
        Runnable saveMultiTargetEmailRadioButtonAction = new Runnable() {
            
            @Override
            public void run() {
                boolean enabled = saveMultiFileRadioButton.isSelected();
                boolean saveToEmail = saveMultiTargetEmailRadioButton.isSelected();
                tabbedPane.setEnabledAt(tabbedPane.indexOfComponent(emailTabPanel), enabled && saveToEmail);
            }
            
        };
        saveMultiTargetEmailRadioButton.addItemListener(new RunnableItemListener(saveMultiTargetEmailRadioButtonAction));
        

        Runnable saveMultiFileRadioButtonAction = new Runnable() {
            
            @Override
            public void run() {
                boolean enabled = saveMultiFileRadioButton.isSelected();
                aspectLabel.setEnabled(enabled);
                extractClassesCheckBox.setEnabled(enabled);
                extractPersonCheckBox.setEnabled(enabled);
                extractLocaleCheckBox.setEnabled(enabled);
                splitByPeriodsCheckBox.setEnabled(enabled);
                filenameTemplateLabel.setEnabled(enabled);
                filenameTemplateField.setEnabled(enabled);
                saveMultiTargetFolderRadioButton.setEnabled(enabled);
                saveMultiTargetEmailRadioButton.setEnabled(enabled);
                
                saveMultiTargetFolderRadioButtonAction.run();
                saveMultiTargetEmailRadioButtonAction.run();
            }
            
        };
        saveMultiFileRadioButton.addItemListener(new RunnableItemListener(saveMultiFileRadioButtonAction));
        saveMultiFileRadioButtonAction.run();
        
        
        Runnable tabbedPaneChangeAction = new Runnable() {
            
            @Override
            public void run() {
                if (tabbedPane.getSelectedComponent() == previewTabPanel) {
                    
                    // XXX
                    Component focusOwner = ImageExportDialog.this.getFocusOwner();
                    if (focusOwner instanceof JFormattedTextField) {
                        try {
                            ((JFormattedTextField)focusOwner).commitEdit();
                        } catch (ParseException e) {
                        }
                    }
                    
                    previewTabPanel.reload(true);
                }
            }
            
        };
        tabbedPane.addChangeListener(new RunnableChangeListener(tabbedPaneChangeAction));
        tabbedPaneChangeAction.run();
    }

    @Override
    protected void save() {
    }

    @Override
    protected boolean trySave() {
        final GeneralWrapper<Boolean> resultWrapper = new GeneralWrapper<Boolean>();
        SwingWorkerProcessDialog processDialog = new SwingWorkerProcessDialog(parent, text("ui.swing.dialog.imageexport.saving"), true) {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected Worker createWorker() {
                return new Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        resultWrapper.set(trySaveUnwrapped(this));
                    }

                    @Override
                    public void _rollBack() {
                    }

                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        if (command instanceof TerminatedCommand) {
                            closeDialog();
                        }
                    }
                    
                };
            }
            
        };
        processDialog.run();
        return resultWrapper.get();
    }

    protected boolean trySaveUnwrapped(SwingWorkerProcessDialog.Worker worker) {
        boolean sendInEmail = false;
        
        if (saveSingleFileRadioButton.isSelected()) {
            if (titleField.getText().isEmpty()) {
                tabbedPane.setSelectedComponent(commonTabPanel);
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.imageexport.can_not_save.no_title"),
                    text("ui.swing.dialog.imageexport.can_not_save.title"),
                    JOptionPane.WARNING_MESSAGE
                );
                titleField.requestFocusInWindow();
                return false;
            }
            if (singleFileField.isEmpty() && !singleFileField.openDialog()) {
                return false;
            }
        } else if (saveMultiFileRadioButton.isSelected()) {
            if (filenameTemplateField.getText().isEmpty()) {
                tabbedPane.setSelectedComponent(saveTabPanel);
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.imageexport.can_not_save.no_filename_template"),
                    text("ui.swing.dialog.imageexport.can_not_save.title"),
                    JOptionPane.WARNING_MESSAGE
                );
                filenameTemplateField.requestFocusInWindow();
                return false;
            }
            if (saveMultiTargetFolderRadioButton.isSelected()) {
                if (multiFileFolderFileField.isEmpty() && !multiFileFolderFileField.openDialog()) {
                    return false;
                }
            } else if (saveMultiTargetEmailRadioButton.isSelected()) {
                if (emailSubjectField.getText().isEmpty()) {
                    tabbedPane.setSelectedComponent(emailTabPanel);
                    OptionPaneUtil.showMessageDialog(
                        parent,
                        text("ui.swing.dialog.imageexport.can_not_save.no_email_subject"),
                        text("ui.swing.dialog.imageexport.can_not_save.title"),
                        JOptionPane.WARNING_MESSAGE
                    );
                    emailSubjectField.requestFocusInWindow();
                    return false;
                }
                if (emailContentTextArea.getText().isEmpty()) {
                    tabbedPane.setSelectedComponent(emailTabPanel);
                    OptionPaneUtil.showMessageDialog(
                        parent,
                        text("ui.swing.dialog.imageexport.can_not_save.no_email_content"),
                        text("ui.swing.dialog.imageexport.can_not_save.title"),
                        JOptionPane.WARNING_MESSAGE
                    );
                    emailContentTextArea.requestFocusInWindow();
                    return false;
                }
                sendInEmail = true;
            }
        }
        
        List<ExportItem> exportItems = getExportItems();
        
        if (sendInEmail) {
            Map<String, Pair<List<Resource>, List<ExportItem>>> recipientMap = new HashMap<String, Pair<List<Resource>, List<ExportItem>>>();
            for (ExportItem exportItem: exportItems) {
                if (exportItem.associatedResource != null) {
                    String email = exportItem.associatedResource.getEmail();
                    if (!email.isEmpty()) {
                        Pair<List<Resource>, List<ExportItem>> recipientData;
                        if (recipientMap.containsKey(email)) {
                            recipientData = recipientMap.get(email);
                        } else {
                            recipientData = new Pair<List<Resource>, List<ExportItem>>(
                                new ArrayList<Resource>(),
                                new ArrayList<ExportItem>()
                            );
                            recipientMap.put(email, recipientData);
                        }
                        recipientData.getLeft().add(exportItem.associatedResource);
                        recipientData.getRight().add(exportItem);
                    }
                }
            }
            
            if (recipientMap.isEmpty()) {
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.imageexport.can_not_save.no_recipient"),
                    text("ui.swing.dialog.imageexport.can_not_save.title"),
                    JOptionPane.ERROR_MESSAGE
                );
                return false;
            }

            String subject = emailSubjectField.getText();
            String content = emailContentTextArea.getText();
            
            Map<ExportItem, Attachment> itemAttachmentMap = new HashMap<ExportItem, Attachment>();
            if (emailRenderAllImagesCheckBox.isSelected()) {
                for (ExportItem exportItem: exportItems) {
                    BufferedImage bufferedImage = createExportedImage(exportItem.title, exportItem.period, exportItem.activityFilter);
                    itemAttachmentMap.put(exportItem, new ImageAttachment(
                        bufferedImage, exportItem.filename, ImageAttachment.ImageType.AUTO, exportItem.title
                    ));
                }
            } else {
                for (Map.Entry<String, Pair<List<Resource>, List<ExportItem>>> entry: recipientMap.entrySet()) {
                    for (ExportItem exportItem: entry.getValue().getRight()) {
                        if (!itemAttachmentMap.containsKey(exportItem)) {
                            BufferedImage bufferedImage = createExportedImage(exportItem.title, exportItem.period, exportItem.activityFilter);
                            itemAttachmentMap.put(exportItem, new ImageAttachment(
                                bufferedImage, exportItem.filename, ImageAttachment.ImageType.AUTO, exportItem.title
                            ));
                        }
                    }
                }
            }
            
            List<SimpleMessage> messages = new ArrayList<SimpleMessage>();
            for (Map.Entry<String, Pair<List<Resource>, List<ExportItem>>> entry: recipientMap.entrySet()) {
                String email = entry.getKey();
                Pair<List<Resource>, List<ExportItem>> recipientData = entry.getValue();
                List<Resource> recipientResources = recipientData.getLeft();
                List<ExportItem> recipientExportItems = recipientData.getRight();
                
                String recipientName = "";
                for (Resource resource: recipientResources) {
                    if (resource.getType()== Resource.Type.PERSON) {
                        recipientName = resource.getLabel();
                        break;
                    } else if (recipientName.isEmpty()) {
                        recipientName = resource.getLabel();
                    }
                }
                
                List<Attachment> attachments = new ArrayList<Attachment>();
                for (ExportItem exportItem: recipientExportItems) {
                    Attachment attachment = itemAttachmentMap.get(exportItem);
                    attachments.add(attachment);
                }
                
                messages.add(new SimpleMessage(
                    recipientName, email, subject, content, attachments
                ));
            }
            
            Timer timer = new Timer(100, null);
            timer.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    timer.stop();
                    
                    SettingsDialog settingsDialog = new SettingsDialog(parent, SettingsDialog.TAB.EMAIL);
                    settingsDialog.run();
                    
                    EmailSenderDialog emailSenderDialog = new EmailSenderDialog(parent, messages, itemAttachmentMap.values());
                    emailSenderDialog.run();
                }
                
            });
            timer.start();
            
            return true;
        } else {
            double step = 1d / exportItems.size();
            double progress = 0;
            for (ExportItem exportItem: exportItems) {
                BufferedImage exportedImage = createExportedImage(exportItem.title, exportItem.period, exportItem.activityFilter);
                try {
                    File outFile = FileUtil.ensureUnique(exportItem.file);
                    ImageIO.write(exportedImage, exportItem.imageType, outFile);
                } catch (IOException e) {
                    OptionPaneUtil.showMessageDialog(
                        parent,
                        text("ui.swing.dialog.imageexport.save_error.message"),
                        text("ui.swing.dialog.imageexport.save_error.title"),
                        JOptionPane.ERROR_MESSAGE
                    );
                    return false;
                }
                progress += step;
                worker.publishCommand(worker.new SetProgressCommand(progress));
            }
        }
        
        return true;
    }

    private List<ExportItem> getExportItems() {
        List<ExportItem> exportItems = new ArrayList<ExportItem>();

        BlockFilter blockFilter = blockFilterPanel.getBlockFilter();
        Period globalPeriod = blockFilterPanel.getPeriod();
        ActivityFilter activityFilter = blockFilterPanel.getActivityFilter();
        
        if (saveSingleFileRadioButton.isSelected()) {
            String title = titleField.getText();
            File file = singleFileField.getFile();
            String filename = file.getName();
            
            // XXX
            String extension = ((AutoExtensionFileChooser)singleFileField.getFileChooser()).getAutoSelectedExtension();
            
            String imageType = (extension.equals("jpg")) ? "jpeg" : extension;
            exportItems.add(new ExportItem(title, file, filename, imageType, globalPeriod, activityFilter));
        } else if (saveMultiFileRadioButton.isSelected()) {
            File folder = multiFileFolderFileChooser.getSelectedFile();
            String filenameTemplate = filenameTemplateField.getText();
            String imageType = getExtensionImageType(FileUtil.getExtension(filenameTemplate));
            
            Board filteredBoard = board.filter(blockFilter);
            
            List<Resource> resources = new ArrayList<Resource>();
            if (extractClassesCheckBox.isSelected()) {
                List<Resource> classes = new ArrayList<Resource>(
                    filteredBoard.getBlocks().getActivities().getResources(Resource.Type.CLASS)
                );
                Collections.sort(classes, new MultiComparator<Resource>(new Labeled.LabeledComparator()));
                resources.addAll(classes);
            }
            if (extractPersonCheckBox.isSelected()) {
                List<Resource> persons = new ArrayList<Resource>(
                    filteredBoard.getBlocks().getActivities().getResources(Resource.Type.PERSON)
                );
                Collections.sort(persons, new MultiComparator<Resource>(new Labeled.LabeledComparator()));
                resources.addAll(persons);
            }
            if (extractLocaleCheckBox.isSelected()) {
                List<Resource> locales = new ArrayList<Resource>(
                    filteredBoard.getBlocks().getActivities().getResources(Resource.Type.LOCALE)
                );
                Collections.sort(locales, new MultiComparator<Resource>(new Labeled.LabeledComparator()));
                resources.addAll(locales);
            }
            
            if (splitByPeriodsCheckBox.isSelected()) { 
                List<Period> periods = new ArrayList<Period>(filteredBoard.getPeriods());
                for (Resource resource: resources) {
                    for (Period period: periods) {
                        String title = resource.getLabel() + " (" + period.getLabel() + ")";
                        String filename = String.format(filenameTemplate, StringUtil.simplify(title));
                        File file = (folder == null) ? null : FileUtil.directoryFile(folder, filename);
                        ActivityFilter innerActivityFilter = new ActivityFilter.And(activityFilter, new ActivityFilter.HasResource(resource));
                        exportItems.add(new ExportItem(title, file, filename, imageType, period, innerActivityFilter, resource));
                    }
                }
            } else {
                for (Resource resource: resources) {
                    String title = resource.getLabel();
                    String filename = String.format(filenameTemplate, StringUtil.simplify(title));
                    File file = (folder == null) ? null : FileUtil.directoryFile(folder, filename);
                    ActivityFilter innerActivityFilter = new ActivityFilter.And(activityFilter, new ActivityFilter.HasResource(resource));
                    exportItems.add(new ExportItem(title, file, filename, imageType, globalPeriod, innerActivityFilter, resource));
                }
            }
            
        } 
        
        return exportItems;
    }
    
    private String getExtensionImageType(String extension) {
        AutoExtensionFileChooser.ImageFilter imageFilter = new AutoExtensionFileChooser.ImageFilter();
        String result = imageFilter.getDefaultExtension();
        for (String imageExtension: imageFilter.getExtensions()) {
            if (imageExtension.equals(extension)) {
                result = imageExtension;
            }
        }
        if (result == "jpg") {
            result = "jpeg";
        }
        return result;
    }
    
    private BufferedImage createExportedImage(String title, Period period, ActivityFilter activityFilter) {
        int topMargin = (int)(Integer)topMarginSpinner.getValue();
        int bottomMargin = (int)(Integer)bottomMarginSpinner.getValue();
        int leftMargin = (int)(Integer)leftMarginSpinner.getValue();
        int rightMargin = (int)(Integer)rightMarginSpinner.getValue();
        int titleImageSpace = (int)(Integer)titleImageSpaceSpinner.getValue();
        int titleBelowSpace = (int)(Integer)titleBelowSpaceSpinner.getValue();
        java.awt.Color backgroundColor = backgroundColorButton.getSelectedAwtColor();
        
        ActivityFlowImageRenderer renderer = new ActivityFlowImageRenderer();
        
        renderer.setTopPad(0);
        renderer.setBottomPad(bottomMargin);
        renderer.setLeftPad(leftMargin);
        renderer.setRightPad(rightMargin);
        renderer.setLeftLabelsWidth((int)(Integer)leftLabelsWidthSpinner.getValue());
        renderer.setHeaderHeight((int)(Integer)headerHeightSpinner.getValue());
        renderer.setHeaderBelowSpace((int)(Integer)headerBelowSpaceSpinner.getValue());
        renderer.setColumnsSpace((int)(Integer)columnsSpaceSpinner.getValue());
        renderer.setColumnTopPad((int)(Integer)columnTopPadSpinner.getValue());
        renderer.setColumnBottomPad((int)(Integer)columnBottomPadSpinner.getValue());
        renderer.setColumnLeftPad((int)(Integer)columnLeftPadSpinner.getValue());
        renderer.setColumnRightPad((int)(Integer)columnRightPadSpinner.getValue());
        renderer.setColumnMinimumInnerWidth((int)(Integer)columnMinimumInnerWidthSpinner.getValue());
        renderer.setColumnMaximumInnerWidth((int)(Integer)columnMaximuminnerWidthSpinner.getValue());
        renderer.setColumnActivityOptimalWidth((int)(Integer)columnActivityOptimalWidthSpinner.getValue());
        renderer.setColumnActivitiesSpace((int)(Integer)columnActivitySpaceSpinner.getValue());
        renderer.setActivityTopPad((int)(Integer)activityTopPadSpinner.getValue());
        renderer.setActivityLeftPad((int)(Integer)activityLeftPadSpinner.getValue());
        renderer.setLeftLabelAboveSpace((int)(Integer)leftLabelAboveSpaceSpinner.getValue());
        renderer.setSecondsPerPixel((int)(Integer)secondsPerPixelSpinner.getValue());
        renderer.setAlignerStrategy(IntervalFlowAligner.STRATEGY.COMPLEX); // XXX / TODO
        renderer.setHeaderFont(headerFontButton.getSelectedFont());
        renderer.setLeftLabelFont(leftLabelFontButton.getSelectedFont());
        renderer.setActivityLabelFont(activityLabelFontButton.getSelectedFont());
        {
            long startWeek = (int)(Integer)startDayWeekSpinner.getValue() - 1;
            long startDay =  + startDayCombo.getSelectedIndex();
            long reguiredFirstDay = (startWeek * Time.WEEK) + (startDay * Time.DAY);
            renderer.setRequiredFirstDay(reguiredFirstDay);
        }
        {
            long endWeek = (int)(Integer)endDayWeekSpinner.getValue() - 1;
            long endDay = endDayCombo.getSelectedIndex();
            long requiredEndDay = (endWeek * Time.WEEK) + (endDay * Time.DAY);
            renderer.setRequiredLastDay(requiredEndDay);
        }
        {
            long startHour = (int)(Integer)startTimeHourSpinner.getValue();
            long startMinute = (int)(Integer)startTimeMinuteSpinner.getValue();
            long startSecond = (int)(Integer)startTimeSecondSpinner.getValue();
            int requiredStartInDay = (int)((startHour * Time.HOUR) + (startMinute * Time.MINUTE) + startSecond);
            renderer.setRequiredStartInDay(requiredStartInDay);
        }
        {
            long endHour = (int)(Integer)endTimeHourSpinner.getValue();
            long endMinute = (int)(Integer)endTimeMinuteSpinner.getValue();
            long endSecond = (int)(Integer)endTimeSecondSpinner.getValue();
            int requiredEndInDay = (int)((endHour * Time.HOUR) + (endMinute * Time.MINUTE) + endSecond);
            renderer.setRequiredEndInDay(requiredEndInDay);
        }
        
        boolean applyActivityColors = (customColorsRadioButton.isSelected() && applyActivityColorsCheckbox.isSelected());
        if (applyActivityColors) {
            Object colorAspect = colorAspectComboBox.getSelectedData();
            renderer.setColorAspect(colorAspect);
        }
        
        if (customColorsRadioButton.isSelected()) {
            ActivityFlowImageRenderer.ColorScheme colorScheme = new ActivityFlowImageRenderer.ColorScheme();
            colorScheme.applyActivityColors = applyActivityColors;
            colorScheme.background = backgroundColor;
            colorScheme.headerBorder = headerBorderColorButton.getSelectedAwtColor();
            colorScheme.headerBackground = headerBackgroundColorButton.getSelectedAwtColor();
            colorScheme.headerLabel = headerLabelColorButton.getSelectedAwtColor();
            colorScheme.columnBorder = columnBorderColorButton.getSelectedAwtColor();
            colorScheme.columnBackground = columnBackgroundColorButton.getSelectedAwtColor();
            colorScheme.startLine = startLineColorButton.getSelectedAwtColor();
            colorScheme.startLabel = startLabelColorButton.getSelectedAwtColor();
            colorScheme.activityBorder = activityBorderColorButton.getSelectedAwtColor();
            colorScheme.activityBackground = activityBackgroundColorButton.getSelectedAwtColor();
            colorScheme.activityLabel = activityLabelColorButton.getSelectedAwtColor();
            colorScheme.activityLabelBackground = activityLabelBackgroundColorButton.getSelectedAwtColor();
            renderer.setColorScheme(colorScheme);
        }
        
        ActivityFlow baseActivityFlow = period == null ? board.toActivityFlow() : board.toActivityFlow(period);
        ActivityFlow activityFlow = baseActivityFlow.filter(new ActivityFlow.ActivityEntryFilter(activityFilter));
        ActivityFlowImageRenderer.Result result = renderer.render(activityFlow);
        BufferedImage innerImage = result.image;
        
        int fullWidth = innerImage.getWidth();
        int innerHeight = innerImage.getHeight();
        
        Font titleFont = titleFontButton.getSelectedFont();
        FontMetrics titleFontMetrics = getFontMetrics(titleFont);
        int titleTextWidth = titleFontMetrics.stringWidth(title);
        int titleTextHeight = titleFontMetrics.getHeight();
        int titleTextDescent = titleFontMetrics.getDescent();

        Image resizedTitleImage = null;
        int resizedTitleImageWidth = 0;
        int resizedTitleImageHeight = 0;
        
        File titleImageFile = titleImageFileField.getFile();
        if (titleImageFile != null) {
            BufferedImage titleImage = null;
            try {
                titleImage = ImageIO.read(titleImageFile);
            } catch (IOException e) {
            }
            try {
                if (titleImage != null) {
                    int titleImageWidth = titleImage.getWidth();
                    int titleImageHeight = titleImage.getHeight();
                    boolean fitToText = titleImageSizingCheckBox.isSelected();
                    BigDecimal scale = (BigDecimal)titleImageScaleSpinner.getValue();
                    if (fitToText || !scale.equals(new BigDecimal("1"))) {
                        double titleImageRatio = (double)titleImageWidth / titleImageHeight;
                        double doubleScale = scale.doubleValue();
                        if (fitToText) {
                            resizedTitleImageHeight = titleTextHeight;
                            resizedTitleImageWidth = (int)Math.round(resizedTitleImageHeight * titleImageRatio);
                        } else {
                            resizedTitleImageWidth = titleImageWidth;
                            resizedTitleImageHeight = titleImageHeight;
                        }
                        resizedTitleImageWidth = (int)(resizedTitleImageWidth * doubleScale);
                        resizedTitleImageHeight = (int)(resizedTitleImageHeight * doubleScale);
                        resizedTitleImage = titleImage.getScaledInstance(resizedTitleImageWidth, resizedTitleImageHeight, Image.SCALE_AREA_AVERAGING);
                    } else {
                        resizedTitleImage = titleImage;
                        resizedTitleImageWidth = titleImageWidth;
                        resizedTitleImageHeight = titleImageHeight;
                    }
                }
            } catch(Throwable e) {
                e.printStackTrace();
            }
        }

        int titleLineHeight = Math.max(titleTextHeight, resizedTitleImageHeight);
        
        int headerHeight = topMargin + titleLineHeight + titleBelowSpace;
        BufferedImage headerImage = new BufferedImage(fullWidth, headerHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D headerG2 = headerImage.createGraphics();
        headerG2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        headerG2.setColor(backgroundColor);
        headerG2.fillRect(0, 0, fullWidth, headerHeight);
        
        int titleTextLeftAnchor = leftMargin;
        int titleTextRightAnchor = fullWidth - rightMargin;
        
        if (resizedTitleImage != null) {
            String titleImageAlign = titleImageAlignComboBox.getSelectedData();
            int titleImageTop = topMargin + ((titleLineHeight - resizedTitleImageHeight) / 2);
            int titleImageLeft;
            if (titleImageAlign.equals("left")) {
                titleImageLeft = leftMargin;
                titleTextLeftAnchor = leftMargin + resizedTitleImageWidth + titleImageSpace;
            } else {
                titleImageLeft = fullWidth - (rightMargin + resizedTitleImageWidth);
                titleTextRightAnchor = titleImageLeft - titleImageSpace;
            }
            headerG2.drawImage(resizedTitleImage, titleImageLeft, titleImageTop, resizedTitleImageWidth, resizedTitleImageHeight, null);
        }
        
        String titleAlign = titleAlignComboBox.getSelectedData();
        int titleTextLeft;
        if (titleAlign.equals("left")) {
            titleTextLeft = titleTextLeftAnchor;
        } else if (titleAlign.equals("center")) {
            titleTextLeft = titleTextLeftAnchor +
                ((titleTextRightAnchor - titleTextLeftAnchor - titleTextWidth) / 2)
            ;
        } else {
            titleTextLeft = titleTextRightAnchor - titleTextWidth;
        }
        int titleTextTop = topMargin + ((titleLineHeight - titleTextHeight) / 2);
        headerG2.setFont(titleFont);
        headerG2.setColor(titleColorButton.getSelectedAwtColor());
        headerG2.drawString(title, titleTextLeft, titleTextTop + titleTextHeight - titleTextDescent);
        
        int fullHeight = innerHeight + headerHeight;
        
        BufferedImage exportedImage = new BufferedImage(fullWidth, fullHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D exportedG2 = exportedImage.createGraphics();
        exportedG2.drawImage(headerImage, 0, 0, fullWidth, headerHeight, null);
        exportedG2.drawImage(innerImage, 0, headerHeight, fullWidth, innerHeight, null);
        
        return exportedImage;
    }

    

    private class ExportItem {
        
        final String title;
        
        final File file;
        
        final String filename;

        final String imageType;
        
        final Period period;
        
        final ActivityFilter activityFilter;
        
        final Resource associatedResource;

        ExportItem(String title, File file, String filename, String imageType, Period period, ActivityFilter activityFilter) {
            this.title = title;
            this.file = file;
            this.filename = filename;
            this.imageType = imageType;
            this.period = period;
            this.activityFilter = activityFilter;
            this.associatedResource = null;
        }

        ExportItem(String title, File file, String filename, String imageType, Period period, ActivityFilter activityFilter, Resource associatedResource) {
            this.title = title;
            this.file = file;
            this.filename = filename;
            this.imageType = imageType;
            this.period = period;
            this.activityFilter = activityFilter;
            this.associatedResource = associatedResource;
        }
        
    }
    
    
    private class PreviewPanel extends JPanel {

        private static final long serialVersionUID = 1L;
        
        private volatile BufferedImage backgroundImage = null;
        
        private volatile BufferedImage exportedImage = null;
        
        private volatile AtomicReference<ReloadSwingWorker> currentReloadSwingWorkerReference = new AtomicReference<ReloadSwingWorker>();
        
        public PreviewPanel() {
            super();
            addComponentListener(new ComponentListener() {
                
                private Timer resizeTimer = new Timer(300, new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        reload(false);
                        resizeTimer.stop();
                    }
                    
                });
                
                @Override
                public void componentShown(ComponentEvent ev) {
                    reload(false);
                }
                
                @Override
                public void componentResized(ComponentEvent ev) {
                    if (resizeTimer.isRunning()) {
                        resizeTimer.restart();
                    } else {
                        resizeTimer.start();
                    }
                }
                
                @Override
                public void componentMoved(ComponentEvent ev) {
                }
                
                @Override
                public void componentHidden(ComponentEvent ev) {
                }
                
            });
        }
        
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            
            Graphics2D g2 = (Graphics2D)g;
            
            
            int width = getWidth();
            int height = getHeight();

            g2.setColor(new java.awt.Color(0xCCCCCC));
            g2.fillRect(0, 0, width, height);
            
            if (backgroundImage != null) {
                int imageWidth = backgroundImage.getWidth();
                int imageHeight = backgroundImage.getHeight();
                g2.drawImage(backgroundImage, 0, 0, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight, null);
            }
        }
        
        public void reload(boolean exportAgain) {
            backgroundImage = null;
            repaint();
            
            ReloadSwingWorker backgroundRendererWorker = new ReloadSwingWorker(exportAgain, exportedImage);
            
            backgroundRendererWorker.execute();
        }

        class ReloadSwingWorker extends SwingWorker<Pair<BufferedImage, BufferedImage>, Void> {

            private final boolean exportAgain;
            private final BufferedImage currentExportedImage;
            private volatile boolean aborted = false;
            
            public ReloadSwingWorker(boolean exportAgain, BufferedImage currentExportedImage) {
                this.exportAgain = exportAgain;
                this.currentExportedImage = currentExportedImage;
            }
            
            @Override
            protected Pair<BufferedImage, BufferedImage> doInBackground() throws Exception {
                init();
                
                if (aborted) {
                    return null;
                }
                
                BufferedImage exportedImage = currentExportedImage;
                
                if (exportAgain || exportedImage == null) {
                    List<ExportItem> exportItems = getExportItems();
                    if (exportItems.isEmpty()) {
                        exportedImage = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
                        Graphics2D fallbackG2 = exportedImage.createGraphics();
                        fallbackG2.setColor(java.awt.Color.WHITE);
                        fallbackG2.fillRect(0, 0, 100, 100);
                    } else {
                        ExportItem firstItem = exportItems.get(0);
                        exportedImage = createExportedImage(firstItem.title, firstItem.period, firstItem.activityFilter);
                    }
                }

                if (aborted) {
                    return null;
                }
                
                int pw = Math.max(1, previewTabPanel.getWidth());
                int ph = Math.max(1, previewTabPanel.getHeight());
                double pr = (pw / ph);

                BufferedImage newBackgroundImage = new BufferedImage(pw, ph, BufferedImage.TYPE_INT_RGB);
                
                Graphics2D g2 = newBackgroundImage.createGraphics();
                
                g2.setColor(java.awt.Color.WHITE);
                g2.fillRect(0, 0, pw, ph);
                
                if (exportedImage == null) {
                    g2.fillOval((pw / 2) - 35, (ph / 2) - 35, 70, 70);
                } else {
                    int iw = exportedImage.getWidth();
                    int ih = exportedImage.getHeight();
                    double ir = (double)iw / ih;
                    
                    int dx, dy, dw, dh;
                    if (ir > pr) {
                        dw = pw;
                        dh = (int)(pw / ir);
                        dx = 0;
                        dy = (ph - dh) / 2;
                    } else {
                        dw = (int)(ph * ir);
                        dh = ph;
                        dx = (pw - dw) / 2;
                        dy = 0;
                    }
                    
                    Image scaledImage = exportedImage.getScaledInstance(dw, dh, Image.SCALE_AREA_AVERAGING);
                    g2.drawImage(scaledImage, dx, dy, dx + dw, dy + dh, 0, 0, dw, dh, null);
                }
                
                if (aborted) {
                    return null;
                }

                Pair<BufferedImage, BufferedImage> result = new Pair<BufferedImage, BufferedImage>(exportedImage, newBackgroundImage);

                finish();
                
                return result;
            }

            @Override
            protected void done() {
                Pair<BufferedImage, BufferedImage> result = null;
                try {
                    result = get();
                } catch (InterruptedException e) {
                } catch (ExecutionException e) {
                }
                
                if (result == null) {
                    return;
                }
                
                exportedImage = result.getLeft();
                backgroundImage = result.getRight();
                
                repaint();
            }

            private void init() {
                ReloadSwingWorker currentReloadSwingWorker = currentReloadSwingWorkerReference.get();
                synchronized (ImageExportDialog.this) {
                    if (currentReloadSwingWorker != null) {
                        if (exportAgain) {
                            currentReloadSwingWorker.aborted = true;
                        } else {
                            aborted = true;
                            return;
                        }
                    }
                    currentReloadSwingWorkerReference.set(this);
                }
            }

            private void finish() {
                currentReloadSwingWorkerReference.compareAndSet(this, null);
            }
            
        }
        
    }
    
}

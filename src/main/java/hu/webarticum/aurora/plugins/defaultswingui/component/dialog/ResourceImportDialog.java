package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import hu.webarticum.aurora.app.memento.StoreMemento;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.chm.AbstractCommand;
import hu.webarticum.chm.Command;


// TODO: egy osszetettebb importalasi folyamat szukseges
public class ResourceImportDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    protected Document document;

    private JTextArea logTextArea;
    
    private JLabel logEndLabel;
    
    private List<Resource> loadedResources;
    
    public ResourceImportDialog(JFrame parent, Document document) {
        super(parent);
        this.title = text("ui.swing.dialog.resourceimport.title");
        this.document = document;
        init(false, true);
    }
    
    @Override
    protected void build() {
        okButton.setEnabled(false);
        
        JPanel importButtonOuter = new JPanel();
        importButtonOuter.setLayout(new BorderLayout());
        containerPanel.add(importButtonOuter, BorderLayout.PAGE_START);
        
        JButton importButton = new MultilingualButton("ui.swing.dialog.resourceimport.choose_file");
        importButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                startImport();
            }
            
        });
        importButtonOuter.add(importButton, BorderLayout.CENTER);
        
        logTextArea = new JTextArea();
        JScrollPane logTextAreaScroller = new JScrollPane(logTextArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        logTextAreaScroller.setPreferredSize(new Dimension(400, 450));
        containerPanel.add(logTextAreaScroller, BorderLayout.CENTER);

        logEndLabel = new JLabel();
        containerPanel.add(logEndLabel, BorderLayout.PAGE_END);
    }

    @Override
    protected void load() {
    }

    @Override
    protected void save() {
        Document.ResourceStore resourceStore = document.getResourceStore();
        final StoreMemento<Resource> oldResourceStoreMemento = new StoreMemento<Resource>(resourceStore);

        for (Resource resource: loadedResources) {
            resourceStore.register(resource);
        }

        final StoreMemento<Resource> newResourceStoreMemento = new StoreMemento<Resource>(resourceStore);
        
        Command importCommand = new AbstractCommand() {
            
            boolean first = true;
            
            @Override
            protected boolean _rollBack() {
                oldResourceStoreMemento.apply(document.getResourceStore());
                first = false;
                return true;
            }
            
            @Override
            protected boolean _execute() {
                if (!first) {
                    newResourceStoreMemento.apply(document.getResourceStore());
                }
                return true;
            }

            @Override
            public String toString() {
                return text("ui.swing.dialog.resourceimport.command.import");
            }

        };
        history().addAndExecute(importCommand);
    }
    
    private String[] parseCsvLine(String line) {
        line = line.replaceFirst("^,", " ,").replaceAll(",,", ", ,").replaceAll(",,", ", ,");
        Matcher m = Pattern.compile("([^\",]+|\"([^\"]|\\\\\\\\|\\\\\"|\"\")*\")(,|$)").matcher(line);
        
        ArrayList<String> ss = new ArrayList<String>();
        
        while (m.find()) {
            String token = m.group();
            if (token.endsWith(",")) {
                token = token.substring(0, token.length()-1);
            }
            token = token.trim();
            if (token.startsWith("\"")) {
                token = token.substring(1, token.length()-1);
                
                // only one of them can be occurred simultaneously:
                token = token.replaceAll("\\\\(.)", "$1");
                token = token.replaceAll("\"\"", "\"");
            }
            ss.add(token);
        }

        String[] result = new String[ss.size()];
        result = ss.toArray(result);
        return result;
    }
    
    private void log(String str) {
        logTextArea.append(str+"\n");
    }
    
    private void clearLog() {
        logTextArea.setText("");
        logEndLabel.setText("");
    }
    
    private void endLog(String message, java.awt.Color color) {
        logEndLabel.setText(message);
        logEndLabel.setForeground(color);
    }
    
    private void startImport() {
        loadedResources = new ArrayList<Resource>();
        okButton.setEnabled(false);
        clearLog();
        
        final JFileChooser fileChooser = new JFileChooser();
        int chooseResult = fileChooser.showOpenDialog(parent);
        if (chooseResult!=JFileChooser.APPROVE_OPTION) {
            return;
        }
        File file = fileChooser.getSelectedFile();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (Exception e) {
            OptionPaneUtil.showMessageDialog(parent, text("ui.swing.dialog.resourceimport.can_not_open_file"));
            return;
        }
        int lineNumber = 0;
        try {
            String line;
            while ((line=reader.readLine())!=null) {
                lineNumber++;
                
                if (line.length()==0) {
                    log(text("ui.swing.dialog.resourceimport.empty_line"));
                    continue;
                }
                String[] tokens = parseCsvLine(line);
                String label = tokens[0].trim();
                if (label.length()==0) {
                    log(text("ui.swing.dialog.resourceimport.empty_label"));
                    continue;
                }
                String typeStr = (tokens.length>1?tokens[1].trim():"");
                String colorStr = (tokens.length>2?tokens[2].trim():"");
                String quantityStr = (tokens.length>3?tokens[3].trim():"");

                if (typeStr.length()==0) typeStr = "OTHER";
                if (colorStr.length()==0) colorStr = "#FFFFFF";
                if (quantityStr.length()==0) quantityStr = "1";

                Resource.Type type;
                hu.webarticum.aurora.core.model.Color color;
                int quantity;
                
                try {
                    type = Resource.Type.valueOf(typeStr);
                } catch (IllegalArgumentException e) {
                    log(String.format(text("ui.swing.dialog.resourceimport.unknown_type"), typeStr));
                    continue;
                }
                
                try {
                    color = new hu.webarticum.aurora.core.model.Color(colorStr);
                } catch (IllegalArgumentException e) {
                    log(String.format(text("ui.swing.dialog.resourceimport.invalid_colorcode"), colorStr));
                    continue;
                }

                try {
                    quantity = Integer.parseInt(quantityStr);
                } catch(NumberFormatException e) {
                    log(String.format(text("ui.swing.dialog.resourceimport.invalid_quantity"), quantityStr));
                    continue;
                }
                
                if (quantity<1) {
                    log(String.format(text("ui.swing.dialog.resourceimport.incorrect_quantity"), quantity));
                    continue;
                }
                
                Resource resource = new Resource();
                resource.setLabel(label);
                resource.setType(type);
                resource.setColor(color);
                resource.setQuantity(quantity);
                
                Resource.SplittingManager splittingManager = resource.getSplittingManager();
                
                List<Resource.Splitting> splittings = new ArrayList<Resource.Splitting>();
                
                String invalidSplittingText = null;
                for (int i=4; i<tokens.length; i++) {
                    String splittingText = tokens[i].trim();
                    if (splittingText.length()==0) {
                        continue;
                    }
                    if (!splittingText.matches("[^:;]+:[^;]+(;[^;]+)*")) {
                        invalidSplittingText = splittingText;
                        break;
                    }
                    int colonPos = splittingText.indexOf(':');
                    String splittingName = splittingText.substring(0, colonPos).trim();
                    String splittingPartsText = splittingText.substring(colonPos+1).trim();
                    String[] splittingParts = splittingPartsText.replaceAll("\\s*;\\s*", ";").split(";");
                    Resource.Splitting splitting = resource.new Splitting(splittingName, splittingParts);
                    splittingManager.add(splitting);
                }
                if (invalidSplittingText!=null) {
                    log(String.format(text("ui.swing.dialog.resourceimport.invalid_splitting"), invalidSplittingText));
                    continue;
                }
                
                for (Resource.Splitting splitting: splittings) {
                    splittingManager.add(splitting);
                }
                
                log(String.format(text("ui.swing.dialog.resourceimport.line_loaded"), label));
                
                loadedResources.add(resource);
            }
            
        } catch (IOException e) {
            OptionPaneUtil.showMessageDialog(parent, text("ui.swing.dialog.resourceimport.read_error"));
            return;
        } catch (Exception e) {
            OptionPaneUtil.showMessageDialog(parent, text("ui.swing.dialog.resourceimport.process_error"));
            return;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
            }
        }
        
        int loadedNumber = loadedResources.size();
        int invalidNumber = lineNumber-loadedNumber;
        String endMessage = String.format(text("ui.swing.dialog.resourceimport.n_loaded_resources"), loadedNumber);
        if (invalidNumber>0) {
            endMessage += ", "+String.format(text("ui.swing.dialog.resourceimport.n_invalid_lines"), invalidNumber);
        }
        java.awt.Color color = new java.awt.Color(0x117700);
        if (loadedNumber==0) {
            color = new java.awt.Color(0xAA3300);
        } else if (invalidNumber>0) {
            color = new java.awt.Color(0xDD7700);
        }
        endLog(endMessage, color);
        
        if (loadedNumber>0) {
            okButton.setEnabled(true);
        }
    }

}

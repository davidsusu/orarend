package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;

public class BoardBlockTimeDialog extends EditDialog {
    
    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<Board> boardWrapper;
    
    private GeneralWrapper<Block> blockWrapper;
    
    private GeneralWrapper<Time> timeWrapper;
    
    private Board board;
    
    private Block block;
    
    private Time time;

    private JButton boardButton;
    
    private JButton blockButton;
    
    private JButton timeButton;
    
    public BoardBlockTimeDialog(JFrame parent, Document document, GeneralWrapper<Board> boardWrapper, GeneralWrapper<Block> blockWrapper, GeneralWrapper<Time> timeWrapper) {
        super(parent);
        this.title = text("ui.swing.dialog.boardblocktime.title");
        this.document = document;
        this.boardWrapper = boardWrapper;
        this.blockWrapper = blockWrapper;
        this.timeWrapper = timeWrapper;
        this.board = boardWrapper.get();
        this.block = blockWrapper.get();
        this.time = timeWrapper.get();
        init();
    }

    @Override
    protected ButtonPurpose getPurpose() {
        return ButtonPurpose.ACTION;
    }

    @Override
    protected void build() {
        JPanel boardPanel = new JPanel();
        boardPanel.setLayout(new BorderLayout());
        boardButton = new JButton("?");
        boardButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Board> wrapper = new GeneralWrapper<Board>(BoardBlockTimeDialog.this.board);
                BoardSelectDialog dialog = new BoardSelectDialog(parent, document, wrapper);
                dialog.run();

                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    BoardBlockTimeDialog.this.board = wrapper.get();
                    reload();
                }
            }
            
        });
        boardPanel.add(boardButton, BorderLayout.CENTER);
        addRow(new MultilingualLabel("ui.swing.dialog.boardblocktime.board"), boardPanel);

        JPanel blockPanel = new JPanel();
        blockPanel.setLayout(new BorderLayout());
        blockButton = new JButton("?");
        blockButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Block> wrapper = new GeneralWrapper<Block>(BoardBlockTimeDialog.this.block);
                BlockSelectDialog dialog = new BlockSelectDialog(parent, document, wrapper);
                dialog.run();

                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    BoardBlockTimeDialog.this.block = wrapper.get();
                    reload();
                }
            }
            
        });
        blockPanel.add(blockButton, BorderLayout.CENTER);
        addRow(new MultilingualLabel("ui.swing.dialog.boardblocktime.block"), blockPanel);

        JPanel timePanel = new JPanel();
        timePanel.setLayout(new BorderLayout());
        timeButton = new JButton("?");
        timeButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Time time = BoardBlockTimeDialog.this.time;
                if (time==null && block!=null) {
                    TimingSet blockTimingSet;
                    if (board!=null) {
                        blockTimingSet = block.getCalculatedTimingSet();
                    } else {
                        blockTimingSet = block.getCalculatedTimingSet(board);
                    }
                    if (blockTimingSet.isEmpty()) {
                        time = new Time(0);
                    } else {
                        time = blockTimingSet.first().getTime();
                    }
                }
                GeneralWrapper<Time> wrapper = new GeneralWrapper<Time>(time);
                BlockTimeSelectDialog dialog = new BlockTimeSelectDialog(parent, block, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    BoardBlockTimeDialog.this.time = wrapper.get();
                    reload();
                }
            }
            
        });
        timePanel.add(timeButton, BorderLayout.CENTER);
        addRow(new MultilingualLabel("ui.swing.dialog.boardblocktime.time"), timePanel);
    }

    @Override
    protected void load() {
        reload();
    }

    protected void reload() {
        if (board==null) {
            boardButton.setText("");
        } else {
            boardButton.setText(board.getLabel());
        }

        if (block==null) {
            blockButton.setText("");
        } else {
            blockButton.setText(block.getLabel());
        }

        if (time==null) {
            timeButton.setText("");
        } else {
            timeButton.setText(time.toReadableString());
        }
        
        if (board==null || block==null || time==null) {
            okButton.setEnabled(false);
        } else {
            okButton.setEnabled(true);
        }
    }

    @Override
    protected void save() {
        boardWrapper.set(board);
        blockWrapper.set(block);
        timeWrapper.set(time);
    }

}

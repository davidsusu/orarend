package hu.webarticum.aurora.plugins.defaultswingui.i18n;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JTabbedPane;

import hu.webarticum.aurora.app.util.common.Pair;

public class MultilingualTabbedPane extends JTabbedPane implements MultilingualComponent {

    private static final long serialVersionUID = 1L;
    
    private List<Pair<MultilingualContent, MultilingualContent>> langDataList = new LinkedList<Pair<MultilingualContent, MultilingualContent>>();

    protected String getLanguageText(String path) {
        return text(path);
    }

    public void addMultilingualTab(String titlePath, Component component) {
        insertMultilingualTab(titlePath, null, component, null, getTabCount());
    }

    public void addMultilingualTab(String titlePath, Icon icon, Component component) {
        insertMultilingualTab(titlePath, icon, component, null, getTabCount());
    }

    public void addMultilingualTab(String titlePath, Icon icon, Component component, String tooltipPath) {
        insertMultilingualTab(titlePath, icon, component, tooltipPath, getTabCount());
    }

    public void insertMultilingualTab(String titlePath, Icon icon, Component component, String tooltipPath, int index) {
        insertMultilingualTab(new PathMultilingualContent(titlePath), icon, component, new PathMultilingualContent(tooltipPath), index);
    }

    public void insertMultilingualTab(MultilingualContent titleContent, Icon icon, Component component, MultilingualContent tooltipContent, int index) {
        langDataList.add(index, new Pair<MultilingualContent, MultilingualContent>(titleContent, tooltipContent));
        super.insertTab(titleContent.toString(), icon, component, tooltipContent.toStringOrNull(), index);
    }
    
    @Override
    public void addTab(String title, Component component) {
        insertTab(title, null, component, null, getTabCount());
    }

    @Override
    public void addTab(String title, Icon icon, Component component) {
        insertTab(title, icon, component, null, getTabCount());
    }

    @Override
    public void addTab(String title, Icon icon, Component component, String tooltip) {
        insertTab(title, icon, component, tooltip, getTabCount());
    }

    @Override
    public void insertTab(String title, Icon icon, Component component, String tooltip, int index) {
        langDataList.add(index, null);
        super.insertTab(title, icon, component, tooltip, index);
    }
    
    @Override
    public void removeTabAt(int index) {
        langDataList.remove(index);
        super.removeTabAt(index);
    }
    
    @Override
    public void reloadLanguageTexts() {
        int tabCount = getTabCount();
        for (int i = 0; i < tabCount; i++) {
            Pair<MultilingualContent, MultilingualContent> langDataItem = langDataList.get(i);
            if (langDataItem != null) {
                MultilingualContent titleContent= langDataItem.getLeft();
                setTitleAt(i, titleContent.toString());

                MultilingualContent tooltipContent = langDataItem.getRight();
                String tooltipText = tooltipContent.toString();
                setToolTipTextAt(i, tooltipText.isEmpty() ? null : tooltipText);
            }
        }
    }

}

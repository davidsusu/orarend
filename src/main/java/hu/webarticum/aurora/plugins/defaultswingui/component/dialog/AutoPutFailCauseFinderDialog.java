package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.rawText;

import javax.swing.JFrame;

public class AutoPutFailCauseFinderDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    
    public AutoPutFailCauseFinderDialog(JFrame parent) {
        super(parent);
        this.title = rawText("This is AutoPutFailCauseFinderDialog");
        
        // TODO
        
        init();
    }


    @Override
    protected void build() {
        addRow(rawText("Hello, AutoPutFailCauseFinderDialog!"));
    }
    
    @Override
    protected void load() {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void save() {
        // TODO Auto-generated method stub
        
    }

}

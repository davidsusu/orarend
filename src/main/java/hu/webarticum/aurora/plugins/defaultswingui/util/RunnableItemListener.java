package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class RunnableItemListener implements ItemListener {

    private final Runnable runnable;
    
    public RunnableItemListener(Runnable runnable) {
        this.runnable = runnable;
    }
    
    @Override
    public void itemStateChanged(ItemEvent ev) {
        runnable.run();
    }

}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;

public class ButtonOptionDialog extends AbstractOptionDialog {

    private static final long serialVersionUID = 1L;

    public ButtonOptionDialog(
        JFrame parent,
        String message,
        String title,
        int messageType,
        Object[] options,
        ButtonPurpose[] optionPurposes,
        Object initialValue
    ) {
        super(parent);
        this.title = title;
        init();
        
        applyMessageType(messageType);
        addButtons(options, optionPurposes, initialValue);

        JLabel messageLabel = new JLabel(message);
        messageLabel.setBorder(new EmptyBorder(15, 0, 15, 15));
        contentPanel.add(messageLabel, BorderLayout.CENTER);
    }
    
}

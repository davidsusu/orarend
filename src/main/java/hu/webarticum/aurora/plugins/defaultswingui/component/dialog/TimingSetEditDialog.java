package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIDefaults;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import hu.webarticum.aurora.app.memento.TimingSetMemento;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.chm.Command;



public class TimingSetEditDialog extends LabeledEditDialog {
    
    private static final long serialVersionUID = 1L;

    private final Document document;
    
    private final TimingSet timingSet;
    
    private final Labeled contextObject;

    private final List<TimingSet.TimeEntry> timeEntries = new ArrayList<TimingSet.TimeEntry>();

    private boolean editable;
    
    private JPanel timeListContainer;

    public TimingSetEditDialog(JFrame parent, Document document, TimingSet timingSet) {
        this(parent, document, timingSet, null);
    }

    public TimingSetEditDialog(JFrame parent, Document document, TimingSet timingSet, Labeled contextObject) {
        this(parent, document, timingSet, contextObject, true);
    }

    public TimingSetEditDialog(JFrame parent, Document document, TimingSet timingSet, boolean editable) {
        this(parent, document, timingSet, null, editable);
    }
    
    public TimingSetEditDialog(JFrame parent, Document document, TimingSet timingSet, Labeled contextObject, boolean editable) {
        super(parent);
        this.document = document;
        this.timingSet = timingSet;
        this.contextObject = contextObject;
        this.editable = editable;
        init(false, true);
    }
    
    @Override
    public String getTitle() {
        String generatedTitle = text("ui.swing.dialog.timingsetedit.title");
        if (contextObject != null) {
            generatedTitle += String.format(" (%s)", contextObject.getLabel());
        }
        return generatedTitle;
    }
    
    @Override
    protected void build() {
        JLabel label;
        JButton button;

        labelField = new JTextField();
        labelField.setMaximumSize(new Dimension(700, 25));
        
        if (editable) {
            JPanel headerPanel = new JPanel();
            headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.LINE_AXIS));
            headerPanel.setBorder(new EmptyBorder(7, 7, 7, 7));
            containerPanel.add(headerPanel, BorderLayout.PAGE_START);
            addRowTo(headerPanel, new MultilingualLabel("ui.swing.dialog.timingsetedit.label"), labelField, formWidth, leftWidth);
        }
        
        JPanel timeListPanel = new JPanel();
        timeListPanel.setLayout(new BorderLayout());
        timeListPanel.setPreferredSize(new Dimension(300, 350));
        timeListPanel.setBackground(java.awt.Color.WHITE);
        containerPanel.add(timeListPanel, BorderLayout.CENTER);

        JPanel timeListHeadOuterPanel = new JPanel();
        timeListHeadOuterPanel.setLayout(new BoxLayout(timeListHeadOuterPanel, BoxLayout.PAGE_AXIS));
        timeListPanel.add(timeListHeadOuterPanel, BorderLayout.PAGE_START);
        
        JPanel timeListHeadPanel = new JPanel();
        timeListHeadPanel.setLayout(new BorderLayout());
        timeListHeadPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        timeListHeadOuterPanel.add(timeListHeadPanel, BorderLayout.PAGE_START);

        label = new MultilingualLabel("ui.swing.dialog.timingsetedit.time_entries");
        timeListHeadPanel.add(label, BorderLayout.LINE_START);

        if (editable) {
            button = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.dialog.timingsetedit.add_time_entry");
            button.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    TimingSet.TimeEntry timeEntry = new TimingSet.TimeEntry(new Time(0), "");
                    TimeEntryEditDialog dialog = new TimeEntryEditDialog(parent, timeEntry);
                    dialog.run();
                    
                    if (dialog.getResult()==EditDialog.RESULT_OK) {
                        modified = true;
                        timeEntries.add(timeEntry);
                        refreshTimeListContainer();
                    }
                }
                
            });
            timeListHeadPanel.add(button, BorderLayout.LINE_END);
        }

        JPanel timeListContainerOuter = new JPanel();
        timeListContainerOuter.setBackground(java.awt.Color.WHITE);
        timeListContainerOuter.setLayout(new BorderLayout());
        JScrollPane timeListScrollPane = new JScrollPane(timeListContainerOuter);
        timeListScrollPane.getVerticalScrollBar().setUnitIncrement(10);
        timeListPanel.add(timeListScrollPane, BorderLayout.CENTER);
        
        timeListContainerOuter.setBackground(java.awt.Color.WHITE);
        
        timeListContainer = new JPanel();
        timeListContainer.setLayout(new BoxLayout(timeListContainer, BoxLayout.PAGE_AXIS));
        timeListContainer.setBackground(java.awt.Color.WHITE);
        timeListContainer.setBorder(new EmptyBorder(0, 0, 10, 0));
        timeListContainerOuter.add(timeListContainer, BorderLayout.PAGE_START);

        if (!editable) {
            okButton.setVisible(false);
            cancelButton.setLanguagePath("ui.swing.dialog.timingsetedit.close");
        }
    }

    @Override
    protected String generateTitle() {
        if (editable) {
            return super.generateTitle();
        } else {
            return text("ui.swing.dialog.timingsetedit.readonly_title");
        }
    }
    
    @Override
    protected void load() {
        labelField.setText(timingSet.getLabel());
        
        for (TimingSet.TimeEntry timeEntry: timingSet) {
            timeEntries.add(new TimingSet.TimeEntry(timeEntry));
        }
        
        refreshTimeListContainer();
    }

    @Override
    protected void save() {
        if (!modified) {
            return;
        }
        
        sortTimes();

        TimingSetMemento newMemento = new TimingSetMemento(labelField.getText(), timeEntries);
        
        Command command;
        Document.TimingSetStore timingSetStore = document.getTimingSetStore();
        
        if (timingSetStore.contains(timingSet)) {
            TimingSetMemento oldMemento = new TimingSetMemento(timingSet);
            command = new StoreItemEditCommand<TimingSet>(
                "ui.swing.dialog.timingsetedit.command.edit",
                timingSetStore, timingSet, oldMemento, newMemento
            );
        } else {
            newMemento.apply(timingSet);
            command = new StoreItemRegisterCommand<TimingSet>(
                "ui.swing.dialog.timingsetedit.command.register",
                timingSetStore, timingSet
            );
        }
        
        history().addAndExecute(command);
    }
    
    private void refreshTimeListContainer() {
        sortTimes();

        timeListContainer.removeAll();

        long previousDay = Long.MIN_VALUE;
        for (final TimingSet.TimeEntry timeEntry: timeEntries) {
            long currentDay = timeEntry.getTime().getPart(Time.PART.FULLDAYS);
            
            JPanel timePanelOuter = new JPanel();
            timePanelOuter.setOpaque(false);
            timePanelOuter.setLayout(new BoxLayout(timePanelOuter, BoxLayout.LINE_AXIS));
            if (currentDay != previousDay) {
                timePanelOuter.setBorder(new EmptyBorder(7, 4, 0, 4));
            } else {
                timePanelOuter.setBorder(new EmptyBorder(4, 4, 0, 4));
            }
            timeListContainer.add(timePanelOuter);
            
            JPanel timePanel = new JPanel();
            timePanel.setLayout(new BorderLayout());
            if (currentDay != previousDay) {
                timePanel.setBorder(new MatteBorder(7, 1, 1, 1, java.awt.Color.GRAY));
            } else {
                timePanel.setBorder(new LineBorder(java.awt.Color.GRAY));
            }
            timePanelOuter.add(timePanel);

            String labelString = timeEntry.toReadableString();
            JLabel label = new JLabel(labelString);
            label.putClientProperty("html.disable", true);
            label.setBorder(new EmptyBorder(0, 4, 0, 0));
            timePanel.add(label, BorderLayout.LINE_START);
            
            if (editable) {
                UIDefaults buttonUIDefaults = new UIDefaults();
                buttonUIDefaults.put("Button.contentMargins", new Insets(4, 10, 4, 10));

                String uiPropertyKey = "Nimbus.Overrides";

                JPanel timeControlPanel = new JPanel();
                timePanel.add(timeControlPanel, BorderLayout.LINE_END);
                
                JButton editTimeButton = new JButton();
                editTimeButton.setIcon(IconLoader.loadIcon("edit"));
                editTimeButton.putClientProperty(uiPropertyKey, buttonUIDefaults);
                editTimeButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        TimeEntryEditDialog dialog = new TimeEntryEditDialog(parent, timeEntry);
                        dialog.run();
                        
                        if (dialog.getResult()==EditDialog.RESULT_OK) {
                            modified = true;
                            refreshTimeListContainer();
                        }
                    }
                    
                });
                timeControlPanel.add(editTimeButton);
    
                JButton removeTimeButton = new JButton();
                removeTimeButton.setIcon(IconLoader.loadIcon("trash"));
                removeTimeButton.putClientProperty(uiPropertyKey, buttonUIDefaults);
                removeTimeButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // TODO: convert to showConfirmDialog
                        int answer = OptionPaneUtil.showOptionDialog(parent,
                            text("ui.swing.dialog.timingsetedit.remove_time.sure"),
                            text("ui.swing.dialog.timingsetedit.remove_time.confirm"),
                            JOptionPane.QUESTION_MESSAGE,
                            new String[]{
                                text("ui.swing.dialog.timingsetedit.remove_time.cancel"),
                                text("ui.swing.dialog.timingsetedit.remove_time.delete"),
                            },
                            new ButtonPurpose[] {
                                ButtonPurpose.NONE,
                                ButtonPurpose.DANGER,
                            },
                            text("ui.swing.dialog.timingsetedit.remove_time.delete")
                        );
                        if (answer == 1) {
                            modified = true;
                            timeEntries.remove(timeEntry);
                            refreshTimeListContainer();
                        }
                    }
                    
                });
                timeControlPanel.add(removeTimeButton);
            }
            
            previousDay = currentDay;
        }

        timeListContainer.getTopLevelAncestor().validate();
        timeListContainer.getTopLevelAncestor().repaint();
        
        timeListContainer.getTopLevelAncestor().validate();
        timeListContainer.getTopLevelAncestor().repaint();
    }
    
    private void sortTimes() {
        Collections.sort(timeEntries, new Comparator<TimingSet.TimeEntry>() {

            @Override
            public int compare(TimingSet.TimeEntry timeEntry1, TimingSet.TimeEntry timeEntry2) {
                return timeEntry1.compareTo(timeEntry2);
            }
            
        });
    }


    public class TimeEntryEditDialog extends EditDialog {

        private static final long serialVersionUID = 1L;

        private TimingSet.TimeEntry timeEntry;
        
        private JTextField labelField;

        private Time time;
        
        private JButton timeButton;
        
        public TimeEntryEditDialog(JFrame parent, TimingSet.TimeEntry timeEntry) {
            super(parent);
            this.title = text("ui.swing.dialog.timingsetedit.time_entry.title");
            this.timeEntry = timeEntry;
            this.time = timeEntry.getTime();
            init();
        }
        
        @Override
        protected void build() {
            timeButton = new JButton("?");
            timeButton.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    GeneralWrapper<Time> wrapper = new GeneralWrapper<Time>(time);
                    TimeEditDialog dialog = new TimeEditDialog(parent, wrapper);
                    dialog.run();
                    
                    if (dialog.getResult()==EditDialog.RESULT_OK) {
                        time = wrapper.get();
                        refreshTime();
                    }
                }
            });
            addRow(new MultilingualLabel("ui.swing.dialog.timingsetedit.time_entry.time"), timeButton);
            
            labelField = new JTextField();
            labelField.setMaximumSize(new Dimension(700, 25));
            addRow(new MultilingualLabel("ui.swing.dialog.timingsetedit.time_entry.label"), labelField);
        }

        @Override
        protected void load() {
            labelField.setText(timeEntry.getLabel());
            refreshTime();
        }

        @Override
        protected void save() {
            timeEntry.setTime(time);
            timeEntry.setLabel(labelField.getText());
        }
        
        private void refreshTime() {
            timeButton.setText(time.toReadableString());
        }

    }
    
}

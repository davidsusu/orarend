package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.FileUtil;
import hu.webarticum.aurora.app.util.ZipCreator;
import hu.webarticum.aurora.app.util.common.StringUtil;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.MultiComparator;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.FileChooserField;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualCheckBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualRadioButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.AutoExtensionFileChooser;
import hu.webarticum.aurora.plugins.defaultswingui.util.RunnableItemListener;


public class WebPageExportDialog extends AbstractExportDialog {

    private static final long serialVersionUID = 1L;

    
    private Board board;

    private MultilingualRadioButton compressedRadioButton = new MultilingualRadioButton();

    private AutoExtensionFileChooser compressedFileChooser = new AutoExtensionFileChooser();
    
    private FileChooserField compressedFileField = new FileChooserField();

    private MultilingualCheckBox enableClassSelectCheckBox = new MultilingualCheckBox();

    private MultilingualCheckBox enablePersonSelectCheckBox = new MultilingualCheckBox();

    private MultilingualCheckBox enableLocaleSelectCheckBox = new MultilingualCheckBox();

    private MultilingualRadioButton dataFileRadioButton = new MultilingualRadioButton();

    private AutoExtensionFileChooser dataFileChooser = new AutoExtensionFileChooser();
    
    private FileChooserField dataFileField = new FileChooserField();
    
    
    public WebPageExportDialog(JFrame parent, Board board) {
        super(parent);
        this.title = text("ui.swing.dialog.webpageexport.title");
        this.board = board;
        this.formWidth = 500;
        init();
    }

    
    @Override
    protected void build() {
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        
        ButtonGroup typeButtonGroup = new ButtonGroup();
        typeButtonGroup.add(compressedRadioButton);
        typeButtonGroup.add(dataFileRadioButton);

        {
            compressedRadioButton.setLanguagePath("ui.swing.dialog.webpageexport.compressed");
            addRow(compressedRadioButton);
            
            final JPanel compressedSubPanel = createSubPanel();
            addRow(compressedSubPanel);

            AutoExtensionFileChooser.ExtensionFilter zipFilter = new AutoExtensionFileChooser.ExtensionFilter(true, "ui.swing.dialog.webpageexport.compressed.chooser.options.zip", "zip");
            compressedFileChooser.addChoosableFileFilter(zipFilter);
            compressedFileChooser.setFileFilter(zipFilter);
            compressedFileField.setFileChooser(compressedFileChooser);
            compressedFileField.setDialogType(JFileChooser.SAVE_DIALOG);
            addRowTo(compressedSubPanel, text("ui.swing.dialog.webpageexport.compressed.file"), compressedFileField, formWidth - 30, 120);

            enableClassSelectCheckBox.setLanguagePath("ui.swing.dialog.webpageexport.compressed.classes");
            enableClassSelectCheckBox.setEnabled(false);
            enableClassSelectCheckBox.setSelected(true);
            addRowTo(compressedSubPanel, enableClassSelectCheckBox, formWidth - 30);
            
            enablePersonSelectCheckBox.setLanguagePath("ui.swing.dialog.webpageexport.compressed.persons");
            enablePersonSelectCheckBox.setSelected(true);
            addRowTo(compressedSubPanel, enablePersonSelectCheckBox, formWidth - 30);
            
            enableLocaleSelectCheckBox.setLanguagePath("ui.swing.dialog.webpageexport.compressed.locales");
            enableLocaleSelectCheckBox.setSelected(true);
            addRowTo(compressedSubPanel, enableLocaleSelectCheckBox, formWidth - 30);
        }

        {
            dataFileRadioButton.setLanguagePath("ui.swing.dialog.webpageexport.datafile");
            addRow(dataFileRadioButton);

            final JPanel dataFileSubPanel = createSubPanel();
            addRow(dataFileSubPanel);
            
            AutoExtensionFileChooser.ExtensionFilter jsonFilter = new AutoExtensionFileChooser.ExtensionFilter(true, "ui.swing.dialog.webpageexport.datafile.chooser.options.json", "json");
            dataFileChooser.addChoosableFileFilter(jsonFilter);
            dataFileChooser.setFileFilter(jsonFilter);
            dataFileField.setFileChooser(dataFileChooser);
            dataFileField.setDialogType(JFileChooser.SAVE_DIALOG);
            addRowTo(dataFileSubPanel, text("ui.swing.dialog.webpageexport.datafile.file"), dataFileField, formWidth - 30, 120);
        }
        
        pack();
    }

    @Override
    protected void load() {
        compressedRadioButton.setSelected(true);
    }

    @Override
    protected void afterLoad() {
        final Runnable compressedRadioButtonAction = new Runnable() {
            
            @Override
            public void run() {
                boolean enabled = compressedRadioButton.isSelected();
                compressedFileField.setEnabled(enabled);
                enablePersonSelectCheckBox.setEnabled(enabled);
                enableLocaleSelectCheckBox.setEnabled(enabled);
            }
            
        };
        
        compressedRadioButton.addItemListener(new RunnableItemListener(compressedRadioButtonAction));
        
        compressedRadioButtonAction.run();
        

        final Runnable singleFileRadioButtonAction = new Runnable() {
            
            @Override
            public void run() {
                boolean enabled = dataFileRadioButton.isSelected();
                dataFileField.setEnabled(enabled);
            }
            
        };
        
        dataFileRadioButton.addItemListener(new RunnableItemListener(singleFileRadioButtonAction));
        
        singleFileRadioButtonAction.run();
    }

    @Override
    protected void save() {
    }

    @Override
    protected boolean trySave() {
        final GeneralWrapper<Boolean> resultWrapper = new GeneralWrapper<Boolean>();
        SwingWorkerProcessDialog processDialog = new SwingWorkerProcessDialog(parent, text("ui.swing.dialog.webpageexport.saving"), true) {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected Worker createWorker() {
                return new Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        resultWrapper.set(trySaveUnwrapped());
                    }

                    @Override
                    public void _rollBack() {
                    }

                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        if (command instanceof TerminatedCommand) {
                            closeDialog();
                        }
                    }
                    
                };
            }
            
        };
        processDialog.run();
        return resultWrapper.get();
    }

    protected boolean trySaveUnwrapped() {
        if (compressedRadioButton.isSelected()) {
            File compressedFile;
            if (compressedFileField.isEmpty()) {
                if (compressedFileChooser.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
                    return false;
                }
                compressedFile = compressedFileChooser.getSelectedFile();
            } else {
                compressedFile = compressedFileField.getFile();
            }

            if (!checkFile(parent, compressedFile)) {
                return false;
            }
            
            String indexHtmlContent;
            try {
                indexHtmlContent = FileUtil.readContent(WebPageExportDialog.class.getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/index.html"));
            } catch (IOException e) {
                e.printStackTrace();
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.webpageexport.compressed.saveerror.message"),
                    text("ui.swing.dialog.webpageexport.compressed.saveerror.title"),
                    JOptionPane.ERROR_MESSAGE
                );
                return false;
            }
            
            indexHtmlContent = indexHtmlContent.replace("{{LANGUAGE}}", Application.instance().getTextRepository().getLanguage());
            
            try {
                ZipCreator zipCreator = new ZipCreator();
                zipCreator.add("timetable/OLVASSEL", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/OLVASSEL"));
                zipCreator.add("timetable/README", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/README"));
                zipCreator.add("timetable/javascript/jquery-3.0.0.js", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/javascript/jquery-3.0.0.js"));
                zipCreator.add("timetable/javascript/aurora-timetable-1.0/aurora-timetable.css", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/javascript/aurora-timetable-1.0/aurora-timetable.css"));
                zipCreator.add("timetable/javascript/aurora-timetable-1.0/aurora-timetable.js", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/javascript/aurora-timetable-1.0/aurora-timetable.js"));
                zipCreator.add("timetable/javascript/aurora-timetable-1.0/skin/default/default.css", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/javascript/aurora-timetable-1.0/skin/default/default.css"));
                zipCreator.add("timetable/javascript/aurora-timetable-1.0/skin/fancy/fancy.css", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/javascript/aurora-timetable-1.0/skin/fancy/fancy.css"));
                zipCreator.add("timetable/javascript/aurora-timetable-1.0/lang/hu_HU.js", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/javascript/aurora-timetable-1.0/lang/hu_HU.js"));
                zipCreator.add("timetable/javascript/aurora-timetable-1.0/lang/en_US.js", getClass().getResource(Info.RESOURCE_PATH + "/x/export/tablewebpage/javascript/aurora-timetable-1.0/lang/en_US.js"));

                zipCreator.add("timetable/index.html", indexHtmlContent);
                
                String jsonString = buildDataJsonString();
                zipCreator.add("timetable/data/timetable-data.json", jsonString);
                
                zipCreator.save(compressedFile);
            } catch (IOException e) {
                e.printStackTrace();
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.webpageexport.compressed.saveerror.message"),
                    text("ui.swing.dialog.webpageexport.compressed.saveerror.title"),
                    JOptionPane.ERROR_MESSAGE
                );
                return false;
            }
            
            return true;
        } else if (dataFileRadioButton.isSelected()) {
            File dataFile;
            if (dataFileField.isEmpty()) {
                if (dataFileChooser.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
                    return false;
                }
                dataFile = dataFileChooser.getSelectedFile();
            } else {
                dataFile = dataFileField.getFile();
            }
            
            if (!checkFile(parent, dataFile)) {
                return false;
            }
            
            String jsonString = buildDataJsonString();
            try {
                FileUtil.writeContent(dataFile, jsonString);
            } catch (IOException e) {
                e.printStackTrace();
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.webpageexport.datafile.saveerror.message"),
                    text("ui.swing.dialog.webpageexport.datafile.saveerror.title"),
                    JOptionPane.ERROR_MESSAGE
                );
                return false;
            }
            
            return true;
        }
        
        return false;
    }
    
    @SuppressWarnings("unchecked")
    private String buildDataJsonString() {
        Set<String> nameSet = new HashSet<String>();
        Map<Object, String> aspectNameMap = new HashMap<Object, String>();
        
        JSONObject dataJsonObject = new JSONObject();
        
        JSONArray selectsJsonArray = new JSONArray();
        dataJsonObject.put("selects", selectsJsonArray);
        
        JSONObject resourcesSelectJsonObject = new JSONObject();
        resourcesSelectJsonObject.put("labelName", "labelNameAttribute");
        selectsJsonArray.add(resourcesSelectJsonObject);

        JSONArray resourcesGroupsJsonArray = new JSONArray();
        resourcesSelectJsonObject.put("groups", resourcesGroupsJsonArray);
        
        List<Resource.Type> resourceTypeAspects = new ArrayList<Resource.Type>();
        
        if (enableClassSelectCheckBox.isSelected()) {
            resourceTypeAspects.add(Resource.Type.CLASS);
        }

        if (enablePersonSelectCheckBox.isSelected()) {
            resourceTypeAspects.add(Resource.Type.PERSON);
        }

        if (enableLocaleSelectCheckBox.isSelected()) {
            resourceTypeAspects.add(Resource.Type.LOCALE);
        }
        
        for (Resource.Type resourceType: resourceTypeAspects) {
            List<Resource> resources = new ArrayList<Resource>(
                board.getBlocks().getActivities().getResources(resourceType)
            );
            Collections.sort(resources, new MultiComparator<Resource>(new Labeled.LabeledComparator()));
            
            if (!resources.isEmpty()) {
                JSONObject resourcesGroupJsonObject = new JSONObject();
                
                resourcesGroupJsonObject.put("labelName", "labelName" + StringUtil.capitalize(resourceType.name()));
                resourcesGroupsJsonArray.add(resourcesGroupJsonObject);

                JSONArray resourcesItemsJsonArray = new JSONArray();
                resourcesGroupJsonObject.put("items", resourcesItemsJsonArray);
                
                for (Resource resource: resources) {
                    String resourceLabel = resource.getLabel();
                    String resourceName = addToSetUnique(nameSet, StringUtil.simplify(resourceLabel));
                    aspectNameMap.put(resource, resourceName);
                    
                    JSONObject resourceJsonObject = new JSONObject();
                    resourceJsonObject.put("name", resourceName);
                    resourceJsonObject.put("label", resourceLabel);
                    resourcesItemsJsonArray.add(resourceJsonObject);
                }
            }
        }
        
        List<Period> periods = new ArrayList<Period>(board.getPeriods());
        Collections.sort(periods, new MultiComparator<Period>(new Labeled.LabeledComparator()));
        
        if (periods.size() > 1) {
            JSONObject periodsSelectJsonObject = new JSONObject();
            periodsSelectJsonObject.put("labelName", "labelNamePeriod");
            selectsJsonArray.add(periodsSelectJsonObject);

            JSONArray periodsGroupsJsonArray = new JSONArray();
            periodsSelectJsonObject.put("groups", periodsGroupsJsonArray);

            JSONObject periodsGroupJsonObject = new JSONObject();
            periodsGroupJsonObject.put("labelName", "labelNamePeriod");
            periodsGroupsJsonArray.add(periodsGroupJsonObject);

            JSONArray periodsItemsJsonArray = new JSONArray();
            periodsGroupJsonObject.put("items", periodsItemsJsonArray);
            
            for (Period period: periods) {
                String periodLabel = period.getLabel();
                String periodName = addToSetUnique(nameSet, StringUtil.simplify(periodLabel));
                aspectNameMap.put(period, periodName);

                JSONObject periodJsonObject = new JSONObject();
                periodJsonObject.put("name", periodName);
                periodJsonObject.put("label", periodLabel);
                periodsItemsJsonArray.add(periodJsonObject);
            }
        }
        
        long firstDay = board.getStartTime().getPart(Time.PART.FULLDAYS);
        long lastDay = board.getEndTime().getPart(Time.PART.FULLDAYS);
        
        JSONArray daysJsonArray = new JSONArray();
        dataJsonObject.put("days", daysJsonArray);
        
        for (long day = firstDay; day <= lastDay; day++) {
            Board dayBoard = board.limitIntersection(new Interval(new Time(day * Time.DAY), new Time((day + 1) * Time.DAY)));
            
            JSONObject dayJsonObject = new JSONObject();
            daysJsonArray.add(dayJsonObject);
            
            JSONArray activitiesJsonArray = new JSONArray();
            dayJsonObject.put("activities", activitiesJsonArray);
            
            for (Board.Entry blockEntry: dayBoard) {
                Interval interval = blockEntry.getInterval();
                Block block = blockEntry.getBlock();
                Block.ActivityManager activityManager = block.getActivityManager();
                ActivityList blockActivities = activityManager.getActivities();
                for (Activity activity: blockActivities) {
                    JSONObject activityJsonObject = new JSONObject();
                    activityJsonObject.put("label", activity.getLabel());
                    activityJsonObject.put("start", interval.getStart().getPart(Time.PART.SECONDS_OF_DAY));
                    activityJsonObject.put("end", interval.getEnd().getPart(Time.PART.SECONDS_OF_DAY));
                    
                    JSONArray activityResourceNamesArray = new JSONArray();
                    
                    for (Resource resource: activity.getResourceManager().getResources()) {
                        String resourceName = aspectNameMap.get(resource);
                        if (resourceName != null) {
                            activityResourceNamesArray.add(resourceName);
                        }
                    }
                    
                    for (Period period: activityManager.getActivityPeriods(activity)) {
                        String periodName = aspectNameMap.get(period);
                        if (periodName != null) {
                            activityResourceNamesArray.add(periodName);
                        }
                    }
                    
                    if (!activityResourceNamesArray.isEmpty()) {
                        activitiesJsonArray.add(activityJsonObject);
                        activityJsonObject.put("tags", activityResourceNamesArray);
                    }
                }
            }
        }
        
        return dataJsonObject.toJSONString();
    }
    
    private String addToSetUnique(Set<String> set, String key) {
        String baseKey = key;
        for (int i = 2; set.contains(key); i++) {
            key = baseKey + "_" + i;
        }
        set.add(key);
        return key;
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import hu.webarticum.aurora.app.check.DefaultBlockProblemCollector;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualRadioButton;

// TODO/FIXME: order?
public class CheckBlocksDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private JRadioButton blockListAllRadioButton;
    
    private JRadioButton blockListSpecifiedRadioButton;

    private JButton blockListButton;

    private List<Block> selectedBlocks = new ArrayList<Block>();

    public CheckBlocksDialog(JFrame parent, Document document) {
        super(parent);
        this.title = text("ui.swing.dialog.checkblocks.title");
        this.document = document;
        init();
    }

    @Override
    protected void build() {
        ButtonGroup blockListTypeButtonGroup = new ButtonGroup();
        
        JPanel blockListAllPanel = new JPanel();
        blockListAllPanel.setLayout(new BorderLayout());
        
        blockListAllRadioButton = new MultilingualRadioButton("ui.swing.dialog.checkblocks.check_all_blocks");
        blockListAllRadioButton.setSelected(true);
        blockListAllPanel.add(blockListAllRadioButton, BorderLayout.CENTER);
        blockListTypeButtonGroup.add(blockListAllRadioButton);
        
        addRow(new MultilingualLabel("ui.swing.dialog.checkblocks.blocks"), blockListAllPanel);

        
        JPanel blockListSpecifiedPanel = new JPanel();
        blockListSpecifiedPanel.setLayout(new BorderLayout());
        
        blockListSpecifiedRadioButton = new JRadioButton();
        blockListSpecifiedPanel.add(blockListSpecifiedRadioButton, BorderLayout.LINE_START);
        blockListTypeButtonGroup.add(blockListSpecifiedRadioButton);
        
        blockListButton = new JButton("");
        blockListButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                BlockMultiSelectDialog dialog = new BlockMultiSelectDialog(parent, document, selectedBlocks);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    blockListSpecifiedRadioButton.setSelected(true);
                    reloadBlocks();
                }
            }
            
        });
        blockListSpecifiedPanel.add(blockListButton, BorderLayout.CENTER);
        addRow("", blockListSpecifiedPanel);
    }

    @Override
    protected void load() {
        reloadBlocks();
    }

    @Override
    protected void save() {
        new MessageCollectorRunner(parent, document, new DefaultBlockProblemCollector(getBlocks(), document)).run();
    }

    private void reloadBlocks() {
        blockListButton.setText(String.format(text("ui.swing.dialog.checkblocks.selected_blocks"), selectedBlocks.size()));
    }
    
    private List<Block> getBlocks() {
        if (blockListSpecifiedRadioButton.isSelected()) {
            return selectedBlocks;
        } else {
            return document.getBlockStore().getAll();
        }
    }
    
}

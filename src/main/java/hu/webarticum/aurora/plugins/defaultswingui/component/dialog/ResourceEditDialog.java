package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;

import hu.webarticum.aurora.app.memento.ResourceMemento;
import hu.webarticum.aurora.app.util.AcronymGenerator;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.ColorButton;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.ListReorderHandler;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.OnOffPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.TimeLimitManagerPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.TimingSetManagerPanel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.DocumentChangeListener;
import hu.webarticum.aurora.plugins.defaultswingui.util.FieldSynchronizer;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.chm.Command;


public class ResourceEditDialog extends LabeledEditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private Resource resource;
    
    private JTabbedPane mainTabbedPane;
    
    private JComboBox<Labeled.PairWrapper<Resource.Type>> typeComboBox;

    private JTextField acronymField;

    private JTextField emailField;
    
    private JSpinner quantitySpinner;
    
    private ColorButton colorButton;
    
    private JPanel splittingListContainer;
    
    private Map<Resource.Type, Labeled.PairWrapper<Resource.Type>> typeMap;
    
    private List<Resource.Splitting> splittings;

    private Map<Resource.Splitting, String> splittingRenames;

    private Map<Resource.Splitting, List<Resource.Splitting.Part>> splittingNewParts;

    private Map<Resource.Splitting.Part, String> splittingPartRenames;

    private OnOffPanel timingSetManagerOnOffPanel;
    
    private TimingSetManagerPanel timingSetManagerPanel;

    private OnOffPanel timeLimitManagerOnOffPanel;
    
    private TimeLimitManagerPanel timeLimitManagerPanel;
    
    private FieldSynchronizer acronymSynchronizer;
    
    public ResourceEditDialog(JFrame parent, Document document, Resource resource) {
        super(parent);
        this.title = text("ui.swing.dialog.resourceedit.title");
        this.document = document;
        this.resource = resource;
        typeMap = new EnumMap<Resource.Type, Labeled.PairWrapper<Resource.Type>>(Resource.Type.class);
        typeMap.put(Resource.Type.CLASS, new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.class"), Resource.Type.CLASS));
        typeMap.put(Resource.Type.PERSON, new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.person"), Resource.Type.PERSON));
        typeMap.put(Resource.Type.LOCALE, new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.locale"), Resource.Type.LOCALE));
        typeMap.put(Resource.Type.OBJECT, new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.object"), Resource.Type.OBJECT));
        typeMap.put(Resource.Type.OTHER, new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.other"), Resource.Type.OTHER));
        splittingRenames = new HashMap<Resource.Splitting, String>();
        splittingNewParts = new HashMap<Resource.Splitting, List<Resource.Splitting.Part>>();
        splittingPartRenames = new HashMap<Resource.Splitting.Part, String>();
        init();
    }
    
    @Override
    protected void build() {
        mainTabbedPane = new JTabbedPane();
        mainPanel.add(mainTabbedPane);
        
        JPanel tabPanel;
        JPanel tabInnerPanel;
        JButton button;
        
        int tabwidth = formWidth-30;

        tabPanel = new JPanel();
        tabPanel.setLayout(new BorderLayout());
        tabPanel.setBackground(new java.awt.Color(250, 247, 242));
        tabInnerPanel = new JPanel();
        tabInnerPanel.setOpaque(false);
        tabPanel.add(tabInnerPanel, BorderLayout.PAGE_START);
        tabInnerPanel.setLayout(new BoxLayout(tabInnerPanel, BoxLayout.PAGE_AXIS));
        tabInnerPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        mainTabbedPane.addTab(text("ui.swing.dialog.resourceedit.common_data"), tabPanel);

        labelField = new JTextField();
        labelField.setMaximumSize(new Dimension(700, 25));
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.resourceedit.common_data.label"), labelField, tabwidth, 150);

        acronymField = new JTextField();
        acronymField.setMaximumSize(new Dimension(700, 25));
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.resourceedit.common_data.acronym"), acronymField, tabwidth, 150);

        emailField = new JTextField();
        emailField.setMaximumSize(new Dimension(700, 25));
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.resourceedit.common_data.email"), emailField, tabwidth, 150);
        
        acronymSynchronizer = new FieldSynchronizer() {
            
            @Override
            public String generateSecondaryValue(String primaryValue) {
                return AcronymGenerator.generateAcronym(primaryValue);
            }
            
        };
        acronymSynchronizer.bind(labelField, acronymField);
        
        typeComboBox = new JComboBox<Labeled.PairWrapper<Resource.Type>>();
        typeComboBox.setMaximumSize(new Dimension(700, 25));
        typeComboBox.addItem(typeMap.get(Resource.Type.CLASS));
        typeComboBox.addItem(typeMap.get(Resource.Type.PERSON));
        typeComboBox.addItem(typeMap.get(Resource.Type.LOCALE));
        typeComboBox.addItem(typeMap.get(Resource.Type.OBJECT));
        typeComboBox.addItem(typeMap.get(Resource.Type.OTHER));
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.resourceedit.common_data.type"), typeComboBox, tabwidth, 150);

        JPanel quantityPanel = new JPanel();
        quantityPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        quantityPanel.setOpaque(false);
        quantitySpinner = new JSpinner();
        quantitySpinner.setPreferredSize(new Dimension(70, 25));
        SpinnerNumberModel quantitySpinnerModel = new SpinnerNumberModel();
        quantitySpinnerModel.setValue(1);
        quantitySpinnerModel.setMinimum(1);
        quantitySpinner.setModel(quantitySpinnerModel);
        quantityPanel.add(quantitySpinner);
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.resourceedit.common_data.quantity"), quantityPanel, tabwidth, 150);
        
        colorButton = new ColorButton(parent);
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.resourceedit.common_data.color"), colorButton, tabwidth, 150);

        addSpaceTo(tabInnerPanel);
        
        if (document.getResourceStore().contains(resource)) {
            JButton showStatisticsButton = new MultilingualButton("ui.swing.dialog.resourceedit.common_data.statistics");
            showStatisticsButton.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    new ResourceStatisticsDialog(parent, document, resource).run();
                }
                
            });
            addRowTo(tabInnerPanel, "", showStatisticsButton, tabwidth, 150);
        }

        
        // new tab
        
        tabPanel = new JPanel();
        tabPanel.setLayout(new BorderLayout());
        tabPanel.setBackground(new java.awt.Color(250, 247, 242));
        tabInnerPanel = new JPanel();
        tabInnerPanel.setOpaque(false);
        tabPanel.add(tabInnerPanel, BorderLayout.PAGE_START);
        tabInnerPanel.setLayout(new BoxLayout(tabInnerPanel, BoxLayout.PAGE_AXIS));
        tabInnerPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
        mainTabbedPane.addTab(text("ui.swing.dialog.resourceedit.splittings"), tabPanel);

        JPanel splittingOuterPanel = new JPanel();
        splittingOuterPanel.setLayout(new BorderLayout());
        splittingOuterPanel.setBorder(new LineBorder(java.awt.Color.BLACK));
        tabInnerPanel.add(splittingOuterPanel);
        
        JPanel splittingHeadPanel = new JPanel();
        splittingHeadPanel.setLayout(new BorderLayout());
        splittingHeadPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        splittingOuterPanel.add(splittingHeadPanel, BorderLayout.PAGE_START);

        splittingHeadPanel.add(new MultilingualLabel("ui.swing.dialog.resourceedit.splittings"), BorderLayout.LINE_START);
        button = new JButton();
        button.setIcon(IconLoader.loadIcon("add"));
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                String splittingLabel = OptionPaneUtil.showInputDialog(
                    parent, text("ui.swing.dialog.resourceedit.splittings.label_of_new_splitting"), ButtonPurpose.SAVE
                );
                if (splittingLabel==null || splittingLabel.length()==0) {
                    return;
                }
                
                modified = true;
                Resource.Splitting splitting = resource.new Splitting(splittingLabel, new String[]{});
                splittings.add(splitting);
                splittingNewParts.put(splitting, new ArrayList<Resource.Splitting.Part>());
                refreshSplittingListContainer();
            }
            
        });
        splittingHeadPanel.add(button, BorderLayout.LINE_END);
        
        JPanel splittingListContainerOuter = new JPanel();
        splittingListContainerOuter.setLayout(new BorderLayout());
        splittingListContainerOuter.setBackground(java.awt.Color.WHITE);
        JScrollPane splittingScrollPane = new JScrollPane(splittingListContainerOuter);
        splittingScrollPane.getVerticalScrollBar().setUnitIncrement(10);
        splittingOuterPanel.add(splittingScrollPane, BorderLayout.CENTER);
        splittingScrollPane.setPreferredSize(new Dimension(200, 350));
        
        splittingListContainer = new JPanel();
        splittingListContainer.setLayout(new BoxLayout(splittingListContainer, BoxLayout.PAGE_AXIS));
        splittingListContainer.setBackground(java.awt.Color.WHITE);
        splittingListContainerOuter.add(splittingListContainer, BorderLayout.PAGE_START);
        
        
        // new tab
        
        tabPanel = new JPanel();
        tabPanel.setLayout(new BorderLayout());
        tabPanel.setBackground(new java.awt.Color(250, 247, 242));
        tabInnerPanel = new JPanel();
        tabInnerPanel.setOpaque(false);
        tabPanel.add(tabInnerPanel, BorderLayout.PAGE_START);
        tabInnerPanel.setLayout(new BoxLayout(tabInnerPanel, BoxLayout.PAGE_AXIS));
        tabInnerPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
        mainTabbedPane.addTab(text("ui.swing.dialog.resourceedit.schedule"), tabPanel);

        timingSetManagerPanel = new TimingSetManagerPanel(parent, document);
        timingSetManagerOnOffPanel = new OnOffPanel(
            text("ui.swing.dialog.resourceedit.schedule.timingset"),
            text("ui.swing.dialog.resourceedit.schedule.timingset.on"),
            timingSetManagerPanel
        );
        tabInnerPanel.add(timingSetManagerOnOffPanel);

        timingSetManagerPanel.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent ev) {
                if (loaded && !timingSetManagerPanel.isEmpty()) {
                    timingSetManagerOnOffPanel.setOn(true);
                }
            }
            
        });

        JPanel spacerPanel = new JPanel();
        spacerPanel.setOpaque(false);
        spacerPanel.setPreferredSize(new Dimension(12, 12));
        tabInnerPanel.add(spacerPanel);
        
        timeLimitManagerPanel = new TimeLimitManagerPanel(parent, document, resource);
        timeLimitManagerOnOffPanel = new OnOffPanel(
            text("ui.swing.dialog.resourceedit.schedule.timelimit"),
            text("ui.swing.dialog.resourceedit.schedule.timelimit.on"),
            timeLimitManagerPanel
        );
        tabInnerPanel.add(timeLimitManagerOnOffPanel);
        
        timeLimitManagerPanel.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent ev) {
                if (loaded && !timeLimitManagerPanel.isEmpty()) {
                    timeLimitManagerOnOffPanel.setOn(true);
                }
            }
            
        });
    }

    @Override
    protected void load() {
        labelField.setText(resource.getLabel());
        acronymField.setText(resource.getAcronym());
        emailField.setText(resource.getEmail());
        typeComboBox.setSelectedItem(typeMap.get(resource.getType()));
        quantitySpinner.setValue(resource.getQuantity());
        colorButton.setSelectedColor(resource.getColor());
        
        List<Resource.Splitting> resourceSplittings = resource.getSplittingManager().getSplittings();
        splittings = new ArrayList<Resource.Splitting>(resourceSplittings);
        
        for (Resource.Splitting splitting: splittings) {
            splittingNewParts.put(splitting, new ArrayList<Resource.Splitting.Part>(splitting.getParts()));
        }
        
        refreshSplittingListContainer();

        timingSetManagerOnOffPanel.setOn(resource.isTimingSetEnabled());
        timingSetManagerPanel.loadFrom(resource.getTimingSetManager());
        
        timeLimitManagerOnOffPanel.setOn(resource.isTimeLimitEnabled());
        timeLimitManagerPanel.loadFrom(resource.getTimeLimitManager());
    }

    
    @Override
    protected void afterLoad() {
        super.afterLoad();

        bindModifyListener(acronymField);
        bindModifyListener(emailField);
        bindModifyListener(typeComboBox);
        bindModifyListener(quantitySpinner);
        bindModifyListener(colorButton);
        bindModifyListener(timingSetManagerOnOffPanel);
        bindModifyListener(timingSetManagerPanel);
        bindModifyListener(timeLimitManagerOnOffPanel);
        bindModifyListener(timeLimitManagerPanel);
    }
    

    private void refreshSplittingListContainer() {
        splittingListContainer.removeAll();
        
        int boxMargin = 6;
        int splittingCount = splittings.size();
        for (int i = 0; i < splittingCount; i++) {
            final int index = i;
            
            final Resource.Splitting splitting = splittings.get(index);
            boolean isFirst = (index == 0);
            boolean isLast = (index == splittingCount - 1);
            
            JPanel outerBox = new JPanel();
            outerBox.setOpaque(false);
            outerBox.setLayout(new BorderLayout());
            outerBox.setBorder(new EmptyBorder(boxMargin, boxMargin, 0, boxMargin));
            splittingListContainer.add(outerBox);
            
            JPanel box = new JPanel();
            box.setLayout(new GridLayout(1, 2));
            box.setBorder(new LineBorder(java.awt.Color.BLACK));
            outerBox.add(box, BorderLayout.CENTER);
            
            
            
            JPanel leftPanel = new JPanel();
            leftPanel.setLayout(new BorderLayout());
            leftPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
            box.add(leftPanel, 0);
            
            JPanel leftTopPanel = new JPanel();
            leftTopPanel.setLayout(new BoxLayout(leftTopPanel, BoxLayout.PAGE_AXIS));
            leftPanel.add(leftTopPanel, BorderLayout.PAGE_START);
            
            JPanel leftBottomPanel = new JPanel();
            leftBottomPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
            leftPanel.add(leftBottomPanel, BorderLayout.PAGE_END);

            JPanel rightPanel = new JPanel();
            rightPanel.setLayout(new BorderLayout());
            rightPanel.setBorder(new EmptyBorder(5, 0, 5, 5));
            box.add(rightPanel, 1);
            
            JPanel rightTopPanel = new JPanel();
            rightTopPanel.setLayout(new BorderLayout());
            rightPanel.add(rightTopPanel, BorderLayout.PAGE_START);
            
            JPanel rightMainPanel = new JPanel();
            rightMainPanel.setLayout(new BorderLayout());
            rightPanel.add(rightMainPanel, BorderLayout.CENTER);
            
            
            leftTopPanel.add(new MultilingualLabel("ui.swing.dialog.resourceedit.splittings.splitting.label_of_splitting"));
            
            String splittingLabel = splitting.getLabel();
            if (splittingRenames.containsKey(splitting)) {
                splittingLabel = splittingRenames.get(splitting);
            }
            final JTextField labelTextField = new JTextField(splittingLabel);
            labelTextField.getDocument().addDocumentListener(new DocumentChangeListener() {
                
                @Override
                public void changed(DocumentEvent ev) {
                    modified = true;
                    splittingRenames.put(splitting, labelTextField.getText());
                }
                
            });
            leftTopPanel.add(labelTextField);

            if (!isFirst) {
                JButton moveUpButton = new JButton("\u2191");
                moveUpButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        modified = true;
                        splittings.remove(index);
                        splittings.add(index - 1, splitting);
                        refreshSplittingListContainer();
                    }
                    
                });
                leftBottomPanel.add(moveUpButton);
            }

            if (!isLast) {
                JButton moveDownButton = new JButton("\u2193");
                moveDownButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        modified = true;
                        splittings.remove(index);
                        splittings.add(index + 1, splitting);
                        refreshSplittingListContainer();
                    }
                    
                });
                leftBottomPanel.add(moveDownButton);
            }
            
            JButton removeButton = new JButton();
            removeButton.setIcon(IconLoader.loadIcon("trash"));
            removeButton.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    int answer = OptionPaneUtil.showOptionDialog(parent,
                        text("ui.swing.dialog.resourceedit.splittings.splitting.delete_splitting.sure"),
                        text("ui.swing.dialog.resourceedit.splittings.splitting.delete_splitting.confirm"),
                        JOptionPane.QUESTION_MESSAGE,
                        new String[]{
                            text("ui.swing.dialog.resourceedit.splittings.splitting.delete_splitting.cancel"),
                            text("ui.swing.dialog.resourceedit.splittings.splitting.delete_splitting.delete"),
                        },
                        new ButtonPurpose[] {
                            ButtonPurpose.NONE,
                            ButtonPurpose.DANGER,
                        },
                        text("ui.swing.dialog.resourceedit.splittings.splitting.delete_splitting.delete")
                    );
                    if (answer==1) {
                        modified = true;
                        splittings.remove(splitting);
                        refreshSplittingListContainer();
                    }
                }
                
            });
            leftBottomPanel.add(removeButton);
            

            rightTopPanel.add(new MultilingualLabel("ui.swing.dialog.resourceedit.splittings.splitting.splittingparts"), BorderLayout.LINE_START);
            
            JButton addPartButton = new JButton();
            addPartButton.setIcon(IconLoader.loadIcon("add"));
            addPartButton.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    String partLabel = OptionPaneUtil.showInputDialog(
                        parent,
                        text("ui.swing.dialog.resourceedit.splittings.splitting.splittingparts.label_of_new_splittingpart"),
                        ButtonPurpose.SAVE
                    );
                    if (partLabel==null || partLabel.length()==0) {
                        return;
                    }
                    
                    modified = true;
                    splittingNewParts.get(splitting).add(splitting.new Part(partLabel));
                    refreshSplittingListContainer();
                }
                
            });
            rightTopPanel.add(addPartButton, BorderLayout.LINE_END);
            
            List<Resource.Splitting.Part> parts = splittingNewParts.get(splitting);
            final JList<Labeled.PairWrapper<Resource.Splitting.Part>> partList = new JList<Labeled.PairWrapper<Resource.Splitting.Part>>();
            DefaultListModel<Labeled.PairWrapper<Resource.Splitting.Part>> partListModel = new DefaultListModel<Labeled.PairWrapper<Resource.Splitting.Part>>();
            ListReorderHandler<Labeled.PairWrapper<Resource.Splitting.Part>> reorderHandler = new ListReorderHandler<Labeled.PairWrapper<Resource.Splitting.Part>>(partList);
            reorderHandler.addListener(new ListReorderHandler.ReorderListener() {

                @Override
                public void onMoved(int sourceIndex, int targetIndex) {
                    modified = true;
                    Resource.Splitting.Part part = parts.remove(sourceIndex);
                    parts.add(targetIndex, part);
                }
                
            });
            reorderHandler.enable();
            for (Resource.Splitting.Part part: parts) {
                String label = part.getLabel();
                if (splittingPartRenames.containsKey(part)) {
                    label = splittingPartRenames.get(part);
                }
                partListModel.addElement(new Labeled.PairWrapper<Resource.Splitting.Part>(new Labeled.StringLabeled(label), part));
            }
            partList.setModel(partListModel);
            partList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            final Runnable removeAction = new Runnable() {
                
                @Override
                public void run() {
                    int selectedIndex = partList.getSelectedIndex();
                    if (selectedIndex<0) {
                        return;
                    }
                    Resource.Splitting.Part part = partList.getModel().getElementAt(selectedIndex).get();
                    int answer = OptionPaneUtil.showOptionDialog(parent,
                        text("ui.swing.dialog.resourceedit.splittings.splitting.splittingparts.delete_splittingpart.sure"),
                        text("ui.swing.dialog.resourceedit.splittings.splitting.splittingparts.delete_splittingpart.confirm"),
                        JOptionPane.QUESTION_MESSAGE,
                        new String[]{
                            text("ui.swing.dialog.resourceedit.splittings.splitting.splittingparts.delete_splittingpart.cancel"),
                            text("ui.swing.dialog.resourceedit.splittings.splitting.splittingparts.delete_splittingpart.delete"),
                        },
                        new ButtonPurpose[] {
                            ButtonPurpose.NONE,
                            ButtonPurpose.DANGER,
                        },
                        text("ui.swing.dialog.resourceedit.splittings.splitting.splittingparts.delete_splittingpart.remove")
                    );
                    if (answer==1) {
                        modified = true;
                        splittingNewParts.get(splitting).remove(part);
                        refreshSplittingListContainer();
                    }
                }
                
            };
            final Runnable editAction = new Runnable() {
                
                @Override
                public void run() {
                    int selectedIndex = partList.getSelectedIndex();
                    if (selectedIndex<0) {
                        return;
                    }
                    Resource.Splitting.Part part = partList.getModel().getElementAt(selectedIndex).get();
                    String oldLabel = part.getLabel();
                    if (splittingPartRenames.containsKey(part)) {
                        oldLabel = splittingPartRenames.get(part);
                    }
                    String newLabel = OptionPaneUtil.showInputDialog(
                        parent,
                        text("ui.swing.dialog.resourceedit.splittings.splitting.splittingparts.label_of_splittingpart"),
                        oldLabel,
                        ButtonPurpose.SAVE
                    );
                    if (newLabel==null) {
                        return;
                    }

                    modified = true;
                    splittingPartRenames.put(part, newLabel);
                    refreshSplittingListContainer();
                }
                
            };
            partList.addMouseListener(new MouseListener() {
                
                @Override
                public void mouseReleased(MouseEvent ev) {
                }
                
                @Override
                public void mousePressed(MouseEvent ev) {
                }
                
                @Override
                public void mouseExited(MouseEvent ev) {
                }
                
                @Override
                public void mouseEntered(MouseEvent ev) {
                }
                
                @Override
                public void mouseClicked(MouseEvent ev) {
                    if (ev.getButton()==MouseEvent.BUTTON3) {
                        removeAction.run();
                    } else if (ev.getButton()==MouseEvent.BUTTON1 && ev.getClickCount()==2) {
                        editAction.run();
                    }
                }
                
            });
            partList.addKeyListener(new KeyListener() {
                
                @Override
                public void keyTyped(KeyEvent ev) {
                }
                
                @Override
                public void keyReleased(KeyEvent ev) {
                }
                
                @Override
                public void keyPressed(KeyEvent ev) {
                    switch (ev.getKeyCode()) {
                        case KeyEvent.VK_DELETE:
                            removeAction.run();
                        break;
                        case KeyEvent.VK_ENTER:
                            editAction.run();
                        break;
                    }
                }
                
            });
            JScrollPane partListScrollPane = new JScrollPane(partList);
            partListScrollPane.setPreferredSize(new Dimension(100, 60));
            //partListScrollPane. // XXX
            rightMainPanel.add(partListScrollPane, BorderLayout.CENTER);
        }
        
        splittingListContainer.getTopLevelAncestor().validate();
        splittingListContainer.getTopLevelAncestor().repaint();
    }

    
    @SuppressWarnings("unchecked")
    @Override
    protected void save() {
        if (!modified) {
            return;
        }
        
        List<Pair<Resource.Splitting, ResourceMemento.SplittingMemento>> splittingMementoPairs = new ArrayList<Pair<Resource.Splitting, ResourceMemento.SplittingMemento>>();
        for (Resource.Splitting splitting: splittings) {
            String splittingLabel = splitting.getLabel();
            if (splittingRenames.containsKey(splitting)) {
                splittingLabel = splittingRenames.get(splitting);
            }
            
            List<Pair<Resource.Splitting.Part, ResourceMemento.SplittingPartMemento>> splittingPartMementoPairs = new ArrayList<Pair<Resource.Splitting.Part, ResourceMemento.SplittingPartMemento>>();
            for (Resource.Splitting.Part splittingPart: splittingNewParts.get(splitting)) {
                String splittingPartLabel = splittingPart.getLabel();
                if (splittingPartRenames.containsKey(splittingPart)) {
                    splittingPartLabel = splittingPartRenames.get(splittingPart);
                }
                
                ResourceMemento.SplittingPartMemento splittingPartMemento = new ResourceMemento.SplittingPartMemento(splittingPartLabel);
                splittingPartMementoPairs.add(new Pair<Resource.Splitting.Part, ResourceMemento.SplittingPartMemento>(splittingPart, splittingPartMemento));
            }
            
            ResourceMemento.SplittingMemento splittingMemento = new ResourceMemento.SplittingMemento(splittingLabel, splittingPartMementoPairs);
            splittingMementoPairs.add(new Pair<Resource.Splitting, ResourceMemento.SplittingMemento>(splitting, splittingMemento));
        }
        
        ResourceMemento newMemento = new ResourceMemento(
            labelField.getText(), acronymField.getText(), emailField.getText(),
            colorButton.getSelectedColor(),
            timingSetManagerOnOffPanel.isOn(), timeLimitManagerOnOffPanel.isOn(),
            new ResourceMemento.SplittingManagerMemento(splittingMementoPairs),
            timingSetManagerPanel.createMemento(),
            timeLimitManagerPanel.createMemento(),
            ((Labeled.PairWrapper<Resource.Type>)typeComboBox.getSelectedItem()).get(),
            (Integer)quantitySpinner.getValue()
        );
        
        Command command;
        Document.ResourceStore resourceStore = document.getResourceStore();
        
        if (resourceStore.contains(resource)) {
            ResourceMemento oldMemento = new ResourceMemento(resource);
            command = new StoreItemEditCommand<Resource>(
                "ui.swing.dialog.resourceedit.command.edit",
                resourceStore, resource, oldMemento, newMemento
            );
        } else {
            newMemento.apply(resource);
            command = new StoreItemRegisterCommand<Resource>(
                "ui.swing.dialog.resourceedit.command.register",
                resourceStore, resource
            );
        }
        
        history().addAndExecute(command);
    }
    
}

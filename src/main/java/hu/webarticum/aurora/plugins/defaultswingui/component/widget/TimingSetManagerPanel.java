package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import hu.webarticum.aurora.app.memento.AspectMementoBase;
import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.MultiComparator;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.EditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.OptionPaneUtil;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.PeriodSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.TimingSetSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class TimingSetManagerPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private JFrame parent;

    private Document document;
    
    private TimingSet defaultTimingSet = null;
    
    private JPanel defaultSelectPanel;
    
    private CardLayout defaultSelectCardLayout;

    private static final String DEFAULTSELECTCARDLAYOUT_NOTSELECTED = "notselected";

    private static final String DEFAULTSELECTCARDLAYOUT_SELECTED = "selected";
    
    private JButton selectedButton;
    
    private TreeMap<Period, TimingSet> periodMap = new TreeMap<Period, TimingSet>(new MultiComparator<Period>(new Labeled.LabeledComparator()));
    
    private JList<Labeled.Wrapper> periodList;
    
    private EventListenerList changeListenerList = new EventListenerList();
    
    public TimingSetManagerPanel(JFrame parent, Document document) {
        this.parent = parent;
        this.document = document;
        build();
    }
    
    private void build() {
        setLayout(new BorderLayout());
        setBorder(new CompoundBorder(
            new LineBorder(new java.awt.Color(150, 150, 150)),
            new EmptyBorder(7, 7, 7, 7)
        ));
        
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        add(topPanel, BorderLayout.PAGE_START);

        topPanel.add(new MultilingualLabel("ui.swing.widget.timingsetmanager.default"), BorderLayout.LINE_START);
        
        defaultSelectPanel = new JPanel();
        defaultSelectCardLayout = new CardLayout();
        defaultSelectPanel.setLayout(defaultSelectCardLayout);
        topPanel.add(defaultSelectPanel, BorderLayout.LINE_END);
        
        JPanel defaultSelectNotSelectedPanel = new JPanel();
        defaultSelectNotSelectedPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        defaultSelectPanel.add(defaultSelectNotSelectedPanel, DEFAULTSELECTCARDLAYOUT_NOTSELECTED);
        JButton notSelectedButton = new MultilingualButton("ui.swing.widget.timingsetmanager.not_selected");
        notSelectedButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<TimingSet> wrapper = new GeneralWrapper<TimingSet>();
                TimingSetSelectDialog dialog = new TimingSetSelectDialog(parent, document, wrapper);
                dialog.run();

                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    defaultTimingSet = wrapper.get();
                    
                    refreshDefault();
                    fireChange();
                }
            }
            
        });
        defaultSelectNotSelectedPanel.add(notSelectedButton);
        
        JPanel defaultSelectSelectedPanel = new JPanel();
        defaultSelectSelectedPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        defaultSelectPanel.add(defaultSelectSelectedPanel, DEFAULTSELECTCARDLAYOUT_SELECTED);
        selectedButton = new JButton("?");
        selectedButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<TimingSet> wrapper = new GeneralWrapper<TimingSet>(defaultTimingSet);
                TimingSetSelectDialog dialog = new TimingSetSelectDialog(parent, document, wrapper);
                dialog.run();

                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    defaultTimingSet = wrapper.get();
                    
                    refreshDefault();
                    fireChange();
                }
            }
            
        });
        defaultSelectSelectedPanel.add(selectedButton);
        JButton removeSelectedButton = new JButton();
        removeSelectedButton.setIcon(IconLoader.loadIcon("abort"));
        removeSelectedButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(parent,
                    text("ui.swing.widget.timingsetmanager.remove_default.sure"),
                    text("ui.swing.widget.timingsetmanager.remove_default.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.widget.timingsetmanager.remove_default.cancel"),
                        text("ui.swing.widget.timingsetmanager.remove_default.remove"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.DANGER,
                    },
                    text("ui.swing.widget.timingsetmanager.remove_default.remove")
                );
                if (answer==1) {
                    defaultTimingSet = null;
                    
                    refreshDefault();
                    fireChange();
                }
            }
            
        });
        defaultSelectSelectedPanel.add(removeSelectedButton);

        defaultSelectCardLayout.show(defaultSelectPanel, DEFAULTSELECTCARDLAYOUT_NOTSELECTED);
        
        JPanel periodPanel = new JPanel();
        periodPanel.setLayout(new BorderLayout());
        add(periodPanel, BorderLayout.CENTER);
        
        JPanel periodTopPanel = new JPanel();
        periodTopPanel.setLayout(new BorderLayout());
        periodPanel.add(periodTopPanel, BorderLayout.PAGE_START);

        periodTopPanel.add(new MultilingualLabel("ui.swing.widget.timingsetmanager.per_period"), BorderLayout.LINE_START);
        
        JButton addButton = new JButton();
        addButton.setIcon(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Period> periodWrapper = new GeneralWrapper<Period>();
                PeriodSelectDialog periodDialog = new PeriodSelectDialog(parent, document, periodWrapper);
                periodDialog.run();
                
                if (periodDialog.getResult()!=EditDialog.RESULT_OK) {
                    return;
                }

                GeneralWrapper<TimingSet> timingSetWrapper = new GeneralWrapper<TimingSet>();
                TimingSetSelectDialog timingSetDialog = new TimingSetSelectDialog(parent, document, timingSetWrapper);
                timingSetDialog.run();

                if (timingSetDialog.getResult()!=EditDialog.RESULT_OK) {
                    return;
                }
                
                periodMap.put(periodWrapper.get(), timingSetWrapper.get());
                
                refreshPeriodList();
                fireChange();
            }
            
        });
        periodTopPanel.add(addButton, BorderLayout.LINE_END);
        
        JPanel periodListPanel = new JPanel();
        periodListPanel.setLayout(new BorderLayout());
        periodPanel.add(periodListPanel, BorderLayout.CENTER);
        
        periodList = new JList<Labeled.Wrapper>();
        JScrollPane periodListScrollPane = new JScrollPane(periodList);
        periodPanel.add(periodListScrollPane, BorderLayout.CENTER);
        periodListScrollPane.setPreferredSize(new Dimension(100, 50));
        final Runnable removeAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = periodList.getSelectedIndex();
                if (selectedIndex<0) {
                    return;
                }
                Period period = (Period)periodList.getModel().getElementAt(selectedIndex).get();
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(parent,
                    text("ui.swing.widget.timingsetmanager.per_period.remove.sure"),
                    text("ui.swing.widget.timingsetmanager.per_period.remove.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.widget.timingsetmanager.per_period.remove.cancel"),
                        text("ui.swing.widget.timingsetmanager.per_period.remove.remove"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.DANGER,
                    },
                    text("ui.swing.widget.timingsetmanager.per_period.remove.remove")
                );
                if (answer==1) {
                    periodMap.remove(period);
                    
                    refreshPeriodList();
                    fireChange();
                }
            }
            
        };
        final Runnable editAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = periodList.getSelectedIndex();
                if (selectedIndex<0) {
                    return;
                }
                Period period = (Period)periodList.getModel().getElementAt(selectedIndex).get();
                GeneralWrapper<TimingSet> timingSetWrapper = new GeneralWrapper<TimingSet>(periodMap.get(period));
                TimingSetSelectDialog timingSetDialog = new TimingSetSelectDialog(parent, document, timingSetWrapper);
                timingSetDialog.run();

                if (timingSetDialog.getResult()==EditDialog.RESULT_OK) {
                    periodMap.put(period, timingSetWrapper.get());
                    
                    refreshPeriodList();
                    fireChange();
                }
            }
            
        };
        periodList.addMouseListener(new MouseListener() {
            
            @Override
            public void mouseReleased(MouseEvent ev) {}
            
            @Override
            public void mousePressed(MouseEvent ev) {}
            
            @Override
            public void mouseExited(MouseEvent ev) {}
            
            @Override
            public void mouseEntered(MouseEvent ev) {}
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getButton()==MouseEvent.BUTTON3) {
                    removeAction.run();
                } else if (ev.getButton()==MouseEvent.BUTTON1 && ev.getClickCount()==2) {
                    editAction.run();
                }
            }
        });
        periodList.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {

                switch (ev.getKeyCode()) {
                    case KeyEvent.VK_DELETE:
                        removeAction.run();
                    break;
                    case KeyEvent.VK_ENTER:
                        editAction.run();
                    break;
                }
            }
            
        });
    }

    public void loadFrom(Aspect.TimingSetManager timingSetManager) {
        defaultTimingSet = timingSetManager.getDefaultTimingSet();
        
        periodMap.clear();
        for (Map.Entry<Period, TimingSet> entry: timingSetManager.getPeriodTimingSets().entrySet()) {
            periodMap.put(entry.getKey(), entry.getValue());
        }
        
        refreshDefault();
        refreshPeriodList();
        fireChange();
    }

    public void saveTo(Aspect.TimingSetManager timingSetManager) {
        timingSetManager.setDefaultTimingSet(defaultTimingSet);
        
        timingSetManager.removePeriodTimingSets();
        for (Map.Entry<Period, TimingSet> entry: periodMap.entrySet()) {
            timingSetManager.setPeriodTimingSet(entry.getKey(), entry.getValue());
        }
    }

    public AspectMementoBase.TimingSetManagerMemento createMemento() {
        return new AspectMementoBase.TimingSetManagerMemento(defaultTimingSet, getPeriodTimingSetMap());
    }

    public TimingSet getDefaultTimingSet() {
        return defaultTimingSet;
    }
    
    public TreeMap<Period, TimingSet> getPeriodTimingSetMap() {
        return new TreeMap<Period, TimingSet>(periodMap);
    }

    public void refreshDefault() {
        if (defaultTimingSet==null) {
            defaultSelectCardLayout.show(defaultSelectPanel, DEFAULTSELECTCARDLAYOUT_NOTSELECTED);
        } else {
            defaultSelectCardLayout.show(defaultSelectPanel, DEFAULTSELECTCARDLAYOUT_SELECTED);
            String label = defaultTimingSet.getLabel();
            if (label.length()>16) {
                label = label.substring(0, 15)+"…";
            }
            selectedButton.setText(label);
        }
    }
    
    public void refreshPeriodList() {
        DefaultListModel<Labeled.Wrapper> model = new DefaultListModel<Labeled.Wrapper>();
        for (Period period: periodMap.keySet()) {
            model.addElement(new Labeled.Wrapper(period));
        }
        periodList.setModel(model);
    }

    public boolean isEmpty() {
        if (defaultTimingSet!=null && !defaultTimingSet.isEmpty()) {
            return false;
        } else if (!periodMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    
    public void addChangeListener(ChangeListener changeListener) {
        changeListenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        changeListenerList.remove(ChangeListener.class, changeListener);
    }
    
    public void fireChange() {
        ChangeListener[] listeners = changeListenerList.getListeners(ChangeListener.class);
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (ChangeListener listener: listeners) {
            listener.stateChanged(changeEvent);
        }
    }
    
}

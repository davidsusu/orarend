package hu.webarticum.aurora.plugins.defaultswingui.util;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.email.SimpleMessageSender;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.OptionPaneUtil;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.SwingWorkerProcessDialog;

public class EmailTestRunner implements Runnable {

    private final JFrame parent;

    private final SimpleMessageSender simpleMessageSender;
    
    
    public EmailTestRunner(JFrame parent, SimpleMessageSender simpleMessageSender) {
        this.parent = parent;
        this.simpleMessageSender = simpleMessageSender;
    }
    
    @Override
    public void run() {
        new EmailTestWorkerProcessDialog(parent).run();
    }
    
    
    private class EmailTestWorkerProcessDialog extends SwingWorkerProcessDialog {

        private static final long serialVersionUID = 1L;

        
        EmailTestWorkerProcessDialog(JFrame parent) {
            super(parent, true);
        }
        
        
        @Override
        protected Worker createWorker() {
            return new EmailTestWorker();
        }
        
        private class EmailTestWorker extends Worker {
            
            @Override
            public void _run() throws CancelledException {
                try {
                    simpleMessageSender.test();
                } catch (IOException e) {
                    OptionPaneUtil.showMessageDialog(
                        parent,
                        text("ui.swing.util.emailtestrunner.error.message") + "\n\n" + e.getMessage(),
                        text("ui.swing.util.emailtestrunner.error.title"),
                        JOptionPane.ERROR_MESSAGE
                    );
                    return;
                }
    
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.util.emailtestrunner.success.message"),
                    text("ui.swing.util.emailtestrunner.success.title"),
                    JOptionPane.INFORMATION_MESSAGE
                );
            }
            
            @Override
            public void _rollBack() {
            }
    
            @Override
            public void processCommand(Command command) {
                super.processCommand(command);
                if (command instanceof TerminatedCommand) {
                    closeDialog();
                }
            }
            
        }
        
    }
    
}

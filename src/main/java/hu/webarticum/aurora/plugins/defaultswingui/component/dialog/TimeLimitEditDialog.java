package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.time.AlwaysTimeLimit;
import hu.webarticum.aurora.core.model.time.CustomTimeLimit;
import hu.webarticum.aurora.core.model.time.NeverTimeLimit;
import hu.webarticum.aurora.core.model.time.TimeLimit;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.timelimiteditor.TimeLimitEditorPanel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class TimeLimitEditDialog extends EditDialog {
    
    private static final long serialVersionUID = 1L;
    
    
    private GeneralWrapper<TimeLimit> timeLimitWrapper;
    
    private Document document;
    
    private Labeled contextObject;
    
    private boolean editable;
    
    private JButton undoButton;
    
    private JButton redoButton;
    
    private JButton setAlwaysButton;
    
    private JButton setNeverButton;
    
    private TimeLimitEditorPanel timeLimitEditorPanel;

    public TimeLimitEditDialog(JFrame parent, GeneralWrapper<TimeLimit> timeLimitWrapper, Document document) {
        this(parent, timeLimitWrapper, document, null);
    }
    
    public TimeLimitEditDialog(JFrame parent, GeneralWrapper<TimeLimit> timeLimitWrapper, Document document, Labeled contextObject) {
        this(parent, timeLimitWrapper, document, contextObject, true);
    }

    public TimeLimitEditDialog(JFrame parent, GeneralWrapper<TimeLimit> timeLimitWrapper, Document document, boolean editable) {
        this(parent, timeLimitWrapper, document, null, editable);
    }
    
    public TimeLimitEditDialog(JFrame parent, GeneralWrapper<TimeLimit> timeLimitWrapper, Document document, Labeled contextObject, boolean editable) {
        super(parent);
        this.timeLimitWrapper = timeLimitWrapper;
        this.document = document;
        this.contextObject = contextObject;
        this.editable = editable;
        
        init();
    }

    @Override
    public String getTitle() {
        String generatedTitle = editable ?
            text("ui.swing.dialog.timelimitedit.title") :
            text("ui.swing.dialog.timelimitedit.readonly_title")
        ;
        if (contextObject != null) {
            generatedTitle += String.format(" (%s)", contextObject.getLabel());
        }
        return generatedTitle;
    }
    
    @Override
    protected void build() {
        if (editable) {
            JToolBar toolBar = new JToolBar("Toolbar", JToolBar.HORIZONTAL);
            toolBar.setFloatable(false);
            containerPanel.add(toolBar, BorderLayout.PAGE_START);
            
            undoButton = new MultilingualButton(
                IconLoader.loadIcon("undo"), "ui.swing.dialog.timelimitedit.toolbar.undo"
            );
            toolBar.add(undoButton);
    
            redoButton = new MultilingualButton(
                IconLoader.loadIcon("redo"), "ui.swing.dialog.timelimitedit.toolbar.redo"
            );
            toolBar.add(redoButton);
    
            
            toolBar.addSeparator();
            
            
            setAlwaysButton = new MultilingualButton(
                IconLoader.loadIcon("timelimit-always"), "ui.swing.dialog.timelimitedit.toolbar.set_always"
            );
            toolBar.add(setAlwaysButton);

            setNeverButton = new MultilingualButton(
                IconLoader.loadIcon("timelimit-never"), "ui.swing.dialog.timelimitedit.toolbar.set_never"
            );
            toolBar.add(setNeverButton);
        }
        
        timeLimitEditorPanel = new TimeLimitEditorPanel(parent, editable);
        containerPanel.add(timeLimitEditorPanel, BorderLayout.CENTER);
    }
    
    @Override
    protected void load() {
        timeLimitEditorPanel.load(timeLimitWrapper.get(), document, contextObject);
    }
    
    @Override
    protected void afterLoad() {
        ActionListener undo = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                timeLimitEditorPanel.undo();
            }
            
        };
        
        ActionListener redo = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                timeLimitEditorPanel.redo();
            }
            
        };

        ActionListener setAlways = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                timeLimitEditorPanel.set(new AlwaysTimeLimit());
            }
            
        };

        ActionListener setNever = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                timeLimitEditorPanel.set(new NeverTimeLimit());
            }
            
        };
        
        rootPane.registerKeyboardAction(undo, KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW);
        rootPane.registerKeyboardAction(redo, KeyStroke.getKeyStroke(KeyEvent.VK_Y, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW);
        rootPane.registerKeyboardAction(redo, KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW);

        undoButton.addActionListener(undo);
        redoButton.addActionListener(redo);
        setAlwaysButton.addActionListener(setAlways);
        setNeverButton.addActionListener(setNever);
        
        addComponentListener(new ComponentListener() {
            
            @Override
            public void componentShown(ComponentEvent ev) {
                timeLimitEditorPanel.focusMain();
            }
            
            @Override
            public void componentResized(ComponentEvent ev) {
            }
            
            @Override
            public void componentMoved(ComponentEvent ev) {
            }
            
            @Override
            public void componentHidden(ComponentEvent ev) {
            }
        });
    }
    
    @Override
    protected void save() {
        timeLimitWrapper.set(timeLimitEditorPanel.getTimeLimit());
    }

    @Override
    protected void tryCancel() {
        TimeLimit baseTimeLimit = new CustomTimeLimit(timeLimitWrapper.get());
        TimeLimit editedTimeLimit = new CustomTimeLimit(timeLimitEditorPanel.getTimeLimit());
        if (!editedTimeLimit.equals(baseTimeLimit)) {
            modified = true;
        }
        super.tryCancel();
    }
    
    @Override
    protected String generateTitle() {
        return contextObject == null ? title : title + " (" + contextObject.getLabel() + ")";
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;

import hu.webarticum.aurora.app.util.ToStringComparator;
import hu.webarticum.aurora.app.util.email.Attachment;
import hu.webarticum.aurora.app.util.email.FileAttachment;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.aurora.plugins.defaultswingui.util.RunnableActionListener;
import hu.webarticum.aurora.plugins.defaultswingui.util.TableRowTransferHandler;


public class EmailAttachmentsDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    
    private final List<Attachment> attachmentsReference;

    private final List<Attachment> selectableAttachments = new ArrayList<Attachment>();

    private JTable attachmentsTable;
    
    private JButton addFromFileButton;
    
    private JButton addFromSelectableButton;
    
    private JButton removeButton;
    
    private AttachmentsTableModel model;

    
    public EmailAttachmentsDialog(JFrame parent, List<Attachment> attachmentsReference, Collection<Attachment> selectableAttachments) {
        super(parent);
        this.title = text("ui.swing.dialog.emailattachments.title");
        this.attachmentsReference = attachmentsReference;
        this.selectableAttachments.addAll(selectableAttachments);
        model = new AttachmentsTableModel(attachmentsReference);
        init();
    }
    
    
    @Override
    protected void build() {
        attachmentsTable = new JTable();
        attachmentsTable.setFillsViewportHeight(true);
        attachmentsTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        attachmentsTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        attachmentsTable.setDragEnabled(true);
        attachmentsTable.setDropMode(DropMode.INSERT_ROWS);
        attachmentsTable.setTransferHandler(new TableRowTransferHandler(attachmentsTable));
        JScrollPane attachmentsTableScrollPane = new JScrollPane(attachmentsTable);
        attachmentsTableScrollPane.setPreferredSize(new Dimension(600, 400));
        mainPanel.add(attachmentsTableScrollPane);
        
        JPanel buttonBar = new JPanel();
        buttonBar.setLayout(new FlowLayout(FlowLayout.RIGHT));
        addRow(buttonBar);

        addFromFileButton = new MultilingualButton("ui.swing.dialog.emailattachments.add_from_file", null, IconLoader.loadIcon("add"));
        buttonBar.add(addFromFileButton);

        addFromSelectableButton = new MultilingualButton("ui.swing.dialog.emailattachments.add_from_selectable", null, IconLoader.loadIcon("add"));
        buttonBar.add(addFromSelectableButton);
        
        removeButton = new MultilingualButton(null, "ui.swing.dialog.emailattachments.remove.tooltip", IconLoader.loadIcon("trash"));
        buttonBar.add(removeButton);
    }

    @Override
    protected void load() {
        attachmentsTable.setModel(model);
        addFromSelectableButton.setVisible(!selectableAttachments.isEmpty());
    }
    
    @Override
    protected void afterLoad() {
        super.afterLoad();

        int mimeColumnWidth = 170;
        attachmentsTable.getColumnModel().getColumn(2).setMinWidth(mimeColumnWidth);
        attachmentsTable.getColumnModel().getColumn(2).setMaxWidth(mimeColumnWidth);
        attachmentsTable.getColumnModel().getColumn(2).setPreferredWidth(mimeColumnWidth);
        attachmentsTable.getColumnModel().getColumn(2).setWidth(mimeColumnWidth);
        
        attachmentsTable.getColumnModel().getColumn(1).setCellEditor(new AttachmentsTableFilenameEditor());
        
        attachmentsTable.doLayout();
        
        Runnable removeAction = new Runnable() {
            
            @Override
            public void run() {
                List<Integer> selectedIndexes = new ArrayList<Integer>();
                for (int i = model.getRowCount() - 1; i >= 0; i--) {
                    if (attachmentsTable.getSelectionModel().isSelectedIndex(i)) {
                        selectedIndexes.add(i);
                    }
                }
                
                if (!selectedIndexes.isEmpty()) {
                    if (OptionPaneUtil.showConfirmDialog(
                        parent,
                        text("ui.swing.dialog.emailattachments.remove.message"),
                        text("ui.swing.dialog.emailattachments.remove.title"),
                        JOptionPane.PLAIN_MESSAGE,
                        null
                    ) != JOptionPane.OK_OPTION) {
                        return;
                    }
                    
                    for (int index: selectedIndexes) {
                        model.remove(index);
                    }
                }
            }
            
        };
        
        attachmentsTable.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {
                if (ev.getKeyCode() == KeyEvent.VK_DELETE) {
                    removeAction.run();
                }
            }
            
        });

        addFromFileButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setMultiSelectionEnabled(true);
                if (fileChooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
                    for (final File selectedFile: fileChooser.getSelectedFiles()) {
                        model.append(new FileAttachment(selectedFile));
                    }
                }
            }
            
        });

        addFromSelectableButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Object[] inputAttachments = selectableAttachments.toArray();
                Arrays.sort(inputAttachments, new ToStringComparator(new Labeled.StringComparator()));
                Attachment selectedAttachment = (Attachment)OptionPaneUtil.showInputDialog(
                    parent,
                    text("ui.swing.dialog.emailattachments.add_from_selectable.message"),
                    text("ui.swing.dialog.emailattachments.add_from_selectable.title"),
                    JOptionPane.PLAIN_MESSAGE,
                    inputAttachments,
                    inputAttachments[0],
                    ButtonPurpose.ACTION
                );
                if (selectedAttachment != null && !model.contains(selectedAttachment)) {
                    model.append(selectedAttachment);
                }
            }
            
        });
        
        removeButton.addActionListener(new RunnableActionListener(removeAction));
    }
    
    @Override
    protected void save() {
        attachmentsReference.clear();
        attachmentsReference.addAll(model.getAttachments());
    }
    
    private class AttachmentsTableFilenameEditor extends AbstractCellEditor implements TableCellEditor {

        private static final long serialVersionUID = 1L;
        
        JTextField editorField = null;
        
        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int columnIndex) {
            if (editorField == null) {
                editorField = new JTextField();
                editorField.setBorder(null);
                editorField.addFocusListener(new FocusListener() {
                    
                    @Override
                    public void focusLost(FocusEvent ev) {
                    }
                    
                    @Override
                    public void focusGained(FocusEvent ev) {
                        String value = editorField.getText();
                        int pos = value.lastIndexOf('.');
                        if (pos > 3 && value.substring(pos - 4, pos).equals(".tar")) {
                            pos -= 4;
                        } else if (pos == -1) {
                            pos = value.length();
                        }
                        if (pos > 0) {
                            editorField.setSelectionStart(0);
                            editorField.setSelectionEnd(pos);
                        }
                    }
                    
                });
            }
            editorField.setText(value.toString());
            return editorField;
        }

        @Override
        public Object getCellEditorValue() {
            return editorField.getText();
        }

    }

    private class AttachmentsTableModel extends AbstractTableModel implements TableRowTransferHandler.Reorderable {

        private static final long serialVersionUID = 1L;

        private final String[] columnNames = new String[] {
            text("ui.swing.dialog.emailattachments.table.description"),
            text("ui.swing.dialog.emailattachments.table.filename"),
            text("ui.swing.dialog.emailattachments.table.mimetype"),
        };
        
        private final List<Attachment> attachments = new ArrayList<Attachment>();

        AttachmentsTableModel(Collection<Attachment> attachments) {
            this.attachments.addAll(attachments);
        }
        
        @Override
        public int getRowCount() {
            return attachments.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return columnNames[columnIndex];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return (columnIndex == 1);
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Attachment attachment = attachments.get(rowIndex);
            if (columnIndex == 0) {
                return attachment.getDescription();
            } else if (columnIndex == 1) {
                return attachment.getFilename();
            } else if (columnIndex == 2) {
                return attachment.getMimeType();
            } else {
                return null;
            }
        }

        @Override
        public void setValueAt(Object value, int rowIndex, int columnIndex) {
            Attachment attachment = attachments.get(rowIndex);
            String stringValue = value.toString();
            if (columnIndex == 1) {
                attachment.setFilename(stringValue);
            }
            fireTableCellUpdated(rowIndex, columnIndex);
        }

        @Override
        public void move(int fromIndex, int toIndex) {
            int targetIndex = toIndex < fromIndex ? toIndex : toIndex - 1;
            Attachment attachment = attachments.remove(fromIndex);
            attachments.add(targetIndex, attachment);
            fireTableRowsUpdated(Math.min(fromIndex, targetIndex), Math.max(fromIndex, targetIndex));
        }
        
        boolean contains(Attachment attachment) {
            return attachments.contains(attachment);
        }
        
        void remove(int rowIndex) {
            attachments.remove(rowIndex);
            fireTableRowsDeleted(rowIndex, rowIndex);
        }

        void append(Attachment attachment) {
            int nextIndex = attachments.size();
            attachments.add(attachment);
            fireTableRowsInserted(nextIndex, nextIndex);
        }
        
        List<Attachment> getAttachments() {
            return new ArrayList<Attachment>(attachments);
        }

    }

}
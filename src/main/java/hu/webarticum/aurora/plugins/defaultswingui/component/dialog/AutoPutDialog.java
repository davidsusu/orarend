package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.extensions;
import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.rawText;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.aurora.app.check.CompoundMessageCollector;
import hu.webarticum.aurora.app.check.DefaultBlockListProblemCollector;
import hu.webarticum.aurora.app.check.DefaultBlockProblemCollector;
import hu.webarticum.aurora.app.check.FilteringMessageCollector;
import hu.webarticum.aurora.app.check.Message;
import hu.webarticum.aurora.app.check.MessageCollector;
import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.memento.BoardMemento;
import hu.webarticum.aurora.app.util.autoput.AutoPutter;
import hu.webarticum.aurora.app.util.autoput.AutoPutter.AutoPutOutput;
import hu.webarticum.aurora.app.util.autoput.AutoPutter.ResultState;
import hu.webarticum.aurora.app.util.autoput.AutoPutterAspectStructure;
import hu.webarticum.aurora.app.util.autoput.AutoPutterFactory;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualCheckBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualRadioButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.chm.Command;


public class AutoPutDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private Board board;
    
    private JComboBox<Labeled.GenericWrapper<AutoPutterFactory>> autoPutterFactoryComboBox;
    
    private JButton boardButton;
    
    private JButton blockListButton;
    
    private JRadioButton blockListAllRadioButton;
    
    private JRadioButton blockListSpecifiedRadioButton;
    
    private BlockList selectedBlocks = new BlockList();

    private JCheckBox runCheckCheckBox;
    
    public AutoPutDialog(JFrame parent, Document document, Board board) {
        super(parent);
        this.title = text("ui.swing.dialog.autoput.title");
        this.document = document;
        this.board = board;
        init();
    }
    
    @Override
    protected ButtonPurpose getPurpose() {
        return ButtonPurpose.ACTION;
    }

    @Override
    protected void build() {
        TreeSet<Labeled.GenericWrapper<AutoPutterFactory>> autoPutterFactoryWrappers = new TreeSet<Labeled.GenericWrapper<AutoPutterFactory>>(
            new GeneralWrapper.WrapperComparator<AutoPutterFactory>(
                new AutoPutterFactory.AutoPutterFactoryComparator()
            )
        );
        for (CommonExtensions.Algorithm algorithm: extensions(CommonExtensions.Algorithm.class)) {
            AutoPutterFactory autoPutterFactory = algorithm.getAutoPutterFactory();
            autoPutterFactoryWrappers.add(new Labeled.GenericWrapper<AutoPutterFactory>(autoPutterFactory));
        }
        
        @SuppressWarnings("unchecked")
        Labeled.GenericWrapper<AutoPutterFactory>[] autoPutterFactoryWrappersArray = (Labeled.GenericWrapper<AutoPutterFactory>[])(new Labeled.GenericWrapper[autoPutterFactoryWrappers.size()]);
        autoPutterFactoryWrappers.toArray(autoPutterFactoryWrappersArray);
        DefaultComboBoxModel<Labeled.GenericWrapper<AutoPutterFactory>> autoPutterFactoryComboBoxModel = new DefaultComboBoxModel<Labeled.GenericWrapper<AutoPutterFactory>>(autoPutterFactoryWrappersArray);
        
        autoPutterFactoryComboBox = new JComboBox<Labeled.GenericWrapper<AutoPutterFactory>>(autoPutterFactoryComboBoxModel);
        addRow(new MultilingualLabel("ui.swing.dialog.autoput.algorithm"), autoPutterFactoryComboBox);
        
        
        JPanel boardPanel = new JPanel();
        boardPanel.setLayout(new BorderLayout());
        boardButton = new JButton("");
        boardButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Board> wrapper = new GeneralWrapper<Board>(AutoPutDialog.this.board);
                BoardSelectDialog dialog = new BoardSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    AutoPutDialog.this.board = wrapper.get();
                    reloadBoard();
                }
            }
            
        });
        boardPanel.add(boardButton, BorderLayout.CENTER);
        
        addRow(new MultilingualLabel("ui.swing.dialog.autoput.board"), boardPanel);
        


        ButtonGroup blockListTypeButtonGroup = new ButtonGroup();
        
        JPanel blockListAllPanel = new JPanel();
        blockListAllPanel.setLayout(new BorderLayout());
        
        blockListAllRadioButton = new MultilingualRadioButton("ui.swing.dialog.autoput.put_all");
        blockListAllRadioButton.setSelected(true);
        blockListAllPanel.add(blockListAllRadioButton, BorderLayout.CENTER);
        blockListTypeButtonGroup.add(blockListAllRadioButton);
        
        addRow(new MultilingualLabel("ui.swing.dialog.autoput.blocks"), blockListAllPanel);

        
        JPanel blockListSpecifiedPanel = new JPanel();
        blockListSpecifiedPanel.setLayout(new BorderLayout());
        
        blockListSpecifiedRadioButton = new JRadioButton();
        blockListSpecifiedPanel.add(blockListSpecifiedRadioButton, BorderLayout.LINE_START);
        blockListTypeButtonGroup.add(blockListSpecifiedRadioButton);
        
        blockListButton = new JButton("");
        blockListButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                BlockMultiSelectDialog dialog = new BlockMultiSelectDialog(parent, document, selectedBlocks);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    blockListSpecifiedRadioButton.setSelected(true);
                    reloadBlocks();
                }
            }
            
        });
        blockListSpecifiedPanel.add(blockListButton, BorderLayout.CENTER);
        addRow("", blockListSpecifiedPanel);

        runCheckCheckBox = new MultilingualCheckBox("ui.swing.dialog.autoput.run_check.checkbox");
        addRow(text("ui.swing.dialog.autoput.run_check.title"), runCheckCheckBox);
    }

    @Override
    protected void load() {
        reloadBoard();
        reloadBlocks();
        runCheckCheckBox.setSelected(document.getExtraData().getAccess("autoput.school.setup.check_enabled").get(new Value(true)).getAsBoolean());
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void save() {
        boolean checkEnabled = runCheckCheckBox.isSelected();
        document.getExtraData().getAccess("autoput.school.setup.check_enabled").set(checkEnabled);
        
        AutoPutterFactory factory = ((Labeled.GenericWrapper<AutoPutterFactory>)autoPutterFactoryComboBox.getSelectedItem()).get();
        
        BlockList blocks;
        if (blockListSpecifiedRadioButton.isSelected()) {
            blocks = selectedBlocks;
        } else {
            blocks = document.getBlockStore().getAll();
        }
        
        if (checkEnabled) {
            MessageCollector collector = new FilteringMessageCollector(
                new CompoundMessageCollector(
                    new DefaultBlockProblemCollector(blocks, document),
                    new DefaultBlockListProblemCollector(blocks, document, board)
                ),
                FilteringMessageCollector.ERROR_FILTER
            );
            MessageCollectorRunner collectorRunner = new MessageCollectorRunner(parent, document, collector, false);
            List<Message> messages = collectorRunner.run();
            List<Message> errorMessages = messages.stream()
                .filter(m -> m.getType() == Message.Type.ERROR)
                .collect(Collectors.toList())
            ;
            if (!errorMessages.isEmpty()) {
                result = RESULT_CANCEL;
                return;
            }
        }

        BoardMemento beforeMemento = new BoardMemento(board);
        
        AutoPutterAspectStructure aspectStructure = factory.collectAspectStructure(document, blocks, board);

        // TODO / FIXME : user enabling
        List<String> enabledAspects = aspectStructure.getAllAspects();
        
        AutoPutter autoPutter = factory.createAutoPutter(document, blocks, board, enabledAspects);
        
        AutoPutOutputDialog dialog = new AutoPutOutputDialog(autoPutter);
        dialog.run();

        ResultState autoPutResult = autoPutter.getResultState();
        
        if (autoPutResult == ResultState.SOLVED) {
            BoardMemento afterMemento = new BoardMemento(board);
            Command command = new StoreItemEditCommand<Board>(
                "ui.swing.dialog.autoput.command.autoput",
                document.getBoardStore(), board, beforeMemento, afterMemento
            );
            history().addAndExecute(command);
            result = RESULT_OK;
        } else if (autoPutResult == ResultState.UNSOLVED) {
            int answer = OptionPaneUtil.showConfirmDialog(
                parent, rawText("elemzes?"), ButtonPurpose.ACTION
            );
            if (answer == JOptionPane.YES_OPTION) {
                // TODO
                new AutoPutFailCauseFinderDialog(parent).run();
            }
            result = RESULT_CANCEL;
        } else {
            result = RESULT_CANCEL;
        }
    }

    private void reloadBoard() {
        if (board == null) {
            boardButton.setText("");
        } else {
            boardButton.setText(board.getLabel());
        }
    }
    
    private void reloadBlocks() {
        blockListButton.setText(String.format(text("ui.swing.dialog.autoput.selected_blocks"), selectedBlocks.size()));
    }
    
    
    public class AutoPutOutputDialog extends SwingWorkerProcessDialog {

        private static final long serialVersionUID = 1L;
        
        private AutoPutter autoPutter;
        
        public AutoPutOutputDialog(AutoPutter autoPutter) {
            super(AutoPutDialog.this.parent);
            this.autoPutter = autoPutter;
        }
        
        @Override
        protected Worker createWorker() {
            return new AutoPutOutputWorker();
        }
        
        
        private class AutoPutOutputWorker extends Worker implements AutoPutOutput {

            private volatile long startTime = -1;
            
            
            @Override
            public void _run() throws CancelledException {
                autoPutter.setOutput(this);
                
                long startNanoTime = System.nanoTime();
                
                autoPutter.runThrows();
                
                long endNanoTime = System.nanoTime();
                NumberFormat formatter = new DecimalFormat("#0.000000");
                double runSeconds = (endNanoTime - startNanoTime) / 1000000000.0d;
                log(String.format(
                    "%n%s: %s%n",
                    text("ui.swing.dialog.swingworkerprocess.execution_time"),
                    formatter.format(runSeconds)
                ));
            }
            
            @Override
            public void _rollBack() {
                
            }
            
            @Override
            public void setLabel(String label) {
                publishCommand(new SetLabelCommand(label));
            }

            @Override
            public void setProgress(Double progress) {
                publishCommand(new SetProgressCommand(progress));
            }

            @Override
            public void log(String line) {
                publishCommand(new LogCommand(line));
            }

            @Override
            public void tick() throws CancelledException {
                if (isCancelled()) {
                    throw new CancelledException();
                }
            }

            @Override
            public void trackTime() {
                startTime = System.currentTimeMillis();
            }

            @Override
            public void logTime(String comment) {
                if (startTime >= 0) {
                    long currentTime = System.currentTimeMillis();
                    long timeDiff = currentTime - startTime;
                    log(String.format("%7.3f | %s", timeDiff / 1000d, comment));
                }
            }
            
            @Override
            public void finish(boolean success) {
                publishCommand(success ? new SuccessCommand() : new FailedCommand());
            }

        }
        
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class TimingSetSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<TimingSet> timingSetWrapper;

    private JComboBox<Labeled.Wrapper> timingSetSelectComboBox;
    
    public TimingSetSelectDialog(JFrame parent, Document document, GeneralWrapper<TimingSet> timingSetWrapper) {
        super(parent);
        this.title = text("ui.swing.dialog.timingsetselect.title");
        this.document = document;
        this.timingSetWrapper = timingSetWrapper;
        setResizable(false);
        init();
    }
    
    @Override
    protected void build() {
        JPanel addPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 5));
        mainPanel.add(addPanel);
        
        JButton addButton = new MultilingualButton("ui.swing.dialog.timingsetselect.new_timingset");
        addButton.setIcon(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                TimingSet timingSet = new TimingSet();
                TimingSetEditDialog dialog = new TimingSetEditDialog(parent, document, timingSet);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    document.getTimingSetStore().register(timingSet);
                    refreshTimingSetList(timingSet);
                }
            }
            
        });
        addPanel.add(addButton);
        
        timingSetSelectComboBox = new JComboBox<Labeled.Wrapper>();
        addRow(new MultilingualLabel("ui.swing.dialog.timingsetselect.timingset"), timingSetSelectComboBox);
    }

    @Override
    protected void load() {
        refreshTimingSetList(timingSetWrapper.get());
    }

    @Override
    protected void save() {
        TimingSet timingSet = (TimingSet)((Labeled.Wrapper)timingSetSelectComboBox.getSelectedItem()).get();
        timingSetWrapper.set(timingSet);
    }
    
    private void refreshTimingSetList(TimingSet selectedTimingSet) {
        DefaultComboBoxModel<Labeled.Wrapper> model = new DefaultComboBoxModel<Labeled.Wrapper>();
        timingSetSelectComboBox.setModel(model);
        Document.TimingSetStore timingSetStore = document.getTimingSetStore();
        if (timingSetStore.size()==0) {
            okButton.setEnabled(false);
        } else {
            okButton.setEnabled(true);
            int pos = 0;
            int selectedPos = -1;
            for (TimingSet timingSet : timingSetStore) {
                model.addElement(new Labeled.Wrapper(timingSet));
                if (timingSet.equals(selectedTimingSet)) {
                    selectedPos = pos;
                }
                pos++;
            }
            if (selectedPos!=(-1)) {
                timingSetSelectComboBox.setSelectedIndex(selectedPos);
            }
        }
    }

}

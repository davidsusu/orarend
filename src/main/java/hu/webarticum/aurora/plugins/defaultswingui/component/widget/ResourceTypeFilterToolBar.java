package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.Painter;
import javax.swing.UIDefaults;
import javax.swing.event.EventListenerList;

import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualToggleButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class ResourceTypeFilterToolBar extends JToolBar {

    private static final long serialVersionUID = 1L;

    
    private final Map<JToggleButton, Resource.Type> buttons = new HashMap<JToggleButton, Resource.Type>();
    
    private final EventListenerList listeners = new EventListenerList();
    
    
    public ResourceTypeFilterToolBar() {
        super(JToolBar.HORIZONTAL);
        setFloatable(false);

        String uiPropertyKey = "Nimbus.Overrides";

        Painter<JComponent> noPainter = new Painter<JComponent>() {

            @Override
            public void paint(Graphics2D g, JComponent object, int width, int height) {
                // nothing to do
            }
            
        };
        
        UIDefaults toolBarUiDefaults = new UIDefaults();
        toolBarUiDefaults.put("ToolBar[East].borderPainter", noPainter);
        toolBarUiDefaults.put("ToolBar[North].borderPainter", noPainter);
        toolBarUiDefaults.put("ToolBar[South].borderPainter", noPainter);
        toolBarUiDefaults.put("ToolBar[West].borderPainter", noPainter);
        putClientProperty(uiPropertyKey, toolBarUiDefaults);
        
        UIDefaults toggleButtonUiDefaults = new UIDefaults();
        toggleButtonUiDefaults.put("ToolBar:ToggleButton.contentMargins", new Insets(7, 7, 7, 7));

        ButtonGroup buttonGroup = new ButtonGroup();
        
        ItemListener buttonItemListener = new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                JToggleButton toggleButton = (JToggleButton)ev.getSource();
                if (toggleButton.isSelected()) {
                    onChanged(buttons.get(toggleButton));
                }
            }
            
        };

        JToggleButton classToggleButton =
            new MultilingualToggleButton("", "core.resource.type.class")
        ;
        classToggleButton.setIcon(IconLoader.loadIcon("resourcetype-class"));
        classToggleButton.putClientProperty(uiPropertyKey, toggleButtonUiDefaults);
        buttonGroup.add(classToggleButton);
        add(classToggleButton);
        buttons.put(classToggleButton, Resource.Type.CLASS);

        JToggleButton personToggleButton =
            new MultilingualToggleButton("", "core.resource.type.person")
        ;
        personToggleButton.setIcon(IconLoader.loadIcon("resourcetype-person"));
        personToggleButton.putClientProperty(uiPropertyKey, toggleButtonUiDefaults);
        buttonGroup.add(personToggleButton);
        add(personToggleButton);
        buttons.put(personToggleButton, Resource.Type.PERSON);

        JToggleButton localeToggleButton =
            new MultilingualToggleButton("", "core.resource.type.locale")
        ;
        localeToggleButton.setIcon(IconLoader.loadIcon("resourcetype-locale"));
        localeToggleButton.putClientProperty(uiPropertyKey, toggleButtonUiDefaults);
        buttonGroup.add(localeToggleButton);
        add(localeToggleButton);
        buttons.put(localeToggleButton, Resource.Type.LOCALE);

        JToggleButton objectToggleButton =
            new MultilingualToggleButton("", "core.resource.type.object")
        ;
        objectToggleButton.setIcon(IconLoader.loadIcon("resourcetype-object"));
        objectToggleButton.putClientProperty(uiPropertyKey, toggleButtonUiDefaults);
        buttonGroup.add(objectToggleButton);
        add(objectToggleButton);
        buttons.put(objectToggleButton, Resource.Type.OBJECT);

        JToggleButton otherToggleButton =
            new MultilingualToggleButton("", "core.resource.type.other")
        ;
        otherToggleButton.setIcon(IconLoader.loadIcon("resourcetype-other"));
        otherToggleButton.putClientProperty(uiPropertyKey, toggleButtonUiDefaults);
        buttonGroup.add(otherToggleButton);
        add(otherToggleButton);
        buttons.put(otherToggleButton, Resource.Type.OTHER);
        
        classToggleButton.setSelected(true);
        
        classToggleButton.addItemListener(buttonItemListener);
        personToggleButton.addItemListener(buttonItemListener);
        localeToggleButton.addItemListener(buttonItemListener);
        objectToggleButton.addItemListener(buttonItemListener);
        otherToggleButton.addItemListener(buttonItemListener);
    }

    public void addChangeListener(ChangeListener changeListener) {
        listeners.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        listeners.remove(ChangeListener.class, changeListener);
    }
    
    public Resource.Type getResourceType() {
        for (Map.Entry<JToggleButton, Resource.Type> entry: buttons.entrySet()) {
            if (entry.getKey().isSelected()) {
                return entry.getValue();
            }
        }
        return null;
    }
    
    private void onChanged(Resource.Type tagType) {
        for (ChangeListener changeListener: listeners.getListeners(ChangeListener.class)) {
            changeListener.onChanged(tagType);
        }
    }
    
    
    public interface ChangeListener extends EventListener {
        
        public void onChanged(Resource.Type resourceType);
        
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.FileUtil;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.BlockFilterPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.FileChooserField;
import hu.webarticum.aurora.plugins.defaultswingui.util.AutoExtensionFileChooser;
import hu.webarticum.simplespreadsheetwriter.HssfApachePoiSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.HtmlSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.OdfToolkitSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.SerializeSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.Sheet;
import hu.webarticum.simplespreadsheetwriter.Spreadsheet;
import hu.webarticum.simplespreadsheetwriter.SpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.XssfApachePoiSpreadsheetDumper;


public class TemplateSpreadsheetExportDialog extends AbstractExportDialog {

    private static final long serialVersionUID = 1L;


    private final Document document;
    
    private final Board board;
    
    private final BlockFilterPanel blockFilterPanel;

    private final AutoExtensionFileChooser saveFileChooser = new AutoExtensionFileChooser();
    
    private final FileChooserField fileChooserField = new FileChooserField();

    private final JTextField titleTextField = new JTextField();

    private final JCheckBox openCheckBox = new JCheckBox();
    
    private final Map<String, SpreadsheetFormat> formatMap = new LinkedHashMap<String, SpreadsheetFormat>();
    
    
    public TemplateSpreadsheetExportDialog(JFrame parent, Document document, Board board) {
        super(parent);
        this.title = text("ui.swing.dialog.templatespreadsheetexport.title");
        this.document = document;
        this.board = board;
        this.blockFilterPanel = new BlockFilterPanel(parent, document);
        this.formWidth = 500;
        init();
    }

    
    @Override
    protected void build() {
        {
            formatMap.put("ods", new SpreadsheetFormat("ods", text("ui.swing.dialog.templatespreadsheetexport.format.ods"), new OdfToolkitSpreadsheetDumper()));
            formatMap.put("xls", new SpreadsheetFormat("xls", text("ui.swing.dialog.templatespreadsheetexport.format.xls"), new HssfApachePoiSpreadsheetDumper()));
            formatMap.put("xlsx", new SpreadsheetFormat("xlsx", text("ui.swing.dialog.templatespreadsheetexport.format.xlsx"), new XssfApachePoiSpreadsheetDumper()));
            formatMap.put("html", new SpreadsheetFormat("html", text("ui.swing.dialog.templatespreadsheetexport.format.html"), new HtmlSpreadsheetDumper()));
            formatMap.put("obj", new SpreadsheetFormat("obj", text("ui.swing.dialog.templatespreadsheetexport.format.obj"), new SerializeSpreadsheetDumper()));
            
            saveFileChooser.setAppendExtension(true);
            saveFileChooser.setMultiSelectionEnabled(false);
            saveFileChooser.setAcceptAllFileFilterUsed(true);
            for (Map.Entry<String, SpreadsheetFormat> entry: formatMap.entrySet()) {
                saveFileChooser.addChoosableFileFilter(entry.getValue().fileFilter);
            }
        }
        
        addRow(text("ui.swing.dialog.templatespreadsheetexport.sheettitle"), titleTextField);

        fileChooserField.setDialogType(JFileChooser.SAVE_DIALOG);
        fileChooserField.setFileChooser(saveFileChooser);
        addRow(text("ui.swing.dialog.templatespreadsheetexport.targetfile"), fileChooserField);

        openCheckBox.setText(text("ui.swing.dialog.templatespreadsheetexport.open_now"));
        addRow(openCheckBox);
        
        pack();
    }

    @Override
    protected void load() {
        titleTextField.setText(board.getLabel());
    }

    @Override
    protected void afterLoad() {
    }

    @Override
    protected void save() {
    }

    @Override
    protected boolean trySave() {
        final GeneralWrapper<File> resultWrapper = new GeneralWrapper<File>();
        SwingWorkerProcessDialog processDialog = new SwingWorkerProcessDialog(parent, text("ui.swing.dialog.templatespreadsheetexport.saving"), true) {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected Worker createWorker() {
                return new Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        resultWrapper.set(trySaveUnwrapped());
                    }

                    @Override
                    public void _rollBack() {
                    }

                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        if (command instanceof TerminatedCommand) {
                            closeDialog();
                        }
                    }
                    
                };
            }
            
        };
        processDialog.run();
        
        File choosenFile = resultWrapper.get();
        
        if (choosenFile == null) {
            return false;
        }

        if (openCheckBox.isSelected()) {
            try {
                Desktop.getDesktop().open(choosenFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return true;
    }

    protected File trySaveUnwrapped() {
        String defaultExtension = "ods";
        Set<String> allExtensions = formatMap.keySet();
        
        final File choosenFile;
        final String choosenExtension;
        if (fileChooserField.isEmpty()) {
            if (saveFileChooser.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
                return null;
            }
            choosenFile = saveFileChooser.getAutoSelectedFile();
            choosenExtension = saveFileChooser.getAutoSelectedExtension();
        } else {
            choosenFile = fileChooserField.getFile();
            String choosenFileExtension = FileUtil.getExtension(choosenFile);
            choosenExtension = allExtensions.contains(choosenFileExtension) ? choosenFileExtension : defaultExtension;
        }
        
        if (!checkFile(parent, choosenFile)) {
            return null;
        }
        
        Spreadsheet spreadsheet = buildSpreadsheet();
        
        SpreadsheetFormat choosenFormat = formatMap.get(choosenExtension);
        try {
            choosenFormat.dumper.dump(spreadsheet, choosenFile);
        } catch (IOException e) {
            OptionPaneUtil.showMessageDialog(
                parent,
                text("ui.swing.dialog.templatespreadsheetexport.saveprocessdialog.error.message"),
                text("ui.swing.dialog.templatespreadsheetexport.saveprocessdialog.error.title"),
                JOptionPane.ERROR_MESSAGE
            );
            return null;
        }
        
        return choosenFile;
    }

    private Spreadsheet buildSpreadsheet() {
        Spreadsheet spreadsheet = new Spreadsheet();
        
        Sheet sheet = spreadsheet.add(titleTextField.getText());
        sheet.write(0, 0, titleTextField.getText());
        
        // TODO
        
        return spreadsheet;
    }
    
    
    private class SpreadsheetFormat {

        final SpreadsheetDumper dumper;
        
        final FileFilter fileFilter;
        
        SpreadsheetFormat(String extension, String description, SpreadsheetDumper dumper) {
            this.dumper = dumper;
            this.fileFilter = new AutoExtensionFileChooser.ExtensionFilter(description, extension);
        }
        
    }
    
}

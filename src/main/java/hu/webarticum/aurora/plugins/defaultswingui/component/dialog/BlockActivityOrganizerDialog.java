package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import hu.webarticum.aurora.app.memento.BlockMemento;
import hu.webarticum.aurora.app.memento.RemovalHelper;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualCheckBox;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.JListReferenceSorter;
import hu.webarticum.chm.AbstractCommand;
import hu.webarticum.chm.Command;


public class BlockActivityOrganizerDialog extends EditDialog {
    
    private static final long serialVersionUID = 1L;

    private final Map<Activity, PeriodSet> activityPeriodsMap = new HashMap<Activity, PeriodSet>();
    
    
    private Document document;
    
    private ActivityListPanel leftPanel;
    
    private ActivityListPanel rightPanel;
    
    private JListReferenceSorter<Activity> handler;
    
    private JCheckBox deleteEmptyBlockCheckBox;
    
    private boolean block2WasNew;
    
    
    public BlockActivityOrganizerDialog(JFrame parent, Document document, Block block1, Block block2) {
        super(parent);
        this.title = text("ui.swing.dialog.blockactivityorganizer.title");
        this.document = document;
        this.leftPanel = new ActivityListPanel(block1);
        this.rightPanel = new ActivityListPanel(block2 != null ? block2 : new Block());
        this.block2WasNew = (block2 == null);
        init(false, true);
    }
    
    
    @Override
    protected void build() {
        JPanel outerPanel = new JPanel(new BorderLayout());
        
        JPanel leftRightPanel = new JPanel();
        leftRightPanel.setLayout(new BoxLayout(leftRightPanel, BoxLayout.LINE_AXIS));
        outerPanel.add(leftRightPanel, BorderLayout.CENTER);
        containerPanel.add(outerPanel, BorderLayout.CENTER);

        deleteEmptyBlockCheckBox = new MultilingualCheckBox("ui.swing.dialog.blockactivityorganizer.delete_empty_block");
        deleteEmptyBlockCheckBox.setSelected(true);
        deleteEmptyBlockCheckBox.setBorder(new EmptyBorder(10, 10, 10, 10));
        outerPanel.add(deleteEmptyBlockCheckBox, BorderLayout.PAGE_END);
        
        leftRightPanel.add(leftPanel);
        leftRightPanel.add(rightPanel);
        
        handler = new JListReferenceSorter<Activity>(leftPanel.getListComponent(), rightPanel.getListComponent());
    }
    
    
    @Override
    protected void load() {
        leftPanel.load();
        rightPanel.load();
    }
    
    
    @Override
    protected void afterLoad() {
        super.afterLoad();
        
        leftPanel.afterLoad();
        rightPanel.afterLoad();
        handler.apply();
    }
    
    
    @Override
    protected boolean canSave() {
        if (!super.canSave()) {
            return false;
        }
        
        if (!leftPanel.canSave()) {
            return false;
        }

        if (!rightPanel.canSave()) {
            return false;
        }
        
        return true;
    }
    
    
    @Override
    protected void save() {
        final boolean deleteEmptyBlock = deleteEmptyBlockCheckBox.isSelected();
        final Block leftBlock = leftPanel.block;
        final Block rightBlock = rightPanel.block;
        Command command = new AbstractCommand() {

            BlockMemento leftOldMemento = null;
            BlockMemento leftNewMemento = null;
            Command leftRemoveCommand = null;

            BlockMemento rightOldMemento = null;
            BlockMemento rightNewMemento = null;
            Command rightRemoveCommand = null;
            Command rightRegisterCommand = null;
            
            boolean structureChanged = false;
            
            @Override
            protected boolean _execute() {
                if (leftOldMemento == null) {
                    RemovalHelper removalHelper = new RemovalHelper(document);
                    leftOldMemento = new BlockMemento(leftBlock);
                    leftPanel.save();
                    leftNewMemento = new BlockMemento(leftBlock);
                    if (deleteEmptyBlock && leftBlock.getActivityManager().getActivities().isEmpty()) {
                        leftRemoveCommand = removalHelper.createRemoveCommandFor(leftBlock, null);
                        leftRemoveCommand.execute();
                    }

                    rightOldMemento = new BlockMemento(rightBlock);
                    rightPanel.save();
                    rightNewMemento = new BlockMemento(rightBlock);
                    boolean removeRight = (deleteEmptyBlock && rightBlock.getActivityManager().getActivities().isEmpty());
                    if (block2WasNew) {
                        if (!removeRight) {
                            rightRegisterCommand = new StoreItemRegisterCommand<Block>(null, document.getBlockStore(), rightBlock);
                            rightRegisterCommand.execute();
                        }
                    } else if (removeRight) {
                        rightRemoveCommand = removalHelper.createRemoveCommandFor(rightBlock, null);
                        rightRemoveCommand.execute();
                    }

                    structureChanged = (
                        leftRemoveCommand != null ||
                        rightRemoveCommand != null ||
                        rightRegisterCommand != null
                    );
                } else {
                    leftNewMemento.apply(leftBlock);
                    rightNewMemento.apply(rightBlock);
                    if (leftRemoveCommand != null) {
                        leftRemoveCommand.execute();
                    }
                    if (rightRemoveCommand != null) {
                        rightRemoveCommand.execute();
                    }
                    if (rightRegisterCommand != null) {
                        rightRegisterCommand.execute();
                    }
                }
                document.getBlockStore().refresh(structureChanged);
                return true;
            }
            
            @Override
            protected boolean _rollBack() {
                leftOldMemento.apply(leftBlock);
                rightOldMemento.apply(rightBlock);
                if (leftRemoveCommand != null) {
                    leftRemoveCommand.rollBack();
                }
                if (rightRemoveCommand != null) {
                    rightRemoveCommand.rollBack();
                }
                if (rightRegisterCommand != null) {
                    rightRegisterCommand.rollBack();
                }
                document.getBlockStore().refresh(structureChanged);
                return true;
            }
            
            @Override
            public String toString() {
                return text("ui.swing.dialog.blockactivityorganizer.command.organize");
            }
            
        };
        
        history().addAndExecute(command);
    }
    
    
    protected class ActivityListPanel extends JPanel {
        
        private static final long serialVersionUID = 1L;
        

        private final Block block;

        private JTextField labelField;
        
        private JSpinner lengthMinutesSpinner;
        
        private JSpinner lengthSecondsSpinner;
        
        private final JList<Activity> activityList = new JList<Activity>();

        private final DefaultListModel<Activity> activityListModel = new DefaultListModel<Activity>();
        
        
        ActivityListPanel(final Block block) {
            this.block = block;
            
            
            setLayout(new BorderLayout());
            setBorder(new EmptyBorder(7, 7, 7, 7));
            
            JPanel headerPanel = new JPanel();
            headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.PAGE_AXIS));
            add(headerPanel, BorderLayout.PAGE_START);
            
            labelField = new JTextField();
            addRowTo(headerPanel, text("ui.swing.dialog.blockactivityorganizer.blockpanel.block_label"), labelField, 300, 100);

            JPanel lengthPanel = new JPanel();
            lengthPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 2));
            
            lengthMinutesSpinner = new JSpinner();
            lengthMinutesSpinner.setToolTipText(text("ui.swing.dialog.blockactivityorganizer.blockpanel.minutes"));
            lengthMinutesSpinner.setPreferredSize(new Dimension(100, 25));
            lengthMinutesSpinner.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
            lengthPanel.add(lengthMinutesSpinner);
            lengthPanel.add(new JLabel(":"));
            lengthSecondsSpinner = new JSpinner();
            lengthSecondsSpinner.setPreferredSize(new Dimension(70, 25));
            lengthSecondsSpinner.setToolTipText(text("ui.swing.dialog.blockactivityorganizer.blockpanel.seconds"));
            lengthSecondsSpinner.setModel(new SpinnerNumberModel(0, 0, 59, 1));
            lengthPanel.add(lengthSecondsSpinner);
            addRowTo(headerPanel, text("ui.swing.dialog.blockactivityorganizer.blockpanel.block_length"), lengthPanel, 300, 100);
            
            activityList.setModel(activityListModel);
            activityList.setCellRenderer(new ListActivityRenderer());
            
            final Runnable editActivityAction = new Runnable() {

                @Override
                public void run() {
                    Activity activity = activityList.getSelectedValue();
                    if (activity == null) {
                        return;
                    }
                    
                    PeriodSet periods = activityPeriodsMap.get(activity);
                    PeriodSet savedPeriods = new PeriodSet(periods);
                    
                    PeriodsEditDialog dialog = new PeriodsEditDialog(activity, periods);
                    dialog.run();
                    
                    if (!periods.equals(savedPeriods)) {
                        modified = true;
                    }
                }
                
            };

            final Runnable removeActivityAction = new Runnable() {

                @Override
                public void run() {
                    Activity activity = activityList.getSelectedValue();
                    if (activity == null) {
                        return;
                    }
                    // TODO: convert to showConfirmDialog
                    int answer = OptionPaneUtil.showOptionDialog(
                        parent,
                        text("ui.swing.dialog.blockactivityorganizer.blockpanel.activity.delete_activity.sure"),
                        text("ui.swing.dialog.blockactivityorganizer.blockpanel.activity.delete_activity.confirm"),
                        JOptionPane.QUESTION_MESSAGE,
                        new String[]{
                            text("ui.swing.dialog.blockactivityorganizer.blockpanel.activity.delete_activity.cancel"),
                            text("ui.swing.dialog.blockactivityorganizer.blockpanel.activity.delete_activity.delete"),
                        },
                        new ButtonPurpose[] {
                            ButtonPurpose.NONE,
                            ButtonPurpose.DANGER,
                        },
                        text("ui.swing.dialog.blockactivityorganizer.blockpanel.activity.delete_activity.delete")
                    );
                    if (answer == 1) {
                        handler.remove(activity);
                    }
                }
                
            };
            
            activityList.addMouseListener(new MouseAdapter() {
                
                @Override
                public void mouseClicked(MouseEvent ev) {
                    if (ev.getButton() == MouseEvent.BUTTON3) {
                        removeActivityAction.run();
                    } else if (ev.getButton() == MouseEvent.BUTTON1 && ev.getClickCount()==2) {
                        editActivityAction.run();
                    }
                }
                
            });
            activityList.addKeyListener(new KeyAdapter() {
                
                @Override
                public void keyPressed(KeyEvent ev) {
                    switch (ev.getKeyCode()) {
                        case KeyEvent.VK_DELETE:
                            removeActivityAction.run();
                        break;
                        case KeyEvent.VK_ENTER:
                            editActivityAction.run();
                        break;
                    }
                }
                
            });
            
            JScrollPane activityListScrollPane = new JScrollPane(activityList);
            activityListScrollPane.setPreferredSize(new Dimension(300, 250));
            activityListScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            add(activityListScrollPane, BorderLayout.CENTER);
        }
        
        
        public JList<Activity> getListComponent() {
            return activityList;
        }
        
        void load() {
            labelField.setText(block.getLabel());
            
            int length = (int)block.getLength();
            lengthMinutesSpinner.setValue(length/60);
            lengthSecondsSpinner.setValue(length%60);
            
            activityListModel.clear();
            Block.ActivityManager activityManager = block.getActivityManager();
            for (Block.ActivityManager.ActivityEntry entry: activityManager) {
                Activity activity = entry.getActivity();
                PeriodSet periods = entry.getPeriods();
                activityPeriodsMap.put(activity, periods);
                activityListModel.addElement(activity);
            }
        }
        

        public void afterLoad() {
            bindModifyListener(labelField);
            bindModifyListener(lengthMinutesSpinner);
            bindModifyListener(lengthSecondsSpinner);
            
            activityListModel.addListDataListener(new ListDataListener() {
                
                @Override
                public void intervalRemoved(ListDataEvent ev) {
                    modified = true;
                }
                
                
                @Override
                public void intervalAdded(ListDataEvent ev) {
                    modified = true;
                }
                
                
                @Override
                public void contentsChanged(ListDataEvent ev) {
                    modified = true;
                }
                
            });
        }
        
        public boolean canSave() {
            if (labelField.getText().isEmpty()) {
                OptionPaneUtil.showMessageDialog(parent, text("ui.swing.dialog.blockactivityorganizer.blockpanel.specify_block_label"));
                labelField.requestFocus();
                return false;
            }
            return true;
        }
        
        
        void save() {
            block.setLabel(labelField.getText());

            long length = (Integer)lengthMinutesSpinner.getValue()*60;
            length += (Integer)lengthSecondsSpinner.getValue();
            block.setLength(length);
            
            Block.ActivityManager activityManager = block.getActivityManager();
            activityManager.clear();
            int count = activityListModel.getSize();
            for (int i = 0; i<count; i++) {
                Activity activity = activityListModel.getElementAt(i);
                PeriodSet periods = activityPeriodsMap.get(activity);
                activityManager.add(activity, periods);
            }
        }
        
        
        class ListActivityRenderer implements ListCellRenderer<Activity>, Serializable {
            
            
            private static final long serialVersionUID = 1L;
            
            
            @Override
            public Component getListCellRendererComponent(
                JList<? extends Activity> list,
                Activity activity,
                int index,
                boolean isSelected,
                boolean cellHasFocus
            ) {
                JPanel outerPanel = new JPanel();
                outerPanel.setLayout(new BorderLayout());
                outerPanel.setBackground(java.awt.Color.WHITE);
                
                JPanel innerPanel = new JPanel();
                innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.PAGE_AXIS));
                innerPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
                outerPanel.add(innerPanel, BorderLayout.CENTER);
                
                JLabel label = new JLabel(activity.getLabel());
                label.setAlignmentX(Component.LEFT_ALIGNMENT);
                innerPanel.add(label);
                
                Font baseFont = label.getFont();
                Font smallerFont = new Font(
                    baseFont.getFontName(),
                    baseFont.getStyle(),
                    Math.max(4, (int)(baseFont.getSize()*0.7))
                );
                Font smallerBoldFont = new Font(
                    baseFont.getFontName(),
                    baseFont.getStyle()&Font.BOLD,
                    Math.max(4, (int)(baseFont.getSize()*0.7))
                );
                
                JPanel periodsPanel = new JPanel();
                periodsPanel.setLayout(new BoxLayout(periodsPanel, BoxLayout.LINE_AXIS));
                periodsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
                periodsPanel.setOpaque(false);
                periodsPanel.setBorder(new EmptyBorder(4, 1, 1, 1));
                innerPanel.add(periodsPanel);
                
                PeriodSet activityPeriods = activityPeriodsMap.get(activity);
                
                JLabel periodsTitleLabel = new JLabel(String.format(text("ui.swing.dialog.blockactivityorganizer.blockpanel.activity.periods"), activityPeriods.size()));
                periodsTitleLabel.setFont(smallerBoldFont);
                periodsPanel.add(periodsTitleLabel);
                
                boolean first = true;
                StringBuilder periodsTextBuilder = new StringBuilder();
                for (Period period: document.getPeriodStore()) {
                    if (activityPeriods.contains(period)) {
                        if (first) {
                            first = false;
                        } else {
                            periodsTextBuilder.append(", ");
                        }
                        periodsTextBuilder.append(period.getLabel());
                    }
                }
                String periodsText = periodsTextBuilder.toString();
                
                JLabel periodListLabel = new JLabel(" "+periodsText);
                periodListLabel.setFont(smallerFont);
                periodsPanel.add(periodListLabel);
                
                
                if (cellHasFocus) {
                    outerPanel.setBorder(new CompoundBorder(new EmptyBorder(2, 2, 2, 2), new LineBorder(java.awt.Color.BLACK)));
                    innerPanel.setBackground(new java.awt.Color(0xCCCCFF));
                } else {
                    outerPanel.setBorder(new EmptyBorder(3, 3, 3, 3));
                    innerPanel.setBackground(new java.awt.Color(0xEEEEEE));
                }
                
                java.awt.Color textColor;
                if (activity == handler.getCopiedObject()) {
                    textColor = new java.awt.Color(0x999999);
                } else {
                    textColor = java.awt.Color.BLACK;
                }
                label.setForeground(textColor);
                periodsTitleLabel.setForeground(textColor);
                periodListLabel.setForeground(textColor);
                
                return outerPanel;
            }
            
        }

        public class PeriodsEditDialog extends EditDialog {
            
            private static final long serialVersionUID = 1L;
            

            private final Activity activity;
            
            private final PeriodSet periodsReference;
            
            private final Map<Period, JCheckBox> periodCheckBoxMap;
            
            
            public PeriodsEditDialog(Activity activity, PeriodSet periodsReference) {
                super(BlockActivityOrganizerDialog.this.parent);
                this.activity = activity;
                this.periodsReference = periodsReference;
                this.periodCheckBoxMap = new HashMap<Period, JCheckBox>();
                this.title = text("ui.swing.dialog.blockactivityorganizer.periods.title");
                this.formWidth = 300;
                this.leftWidth = 100;
                init();
            }

            @Override
            protected void build() {
                addRow(text("ui.swing.dialog.blockactivityorganizer.periods.block"), new JLabel(block.getLabel()));
                addRow(text("ui.swing.dialog.blockactivityorganizer.periods.activity"), new JLabel(activity.getLabel()));
                
                JPanel periodsPanel = new JPanel();
                periodsPanel.setLayout(new BoxLayout(periodsPanel, BoxLayout.PAGE_AXIS));
                
                for (final Period period: BlockActivityOrganizerDialog.this.document.getPeriodStore()) {
                    final JCheckBox checkBox = new JCheckBox(period.getLabel());
                    periodsPanel.add(checkBox);
                    periodCheckBoxMap.put(period, checkBox);
                }

                addRow(text("ui.swing.dialog.blockactivityorganizer.periods.periods"), periodsPanel);
            }
            
            @Override
            protected void load() {
                for (Period period: periodsReference) {
                    periodCheckBoxMap.get(period).setSelected(true);
                }
            }

            @Override
            protected void save() {
                periodsReference.clear();
                for (Map.Entry<Period, JCheckBox> entry: periodCheckBoxMap.entrySet()) {
                    Period period = entry.getKey();
                    JCheckBox checkBox = entry.getValue();
                    if (checkBox.isSelected()) {
                        periodsReference.add(period);
                    }
                }
            }

        }
        
    }
    
    
}

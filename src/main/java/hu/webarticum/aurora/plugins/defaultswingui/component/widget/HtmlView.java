package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import javax.swing.JEditorPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import hu.webarticum.aurora.plugins.defaultswingui.util.MatchReplacer;

public class HtmlView extends JEditorPane {

    private static final long serialVersionUID = 1L;

    protected String html;
    
    protected String translatedHtml;
    
    public HtmlView() {
        this("");
    }
    
    public HtmlView(String html) {
        setOpaque(true);
        setBorder(new EmptyBorder(10, 10, 10, 10));
        setContentType("text/html");
        setEditable(false);
        setHtml(html);
        
        addHyperlinkListener(new HyperlinkListener() {
            
            @Override
            public void hyperlinkUpdate(HyperlinkEvent ev) {
                if (ev.getEventType()==HyperlinkEvent.EventType.ACTIVATED) {
                    if (Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        if (desktop!=null && desktop.isSupported(Desktop.Action.BROWSE)) {
                            URI uri;
                            try {
                                uri = ev.getURL().toURI();
                            } catch (URISyntaxException e) {
                                return;
                            }
                            try {
                                desktop.browse(uri);
                            } catch (IOException e) {
                            }
                        }
                    }
                }
            }
            
        });
        
        try {
            HTMLEditorKit htmlEditorKit = (HTMLEditorKit)getEditorKit();
            StyleSheet styleSheet = htmlEditorKit.getStyleSheet();
            styleSheet.addRule("a:hover{color:red;}");
        } catch (Exception e) {
        }
    }
    
    public void setHtml(String html) {
        this.html = html;
        translatedHtml = translateHtml(html);
        setText(translatedHtml);
    }

    public String getHtml() {
        return html;
    }

    public String getTranslatedHtml() {
        return translatedHtml;
    }
    
    protected String translateHtml(String html) {
        MatchReplacer replacer = new MatchReplacer(html);
        replacer.replaceAll(Pattern.compile("<(p|h1)\\b[^>]*>"), new MatchReplacer.ReplaceCallback() {
            
            @Override
            public String replace(MatchResult result) {
                String tagHtml = result.group();
                String tagName = result.group(1);
                if (tagName.equals("p")) {
                    return setTagStyle(tagHtml, "margin", "7px 0px", false);
                } else if (tagName.equals("h1")) {
                    return setTagStyle(tagHtml, "font-size", "120%", false);
                } else {
                    return tagHtml;
                }
            }
            
        });
        return replacer.getText();
    }
    
    protected String setTagStyle(String tagHtml, final String property, final String value, final boolean replace) {
        Pattern pattern = Pattern.compile("^(<[^> \"]+\\b(\\s+\\w+=\"[^\"]*\")*\\s+style=\")([^\"]*)(\"(\\s+\\w+=\"[^\"]*\")*\\s*/?>)$");
        if (pattern.matcher(tagHtml).matches()) {
            MatchReplacer replacer = new MatchReplacer(tagHtml);
            replacer.replaceAll(pattern, new MatchReplacer.ReplaceCallback() {
                
                @Override
                public String replace(MatchResult result) {
                    String styleContent = htmldecode(result.group(3));
                    styleContent = setStyle(styleContent, property, value, replace);
                    return result.group(1)+htmlencode(styleContent)+result.group(4);
                }
                
            });
            return replacer.getText();
        } else {
            return tagHtml.replaceAll("^<[^> \"]+\\b", "$0 style=\""+htmlencode(property)+":"+htmlencode(value)+"\"");
        }
    }
    
    protected String setStyle(String styleStr, final String property, final String value, final boolean replace) {
        styleStr = styleStr.trim();
        if (styleStr.isEmpty()) {
            return property+":"+value+";";
        }
        if (styleStr.charAt(styleStr.length()-1)!=';') {
            styleStr += ";";
        }
        String valueSubPattern = "\\s*:\\s*(([^;\"]+|\"[^\"]+\")+)\\s*;\\s*";
        String xValueSubPattern = "\\s*:\\s*)(([^;\"]+|\"[^\"]+\")+)(\\s*;\\s*";
        String ruleSubPattern = "[^;\"]+"+valueSubPattern;
        Pattern pattern = Pattern.compile("^(("+ruleSubPattern+")*"+Pattern.quote(property)+xValueSubPattern+"("+ruleSubPattern+")*)$");
        if (pattern.matcher(styleStr).matches()) {
            if (replace) {
                MatchReplacer replacer = new MatchReplacer(styleStr);
                replacer.replaceAll(pattern, new MatchReplacer.ReplaceCallback() {
                    
                    @Override
                    public String replace(MatchResult result) {
                        String preText = result.group(1);
                        String postText = result.group(7);
                        return preText+value+postText;
                    }
                    
                });
                return replacer.getText();
            } else {
                return styleStr;
            }
        } else {
            return styleStr+property+":"+value+";";
        }
    }
    
    public static String htmlencode(String value) {
        return value
            .replace("&", "&amp;")
            .replace("<", "&lt;")
            .replace(">", "&gt;")
            .replace("\"", "&quot;")
        ;
    }
    
    public static String htmldecode(String encodedValue) {
        return encodedValue
            .replace("&quot;", "\"")
            .replace("&gt;", ">")
            .replace("&lt;", "<")
            .replace("&amp;","&")
        ;
    }
    
}

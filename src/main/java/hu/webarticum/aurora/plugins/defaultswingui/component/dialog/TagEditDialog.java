package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.EnumMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hu.webarticum.aurora.app.memento.TagMemento;
import hu.webarticum.aurora.app.util.AcronymGenerator;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.ColorButton;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.OnOffPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.TimeLimitManagerPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.TimingSetManagerPanel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.FieldSynchronizer;
import hu.webarticum.chm.Command;


public class TagEditDialog extends LabeledEditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private Tag tag;

    private JTabbedPane mainTabbedPane;
    
    private JComboBox<Labeled.PairWrapper<Tag.Type>> typeComboBox;

    private JTextField acronymField;
    
    private ColorButton colorButton;
    
    private Map<Tag.Type, Labeled.PairWrapper<Tag.Type>> typeMap;
    
    private OnOffPanel timingSetManagerOnOffPanel;
    
    private TimingSetManagerPanel timingSetManagerPanel;

    private OnOffPanel timeLimitManagerOnOffPanel;
    
    private TimeLimitManagerPanel timeLimitManagerPanel;

    private FieldSynchronizer acronymSynchronizer;
    
    public TagEditDialog(JFrame parent, Document document, Tag tag) {
        super(parent);
        this.title = text("ui.swing.dialog.tagedit.title");
        this.document = document;
        this.tag = tag;
        typeMap = new EnumMap<Tag.Type, Labeled.PairWrapper<Tag.Type>>(Tag.Type.class);
        typeMap.put(Tag.Type.SUBJECT, new Labeled.PairWrapper<Tag.Type>(text("core.tag.type.subject"), Tag.Type.SUBJECT));
        typeMap.put(Tag.Type.LANGUAGE, new Labeled.PairWrapper<Tag.Type>(text("core.tag.type.language"), Tag.Type.LANGUAGE));
        typeMap.put(Tag.Type.OTHER, new Labeled.PairWrapper<Tag.Type>(text("core.tag.type.other"), Tag.Type.OTHER));
        init();
    }
    
    @Override
    protected void build() {
        mainTabbedPane = new JTabbedPane();
        mainPanel.add(mainTabbedPane);
        
        JPanel tabPanel;
        JPanel tabInnerPanel;

        int tabwidth = formWidth-30;

        tabPanel = new JPanel();
        tabPanel.setLayout(new BorderLayout());
        tabPanel.setBackground(new java.awt.Color(250, 247, 242));
        tabInnerPanel = new JPanel();
        tabInnerPanel.setOpaque(false);
        tabPanel.add(tabInnerPanel, BorderLayout.PAGE_START);
        tabInnerPanel.setLayout(new BoxLayout(tabInnerPanel, BoxLayout.PAGE_AXIS));
        tabInnerPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
        mainTabbedPane.addTab(text("ui.swing.dialog.tagedit.common_data"), tabPanel);

        labelField = new JTextField();
        labelField.setMaximumSize(new Dimension(700, 25));
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.tagedit.common_data.label"), labelField, tabwidth, 150);

        acronymField = new JTextField();
        acronymField.setMaximumSize(new Dimension(700, 25));
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.resourceedit.common_data.acronym"), acronymField, tabwidth, 150);

        acronymSynchronizer = new FieldSynchronizer() {
            
            @Override
            public String generateSecondaryValue(String primaryValue) {
                return AcronymGenerator.generateAcronym(primaryValue);
            }
            
        };
        acronymSynchronizer.bind(labelField, acronymField);
        
        typeComboBox = new JComboBox<Labeled.PairWrapper<Tag.Type>>();
        typeComboBox.setMaximumSize(new Dimension(700, 25));
        typeComboBox.addItem(typeMap.get(Tag.Type.SUBJECT));
        typeComboBox.addItem(typeMap.get(Tag.Type.LANGUAGE));
        typeComboBox.addItem(typeMap.get(Tag.Type.OTHER));
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.tagedit.common_data.type"), typeComboBox, tabwidth, 150);
        
        colorButton = new ColorButton(parent);
        addRowTo(tabInnerPanel, new MultilingualLabel("ui.swing.dialog.tagedit.common_data.color"), colorButton, tabwidth, 150);

        
        // new tab
        
        tabPanel = new JPanel();
        tabPanel.setLayout(new BorderLayout());
        tabPanel.setBackground(new java.awt.Color(250, 247, 242));
        tabInnerPanel = new JPanel();
        tabInnerPanel.setOpaque(false);
        tabPanel.add(tabInnerPanel, BorderLayout.PAGE_START);
        tabInnerPanel.setLayout(new BoxLayout(tabInnerPanel, BoxLayout.PAGE_AXIS));
        tabInnerPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        mainTabbedPane.addTab(text("ui.swing.dialog.tagedit.schedule"), tabPanel);

        timingSetManagerPanel = new TimingSetManagerPanel(parent, document);
        timingSetManagerOnOffPanel = new OnOffPanel(
            text("ui.swing.dialog.tagedit.schedule.timingset"),
            text("ui.swing.dialog.tagedit.schedule.timingset.on"),
            timingSetManagerPanel
        );
        tabInnerPanel.add(timingSetManagerOnOffPanel);

        timingSetManagerPanel.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent ev) {
                if (loaded && !timingSetManagerPanel.isEmpty()) {
                    timingSetManagerOnOffPanel.setOn(true);
                }
            }
            
        });

        JPanel spacerPanel = new JPanel();
        spacerPanel.setOpaque(false);
        spacerPanel.setPreferredSize(new Dimension(12, 12));
        tabInnerPanel.add(spacerPanel);
        
        timeLimitManagerPanel = new TimeLimitManagerPanel(parent, document, tag);
        timeLimitManagerOnOffPanel = new OnOffPanel(
            text("ui.swing.dialog.tagedit.schedule.timelimit"),
            text("ui.swing.dialog.tagedit.schedule.timelimit.on"),
            timeLimitManagerPanel
        );
        tabInnerPanel.add(timeLimitManagerOnOffPanel);

        timeLimitManagerPanel.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent ev) {
                if (loaded && !timeLimitManagerPanel.isEmpty()) {
                    timeLimitManagerOnOffPanel.setOn(true);
                }
            }
            
        });

    }

    @Override
    protected void load() {
        labelField.setText(tag.getLabel());
        acronymField.setText(tag.getAcronym());
        typeComboBox.setSelectedItem(typeMap.get(tag.getType()));
        colorButton.setSelectedColor(tag.getColor());
        
        timingSetManagerOnOffPanel.setOn(tag.isTimingSetEnabled());
        timingSetManagerPanel.loadFrom(tag.getTimingSetManager());
        
        timeLimitManagerOnOffPanel.setOn(tag.isTimeLimitEnabled());
        timeLimitManagerPanel.loadFrom(tag.getTimeLimitManager());
    }

    
    @Override
    protected void afterLoad() {
        super.afterLoad();

        bindModifyListener(acronymField);
        bindModifyListener(typeComboBox);
        bindModifyListener(colorButton);
        bindModifyListener(timingSetManagerOnOffPanel);
        bindModifyListener(timingSetManagerPanel);
        bindModifyListener(timeLimitManagerOnOffPanel);
        bindModifyListener(timeLimitManagerPanel);
    }
    
    
    @SuppressWarnings("unchecked")
    @Override
    protected void save() {
        if (!modified) {
            return;
        }
        
        TagMemento newMemento = new TagMemento(
            labelField.getText(), acronymField.getText(), colorButton.getSelectedColor(),
            timingSetManagerOnOffPanel.isOn(), timeLimitManagerOnOffPanel.isOn(),
            timingSetManagerPanel.createMemento(),
            timeLimitManagerPanel.createMemento(),
            ((Labeled.PairWrapper<Tag.Type>)typeComboBox.getSelectedItem()).get()
        );
        
        Command command;
        Document.TagStore tagStore = document.getTagStore();
        
        if (tagStore.contains(tag)) {
            TagMemento oldMemento = new TagMemento(tag);
            command = new StoreItemEditCommand<Tag>(
                "ui.swing.dialog.tagedit.command.edit",
                tagStore, tag, oldMemento, newMemento
            );
        } else {
            newMemento.apply(tag);
            command = new StoreItemRegisterCommand<Tag>(
                "ui.swing.dialog.tagedit.command.register",
                tagStore, tag
            );
        }
        
        history().addAndExecute(command);
    }

}

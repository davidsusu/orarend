package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import io.github.dheid.fontchooser.FontDialog;

public class FontButton extends JButton {

    private static final long serialVersionUID = 1L;

    
    private static Font defaultFont = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
    
    
    private JFrame parent;
    
    private Font font;
    

    public FontButton(JFrame parent) {
        this(parent, defaultFont);
    }

    public FontButton(JFrame parent, Font font) {
        this.parent = parent;
        this.font = font;
        redraw();
        
        addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                openDialog();
            }
            
        });
    }

    public void openDialog() {
        FontDialog fontDialog = new FontDialog(
            parent,
            text("ui.swing.widget.fontbutton.dialog.title"),
            true
        );
        fontDialog.setSelectedFont(font);
        fontDialog.setDefaultCloseOperation(FontDialog.DISPOSE_ON_CLOSE);
        fontDialog.setVisible(true);
        if (!fontDialog.isCancelSelected()) {
            Font newFont = fontDialog.getSelectedFont();
            if (newFont != null) {
                setSelectedFont(newFont);
            }
        }
    }
    
    public Font getSelectedFont() {
        return font;
    }
    
    public void setSelectedFont(Font font) {
        Font oldFont = this.font;
        this.font = font;
        redraw();
        fireChange(oldFont, font);
    }

    private void redraw() {
        Font currentFont = getFont();
        Font buttonFont = new Font(font.getName(), font.getStyle(), currentFont.getSize());
        String buttonText = 
            font.getFontName() +
            " (" + font.getSize() + ")"
        ;
        setFont(buttonFont);
        setText(buttonText);
        setToolTipText(buttonText);
    }
    
    @Override
    public Object[] getSelectedObjects() {
        return new Object[]{null};
    }
    
    private void fireChange(Font oldFont, Font newFont) {
        ItemListener[] listeners = listenerList.getListeners(ItemListener.class);
        ItemEvent deselectEvent = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, oldFont, ItemEvent.DESELECTED);
        for (ItemListener listener: listeners) {
            listener.itemStateChanged(deselectEvent);
        }
        ItemEvent selectEvent = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, newFont, ItemEvent.SELECTED);
        for (ItemListener listener: listeners) {
            listener.itemStateChanged(selectEvent);
        }
    }

}

package hu.webarticum.aurora.plugins.defaultswingui.component.widget;


import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.EditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.PeriodSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.ResourceSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.TagSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualComboBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.aurora.plugins.defaultswingui.util.WrapLayout;

public class BlockFilterPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private JFrame parent;
    
    private Document document;

    private Tag tag = null;

    private Resource resource = null;

    private Period period = null;
    
    private JPanel tagPanel;
    
    private CardLayout tagCardLayout;
    
    private static final String TAGCARDLAYOUT_NOTAG = "No tag";
    
    private static final String TAGCARDLAYOUT_TAG = "Tag";
    
    private JButton tagButton;
    
    private JPanel resourcePanel;
    
    private CardLayout resourceCardLayout;
    
    private static final String RESOURCECARDLAYOUT_NORESOURCE = "No resource";
    
    private static final String RESOURCECARDLAYOUT_RESOURCE = "Resource";
    
    private JButton resourceButton;
    
    private JPanel periodPanel;
    
    private CardLayout periodCardLayout;
    
    private static final String PERIODCARDLAYOUT_NOPERIOD = "No period";
    
    private static final String PERIODCARDLAYOUT_PERIOD = "Period";
    
    private JButton periodButton;

    private JPanel colorAspectComboBoxWrapPanel;
    
    private MultilingualComboBox<Object> colorAspectComboBox;
    
    private boolean loaded;
    
    private List<BlockFilterChangeListener> listeners = new ArrayList<BlockFilterChangeListener>();
    
    public BlockFilterPanel(JFrame parent, Document document) {
        setLayout(new WrapLayout(FlowLayout.LEFT));
        this.parent = parent;
        this.document = document;
        loaded = false;
        build();
        load();
    }
    
    public void reload() {
        load();
    }
    
    public void setTag(Tag tag) {
        if (this.tag != tag) {
            this.tag = tag;
            reload();
            fireChangeEvent(true);
        }
    }
    
    public void setResource(Resource resource) {
        if (this.resource != resource) {
            this.resource = resource;
            reload();
            fireChangeEvent(true);
        }
    }

    public void setPeriod(Period period) {
        if (this.period != period) {
            this.period = period;
            reload();
            fireChangeEvent(true);
        }
    }
    
    public void addChangeListener(BlockFilterChangeListener listener) {
        listeners.add(listener);
    }
    
    public Period getPeriod() {
        return period;
    }
    
    public ActivityFilter getActivityFilter() {
        ActivityFilter activityFilter;
        
        List<ActivityFilter> activityFilters = new ArrayList<ActivityFilter>();
        if (tag!=null) {
            activityFilters.add(new ActivityFilter.HasTag(tag));
        }
        if (resource!=null) {
            activityFilters.add(new ActivityFilter.HasResource(resource));
        }
        if (activityFilters.isEmpty()) {
            activityFilter = new ActivityFilter.TrueActivityFilter();
        } else if (activityFilters.size()==1) {
            activityFilter = activityFilters.get(0);
        } else {
            activityFilter = new ActivityFilter.And(activityFilters);
        }
        
        return activityFilter;
    }
    
    public BlockFilter getBlockFilter() {
        ActivityFilter activityFilter = getActivityFilter();
        Period period = getPeriod();
        if (activityFilter instanceof ActivityFilter.TrueActivityFilter) {
            if (period==null) {
                return new BlockFilter.TrueBlockFilter();
            } else {
                return new BlockFilter.PeriodBlockFilter(period);
            }
        } else {
            if (period==null) {
                return new BlockFilter.ActivityBlockFilter(activityFilter);
            } else {
                return new BlockFilter.PeriodActivityBlockFilter(activityFilter, period);
            }
        }
    }
    
    public Object getColorAspect() {
        Object selectedData = colorAspectComboBox.getSelectedData();
        if (selectedData instanceof Tag.Type || selectedData instanceof Resource.Type) {
            return selectedData;
        }
        return null;
    }
    
    public void setColorAspectVisible(boolean visible) {
        colorAspectComboBoxWrapPanel.setVisible(visible);
    }
    
    public void loadFrom(BlockFilterPanel other) {
        setTag(other.tag);
        setResource(other.resource);
        setPeriod(other.period);
    }
    
    public void fireChangeEvent(boolean filterChanged) {
        BlockFilterChangeEvent event = new BlockFilterChangeEvent(this, filterChanged);
        for (BlockFilterChangeListener listener: listeners) {
            listener.changed(event);
        }
    }
    
    private void build() {
        JPanel wrapPanel;
        
        
        // TAG:
        
        wrapPanel = new JPanel();
        wrapPanel.setLayout(new BoxLayout(wrapPanel, BoxLayout.LINE_AXIS));
        
        wrapPanel.add(new MultilingualLabel("ui.swing.widget.blockfilter.tag"));
        
        tagPanel = new JPanel();
        tagCardLayout = new CardLayout();
        tagPanel.setLayout(tagCardLayout);
        tagPanel.setMinimumSize(new Dimension(120, 30));
        wrapPanel.add(tagPanel);
        
        JPanel tagCardNoTag = new JPanel();
        tagCardNoTag.setLayout(new BoxLayout(tagCardNoTag, BoxLayout.LINE_AXIS));
        tagPanel.add(tagCardNoTag, TAGCARDLAYOUT_NOTAG);

        JButton noTagButton = new MultilingualButton("ui.swing.widget.blockfilter.tag.no_selection");
        noTagButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                GeneralWrapper<Tag> wrapper = new GeneralWrapper<Tag>();
                TagSelectDialog dialog = new TagSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    setTag(wrapper.get());
                }
            }
            
        });
        tagCardNoTag.add(noTagButton);
        
        JPanel tagCardTag = new JPanel();
        tagCardTag.setLayout(new BoxLayout(tagCardTag, BoxLayout.LINE_AXIS));
        tagPanel.add(tagCardTag, TAGCARDLAYOUT_TAG);
        
        tagButton = new JButton("?");
        tagButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                GeneralWrapper<Tag> wrapper = new GeneralWrapper<Tag>(BlockFilterPanel.this.tag);
                TagSelectDialog dialog = new TagSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    setTag(wrapper.get());
                }
            }
            
        });
        tagCardTag.add(tagButton);
        
        JButton removeTagButton = new JButton();
        removeTagButton.setIcon(IconLoader.loadIcon("abort"));
        removeTagButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                setTag(null);
            }
            
        });
        tagCardTag.add(removeTagButton);
        
        add(wrapPanel);
        
        
        add(new JLabel("  "));
        
        
        // RESOURCE:
        
        wrapPanel = new JPanel();
        wrapPanel.setLayout(new BoxLayout(wrapPanel, BoxLayout.LINE_AXIS));

        wrapPanel.add(new MultilingualLabel("ui.swing.widget.blockfilter.resource"));
        
        resourcePanel = new JPanel();
        resourceCardLayout = new CardLayout();
        resourcePanel.setLayout(resourceCardLayout);
        wrapPanel.add(resourcePanel);
        
        JPanel resourceCardNoResource = new JPanel();
        resourceCardNoResource.setLayout(new BoxLayout(resourceCardNoResource, BoxLayout.LINE_AXIS));
        resourcePanel.add(resourceCardNoResource, RESOURCECARDLAYOUT_NORESOURCE);

        JButton noResourceButton = new MultilingualButton("ui.swing.widget.blockfilter.resource.no_selection");
        noResourceButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                GeneralWrapper<Resource> wrapper = new GeneralWrapper<Resource>();
                ResourceSelectDialog dialog = new ResourceSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    setResource(wrapper.get());
                }
            }
            
        });
        resourceCardNoResource.add(noResourceButton);
        
        JPanel resourceCardResource = new JPanel();
        resourceCardResource.setLayout(new BoxLayout(resourceCardResource, BoxLayout.LINE_AXIS));
        resourcePanel.add(resourceCardResource, RESOURCECARDLAYOUT_RESOURCE);
        
        resourceButton = new JButton("?");
        resourceButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Resource> wrapper = new GeneralWrapper<Resource>(BlockFilterPanel.this.resource);
                ResourceSelectDialog dialog = new ResourceSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    setResource(wrapper.get());
                }
            }
            
        });
        resourceCardResource.add(resourceButton);
        
        JButton removeResourceButton = new JButton();
        removeResourceButton.setIcon(IconLoader.loadIcon("abort"));
        removeResourceButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                setResource(null);
            }
            
        });
        resourceCardResource.add(removeResourceButton);
        
        add(wrapPanel);
        
        
        add(new JLabel("  "));
        
        
        // PERIOD:
        
        wrapPanel = new JPanel();
        wrapPanel.setLayout(new BoxLayout(wrapPanel, BoxLayout.LINE_AXIS));

        wrapPanel.add(new MultilingualLabel("ui.swing.widget.blockfilter.period"));
        
        periodPanel = new JPanel();
        periodCardLayout = new CardLayout();
        periodPanel.setLayout(periodCardLayout);
        wrapPanel.add(periodPanel);
        
        JPanel periodCardNoPeriod = new JPanel();
        periodCardNoPeriod.setLayout(new BoxLayout(periodCardNoPeriod, BoxLayout.LINE_AXIS));
        periodPanel.add(periodCardNoPeriod, PERIODCARDLAYOUT_NOPERIOD);

        JButton noPeriodButton = new MultilingualButton("ui.swing.widget.blockfilter.period.no_selection");
        noPeriodButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                GeneralWrapper<Period> wrapper = new GeneralWrapper<Period>();
                PeriodSelectDialog dialog = new PeriodSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    setPeriod(wrapper.get());
                }
            }
            
        });
        periodCardNoPeriod.add(noPeriodButton);
        
        JPanel periodCardPeriod = new JPanel();
        periodCardPeriod.setLayout(new BoxLayout(periodCardPeriod, BoxLayout.LINE_AXIS));
        periodPanel.add(periodCardPeriod, PERIODCARDLAYOUT_PERIOD);
        
        periodButton = new JButton("?");
        periodButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Period> wrapper = new GeneralWrapper<Period>(BlockFilterPanel.this.period);
                PeriodSelectDialog dialog = new PeriodSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    setPeriod(wrapper.get());
                }
            }
            
        });
        periodCardPeriod.add(periodButton);
        
        JButton removePeriodButton = new JButton();
        removePeriodButton.setIcon(IconLoader.loadIcon("abort"));
        removePeriodButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                setPeriod(null);
            }
            
        });
        periodCardPeriod.add(removePeriodButton);
        
        add(wrapPanel);
        
        
        add(new JLabel("  "));
        
        
        // COLOR ASPECT
        
        colorAspectComboBoxWrapPanel = new JPanel();
        colorAspectComboBoxWrapPanel.setLayout(new BoxLayout(colorAspectComboBoxWrapPanel, BoxLayout.LINE_AXIS));
        
        colorAspectComboBoxWrapPanel.add(new MultilingualLabel("ui.swing.widget.blockfilter.coloring"));
        
        colorAspectComboBox = new MultilingualComboBox<Object>();

        colorAspectComboBox.addMultilingualItem("core.aspect.type.subject", Tag.Type.SUBJECT);
        colorAspectComboBox.addMultilingualItem("core.aspect.type.language", Tag.Type.LANGUAGE);
        colorAspectComboBox.addMultilingualItem("core.aspect.type.other_tag", Tag.Type.OTHER);
        colorAspectComboBox.addMultilingualItem("core.aspect.type.class", Resource.Type.CLASS);
        colorAspectComboBox.addMultilingualItem("core.aspect.type.person", Resource.Type.PERSON);
        colorAspectComboBox.addMultilingualItem("core.aspect.type.locale", Resource.Type.LOCALE);
        colorAspectComboBox.addMultilingualItem("core.aspect.type.object", Resource.Type.OBJECT);
        colorAspectComboBox.addMultilingualItem("core.aspect.type.other_resource", Resource.Type.OTHER);
        colorAspectComboBox.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                if (loaded) {
                    fireChangeEvent(false);
                }
            }
            
        });
        colorAspectComboBoxWrapPanel.add(colorAspectComboBox);
        
        add(colorAspectComboBoxWrapPanel);
    }
    
    private void load() {
        loaded = false;
        
        if (tag!=null && !document.getTagStore().contains(tag)) {
            tag = null;
        }
        if (tag==null) {
            tagCardLayout.show(tagPanel, TAGCARDLAYOUT_NOTAG);
            tagButton.setText("?");
        } else {
            tagCardLayout.show(tagPanel, TAGCARDLAYOUT_TAG);
            String label = tag.getLabel();
            if (label.length()>16) {
                label = label.substring(0, 15)+"…";
            }
            tagButton.setText(label);
        }

        if (resource!=null && !document.getResourceStore().contains(resource)) {
            resource = null;
        }
        if (resource==null) {
            resourceCardLayout.show(resourcePanel, RESOURCECARDLAYOUT_NORESOURCE);
            resourceButton.setText("?");
        } else {
            resourceCardLayout.show(resourcePanel, RESOURCECARDLAYOUT_RESOURCE);
            String label = resource.getLabel();
            if (label.length()>16) {
                label = label.substring(0, 15)+"…";
            }
            resourceButton.setText(label);
        }

        if (period!=null && !document.getPeriodStore().contains(period)) {
            period = null;
        }
        if (period==null) {
            periodCardLayout.show(periodPanel, PERIODCARDLAYOUT_NOPERIOD);
            periodButton.setText("?");
        } else {
            periodCardLayout.show(periodPanel, PERIODCARDLAYOUT_PERIOD);
            String label = period.getLabel();
            if (label.length()>16) {
                label = label.substring(0, 15)+"…";
            }
            periodButton.setText(label);
        }
        
        loaded = true;
    }
    
    public interface BlockFilterChangeListener extends EventListener {
        
        public void changed(BlockFilterChangeEvent ev);
        
    }
    
    public class BlockFilterChangeEvent {
        
        private final BlockFilterPanel blockFilterPanel;
        private final boolean filterChanged;
        
        public BlockFilterChangeEvent(BlockFilterPanel blockFilterPanel, boolean filterChanged) {
            this.blockFilterPanel = blockFilterPanel;
            this.filterChanged = filterChanged;
        }
        
        public BlockFilterPanel getBlockFilterPanel() {
            return blockFilterPanel;
        }
        
        public boolean isFilterChanged() {
            return filterChanged;
        }
        
    }
    
}

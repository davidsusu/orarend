package hu.webarticum.aurora.plugins.defaultswingui;

import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.jpluginmanager.core.AbstractSimplePlugin;
import hu.webarticum.jpluginmanager.core.Version;

public class DefaultSwingUiPlugin extends AbstractSimplePlugin {
    
    public DefaultSwingUiPlugin() {
        extensionBinder.bind(CommonExtensions.Runner.class, UninstallRunner.class);
        extensionBinder.bind(CommonExtensions.Runner.class, DefaultSwingUiRunner.class);
    }
    
    @Override
    public String getName() {
        return "xxx.swingui";
    }

    @Override
    public Version getVersion() {
        return new Version("0.1.0");
    }

    @Override
    public String getLabel() {
        return "Default Swing UI";
    }

}

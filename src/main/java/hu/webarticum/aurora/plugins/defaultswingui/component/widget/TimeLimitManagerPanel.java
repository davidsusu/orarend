package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import hu.webarticum.aurora.app.memento.AspectMementoBase;
import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.MultiComparator;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.time.AlwaysTimeLimit;
import hu.webarticum.aurora.core.model.time.TimeLimit;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.EditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.OptionPaneUtil;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.PeriodSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.TimeLimitEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class TimeLimitManagerPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private JFrame parent;

    private Document document;

    private Labeled contextObject;
    
    private TimeLimit defaultTimeLimit = AlwaysTimeLimit.INSTANCE;
    
    private JPanel defaultSelectPanel;
    
    private CardLayout defaultSelectCardLayout;

    private static final String DEFAULTSELECTCARDLAYOUT_NOTSELECTED = "notselected";

    private static final String DEFAULTSELECTCARDLAYOUT_SELECTED = "selected";
    
    private JButton selectedButton;
    
    private TreeMap<Period, TimeLimit> periodMap = new TreeMap<Period, TimeLimit>(new MultiComparator<Period>(new Labeled.LabeledComparator()));
    
    private JList<Labeled.Wrapper> periodList;

    private EventListenerList changeListenerList = new EventListenerList();

    public TimeLimitManagerPanel(JFrame parent, Document document) {
        this(parent, document, null);
    }

    public TimeLimitManagerPanel(JFrame parent, Document document, Labeled contextObject) {
        this.parent = parent;
        this.document = document;
        this.contextObject = contextObject;
        build();
    }
    
    private void build() {
        setLayout(new BorderLayout());
        setBorder(new CompoundBorder(
            new LineBorder(new java.awt.Color(150, 150, 150)),
            new EmptyBorder(7, 7, 7, 7)
        ));
        
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        add(topPanel, BorderLayout.PAGE_START);

        topPanel.add(new MultilingualLabel("ui.swing.widget.timelimitmanager.default"), BorderLayout.LINE_START);
        
        defaultSelectPanel = new JPanel();
        defaultSelectCardLayout = new CardLayout();
        defaultSelectPanel.setLayout(defaultSelectCardLayout);
        topPanel.add(defaultSelectPanel, BorderLayout.LINE_END);
        
        JPanel defaultSelectNotSelectedPanel = new JPanel();
        defaultSelectNotSelectedPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        defaultSelectPanel.add(defaultSelectNotSelectedPanel, DEFAULTSELECTCARDLAYOUT_NOTSELECTED);
        JButton notSelectedButton = new MultilingualButton("ui.swing.widget.timelimitmanager.unlimited");
        notSelectedButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<TimeLimit> wrapper = new GeneralWrapper<TimeLimit>(AlwaysTimeLimit.INSTANCE);
                TimeLimitEditDialog dialog = new TimeLimitEditDialog(parent, wrapper, document, contextObject, true);
                dialog.run();

                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    defaultTimeLimit = wrapper.get();
                    
                    refreshDefault();
                    fireChange();
                }
            }
            
        });
        defaultSelectNotSelectedPanel.add(notSelectedButton);
        
        JPanel defaultSelectSelectedPanel = new JPanel();
        defaultSelectSelectedPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        defaultSelectPanel.add(defaultSelectSelectedPanel, DEFAULTSELECTCARDLAYOUT_SELECTED);
        selectedButton = new MultilingualButton("ui.swing.widget.timelimitmanager.limited");
        selectedButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<TimeLimit> wrapper = new GeneralWrapper<TimeLimit>(defaultTimeLimit);
                TimeLimitEditDialog dialog = new TimeLimitEditDialog(parent, wrapper, document, contextObject, true);
                dialog.run();

                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    defaultTimeLimit = wrapper.get();
                    
                    refreshDefault();
                    fireChange();
                }
            }
            
        });
        defaultSelectSelectedPanel.add(selectedButton);
        JButton removeSelectedButton = new JButton();
        removeSelectedButton.setIcon(IconLoader.loadIcon("abort"));
        removeSelectedButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(parent,
                    text("ui.swing.widget.timelimitmanager.remove_default.sure"),
                    text("ui.swing.widget.timelimitmanager.remove_default.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.widget.timelimitmanager.remove_default.cancel"),
                        text("ui.swing.widget.timelimitmanager.remove_default.remove"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.DANGER,
                    },
                    text("ui.swing.widget.timelimitmanager.remove_default.remove")
                );
                if (answer==1) {
                    defaultTimeLimit = AlwaysTimeLimit.INSTANCE;
                    
                    refreshDefault();
                    fireChange();
                }
            }
            
        });
        defaultSelectSelectedPanel.add(removeSelectedButton);

        defaultSelectCardLayout.show(defaultSelectPanel, DEFAULTSELECTCARDLAYOUT_NOTSELECTED);
        
        JPanel periodPanel = new JPanel();
        periodPanel.setLayout(new BorderLayout());
        add(periodPanel, BorderLayout.CENTER);
        
        JPanel periodTopPanel = new JPanel();
        periodTopPanel.setLayout(new BorderLayout());
        periodPanel.add(periodTopPanel, BorderLayout.PAGE_START);

        periodTopPanel.add(new MultilingualLabel("ui.swing.widget.timelimitmanager.per_period"), BorderLayout.LINE_START);
        
        JButton addButton = new JButton();
        addButton.setIcon(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Period> periodWrapper = new GeneralWrapper<Period>();
                PeriodSelectDialog periodDialog = new PeriodSelectDialog(parent, document, periodWrapper);
                periodDialog.run();
                
                if (periodDialog.getResult()!=EditDialog.RESULT_OK) {
                    return;
                }
                
                GeneralWrapper<TimeLimit> timeLimitWrapper = new GeneralWrapper<TimeLimit>();
                TimeLimitEditDialog dialog = new TimeLimitEditDialog(parent, timeLimitWrapper, document, contextObject, true);
                dialog.run();
                
                if (dialog.getResult()!=EditDialog.RESULT_OK) {
                    return;
                }
                
                periodMap.put(periodWrapper.get(), timeLimitWrapper.get());
                
                refreshPeriodList();
                fireChange();
            }
            
        });
        periodTopPanel.add(addButton, BorderLayout.LINE_END);
        
        JPanel periodListPanel = new JPanel();
        periodListPanel.setLayout(new BorderLayout());
        periodPanel.add(periodListPanel, BorderLayout.CENTER);
        
        periodList = new JList<Labeled.Wrapper>();
        JScrollPane periodListScrollPane = new JScrollPane(periodList);
        periodPanel.add(periodListScrollPane, BorderLayout.CENTER);
        periodListScrollPane.setPreferredSize(new Dimension(100, 50));
        final Runnable removeAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = periodList.getSelectedIndex();
                if (selectedIndex<0) {
                    return;
                }
                Period period = (Period)periodList.getModel().getElementAt(selectedIndex).get();
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(parent,
                    text("ui.swing.widget.timelimitmanager.per_period.remove.sure"),
                    text("ui.swing.widget.timelimitmanager.per_period.remove.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.widget.timelimitmanager.per_period.remove.cancel"),
                        text("ui.swing.widget.timelimitmanager.per_period.remove.remove"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.DANGER,
                    },
                    text("ui.swing.widget.timelimitmanager.per_period.remove.remove")
                );
                if (answer==1) {
                    periodMap.remove(period);
                    
                    refreshPeriodList();
                    fireChange();
                }
            }
            
        };
        final Runnable editAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = periodList.getSelectedIndex();
                if (selectedIndex<0) {
                    return;
                }
                Period period = (Period)periodList.getModel().getElementAt(selectedIndex).get();
                GeneralWrapper<TimeLimit> wrapper = new GeneralWrapper<TimeLimit>(periodMap.get(period));
                TimeLimitEditDialog dialog = new TimeLimitEditDialog(parent, wrapper, document, contextObject, true);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    periodMap.put(period, wrapper.get());
                    
                    refreshPeriodList();
                    fireChange();
                }
            }
                
        };
        periodList.addMouseListener(new MouseListener() {
            
            @Override
            public void mouseReleased(MouseEvent ev) {}
            
            @Override
            public void mousePressed(MouseEvent ev) {}
            
            @Override
            public void mouseExited(MouseEvent ev) {}
            
            @Override
            public void mouseEntered(MouseEvent ev) {}
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getButton()==MouseEvent.BUTTON3) {
                    removeAction.run();
                } else if (ev.getButton()==MouseEvent.BUTTON1 && ev.getClickCount()==2) {
                    editAction.run();
                }
            }
        });
        periodList.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {

                switch (ev.getKeyCode()) {
                    case KeyEvent.VK_DELETE:
                        removeAction.run();
                    break;
                    case KeyEvent.VK_ENTER:
                        editAction.run();
                    break;
                }
            }
            
        });
    }

    public void loadFrom(Aspect.TimeLimitManager timeLimitManager) {
        defaultTimeLimit = timeLimitManager.getDefaultTimeLimit();
        if (defaultTimeLimit==null) {
            defaultTimeLimit = AlwaysTimeLimit.INSTANCE;
        }
        
        periodMap.clear();
        for (Map.Entry<Period, TimeLimit> entry: timeLimitManager.getPeriodTimeLimits().entrySet()) {
            periodMap.put(entry.getKey(), entry.getValue());
        }
        
        refreshDefault();
        refreshPeriodList();
        fireChange();
    }

    public void saveTo(Aspect.TimeLimitManager timeLimitManager) {
        timeLimitManager.setDefaultTimeLimit(defaultTimeLimit);
        
        timeLimitManager.removePeriodTimeLimits();
        for (Map.Entry<Period, TimeLimit> entry: periodMap.entrySet()) {
            timeLimitManager.setPeriodTimeLimit(entry.getKey(), entry.getValue());
        }
    }
    
    public AspectMementoBase.TimeLimitManagerMemento createMemento() {
        return new AspectMementoBase.TimeLimitManagerMemento(defaultTimeLimit, getPeriodTimeLimitMap());
    }

    public TimeLimit getDefaultTimingSet() {
        return defaultTimeLimit;
    }
    
    public TreeMap<Period, TimeLimit> getPeriodTimeLimitMap() {
        return new TreeMap<Period, TimeLimit>(periodMap);
    }

    public void refreshDefault() {
        if (defaultTimeLimit.isAlways()) {
            defaultSelectCardLayout.show(defaultSelectPanel, DEFAULTSELECTCARDLAYOUT_NOTSELECTED);
        } else {
            defaultSelectCardLayout.show(defaultSelectPanel, DEFAULTSELECTCARDLAYOUT_SELECTED);
        }
    }
    
    public void refreshPeriodList() {
        DefaultListModel<Labeled.Wrapper> model = new DefaultListModel<Labeled.Wrapper>();
        for (Period period: periodMap.keySet()) {
            model.addElement(new Labeled.Wrapper(period));
        }
        periodList.setModel(model);
    }

    public boolean isEmpty() {
        if (!defaultTimeLimit.isAlways()) {
            return false;
        } else if (!periodMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    
    public void addChangeListener(ChangeListener changeListener) {
        changeListenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        changeListenerList.remove(ChangeListener.class, changeListener);
    }
    
    public void fireChange() {
        ChangeListener[] listeners = changeListenerList.getListeners(ChangeListener.class);
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (ChangeListener listener: listeners) {
            listener.stateChanged(changeEvent);
        }
    }
    
}

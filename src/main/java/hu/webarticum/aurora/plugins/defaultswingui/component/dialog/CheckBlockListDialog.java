package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import hu.webarticum.aurora.app.check.DefaultBlockListProblemCollector;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualRadioButton;

public class CheckBlockListDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private JRadioButton blockListAllRadioButton;
    
    private JRadioButton blockListSpecifiedRadioButton;

    private JButton blockListButton;

    private JRadioButton boardNoneRadioButton;
    
    private JRadioButton boardSpecifiedRadioButton;

    private JButton boardButton;

    private BlockList selectedBlocks = new BlockList();
    
    private Board board = null;

    public CheckBlockListDialog(JFrame parent, Document document) {
        super(parent);
        this.title = text("ui.swing.dialog.checkblocklist.title");
        this.document = document;
        init();
    }

    @Override
    protected void build() {
        ButtonGroup blockListTypeButtonGroup = new ButtonGroup();
        
        JPanel blockListAllPanel = new JPanel();
        blockListAllPanel.setLayout(new BorderLayout());
        
        blockListAllRadioButton = new MultilingualRadioButton("ui.swing.dialog.checkblocklist.all_blocks");
        blockListAllRadioButton.setSelected(true);
        blockListAllPanel.add(blockListAllRadioButton, BorderLayout.CENTER);
        blockListTypeButtonGroup.add(blockListAllRadioButton);
        
        addRow(new MultilingualLabel("ui.swing.dialog.checkblocklist.blocks"), blockListAllPanel);

        
        JPanel blockListSpecifiedPanel = new JPanel();
        blockListSpecifiedPanel.setLayout(new BorderLayout());
        
        blockListSpecifiedRadioButton = new JRadioButton();
        blockListSpecifiedPanel.add(blockListSpecifiedRadioButton, BorderLayout.LINE_START);
        blockListTypeButtonGroup.add(blockListSpecifiedRadioButton);
        
        blockListButton = new JButton("");
        blockListButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                BlockMultiSelectDialog dialog = new BlockMultiSelectDialog(parent, document, selectedBlocks);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    blockListSpecifiedRadioButton.setSelected(true);
                    reloadBlocks();
                }
            }
            
        });
        blockListSpecifiedPanel.add(blockListButton, BorderLayout.CENTER);
        addRow("", blockListSpecifiedPanel);

        ButtonGroup boardTypeButtonGroup = new ButtonGroup();
        
        JPanel boardNonePanel = new JPanel();
        boardNonePanel.setLayout(new BorderLayout());
        
        boardNoneRadioButton = new MultilingualRadioButton("ui.swing.dialog.checkblocklist.board_independent");
        boardNoneRadioButton.setSelected(true);
        boardNonePanel.add(boardNoneRadioButton, BorderLayout.CENTER);
        boardTypeButtonGroup.add(boardNoneRadioButton);
        
        addRow(new MultilingualLabel("ui.swing.dialog.checkblocklist.board"), boardNonePanel);

        
        JPanel boardSpecifiedPanel = new JPanel();
        boardSpecifiedPanel.setLayout(new BorderLayout());
        
        boardSpecifiedRadioButton = new JRadioButton();
        boardSpecifiedPanel.add(boardSpecifiedRadioButton, BorderLayout.LINE_START);
        boardTypeButtonGroup.add(boardSpecifiedRadioButton);
        
        boardButton = new JButton("");
        boardButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Board> wrapper = new GeneralWrapper<Board>(CheckBlockListDialog.this.board);
                BoardSelectDialog dialog = new BoardSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    CheckBlockListDialog.this.board = wrapper.get();
                    reloadBoard();
                    if (CheckBlockListDialog.this.board!=null) {
                        boardSpecifiedRadioButton.setSelected(true);
                    }
                } else if (CheckBlockListDialog.this.board==null) {
                    boardNoneRadioButton.setSelected(true);
                }
            }
            
        });
        boardSpecifiedPanel.add(boardButton, BorderLayout.CENTER);
        addRow("", boardSpecifiedPanel);
    }

    @Override
    protected void load() {
        reloadBlocks();
        reloadBoard();
    }

    @Override
    protected void save() {
        new MessageCollectorRunner(parent, document, new DefaultBlockListProblemCollector(getBlocks(), document, getBoard())).run();
    }
    
    private void reloadBlocks() {
        blockListButton.setText(String.format(text("ui.swing.dialog.checkblocklist.selected_blocks"), selectedBlocks.size()));
    }

    private void reloadBoard() {
        if (board==null) {
            boardButton.setText("");
        } else {
            boardButton.setText(board.getLabel());
        }
    }

    private BlockList getBlocks() {
        if (blockListSpecifiedRadioButton.isSelected()) {
            return selectedBlocks;
        } else {
            return document.getBlockStore().getAll();
        }
    }

    private Board getBoard() {
        return board;
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import java.io.File;
import java.io.IOException;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import javax.swing.JFrame;

import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.FileChooserField;
import hu.webarticum.aurora.x.ascxmlimport.AscXmlImport;

public class ImportDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    
    protected Document document;
    
    protected BooleanSupplier handleUnsavedCallback;
    
    protected Consumer<Document> openDocumentCallback;
    
    protected FileChooserField sourceFileChooserField;

    
    public ImportDialog(
            JFrame parent,
            Document document,
            BooleanSupplier handleUnsavedCallback,
            Consumer<Document> openDocumentCallback) {
        super(parent);
        this.title = "IMPORT"; // TODO
        this.document = document;
        this.handleUnsavedCallback = handleUnsavedCallback;
        this.openDocumentCallback = openDocumentCallback;
        init(true, true);
    }

    
    @Override
    protected void build() {
        sourceFileChooserField = new FileChooserField();
        contentPanel.add(sourceFileChooserField);
    }

    @Override
    protected void load() {
        // TODO
    }
    
    @Override
    protected boolean trySave() {
        if (!handleUnsavedCallback.getAsBoolean()) {
            return false;
        }
        
        // FIXME
        String path = sourceFileChooserField.getTextField().getText();
        
        Document newDocument;
        try {
            newDocument = new AscXmlImport().load(new File(path));
        } catch (IOException e) {
            // TODO
            return false;
        }
        
        openDocumentCallback.accept(newDocument);
        
        return true;
    }
    
    @Override
    protected void save() {
        // trySave() is used
    }

}

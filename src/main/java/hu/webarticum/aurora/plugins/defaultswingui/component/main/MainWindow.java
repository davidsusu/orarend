package hu.webarticum.aurora.plugins.defaultswingui.component.main;


import static hu.webarticum.aurora.app.Shortcut.extensions;
import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.listen;
import static hu.webarticum.aurora.app.Shortcut.text;
import static hu.webarticum.aurora.app.Shortcut.texts;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.Painter;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.check.DefaultBlockProblemCollector;
import hu.webarticum.aurora.app.check.FilteringMessageCollector;
import hu.webarticum.aurora.app.check.Message;
import hu.webarticum.aurora.app.check.MessageCollector;
import hu.webarticum.aurora.app.event.Listener;
import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.memento.BoardMemento;
import hu.webarticum.aurora.app.memento.RemovalHelper;
import hu.webarticum.aurora.app.util.DocumentFactory;
import hu.webarticum.aurora.app.util.FileUtil;
import hu.webarticum.aurora.app.util.RecentFilesManager;
import hu.webarticum.aurora.app.util.Settings;
import hu.webarticum.aurora.app.util.UrlDocumentFactory;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.io.DocumentIo;
import hu.webarticum.aurora.core.io.DocumentIo.DocumentIoQueue;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.ActivityFlow;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Store;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.Tag.Type;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.core.model.time.TimeLimit;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.AboutDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.AscTimetablesExportDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.AutoPutDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.BigSpreadsheetExportDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.BlockActivityOrganizerDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.BlockEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.BlockTimeSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.BoardBlockTimeDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.BoardEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.CheckBlockListDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.CheckBlocksDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.CheckBoardDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.DocumentEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.EditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.IconButtonSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.ImageExportDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.ImportDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.ListSpreadsheetExportDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.OptionPaneUtil;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.OrganizerBlockSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.PeriodEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.ResourceEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.ResourceImportDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.SettingsDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.SwingWorkerProcessDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.TagEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.TemplateSpreadsheetExportDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.TimingSetEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.WebPageExportDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.BlockFilterPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.BlockFilterPanel.BlockFilterChangeEvent;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.ResourceTypeFilterToolBar;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.StoreListPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.TagTypeFilterToolBar;
import hu.webarticum.aurora.plugins.defaultswingui.global.GlobalUiObjects;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualComboBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualComponent;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualMenu;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualMenuItem;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualTabbedPane;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.PathMultilingualContent;
import hu.webarticum.aurora.plugins.defaultswingui.util.ActivityFlowImageRenderer;
import hu.webarticum.aurora.plugins.defaultswingui.util.AutoExtensionFileChooser;
import hu.webarticum.aurora.plugins.defaultswingui.util.BoardImageColorSchemeListBuilder;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.ColorUtil;
import hu.webarticum.aurora.plugins.defaultswingui.util.ComponentTreeUtil;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.aurora.plugins.defaultswingui.util.MainIconImageListProvider;
import hu.webarticum.aurora.plugins.defaultswingui.util.NotificationQueue;
import hu.webarticum.aurora.plugins.defaultswingui.util.PaintedPanel;
import hu.webarticum.aurora.plugins.defaultswingui.util.SizingPanel;
import hu.webarticum.aurora.plugins.defaultswingui.util.StripsPatternDrawer;
import hu.webarticum.aurora.plugins.defaultswingui.util.VerticallyScrollablePanel;
import hu.webarticum.aurora.plugins.defaultswingui.util.WrapLayout;
import hu.webarticum.aurora.x.autoputsetup.SchoolAutoputSetupDialog;
import hu.webarticum.aurora.x.documentwizard.NewDocumentWizardDialog;
import hu.webarticum.chm.AbstractCommand;
import hu.webarticum.chm.Command;
import hu.webarticum.chm.History;



public class MainWindow {
    
    // XXX
    public static final String COMPONENT_ID_PROPERTY = "component.id";
    
    private volatile MainFrame frame = null;
    private volatile BoardViewImageProvider boardViewImageProvider = new BoardViewImageProvider();
    private volatile Rectangle boardViewHoverRectangle = null;
    private volatile Map<Time, DragTargetRectangleEntry> dragTargetRectangles = null;
    private volatile List<Rectangle> similarMarkedRectangles = null;
    
    // languages
    private Map<String, String> languageMap = new LinkedHashMap<String, String>();
    
    // state
    private volatile File associatedFile = null;
    private volatile Document document = null;
    private volatile boolean unsavedState = false;
    private volatile Command savedPositionCommand = null;
    
    // components
    private MultilingualMenu openRecentSubMenu = null;
    private MultilingualMenuItem undoMenuItem = null;
    private MultilingualMenuItem redoMenuItem = null;
    private JPanel tabItemSummary = null;
    private JLabel summaryDocumentLabel;
    private JLabel summaryPeriodCountLabel;
    private JLabel summaryTimingSetCountLabel;
    private JLabel summarySubjectCountLabel;
    private JLabel summaryLanguageCountLabel;
    private JLabel summaryOtherTagCountLabel;
    private JLabel summaryClassCountLabel;
    private JLabel summaryPersonCountLabel;
    private JLabel summaryLocaleCountLabel;
    private JLabel summaryObjectCountLabel;
    private JLabel summaryOtherResourceCountLabel;
    private JLabel summaryBlockCountLabel;
    private JLabel summaryTableCountLabel;
    private JPanel tabItemCatalog = null;
    private JPanel tabItemBlocks = null;
    private JPanel tabItemBoards = null;
    private BlockFilterPanel boardBlockFilterPanel;
    private JScrollPane boardViewImageScrollPane = null;
    private BoardViewImagePanel boardViewImagePanel = null;
    private MultilingualComboBox<ActivityFlowImageRenderer.ColorScheme> colorSchemeComboBox = null;
    
    // store lists
    @SuppressWarnings("unused")
    private StoreListPanel<Period> periodStoreListPanel = null;
    @SuppressWarnings("unused")
    private StoreListPanel<TimingSet> timingSetStoreListPanel = null;
    @SuppressWarnings("unused")
    private StoreListPanel<Tag> tagStoreListPanel = null;
    @SuppressWarnings("unused")
    private StoreListPanel<Resource> resourceStoreListPanel = null;
    private StoreListPanel<Block> blockStoreListPanel = null;
    @SuppressWarnings("unused")
    private StoreListPanel<Board> boardStoreListPanel = null;
    
    // miscellaneous
    private java.awt.Color defaultBoxColor = new java.awt.Color(0xDDE2CC);
    
    public MainWindow() {
        // FIXME
        languageMap.put("hu_HU", "Magyar");
        languageMap.put("en_US", "English");
    }

    public void run() {
        init();
        initNoDocument();
    }
    
    public void run(Document document) {
        init();
        initDocument(document);
    }

    public void run(File file) {
        init();
        if (loadDocumentFromFile(file)) {
            associatedFile = file;
        }
    }

    public void setSaved() {
        unsavedState = false;
        savedPositionCommand = history().getPrevious();
        refreshTitle();
    }

    public void setUnsaved() {
        unsavedState = true;
        refreshTitle();
    }

    public void refreshTitle() {
        String nameWithVersion = Info.NAME+" "+Info.MAJOR_VERSION+"."+Info.MINOR_VERSION + "." + Info.PATCH_VERSION;
        if (document==null) {
            frame.setTitle(nameWithVersion);
        } else {
            frame.setTitle((this.unsavedState?"*":"")+document.getLabel()+" | "+nameWithVersion);
        }
    }
    
    public void checkHistorySavedPosition() {
        if (savedPositionCommand != null && savedPositionCommand == history().getPrevious()) {
            unsavedState = false;
            refreshTitle();
        }
    }
    
    public void init() {
        frame = new MainFrame();
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            
            @Override
            public void windowClosing(WindowEvent ev) {
                tryExit();
            }
            
        });
        frame.setSize(900, 600);
        frame.setLocationByPlatform(true);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        
        frame.setIconImages(MainIconImageListProvider.getImageList());
        
        setLanguage(texts().getLanguage());
        
        GlobalUiObjects.setMainFrame(frame);
        
        setSaved();

        Runnable extraCalculationRunner = new Runnable() {

            @Override
            public void run() {
                GlobalUiObjects.getScreenInsets();
                GlobalUiObjects.getScreenSize();
            }
            
        };
        Thread extraCalculationThread = new Thread(extraCalculationRunner);
        extraCalculationThread.start();
    }

    public void setLanguage(String languageName) {
        if (!texts().getLanguage().equals(languageName)) {
            texts().setLanguage(languageName);
            hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualUtil.reloadComponent(frame);
        }
        
        Settings settings = Application.instance().getSettings();
        
        if (!settings.get("language").equals(languageName)) {
            settings.set("language", languageName);
        }
        
        String localeIdentifier = languageName.replace('_', '-');
        Locale newLocale = Locale.forLanguageTag(localeIdentifier);
        
        frame.setLocale(newLocale);
        Locale.setDefault(newLocale);
        JComponent.setDefaultLocale(newLocale);

        UIDefaults uiDefaults = UIManager.getDefaults();
        
        String[] uiProperties = new String[] {
            
            /* TODO / XXX
            "FileChooser.acceptAllFileFilterText",
            "FileChooser.byDateText",
            "FileChooser.byNameText",
            "FileChooser.cancelButtonText",
            "FileChooser.chooseButtonText",
            "FileChooser.createButtonText",
            "FileChooser.desktopName",
            "FileChooser.directoryDescriptionText",
            "FileChooser.directoryOpenButtonText",
            "FileChooser.fileDescriptionText",
            "FileChooser.fileNameLabelText",
            "FileChooser.filesOfTypeLabelText",
            "FileChooser.helpButtonText",
            "FileChooser.newFolderAccessibleName",
            "FileChooser.newFolderButtonText",
            "FileChooser.newFolderErrorSeparator",
            "FileChooser.newFolderErrorText",
            "FileChooser.newFolderExistsErrorText",
            "FileChooser.newFolderPromptText",
            "FileChooser.newFolderTitleText",
            "FileChooser.openButtonText",
            "FileChooser.openDialogTitleText",
            "FileChooser.openTitleText",
            "FileChooser.saveButtonText",
            "FileChooser.saveDialogFileNameLabelText",
            "FileChooser.saveDialogTitleText",
            "FileChooser.saveTitleText",
            "FileChooser.untitledFileName",
            "FileChooser.untitledFolderName",
            "FileChooser.updateButtonText",
            */
            
            "OptionPane.titleText",
            "OptionPane.yesButtonText",
            "OptionPane.noButtonText",
            "OptionPane.okButtonText",
            "OptionPane.cancelButtonText",
        };
        
        for (String propertyName : uiProperties) {
            uiDefaults.put(propertyName, text("java.swing." + propertyName));
        }
    }
    
    public JFrame getFrame() {
        return frame;
    }

    private void initNoDocument() {
        this.document = null;
        
        Application.instance().resetHistory();
        
        boardViewImageProvider.reset();
        
        JPanel contentPane = new JPanel(new BorderLayout());
        frame.setContentPane(contentPane);
        
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        panel.setBorder(new EmptyBorder(20, 20, 20, 20));
        contentPane.add(panel, BorderLayout.CENTER);
        
        final JPanel box = new JPanel();
        box.setLayout(new BoxLayout(box, BoxLayout.PAGE_AXIS));
        box.setBorder(BorderFactory.createCompoundBorder(new LineBorder(new java.awt.Color(70, 50, 30)), new EmptyBorder(10, 10, 10, 10)));
        box.setBackground(new java.awt.Color(255, 240, 180));
        box.setAlignmentX(0.5f);

        panel.add(Box.createHorizontalGlue());
        panel.add(box);
        panel.add(Box.createHorizontalGlue());
        
        final MultilingualLabel label = new MultilingualLabel("ui.swing.main.nodocument.intro");
        label.setSize(100, 30);
        box.add(label);

        panel.setDropTarget(new DropTarget(panel, new DropTargetListener() {
            
            java.awt.Color originalBackground = box.getBackground();

            java.awt.Color originalForeground = label.getForeground();

            Border originalBorder = box.getBorder();
            
            @Override
            public void dropActionChanged(DropTargetDragEvent ev) {
            }
            
            @Override
            @SuppressWarnings("unchecked")
            public void drop(DropTargetDropEvent ev) {
                ev.acceptDrop(DnDConstants.ACTION_COPY);
                List<File> droppedFiles = new ArrayList<File>();
                File usedFile = null;
                try {
                    droppedFiles = (List<File>)ev.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                } catch (Exception e) {
                }
                for (File droppedFile: droppedFiles) {
                    if (loadDocumentFromFile(droppedFile)) {
                        usedFile = droppedFile;
                        break;
                    }
                }
                if (usedFile==null) {
                    off();
                    OptionPaneUtil.showMessageDialog(frame, text("ui.swing.main.nodocument.drop_file.failed"));
                    return;
                }
                associatedFile = usedFile;
            }
            
            @Override
            public void dragOver(DropTargetDragEvent ev) {
            }
            
            @Override
            public void dragExit(DropTargetEvent ev) {
                off();
            }
            
            @Override
            @SuppressWarnings("unchecked")
            public void dragEnter(DropTargetDragEvent ev) {
                ev.acceptDrag(DnDConstants.ACTION_COPY);
                List<File> droppedFiles = new ArrayList<File>();
                try {
                    droppedFiles = (List<File>)ev.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                } catch (Exception e) {
                }
                if (!droppedFiles.isEmpty()) {
                    on();
                }
            }
            
            protected void on() {
                box.setBackground(new java.awt.Color(0x993300));
                label.setForeground(new java.awt.Color(0xFFFFFF));
                box.setBorder(new EmptyBorder(30, 30, 30, 30));
                label.setLanguagePath("ui.swing.main.nodocument.drop_file");
            }

            protected void off() {
                box.setBackground(originalBackground);
                label.setForeground(originalForeground);
                box.setBorder(originalBorder);
                label.setLanguagePath("ui.swing.main.nodocument.intro");
            }
            
        }));
        
        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        
        JMenu menu;
        
        menu = new MultilingualMenu("ui.swing.main.menu.file");
        menu.setMnemonic('F');
        menuBar.add(menu);

        List<Component> fileCommonMenu = buildFileCommonMenu();
        for (Component itemComponent : fileCommonMenu) {
            menu.add(itemComponent);
        }
        
        JMenuItem menuItem;

        menu.addSeparator();
        
        menuItem = new MultilingualMenuItem("ui.swing.main.menu.file.exit");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_Q, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tryExit();
            }
            
        });
        menu.add(menuItem);

        addCommonMenus(menuBar);

        
        setSaved();
        
        frame.setVisible(true);
        
        contentPane.validate();
        contentPane.repaint();
    }

    public void initDocument(final Document document) {
        this.document = document;

        Application.instance().resetHistory();
        
        boardViewImageProvider.reset();
        
        JPanel contentPane = new JPanel(new BorderLayout());
        frame.setContentPane(contentPane);

        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        JMenuItem menuItem;
        JMenu menu;

        menu = new MultilingualMenu("ui.swing.main.menu.file");
        menu.setMnemonic('F');
        menuBar.add(menu);

        List<Component> fileCommonMenu = buildFileCommonMenu();
        for (Component itemComponent : fileCommonMenu) {
            menu.add(itemComponent);
        }
        
        menu.addSeparator();

        menuItem = new MultilingualMenuItem("ui.swing.main.menu.file.save");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_S, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                trySave();
            }
            
        });
        menu.add(menuItem);

        menuItem = new MultilingualMenuItem("ui.swing.main.menu.file.save_as");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_S, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                trySaveAs();
            }
            
        });
        menu.add(menuItem);

        
        
        menu.addSeparator();
        
        menuItem = new MultilingualMenuItem("ui.swing.main.menu.file.leave");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_X, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                if (tryLeave()) {
                    associatedFile = null;
                    refreshRecentFilesMenuItems();
                }
            }
            
        });
        menu.add(menuItem);
        
        menu.addSeparator();

        menuItem = new MultilingualMenuItem("ui.swing.main.menu.file.exit");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_Q, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tryExit();
            }
            
        });
        menu.add(menuItem);
        

        menu = new MultilingualMenu("ui.swing.main.menu.edit");
        menuBar.add(menu);

        menuItem = new MultilingualMenuItem("ui.swing.main.menu.edit.document");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_D, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                DocumentEditDialog dialog = new DocumentEditDialog(frame, MainWindow.this.document);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    refreshSummaryDocumentLabel();
                    setUnsaved();
                }
            }
            
        });
        menu.add(menuItem);

        menuItem = new MultilingualMenuItem("ui.swing.main.menu.edit.organize_activities");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_M, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Block> blockWrapper1 = new GeneralWrapper<Block>();
                GeneralWrapper<Block> blockWrapper2 = new GeneralWrapper<Block>();
                OrganizerBlockSelectDialog selectDialog = new OrganizerBlockSelectDialog(frame, document, blockWrapper1, blockWrapper2);
                selectDialog.run();

                if (selectDialog.getResult() == EditDialog.RESULT_OK) {
                    Block block1 = blockWrapper1.get();
                    Block block2 = blockWrapper2.get();
                    
                    BlockActivityOrganizerDialog organizerDialog = new BlockActivityOrganizerDialog(frame, document, block1, block2);
                    organizerDialog.run();
                    
                    if (organizerDialog.getResult() == EditDialog.RESULT_OK) {
                        Document.BlockStore blockStore = document.getBlockStore();
                        if (block2 != null) {
                            blockStore.refresh(block1);
                            blockStore.refresh(block2);
                        } else {
                            blockStore.refresh();
                        }
                    }
                }
            }
            
        });
        menu.add(menuItem);
        
        menuItem = new MultilingualMenuItem("ui.swing.main.menu.edit.import");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_I, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tryImport();
            }
            
        });
        menu.add(menuItem);

        // XXX
        menuItem = new MultilingualMenuItem("ui.swing.main.menu.edit.schoolautoput_settings");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_E, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                SchoolAutoputSetupDialog dialog = new SchoolAutoputSetupDialog(frame, document);
                dialog.run();
                
                if (dialog.getResult() == EditDialog.RESULT_OK) {
                    setUnsaved();
                }
            }
            
        });
        menu.add(menuItem);

        menuItem = new MultilingualMenuItem("ui.swing.main.menu.edit.autoput");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_A, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tryAutoPut();
            }
            
        });
        menu.add(menuItem);

        menu.addSeparator();

        menuItem = undoMenuItem = new MultilingualMenuItem("ui.swing.main.menu.edit.undo");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_Z, ActionEvent.CTRL_MASK
        ));
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                history().rollBackPrevious();
                checkHistorySavedPosition();
            }
            
        });
        menu.add(menuItem);

        menuItem = redoMenuItem = new MultilingualMenuItem("ui.swing.main.menu.edit.redo");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_Y, ActionEvent.CTRL_MASK
        ));
        ActionListener redo = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                history().executeNext();
                checkHistorySavedPosition();
            }
            
        };
        menuItem.addActionListener(redo);
        menu.add(menuItem);
        contentPane.registerKeyboardAction(redo, KeyStroke.getKeyStroke(
            KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW
        );
        

        menu = new MultilingualMenu("ui.swing.main.menu.check");
        menuBar.add(menu);

        menuItem = new MultilingualMenuItem("ui.swing.main.menu.check.blocks");
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                CheckBlocksDialog dialog = new CheckBlocksDialog(frame, MainWindow.this.document);
                dialog.run();
            }
            
        });
        menu.add(menuItem);

        menuItem = new MultilingualMenuItem("ui.swing.main.menu.check.blocklist");
        menuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                CheckBlockListDialog dialog = new CheckBlockListDialog(frame, MainWindow.this.document);
                dialog.run();
            }
            
        });
        menu.add(menuItem);
        
        addCommonMenus(menuBar);
        refreshUndoRedoMenuItems();
        
        history().addListener(new History.Listener() {
            
            @Override
            public void changed(History history, OperationType operationType) {
                refreshUndoRedoMenuItems();
            }
        });
        
        
        JToolBar toolBar = new JToolBar("Toolbar", JToolBar.HORIZONTAL);
        toolBar.setFloatable(false);
        contentPane.add(toolBar, BorderLayout.PAGE_START);
        
        JButton button;

        button = new MultilingualButton(IconLoader.loadIcon("undo"), "ui.swing.main.toolbar.undo");
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                history().rollBackPrevious();
                checkHistorySavedPosition();
            }
            
        });
        toolBar.add(button);

        button = new MultilingualButton(IconLoader.loadIcon("redo"), "ui.swing.main.toolbar.redo");
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                history().executeNext();
                checkHistorySavedPosition();
            }
            
        });
        toolBar.add(button);

        button = new MultilingualButton(IconLoader.loadIcon("save"), "ui.swing.main.toolbar.save");
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                trySave();
            }
            
        });
        toolBar.add(button);

        button = new MultilingualButton(IconLoader.loadIcon("open"), "ui.swing.main.toolbar.open");
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                tryOpen();
            }
            
        });
        toolBar.add(button);
        
        button = new MultilingualButton(IconLoader.loadIcon("new"), "ui.swing.main.toolbar.new");
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                tryNew();
            }
            
        });
        toolBar.add(button);

        toolBar.addSeparator();

        button = new MultilingualButton(IconLoader.loadIcon("check_blocks"), "ui.swing.main.toolbar.check_blocks");
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                CheckBlocksDialog dialog = new CheckBlocksDialog(frame, MainWindow.this.document);
                dialog.run();
            }
            
        });
        toolBar.add(button);

        button = new MultilingualButton(IconLoader.loadIcon("check_blocklist"), "ui.swing.main.toolbar.check_blocklist");
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                CheckBlockListDialog dialog = new CheckBlockListDialog(frame, MainWindow.this.document);
                dialog.run();
            }
            
        });
        toolBar.add(button);
        
        MultilingualTabbedPane tabbedPane = new MultilingualTabbedPane();
        contentPane.add(tabbedPane, BorderLayout.CENTER);

        tabItemSummary = new JPanel();
        tabbedPane.addMultilingualTab("ui.swing.main.tabbar.summary", tabItemSummary);
        initSummaryTab();

        tabItemCatalog = new JPanel();
        tabbedPane.addMultilingualTab("ui.swing.main.tabbar.catalog", tabItemCatalog);
        
        tabItemBlocks = new JPanel();
        tabbedPane.addMultilingualTab("ui.swing.main.tabbar.blocks", tabItemBlocks);
        
        tabItemBoards = new JPanel();
        tabbedPane.addMultilingualTab("ui.swing.main.tabbar.tables", tabItemBoards);
        
        setSaved();
        
        SwingUtilities.invokeLater(new Runnable() {
            
            @Override
            public void run() {
                initBlocksTab();
                initCatalogTab();
                initTablesTab();
            }
            
        });
        
        listen("document-edited", document, new Listener() {
            
            @Override
            public void invoke(hu.webarticum.aurora.app.event.Event event) {
                refreshSummaryDocumentLabel();
                setUnsaved();
            }
            
        });

        document.getPeriodStore().addStoreListener(new Store.StoreListener<Period>(){

            @Override
            public void registered(Store.StoreListener.RegisterEvent<Period> registerEvent) {
                refreshSummaryStats();
                setUnsaved();
            }

            @Override
            public void removed(Store.StoreListener.RemoveEvent<Period> removeEvent) {
                refreshSummaryStats();
                setUnsaved();
            }

            @Override
            public void itemRefreshed(Store.StoreListener.ItemRefreshEvent<Period> itemRefreshEvent) {
                setUnsaved();
            }

            @Override
            public void storeRefreshed(Store.StoreListener.StoreRefreshEvent<Period> storeRefreshEvent) {
                setUnsaved();
            }
            
        });

        document.getTimingSetStore().addStoreListener(new Store.StoreListener<TimingSet>(){

            @Override
            public void registered(Store.StoreListener.RegisterEvent<TimingSet> registerEvent) {
                refreshSummaryStats();
                setUnsaved();
            }

            @Override
            public void removed(Store.StoreListener.RemoveEvent<TimingSet> removeEvent) {
                refreshSummaryStats();
                setUnsaved();
            }

            @Override
            public void itemRefreshed(Store.StoreListener.ItemRefreshEvent<TimingSet> itemRefreshEvent) {
                setUnsaved();
            }

            @Override
            public void storeRefreshed(Store.StoreListener.StoreRefreshEvent<TimingSet> storeRefreshEvent) {
                setUnsaved();
            }
            
        });
        
        document.getTagStore().addStoreListener(new Store.StoreListener<Tag>(){

            @Override
            public void registered(Store.StoreListener.RegisterEvent<Tag> registerEvent) {
                refreshSummaryStats();
                setUnsaved();
            }

            @Override
            public void removed(Store.StoreListener.RemoveEvent<Tag> removeEvent) {
                refreshSummaryStats();
                refreshBlockListPanelAsynchronous();
                refreshTableViewAsynchronous();
                setUnsaved();
            }

            @Override
            public void itemRefreshed(Store.StoreListener.ItemRefreshEvent<Tag> itemRefreshEvent) {
                blockStoreListPanel.refresh(false);
                refreshTableViewAsynchronous();
                setUnsaved();
            }

            @Override
            public void storeRefreshed(Store.StoreListener.StoreRefreshEvent<Tag> storeRefreshEvent) {
                blockStoreListPanel.refresh(false);
                refreshTableViewAsynchronous();
                setUnsaved();
            }
            
        });

        document.getResourceStore().addStoreListener(new Store.StoreListener<Resource>(){

            @Override
            public void registered(Store.StoreListener.RegisterEvent<Resource> registerEvent) {
                refreshSummaryStats();
                setUnsaved();
            }

            @Override
            public void removed(Store.StoreListener.RemoveEvent<Resource> removeEvent) {
                refreshSummaryStats();
                refreshBlockListPanelAsynchronous();
                refreshTableViewAsynchronous();
                setUnsaved();
            }

            @Override
            public void itemRefreshed(Store.StoreListener.ItemRefreshEvent<Resource> itemRefreshEvent) {
                blockStoreListPanel.refresh(false);
                refreshTableViewAsynchronous();
                setUnsaved();
            }

            @Override
            public void storeRefreshed(Store.StoreListener.StoreRefreshEvent<Resource> storeRefreshEvent) {
                blockStoreListPanel.refresh(false);
                refreshTableViewAsynchronous();
                setUnsaved();
            }
            
        });

        document.getBlockStore().addStoreListener(new Store.StoreListener<Block>(){

            @Override
            public void registered(Store.StoreListener.RegisterEvent<Block> registerEvent) {
                refreshSummaryStats();
                setUnsaved();
            }

            @Override
            public void removed(Store.StoreListener.RemoveEvent<Block> removeEvent) {
                refreshSummaryStats();
                refreshTableViewAsynchronous();
                setUnsaved();
            }

            @Override
            public void itemRefreshed(Store.StoreListener.ItemRefreshEvent<Block> itemRefreshEvent) {
                refreshTableViewAsynchronous();
                setUnsaved();
            }

            @Override
            public void storeRefreshed(Store.StoreListener.StoreRefreshEvent<Block> storeRefreshEvent) {
                refreshTableViewAsynchronous();
                setUnsaved();
            }
            
        });

        document.getBoardStore().addStoreListener(new Store.StoreListener<Board>(){

            @Override
            public void registered(Store.StoreListener.RegisterEvent<Board> registerEvent) {
                refreshSummaryStats();
                setUnsaved();
            }

            @Override
            public void removed(Store.StoreListener.RemoveEvent<Board> removeEvent) {
                refreshSummaryStats();
                if (removeEvent.getItem() == boardViewImageProvider.getBoard()) {
                    boardViewImageProvider.setBoard(null);
                    refreshTableViewAsynchronous();
                }
                setUnsaved();
            }

            @Override
            public void itemRefreshed(Store.StoreListener.ItemRefreshEvent<Board> itemRefreshEvent) {
                if (itemRefreshEvent.getItem() == boardViewImageProvider.getBoard()) {
                    refreshTableViewAsynchronous();
                }
                setUnsaved();
            }

            @Override
            public void storeRefreshed(Store.StoreListener.StoreRefreshEvent<Board> storeRefreshEvent) {
                refreshTableViewAsynchronous();
                setUnsaved();
            }
            
        });
        
        frame.setVisible(true);
    }
    
    private List<Component> buildFileCommonMenu() {
        List<Component> fileCommonMenuItems = new ArrayList<Component>();
        
        JMenuItem newMenuItem = new MultilingualMenuItem("ui.swing.main.menu.file.new");
        newMenuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_N, ActionEvent.CTRL_MASK
        ));
        newMenuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tryNew();
            }
            
        });
        fileCommonMenuItems.add(newMenuItem);

        JMenuItem openMenuItem = new MultilingualMenuItem("ui.swing.main.menu.file.open");
        openMenuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_O, ActionEvent.CTRL_MASK
        ));
        openMenuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tryOpen();
            }
            
        });
        fileCommonMenuItems.add(openMenuItem);
        
        openRecentSubMenu = new MultilingualMenu("ui.swing.main.menu.file.open_recent");
        fileCommonMenuItems.add(openRecentSubMenu);
        refreshRecentFilesMenuItems();

        JMenuItem openDemoMenuItem = new MultilingualMenuItem("ui.swing.main.menu.file.open_demo");
        openDemoMenuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_D, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK
        ));
        openDemoMenuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tryOpenDemo();
            }
            
        });
        fileCommonMenuItems.add(openDemoMenuItem);

        JMenuItem importMenuItem = new JMenuItem("IMPORT"); // TODO
        importMenuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_I, ActionEvent.CTRL_MASK
        ));
        importMenuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                ImportDialog importDialog = new ImportDialog(frame, document, MainWindow.this::handleUnsavedDocument, MainWindow.this::initDocument);
                importDialog.run();
            }
            
        });
        fileCommonMenuItems.add(importMenuItem);
        
        return fileCommonMenuItems;
    }

    private void refreshRecentFilesMenuItems() {
        refreshRecentFilesMenuItems(associatedFile != null ? associatedFile.getAbsolutePath() : "");
    }
    
    private void refreshRecentFilesMenuItems(String currentPath) {
        RecentFilesManager recentFilesManager = Application.instance().getRecentFilesManager();
        int capacity = recentFilesManager.getCapacity();
        List<String> fullPathList = recentFilesManager.getPathList();
        List<String> pathList = new ArrayList<String>();
        int pathCount = 0;
        for (String path : fullPathList) {
            if (!path.equals(currentPath)) {
                pathList.add(path);
                pathCount++;
            }
            if (pathCount == capacity) {
                break;
            }
        }
        openRecentSubMenu.removeAll();
        for (String path : pathList) {
            int pathLength = path.length();
            String displayedPath = path;
            if (pathLength > 40) {
                displayedPath = "\u2026" + displayedPath.substring(pathLength - 39);
            }
            JMenuItem recentPathItem = new JMenuItem(displayedPath);
            recentPathItem.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    File file = new File(path);
                    if (loadDocumentFromFile(file)) {
                        associatedFile = file;
                    }
                }
                
            });
            openRecentSubMenu.add(recentPathItem);
        }
        if (pathCount == 0) {
            JMenuItem noneItem = new MultilingualMenuItem("ui.swing.main.menu.file.open_recent.none");
            noneItem.setEnabled(false);
            openRecentSubMenu.add(noneItem);
        }
    }
    
    private void addCommonMenus(JMenuBar menuBar) {
        JMenu settingsMenu = new MultilingualMenu("ui.swing.main.menu.settings");
        settingsMenu.setMnemonic('S');
        menuBar.add(settingsMenu);
        
        {
            JMenu subMenu = new MultilingualMenu("ui.swing.main.menu.settings.language");
            settingsMenu.add(subMenu);
            
            for (Map.Entry<String, String> entry: languageMap.entrySet()) {
                final String languageName = entry.getKey();
                final String languageLabel = entry.getValue();
                JMenuItem languageMenuItem = new JMenuItem(languageLabel);
                languageMenuItem.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        setLanguage(languageName);
                    }
                    
                });
                subMenu.add(languageMenuItem);
            }
        }

        JMenuItem settingsMenuItem = new MultilingualMenuItem("ui.swing.main.menu.settings.settings");
        settingsMenuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                SettingsDialog settingsDialog = new SettingsDialog(frame);
                settingsDialog.run();
            }
            
        });
        settingsMenu.add(settingsMenuItem);

        
        JMenu infoMenu = new MultilingualMenu("ui.swing.main.menu.info");
        infoMenu.setMnemonic('I');
        menuBar.add(infoMenu);
        
        /*
        JMenuItem helpMenuItem = new MultilingualMenuItem("ui.swing.main.menu.info.help");
        helpMenuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                HelpWindow helpWindow = new HelpWindow();
                helpWindow.run();
            }
            
        });
        infoMenu.add(helpMenuItem);

        infoMenu.addSeparator();
        //*/
        
        JMenuItem aboutMenuItem = new MultilingualMenuItem("ui.swing.main.menu.info.about");
        aboutMenuItem.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                AboutDialog dialog = new AboutDialog(frame);
                dialog.run();
            }
            
        });
        infoMenu.add(aboutMenuItem);

    }
    
    private void refreshUndoRedoMenuItems() {
        if (history().hasPrevious()) {
            undoMenuItem.setLanguagePath(new PathMultilingualContent("ui.swing.main.menu.edit.undo_command", history().getPrevious()));
            undoMenuItem.setEnabled(true);
        } else {
            undoMenuItem.setLanguagePath("ui.swing.main.menu.edit.undo");
            undoMenuItem.setEnabled(false);
        }

        if (history().hasNext()) {
            redoMenuItem.setLanguagePath(new PathMultilingualContent("ui.swing.main.menu.edit.redo_command", history().getNext()));
            redoMenuItem.setEnabled(true);
        } else {
            redoMenuItem.setLanguagePath("ui.swing.main.menu.edit.redo");
            redoMenuItem.setEnabled(false);
        }
    }
    
    private void initSummaryTab() {
        tabItemSummary.setLayout(new BorderLayout());
        
        JPanel summaryContainerPanel = new JPanel(){
            
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                int width = getWidth();
                int height = getHeight();
                if (width > 40 && height > 40) {
                    Graphics2D g2 = (Graphics2D)g;
                    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    g2.setColor(java.awt.Color.WHITE);
                    g2.fillRoundRect(20, 20, width - 40, height - 40, 20, 20);
                    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    g2.setColor(new java.awt.Color(150, 150, 150));
                    g2.drawRoundRect(20, 20, width - 40, height - 40, 20, 20);
                }
            }
            
        };
        summaryContainerPanel.setLayout(new BorderLayout());
        tabItemSummary.add(summaryContainerPanel, BorderLayout.CENTER);

        
        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.PAGE_AXIS));
        innerPanel.setBackground(java.awt.Color.WHITE);
        
        JScrollPane innerScrollPane = new JScrollPane(innerPanel);
        innerScrollPane.setBorder(new EmptyBorder(35, 35, 35, 35));
        innerScrollPane.setFocusable(true);
        summaryContainerPanel.add(innerScrollPane, BorderLayout.CENTER);
        
        summaryDocumentLabel = new JLabel();
        summaryDocumentLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        summaryDocumentLabel.setForeground(new java.awt.Color(0x6CA0F7));
        summaryDocumentLabel.setAlignmentX(0);
        innerPanel.add(summaryDocumentLabel);
        
        JPanel leftRightPanel = new JPanel(new GridLayout(1, 2));
        leftRightPanel.setAlignmentX(0);
        leftRightPanel.setBackground(java.awt.Color.WHITE);
        leftRightPanel.setBorder(new EmptyBorder(20, 0, 0, 0));
        innerPanel.add(leftRightPanel);
        
        JPanel leftOuterPanel = new JPanel(new BorderLayout());
        leftOuterPanel.setBackground(java.awt.Color.WHITE);
        leftRightPanel.add(leftOuterPanel);
        
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS));
        leftPanel.setBackground(java.awt.Color.WHITE);
        leftOuterPanel.add(leftPanel, BorderLayout.PAGE_START);
        
        summaryPeriodCountLabel = new JLabel();
        leftPanel.add(createSummaryRow("cover_periods", summaryPeriodCountLabel, "ui.swing.main.summary.periods"));

        summaryTimingSetCountLabel = new JLabel();
        leftPanel.add(createSummaryRow("cover_timingsets", summaryTimingSetCountLabel, "ui.swing.main.summary.timingsets"));

        summarySubjectCountLabel = new JLabel();
        leftPanel.add(createSummaryRow("cover_subjects", summarySubjectCountLabel, "ui.swing.main.summary.subjects"));

        summaryLanguageCountLabel = new JLabel();
        leftPanel.add(createSummaryRow("cover_languages", summaryLanguageCountLabel, "ui.swing.main.summary.languages"));

        summaryOtherTagCountLabel = new JLabel();
        leftPanel.add(createSummaryRow("cover_othertags", summaryOtherTagCountLabel, "ui.swing.main.summary.othertags"));

        JPanel rightOuterPanel = new JPanel(new BorderLayout());
        rightOuterPanel.setBackground(java.awt.Color.WHITE);
        leftRightPanel.add(rightOuterPanel);
        
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
        rightPanel.setBackground(java.awt.Color.WHITE);
        rightOuterPanel.add(rightPanel, BorderLayout.PAGE_START);
        
        summaryClassCountLabel = new JLabel();
        rightPanel.add(createSummaryRow("cover_classes", summaryClassCountLabel, "ui.swing.main.summary.classes"));

        summaryPersonCountLabel = new JLabel();
        rightPanel.add(createSummaryRow("cover_persons", summaryPersonCountLabel, "ui.swing.main.summary.persons"));

        summaryLocaleCountLabel = new JLabel();
        rightPanel.add(createSummaryRow("cover_locales", summaryLocaleCountLabel, "ui.swing.main.summary.locales"));

        summaryObjectCountLabel = new JLabel();
        rightPanel.add(createSummaryRow("cover_objects", summaryObjectCountLabel, "ui.swing.main.summary.objects"));

        summaryOtherResourceCountLabel = new JLabel();
        rightPanel.add(createSummaryRow("cover_otherresources", summaryOtherResourceCountLabel, "ui.swing.main.summary.otherresources"));

        summaryBlockCountLabel = new JLabel();
        rightPanel.add(createSummaryRow("cover_blocks", summaryBlockCountLabel, "ui.swing.main.summary.blocks"));

        summaryTableCountLabel = new JLabel("?");
        rightPanel.add(createSummaryRow("cover_boards", summaryTableCountLabel, "ui.swing.main.summary.timetables"));
        
        refreshSummaryDocumentLabel();
        refreshSummaryStats();
    }
    
    private JPanel createSummaryRow(String iconName, JLabel quantityLabel, String languagePath) {
        JPanel rowPanel = new JPanel();
        rowPanel.setOpaque(false);
        rowPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        rowPanel.setAlignmentX(0);
        rowPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        JLabel iconLabel = new JLabel(IconLoader.loadIcon(iconName));
        rowPanel.add(iconLabel);
        JLabel multiplyLabel = new JLabel("×");
        multiplyLabel.setBorder(new EmptyBorder(0, 10, 0, 10));
        multiplyLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        rowPanel.add(multiplyLabel);
        quantityLabel.setText("?");
        quantityLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        quantityLabel.setAlignmentY(0.5f);
        SizingPanel quantityLabelPanel = new SizingPanel(quantityLabel, FlowLayout.CENTER);
        quantityLabelPanel.setMinimumSize(new Dimension(30, 1));
        quantityLabelPanel.setOpaque(false);
        rowPanel.add(quantityLabelPanel);
        JLabel typeLabel = new MultilingualLabel(languagePath);
        typeLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
        typeLabel.setBorder(new EmptyBorder(0, 10, 0, 0));
        typeLabel.setForeground(new java.awt.Color(0x999999));
        rowPanel.add(typeLabel);
        return rowPanel;
    }

    private void initCatalogTab() {
        tabItemCatalog.setLayout(new BorderLayout());
        
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.LINE_AXIS));
        tabItemCatalog.add(leftPanel, BorderLayout.LINE_START);
        
        JPanel periodsPanel = createPeriodsPanel();
        {
            @SuppressWarnings("unchecked")
            StoreListPanel<Period> _periodStoreListPanel = (StoreListPanel<Period>)ComponentTreeUtil.findByProperty(
                periodsPanel, COMPONENT_ID_PROPERTY, "period-store-list-panel"
            );
            periodStoreListPanel = _periodStoreListPanel;
        }
        leftPanel.add(periodsPanel);

        JPanel timingSetsPanel = createTimingSetsPanel();
        {
            @SuppressWarnings("unchecked")
            StoreListPanel<TimingSet> _timingSetStoreListPanel = (StoreListPanel<TimingSet>)ComponentTreeUtil.findByProperty(
                timingSetsPanel, COMPONENT_ID_PROPERTY, "timingset-store-list-panel"
            );
            timingSetStoreListPanel = _timingSetStoreListPanel;
        }
        leftPanel.add(timingSetsPanel);

        JPanel tagsPanel = createTagsPanel();
        {
            @SuppressWarnings("unchecked")
            StoreListPanel<Tag> _tagStoreListPanel = (StoreListPanel<Tag>)ComponentTreeUtil.findByProperty(
                tagsPanel, COMPONENT_ID_PROPERTY, "tag-store-list-panel"
            );
            tagStoreListPanel = _tagStoreListPanel;
        }
        leftPanel.add(tagsPanel);

        JPanel resourcesPanel = createResourcesPanel();
        {
            @SuppressWarnings("unchecked")
            StoreListPanel<Resource> _resourceStoreListPanel = (StoreListPanel<Resource>)ComponentTreeUtil.findByProperty(
                resourcesPanel, COMPONENT_ID_PROPERTY, "resource-store-list-panel"
            );
            resourceStoreListPanel = _resourceStoreListPanel;
        }
        tabItemCatalog.add(resourcesPanel, BorderLayout.CENTER);
    }

    private JPanel createPeriodsPanel() {
        JPanel periodListPanel = new JPanel();
        periodListPanel.setLayout(new BorderLayout());
        periodListPanel.setPreferredSize(new Dimension(220, 30));
        periodListPanel.setBackground(java.awt.Color.WHITE);
        periodListPanel.setBorder(new LineBorder(java.awt.Color.GRAY));

        JPanel periodListHeadOuterPanel = new JPanel();
        periodListHeadOuterPanel.setLayout(new BoxLayout(periodListHeadOuterPanel, BoxLayout.PAGE_AXIS));
        periodListPanel.add(periodListHeadOuterPanel, BorderLayout.PAGE_START);
        
        JPanel periodListHeadPanel = new JPanel();
        periodListHeadPanel.setLayout(new BorderLayout());
        periodListHeadPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        periodListHeadOuterPanel.add(periodListHeadPanel, BorderLayout.PAGE_START);

        periodListHeadPanel.add(new MultilingualLabel("ui.swing.main.catalog.periods"), BorderLayout.LINE_START);

        MultilingualButton addButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.main.catalog.add_period");
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Period period = new Period();
                PeriodEditDialog dialog = new PeriodEditDialog(frame, document, period);
                dialog.run();
            }
            
        });
        periodListHeadPanel.add(addButton, BorderLayout.LINE_END);
        
        VerticallyScrollablePanel periodListContainerOuter = new VerticallyScrollablePanel();
        periodListContainerOuter.setBackground(java.awt.Color.WHITE);
        periodListContainerOuter.setLayout(new BorderLayout());
        JScrollPane tagListScrollPane = new JScrollPane(periodListContainerOuter);
        tagListScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        periodListPanel.add(tagListScrollPane, BorderLayout.CENTER);
        
        periodListContainerOuter.setBackground(java.awt.Color.WHITE);
        
        final StoreListPanel<Period> periodStoreListPanel = new StoreListPanel<Period>(document.getPeriodStore(), new StoreListPanel.ComponentProvider<Period>() {

            @Override
            public Component buildComponentFor(final Period period, StoreListPanel<Period> periodStoreListPanel) {
                JPanel container = createItemContainer();
                JPanel box = (JPanel)ComponentTreeUtil.findByProperty(container, COMPONENT_ID_PROPERTY, "box");
                JPanel buttonBar = (JPanel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "button-bar");
                
                JButton editButton = new MultilingualButton(IconLoader.loadIcon("edit"), "ui.swing.main.catalog.edit_period");
                editButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        PeriodEditDialog dialog = new PeriodEditDialog(frame, document, period);
                        dialog.run();
                    }
                    
                });
                buttonBar.add(editButton);
                
                JButton deleteButton = new MultilingualButton(IconLoader.loadIcon("trash"), "ui.swing.main.catalog.delete_period");
                deleteButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // TODO: convert to showConfirmDialog
                        int answer = OptionPaneUtil.showOptionDialog(
                            frame,
                            text("ui.swing.main.catalog.delete_period.sure"),
                            text("ui.swing.main.catalog.delete_period.confirm"),
                            JOptionPane.QUESTION_MESSAGE,
                            new String[]{
                                text("ui.swing.main.catalog.delete_period.cancel"),
                                text("ui.swing.main.catalog.delete_period.delete"),
                            },
                            new ButtonPurpose[] {
                                ButtonPurpose.NONE,
                                ButtonPurpose.DANGER,
                            },
                            text("ui.swing.main.catalog.delete_period.delete")
                        );
                        if (answer==1) {
                            RemovalHelper removalHelper = new RemovalHelper(document);
                            Command command = removalHelper.createRemoveCommandFor(
                                period,
                                "ui.swing.main.command.delete_period"
                            );
                            history().addAndExecute(command);
                        }
                    }
                    
                });
                buttonBar.add(deleteButton);
                
                return container;
            }

            @Override
            public void refreshComponentFor(Component component, Period period, StoreListPanel<Period> periodStoreListPanel) {
                JPanel box = (JPanel)ComponentTreeUtil.findByProperty(component, COMPONENT_ID_PROPERTY, "box");
                JLabel label = (JLabel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "item-label");
               
                label.setText(period.getLabel());
            }
            
        });
        
        periodStoreListPanel.putClientProperty(COMPONENT_ID_PROPERTY, "period-store-list-panel");
        periodStoreListPanel.setLayout(new BoxLayout(periodStoreListPanel, BoxLayout.PAGE_AXIS));
        periodStoreListPanel.setBackground(java.awt.Color.WHITE);
        periodListContainerOuter.add(periodStoreListPanel, BorderLayout.PAGE_START);
        
        return periodListPanel;
    }

    private JPanel createTimingSetsPanel() {
        JPanel timingSetListPanel = new JPanel();
        timingSetListPanel.setLayout(new BorderLayout());
        timingSetListPanel.setPreferredSize(new Dimension(220, 30));
        timingSetListPanel.setBackground(java.awt.Color.WHITE);
        timingSetListPanel.setBorder(new LineBorder(java.awt.Color.GRAY));

        JPanel timingSetListHeadOuterPanel = new JPanel();
        timingSetListHeadOuterPanel.setLayout(new BoxLayout(timingSetListHeadOuterPanel, BoxLayout.PAGE_AXIS));
        timingSetListPanel.add(timingSetListHeadOuterPanel, BorderLayout.PAGE_START);
        
        JPanel timingSetListHeadPanel = new JPanel();
        timingSetListHeadPanel.setLayout(new BorderLayout());
        timingSetListHeadPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        timingSetListHeadOuterPanel.add(timingSetListHeadPanel, BorderLayout.PAGE_START);

        timingSetListHeadPanel.add(new MultilingualLabel("ui.swing.main.catalog.timingsets"), BorderLayout.LINE_START);

        MultilingualButton addButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.main.catalog.add_timingset");
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                TimingSet timingSet = new TimingSet();
                TimingSetEditDialog dialog = new TimingSetEditDialog(frame, document, timingSet);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    document.getTimingSetStore().register(timingSet);
                }
            }
            
        });
        timingSetListHeadPanel.add(addButton, BorderLayout.LINE_END);
        
        VerticallyScrollablePanel timingSetListContainerOuter = new VerticallyScrollablePanel();
        timingSetListContainerOuter.setBackground(java.awt.Color.WHITE);
        timingSetListContainerOuter.setLayout(new BorderLayout());
        JScrollPane tagListScrollPane = new JScrollPane(timingSetListContainerOuter);
        tagListScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        timingSetListPanel.add(tagListScrollPane, BorderLayout.CENTER);
        
        timingSetListContainerOuter.setBackground(java.awt.Color.WHITE);
        
        final StoreListPanel<TimingSet> timingSetStoreListPanel = new StoreListPanel<TimingSet>(
            document.getTimingSetStore(),
            new StoreListPanel.ComponentProvider<TimingSet>() {
    
                @Override
                public Component buildComponentFor(final TimingSet timingSet, StoreListPanel<TimingSet> timingSetStoreListPanel) {
                    JPanel container = createItemContainer();
                    JPanel box = (JPanel)ComponentTreeUtil.findByProperty(container, COMPONENT_ID_PROPERTY, "box");
                    JPanel buttonBar = (JPanel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "button-bar");
                    
                    JButton editButton = new MultilingualButton(IconLoader.loadIcon("edit"), "ui.swing.main.catalog.edit_timingset");
                    editButton.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            TimingSetEditDialog dialog = new TimingSetEditDialog(frame, document, timingSet);
                            dialog.run();
                        }
                        
                    });
                    buttonBar.add(editButton);
    
                    JButton duplicateButton = new MultilingualButton(IconLoader.loadIcon("duplicate"), "ui.swing.main.catalog.duplicate_timingset");
                    duplicateButton.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            String newTimingSetLabel = OptionPaneUtil.showInputDialog(
                                frame,
                                text("ui.swing.main.catalog.duplicate_timingset.name_of_new"),
                                String.format(text("ui.swing.main.catalog.duplicate_timingset.copy_of"), timingSet.getLabel()),
                                ButtonPurpose.SAVE
                            );
                            if (newTimingSetLabel == null || newTimingSetLabel.length() == 0) {
                                return;
                            }
                            history().addAndExecute(new DuplicateCommand(newTimingSetLabel));
                        }
    
                        class DuplicateCommand extends AbstractCommand {
                            
                            final String newLabel;
                            
                            TimingSet newTimingSet = null;
                            
                            DuplicateCommand(String newLabel) {
                                this.newLabel = newLabel;
                            }
                            
                            @Override
                            protected boolean _execute() {
                                if (newTimingSet == null) {
                                    newTimingSet = new TimingSet(timingSet);
                                    newTimingSet.setLabel(newLabel);
                                }
                                document.getTimingSetStore().register(newTimingSet);
                                return true;
                            }
    
                            @Override
                            protected boolean _rollBack() {
                                document.getTimingSetStore().remove(newTimingSet);
                                return true;
                            }
    
                            @Override
                            public String toString() {
                                return text("ui.swing.main.command.duplicate_timingset");
                            }
                            
                        }
                        
                    });
                    buttonBar.add(duplicateButton);
                    
                    JButton deleteButton = new MultilingualButton(IconLoader.loadIcon("trash"), "ui.swing.main.catalog.delete_timingset");
                    deleteButton.addActionListener(new ActionListener() {
    
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            // TODO: convert to showConfirmDialog
                            int answer = OptionPaneUtil.showOptionDialog(
                                frame,
                                text("ui.swing.main.catalog.delete_timingset.sure"),
                                text("ui.swing.main.catalog.delete_timingset.confirm"),
                                JOptionPane.QUESTION_MESSAGE,
                                new String[]{
                                    text("ui.swing.main.catalog.delete_timingset.cancel"),
                                    text("ui.swing.main.catalog.delete_timingset.delete"),
                                },
                                new ButtonPurpose[] {
                                    ButtonPurpose.NONE,
                                    ButtonPurpose.DANGER,
                                },
                                text("ui.swing.main.catalog.delete_timingset.delete")
                            );
                            if (answer==1) {
                                RemovalHelper removalHelper = new RemovalHelper(document);
                                Command command = removalHelper.createRemoveCommandFor(
                                    timingSet,
                                    "ui.swing.main.command.delete_timingset"
                                );
                                history().addAndExecute(command);
                            }
                        }
                        
                    });
                    buttonBar.add(deleteButton);
                    
                    return container;
                }
    
                @Override
                public void refreshComponentFor(Component component, TimingSet timingSet, StoreListPanel<TimingSet> timingSetStoreListPanel) {
                    JPanel box = (JPanel)ComponentTreeUtil.findByProperty(component, COMPONENT_ID_PROPERTY, "box");
                    JLabel label = (JLabel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "item-label");
                   
                    label.setText(timingSet.getLabel());
                }
                
            }
        );

        timingSetStoreListPanel.putClientProperty(COMPONENT_ID_PROPERTY, "timingset-store-list-panel");
        timingSetStoreListPanel.setLayout(new BoxLayout(timingSetStoreListPanel, BoxLayout.PAGE_AXIS));
        timingSetStoreListPanel.setBackground(java.awt.Color.WHITE);
        timingSetListContainerOuter.add(timingSetStoreListPanel, BorderLayout.PAGE_START);
        
        return timingSetListPanel;
    }

    private JPanel createTagsPanel() {
        JPanel tagListPanel = new JPanel();
        tagListPanel.setLayout(new BorderLayout());
        tagListPanel.setPreferredSize(new Dimension(220, 30));
        tagListPanel.setBackground(java.awt.Color.WHITE);
        tagListPanel.setBorder(new LineBorder(java.awt.Color.GRAY));

        JPanel tagListHeadOuterPanel = new JPanel();
        tagListHeadOuterPanel.setLayout(new BoxLayout(tagListHeadOuterPanel, BoxLayout.PAGE_AXIS));
        tagListPanel.add(tagListHeadOuterPanel, BorderLayout.PAGE_START);
        
        JPanel tagListHeadPanel = new JPanel();
        tagListHeadPanel.setLayout(new BorderLayout());
        tagListHeadPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        tagListHeadOuterPanel.add(tagListHeadPanel, BorderLayout.PAGE_START);

        tagListHeadPanel.add(new MultilingualLabel("ui.swing.main.catalog.tags"), BorderLayout.LINE_START);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        tagListHeadPanel.add(buttonPanel, BorderLayout.LINE_END);

        TagTypeFilterToolBar tagTypeFilterToolBar = new TagTypeFilterToolBar();
        buttonPanel.add(tagTypeFilterToolBar);
        
        MultilingualButton addButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.main.catalog.add_tag");
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Tag.Type type = tagTypeFilterToolBar.getTagType();
                Tag tag = new Tag(type);
                TagEditDialog dialog = new TagEditDialog(frame, document, tag);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    document.getTagStore().register(tag);
                }
            }
            
        });
        buttonPanel.add(addButton);

        VerticallyScrollablePanel tagListContainerOuter = new VerticallyScrollablePanel();
        tagListContainerOuter.setBackground(java.awt.Color.WHITE);
        tagListContainerOuter.setLayout(new BorderLayout());
        JScrollPane tagListScrollPane = new JScrollPane(tagListContainerOuter);
        tagListScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        tagListPanel.add(tagListScrollPane, BorderLayout.CENTER);
        
        tagListContainerOuter.setBackground(java.awt.Color.WHITE);
        
        final StoreListPanel<Tag> tagStoreListPanel = new StoreListPanel<Tag>(document.getTagStore(), new StoreListPanel.ComponentProvider<Tag>() {

            @Override
            public Component buildComponentFor(final Tag tag, StoreListPanel<Tag> tagStoreListPanel) {
                JPanel container = createItemContainer();
                JPanel box = (JPanel)ComponentTreeUtil.findByProperty(container, COMPONENT_ID_PROPERTY, "box");
                JPanel buttonBar = (JPanel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "button-bar");
                
                JButton editButton = new MultilingualButton(IconLoader.loadIcon("edit"), "ui.swing.main.catalog.edit_tag");
                editButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        TagEditDialog dialog = new TagEditDialog(frame, document, tag);
                        dialog.run();
                    }
                    
                });
                buttonBar.add(editButton);

                MultilingualButton timeLimitButton = new MultilingualButton();
                timeLimitButton.putClientProperty(COMPONENT_ID_PROPERTY, "toggle-timelimit-button");
                timeLimitButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        history().addAndExecute(new ToggleCommand(tag.isTimeLimitEnabled()));
                    }
                    
                    class ToggleCommand extends AbstractCommand {
                        
                        final boolean wasTagTimeLimitEnabled;
                        
                        ToggleCommand(boolean wasTagTimeLimitEnabled) {
                            this.wasTagTimeLimitEnabled = wasTagTimeLimitEnabled;
                        }
                        
                        @Override
                        protected boolean _execute() {
                            tag.setTimeLimitEnabled(!wasTagTimeLimitEnabled);
                            document.getTagStore().refresh(tag);
                            return true;
                        }

                        @Override
                        protected boolean _rollBack() {
                            tag.setTimeLimitEnabled(wasTagTimeLimitEnabled);
                            document.getTagStore().refresh(tag);
                            return true;
                        }

                        @Override
                        public String toString() {
                            return text("ui.swing.main.command.toggle_tag_timelimit");
                        }
                        
                    }
                    
                });
                buttonBar.add(timeLimitButton);
                
                JButton deleteButton = new MultilingualButton(IconLoader.loadIcon("trash"), "ui.swing.main.catalog.delete_tag");
                deleteButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // TODO: convert to showConfirmDialog
                        int answer = OptionPaneUtil.showOptionDialog(
                            frame,
                            text("ui.swing.main.catalog.delete_tag.sure"),
                            text("ui.swing.main.catalog.delete_tag.confirm"),
                            JOptionPane.QUESTION_MESSAGE,
                            new String[]{
                                text("ui.swing.main.catalog.delete_tag.cancel"),
                                text("ui.swing.main.catalog.delete_tag.delete"),
                            },
                            new ButtonPurpose[] {
                                ButtonPurpose.NONE,
                                ButtonPurpose.DANGER,
                            },
                            text("ui.swing.main.catalog.delete_tag.delete")
                        );
                        if (answer==1) {
                            RemovalHelper removalHelper = new RemovalHelper(document);
                            Command command = removalHelper.createRemoveCommandFor(
                                tag,
                                "ui.swing.main.command.delete_tag"
                            );
                            history().addAndExecute(command);
                        }
                    }
                    
                });
                buttonBar.add(deleteButton);
                
                return container;
            }

            @Override
            public void refreshComponentFor(Component component, Tag tag, StoreListPanel<Tag> tagStoreListPanel) {
                JPanel box = (JPanel)ComponentTreeUtil.findByProperty(component, COMPONENT_ID_PROPERTY, "box");
                JLabel label = (JLabel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "item-label");
                MultilingualButton timeLimitButton = (MultilingualButton)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "toggle-timelimit-button");
                
                
                hu.webarticum.aurora.core.model.Color color = tag.getColor();
                hu.webarticum.aurora.core.model.Color contrastColor = color.getBlackWhiteContrastColor();
                
                box.setBackground(ColorUtil.toAwtColor(color));
                label.setText(tag.getLabel());
                label.setForeground(ColorUtil.toAwtColor(contrastColor));

                boolean isTagTimeLimitEnabled = tag.isTimeLimitEnabled();
                TimeLimit tagDefaultTimeLimit = tag.getTimeLimitManager().getDefaultTimeLimit();
                boolean hasTagDefaultTimeLimit = (tagDefaultTimeLimit!=null && !tagDefaultTimeLimit.isAlways());
                timeLimitButton.setIcon(IconLoader.loadIcon("timelimit_"+(isTagTimeLimitEnabled?"enabled":"disabled")));
                timeLimitButton.setLanguagePath("", "ui.swing.main.catalog."+(isTagTimeLimitEnabled?"disable_aspect_timelimit":"enable_aspect_timelimit"));
                java.awt.Color buttonBackgroundColor;
                if (isTagTimeLimitEnabled) {
                    if (hasTagDefaultTimeLimit) {
                        buttonBackgroundColor = new java.awt.Color(0x7799FF);
                    } else {
                        buttonBackgroundColor = new java.awt.Color(0xFFFF77);
                    }
                } else {
                    if (hasTagDefaultTimeLimit) {
                        buttonBackgroundColor = new java.awt.Color(0xFF9933);
                    } else {
                        buttonBackgroundColor = new java.awt.Color(0xEEEEEE);
                    }
                }
                timeLimitButton.setBackground(buttonBackgroundColor);
            }
            
        }, new StoreListPanel.ItemFilter<Tag>() {

            @Override
            public boolean validate(Tag tag, StoreListPanel<Tag> tagStoreListPanel) {
                Tag.Type currentType = tagTypeFilterToolBar.getTagType();
                return (tag.getType() == currentType);
            }
            
        });
        
        tagTypeFilterToolBar.addChangeListener(new TagTypeFilterToolBar.ChangeListener() {
            
            @Override
            public void onChanged(Type tagType) {
                tagStoreListPanel.refresh();
            }
            
        });

        tagStoreListPanel.putClientProperty(COMPONENT_ID_PROPERTY, "tag-store-list-panel");
        tagStoreListPanel.setLayout(new BoxLayout(tagStoreListPanel, BoxLayout.PAGE_AXIS));
        tagStoreListPanel.setBackground(java.awt.Color.WHITE);
        tagListContainerOuter.add(tagStoreListPanel, BorderLayout.PAGE_START);
        
        return tagListPanel;
    }

    private JPanel createResourcesPanel() {
        JPanel resourceListPanel = new JPanel();
        resourceListPanel.setLayout(new BorderLayout());
        resourceListPanel.setPreferredSize(new Dimension(220, 30));
        resourceListPanel.setBackground(java.awt.Color.WHITE);
        resourceListPanel.setBorder(new LineBorder(java.awt.Color.GRAY));

        JPanel resourceListHeadOuterPanel = new JPanel();
        resourceListHeadOuterPanel.setLayout(new BoxLayout(resourceListHeadOuterPanel, BoxLayout.PAGE_AXIS));
        resourceListPanel.add(resourceListHeadOuterPanel, BorderLayout.PAGE_START);
        
        JPanel resourceListHeadPanel = new JPanel();
        resourceListHeadPanel.setLayout(new BorderLayout());
        resourceListHeadPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        resourceListHeadOuterPanel.add(resourceListHeadPanel, BorderLayout.PAGE_START);

        resourceListHeadPanel.add(new MultilingualLabel("ui.swing.main.catalog.resources"), BorderLayout.LINE_START);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        resourceListHeadPanel.add(buttonPanel, BorderLayout.LINE_END);

        ResourceTypeFilterToolBar resourceTypeFilterToolBar = new ResourceTypeFilterToolBar();
        buttonPanel.add(resourceTypeFilterToolBar);
        
        MultilingualButton addButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.main.catalog.add_resource");
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Resource.Type type = resourceTypeFilterToolBar.getResourceType();
                Resource resource = new Resource(type);
                ResourceEditDialog dialog = new ResourceEditDialog(frame, document, resource);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    document.getResourceStore().register(resource);
                }
            }
            
        });
        buttonPanel.add(addButton);
        
        VerticallyScrollablePanel resourceListContainerOuter = new VerticallyScrollablePanel();
        resourceListContainerOuter.setBackground(java.awt.Color.WHITE);
        resourceListContainerOuter.setLayout(new BorderLayout());
        JScrollPane resourceListScrollPane = new JScrollPane(resourceListContainerOuter);
        resourceListScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        resourceListPanel.add(resourceListScrollPane, BorderLayout.CENTER);
        
        resourceListContainerOuter.setBackground(java.awt.Color.WHITE);
        
        final StoreListPanel<Resource> resourceStoreListPanel = new StoreListPanel<Resource>(
            document.getResourceStore(),
            new StoreListPanel.ComponentProvider<Resource>() {
    
                @Override
                public Component buildComponentFor(final Resource resource, StoreListPanel<Resource> resourceStoreListPanel) {
                    JPanel container = createItemContainer();
                    JPanel box = (JPanel)ComponentTreeUtil.findByProperty(container, COMPONENT_ID_PROPERTY, "box");
                    JPanel buttonBar = (JPanel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "button-bar");
                    
                    JButton editButton = new MultilingualButton(IconLoader.loadIcon("edit"), "ui.swing.main.catalog.edit_resource");
                    editButton.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            ResourceEditDialog dialog = new ResourceEditDialog(frame, document, resource);
                            dialog.run();
                        }
                        
                    });
                    buttonBar.add(editButton);
    
                    MultilingualButton timeLimitButton = new MultilingualButton();
                    timeLimitButton.putClientProperty(COMPONENT_ID_PROPERTY, "toggle-timelimit-button");
                    timeLimitButton.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            history().addAndExecute(new ToggleCommand(resource.isTimeLimitEnabled()));
                        }
                        
                        class ToggleCommand extends AbstractCommand {
                            
                            final boolean wasResourceTimeLimitEnabled;
                            
                            ToggleCommand(boolean wasResourceTimeLimitEnabled) {
                                this.wasResourceTimeLimitEnabled = wasResourceTimeLimitEnabled;
                            }
                            
                            @Override
                            protected boolean _execute() {
                                resource.setTimeLimitEnabled(!wasResourceTimeLimitEnabled);
                                document.getResourceStore().refresh(resource);
                                return true;
                            }
    
                            @Override
                            protected boolean _rollBack() {
                                resource.setTimeLimitEnabled(wasResourceTimeLimitEnabled);
                                document.getResourceStore().refresh(resource);
                                return true;
                            }
    
                            @Override
                            public String toString() {
                                return text("ui.swing.main.command.toggle_resource_timelimit");
                            }
                            
                        }
                        
                    });
                    buttonBar.add(timeLimitButton);
                    
                    JButton deleteButton = new MultilingualButton(IconLoader.loadIcon("trash"), "ui.swing.main.catalog.delete_resource");
                    deleteButton.addActionListener(new ActionListener() {
    
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            // TODO: convert to showConfirmDialog
                            int answer = OptionPaneUtil.showOptionDialog(
                                frame,
                                text("ui.swing.main.catalog.delete_resource.sure"),
                                text("ui.swing.main.catalog.delete_resource.confirm"),
                                JOptionPane.QUESTION_MESSAGE,
                                new String[]{
                                    text("ui.swing.main.catalog.delete_resource.cancel"),
                                    text("ui.swing.main.catalog.delete_resource.delete"),
                                },
                                new ButtonPurpose[] {
                                    ButtonPurpose.NONE,
                                    ButtonPurpose.DANGER,
                                },
                                text("ui.swing.main.catalog.delete_resource.delete")
                            );
                            if (answer == 1) {
                                RemovalHelper removalHelper = new RemovalHelper(document);
                                Command command = removalHelper.createRemoveCommandFor(
                                    resource,
                                    "ui.swing.main.command.delete_resource"
                                );
                                history().addAndExecute(command);
                            }
                        }
                        
                    });
                    buttonBar.add(deleteButton);
                    
                    return container;
                }
    
                @Override
                public void refreshComponentFor(Component component, Resource resource, StoreListPanel<Resource> resourceStoreListPanel) {
                    JPanel box = (JPanel)ComponentTreeUtil.findByProperty(component, COMPONENT_ID_PROPERTY, "box");
                    JLabel label = (JLabel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "item-label");
                    MultilingualButton timeLimitButton = (MultilingualButton)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "toggle-timelimit-button");
                    
                    hu.webarticum.aurora.core.model.Color color = resource.getColor();
                    hu.webarticum.aurora.core.model.Color contrastColor = color.getBlackWhiteContrastColor();
                    
                    box.setBackground(ColorUtil.toAwtColor(color));
                    label.setText(resource.getLabel());
                    label.setForeground(ColorUtil.toAwtColor(contrastColor));
                    
                    boolean isResourceTimeLimitEnabled = resource.isTimeLimitEnabled();
                    TimeLimit resourceDefaultTimeLimit = resource.getTimeLimitManager().getDefaultTimeLimit();
                    boolean hasResourceDefaultTimeLimit = (resourceDefaultTimeLimit!=null && !resourceDefaultTimeLimit.isAlways());
                    timeLimitButton.setIcon(IconLoader.loadIcon("timelimit_"+(isResourceTimeLimitEnabled?"enabled":"disabled")));
                    timeLimitButton.setLanguagePath("", "ui.swing.main.catalog."+(
                        isResourceTimeLimitEnabled?"disable_aspect_timelimit":"enable_aspect_timelimit"
                    ));
                    java.awt.Color buttonBackgroundColor;
                    if (isResourceTimeLimitEnabled) {
                        if (hasResourceDefaultTimeLimit) {
                            buttonBackgroundColor = new java.awt.Color(0x7799FF);
                        } else {
                            buttonBackgroundColor = new java.awt.Color(0xFFFF77);
                        }
                    } else {
                        if (hasResourceDefaultTimeLimit) {
                            buttonBackgroundColor = new java.awt.Color(0xFF9933);
                        } else {
                            buttonBackgroundColor = new java.awt.Color(0xEEEEEE);
                        }
                    }
                    timeLimitButton.setBackground(buttonBackgroundColor);
                }
                
            }, new StoreListPanel.ItemFilter<Resource>() {
    
                @Override
                public boolean validate(Resource resource, StoreListPanel<Resource> resourceStoreListPanel) {
                    Resource.Type currentType = resourceTypeFilterToolBar.getResourceType();
                    return (resource.getType() == currentType);
                }
                
        });
        
        resourceTypeFilterToolBar.addChangeListener(new ResourceTypeFilterToolBar.ChangeListener() {
            
            @Override
            public void onChanged(Resource.Type resourceType) {
                resourceStoreListPanel.refresh();
            }
        });
        
        resourceStoreListPanel.putClientProperty(COMPONENT_ID_PROPERTY, "resource-store-list-panel");
        resourceStoreListPanel.setLayout(new BoxLayout(resourceStoreListPanel, BoxLayout.PAGE_AXIS));
        resourceStoreListPanel.setBackground(java.awt.Color.WHITE);
        resourceListContainerOuter.add(resourceStoreListPanel, BorderLayout.PAGE_START);
        
        return resourceListPanel;
    }
    
    private void initBlocksTab() {
        tabItemBlocks.setLayout(new BorderLayout());
        
        JPanel blocksPanel = createBlocksPanel();
        {
            @SuppressWarnings("unchecked")
            StoreListPanel<Block> _blockStoreListPanel = (StoreListPanel<Block>)ComponentTreeUtil.findByProperty(
                blocksPanel, COMPONENT_ID_PROPERTY, "block-store-list-panel"
            );
            blockStoreListPanel = _blockStoreListPanel;
        }
        blockStoreListPanel.setLayout(new WrapLayout(WrapLayout.LEFT));
        tabItemBlocks.add(blocksPanel, BorderLayout.CENTER);
    }

    private JPanel createBlocksPanel() {
        JPanel blockListPanel = new JPanel();
        blockListPanel.setLayout(new BorderLayout());
        blockListPanel.setBackground(java.awt.Color.WHITE);
        blockListPanel.setBorder(new LineBorder(java.awt.Color.GRAY));
        
        JPanel blockListHeadPanel = new JPanel();
        blockListHeadPanel.setLayout(new BorderLayout());
        blockListHeadPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        blockListPanel.add(blockListHeadPanel, BorderLayout.PAGE_START);
        
        final BlockFilterPanel blockListFilterPanel = new BlockFilterPanel(frame, document);
        blockListHeadPanel.add(blockListFilterPanel, BorderLayout.CENTER);
        
        List<Resource> classes = document.getResourceStore().getAll(Resource.Type.CLASS);
        if (!classes.isEmpty()) {
            blockListFilterPanel.setResource(classes.get(0));
        }

        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BorderLayout());
        blockListHeadPanel.add(rightPanel, BorderLayout.LINE_END);
        
        MultilingualButton addButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.main.blocks.add_block");
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Block block = new Block(Block.DEFAULT_LENGTH);
                BlockEditDialog dialog = new BlockEditDialog(frame, document, block);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    document.getBlockStore().register(block);
                    checkBlockForNotification(block);
                }
            }
            
        });
        rightPanel.add(addButton, BorderLayout.PAGE_END);
        
        VerticallyScrollablePanel blockListContainerOuter = new VerticallyScrollablePanel();
        blockListContainerOuter.setBackground(java.awt.Color.WHITE);
        blockListContainerOuter.setLayout(new BorderLayout());
        JScrollPane tagListScrollPane = new JScrollPane(blockListContainerOuter);
        tagListScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        blockListPanel.add(tagListScrollPane, BorderLayout.CENTER);
        
        blockListContainerOuter.setBackground(java.awt.Color.WHITE);
        
        final StoreListPanel<Block> blockStoreListPanel = new StoreListPanel<Block>(document.getBlockStore(), new StoreListPanel.ComponentProvider<Block>() {

            @Override
            public Component buildComponentFor(final Block block, StoreListPanel<Block> blockListPanel) {
                JPanel container = createItemContainer();
                JPanel box = (JPanel)ComponentTreeUtil.findByProperty(container, COMPONENT_ID_PROPERTY, "box");
                JLabel label = (JLabel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "item-label");
                JPanel buttonBar = (JPanel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "button-bar");
                
                box.setPreferredSize(new Dimension(250, 60));
                
                label.setOpaque(true);
                
                JButton editButton = new MultilingualButton(IconLoader.loadIcon("edit"), "ui.swing.main.blocks.edit_block");
                editButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        BlockEditDialog dialog = new BlockEditDialog(frame, document, block);
                        dialog.run();
                        if (dialog.getResult()==EditDialog.RESULT_OK) {
                            checkBlockForNotification(block);
                        }
                    }
                    
                });
                buttonBar.add(editButton);

                JButton reorganizeButton = new MultilingualButton(IconLoader.loadIcon("organizer"), "ui.swing.main.blocks.reorganize_block");
                reorganizeButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        BlockActivityOrganizerDialog organizerDialog = new BlockActivityOrganizerDialog(frame, document, block, null);
                        organizerDialog.run();
                        
                        if (organizerDialog.getResult() == EditDialog.RESULT_OK) {
                            Document.BlockStore blockStore = document.getBlockStore();
                            blockStore.refresh();
                        }
                    }
                    
                });
                buttonBar.add(reorganizeButton);

                JButton putButton = new MultilingualButton(IconLoader.loadIcon("place"), "ui.swing.main.blocks.put_block");
                putButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        Board board = null;
                        if (document.getBoardStore().size()>0) {
                            board = document.getBoardStore().getAll().get(0);
                        }
                        tryPutBlock(board, block);
                    }
                    
                });
                buttonBar.add(putButton);

                JButton duplicatButton = new MultilingualButton(IconLoader.loadIcon("duplicate"), "ui.swing.main.blocks.duplicate_block");
                duplicatButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // TODO: convert to showConfirmDialog
                        int answer = OptionPaneUtil.showOptionDialog(
                            frame,
                            text("ui.swing.main.blocks.duplicate_block.sure"),
                            text("ui.swing.main.blocks.duplicate_block.confirm"),
                            JOptionPane.QUESTION_MESSAGE,
                            new String[]{
                                text("ui.swing.main.blocks.duplicate_block.cancel"),
                                text("ui.swing.main.blocks.duplicate_block.duplicate"),
                            },
                            new ButtonPurpose[] {
                                ButtonPurpose.NONE,
                                ButtonPurpose.ACTION,
                            },
                            text("ui.swing.main.blocks.duplicate_block.duplicate")
                        );
                        if (answer==1) {
                            history().addAndExecute(new DuplicateCommand());
                        }
                    }

                    class DuplicateCommand extends AbstractCommand {
                        
                        Block newBlock = null;
                        
                        @Override
                        protected boolean _execute() {
                            if (newBlock == null) {
                                newBlock = new Block(block);
                            }
                            document.getBlockStore().register(newBlock);
                            return true;
                        }

                        @Override
                        protected boolean _rollBack() {
                            document.getBlockStore().remove(newBlock);
                            return true;
                        }

                        @Override
                        public String toString() {
                            return text("ui.swing.main.command.duplicate_block");
                        }
                        
                    }
                    
                });
                buttonBar.add(duplicatButton);

                JButton deleteButton = new MultilingualButton(IconLoader.loadIcon("trash"), "ui.swing.main.blocks.delete_block");
                deleteButton.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        // TODO: convert to showConfirmDialog
                        int answer = OptionPaneUtil.showOptionDialog(
                            frame,
                            text("ui.swing.main.blocks.delete_block.sure"),
                            text("ui.swing.main.blocks.delete_block.confirm"),
                            JOptionPane.QUESTION_MESSAGE,
                            new String[]{
                                text("ui.swing.main.blocks.delete_block.cancel"),
                                text("ui.swing.main.blocks.delete_block.delete"),
                            },
                            new ButtonPurpose[] {
                                ButtonPurpose.NONE,
                                ButtonPurpose.DANGER,
                            },
                            text("ui.swing.main.blocks.delete_block.delete")
                        );
                        if (answer==1) {
                            RemovalHelper removalHelper = new RemovalHelper(document);
                            Command command = removalHelper.createRemoveCommandFor(
                                block,
                                "ui.swing.main.command.delete_block"
                            );
                            history().addAndExecute(command);
                        }
                    }
                    
                });
                buttonBar.add(deleteButton);
                
                return container;
            }

            @Override
            public void refreshComponentFor(Component component, Block block, StoreListPanel<Block> blockListPanel) {
                PaintedPanel box = (PaintedPanel)ComponentTreeUtil.findByProperty(component, COMPONENT_ID_PROPERTY, "box");
                JLabel label = (JLabel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "item-label");
                
                Object colorAspect = blockListFilterPanel.getColorAspect();
                
                Set<hu.webarticum.aurora.core.model.Color> colors = new LinkedHashSet<hu.webarticum.aurora.core.model.Color>();
                if (colorAspect instanceof Tag.Type) {
                    for (Tag tag: block.getActivityManager().getActivities().getTags((Tag.Type)colorAspect)) {
                        colors.add(tag.getColor());
                    }
                } else if (colorAspect instanceof Resource.Type) {
                    for (Resource resource: block.getActivityManager().getActivities().getResources((Resource.Type)colorAspect)) {
                        colors.add(resource.getColor());
                    }
                }
                
                Painter<Component> painter = ColorUtil.createStripPainter(colors);
                box.setPainter(painter);
                
                hu.webarticum.aurora.core.model.Color avgColor;
                if (!colors.isEmpty()) {
                    avgColor = hu.webarticum.aurora.core.model.Color.avg(colors);
                } else {
                    avgColor = hu.webarticum.aurora.core.model.Color.WHITE;
                }
                
                String blockText = block.getLabel() + " ";
                for (Board board: document.getBoardStore()) {
                    if (board.getBlocks().contains(block)) {
                        blockText += "*";
                    }
                }
                
                label.setText(blockText);
                label.setBackground(ColorUtil.toAwtColor(avgColor));
                label.setForeground(ColorUtil.toAwtColor(avgColor.getBlackWhiteContrastColor()));
                
                box.repaint();
            }
            
        }, new StoreListPanel.ItemFilter<Block>() {

            @Override
            public boolean validate(Block block, StoreListPanel<Block> blockListPanel) {
                return blockListFilterPanel.getBlockFilter().validate(block);
            }
            
        });
        blockListFilterPanel.addChangeListener(new BlockFilterPanel.BlockFilterChangeListener() {
            
            @Override
            public void changed(BlockFilterChangeEvent ev) {
                blockStoreListPanel.refresh(ev.isFilterChanged());
            }
            
        });

        blockStoreListPanel.putClientProperty(COMPONENT_ID_PROPERTY, "block-store-list-panel");
        blockStoreListPanel.setLayout(new BoxLayout(blockStoreListPanel, BoxLayout.PAGE_AXIS));
        blockStoreListPanel.setBackground(java.awt.Color.WHITE);
        blockListContainerOuter.add(blockStoreListPanel, BorderLayout.PAGE_START);
        
        return blockListPanel;
    }

    private void initTablesTab() {
        tabItemBoards.setLayout(new BorderLayout());
        

        JPanel tablesPanel = createTablesPanel();
        {
            @SuppressWarnings("unchecked")
            StoreListPanel<Board> _boardStoreListPanel = (StoreListPanel<Board>)ComponentTreeUtil.findByProperty(
                tablesPanel, COMPONENT_ID_PROPERTY, "board-store-list-panel"
            );
            boardStoreListPanel = _boardStoreListPanel;
        }
        tabItemBoards.add(tablesPanel, BorderLayout.LINE_START);
        
        JPanel boardViewPanel = new JPanel();
        boardViewPanel.setLayout(new BorderLayout());
        tabItemBoards.add(boardViewPanel, BorderLayout.CENTER);
        
        JPanel boardViewSettingsPanel = new JPanel();
        boardViewSettingsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        boardViewPanel.add(boardViewSettingsPanel, BorderLayout.PAGE_START);


        boardBlockFilterPanel = new BlockFilterPanel(frame, document);

        List<Resource> classes = document.getResourceStore().getAll(Resource.Type.CLASS);
        if (!classes.isEmpty()) {
            boardBlockFilterPanel.setResource(classes.get(0));
        }

        boardBlockFilterPanel.addChangeListener(new BlockFilterPanel.BlockFilterChangeListener() {
            
            @Override
            public void changed(BlockFilterChangeEvent ev) {
                refreshTableViewAsynchronous();
            }
            
        });
        boardViewPanel.add(boardBlockFilterPanel, BorderLayout.PAGE_START);
        
        
        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BorderLayout());
        boardViewPanel.add(bottomPanel, BorderLayout.PAGE_END);
        
        
        
        JPanel colorSchemePanel = new JPanel();
        colorSchemePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        bottomPanel.add(colorSchemePanel, BorderLayout.LINE_START);
        
        
        JLabel colorSchemeLabel = new MultilingualLabel("ui.swing.main.tables.preview.theme");
        colorSchemePanel.add(colorSchemeLabel);
        
        colorSchemeComboBox = new MultilingualComboBox<ActivityFlowImageRenderer.ColorScheme>();
        colorSchemeComboBox.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                refreshTableViewAsynchronous();
            }
            
        });
        addColorSchemes();
        colorSchemePanel.add(colorSchemeComboBox);
        
        JPanel boardViewButtonsPanel = new JPanel();
        boardViewButtonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        bottomPanel.add(boardViewButtonsPanel, BorderLayout.LINE_END);
        
        JButton saveBoardViewImageButton = new MultilingualButton("ui.swing.main.tables.save_as_image");
        saveBoardViewImageButton.setIcon(IconLoader.loadIcon("table-save"));
        saveBoardViewImageButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                final AutoExtensionFileChooser fileChooser = new AutoExtensionFileChooser();
                FileFilter singleFileChooserFilter = new AutoExtensionFileChooser.ImageFilter();
                fileChooser.addChoosableFileFilter(singleFileChooserFilter);
                fileChooser.setFileFilter(singleFileChooserFilter);
                
                int chooseResult = fileChooser.showSaveDialog(frame);
                if (chooseResult == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getAutoSelectedFile();
                    
                    BufferedImage image = boardViewImageProvider.getImage();
                    String format = "png";
                    String filename = file.getName();
                    int pos = filename.lastIndexOf('.');
                    if (pos >= 0) {
                        String extension = filename.substring(pos + 1);
                        if (extension.matches("png|jpg|gif|bmp|wbmp")) {
                            format = extension;
                        }
                    }
                    try {
                        ImageIO.write(image, format, file);
                    } catch (IOException e) {
                        OptionPaneUtil.showMessageDialog(frame, text("ui.swing.main.tables.save_as_image.error"));
                    }
                }
            }
            
        });
        boardViewButtonsPanel.add(saveBoardViewImageButton);
        
        JButton printTableViewImageButton = new MultilingualButton("ui.swing.main.tables.print");
        printTableViewImageButton.setIcon(IconLoader.loadIcon("table-print"));
        printTableViewImageButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                final BufferedImage image = boardViewImageProvider.getImage();
                PrinterJob printerJob = PrinterJob.getPrinterJob();
                printerJob.setPrintable(new Printable() {
                    
                    @Override
                    public int print(Graphics g, PageFormat pf, int page) throws PrinterException {
                        if (page>0) {
                            return NO_SUCH_PAGE;
                        }
                        int iw = image.getWidth();
                        int ih = image.getHeight();
                        g.drawImage(image, 0, 0, iw, ih, 0, 0, iw, ih, null);
                        return PAGE_EXISTS;
                    }
                    
                });
                if (printerJob.printDialog()) {
                    try {
                        printerJob.print();
                    } catch (PrinterException e) {
                        OptionPaneUtil.showMessageDialog(frame, text("ui.swing.main.tables.print.error"));
                    }
                }
            }
            
        });
        boardViewButtonsPanel.add(printTableViewImageButton);
        
        JPanel boardViewImagePanelOuter = new JPanel();
        boardViewImagePanelOuter.setLayout(new BorderLayout());
        boardViewImagePanelOuter.setBackground(java.awt.Color.WHITE);
        boardViewImageScrollPane = new JScrollPane(boardViewImagePanelOuter);
        boardViewImageScrollPane.getHorizontalScrollBar().setUnitIncrement(16);
        boardViewImageScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        boardViewPanel.add(boardViewImageScrollPane, BorderLayout.CENTER);

        final GeneralWrapper<Point> dragStartPointWrapper = new GeneralWrapper<Point>();
        final GeneralWrapper<ActivityFlowImageRenderer.PositionedActivityEntry> clickedPositionedActivityEntryWrapper =
            new GeneralWrapper<ActivityFlowImageRenderer.PositionedActivityEntry>()
        ;

        final Runnable editBlockAction = new Runnable() {
            
            @Override
            public void run() {
                if (clickedPositionedActivityEntryWrapper.isNull()) {
                    return;
                }
                
                ActivityFlowImageRenderer.PositionedActivityEntry positionedActivityEntry = clickedPositionedActivityEntryWrapper.get();
                Board board = boardViewImageProvider.getBoard();
                Board.Entry blockEntry = board.search(positionedActivityEntry.activity);
                
                if (blockEntry!=null) {
                    Block block = blockEntry.getBlock();

                    BlockEditDialog dialog = new BlockEditDialog(frame, document, block);
                    dialog.run();
                }
            }
            
        };

        final Runnable removeBlockAction = new Runnable() {
            
            @Override
            public void run() {
                if (clickedPositionedActivityEntryWrapper.isNull()) {
                    return;
                }
                
                ActivityFlowImageRenderer.PositionedActivityEntry positionedActivityEntry = clickedPositionedActivityEntryWrapper.get();
                Board board = boardViewImageProvider.getBoard();
                Board.Entry blockEntry = board.search(positionedActivityEntry.activity);
                
                if (blockEntry!=null) {
                    Block block = blockEntry.getBlock();

                    // TODO: convert to showConfirmDialog
                    int answer = OptionPaneUtil.showOptionDialog(
                        frame,
                        text("ui.swing.main.tables.preview.block_operations.remove_block.sure")+"\n\n'"+block.getLabel()+"'",
                        text("ui.swing.main.tables.preview.block_operations.remove_block.confirm"),
                        JOptionPane.QUESTION_MESSAGE,
                        new String[]{
                            text("ui.swing.main.tables.preview.block_operations.remove_block.cancel"),
                            text("ui.swing.main.tables.preview.block_operations.remove_block.remove"),
                        },
                        new ButtonPurpose[] {
                            ButtonPurpose.NONE,
                            ButtonPurpose.DANGER,
                        },
                        text("ui.swing.main.tables.preview.block_operations.remove_block.remove")
                    );
                    if (answer==1) {
                        history().addAndExecute(new RemoveBlockFromTableCommand(document, board, block));
                    }
                }
            }
            
        };

        final Runnable moveBlockAction = new Runnable() {
            
            @Override
            public void run() {
                ActivityFlowImageRenderer.PositionedActivityEntry positionedActivityEntry = clickedPositionedActivityEntryWrapper.get();
                if (positionedActivityEntry == null) {
                    return;
                }
                
                Board board = boardViewImageProvider.getBoard();
                Board.Entry blockEntry = board.search(positionedActivityEntry.activity);
                
                if (blockEntry!=null) {
                    Block block = blockEntry.getBlock();
                    Time startTime = blockEntry.getTime();
                    
                    GeneralWrapper<Time> wrapper = new GeneralWrapper<Time>(startTime);
                    BlockTimeSelectDialog blockTimeSelectDialog = new BlockTimeSelectDialog(frame, block, wrapper);
                    blockTimeSelectDialog.run();
                    
                    if (blockTimeSelectDialog.getResult()==EditDialog.RESULT_OK) {
                        Time time = wrapper.get();
                        if (board.conflictsWithBlockAt(block, time, false)) {
                            // TODO: convert to showConfirmDialog
                            int answer = OptionPaneUtil.showOptionDialog(
                                frame,
                                text("ui.swing.main.tables.preview.block_operations.move_block.conflict.sure"),
                                text("ui.swing.main.tables.preview.block_operations.move_block.conflict.confirm"),
                                JOptionPane.QUESTION_MESSAGE,
                                new String[]{
                                    text("ui.swing.main.tables.preview.block_operations.move_block.conflict.cancel"),
                                    text("ui.swing.main.tables.preview.block_operations.move_block.conflict.move"),
                                },
                                new ButtonPurpose[] {
                                    ButtonPurpose.NONE,
                                    ButtonPurpose.ACTION,
                                },
                                text("ui.swing.main.tables.preview.block_operations.move_block.conflict.move")
                            );
                            if (answer!=1) {
                                return;
                            }
                        }
                        history().addAndExecute(new MoveBlockOnTableCommand(document, board, block, time));
                    }
                }
            }
            
        };

        boardViewImagePanel = new BoardViewImagePanel();
        boardViewImagePanel.addMouseListener(new MouseListener() {
            
            @Override
            public void mouseReleased(MouseEvent ev) {
                int mouseX = ev.getX();
                int mouseY = ev.getY();
                
                if (!clickedPositionedActivityEntryWrapper.isNull()) {
                    ActivityFlowImageRenderer.PositionedActivityEntry activityEntry = clickedPositionedActivityEntryWrapper.get();
                    Board board = boardViewImageProvider.getBoard();
                    Activity activity = activityEntry.activity;
                    Board.Entry blockEntry = board.search(activity);
                    if (blockEntry!=null) {
                        if (dragTargetRectangles != null) {
                            int pw = boardViewImagePanel.getWidth();
                            int ph = boardViewImagePanel.getHeight();
                            BufferedImage image = boardViewImageProvider.getImage();
                            int iw = image.getWidth();
                            int ih = image.getHeight();
                            int top = Math.max(0, (ph-ih)/2);
                            int left = Math.max(0, (pw-iw)/2);
                            int imageTop = mouseY-top;
                            int imageLeft = mouseX-left;
                            
                            for (Map.Entry<Time, DragTargetRectangleEntry> entry: dragTargetRectangles.entrySet()) {
                                DragTargetRectangleEntry dragTargetRectangleEntry = entry.getValue();
                                if (
                                    imageLeft >= dragTargetRectangleEntry.rectangle.x &&
                                    imageLeft < dragTargetRectangleEntry.rectangle.x + dragTargetRectangleEntry.rectangle.width &&
                                    imageTop >= dragTargetRectangleEntry.rectangle.y &&
                                    imageTop < dragTargetRectangleEntry.rectangle.y + dragTargetRectangleEntry.rectangle.height
                                ) {
                                    Time time = entry.getKey();
                                    Board.Entry boardEntry = board.search(clickedPositionedActivityEntryWrapper.get().activity);
                                    Block block = boardEntry.getBlock();
                                    Time oldTime = boardEntry.getTime();
                                    if (!time.equals(oldTime)) {
                                        history().addAndExecute(new MoveBlockOnTableCommand(document, board, block, time));
                                    }
                                    break;
                                }
                            }
                        } else if (ev.getButton()==MouseEvent.BUTTON1) {
                            IconButtonSelectDialog.Item[] items = new IconButtonSelectDialog.Item[] {

                                new IconButtonSelectDialog.Item(
                                    text("ui.swing.main.tables.preview.block_operations.edit_block"),
                                    IconLoader.loadIcon("block-edit"),
                                    editBlockAction
                                ),

                                new IconButtonSelectDialog.Item(
                                    text("ui.swing.main.tables.preview.block_operations.move_block"),
                                    IconLoader.loadIcon("block-move"),
                                    moveBlockAction
                                ),

                                new IconButtonSelectDialog.Item(
                                    text("ui.swing.main.tables.preview.block_operations.remove_block"),
                                    IconLoader.loadIcon("block-remove"),
                                    removeBlockAction
                                ),
                                
                            };
                            IconButtonSelectDialog dialog = new IconButtonSelectDialog(
                                frame, text("ui.swing.main.tables.preview.block_operations"), items
                            );
                            dialog.run();
                        } else if (ev.getButton()==MouseEvent.BUTTON2) {
                            moveBlockAction.run();
                        } else if (ev.getButton()==MouseEvent.BUTTON3) {
                            removeBlockAction.run();
                        }
                    }
                }
                
                dragStartPointWrapper.set(null);
                clickedPositionedActivityEntryWrapper.set(null);
                dragTargetRectangles = null;
                boardViewImagePanel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                
                Point mousePosition = MouseInfo.getPointerInfo().getLocation();
                SwingUtilities.convertPointFromScreen(mousePosition, boardViewImagePanel);
                onMouseMovedOnTableView((int)mousePosition.getX(), (int)mousePosition.getY());
            }
            
            @Override
            public void mousePressed(MouseEvent ev) {
                int x = ev.getX();
                int y = ev.getY();
                dragStartPointWrapper.set(new Point(x, y));
                int pw = boardViewImagePanel.getWidth();
                int ph = boardViewImagePanel.getHeight();
                BufferedImage image = boardViewImageProvider.getImage();
                int iw = image.getWidth();
                int ih = image.getHeight();
                int left = Math.max(0, (pw-iw)/2);
                int top = Math.max(0, (ph-ih)/2);
                int imageLeft = x-left;
                int imageTop = y-top;
                ActivityFlowImageRenderer.PositionedActivityEntry activityEntry = boardViewImageProvider.getActivityEntryAtPosition(imageLeft, imageTop);
                clickedPositionedActivityEntryWrapper.set(activityEntry);
            }
            
            @Override
            public void mouseExited(MouseEvent ev) {
                if (dragStartPointWrapper.isNull()) {
                    onMouseMovedOnTableView(Integer.MIN_VALUE, Integer.MIN_VALUE);
                }
            }
            
            @Override
            public void mouseEntered(MouseEvent ev) {
                if (dragStartPointWrapper.isNull()) {
                    onMouseMovedOnTableView(ev.getX(), ev.getY());
                }
            }
            
            @Override
            public void mouseClicked(MouseEvent ev) {
            }
            
        });
        boardViewImagePanel.addMouseMotionListener(new MouseMotionListener() {
            
            @Override
            public void mouseMoved(MouseEvent ev) {
                onMouseMovedOnTableView(ev.getX(), ev.getY());
            }
            
            @Override
            public void mouseDragged(MouseEvent ev) {
                int mouseX = ev.getX();
                int mouseY = ev.getY();
                
                if (!dragStartPointWrapper.isNull()) {
                    double minimumMove = 10;
                    Point dragStartPoint = dragStartPointWrapper.get();
                    int startX = (int)dragStartPoint.getX();
                    int startY = (int)dragStartPoint.getY();
                    int currentX = ev.getX();
                    int currentY = ev.getY();
                    int moveX = currentX-startX;
                    int moveY = currentY-startY;
                    double move = Math.sqrt((moveX*moveX)+(moveY*moveY));
                    if (move >= minimumMove && dragTargetRectangles == null && !clickedPositionedActivityEntryWrapper.isNull()) {
                        Board board = boardViewImageProvider.getBoard();
                        Block block = board.search(clickedPositionedActivityEntryWrapper.get().activity).getBlock();
                        dragTargetRectangles = new HashMap<Time, DragTargetRectangleEntry>();
                        long blockLength = block.getLength();

                        ActivityFlowImageRenderer.Result rendererResult = boardViewImageProvider.getRendererResult();
                        int rectangleHeight = (int)(blockLength / rendererResult.secondsPerPixel);
                        
                        for (TimingSet.TimeEntry timeEntry: block.getCalculatedTimingSet()) {
                            Time time = timeEntry.getTime();
                            long seconds = time.getSeconds();
                            int secondsInDay = (int)(seconds % 86400);
                            long daySeconds = seconds - secondsInDay;
                            Time dayTime = new Time(daySeconds);
                            boolean free = !board.conflictsWithBlockAt(block, time, false);
                            
                            // XXX: next/prev day?
                            if (!rendererResult.dayColumns.containsKey(dayTime)) {
                                continue;
                            }
                            
                            Pair<Integer, Integer> columnLeftWidth = rendererResult.dayColumns.get(dayTime);
                            int columnLeft = columnLeftWidth.getLeft();
                            int columnWidth = columnLeftWidth.getRight();
                            
                            int rectangleTop = rendererResult.dayColumnTop + ((secondsInDay - rendererResult.startInDay) / rendererResult.secondsPerPixel);
                            
                            Rectangle rectangle = new Rectangle(columnLeft, rectangleTop, columnWidth, rectangleHeight);
                            
                            dragTargetRectangles.put(time, new DragTargetRectangleEntry(rectangle, free));
                        }
                        
                        boardViewImagePanel.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                    }
                    if (dragTargetRectangles != null) {
                        int pw = boardViewImagePanel.getWidth();
                        int ph = boardViewImagePanel.getHeight();
                        BufferedImage image = boardViewImageProvider.getImage();
                        int iw = image.getWidth();
                        int ih = image.getHeight();
                        int top = Math.max(0, (ph-ih)/2);
                        int left = Math.max(0, (pw-iw)/2);
                        int imageTop = mouseY-top;
                        int imageLeft = mouseX-left;
                        
                        boolean hoverFound = false;
                        for (Map.Entry<Time, DragTargetRectangleEntry> entry: dragTargetRectangles.entrySet()) {
                            DragTargetRectangleEntry dragTargetRectangleEntry = entry.getValue();
                            if (
                                imageLeft >= dragTargetRectangleEntry.rectangle.x &&
                                imageLeft < dragTargetRectangleEntry.rectangle.x + dragTargetRectangleEntry.rectangle.width &&
                                imageTop >= dragTargetRectangleEntry.rectangle.y &&
                                imageTop < dragTargetRectangleEntry.rectangle.y + dragTargetRectangleEntry.rectangle.height &&
                                !hoverFound
                            ) {
                                dragTargetRectangleEntry.hover = true;
                                hoverFound = true;
                            } else {
                                dragTargetRectangleEntry.hover = false;
                            }
                        }
                    }
                }
                
                onMouseMovedOnTableView(mouseX, mouseY);
            }
            
        });
        boardViewImagePanelOuter.add(boardViewImagePanel, BorderLayout.CENTER);

        refreshTableViewAsynchronous();
    }
    
    private void checkBlockForNotification(Block block) {
        MessageCollector collector = new FilteringMessageCollector(
            new DefaultBlockProblemCollector(block, document),
            FilteringMessageCollector.ERROR_FILTER
        );
        for (Message message : collector.collect()) {
            GlobalUiObjects.getNotificationQueue().send(
                message.getTitle().toString(),
                message.getDescription().toString(),
                NotificationQueue.Notification.TYPE.ERROR
            );
        }
    }

    private JPanel createTablesPanel() {
        JPanel boardListPanel = new JPanel();
        boardListPanel.setLayout(new BorderLayout());
        boardListPanel.setPreferredSize(new Dimension(270, 30));
        boardListPanel.setBackground(java.awt.Color.WHITE);
        boardListPanel.setBorder(new LineBorder(java.awt.Color.GRAY));

        JPanel boardListHeadOuterPanel = new JPanel();
        boardListHeadOuterPanel.setLayout(new BoxLayout(boardListHeadOuterPanel, BoxLayout.PAGE_AXIS));
        boardListPanel.add(boardListHeadOuterPanel, BorderLayout.PAGE_START);
        
        JPanel boardListHeadPanel = new JPanel();
        boardListHeadPanel.setLayout(new BorderLayout());
        boardListHeadPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        boardListHeadOuterPanel.add(boardListHeadPanel, BorderLayout.PAGE_START);

        boardListHeadPanel.add(new MultilingualLabel("ui.swing.main.tables.tables"), BorderLayout.LINE_START);

        MultilingualButton addButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.main.tables.add_table");
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Board board = new Board();
                BoardEditDialog dialog = new BoardEditDialog(frame, document, board);
                dialog.run();
                if (dialog.getResult() == EditDialog.RESULT_OK) {
                    document.getBoardStore().register(board);
                    boardViewImageProvider.setBoard(board);
                    document.getBoardStore().refresh(false);
                }
            }
            
        });
        boardListHeadPanel.add(addButton, BorderLayout.LINE_END);
        
        VerticallyScrollablePanel boardListContainerOuter = new VerticallyScrollablePanel();
        boardListContainerOuter.setBackground(java.awt.Color.WHITE);
        boardListContainerOuter.setLayout(new BorderLayout());
        JScrollPane tagListScrollPane = new JScrollPane(boardListContainerOuter);
        tagListScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        boardListPanel.add(tagListScrollPane, BorderLayout.CENTER);
        
        boardListContainerOuter.setBackground(java.awt.Color.WHITE);
        
        final StoreListPanel<Board> boardStoreListPanel = new StoreListPanel<Board>(
            document.getBoardStore(), new StoreListPanel.ComponentProvider<Board>() {
    
                @Override
                public Component buildComponentFor(final Board board, final StoreListPanel<Board> boardStoreListPanel) {
                    JPanel container = createItemContainer();
                    JPanel box = (JPanel)ComponentTreeUtil.findByProperty(container, COMPONENT_ID_PROPERTY, "box");
                    JPanel buttonBar = (JPanel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "button-bar");
                    
                    JButton editButton = new MultilingualButton(IconLoader.loadIcon("edit"), "ui.swing.main.tables.edit_table");
                    editButton.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            BoardEditDialog dialog = new BoardEditDialog(frame, document, board);
                            dialog.run();
                        }
                        
                    });
                    buttonBar.add(editButton);
                    
                    JButton viewButton = new MultilingualButton(IconLoader.loadIcon("hidden"), "ui.swing.main.tables.view_table");
                    viewButton.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            boardViewImageProvider.setBoard(board);
                            boardStoreListPanel.refresh(false);
                            refreshTableViewAsynchronous();
                        }
                        
                    });
                    viewButton.putClientProperty(COMPONENT_ID_PROPERTY, "view-button");
                    buttonBar.add(viewButton);
                    
                    JButton checkButton = new MultilingualButton(IconLoader.loadIcon("check"), "ui.swing.main.tables.check_table");
                    checkButton.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            runBoardCheck(board);
                        }
                        
                    });
                    buttonBar.add(checkButton);
                    
                    JButton moreButton = new MultilingualButton(IconLoader.loadIcon("more"), "ui.swing.main.tables.more_table_operations");
                    moreButton.addActionListener(new ActionListener() {
                        
                        @Override
                        public void actionPerformed(ActionEvent ev) {
                            showBoardMoreOptions(board);
                        }
                        
                    });
                    buttonBar.add(moreButton);
                    
                    return container;
                }
    
                @Override
                public void refreshComponentFor(Component component, Board board, StoreListPanel<Board> boardStoreListPanel) {
                    JPanel box = (JPanel)ComponentTreeUtil.findByProperty(component, COMPONENT_ID_PROPERTY, "box");
                    JLabel label = (JLabel)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "item-label");
    
                    JButton viewButton = (JButton)ComponentTreeUtil.findByProperty(box, COMPONENT_ID_PROPERTY, "view-button");
                    
                    if (board == boardViewImageProvider.getBoard()) {
                        box.setBackground(new java.awt.Color(0xFFF099));
                        viewButton.setIcon(IconLoader.loadIcon("view"));
                    } else {
                        box.setBackground(defaultBoxColor);
                        viewButton.setIcon(IconLoader.loadIcon("hidden"));
                    }
                    
                    label.setText(board.getLabel());
                }
                
            }
        );

        boardStoreListPanel.putClientProperty(COMPONENT_ID_PROPERTY, "board-store-list-panel");
        boardStoreListPanel.setLayout(new BoxLayout(boardStoreListPanel, BoxLayout.PAGE_AXIS));
        boardStoreListPanel.setBackground(java.awt.Color.WHITE);
        boardListContainerOuter.add(boardStoreListPanel, BorderLayout.PAGE_START);
        
        return boardListPanel;
    }

    private void showBoardMoreOptions(final Board board) {
        final GeneralWrapper<IconButtonSelectDialog> operationsSelectWrapper = new GeneralWrapper<IconButtonSelectDialog>();
        
        IconButtonSelectDialog.Item[] items = new IconButtonSelectDialog.Item[] {
            
            new IconButtonSelectDialog.Item(
                text("ui.swing.main.tables.table_operations.auto_put"),
                IconLoader.loadIcon("board-autoput"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        AutoPutDialog dialog = new AutoPutDialog(frame, document, board);
                        dialog.run();
                        if (dialog.getResult() == EditDialog.RESULT_OK) {
                            document.getBlockStore().refresh(false);
                        }
                    }
                    
                }
            ),

            new IconButtonSelectDialog.Item(
                text("ui.swing.main.tables.table_operations.manual_put"),
                IconLoader.loadIcon("board-manualput"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        tryPutBlock(board, null);
                    }
                    
                }
            ),

            new IconButtonSelectDialog.Item(
                text("ui.swing.main.tables.table_operations.export_table"),
                IconLoader.loadIcon("board-export"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        final GeneralWrapper<IconButtonSelectDialog> exportSelectWrapper = new GeneralWrapper<IconButtonSelectDialog>();
                        IconButtonSelectDialog boardExportSelectDialog = new IconButtonSelectDialog(
                            frame,
                            operationsSelectWrapper.get(),
                            text("ui.swing.main.tables.table_operations.export_table.title"),
                            new IconButtonSelectDialog.Item(
                                text("ui.swing.main.tables.table_operations.export_table.type.image"),
                                IconLoader.loadIcon("boardexport-image"),
                                new Runnable() {
                                    
                                    @Override
                                    public void run() {
                                        new ImageExportDialog(frame, board, document, boardBlockFilterPanel, colorSchemeComboBox.getSelectedData()).run();
                                    }
                                    
                                }
                            ),
                            new IconButtonSelectDialog.Item(
                                text("ui.swing.main.tables.table_operations.export_table.type.webpage"),
                                IconLoader.loadIcon("boardexport-web"),
                                new Runnable() {
                                    
                                    @Override
                                    public void run() {
                                        new WebPageExportDialog(frame, board).run();
                                    }
                                    
                                }
                            ),
                            new IconButtonSelectDialog.Item(
                                text("ui.swing.main.tables.table_operations.export_table.type.spreadsheet"),
                                IconLoader.loadIcon("boardexport-excel"),
                                new Runnable() {
                                    
                                    @Override
                                    public void run() {
                                        new IconButtonSelectDialog(
                                            frame,
                                            exportSelectWrapper.get(),
                                            text("ui.swing.main.tables.table_operations.export_table.type.spreadsheet.title"),
                                            new IconButtonSelectDialog.Item(
                                                text("ui.swing.main.tables.table_operations.export_table.type.spreadsheet.type.big"),
                                                IconLoader.loadIcon("boardexport-big"),
                                                new Runnable() {
                                                    
                                                    @Override
                                                    public void run() {
                                                        new BigSpreadsheetExportDialog(frame, board).run();
                                                    }
                                                    
                                                }
                                            ),
                                            new IconButtonSelectDialog.Item(
                                                    text("ui.swing.main.tables.table_operations.export_table.type.spreadsheet.type.list"),
                                                IconLoader.loadIcon("boardexport-list"),
                                                new Runnable() {
                                                    
                                                    @Override
                                                    public void run() {
                                                        new ListSpreadsheetExportDialog(frame, document, board).run();
                                                    }
                                                    
                                                }
                                            ),
                                            new IconButtonSelectDialog.Item(
                                                    text("ui.swing.main.tables.table_operations.export_table.type.spreadsheet.type.template"),
                                                IconLoader.loadIcon("boardexport-template"),
                                                new Runnable() {
                                                    
                                                    @Override
                                                    public void run() {
                                                        new TemplateSpreadsheetExportDialog(frame, document, board).run();
                                                    }
                                                    
                                                }
                                            )
                                        ).run();
                                    }
                                    
                                }
                            ),
                            new IconButtonSelectDialog.Item(
                                text("ui.swing.main.tables.table_operations.export_table.type.asctimetables"),
                                IconLoader.loadIcon("boardexport-asc"),
                                new Runnable() {
                                    
                                    @Override
                                    public void run() {
                                        new AscTimetablesExportDialog(frame, board, document).run();
                                    }
                                    
                                }
                            )
                        );
                        exportSelectWrapper.set(boardExportSelectDialog);
                        boardExportSelectDialog.run();
                    }
                    
                }
            ),

            new IconButtonSelectDialog.Item(
                text("ui.swing.main.tables.table_operations.duplicate_table"),
                IconLoader.loadIcon("board-duplicate"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        String newBoradLabel = OptionPaneUtil.showInputDialog(
                            frame,
                            text("ui.swing.main.tables.table_operations.duplicate_table.name_of_new"),
                            String.format(text("ui.swing.main.tables.table_operations.duplicate_table.copy_of"), board.getLabel()),
                            ButtonPurpose.SAVE
                        );
                        if (newBoradLabel == null || newBoradLabel.length() == 0) {
                            return;
                        }
                        Board newBoard = new Board(board);
                        newBoard.setLabel(newBoradLabel);
                        history().addAndExecute(new EditDialog.StoreItemRegisterCommand<Board>(
                            "ui.swing.main.command.duplicate_table", document.getBoardStore(), newBoard
                        ) {

                            @Override
                            protected boolean _execute() {
                                if (!super._execute()) {
                                    return false;
                                }
                                document.getBlockStore().refresh(false);
                                return true;
                            }

                            @Override
                            protected boolean _rollBack() {
                                if (!super._rollBack()) {
                                    return false;
                                }
                                document.getBlockStore().refresh(false);
                                return true;
                            }
                            
                        });
                    }
                    
                }
            ),

            new IconButtonSelectDialog.Item(
                text("ui.swing.main.tables.table_operations.duplicate_filtered_table"),
                IconLoader.loadIcon("board-filterduplicate"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        String newTableLabel = OptionPaneUtil.showInputDialog(
                            frame,
                            text("ui.swing.main.tables.table_operations.duplicate_filtered_table.name_of_new"),
                            String.format(text("ui.swing.main.tables.table_operations.duplicate_filtered_table.copy_of"), board.getLabel()),
                            ButtonPurpose.SAVE
                        );
                        if (newTableLabel == null || newTableLabel.length() == 0) {
                            return;
                        }
                        Board newBoard = board.filter(boardBlockFilterPanel.getBlockFilter());
                        newBoard.setLabel(newTableLabel);
                        history().addAndExecute(new EditDialog.StoreItemRegisterCommand<Board>(
                            "ui.swing.main.command.duplicate_filtered_table", document.getBoardStore(), newBoard
                        ) {

                            @Override
                            protected boolean _execute() {
                                if (!super._execute()) {
                                    return false;
                                }
                                document.getBlockStore().refresh(false);
                                return true;
                            }
                            
                            @Override
                            protected boolean _rollBack() {
                                if (!super._rollBack()) {
                                    return false;
                                }
                                document.getBlockStore().refresh(false);
                                return true;
                            }
                            
                        });
                    }
                    
                }
            ),

            new IconButtonSelectDialog.Item(
                text("ui.swing.main.tables.table_operations.empty_table"),
                IconLoader.loadIcon("board-empty"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        // TODO: convert to showConfirmDialog
                        int answer = OptionPaneUtil.showOptionDialog(
                            frame,
                            text("ui.swing.main.tables.table_operations.empty_table.sure"),
                            text("ui.swing.main.tables.table_operations.empty_table.confirm"),
                            JOptionPane.QUESTION_MESSAGE,
                            new String[]{
                                text("ui.swing.main.tables.table_operations.empty_table.cancel"),
                                text("ui.swing.main.tables.table_operations.empty_table.empty"),
                            },
                            new ButtonPurpose[] {
                                ButtonPurpose.NONE,
                                ButtonPurpose.DANGER,
                            },
                            text("ui.swing.main.tables.table_operations.empty_table.empty")
                        );
                        if (answer == 1) {
                            history().addAndExecute(new ClearBoardCommand(document, board));
                        }
                    }
                    
                }
            ),

            new IconButtonSelectDialog.Item(
                text("ui.swing.main.tables.table_operations.delete_table"),
                IconLoader.loadIcon("board-delete"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        // TODO: convert to showConfirmDialog
                        int answer = OptionPaneUtil.showOptionDialog(
                            frame,
                            text("ui.swing.main.tables.table_operations.delete_table.sure"),
                            text("ui.swing.main.tables.table_operations.delete_table.confirm"),
                            JOptionPane.QUESTION_MESSAGE,
                            new String[]{
                                text("ui.swing.main.tables.table_operations.delete_table.cancel"),
                                text("ui.swing.main.tables.table_operations.delete_table.delete"),
                            },
                            new ButtonPurpose[] {
                                ButtonPurpose.NONE,
                                ButtonPurpose.DANGER,
                            },
                            text("ui.swing.main.tables.table_operations.delete_table.delete")
                        );
                        if (answer == 1) {
                            if (boardViewImageProvider.getBoard() == board) {
                                boardViewImageProvider.setBoard(null);
                            }
                            RemovalHelper removalHelper = new RemovalHelper(document);
                            Command command = removalHelper.createRemoveCommandFor(
                                board,
                                "ui.swing.main.command.delete_table"
                            );
                            history().addAndExecute(command);
                        }
                    }
                    
                }
            ),

        };
        
        IconButtonSelectDialog boardOperationsSelectDialog = new IconButtonSelectDialog(frame, text("ui.swing.main.tables.table_operations"), items);
        operationsSelectWrapper.set(boardOperationsSelectDialog);
        boardOperationsSelectDialog.run();
    }
    
    private JPanel createItemContainer() {
        JPanel container = new JPanel();
        container.setBackground(java.awt.Color.WHITE);
        container.setBorder(BorderFactory.createCompoundBorder(
            new EmptyBorder(5, 5, 0, 5),
            new LineBorder(new java.awt.Color(40, 40, 40))
        ));
        container.setLayout(new BorderLayout());
        
        PaintedPanel box = new PaintedPanel();
        box.putClientProperty(COMPONENT_ID_PROPERTY, "box");
        box.setLayout(new BorderLayout());
        box.setBorder(new EmptyBorder(4, 4, 4, 4));
        box.setBackground(defaultBoxColor);
        container.add(box, BorderLayout.CENTER);
        
        JLabel itemLabel = new JLabel();
        itemLabel.putClientProperty(COMPONENT_ID_PROPERTY, "item-label");
        itemLabel.putClientProperty("html.disable", true);
        box.add(itemLabel, BorderLayout.PAGE_START);
        
        FlowLayout buttonBarLayout = new FlowLayout(FlowLayout.RIGHT);
        buttonBarLayout.setVgap(0);
        buttonBarLayout.setHgap(1);
        
        JPanel buttonBar = new JPanel();
        buttonBar.putClientProperty(COMPONENT_ID_PROPERTY, "button-bar");
        buttonBar.setLayout(buttonBarLayout);
        buttonBar.setOpaque(false);
        buttonBar.setBorder(new EmptyBorder(2, 0, 0, 0));
        box.add(buttonBar, BorderLayout.PAGE_END);
        
        return container;
    }
    
    private void onMouseMovedOnTableView(int mouseX, int mouseY) {
        Board board = boardViewImageProvider.getBoard();
        if (board==null) {
            boardViewImagePanel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            return;
        }
        
        int pw = boardViewImagePanel.getWidth();
        int ph = boardViewImagePanel.getHeight();
        BufferedImage image = boardViewImageProvider.getImage();
        int iw = image.getWidth();
        int ih = image.getHeight();
        int top = Math.max(0, (ph-ih)/2);
        int left = Math.max(0, (pw-iw)/2);
        int imageTop = mouseY-top;
        int imageLeft = mouseX-left;
        ActivityFlowImageRenderer.PositionedActivityEntry activityEntry = boardViewImageProvider.getActivityEntryAtPosition(imageLeft, imageTop);
        if (dragTargetRectangles!=null) {
            // FIXME: drag
        } else if (activityEntry!=null) {
            boardViewImagePanel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            boardViewHoverRectangle = activityEntry.rectangle;

            List<Rectangle> newSimilarMarkedRectangles = new ArrayList<Rectangle>();
            Board.Entry boardEntry = board.search(activityEntry.activity);
            if (boardEntry != null) {
                Block block = boardEntry.getBlock();
                PeriodSet periods = block.getActivityManager().getActivityPeriods(activityEntry.activity);
                List<Resource> classes = activityEntry.activity.getResourceManager().getResources(Resource.Type.CLASS);
                Set<Tag> tags = activityEntry.activity.getTagManager().getTags(Tag.Type.SUBJECT);
                List<ActivityFlowImageRenderer.PositionedActivityEntry> activityEntries = boardViewImageProvider.getActivityEntries();
                if (activityEntries.size() < 200) {
                    for (ActivityFlowImageRenderer.PositionedActivityEntry otherActivityEntry: boardViewImageProvider.getActivityEntries()) {
                        boolean isSimilar = false;
                        if (activityEntry.activity == otherActivityEntry.activity) {
                            isSimilar = true;
                        } else {
                            Board.Entry otherBoardEntry = board.search(otherActivityEntry.activity);
                            if (otherBoardEntry != null) {
                                Block otherBlock = board.search(otherActivityEntry.activity).getBlock();
                                if (!Collections.disjoint(periods, otherBlock.getActivityManager().getActivityPeriods(otherActivityEntry.activity))) {
                                    List<Resource> otherClasses = otherActivityEntry.activity.getResourceManager().getResources(Resource.Type.CLASS);
                                    Set<Tag> otherTags = otherActivityEntry.activity.getTagManager().getTags(Tag.Type.SUBJECT);
                                    if (!Collections.disjoint(classes, otherClasses) && !Collections.disjoint(tags, otherTags)) {
                                        isSimilar = true;
                                    }
                                }
                            }
                        }
                        if (isSimilar) {
                            newSimilarMarkedRectangles.add(otherActivityEntry.rectangle);
                        }
                    }
                }
            }
            similarMarkedRectangles = newSimilarMarkedRectangles;
        } else {
            boardViewImagePanel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            boardViewHoverRectangle = null;
            similarMarkedRectangles = null;
        }
        boardViewImagePanel.repaint();
        boardViewImagePanel.revalidate();
    }
    
    private void tryPutBlock(Board board, Block block) {
        GeneralWrapper<Board> boardWrapper = new GeneralWrapper<Board>(board);
        GeneralWrapper<Block> blockWrapper = new GeneralWrapper<Block>(block);

        Time firstTime = null;
        if (block!=null) {
            for (TimingSet.TimeEntry timeEntry : block.getCalculatedTimingSet()) {
                firstTime = timeEntry.getTime();
                break;
            }
        }
        GeneralWrapper<Time> timeWrapper = new GeneralWrapper<Time>(firstTime);
        BoardBlockTimeDialog dialog = new BoardBlockTimeDialog(frame, document, boardWrapper, blockWrapper, timeWrapper);
        dialog.run();
        
        if (dialog.getResult()==EditDialog.RESULT_OK) {
            Board newBoard = boardWrapper.get();
            Block newBlock = blockWrapper.get();
            Time time = timeWrapper.get();
            
            Board.Entry originalEntry = newBoard.search(newBlock);
            
            boolean move = false;
            
            if (originalEntry!=null) {
                int answer = OptionPaneUtil.showConfirmDialog(
                    frame, text("ui.swing.main.blocks.put_block.already_put"), ButtonPurpose.WARNING
                );
                if (answer==JOptionPane.OK_OPTION) {
                    move = true;
                } else {
                    return;
                }
            }
            if (newBoard.conflictsWithBlockAt(newBlock, time, false)) {
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(
                    frame,
                    text("ui.swing.main.blocks.put_block.conflict.sure"),
                    text("ui.swing.main.blocks.put_block.conflict.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.main.blocks.put_block.conflict.cancel"),
                        text("ui.swing.main.blocks.put_block.conflict.move"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.ACTION,
                    },
                    text("ui.swing.main.blocks.put_block.conflict.move")
                );
                if (answer!=1) {
                    return;
                }
            }
            
            hu.webarticum.chm.Command command;
            if (move) {
                command = new MoveBlockOnTableCommand(document, newBoard, newBlock, time);
            } else {
                command = new PutBlockToTableCommand(document, newBoard, newBlock, time);
            }
            history().addAndExecute(command);
        }
    }
    
    private void refreshSummaryDocumentLabel() {
        summaryDocumentLabel.setText(document.getLabel());
    }
    
    private void refreshSummaryStats() {
        summaryPeriodCountLabel.setText("" + document.getPeriodStore().size());
        summaryTimingSetCountLabel.setText("" + document.getTimingSetStore().size());
        Document.TagStore tagStore = document.getTagStore();
        summarySubjectCountLabel.setText("" + tagStore.getAll(Tag.Type.SUBJECT).size());
        summaryLanguageCountLabel.setText("" + tagStore.getAll(Tag.Type.LANGUAGE).size());
        summaryOtherTagCountLabel.setText("" + tagStore.getAll(Tag.Type.OTHER).size());
        Document.ResourceStore resourceStore = document.getResourceStore();
        summaryClassCountLabel.setText("" + resourceStore.getAll(Resource.Type.CLASS).size());
        summaryPersonCountLabel.setText("" + resourceStore.getAll(Resource.Type.PERSON).size());
        summaryLocaleCountLabel.setText("" + resourceStore.getAll(Resource.Type.LOCALE).size());
        summaryObjectCountLabel.setText("" + resourceStore.getAll(Resource.Type.OBJECT).size());
        summaryOtherResourceCountLabel.setText("" + resourceStore.getAll(Resource.Type.OTHER).size());
        summaryBlockCountLabel.setText("" + document.getBlockStore().size());
        summaryTableCountLabel.setText("" + document.getBoardStore().size());
    }

    private void refreshBlockListPanelAsynchronous() {
        SwingUtilities.invokeLater(new Runnable() {
            
            @Override
            public void run() {
                blockStoreListPanel.refresh(true);
            }
            
        });
    }
    
    private void refreshTableViewAsynchronous() {
        Thread refreshTableViewThread = new Thread(new Runnable() {

            @Override
            public void run() {
                if (boardViewImagePanel==null) {
                    return;
                }
                
                // XXX
                onMouseMovedOnTableView(-1, -1);
                
                synchronized (MainWindow.this) {
                    boardViewImagePanel.setInProgress(true);
                    boardViewImageProvider.expire();

                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            
                            @Override
                            public void run() {
                                boardViewImagePanel.repaint();
                            }
                            
                        });
                    } catch (InvocationTargetException e) {
                    } catch (InterruptedException e) {
                    }
                    
                    BufferedImage image = boardViewImageProvider.getImage();
                    final int width = image.getWidth();
                    final int height = image.getHeight();
                    
                    SwingUtilities.invokeLater(new Runnable() {
                        
                        @Override
                        public void run() {
                            boardViewImagePanel.setInProgress(false);
                            boardViewImagePanel.setMinimumSize(new Dimension(width, height));
                            boardViewImagePanel.setPreferredSize(new Dimension(width, height));
                            boardViewImagePanel.revalidate();
                            boardViewImagePanel.repaint();
                        }
                        
                    });
                    
                }
            }
            
        });
        refreshTableViewThread.start();
    }
    
    private void addColorSchemes() {
        for (Pair<String, ActivityFlowImageRenderer.ColorScheme> pair: new BoardImageColorSchemeListBuilder().getColorSchemes()) {
            colorSchemeComboBox.addMultilingualItem(pair.getLeft(), pair.getRight());
        }
    }
    
    public boolean tryExit() {
        if (!handleUnsavedDocument()) {
            return false;
        }
        
        System.exit(0);
        return true;
    }

    public boolean tryLeave() {
        if (!handleUnsavedDocument()) {
            return false;
        }
        
        initNoDocument();
        return true;
    }
    
    public boolean trySave() {
        if (associatedFile==null) {
            return trySaveAs();
        } else {
            String extension = FileUtil.getExtension(associatedFile);
            DocumentIo documentIo = Application.instance().getDocumentIoQueue().getByExtension(extension);
            try {
                documentIo.save(document, associatedFile);
            } catch (Exception e) {
                OptionPaneUtil.showMessageDialog(
                    frame,
                    text("ui.swing.main.save.failed")+"\n\n"+e.getMessage()
                );
                return false;
            }
            registerRecentFile(associatedFile.getAbsolutePath());
            setSaved();
            GlobalUiObjects.getNotificationQueue().send(
                text("ui.swing.main.save.success.title"),
                text("ui.swing.main.save.success.description"),
                NotificationQueue.Notification.TYPE.SUCCESS
            );
            return true;
        }
    }
    
    public boolean trySaveAs() {
        AutoExtensionFileChooser fileChooser = new AutoExtensionFileChooser();
        if (associatedFile!=null) {
            fileChooser.setSelectedFile(associatedFile);
        }
        DocumentIoQueue documentIoQueue = Application.instance().getDocumentIoQueue();
        for (DocumentIo documentIo: documentIoQueue) {
            fileChooser.addChoosableFileFilter(documentIoFileFilter(documentIo));
        }
        int chooseResult = fileChooser.showSaveDialog(frame);
        if (chooseResult!=JFileChooser.APPROVE_OPTION) {
            return false;
        }
        
        File file = fileChooser.getAutoSelectedFile();
        String extension = fileChooser.getAutoSelectedExtension();
        
        DocumentIo documentIo = documentIoQueue.getByExtension(extension);
        if (documentIo==null) {
            OptionPaneUtil.showMessageDialog(frame, text("ui.swing.main.save_as.unsupported_format"));
            return false;
        }
        if (file.exists()) {
            int confirmResult = OptionPaneUtil.showConfirmDialog(frame, text("ui.swing.main.save_as.existing_file"), ButtonPurpose.DANGER);
            if (confirmResult!=JOptionPane.OK_OPTION) {
                return false;
            }
        }
        try {
            documentIo.save(document, file);
        } catch (Exception e) {
            e.printStackTrace();
            OptionPaneUtil.showMessageDialog(frame, text("ui.swing.main.save_as.failed"));
            return false;
        }
        associatedFile = file;
        registerRecentFile(associatedFile.getAbsolutePath());
        setSaved();
        GlobalUiObjects.getNotificationQueue().send(
            text("ui.swing.main.save_as.success.title"),
            text("ui.swing.main.save_as.success.description"),
            NotificationQueue.Notification.TYPE.SUCCESS
        );
        return true;
    }

    public boolean tryNew() {
        if (!handleUnsavedDocument()) {
            return false;
        }
        
        associatedFile = null;
        
        IconButtonSelectDialog.Item[] items = new IconButtonSelectDialog.Item[] {
            
            new IconButtonSelectDialog.Item(
                text("ui.swing.main.newdocument.empty.label"),
                text("ui.swing.main.newdocument.empty.tooltip"),
                IconLoader.loadIcon("new_empty"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        initDocument(new Document(text("ui.swing.main.newdocument.new_document_label")));
                    }
                    
                }
            ),

            new IconButtonSelectDialog.Item(
                text("ui.swing.main.newdocument.wizard.label"),
                text("ui.swing.main.newdocument.wizard.tooltip"),
                IconLoader.loadIcon("new_wizard"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        NewDocumentWizardDialog dialog = new NewDocumentWizardDialog(frame);
                        dialog.run();
                        Document resultDocument = dialog.getResultDocument();
                        if (resultDocument != null) {
                            initDocument(resultDocument);
                            setUnsaved();
                            refreshTitle();
                        }
                    }
                    
                }
            ),

            new IconButtonSelectDialog.Item(
                text("ui.swing.main.newdocument.template.label"),
                text("ui.swing.main.newdocument.template.tooltip"),
                IconLoader.loadIcon("new_template"),
                new Runnable() {
                    
                    @Override
                    public void run() {
                        Map<String, DocumentFactory> labelFactoryMap = new TreeMap<String, DocumentFactory>(new Labeled.StringComparator());
                        for (CommonExtensions.Template template: extensions(CommonExtensions.Template.class)) {
                            labelFactoryMap.put(template.getLabel(), template.getDocumentFactory());
                        }
                        
                        if (labelFactoryMap.isEmpty()) {
                            // XXX
                            return;
                        }
                        
                        String[] labelArray = new String[labelFactoryMap.size()];
                        labelFactoryMap.keySet().toArray(labelArray);
                        String defaultLabel = labelArray[0];

                        String option = (String)OptionPaneUtil.showInputDialog(
                            frame,
                            text("ui.swing.main.newdocument.template.new.title"),
                            text("ui.swing.main.newdocument.template.new.description"),
                            JOptionPane.PLAIN_MESSAGE,
                            labelArray,
                            defaultLabel,
                            ButtonPurpose.SAVE
                        );

                        if (option != null) {
                            loadDocument(labelFactoryMap.get(option));
                        }
                    }
                    
                }
            ),
            
        };
        IconButtonSelectDialog dialog = new IconButtonSelectDialog(frame, text("ui.swing.main.newdocument.title"), items);
        dialog.run();
        
        return dialog.isCancelled();
    }

    public boolean tryOpen() {
        if (!handleUnsavedDocument()) {
            return false;
        }
        
        final JFileChooser fileChooser = new JFileChooser();
        FileFilter defaultFileFilter = null;
        for (DocumentIo documentIo: Application.instance().getDocumentIoQueue()) {
            FileFilter fileFilter = documentIoFileFilter(documentIo);
            if (defaultFileFilter == null) {
                defaultFileFilter = fileFilter;
            }
            fileChooser.addChoosableFileFilter(fileFilter);
        }
        fileChooser.setFileFilter(defaultFileFilter);
        int chooseResult = fileChooser.showOpenDialog(frame);
        if (chooseResult!=JFileChooser.APPROVE_OPTION) {
            return false;
        }
        File file = fileChooser.getSelectedFile();
        if (loadDocumentFromFile(file)) {
            associatedFile = file;
            return true;
        } else {
            return false;
        }
    }

    // TODO: separate window
    public boolean tryOpenDemo() {
        if (!handleUnsavedDocument()) {
            return false;
        }
        
        Map<String, DocumentFactory> labelFactoryMap = new TreeMap<String, DocumentFactory>(new Labeled.StringComparator());
        for (CommonExtensions.Demo demo: extensions(CommonExtensions.Demo.class)) {
            labelFactoryMap.put(demo.getLabel(), demo.getDocumentFactory());
        }
        
        if (labelFactoryMap.isEmpty()) {
            // XXX
            return false;
        }
        
        String[] labelArray = new String[labelFactoryMap.size()];
        labelFactoryMap.keySet().toArray(labelArray);
        String defaultLabel = labelArray[0];

        String option = (String)OptionPaneUtil.showInputDialog(
            frame,
            text("ui.swing.main.newdemo.title"),
            text("ui.swing.main.newdemo.description"),
            JOptionPane.PLAIN_MESSAGE,
            labelArray,
            defaultLabel,
            ButtonPurpose.ACTION
        );

        if (option != null) {
            return loadDocument(labelFactoryMap.get(option));
        }
        
        return false;
    }
    
    private boolean handleUnsavedDocument() {
        if (unsavedState) {
            int answer = OptionPaneUtil.showOptionDialog(
                frame,
                text("ui.swing.main.unsaved.sure"),
                text("ui.swing.main.unsaved.confirm"),
                JOptionPane.QUESTION_MESSAGE,
                new String[]{
                        text("ui.swing.main.unsaved.cancel"),
                    text("ui.swing.main.unsaved.nosave"),
                    text("ui.swing.main.unsaved.save"),
                },
                new ButtonPurpose[] {
                    ButtonPurpose.ABORT,
                    ButtonPurpose.DANGER,
                    ButtonPurpose.SAVE,
                },
                text("ui.swing.main.unsaved.save")
            );
            if (answer==2) {
                return trySave();
            } else if (answer==1) {
                return true;
            } else {
                return false;
            }
        }
        
        return true;
    }
    
    private FileFilter documentIoFileFilter(DocumentIo documentIo) {
        return new FileNameExtensionFilter(documentIo.getLabel(), documentIo.getExtensions());
    }
    
    private boolean tryImport() {
        ResourceImportDialog dialog = new ResourceImportDialog(frame, document);
        dialog.run();
        return (dialog.getResult() == EditDialog.RESULT_OK);
    }
    
    private boolean tryAutoPut() {
        Board board = null;
        if (document.getBoardStore().size()>0) {
            board = document.getBoardStore().getAll().get(0);
        }
        AutoPutDialog dialog = new AutoPutDialog(frame, document, board);
        dialog.run();
        
        if (dialog.getResult() == EditDialog.RESULT_OK) {
            refreshTableViewAsynchronous();
            return true;
        } else {
            return false;
        }
    }
    
    private void runBoardCheck(Board board) {
        (new CheckBoardDialog(frame, document, board)).run();
    }

    private boolean loadDocumentFromFile(final File file) {
        try {
            loadDocumentFromUrl(file.toURI().toURL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        }
        registerRecentFile(file.getAbsolutePath());
        return true;
    }

    private boolean loadDocumentFromUrl(URL url) {
        return loadDocument(new UrlDocumentFactory(url));
    }

    public boolean loadDocument(final DocumentFactory factory) {
        final GeneralWrapper<Boolean> isLoadedWrapper = new GeneralWrapper<Boolean>(false);
        
        SwingWorkerProcessDialog dialog = new SwingWorkerProcessDialog(frame, text("ui.swing.main.load_document.load"), true) {
            
            private static final long serialVersionUID = 1L;
            
            @Override
            protected Worker createWorker() {
                return new SwingWorkerProcessDialog.Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        final Document document = factory.createDocument();
                        
                        if (document == null) {
                            publish(new Command(){

                                @Override
                                public void run() {
                                    OptionPaneUtil.showMessageDialog(frame, text("ui.swing.main.load_document.failed"));
                                }
                                
                            });
                            return;
                        }


                        publish(new Command(){

                            @Override
                            public void run() {
                                initDocument(document);
                            }
                            
                        });
                        
                        isLoadedWrapper.set(true);
                    }

                    @Override
                    public void _rollBack() {
                    }
                    
                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        if (command instanceof TerminatedCommand) {
                            closeDialog();
                        }
                    }
                    
                };
            }
            
        };

        
        dialog.run();
        
        return isLoadedWrapper.get();
    }
    
    private void registerRecentFile(String path) {
        RecentFilesManager recentFilesManager = Application.instance().getRecentFilesManager();
        recentFilesManager.register(path);
        refreshRecentFilesMenuItems(path);
    }
    
    private class MainFrame extends JFrame implements MultilingualComponent, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public void reloadLanguageTexts() {
            String langCode = texts().getLanguage();
            this.setLocale(new Locale(langCode));
        }
        
    }
    
    private class BoardViewImagePanel extends JPanel implements MultilingualComponent {

        private static final long serialVersionUID = 1L;
        
        private volatile boolean inProgress = false;
        
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            
            Graphics2D g2 = (Graphics2D)g;
            
            int pw = getWidth();
            int ph = getHeight();
            BufferedImage image = getImage();
            int iw = image.getWidth();
            int ih = image.getHeight();
            int top = Math.max(0, (ph-ih)/2);
            int left = Math.max(0, (pw-iw)/2);
            g2.setColor(java.awt.Color.WHITE);
            g2.fillRect(0, 0, pw, ph);
            g2.drawImage(image, left, top, left+iw, top+ih, 0, 0, iw, ih, null);

            if (similarMarkedRectangles != null) {
                for (Rectangle rectangle: similarMarkedRectangles) {
                    java.awt.Color color = rectangle == boardViewHoverRectangle ?
                        new java.awt.Color(255, 255, 255, 40):
                        new java.awt.Color(20, 20, 20, 40)
                    ;
                    g2.setColor(color);
                    g2.fillRect(rectangle.x + left, rectangle.y + top, rectangle.width, rectangle.height);
                    
                    if (rectangle != boardViewHoverRectangle) {
                        Stroke previousStroke = g2.getStroke();
                        g2.setStroke(new BasicStroke(3));
                        g2.setColor(new java.awt.Color(40, 50, 90, 200));
                        g2.drawRect(rectangle.x + left - 2, rectangle.y + top - 2, rectangle.width + 4, rectangle.height + 4);
                        g2.setStroke(previousStroke);
                    }
                }
            }
            
            if (boardViewHoverRectangle != null) {
                int rectX = (int)boardViewHoverRectangle.getX() + left;
                int rectY  = (int)boardViewHoverRectangle.getY() + top;
                int rectWidth = (int)boardViewHoverRectangle.getWidth();
                int rectHeight = (int)boardViewHoverRectangle.getHeight();

                int lineSize = 3;
                
                g2.setStroke(new BasicStroke(lineSize));
                
                g2.setColor(new java.awt.Color(0, 0, 0, 100));
                g2.drawLine(rectX + lineSize, rectY + rectHeight + lineSize, rectX + rectWidth, rectY + rectHeight + lineSize);
                g2.drawLine(rectX + rectWidth + lineSize, rectY + lineSize, rectX + rectWidth + lineSize, rectY + rectHeight + lineSize);

                g2.setColor(new java.awt.Color(0xDE6600));
                g2.drawRect(rectX, rectY, rectWidth, rectHeight);
            }
            
            if (dragTargetRectangles != null) {
                for (Map.Entry<Time, DragTargetRectangleEntry> entry: dragTargetRectangles.entrySet()) {
                    DragTargetRectangleEntry dragTargetRectangleEntry = entry.getValue();
                    Rectangle rectangle = new Rectangle(
                        dragTargetRectangleEntry.rectangle.x + left,
                        dragTargetRectangleEntry.rectangle.y + top,
                        Math.min(400, dragTargetRectangleEntry.rectangle.width),
                        dragTargetRectangleEntry.rectangle.height
                    );

                    int alpha = dragTargetRectangleEntry.hover ? 255 : 190;
                    java.awt.Color color = dragTargetRectangleEntry.free ? new java.awt.Color(30, 180, 10, alpha) : new java.awt.Color(140, 0, 0, alpha);
                    java.awt.Color opaqueColor = dragTargetRectangleEntry.free ? new java.awt.Color(30, 180, 10) : new java.awt.Color(140, 0, 0);
                    
                    new StripsPatternDrawer().drawStripes(g2, rectangle, color, 3, 5);

                    g2.setColor(color);
                    
                    Stroke currentStroke = g2.getStroke();
                    g2.setStroke(new BasicStroke(4));
                    g2.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                    g2.setStroke(currentStroke);

                    g2.setColor(opaqueColor);
                    g2.fillRect(rectangle.x - 2, rectangle.y - 2, 30, 20);
                }
            }
        }
        
        public BufferedImage getImage() {
            if (inProgress) {
                Image iconImage = IconLoader.loadIcon("board_loading").getImage();
                
                int width = iconImage.getWidth(null);
                int height = iconImage.getHeight(null);
                
                BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = (Graphics2D)image.getGraphics();
                g.setColor(java.awt.Color.WHITE);
                g.fillRect(0, 0, width, height);
                g.drawImage(iconImage, 0, 0, null);
                g.dispose();
                
                return image;
            } else {
                return boardViewImageProvider.getImage();
            }
        }
        
        @Override
        public void reloadLanguageTexts() {
            refreshTableViewAsynchronous();
        }
        
        public void setInProgress(boolean inProgress) {
            this.inProgress = inProgress;
        }
        
    }
    
    public class ListItemStringifier<T extends Labeled> {
        
        public String stringify(T item) {
            return item.getLabel();
        }
        
    }
    
    public interface ListItemValidator<T extends Labeled> {
        
        public boolean validate(T item);
        
        public static class NoFilter<T extends Labeled> implements ListItemValidator<T> {

            @Override
            public boolean validate(T item) {
                return true;
            }
            
        }
        
    }
    
    public class BoardViewImageProvider {
        
        private BufferedImage image = null;
        
        private Board board = null;
        
        private ActivityFlowImageRenderer.Result rendererResult = null;

        public void setBoard(Board board) {
            this.board = board;
            expire();
        }

        public Board getBoard() {
            return board;
        }
        
        public BufferedImage getImage() {
            if (image==null) {
                if (board==null) {
                    Image iconImage = IconLoader.loadIcon("noboard").getImage();
                    
                    image = new BufferedImage(iconImage.getWidth(null), iconImage.getHeight(null), BufferedImage.TYPE_INT_RGB);
                    Graphics2D g = (Graphics2D)image.getGraphics();
                    g.drawImage(iconImage, 0, 0, null);
                    g.dispose();
                    
                    return image;
                } else {
                    Object colorAspect = boardBlockFilterPanel.getColorAspect();
                    ActivityFlowImageRenderer.ColorScheme colorScheme = colorSchemeComboBox.getSelectedData();
                    
                    ActivityFlowImageRenderer renderer = new ActivityFlowImageRenderer();
                    renderer.setColorAspect(colorAspect);
                    renderer.setColorScheme(colorScheme);
                    Period period = boardBlockFilterPanel.getPeriod();
                    ActivityFilter activityFilter = boardBlockFilterPanel.getActivityFilter();
                    ActivityFlow baseActivityFlow = period == null ? board.toActivityFlow() : board.toActivityFlow(period);
                    ActivityFlow activityFlow = baseActivityFlow.filter(new ActivityFlow.ActivityEntryFilter(activityFilter));
                    
                    ActivityFlowImageRenderer.Result result = renderer.render(activityFlow);
                    image = result.image;
                    rendererResult = result;
                }
            }
            return image;
        }

        public ActivityFlowImageRenderer.Result getRendererResult() {
            return rendererResult;
        }
        
        public void reset() {
            board = null;
            expire();
        }
        
        public void expire() {
            image = null;
            rendererResult = null;
        }
        
        public ActivityFlowImageRenderer.PositionedActivityEntry getActivityEntryAtPosition(int x, int y) {
            if (rendererResult!=null) {
                for (int i = rendererResult.positionedActivityEntries.size() - 1; i >= 0; i--) {
                    ActivityFlowImageRenderer.PositionedActivityEntry positionedActivityEntry = rendererResult.positionedActivityEntries.get(i);
                    if (positionedActivityEntry.rectangle.contains(x, y)) {
                        return positionedActivityEntry;
                    }
                }
            }
            return null;
        }
        
        public List<ActivityFlowImageRenderer.PositionedActivityEntry> getActivityEntries() {
            List<ActivityFlowImageRenderer.PositionedActivityEntry> result = new ArrayList<ActivityFlowImageRenderer.PositionedActivityEntry>();
            if (rendererResult != null) {
                result.addAll(rendererResult.positionedActivityEntries); 
            }
            return result;
        }
        
    }
    
    private static class DragTargetRectangleEntry {
        
        final Rectangle rectangle;
        
        final boolean free;
        
        boolean hover = false;
        
        DragTargetRectangleEntry(Rectangle rectangle, boolean free) {
            this.rectangle = rectangle;
            this.free = free;
        }
        
    }
    
    public static class ClearBoardCommand extends AbstractCommand {
        
        private final Document document;
        
        private final Board board;
        
        private BoardMemento oldMemento = null;
        
        public ClearBoardCommand(Document document, Board board) {
            this.document = document;
            this.board = board;
        }
        
        @Override
        protected boolean _rollBack() {
            oldMemento.apply(board);
            document.getBlockStore().refresh(false);
            return true;
        }
        
        @Override
        protected boolean _execute() {
            if (oldMemento == null) {
                oldMemento = new BoardMemento(board);
            }
            board.clear();
            document.getBlockStore().refresh(false);
            return true;
        }

        @Override
        public String toString() {
            return text("ui.swing.main.command.empty_table");
        }
        
    }

    public static class PutBlockToTableCommand extends AbstractCommand {
        
        private final Document document;
        
        private final Board board;
        
        private final Block block;
        
        private final Time time;
        
        public PutBlockToTableCommand(Document document, Board board, Block block, Time time) {
            this.document = document;
            this.board = board;
            this.block = block;
            this.time = time;
        }
        
        @Override
        protected boolean _rollBack() {
            board.remove(block);
            document.getBlockStore().refresh(block);
            return true;
        }
        
        @Override
        protected boolean _execute() {
            board.add(block, time);
            document.getBlockStore().refresh(block);
            return true;
        }

        @Override
        public String toString() {
            return text("ui.swing.main.command.put_block");
        }
        
    }

    public static class MoveBlockOnTableCommand extends AbstractCommand {
        
        private final Document document;
        
        private final Board board;
        
        private final Block block;
        
        private final Time time;
        
        private Time oldTime;
        
        public MoveBlockOnTableCommand(Document document, Board board, Block block, Time time) {
            this.document = document;
            this.board = board;
            this.block = block;
            this.time = time;
        }
        
        @Override
        protected boolean _rollBack() {
            board.remove(block);
            board.add(block, oldTime);
            document.getBlockStore().refresh(block);
            return true;
        }
        
        @Override
        protected boolean _execute() {
            oldTime = board.search(block).getTime();
            board.remove(block);
            board.add(block, time);
            document.getBlockStore().refresh(block);
            return true;
        }

        @Override
        public String toString() {
            return text("ui.swing.main.command.move_block");
        }
        
    }

    public static class RemoveBlockFromTableCommand extends AbstractCommand {
        
        private final Document document;
        
        private final Board board;
        
        private final Block block;

        private Time time = null;
        
        public RemoveBlockFromTableCommand(Document document, Board board, Block block) {
            this.document = document;
            this.board = board;
            this.block = block;
        }
        
        @Override
        protected boolean _rollBack() {
            board.add(block, time);
            document.getBlockStore().refresh(block);
            return true;
        }
        
        @Override
        protected boolean _execute() {
            time = board.search(block).getTime();
            board.remove(block);
            document.getBlockStore().refresh(block);
            return true;
        }

        @Override
        public String toString() {
            return text("ui.swing.main.command.remove_block");
        }
        
    }

}

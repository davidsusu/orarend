package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.ItemSelectable;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.event.EventListenerList;

import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.plugins.defaultswingui.util.ColorUtil;


public class ColorButton extends JPanel implements ItemSelectable {

    private static final long serialVersionUID = 1L;

    private JFrame parent;
    
    private hu.webarticum.aurora.core.model.Color color;
    
    private EventListenerList listenerList = new EventListenerList();
    
    private boolean isFocused = false;
    
    public ColorButton(JFrame parent) {
        this.parent = parent;
        this.color = hu.webarticum.aurora.core.model.Color.WHITE;
        init();
    }
    
    public ColorButton(JFrame parent, hu.webarticum.aurora.core.model.Color color) {
        this.parent = parent;
        this.color = color;
        init();
    }

    public ColorButton(JFrame parent, java.awt.Color color) {
        this.parent = parent;
        this.color = ColorUtil.fromAwtColor(color);
        init();
    }
    
    private void init() {
        setPreferredSize(new Dimension(25, 25));
        setMinimumSize(new Dimension(25, 25));
        setMaximumSize(new Dimension(25, 25));
        setSize(new Dimension(25, 25));
        setBorder(new LineBorder(java.awt.Color.BLACK));
        setCursor(new Cursor(Cursor.HAND_CURSOR));
        setFocusable(true);
        redraw();
        addMouseListener(new MouseListener() {
            
            @Override
            public void mouseReleased(MouseEvent ev) {}
            
            @Override
            public void mousePressed(MouseEvent ev) {
                requestFocus();
            }
            
            @Override
            public void mouseExited(MouseEvent ev) {}
            
            @Override
            public void mouseEntered(MouseEvent ev) {}
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                openDialog();
            }
        });
        addFocusListener(new FocusListener() {
            
            @Override
            public void focusGained(FocusEvent ev) {
                isFocused = true;
                redraw();
            }

            @Override
            public void focusLost(FocusEvent ev) {
                isFocused = false;
                redraw();
            }
            
        });
        addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {}
            
            @Override
            public void keyReleased(KeyEvent ev) {}
            
            @Override
            public void keyPressed(KeyEvent ev) {
                if (ev.getKeyCode()==KeyEvent.VK_SPACE) {
                    openDialog();
                }
            }
        });
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        setFocusable(enabled);
        redraw();
    }
    
    private void redraw() {
        boolean enabled = isEnabled();
        if (isFocused) {
            setBorder(BorderFactory.createDashedBorder(ColorUtil.toAwtColor(this.color.getBlackWhiteContrastColor()), 3, 2));
        } else {
            setBorder(new LineBorder(enabled ? java.awt.Color.BLACK : java.awt.Color.GRAY));
        }
        Color color = this.color;
        if (!enabled) {
            color = getDisabledColor(color);
        }
        setBackground(ColorUtil.toAwtColor(color));
        repaint();
    }
    
    private Color getDisabledColor(Color color) {
        int newRed = (color.getRed() + 370) / 3;
        int newGreen = (color.getGreen() + 370) / 3;
        int newBlue = (color.getBlue() + 370) / 3;
        return new Color(newRed, newGreen, newBlue);
    }
    
    public void setSelectedColor(hu.webarticum.aurora.core.model.Color color) {
        Color oldColor = this.color;
        this.color = color;
        redraw();
        fireChange(oldColor, color);
    }
    
    public hu.webarticum.aurora.core.model.Color getSelectedColor() {
        return color;
    }

    public void setSelectedAwtColor(java.awt.Color color) {
        setSelectedColor(ColorUtil.fromAwtColor(color));
    }
    
    public java.awt.Color getSelectedAwtColor() {
        return ColorUtil.toAwtColor(color);
    }
    
    public void openDialog() {
        if (isEnabled()) {
            java.awt.Color newAwtColor = JColorChooser.showDialog(
                parent, text("ui.swing.widget.colorbutton.choose"), ColorUtil.toAwtColor(color)
            );
            if (newAwtColor!=null) {
                Color newColor = ColorUtil.fromAwtColor(newAwtColor);
                setSelectedColor(newColor);
            }
        }
    }
    
    @Override
    public Object[] getSelectedObjects() {
        return new Object[]{color};
    }

    @Override
    public void addItemListener(ItemListener listener) {
        listenerList.add(ItemListener.class, listener);
    }

    @Override
    public void removeItemListener(ItemListener listener) {
        listenerList.remove(ItemListener.class, listener);
    }

    // TODO: deselect...
    private void fireChange(Color oldColor, Color newColor) {
        ItemListener[] listeners = listenerList.getListeners(ItemListener.class);
        ItemEvent deselectEvent = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, oldColor, ItemEvent.DESELECTED);
        for (ItemListener listener: listeners) {
            listener.itemStateChanged(deselectEvent);
        }
        ItemEvent selectEvent = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, newColor, ItemEvent.SELECTED);
        for (ItemListener listener: listeners) {
            listener.itemStateChanged(selectEvent);
        }
    }

}

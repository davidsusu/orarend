package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.util.Settings;
import hu.webarticum.aurora.app.util.email.SimpleMessageSender;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualCheckBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualComboBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualTabbedPane;
import hu.webarticum.aurora.plugins.defaultswingui.util.EmailTestRunner;

public class SettingsDialog extends EditDialog {
    
    public enum TAB { COMMON, EMAIL }
    
    
    private static final long serialVersionUID = 1L;
    

    private Settings settings;

    private MultilingualTabbedPane settingsTabbedPane;
    
    private JCheckBox commonEnableFetchingAnnouncementCheckBox;
    private JCheckBox commonRemoveUserSettingsOnUninstallCheckBox;
    private JSpinner commonHistoryLengthSpinner;
    private JSpinner commonRecentFilesCountSpinner;
    
    private JTextField emailFromEmailTextField;
    private JTextField emailFromNameTextField;
    private JTextField emailHostTextField;
    private MultilingualComboBox<Boolean> emailUseSslComboBox;
    private JSpinner emailPortSpinner;
    private MultilingualComboBox<Boolean> emailUseAuthComboBox;
    private JTextField emailUserTextField;
    private JPasswordField emailPasswordField;


    public SettingsDialog(JFrame parent) {
        this(parent, TAB.COMMON);
    }
    
    public SettingsDialog(JFrame parent, TAB initialTab) {
        super(parent);
        this.title = text("ui.swing.dialog.settings.title");
        this.settings = Application.instance().getSettings();
        init();
        settingsTabbedPane.setSelectedIndex(initialTab.ordinal());
    }

    @Override
    protected void build() {
        settingsTabbedPane = new MultilingualTabbedPane();
        settingsTabbedPane.setPreferredSize(new Dimension(500, 350));
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(settingsTabbedPane, BorderLayout.CENTER);

        addCommonTab();
        addEmailTab();
    }

    @Override
    protected void load() {
        settings.load(true);
        loadCommonTab();
        loadEmailTab();
    }

    @Override
    protected void afterLoad() {
        afterLoadCommonTab();
        afterLoadEmailTab();
    }

    @Override
    protected void save() {
        saveCommonTab();
        saveEmailTab();
        
        settings.save();
    }

    
    // COMMON
    
    private void addCommonTab() {
        JPanel commonTabOuterPanel = new JPanel(new BorderLayout());
        settingsTabbedPane.addMultilingualTab("ui.swing.dialog.settings.tabs.common", commonTabOuterPanel);

        JPanel commonPanel = new JPanel();
        commonPanel.setLayout(new BoxLayout(commonPanel, BoxLayout.PAGE_AXIS));
        commonTabOuterPanel.add(commonPanel, BorderLayout.PAGE_START);
        
        commonEnableFetchingAnnouncementCheckBox =
            new MultilingualCheckBox("ui.swing.dialog.settings.tabs.common.enable_fetching_announcements")
        ;
        addRowTo(commonPanel, commonEnableFetchingAnnouncementCheckBox, 450);
        
        commonRemoveUserSettingsOnUninstallCheckBox =
            new MultilingualCheckBox("ui.swing.dialog.settings.tabs.common.delete_usersettings_onuninstall")
        ;
        addRowTo(commonPanel, commonRemoveUserSettingsOnUninstallCheckBox, 450);

        JPanel historyLengthPanel = new JPanel(new BorderLayout());
        commonHistoryLengthSpinner = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        commonHistoryLengthSpinner.setPreferredSize(new Dimension(70, 25));
        historyLengthPanel.add(commonHistoryLengthSpinner, BorderLayout.LINE_START);
        addRowTo(
            commonPanel,
            text("ui.swing.dialog.settings.tabs.common.history_length"), historyLengthPanel,
            450, 250
        );
        
        JPanel recentFilesCountPanel = new JPanel(new BorderLayout());
        commonRecentFilesCountSpinner = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        commonRecentFilesCountSpinner.setPreferredSize(new Dimension(70, 25));
        recentFilesCountPanel.add(commonRecentFilesCountSpinner, BorderLayout.LINE_START);
        addRowTo(
            commonPanel,
            text("ui.swing.dialog.settings.tabs.common.recent_files_count"), recentFilesCountPanel,
            450, 250
        );
    }

    private void loadCommonTab() {
        commonEnableFetchingAnnouncementCheckBox.setSelected(
            settings.get("enable_fetching_announcements", "1").equals("1")
        );
        
        commonRemoveUserSettingsOnUninstallCheckBox.setSelected(
            settings.get("delete_usersettings_onuninstall", "1").equals("1")
        );

        commonHistoryLengthSpinner.setValue(
            parseNonNegativeNumber(settings.get("history_length", "0"))
        );

        commonRecentFilesCountSpinner.setValue(
            parseNonNegativeNumber(settings.get("recent_files_count", "10"))
        );
    }
    
    private void afterLoadCommonTab() {
        bindModifyListener(commonEnableFetchingAnnouncementCheckBox);
        bindModifyListener(commonRemoveUserSettingsOnUninstallCheckBox);
        bindModifyListener(commonHistoryLengthSpinner);
        bindModifyListener(commonRecentFilesCountSpinner);
    }

    private void saveCommonTab() {
        settings.set(
            "enable_fetching_announcements",
            commonEnableFetchingAnnouncementCheckBox.isSelected() ? "1" : "0",
            false
        );

        settings.set(
            "delete_usersettings_onuninstall",
            commonRemoveUserSettingsOnUninstallCheckBox.isSelected() ? "1" : "0",
            false
        );

        settings.set(
            "history_length",
            Integer.toString((Integer)commonHistoryLengthSpinner.getValue()),
            false
        );

        settings.set(
            "recent_files_count",
            Integer.toString((Integer)commonRecentFilesCountSpinner.getValue()),
            false
        );
    }


    // EMAIL
    
    private void addEmailTab() {
        JPanel emailTabOuterPanel = new JPanel(new BorderLayout());
        settingsTabbedPane.addMultilingualTab("ui.swing.dialog.settings.tabs.email", emailTabOuterPanel);

        JPanel emailPanel = new JPanel();
        emailPanel.setLayout(new BoxLayout(emailPanel, BoxLayout.PAGE_AXIS));
        emailTabOuterPanel.add(emailPanel, BorderLayout.PAGE_START);
        
        emailFromEmailTextField = new JTextField();
        addRowTo(
            emailPanel,
            text("ui.swing.dialog.settings.tabs.email.from_email"), emailFromEmailTextField,
            450, 150
        );
        
        emailFromNameTextField = new JTextField();
        addRowTo(
            emailPanel,
            text("ui.swing.dialog.settings.tabs.email.from_name"), emailFromNameTextField,
            450, 150
        );
        
        emailHostTextField = new JTextField();
        addRowTo(
            emailPanel,
            text("ui.swing.dialog.settings.tabs.email.host"), emailHostTextField,
            450, 150
        );
        
        emailUseSslComboBox = new MultilingualComboBox<Boolean>();
        emailUseSslComboBox.addMultilingualItem("ui.swing.dialog.settings.tabs.email.use_ssl.yes", true);
        emailUseSslComboBox.addMultilingualItem("ui.swing.dialog.settings.tabs.email.use_ssl.no", false);
        addRowTo(
            emailPanel,
            text("ui.swing.dialog.settings.tabs.email.use_ssl"), emailUseSslComboBox,
            450, 150
        );
        
        JPanel portPanel = new JPanel(new BorderLayout());
        emailPortSpinner = new JSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
        emailPortSpinner.setPreferredSize(new Dimension(70, 25));
        portPanel.add(emailPortSpinner, BorderLayout.LINE_START);
        addRowTo(
            emailPanel,
            text("ui.swing.dialog.settings.tabs.email.port"), portPanel,
            450, 150
        );

        emailUseAuthComboBox = new MultilingualComboBox<Boolean>();
        emailUseAuthComboBox.addMultilingualItem("ui.swing.dialog.settings.tabs.email.use_authentication.yes", true);
        emailUseAuthComboBox.addMultilingualItem("ui.swing.dialog.settings.tabs.email.use_authentication.no", false);
        addRowTo(
            emailPanel,
            text("ui.swing.dialog.settings.tabs.email.use_authentication"), emailUseAuthComboBox,
            450, 150
        );
        
        emailUserTextField = new JTextField();
        addRowTo(
            emailPanel,
            text("ui.swing.dialog.settings.tabs.email.user"), emailUserTextField,
            450, 150
        );
        
        emailPasswordField = new JPasswordField();
        addRowTo(
            emailPanel,
            text("ui.swing.dialog.settings.tabs.email.password"), emailPasswordField,
            450, 150
        );

        JPanel testButtonPanel = new JPanel(new BorderLayout());
        addRowTo(emailPanel, testButtonPanel, 450);
        
        JButton testButton = new MultilingualButton("ui.swing.dialog.settings.tabs.email.test");
        testButton.setPreferredSize(new Dimension(70, 40));
        testButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                new EmailTestRunner(parent, createSimpleMessageSender()).run();
            }
            
        });
        testButtonPanel.add(testButton, BorderLayout.LINE_END);
    }

    private void loadEmailTab() {
        emailFromEmailTextField.setText(settings.get("email_from_email"));
        emailFromNameTextField.setText(settings.get("email_from_name"));
        emailHostTextField.setText(settings.get("email_host"));
        emailUseSslComboBox.setSelectedData(settings.get("email_use_ssl").equals("1"));
        emailPortSpinner.setValue(parseNonNegativeNumber(settings.get("email_port", "25")));
        emailUseAuthComboBox.setSelectedData(settings.get("email_use_auth").equals("1"));
        emailUserTextField.setText(settings.get("email_user"));
        emailPasswordField.setText(settings.get("email_password"));
    }

    protected void afterLoadEmailTab() {
        super.afterLoad();

        emailUseSslComboBox.addItemListener(new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                boolean useSsl = emailUseSslComboBox.getSelectedData();
                int currentPort = (Integer)emailPortSpinner.getValue();
                
                if (
                    useSsl &&
                    (currentPort == 0 || currentPort == SimpleMessageSender.DEFAULT_PORT)
                ) {
                    emailPortSpinner.setValue((Integer)SimpleMessageSender.DEFAULT_PORT_SSL);
                } else if (
                    !useSsl &&
                    (currentPort == 0 || currentPort == SimpleMessageSender.DEFAULT_PORT_SSL)
                ) {
                    emailPortSpinner.setValue((Integer)SimpleMessageSender.DEFAULT_PORT);
                }
            }
            
        });
        
        emailUseAuthComboBox.addItemListener(new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                refreshEmailAuthFields();
            }
            
        });
        
        refreshEmailAuthFields();
        
        bindModifyListener(emailFromEmailTextField);
        bindModifyListener(emailFromNameTextField);
        bindModifyListener(emailHostTextField);
        bindModifyListener(emailUseSslComboBox);
        bindModifyListener(emailPortSpinner);
        bindModifyListener(emailUseAuthComboBox);
        bindModifyListener(emailUserTextField);
        bindModifyListener(emailPasswordField);
    }
    
    private void saveEmailTab() {
        settings.set("email_from_email", emailFromEmailTextField.getText(), false);
        settings.set("email_from_name", emailFromNameTextField.getText(), false);
        settings.set("email_host", emailHostTextField.getText(), false);
        settings.set("email_use_ssl", emailUseSslComboBox.getSelectedData() ? "1" : "0", false);
        settings.set("email_port", Integer.toString((Integer)emailPortSpinner.getValue()), false);
        settings.set("email_use_auth", emailUseAuthComboBox.getSelectedData() ? "1" : "0", false);
        settings.set("email_user", emailUserTextField.getText(), false);
        settings.set("email_password", new String(emailPasswordField.getPassword()), false);
    }

    private SimpleMessageSender createSimpleMessageSender() {
        SimpleMessageSender simpleMessageSender = new SimpleMessageSender();
        
        simpleMessageSender.setFromEmail(emailFromEmailTextField.getText());
        simpleMessageSender.setFromName(emailFromNameTextField.getText());
        simpleMessageSender.setHost(emailHostTextField.getText());
        simpleMessageSender.setUseSsl(emailUseSslComboBox.getSelectedData());
        simpleMessageSender.setPort((Integer)emailPortSpinner.getValue());
        simpleMessageSender.setUseAuthentication(emailUseAuthComboBox.getSelectedData());
        simpleMessageSender.setUser(emailUserTextField.getText());
        simpleMessageSender.setPassword(new String(emailPasswordField.getPassword()));
        
        return simpleMessageSender;
    }
    
    private int parseNonNegativeNumber(String text) {
        try {
            return Math.max(0, Integer.parseInt(text));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private void refreshEmailAuthFields() {
        boolean useAuth = emailUseAuthComboBox.getSelectedData();
        emailUserTextField.setEnabled(useAuth);
        emailPasswordField.setEnabled(useAuth);
    }

}

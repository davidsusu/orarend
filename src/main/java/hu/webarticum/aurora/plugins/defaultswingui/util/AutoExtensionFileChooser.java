package hu.webarticum.aurora.plugins.defaultswingui.util;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import hu.webarticum.aurora.app.util.FileUtil;
import hu.webarticum.aurora.app.util.common.Pair;


public class AutoExtensionFileChooser extends JFileChooser {
    
    private static final long serialVersionUID = 1L;
    
    
    protected boolean appendExtension = true;
    
    
    public AutoExtensionFileChooser() {
        super();
    }
    
    public void setAppendExtension(boolean appendExtension) {
        this.appendExtension = appendExtension;
    }
    
    @Override
    public File[] getSelectedFiles() {
        File[] selectedFiles = super.getSelectedFiles();
        File[] resultFiles = new File[selectedFiles.length];
        for (int i = 0; i < selectedFiles.length; i++) {
            resultFiles[i] = selectedFiles[i];
        }
        return resultFiles;
    }
    
    public File getAutoSelectedFile() {
        return getFileExtensionPairFrom(super.getSelectedFile()).getLeft();
    }
    
    public String getAutoSelectedExtension() {
        return getFileExtensionPairFrom(super.getSelectedFile()).getRight();
    }
    
    public Pair<File, String> getFileExtensionPairFrom(File originalFile) {
        if (originalFile == null) {
            return new Pair<File, String>(null, "");
        }
        
        String defaultExtension;
        String[] extensions;

        FileFilter acceptAllFilter = getAcceptAllFileFilter();
        
        FileFilter fileFilter = getFileFilter();
        
        if (fileFilter.equals(acceptAllFilter)) {
            fileFilter = null;
        }
        
        if (fileFilter == null) {
            String detectedExtension = FileUtil.getExtension(originalFile);
            
            if (detectedExtension.isEmpty()) {
                for (FileFilter choosableFilter: getChoosableFileFilters()) {
                    if (!choosableFilter.equals(acceptAllFilter)) {
                        fileFilter = choosableFilter;
                        break;
                    }
                }            
            } else {
                FileFilter secondaryFilter = null;
                FileFilter tertiaryFilter = null;
                for (FileFilter choosableFilter: getChoosableFileFilters()) {
                    if (choosableFilter.equals(acceptAllFilter)) {
                        continue;
                    } else if (choosableFilter instanceof ExtensionFilter) {
                        if (((ExtensionFilter)choosableFilter).getDefaultExtension().equals(detectedExtension)) {
                            fileFilter = choosableFilter;
                            break;
                        }
                    } else if (choosableFilter.accept(originalFile)) {
                        if (secondaryFilter == null) {
                            secondaryFilter = choosableFilter;
                        }
                    } else {
                        if (tertiaryFilter == null) {
                            tertiaryFilter = choosableFilter;
                        }
                    }
                }
                
                if (fileFilter == null) {
                    fileFilter = (secondaryFilter != null) ? secondaryFilter : tertiaryFilter;
                }
            }
            
            if (fileFilter == null) {
                return new Pair<File, String>(originalFile, "");
            }
        }
        
        if (fileFilter instanceof ExtensionFilter) {
            ExtensionFilter extensionFilter = (ExtensionFilter)fileFilter;
            defaultExtension = extensionFilter.getDefaultExtension();
            extensions = extensionFilter.getExtensions();
        } else if (fileFilter instanceof FileNameExtensionFilter) {
            FileNameExtensionFilter fileNameExtensionFilter = (FileNameExtensionFilter)fileFilter;
            extensions = fileNameExtensionFilter.getExtensions();
            defaultExtension = (extensions.length > 0) ? extensions[0] : "";
        } else if (fileFilter != null && fileFilter.accept(originalFile)) {
            return new Pair<File, String>(originalFile, FileUtil.getExtension(originalFile));
        } else {
            return new Pair<File, String>(originalFile, "");
        }
        
        return FileUtil.ensureExtension(
            originalFile, defaultExtension, extensions, appendExtension
        );
        
        /*
        if (originalFile == null) {
            return new Pair<File, String>(null, "");
        } else if (fileFilter instanceof ExtensionFilter) {
            ExtensionFilter extensionFilter = (ExtensionFilter)fileFilter;
            String defaultExtension = extensionFilter.getDefaultExtension();
            String[] extensions = extensionFilter.getExtensions();
            return FileUtil.ensureExtension(
                originalFile, defaultExtension, extensions, appendExtension
            );
        } else if (fileFilter instanceof FileNameExtensionFilter) {
            FileNameExtensionFilter fileNameExtensionFilter = (FileNameExtensionFilter)fileFilter;
            String[] extensions = fileNameExtensionFilter.getExtensions();
            String defaultExtension = (extensions.length > 0) ? extensions[0] : "";
            return FileUtil.ensureExtension(
                originalFile, defaultExtension, extensions, appendExtension
            );
        } else if (fileFilter != null && fileFilter.accept(originalFile)) {
            return new Pair<File, String>(originalFile, FileUtil.getExtension(originalFile));
        } else {
            return new Pair<File, String>(originalFile, "");
        }
        */
    }
    
    public static class ExtensionFilter extends FileFilter {

        private final boolean multilang;

        private final String label;

        private final String defaultExtension;

        private final String[] extensions;
        
        public ExtensionFilter(String label, String extension) {
            this(false, label, extension);
        }

        public ExtensionFilter(String label, String defaultExtension, String... extensions) {
            this(false, label, defaultExtension, extensions);
        }
        
        public ExtensionFilter(boolean multilang, String label, String extension) {
            this(multilang, label, extension, new String[]{extension});
        }

        public ExtensionFilter(boolean multilang, String label, String defaultExtension, String... extensions) {
            this.multilang = multilang;
            this.label = label;
            this.defaultExtension = defaultExtension;
            this.extensions = extensions;
        }

        @Override
        public boolean accept(File file) {
            if (file.isDirectory()) {
                return true;
            } else if (!file.isFile()) {
                return false;
            }
            String extension = FileUtil.getExtension(file);
            for (String _extension: extensions) {
                if (_extension.equals(extension)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String getDescription() {
            if (multilang) {
                return text(label);
            } else {
                return label;
            }
        }
        
        public String getDefaultExtension() {
            return defaultExtension;
        }
        
        public String[] getExtensions() {
            return extensions;
        }
        
    }
    
    public static class ImageFilter extends ExtensionFilter {

        public ImageFilter() {
            this(true);
        }

        public ImageFilter(boolean multilang) {
            this(multilang, multilang ? "ui.swing.util.autoextensionfilechooser.imagefilter.label" : "Select image");
        }

        public ImageFilter(String label) {
            this(false, label);
        }
        
        public ImageFilter(boolean multilang, String label) {
            super(
                multilang, label, "png",
                new String[]{
                    "png", "jpg", "jpeg", "gif", "tif", "tiff"
                }
            );
        }
        
    }
    
}

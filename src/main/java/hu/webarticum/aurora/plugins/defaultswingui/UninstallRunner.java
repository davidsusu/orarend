package hu.webarticum.aurora.plugins.defaultswingui;

import java.io.File;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.aurora.app.util.FileUtil;


public class UninstallRunner implements CommonExtensions.Runner {

    @Override
    public Type getType() {
        return Type.OPERATION;
    }
    
    @Override
    public boolean isApplicable() {
        String runOperationName = Application.instance().getArgumentMap().get("run-operation");
        if (runOperationName == null) {
            return false;
        } else {
            return runOperationName.equals("prerm");
        }
    }
    
    @Override
    public void run() {
        String user = System.getProperty("user.name");
        String prefix = "AURORA Uninstall (" + user + "): ";

        String stringValue = Application.instance().getSettings().get("delete_usersettings_onuninstall");
        boolean doRemove = stringValue.equals("1");
        
        if (doRemove) {
            File settingsDirectory = Application.instance().getSettingsDirectory();
            System.out.println(prefix + "check settings directory: " + settingsDirectory);
            if (settingsDirectory.exists()) {
                System.out.print(prefix + "delete settings directory ... ");
                boolean success = FileUtil.deleteRecursively(settingsDirectory);
                System.out.println(success ? "success" : "FAILED");
            } else {
                System.out.println(prefix + "settings directory not found");
            }
        } else {
            System.out.println(prefix + "settings directory deletion skipped");
        }
        
        System.out.println(prefix + "finished");
    }

}

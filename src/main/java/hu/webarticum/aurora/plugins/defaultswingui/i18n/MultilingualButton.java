package hu.webarticum.aurora.plugins.defaultswingui.i18n;

import javax.swing.Icon;

import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.PurposeButton;

public class MultilingualButton extends PurposeButton implements MultilingualComponent {

    private static final long serialVersionUID = 1L;

    protected MultilingualContent labelContent;

    protected MultilingualContent tooltipContent;
    
    public MultilingualButton() {
        this("");
    }

    public MultilingualButton(String labelPath) {
        this(labelPath, "");
    }

    public MultilingualButton(String labelPath, ButtonPurpose purpose) {
        this(labelPath, "", null, purpose);
    }

    public MultilingualButton(String labelPath, String tooltipPath) {
        this(labelPath, tooltipPath, null);
    }

    public MultilingualButton(String labelPath, String tooltipPath, Icon icon) {
        this(labelPath, tooltipPath, icon, ButtonPurpose.NONE);
    }

    public MultilingualButton(Icon icon, String tooltipPath) {
        this("", tooltipPath, icon, ButtonPurpose.NONE);
    }

    public MultilingualButton(String labelPath, String tooltipPath, Icon icon, ButtonPurpose purpose) {
        super(purpose);
        setLanguagePath(labelPath, tooltipPath);
        if (icon != null) {
            setIcon(icon);
        }
    }
    
    
    public void setLanguagePath(String labelPath) {
        setLanguagePath(labelPath, "");
    }
    
    public void setLanguagePath(String labelPath, String tooltipPath) {
        setLanguagePath(new PathMultilingualContent(labelPath), new PathMultilingualContent(tooltipPath));
        reloadLanguageTexts();
    }

    public void setLanguagePath(MultilingualContent labelContent) {
        setLanguagePath(labelContent, new PathMultilingualContent(""));
    }
    
    public void setLanguagePath(MultilingualContent labelContent, MultilingualContent tooltipContent) {
        this.labelContent = labelContent;
        this.tooltipContent = tooltipContent;
        reloadLanguageTexts();
    }
    
    @Override
    public void reloadLanguageTexts() {
        setText(labelContent.toString());
        setToolTipText(tooltipContent.toStringOrNull());
    }

}

package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import hu.webarticum.aurora.app.Info;

public class IconLoader {

    public static ImageIcon loadIcon(String name) {
        String path = Info.RESOURCE_PATH+"/plugins/defaultswingui/icon/"+name+".png";
        java.net.URL url = IconLoader.class.getResource(path);
        if (url!=null) {
            return new ImageIcon(url, "Icon: "+name);
        } else {
            System.err.println("Icon not found: " + name + ", default icon used");
            return getFallbackIcon();
        }
    }

    public static Image loadImage(String name) {
        return loadIcon(name).getImage();
    }
    
    public static ImageIcon getFallbackIcon() {
        BufferedImage image = new BufferedImage(16, 16, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        graphics.setColor(Color.RED);
        graphics.fillRect(0, 0, 16, 16);
        graphics.setColor(Color.WHITE);
        graphics.setStroke(new BasicStroke(3));
        graphics.drawLine(5, 5, 10, 10);
        graphics.drawLine(5, 10, 10, 5);
        return new ImageIcon(image);
    }
    
}

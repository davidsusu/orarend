package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import java.awt.Component;
import java.awt.Container;
import java.awt.Window;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

import hu.webarticum.aurora.core.model.Store;
import hu.webarticum.aurora.plugins.defaultswingui.util.ComponentTreeUtil;


public class StoreListPanel<T> extends JPanel {
    
    private static final long serialVersionUID = 1L;
    
    private EventListenerList eventListenerList = new EventListenerList();
    
    private final Map<T, Component> itemComponentMap = new HashMap<T, Component>();

    private final Store.StoreListener<T> innerStoreListener = new InnerStoreListener();

    private final Store<T> store;

    private final ComponentProvider<T> componentProvider;

    private final ItemFilter<T> itemFilter;

    private boolean attached = false;

    public StoreListPanel(Store<T> store, ComponentProvider<T> componentProvider) {
        this(store, componentProvider, null, true);
    }

    public StoreListPanel(Store<T> store, ComponentProvider<T> componentProvider, ItemFilter<T> itemFilter) {
        this(store, componentProvider, itemFilter, true);
    }
    
    public StoreListPanel(Store<T> store, ComponentProvider<T> componentProvider, ItemFilter<T> itemFilter, boolean attach) {
        super();
        this.store = store;
        this.componentProvider = componentProvider;
        this.itemFilter = itemFilter;
        if (attach) {
            attach();
        }
    }

    public boolean isAttached() {
        return attached;
    }
    
    public void attach() {
        if (!attached) {
            itemComponentMap.clear();
            removeAll();
            for (T item: store) {
                if (itemFilter == null || itemFilter.validate(item, this)) {
                    Component component = componentProvider.buildComponentFor(item, this);
                    componentProvider.refreshComponentFor(component, item, this);
                    itemComponentMap.put(item, component);
                    add(component);
                }
            }
            store.addStoreListener(innerStoreListener);
            listChanged();
            attached = true;
        }
    }

    public void detach() {
        if (attached) {
            removeAll();
            store.removeStoreListener(innerStoreListener);
            attached = false;
        }
    }
    
    public void addChangeListener(ListChangeListener<T> changeListener) {
        eventListenerList.add(ListChangeListener.class, changeListener);
    }

    public void removeChangeListener(ListChangeListener<T> changeListener) {
        eventListenerList.remove(ListChangeListener.class, changeListener);
    }
    
    public void refresh() {
        refresh(true);
    }
    
    public void refresh(boolean reorder) {
        if (attached) {
            if (reorder && store.isSorted()) {
                // XXX
                detach();
                attach();
            } else {
                for (Map.Entry<T, Component> entry: itemComponentMap.entrySet()) {
                    T item = entry.getKey();
                    Component component = entry.getValue();
                    componentProvider.refreshComponentFor(component, item, this);
                }
            }
            listChanged();
        }
    }

    public void refresh(T item) {
        if (itemComponentMap.containsKey(item)) {
            if (itemFilter == null || itemFilter.validate(item, this)) {
                Component component = itemComponentMap.get(item);
                if (store.isSorted()) {
                    int index = 0;
                    for (T storeItem: store) {
                        if (itemFilter == null || itemFilter.validate(storeItem, this)) {
                            if (item.equals(storeItem)) {
                                break;
                            }
                            index++;
                        }
                    }
                    int currentIndex = -1;
                    for (int i = getComponentCount() - 1; i >= 0; i--) {
                        Component _component = getComponent(i);
                        if (_component.equals(component)) {
                            currentIndex = i;
                            break;
                        }
                    }
                    if (currentIndex != index) {
                        Window window = SwingUtilities.getWindowAncestor(this);
                        Component focusedComponent = window.getMostRecentFocusOwner();
                        remove(component);
                        add(component, index);
                        if (focusedComponent.equals(component) || SwingUtilities.isDescendingFrom(focusedComponent, component)) {
                            focusedComponent.requestFocusInWindow();
                        }
                    }
                }
                componentProvider.refreshComponentFor(component, item, this);
                listChanged();
            } else {
                itemRemoved(item);
            }
        } else if (store.contains(item)) {
            if (itemFilter == null || itemFilter.validate(item, this)) {
                itemRegistered(item);
            }
        }
    }
    
    @Override
    public void revalidate() {
        super.revalidate();
        repaint(); // XXX
    }
    
    private void itemRegistered(T item) {
        if (itemFilter == null || itemFilter.validate(item, this)) {
            Component component = componentProvider.buildComponentFor(item, this);
            componentProvider.refreshComponentFor(component, item, this);
            itemComponentMap.put(item, component);
            if (store.isSorted()) {
                int index = 0;
                for (T storeItem: store) {
                    if (itemFilter == null || itemFilter.validate(storeItem, this)) {
                        if (item.equals(storeItem)) {
                            break;
                        }
                        index++;
                    }
                }
                add(component, index);
            } else {
                add(component);
            }
            listChanged();
        }
    }

    private void itemRemoved(T item) {
        if (itemComponentMap.containsKey(item)) {
            Component focusableComponent = null;
            Component component = itemComponentMap.get(item);
            itemComponentMap.remove(item);
            Window window = SwingUtilities.getWindowAncestor(this);
            Component focusedComponent = window.getMostRecentFocusOwner();
            if (focusedComponent != null && (focusedComponent.equals(component) || SwingUtilities.isDescendingFrom(focusedComponent, component))) {
                int currentIndex = -1;
                int componentCount = getComponentCount();
                for (int i = 0; i < componentCount; i++) {
                    Component _component = getComponent(i);
                    if (_component.equals(component)) {
                        currentIndex = i;
                        break;
                    }
                }
                if (currentIndex < componentCount - 1) {
                    focusableComponent = getComponent(currentIndex + 1);
                } else if (currentIndex > 0) {
                    focusableComponent = getComponent(currentIndex - 1);
                }
            }
            remove(component);
            listChanged();
            if (focusableComponent instanceof Container) {
                final Container foundFocusableComponent = (Container)focusableComponent;
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        JButton button = ComponentTreeUtil.getFirstButton(foundFocusableComponent);
                        if (button != null) {
                            // XXX
                            button.requestFocusInWindow();
                        }
                    }
                    
                });
            }
        }
    }
    
    private void listChanged() {
        revalidate();
        ListChangeListener.ChangeEvent<T> changeEvent = new ListChangeListener.ChangeEvent<T>(this);
        for (ListChangeListener<?> listChangeListener: eventListenerList.getListeners(ListChangeListener.class)) {
            @SuppressWarnings("unchecked")
            ListChangeListener<T> typedListChangeListener = (ListChangeListener<T>)listChangeListener;
            typedListChangeListener.changed(changeEvent);
        }
    }
    
    public interface ComponentProvider<T> {

        public Component buildComponentFor(T item, StoreListPanel<T> storeListPanel);

        public void refreshComponentFor(Component component, T item, StoreListPanel<T> storeListPanel);
        
    }
    
    public interface ItemFilter<T> {
        
        public boolean validate(T item, StoreListPanel<T> storeListPanel);
        
    }
    
    public interface ListChangeListener<T> extends EventListener {
        
        public void changed(ChangeEvent<T> changeEvent);
        
        public class ChangeEvent<T> {
            
            final StoreListPanel<T> panel;
            
            public ChangeEvent(StoreListPanel<T> panel) {
                this.panel = panel;
            }
            
            public StoreListPanel<T> getPanel() {
                return panel;
            }
            
            public Store<T> getStore() {
                return panel.store;
            }
            
        }
        
    }
    
    protected class InnerStoreListener implements Store.StoreListener<T> {

        @Override
        public void registered(RegisterEvent<T> registerEvent) {
            itemRegistered(registerEvent.getItem());
        }

        @Override
        public void removed(RemoveEvent<T> removeEvent) {
            itemRemoved(removeEvent.getItem());
        }

        @Override
        public void itemRefreshed(ItemRefreshEvent<T> itemRefreshEvent) {
            refresh(itemRefreshEvent.getItem());
        }
        
        @Override
        public void storeRefreshed(StoreRefreshEvent<T> storeRefreshEvent) {
            refresh(storeRefreshEvent.isReordered());
        }
        
    }
    
}

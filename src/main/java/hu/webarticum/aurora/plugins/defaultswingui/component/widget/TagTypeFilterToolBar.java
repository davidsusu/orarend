package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.Painter;
import javax.swing.UIDefaults;
import javax.swing.event.EventListenerList;

import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualToggleButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class TagTypeFilterToolBar extends JToolBar {

    private static final long serialVersionUID = 1L;

    
    private final Map<JToggleButton, Tag.Type> buttons = new HashMap<JToggleButton, Tag.Type>();
    
    private final EventListenerList listeners = new EventListenerList();
    
    
    public TagTypeFilterToolBar() {
        super(JToolBar.HORIZONTAL);
        setFloatable(false);

        String uiPropertyKey = "Nimbus.Overrides";

        Painter<JComponent> noPainter = new Painter<JComponent>() {

            @Override
            public void paint(Graphics2D g, JComponent object, int width, int height) {
                // nothing to do
            }
            
        };
        
        UIDefaults toolBarUiDefaults = new UIDefaults();
        toolBarUiDefaults.put("ToolBar[East].borderPainter", noPainter);
        toolBarUiDefaults.put("ToolBar[North].borderPainter", noPainter);
        toolBarUiDefaults.put("ToolBar[South].borderPainter", noPainter);
        toolBarUiDefaults.put("ToolBar[West].borderPainter", noPainter);
        putClientProperty(uiPropertyKey, toolBarUiDefaults);
        
        UIDefaults toggleButtonUiDefaults = new UIDefaults();
        toggleButtonUiDefaults.put("ToolBar:ToggleButton.contentMargins", new Insets(7, 7, 7, 7));

        ButtonGroup buttonGroup = new ButtonGroup();
        
        ItemListener buttonItemListener = new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                JToggleButton toggleButton = (JToggleButton)ev.getSource();
                if (toggleButton.isSelected()) {
                    onChanged(buttons.get(toggleButton));
                }
            }
            
        };
        
        JToggleButton subjectToggleButton = new MultilingualToggleButton("", "core.tag.type.subject");
        subjectToggleButton.setIcon(IconLoader.loadIcon("tagtype-subject"));
        subjectToggleButton.putClientProperty(uiPropertyKey, toggleButtonUiDefaults);
        buttonGroup.add(subjectToggleButton);
        add(subjectToggleButton);
        buttons.put(subjectToggleButton, Tag.Type.SUBJECT);

        JToggleButton languageToggleButton = new MultilingualToggleButton("", "core.tag.type.language");
        languageToggleButton.setIcon(IconLoader.loadIcon("tagtype-language"));
        languageToggleButton.putClientProperty(uiPropertyKey, toggleButtonUiDefaults);
        buttonGroup.add(languageToggleButton);
        add(languageToggleButton);
        buttons.put(languageToggleButton, Tag.Type.LANGUAGE);
        
        JToggleButton otherToggleButton = new MultilingualToggleButton("", "core.tag.type.other");
        otherToggleButton.setIcon(IconLoader.loadIcon("tagtype-other"));
        otherToggleButton.putClientProperty(uiPropertyKey, toggleButtonUiDefaults);
        buttonGroup.add(otherToggleButton);
        add(otherToggleButton);
        buttons.put(otherToggleButton, Tag.Type.OTHER);
        
        subjectToggleButton.setSelected(true);
        
        subjectToggleButton.addItemListener(buttonItemListener);
        languageToggleButton.addItemListener(buttonItemListener);
        otherToggleButton.addItemListener(buttonItemListener);
    }

    public void addChangeListener(ChangeListener changeListener) {
        listeners.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        listeners.remove(ChangeListener.class, changeListener);
    }
    
    public Tag.Type getTagType() {
        for (Map.Entry<JToggleButton, Tag.Type> entry: buttons.entrySet()) {
            if (entry.getKey().isSelected()) {
                return entry.getValue();
            }
        }
        return null;
    }
    
    private void onChanged(Tag.Type tagType) {
        for (ChangeListener changeListener: listeners.getListeners(ChangeListener.class)) {
            changeListener.onChanged(tagType);
        }
    }
    
    
    public interface ChangeListener extends EventListener {
        
        public void onChanged(Tag.Type tagType);
        
    }
    
}

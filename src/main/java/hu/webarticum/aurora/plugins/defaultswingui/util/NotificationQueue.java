package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.io.Closeable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.app.util.common.StringUtil;
import hu.webarticum.aurora.plugins.defaultswingui.global.GlobalUiObjects;
import hu.webarticum.aurora.plugins.defaultswingui.util.NotificationQueue.Notification.TYPE;

public interface NotificationQueue extends Closeable {

    public void send(Notification notification);
    
    public void send(String title, String description, Notification.TYPE type);
    
    public class DefaultNotificationQueue implements NotificationQueue {
        
        private volatile List<Pair<Notification, JDialog>> items = new LinkedList<Pair<Notification, JDialog>>();
        
        private Timer timer;
        
        public DefaultNotificationQueue() {
            timer = new Timer();
            TimerTask task = new TimerTask() {
                
                @Override
                public void run() {
                    refresh();
                }
                
            };
            timer.schedule(task, 0, 1000);
        }
        
        @Override
        public synchronized void send(Notification notification) {
            JDialog dialog = new JDialog(
                GlobalUiObjects.getMainFrame(),
                notification.getTitle()
            );
            dialog.setModal(false);
            JPanel contentPane = new JPanel();
            contentPane.setLayout(new BorderLayout());
            contentPane.setBackground(typeToColor(notification.getType()));
            dialog.setContentPane(contentPane);
            JLabel label = new JLabel("<html><div style=\"width:200px;\">" + StringUtil.toHtml(notification.getDescription()) + "</div></html>");
            label.setBorder(new EmptyBorder(10, 10, 10, 10));
            contentPane.add(label, BorderLayout.CENTER);
            dialog.pack();
            
            final Pair<Notification, JDialog> item = new Pair<Notification, JDialog>(notification, dialog);
            items.add(item);
            
            dialog.addWindowFocusListener(new WindowFocusListener() {
                
                @Override
                public void windowLostFocus(WindowEvent ev) {
                }
                
                @Override
                public void windowGainedFocus(WindowEvent ev) {
                    JFrame mainFrame = GlobalUiObjects.getMainFrame();
                    if (mainFrame != null) {
                        mainFrame.requestFocus();
                    }
                }
                
            });
            
            dialog.addWindowListener(new WindowAdapter() {
                
                @Override
                public void windowClosing(WindowEvent ev) {
                    items.remove(item);
                    JDialog dialog = item.getRight();
                    dialog.setVisible(false);
                    dialog.dispose();
                    refresh();
                }
                
            });
        }

        @Override
        public void send(String title, String description, TYPE type) {
            send(new Notification(title, description, type));
        }
        
        private Color typeToColor(Notification.TYPE type) {
            switch (type) {
                case WARNING:
                    return new Color(0xFFCC55);
                case ERROR:
                    return new Color(0xFF7755);
                case INFO:
                    return new Color(0xCCCCCC);
                case SUCCESS:
                    return new Color(0x99CC77);
                default:
                    return new Color(0xFFFFFF);
            }
        }
        
        @Override
        public void close() {
            timer.cancel();
        }
        
        public void refresh() {
            SwingUtilities.invokeLater(new Runnable() {
                
                @Override
                public void run() {
                    Dimension screenSize = GlobalUiObjects.getScreenSize();
                    Insets screenInsets = GlobalUiObjects.getScreenInsets();
                    int right = screenSize.width - screenInsets.right;
                    int bottom = screenSize.height - screenInsets.bottom;
                    Iterator<Pair<Notification, JDialog>> itemIterator = items.iterator();
                    int verticalPosition = 0;
                    while (itemIterator.hasNext()) {
                        Date date = new Date();
                        Pair<Notification, JDialog> item = itemIterator.next();
                        Notification notification = item.getLeft();
                        JDialog dialog = item.getRight();
                        if (date.getTime() - notification.getDate().getTime() > 5000) {
                            dialog.setVisible(false);
                            itemIterator.remove();
                        } else {
                            if (!dialog.isVisible()) {
                                dialog.setVisible(true);
                            }
                            verticalPosition += dialog.getHeight();
                            dialog.setLocation(right - dialog.getWidth(), bottom - verticalPosition);
                        }
                    }
                }
                
            });
        }
        
    }
    
    public static class Notification {
        
        public enum TYPE {INFO, SUCCESS, WARNING, ERROR}

        private static final IdGenerator idGenerator = new IdGenerator();
        
        private final String title;
        
        private final String description;
        
        private final TYPE type;
        
        private final String id;
        
        private final Date date;
        
        public Notification(String title, String description, TYPE type) {
            this.title = title;
            this.description = description;
            this.type = type;
            this.id = idGenerator.getNext();
            this.date = new Date();
        }
        
        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public TYPE getType() {
            return type;
        }

        public String getId() {
            return id;
        }

        public Date getDate() {
            return date;
        }

    }
    
    public static class IdGenerator {
        
        private int i = 0;
        
        private Random random = new Random(0);
        
        public String getNext() {
            return Integer.toOctalString(random.nextInt()) + "0" + (++i);
        }
        
    }
    
}

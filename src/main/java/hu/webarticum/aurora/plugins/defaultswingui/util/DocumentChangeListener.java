package hu.webarticum.aurora.plugins.defaultswingui.util;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public abstract class DocumentChangeListener implements DocumentListener {

    @Override
    public void changedUpdate(DocumentEvent ev) {
        changed(ev);
    }

    @Override
    public void insertUpdate(DocumentEvent ev) {
        changed(ev);
    }

    @Override
    public void removeUpdate(DocumentEvent ev) {
        changed(ev);
    }
    
    protected abstract void changed(DocumentEvent ev);

}

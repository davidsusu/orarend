package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingWorker;

import hu.webarticum.aurora.app.lang.CancelledException;

public abstract class SwingWorkerProcessDialog extends ProcessDialog {

    private static final long serialVersionUID = 1L;

    protected Worker worker;

    
    public SwingWorkerProcessDialog(JFrame parent) {
        super(parent);
        init();
    }

    public SwingWorkerProcessDialog(JFrame parent, String title) {
        super(parent, title);
        init();
    }

    public SwingWorkerProcessDialog(JFrame parent, boolean compact) {
        super(parent, compact);
        init();
    }

    public SwingWorkerProcessDialog(JFrame parent, String title, boolean compact) {
        super(parent, title, compact);
        init();
    }
    
    
    private void init() {
        this.worker = createWorker();
    }

    @Override
    protected void _start() {
        worker.execute();
    }

    @Override
    protected void _cancel() {
        worker.cancel(false);
    }
    
    
    protected abstract Worker createWorker();
    
    
    public abstract class Worker extends SwingWorker<Void, Worker.Command> {
        
        protected boolean cancelled = false;
        
        @Override
        protected Void doInBackground() {
            SwingWorkerProcessDialog.this.progressBar.setIndeterminate(true);
            
            try {
                _run();
            } catch (CancelledException e) {
                cancelled = true;
            } catch (Exception e) {
                e.printStackTrace();
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.swingworkerprocess.error_occured") + "\n\n" + e.getMessage()
                );
                cancelled = true;
            }
            
            if (cancelled) {
                _rollBack();
                publishCommand(new CancelCommand());
            } else {
                publishCommand(new SuccessCommand());
            }

            return null;
        }
        
        public synchronized void publishCommand(Command command) {
            publish(command);
        }
        
        @Override
        public void process(List<Command> commands) {
            for (Command command: commands) {
                processCommand(command);
            }
        }
        
        public void processCommand(Command command) {
            command.run();
        }

        public abstract void _run() throws CancelledException;

        public abstract void _rollBack();
        

        public abstract class Command implements Runnable {
        }
        
        public class SetLabelCommand extends Command {
            
            private String label;
            
            public SetLabelCommand(String label) {
                this.label = label;
            }

            @Override
            public void run() {
                SwingWorkerProcessDialog.this.labelPlace.setText(label);
            }
            
        }
        
        public class SetProgressCommand extends Command {

            private Double progress;
            
            public SetProgressCommand(Double progress) {
                this.progress = progress;
            }
            
            @Override
            public void run() {
                if (progress == null) {
                    SwingWorkerProcessDialog.this.progressBar.setIndeterminate(true);
                } else {
                    SwingWorkerProcessDialog.this.progressBar.setIndeterminate(false);
                    SwingWorkerProcessDialog.this.progressBar.setValue((int) (progress * 100));
                }
            }
            
        }
        
        public class LogCommand extends Command {

            private String line;
            
            public LogCommand(String line) {
                this.line = line;
            }

            @Override
            public void run() {
                SwingWorkerProcessDialog.this.logTextArea.append(line + "\n");
            }
            
        }
        
        public abstract class TerminatedCommand extends Command {
        }
        
        public class CancelCommand extends TerminatedCommand {

            @Override
            public void run() {
                SwingWorkerProcessDialog.this.labelPlace.setText(text("ui.swing.dialog.swingworkerprocess.stopped"));
                SwingWorkerProcessDialog.this.setRunning(false);
                SwingWorkerProcessDialog.this.setCancelled(true);
            }
            
        }

        public class FailedCommand extends TerminatedCommand {

            @Override
            public void run() {
                SwingWorkerProcessDialog.this.labelPlace.setText(text("ui.swing.dialog.swingworkerprocess.failed"));
                SwingWorkerProcessDialog.this.setRunning(false);
            }
            
        }

        public class SuccessCommand extends TerminatedCommand {

            @Override
            public void run() {
                SwingWorkerProcessDialog.this.setRunning(false);
            }
            
        }

    }

}

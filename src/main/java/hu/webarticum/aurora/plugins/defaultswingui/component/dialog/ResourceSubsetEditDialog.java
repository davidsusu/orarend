package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.ResourceSubset;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class ResourceSubsetEditDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<ResourceSubset> resourceSubsetWrapper;
    
    private Resource resource;
    
    private JButton resourceEditButton;
    
    private JTree tree;
    
    private List<DefaultMutableTreeNode> invalidTreeNodes = new ArrayList<DefaultMutableTreeNode>();
    
    public ResourceSubsetEditDialog(JFrame parent, Document document, GeneralWrapper<ResourceSubset> resourceSubsetWrapper) {
        super(parent);
        this.title = text("ui.swing.dialog.resourcesubsetedit.title");
        this.document = document;
        this.resourceSubsetWrapper = resourceSubsetWrapper;
        this.resource = resourceSubsetWrapper.get().getResource();
        init(false, true);
    }
    
    @Override
    protected void build() {
        JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.PAGE_AXIS));
        headerPanel.setBorder(new EmptyBorder(7, 7, 7, 7));
        containerPanel.add(headerPanel, BorderLayout.PAGE_START);
        
        JPanel resourceEditPanel = new JPanel();
        resourceEditPanel.setLayout(new BorderLayout());
        addRowTo(headerPanel, new MultilingualLabel("ui.swing.dialog.resourcesubsetedit.resource"), resourceEditPanel, formWidth, leftWidth);
        
        resourceEditButton = new JButton(resource.getLabel());
        resourceEditButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                ResourceEditDialog dialog = new ResourceEditDialog(parent, document, resource);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    resourceEditButton.setText(resource.getLabel());
                }
            }
            
        });
        resourceEditPanel.add(resourceEditButton, BorderLayout.CENTER);
        
        JPanel treeOuter = new JPanel();
        treeOuter.setLayout(new BorderLayout());
        treeOuter.setPreferredSize(new Dimension(270, 200));
        tree = new JTree();
        tree.setCellRenderer(new SubsetCellRenderer());
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeWillExpandListener(new TreeWillExpandListener() {
            
            @Override
            public void treeWillExpand(TreeExpansionEvent ev) throws ExpandVetoException {}
            
            @Override
            public void treeWillCollapse(TreeExpansionEvent ev) throws ExpandVetoException {
                throw new ExpandVetoException(ev);
            }
            
        });
        tree.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                int mouseButton = ev.getButton();
                int mouseClicks = ev.getClickCount();
                boolean mouseShift = ev.isShiftDown();
                if (mouseButton==MouseEvent.BUTTON3) {
                    TreePath selectionPath = tree.getSelectionPath();
                    if (selectionPath==null) {
                        return;
                    }
                    Object selectedItem = selectionPath.getLastPathComponent();
                    if (selectedItem==null) {
                        return;
                    }
                    DefaultTreeModel treeModel = (DefaultTreeModel)tree.getModel();
                    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)selectedItem;
                    if (treeNode==treeModel.getRoot()) {
                        return;
                    }
                    
                    int result = OptionPaneUtil.showConfirmDialog(
                        parent, text("ui.swing.dialog.resourcesubsetedit.delete_node.sure"), ButtonPurpose.DANGER
                    );
                    if (result!=JOptionPane.OK_OPTION) {
                        return;
                    }
                    
                    DefaultMutableTreeNode parentTreeNode = (DefaultMutableTreeNode)treeNode.getParent();
                    
                    parentTreeNode.remove(treeNode);
                    treeModel.reload(parentTreeNode);
                    refreshStatus();
                    expandAll();
                } else if (mouseButton==MouseEvent.BUTTON1 && mouseClicks==2 && !mouseShift) {
                    TreePath selectionPath = tree.getSelectionPath();
                    if (selectionPath==null) {
                        return;
                    }
                    Object selectedItem = selectionPath.getLastPathComponent();
                    if (selectedItem==null) {
                        return;
                    }
                    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)selectedItem;
                    Object userObject = treeNode.getUserObject();
                    if (userObject==null || !(userObject instanceof NodeEntry)) {
                        return;
                    }
                    DefaultTreeModel treeModel = (DefaultTreeModel)tree.getModel();
                    NodeEntry nodeEntry = (NodeEntry)userObject;
                    GeneralWrapper<NodeEntry> wrapper = new GeneralWrapper<NodeEntry>(nodeEntry);
                    SubsetNodeEditDialog dialog = new SubsetNodeEditDialog(parent, wrapper);
                    dialog.run();
                    
                    if (dialog.getResult()!=EditDialog.RESULT_OK) {
                        return;
                    }
                    
                    NodeEntry newNodeEntry = wrapper.get();
                    NodeEntry.Type newType = newNodeEntry.getType();
                    
                    treeNode.setUserObject(newNodeEntry);
                    
                    if (newType==NodeEntry.Type.WHOLE || newType==NodeEntry.Type.SPLITTINGPART) {
                        int childnum = treeNode.getChildCount();
                        for (int i=childnum-1; i>=0; i--) {
                            treeNode.remove(i);
                        }
                    } else if (newType==NodeEntry.Type.INVERSE) {
                        int childnum = treeNode.getChildCount();
                        for (int i=childnum-1; i>0; i--) {
                            treeNode.remove(i);
                        }
                    }
                    
                    treeModel.reload(treeNode);
                    refreshStatus();
                    expandAll();
                } else if (mouseButton==MouseEvent.BUTTON1 && mouseShift) {
                    TreePath selectionPath = tree.getSelectionPath();
                    if (selectionPath==null) {
                        return;
                    }
                    Object selectedItem = selectionPath.getLastPathComponent();
                    if (selectedItem==null) {
                        return;
                    }
                    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)selectedItem;
                    Object userObject = treeNode.getUserObject();
                    if (userObject==null || !(userObject instanceof NodeEntry)) {
                        return;
                    }
                    DefaultTreeModel treeModel = (DefaultTreeModel)tree.getModel();
                    NodeEntry nodeEntry = (NodeEntry)userObject;
                    NodeEntry.Type type = nodeEntry.getType();
                    
                    if (type==NodeEntry.Type.WHOLE || type==NodeEntry.Type.SPLITTINGPART) {
                        return;
                    }
                    
                    GeneralWrapper<NodeEntry> wrapper = new GeneralWrapper<NodeEntry>(
                        NodeEntry.builder(NodeEntry.Type.WHOLE).build()
                    );
                    SubsetNodeEditDialog dialog = new SubsetNodeEditDialog(parent, wrapper);
                    dialog.run();
                    
                    if (dialog.getResult()!=EditDialog.RESULT_OK) {
                        return;
                    }
                    
                    if (type==NodeEntry.Type.INVERSE) {
                        int childnum = treeNode.getChildCount();
                        if (childnum>0) {
                            treeNode.remove(0);
                        }
                    }
                    
                    DefaultMutableTreeNode newTreeNode = new DefaultMutableTreeNode();
                    newTreeNode.setUserObject(wrapper.get());
                    
                    treeNode.add(newTreeNode);
                    
                    treeModel.reload(treeNode);
                    refreshStatus();
                    expandAll();
                }
            }
            
        });
        JScrollPane treeScollPane = new JScrollPane(tree);
        treeOuter.add(treeScollPane, BorderLayout.CENTER);
        containerPanel.add(treeOuter, BorderLayout.CENTER);
    }

    @Override
    protected void load() {
        ResourceSubset subset = resourceSubsetWrapper.get();
        
        DefaultMutableTreeNode root = loadNode(subset);
        DefaultTreeModel model = new DefaultTreeModel(root);
        
        tree.setModel(model);
        expandAll();
    }

    @Override
    protected void save() {
        resourceSubsetWrapper.set(loadSubset((DefaultMutableTreeNode)tree.getModel().getRoot()));
    }
    
    private DefaultMutableTreeNode loadNode(ResourceSubset subset) {
        if (subset instanceof ResourceSubset.Whole) {
            return new DefaultMutableTreeNode(NodeEntry.builder(NodeEntry.Type.WHOLE).build());
        } else if (subset instanceof ResourceSubset.SplittingPart) {
            ResourceSubset.SplittingPart splittingPartSubset = (ResourceSubset.SplittingPart)subset;
            return new DefaultMutableTreeNode(
                NodeEntry.builder(NodeEntry.Type.SPLITTINGPART).setSplittingPart(
                    splittingPartSubset.getSplittingPart()
                ).build()
            );
        } else {
            NodeEntry nodeEntry;
            if (subset instanceof ResourceSubset.Union) {
                nodeEntry = NodeEntry.builder(NodeEntry.Type.UNION).build();
            } else if (subset instanceof ResourceSubset.Intersection) {
                nodeEntry = NodeEntry.builder(NodeEntry.Type.INTERSECTION).build();
            } else if (subset instanceof ResourceSubset.Difference) {
                nodeEntry = NodeEntry.builder(NodeEntry.Type.DIFFERENCE).build();
            } else if (subset instanceof ResourceSubset.SymmetricDifference) {
                nodeEntry = NodeEntry.builder(NodeEntry.Type.SYMMETRICDIFFERENCE).build();
            } else if (subset instanceof ResourceSubset.Inverse) {
                nodeEntry = NodeEntry.builder(NodeEntry.Type.INVERSE).build();
            } else {
                nodeEntry = NodeEntry.builder(NodeEntry.Type.UNION).build();
            }
            DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(nodeEntry);
            for (ResourceSubset childSubset : subset.getChildren()) {
                treeNode.add(loadNode(childSubset));
            }
            return treeNode;
        }
    }
    
    private ResourceSubset loadSubset(DefaultMutableTreeNode treeNode) {
        NodeEntry nodeEntry = (NodeEntry)treeNode.getUserObject();
        NodeEntry.Type type = nodeEntry.getType();
        if (type==NodeEntry.Type.WHOLE) {
            return new ResourceSubset.Whole(resource);
        } else if (type==NodeEntry.Type.SPLITTINGPART) {
            return new ResourceSubset.SplittingPart(nodeEntry.getSplittingPart());
        } else if (type==NodeEntry.Type.INVERSE) {
            return new ResourceSubset.Inverse(loadSubset((DefaultMutableTreeNode)treeNode.getChildAt(0)));
        } else {
            int childnum = treeNode.getChildCount();
            ResourceSubset[] subsets = new ResourceSubset[childnum];
            for (int i=0; i<childnum; i++) {
                subsets[i] = loadSubset((DefaultMutableTreeNode)treeNode.getChildAt(i));
            }
            switch (type) {
                case UNION:
                    return new ResourceSubset.Union(subsets);
                case INTERSECTION:
                    return new ResourceSubset.Intersection(subsets);
                case DIFFERENCE:
                    return new ResourceSubset.Difference(subsets);
                case SYMMETRICDIFFERENCE:
                    return new ResourceSubset.SymmetricDifference(subsets);
                default:
                    throw new RuntimeException();
            }
        }
    }
    
    private void refreshStatus() {
        invalidTreeNodes.clear();
        if (validateNode((DefaultMutableTreeNode)tree.getModel().getRoot())) {
            okButton.setEnabled(true);
        } else {
            okButton.setEnabled(false);
        }
    }
    
    private boolean validateNode(DefaultMutableTreeNode treeNode) {
        NodeEntry nodeEntry = (NodeEntry)treeNode.getUserObject();
        NodeEntry.Type type = nodeEntry.getType();
        int childnum = treeNode.getChildCount();
        if (type==NodeEntry.Type.WHOLE || type==NodeEntry.Type.SPLITTINGPART) {
            return true;
        }
        if (childnum==0) {
            invalidTreeNodes.add(treeNode);
            return false;
        }
        boolean result = true;
        for (int i=0; i<childnum; i++) {
            DefaultMutableTreeNode subTreeNode = (DefaultMutableTreeNode)treeNode.getChildAt(i);
            boolean subResult = validateNode(subTreeNode);
            result = (result && subResult);
        }
        return result;
    }
    
    private void expandAll() {
        for (int i=0; i<tree.getRowCount(); i++) {
            tree.expandRow(i);
        }
    }

    private static class NodeEntry {
        
        final Type type;
        
        final Resource.Splitting.Part splittingPart;

        public enum Type {
            WHOLE, SPLITTINGPART,
            UNION, INTERSECTION, DIFFERENCE, SYMMETRICDIFFERENCE,
            INVERSE,
        }

        NodeEntry(Builder builder) {
            this.type = builder.type;
            this.splittingPart = builder.splittingPart;
        }
        
        Type getType() {
            return type;
        }

        Resource.Splitting.Part getSplittingPart() {
            return splittingPart;
        }

        @Override
        public String toString() {
            switch (type) {
                case WHOLE:
                    return text("core.resourcesubset.type.whole");
                case SPLITTINGPART:
                    return splittingPart.getSplitting().getLabel()+" / "+splittingPart.getLabel();
                case UNION:
                    return text("core.resourcesubset.type.union");
                case INTERSECTION:
                    return text("core.resourcesubset.type.intersection");
                case DIFFERENCE:
                    return text("core.resourcesubset.type.difference");
                case SYMMETRICDIFFERENCE:
                    return text("core.resourcesubset.type.symmetricdifference");
                case INVERSE:
                    return text("core.resourcesubset.type.inverse");
                default:
                    return "?";
            }
        }
        
        static Builder builder(Type type) {
            return new Builder(type);
        }
        
        static class Builder {

            final Type type;
            
            Resource.Splitting.Part splittingPart = null;

            Builder(Type type) {
                this.type = type;
            }

            Builder setSplittingPart(Resource.Splitting.Part splittingPart) {
                this.splittingPart = splittingPart;
                return this;
            }

            NodeEntry build() {
                return new NodeEntry(this);
            }
            
        }
        
    }
    
    private class SubsetCellRenderer extends DefaultTreeCellRenderer {
        
        private static final long serialVersionUID = 1L;

        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            if (!(value instanceof DefaultMutableTreeNode)) {
                return super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
            }
            DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)value;
            
            Object userObject = treeNode.getUserObject();
            if (!(userObject instanceof NodeEntry)) {
                return super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
            }
            NodeEntry nodeEntry = (NodeEntry)userObject;
            
            JLabel label = new JLabel(value.toString());
            label.putClientProperty("html.disable", true);
            label.setIcon(IconLoader.loadIcon(getTypeIcon(nodeEntry.getType())));

            if (selected) {
                Color selectionBackgroundColor = UIManager.getDefaults().getColor("List.selectionBackground");
                if (selectionBackgroundColor != null) {
                    label.setOpaque(true);
                    label.setBackground(selectionBackgroundColor);
                } else {
                    label.setOpaque(false);
                }
            } else {
                Color defaultBackgroundColor = UIManager.getDefaults().getColor("List.background");
                if (defaultBackgroundColor != null) {
                    label.setOpaque(true);
                    label.setBackground(defaultBackgroundColor);
                    label.setOpaque(false);
                }
            }
            
            if (invalidTreeNodes.contains(value)) {
                label.setForeground(Color.RED);
            } else if (selected) {
                Color selectionColor = UIManager.getDefaults().getColor("List.selectionForeground");
                label.setForeground((selectionColor != null) ? selectionColor : Color.WHITE);
            } else {
                Color defaultColor = UIManager.getDefaults().getColor("List.foreground");
                label.setForeground((defaultColor != null) ? defaultColor : Color.BLACK);
            }
            
            return label;
        }
        
        private String getTypeIcon(NodeEntry.Type type) {
            switch (type) {
            case WHOLE:
                return "set_item";
            case SPLITTINGPART:
                return "set_item";
            case UNION:
                return "set_union";
            case INTERSECTION:
                return "set_intersection";
            case DIFFERENCE:
                return "set_difference";
            case SYMMETRICDIFFERENCE:
                return "set_symmetricdifference";
            case INVERSE:
                return "set_inverse";
            default:
                return "set_item";
            }
        }
        
    }
    
    private class SubsetNodeEditDialog extends EditDialog {
        
        private static final long serialVersionUID = 1L;

        private GeneralWrapper<NodeEntry> nodeEntryWrapper;
        
        private JComboBox<Labeled.PairWrapper<NodeEntry.Type>> typeCombo;
        
        private JComboBox<Labeled.PairWrapper<?>> splittingPartCombo;

        @SuppressWarnings("unchecked")
        private Labeled.PairWrapper<NodeEntry.Type>[] typeOptions = new Labeled.PairWrapper[]{
                new Labeled.PairWrapper<NodeEntry.Type>(text("core.resourcesubset.type.whole"), NodeEntry.Type.WHOLE),
                new Labeled.PairWrapper<NodeEntry.Type>(text("core.resourcesubset.type.splittingpart"), NodeEntry.Type.SPLITTINGPART),
                new Labeled.PairWrapper<NodeEntry.Type>(text("core.resourcesubset.type.union"), NodeEntry.Type.UNION),
                new Labeled.PairWrapper<NodeEntry.Type>(text("core.resourcesubset.type.intersection"), NodeEntry.Type.INTERSECTION),
                new Labeled.PairWrapper<NodeEntry.Type>(text("core.resourcesubset.type.difference"), NodeEntry.Type.DIFFERENCE),
                new Labeled.PairWrapper<NodeEntry.Type>(text("core.resourcesubset.type.symmetricdifference"), NodeEntry.Type.SYMMETRICDIFFERENCE),
                new Labeled.PairWrapper<NodeEntry.Type>(text("core.resourcesubset.type.inverse"), NodeEntry.Type.INVERSE),
        };

        private Labeled.PairWrapper<Object>[] splittingPartOptions;
        
        @SuppressWarnings({ "unchecked", "rawtypes" })
        public SubsetNodeEditDialog(JFrame parent, GeneralWrapper<NodeEntry> nodeEntryWrapper) {
            super(parent);
            this.title = text("ui.swing.dialog.resourcesubsetedit.subsetnodeedit.title");
            this.nodeEntryWrapper = nodeEntryWrapper;
            
            ArrayList<Labeled.PairWrapper<Object>> typeOptionsList = new ArrayList<Labeled.PairWrapper<Object>>();
            typeOptionsList.add(new Labeled.PairWrapper<Object>(" --- "+text("ui.swing.dialog.resourcesubsetedit.subsetnodeedit.choose")+" --- ", new Object()));
            for (Resource.Splitting splitting: resource.getSplittingManager().getSplittings()) {
                typeOptionsList.add(new Labeled.PairWrapper<Object>("[ "+splitting.getLabel()+" ]", new Object()));
                for (Resource.Splitting.Part splittingPart : splitting.getParts()) {
                    typeOptionsList.add(new Labeled.PairWrapper<Object>("     "+splittingPart.getLabel(), splittingPart));
                }
            }
            splittingPartOptions = (Labeled.PairWrapper<Object>[])Arrays.<Labeled.PairWrapper, Object>copyOf(typeOptionsList.toArray(), typeOptionsList.size(), Labeled.PairWrapper[].class);
            
            init();
        }

        @Override
        protected void build() {
            typeCombo = new JComboBox<Labeled.PairWrapper<NodeEntry.Type>>(typeOptions);
            typeCombo.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    refreshStatus();
                }
                
            });
            addRow(new MultilingualLabel("ui.swing.dialog.resourcesubsetedit.subsetnodeedit.type"), typeCombo);
            
            splittingPartCombo = new JComboBox<Labeled.PairWrapper<?>>(splittingPartOptions);
            splittingPartCombo.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    refreshStatus();
                }
                
            });
            addRow(new MultilingualLabel("ui.swing.dialog.resourcesubsetedit.subsetnodeedit.group"), splittingPartCombo);
        }

        @Override
        protected void load() {
            NodeEntry nodeEntry = nodeEntryWrapper.get();
            NodeEntry.Type type = nodeEntry.getType();
            Resource.Splitting.Part splittingPart = nodeEntry.getSplittingPart();
            
            int i;
            
            i = 0;
            for (Labeled.PairWrapper<NodeEntry.Type> typeOption: typeOptions) {
                if (typeOption.get()==type) {
                    typeCombo.setSelectedIndex(i);
                    break;
                }
                i++;
            }

            i = 0;
            for (Labeled.PairWrapper<Object> splittingPartOption: splittingPartOptions) {
                if (splittingPartOption.get()==splittingPart) {
                    splittingPartCombo.setSelectedIndex(i);
                    break;
                }
                i++;
            }
            
            refreshStatus();
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void save() {
            NodeEntry.Type type = ((Labeled.PairWrapper<NodeEntry.Type>)typeCombo.getSelectedItem()).get();
            Resource.Splitting.Part splittingPart = null;
            if (type == NodeEntry.Type.SPLITTINGPART) {
                Object splittingPartObject = ((Labeled.PairWrapper<Object>)splittingPartCombo.getSelectedItem()).get();
                if (splittingPartObject instanceof Resource.Splitting.Part) {
                    splittingPart = (Resource.Splitting.Part)splittingPartObject;
                }
            }
            NodeEntry nodeEntry = NodeEntry.builder(type).setSplittingPart(splittingPart).build();
            nodeEntryWrapper.set(nodeEntry);
        }
        
        @SuppressWarnings("unchecked")
        private void refreshStatus() {
            NodeEntry.Type type = ((Labeled.PairWrapper<NodeEntry.Type>)typeCombo.getSelectedItem()).get();
            Resource.Splitting.Part splittingPart = null;
            if (type == NodeEntry.Type.SPLITTINGPART) {
                Object splittingPartObject = ((Labeled.PairWrapper<Object>)splittingPartCombo.getSelectedItem()).get();
                if (splittingPartObject instanceof Resource.Splitting.Part) {
                    splittingPart = (Resource.Splitting.Part)splittingPartObject;
                }
            }
            if (type==NodeEntry.Type.SPLITTINGPART) {
                splittingPartCombo.setEnabled(true);
                okButton.setEnabled(splittingPart!=null);
            } else {
                splittingPartCombo.setEnabled(false);
                okButton.setEnabled(true);
            }
        }
        
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.i18n;

import javax.swing.JCheckBox;


public class MultilingualCheckBox extends JCheckBox implements MultilingualComponent {

    private static final long serialVersionUID = 1L;

    protected MultilingualContent labelContent;

    protected MultilingualContent tooltipContent;

    public MultilingualCheckBox() {
        setLanguagePath("");
    }

    public MultilingualCheckBox(String labelPath) {
        setLanguagePath(labelPath);
    }

    public MultilingualCheckBox(String labelPath, String tooltipPath) {
        setLanguagePath(labelPath, tooltipPath);
    }

    public void setLanguagePath(String labelPath) {
        setLanguagePath(labelPath, "");
    }

    public void setLanguagePath(String labelPath, String tooltipPath) {
        setLanguagePath(new PathMultilingualContent(labelPath), new PathMultilingualContent(tooltipPath));
        reloadLanguageTexts();
    }

    public void setLanguagePath(MultilingualContent labelContent) {
        setLanguagePath(labelContent, new PathMultilingualContent(""));
    }
    
    public void setLanguagePath(MultilingualContent labelContent, MultilingualContent tooltipContent) {
        this.labelContent = labelContent;
        this.tooltipContent = tooltipContent;
        reloadLanguageTexts();
    }
    
    @Override
    public void reloadLanguageTexts() {
        setText(labelContent.toString());
        setToolTipText(tooltipContent.toStringOrNull());
    }

}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Labeled.Wrapper;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.BlockFilterPanel;

public class BlockSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<Block> blockWrapper;
    
    private Set<Block> markedBlocks = new HashSet<Block>();
    
    private BlockFilterPanel blockFilterPanel;
    
    private JList<Labeled.Wrapper> blockListDisplay;
    
    private Block block;

    public BlockSelectDialog(JFrame parent, Document document, GeneralWrapper<Block> blockWrapper) {
        this(parent, document, blockWrapper, null);
    }
    
    public BlockSelectDialog(
        JFrame parent, Document document, GeneralWrapper<Block> blockWrapper,
        Collection<Block> markedBlocks
    ) {
        super(parent);
        this.title = text("ui.swing.dialog.blockselect.title");
        this.document = document;
        this.blockWrapper = blockWrapper;
        this.block = blockWrapper.get();
        if (markedBlocks != null) {
            this.markedBlocks.addAll(markedBlocks);
        }
        init(false, true);
    }
    
    @Override
    protected void build() {
        blockFilterPanel = new BlockFilterPanel(parent, document);
        blockFilterPanel.setColorAspectVisible(false);
        blockFilterPanel.addChangeListener(new BlockFilterPanel.BlockFilterChangeListener() {
            
            @Override
            public void changed(BlockFilterPanel.BlockFilterChangeEvent ev) {
                reloadList();
                reloadState();
            }
        });
        containerPanel.add(blockFilterPanel, BorderLayout.PAGE_START);
        
        blockListDisplay = new JList<Labeled.Wrapper>();
        blockListDisplay.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        blockListDisplay.addListSelectionListener(new ListSelectionListener() {
            
            @Override
            public void valueChanged(ListSelectionEvent ev) {
                Labeled.Wrapper wrapper = blockListDisplay.getSelectedValue();
                if (wrapper==null) {
                    BlockSelectDialog.this.block = null;
                } else {
                    BlockSelectDialog.this.block = (Block)wrapper.get();
                }
                reloadState();
            }
            
        });
        blockListDisplay.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getClickCount()==2) {
                    Labeled.Wrapper wrapper = blockListDisplay.getSelectedValue();
                    if (wrapper!=null) {
                        BlockSelectDialog.this.block = (Block)wrapper.get();
                        fireOk();
                    }
                }
            }
            
        });
        final ListCellRenderer<? super Labeled.Wrapper> originalRenderer = blockListDisplay.getCellRenderer();
        blockListDisplay.setCellRenderer(new ListCellRenderer<Labeled.Wrapper>() {

            @Override
            public Component getListCellRendererComponent(
                JList<? extends Wrapper> list, Wrapper value, int index,
                boolean isSelected, boolean cellHasFocus
            ) {
                Component component = originalRenderer.getListCellRendererComponent(
                    list, value, index, isSelected, cellHasFocus
                );
                if (component instanceof JLabel) {
                    JLabel label = (JLabel)component;
                    if (markedBlocks.contains(value.get())) {
                        Font font = label.getFont();
                        Map<TextAttribute, Object> fontAttributes = new HashMap<TextAttribute, Object>(
                            font.getAttributes()
                        );
                        fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                        fontAttributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
                        label.setFont(font.deriveFont(fontAttributes));
                    }
                }
                return component;
            }
            
        });
        
        JScrollPane blockListScrollPane = new JScrollPane(blockListDisplay);
        blockListScrollPane.setPreferredSize(new Dimension(200, 230));
        containerPanel.add(blockListScrollPane, BorderLayout.CENTER);
    }

    @Override
    protected void load() {
        reloadList();
        reloadState();
    }

    @Override
    protected void save() {
        blockWrapper.set(block);
    }
    
    protected void reloadList() {
        DefaultComboBoxModel<Labeled.Wrapper> model = new DefaultComboBoxModel<Labeled.Wrapper>();
        List<Block> filteredBlocks = document.getBlockStore().getAll().filter(blockFilterPanel.getBlockFilter());
        int i = 0;
        int newSelectedIndex = -1;
        for (Block block : filteredBlocks) {
            model.addElement(new Labeled.Wrapper(block));
            if (block==this.block) {
                newSelectedIndex = i;
            }
            i++;
        }
        blockListDisplay.setModel(model);
        if (newSelectedIndex>=0) {
            blockListDisplay.setSelectedIndex(newSelectedIndex);
        }
    }
    
    protected void reloadState() {
        if (this.block==null) {
            okButton.setEnabled(false);
        } else {
            okButton.setEnabled(true);
        }
    }

}

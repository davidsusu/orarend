package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.HtmlView;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;


public class AboutDialog extends Dialog {
    
    private static final long serialVersionUID = 1L;

    public AboutDialog(JFrame parent) {
        super(parent);
        this.title = text("ui.swing.dialog.about.title");
        setResizable(false);
        init();
    }

    @Override
    protected void build() {
        JPanel leftPanel = new JPanel();
        containerPanel.add(leftPanel, BorderLayout.CENTER);
        
        JLabel iconLabel = new JLabel(IconLoader.loadIcon("main_128"));
        leftPanel.add(iconLabel);
        
        
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BorderLayout());
        containerPanel.add(rightPanel, BorderLayout.LINE_END);
        
        HtmlView infoView = new HtmlView(
            HtmlView.htmlencode(Info.NAME + " " + Info.MAJOR_VERSION + "." + Info.MINOR_VERSION + "." + Info.PATCH_VERSION) + "<br />" +
            HtmlView.htmlencode(Info.AUTHOR) + "<br />" +
            "<br /><br />" +
            "<a href=\"" + HtmlView.htmlencode(Info.WEBSITE) + "\">" +
            HtmlView.htmlencode(Info.WEBSITE_NAME) +
            "</a>"
        );
        rightPanel.add(infoView, BorderLayout.CENTER);
        
        JButton closeButton = new MultilingualButton("ui.swing.dialog.about.close");
        closeButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
            
        });
        rightPanel.add(closeButton, BorderLayout.PAGE_END);
    }
    
}

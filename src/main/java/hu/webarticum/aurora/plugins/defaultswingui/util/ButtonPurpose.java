package hu.webarticum.aurora.plugins.defaultswingui.util;

import hu.webarticum.aurora.core.model.Color;

public enum ButtonPurpose {

    NONE(),
    DANGER(new Color("#DD2211"), Color.BLACK, Color.WHITE, true),
    WARNING(new Color("#775500"), Color.BLACK, Color.WHITE, true),
    SAVE(new Color("#448822"), new Color("#113300"), Color.WHITE, true),
    ABORT(new Color("#EEDD33"), new Color("557711"), new Color("557711"), false),
    ACTION(new Color("#4433DD"), Color.BLACK, Color.WHITE, true),
    ;
    
    
    private final Color backgroundColor;
    
    private final Color borderColor;
    
    private final Color fontColor;
    
    private final boolean fontBold;
    

    private ButtonPurpose() {
        this.backgroundColor = Color.WHITE;
        this.borderColor = Color.BLACK;
        this.fontColor = Color.BLACK;
        this.fontBold = false;
    }
    
    private ButtonPurpose(Color backgroundColor, Color borderColor, Color fontColor, boolean fontBold) {
        this.backgroundColor = backgroundColor;
        this.borderColor = borderColor;
        this.fontColor = fontColor;
        this.fontBold = fontBold;
    }

    
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public boolean isFontBold() {
        return fontBold;
    }

}

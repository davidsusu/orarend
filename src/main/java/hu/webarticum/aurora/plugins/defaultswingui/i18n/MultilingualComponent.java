package hu.webarticum.aurora.plugins.defaultswingui.i18n;

public interface MultilingualComponent {

    public void reloadLanguageTexts();
    
}

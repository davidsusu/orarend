package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hu.webarticum.aurora.plugins.defaultswingui.util.AutoExtensionFileChooser;


public class FileChooserField extends JPanel {

    private static final long serialVersionUID = 1L;

    
    protected JTextField textField;
    
    protected JButton button;
    
    protected JFileChooser chooser;
    
    protected int dialogType;
    
    
    public FileChooserField() {
        this("", null, JFileChooser.OPEN_DIALOG);
    }

    public FileChooserField(File file) {
        this(file.getAbsolutePath(), null, JFileChooser.OPEN_DIALOG);
    }

    public FileChooserField(String path) {
        this(path, null, JFileChooser.OPEN_DIALOG);
    }

    public FileChooserField(File file, JFileChooser chooser, int dialogType) {
        this(file.getAbsolutePath(), chooser, dialogType);
    }

    public FileChooserField(String path, final JFileChooser chooser, int dialogType) {
        textField = new JTextField();
        textField.setText(path);
        button = new JButton();
        button.setText("...");
        
        this.chooser = chooser;
        
        setLayout(new BorderLayout());
        add(textField, BorderLayout.CENTER);
        add(button, BorderLayout.LINE_END);
        
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                openDialog();
            }
            
        });
    }
    
    protected JFileChooser getFallbackChooser() {
        JFileChooser chooser = new JFileChooser();
        
        // TODO
        
        return chooser;
    }
    
    public boolean openDialog() {
        JFileChooser chooser = FileChooserField.this.chooser;
        if (chooser == null) {
            chooser = getFallbackChooser();
        }
        
        File currentFile = getFile();
        
        chooser.setCurrentDirectory(currentFile.getParentFile());
        chooser.setSelectedFile(currentFile);
        
        int result = 0;
        
        if (dialogType == JFileChooser.OPEN_DIALOG) {
            result = chooser.showOpenDialog(FileChooserField.this);
        } else if (dialogType == JFileChooser.SAVE_DIALOG) {
            result = chooser.showSaveDialog(FileChooserField.this);
        }
        
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile;
            if (dialogType == JFileChooser.SAVE_DIALOG && chooser instanceof AutoExtensionFileChooser) {
                selectedFile = ((AutoExtensionFileChooser)chooser).getAutoSelectedFile();
            } else {
                selectedFile = chooser.getSelectedFile();
            }
            String selectedPath = selectedFile.getAbsolutePath();
            
            textField.setText(selectedPath);
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        textField.setEnabled(enabled);
        button.setEnabled(enabled);
    }
    
    public JTextField getTextField() {
        return textField;
    }
    
    public JButton getButton() {
        return button;
    }

    public String getPath() {
        return textField.getText();
    }

    public File getFile() {
        return new File(textField.getText());
    }

    public JFileChooser getFileChooser() {
        return chooser;
    }

    public void setFileChooser(JFileChooser chooser) {
        this.chooser = chooser;
    }
    
    public void setDialogType(int dialogType) {
        this.dialogType = dialogType;
    }
    
    public boolean isEmpty() {
        return textField.getText().isEmpty();
    }

    protected JFileChooser getChooserInternal() {
        if (chooser != null) {
            return chooser;
        } else {
            return new JFileChooser();
        }
    }

}

package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class IconButtonSelectDialog extends Dialog {

    private static final long serialVersionUID = 1L;

    private final IconButtonSelectDialog previous;
    
    private final Item[] items;
    
    private JPanel iconPanel;

    public IconButtonSelectDialog(JFrame parent, String title, Item... items) {
        this(parent, null, title, items);
    }
    
    public IconButtonSelectDialog(JFrame parent, IconButtonSelectDialog previous, String title, Item... items) {
        super(parent);
        setResizable(false);
        this.previous = previous;
        this.title = title;
        this.items = items;
        init();
    }

    @Override
    protected void build() {
        iconPanel = new JPanel();
        iconPanel.setLayout(new BoxLayout(iconPanel, BoxLayout.X_AXIS));
        containerPanel.add(iconPanel, BorderLayout.CENTER);
        
        if (previous != null) {
            JPanel leftPanel = new JPanel(new BorderLayout());
            containerPanel.add(leftPanel, BorderLayout.LINE_START);
            JButton previousButton = new JButton(IconLoader.loadIcon("back-mini"));
            previousButton.setToolTipText(text("ui.swing.dialog.iconbuttonselect.back"));
            previousButton.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 40));
            previousButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            previousButton.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    closeDialog();
                    previous.run();
                }
                
            });
            leftPanel.add(previousButton, BorderLayout.PAGE_START);
        }
        
        for (final Item item: items) {
            JButton button = new JButton(item.label, item.icon);
            if (item.tooltip != null) {
                button.setToolTipText(item.tooltip);
            }
            button.setVerticalTextPosition(JButton.BOTTOM);
            button.setHorizontalTextPosition(JButton.CENTER);
            button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            button.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    closeDialog();
                    item.runnable.run();
                }
                
            });
            iconPanel.add(button);
        }
    }
    
    public static class Item {
        
        private final String label;
        private final String tooltip;
        private final Icon icon;
        private final Runnable runnable;

        public Item(String label, Icon icon, Runnable runnable) {
            this(label, null, icon, runnable);
        }
        
        public Item(String label, String tooltip, Icon icon, Runnable runnable) {
            this.label = label;
            this.tooltip = tooltip;
            this.icon = icon;
            this.runnable = runnable;
        }
        
    }

}

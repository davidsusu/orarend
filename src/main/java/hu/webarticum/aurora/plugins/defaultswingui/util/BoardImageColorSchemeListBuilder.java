package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.util.ArrayList;
import java.util.List;

import hu.webarticum.aurora.app.util.common.Pair;

public class BoardImageColorSchemeListBuilder {

    public List<Pair<String, ActivityFlowImageRenderer.ColorScheme>> getColorSchemes() {
        List<Pair<String, ActivityFlowImageRenderer.ColorScheme>> colorSchemes = new ArrayList<Pair<String, ActivityFlowImageRenderer.ColorScheme>>();
        
        colorSchemes.add(new Pair<String, ActivityFlowImageRenderer.ColorScheme>(
            "ui.swing.main.tables.preview.theme.default",
            new ActivityFlowImageRenderer.ColorScheme(){
    
                {
                    applyActivityColors = true;
    
                    background = new java.awt.Color(0xFFFFFF);
                    
                    headerBorder = new java.awt.Color(0x6B4321);
                    headerBackground = new java.awt.Color(0xDE6600);
                    headerLabel = new java.awt.Color(0xEDE3BB);
                    columnBorder = new java.awt.Color(0xA89671);
                    columnBackground = new java.awt.Color(0xE4DDC4);
                    startLine = new java.awt.Color(0x9D8F83);
                    startLabel = new java.awt.Color(0xE9954F);
                    
                    activityBorder = new java.awt.Color(0x616E42);
                    activityBackground = new java.awt.Color(0xB5C68A);
                    activityLabel = new java.awt.Color(0x75520E);
                    activityLabelBackground = new java.awt.Color(0xB5C68A);
                }
                
            }
        ));
        
        colorSchemes.add(new Pair<String, ActivityFlowImageRenderer.ColorScheme>(
            "ui.swing.main.tables.preview.theme.elegant",
            new ActivityFlowImageRenderer.ColorScheme(){
    
                {
                    applyActivityColors = true;
    
                    background = new java.awt.Color(0xFFFFFF);
                    
                    headerBorder = new java.awt.Color(0x640F0F);
                    headerBackground = new java.awt.Color(0xB12525);
                    headerLabel = new java.awt.Color(0xE2E2E2);
                    columnBorder = new java.awt.Color(0xAD9090);
                    columnBackground = new java.awt.Color(0xF2EDED);
                    startLine = new java.awt.Color(0x6F6F6F);
                    startLabel = new java.awt.Color(0xC26666);
                    
                    activityBorder = new java.awt.Color(0xC95F5F);
                    activityBackground = new java.awt.Color(0xD7A8A8);
                    activityLabel = new java.awt.Color(0x612626);
                    activityLabelBackground = new java.awt.Color(0xD7A8A8);
                }
                
            }
        ));
        colorSchemes.add(new Pair<String, ActivityFlowImageRenderer.ColorScheme>(
            "ui.swing.main.tables.preview.theme.blue",
            new ActivityFlowImageRenderer.ColorScheme(){
    
                {
                    applyActivityColors = true;
    
                    background = new java.awt.Color(0xFFFFFF);
                    
                    headerBorder = new java.awt.Color(0x132A77);
                    headerBackground = new java.awt.Color(0x4363C5);
                    headerLabel = new java.awt.Color(0xFFFFFF);
                    columnBorder = new java.awt.Color(0x132A77);
                    columnBackground = new java.awt.Color(0xAAB9E8);
                    startLine = new java.awt.Color(0x858EA8);
                    startLabel = new java.awt.Color(0x4363C5);
                    
                    activityBorder = new java.awt.Color(0x132A77);
                    activityBackground = new java.awt.Color(0x4363C5);
                    activityLabel = new java.awt.Color(0xFFFFFF);
                    activityLabelBackground = new java.awt.Color(0x4363C5);
                }
                
            }
        ));
        colorSchemes.add(new Pair<String, ActivityFlowImageRenderer.ColorScheme>(
            "ui.swing.main.tables.preview.theme.pink",
            new ActivityFlowImageRenderer.ColorScheme(){
    
                {
                    applyActivityColors = true;
    
                    background = new java.awt.Color(0xFFFFFF);
                    
                    headerBorder = new java.awt.Color(0x690737);
                    headerBackground = new java.awt.Color(0xA42B66);
                    headerLabel = new java.awt.Color(0xFFFFFF);
                    columnBorder = new java.awt.Color(0x690737);
                    columnBackground = new java.awt.Color(0xF169AB);
                    startLine = new java.awt.Color(0x858EA8);
                    startLabel = new java.awt.Color(0xA42B66);
                    
                    activityBorder = new java.awt.Color(0x690737);
                    activityBackground = new java.awt.Color(0xA42B66);
                    activityLabel = new java.awt.Color(0xFFFFFF);
                    activityLabelBackground = new java.awt.Color(0xA42B66);
                }
                
            }
        ));
        colorSchemes.add(new Pair<String, ActivityFlowImageRenderer.ColorScheme>(
            "ui.swing.main.tables.preview.theme.minimalist",
            new ActivityFlowImageRenderer.ColorScheme(){
    
                {
                    applyActivityColors = false;
    
                    background = new java.awt.Color(0xFFFFFF);
                    
                    headerBorder = new java.awt.Color(0xFFFFFF);
                    headerBackground = new java.awt.Color(0xFFFFFF);
                    headerLabel = new java.awt.Color(0x999999);
                    columnBorder = new java.awt.Color(0xFFFFFF);
                    columnBackground = new java.awt.Color(0xFFFFFF);
                    startLine = new java.awt.Color(0xCCCCCC);
                    startLabel = new java.awt.Color(0x999999);
                    
                    activityBorder = new java.awt.Color(0xDDDDDD);
                    activityBackground = new java.awt.Color(0xDDDDDD);
                    activityLabel = new java.awt.Color(0x555555);
                    activityLabelBackground = new java.awt.Color(0xDDDDDD);
                }
                
            }
        ));
        colorSchemes.add(new Pair<String, ActivityFlowImageRenderer.ColorScheme>(
            "ui.swing.main.tables.preview.theme.printable",
            new ActivityFlowImageRenderer.ColorScheme(){
    
                {
                    applyActivityColors = false;
    
                    background = new java.awt.Color(0xFFFFFF);
                    
                    headerBorder = new java.awt.Color(0x000000);
                    headerBackground = new java.awt.Color(0xFFFFFF);
                    headerLabel = new java.awt.Color(0x000000);
                    columnBorder = new java.awt.Color(0x000000);
                    columnBackground = new java.awt.Color(0xFFFFFF);
                    startLine = new java.awt.Color(0xCCCCCC);
                    startLabel = new java.awt.Color(0x000000);
                    
                    activityBorder = new java.awt.Color(0x000000);
                    activityBackground = new java.awt.Color(0xFFFFFF);
                    activityLabel = new java.awt.Color(0x000000);
                    activityLabelBackground = new java.awt.Color(0xFFFFFF);
                }
                
            }
        ));
        colorSchemes.add(new Pair<String, ActivityFlowImageRenderer.ColorScheme>(
            "ui.swing.main.tables.preview.theme.contrast",
            new ActivityFlowImageRenderer.ColorScheme(){
    
                {
                    applyActivityColors = false;
    
                    background = new java.awt.Color(0x000000);
                    
                    headerBorder = new java.awt.Color(0xFFFFFF);
                    headerBackground = new java.awt.Color(0x000000);
                    headerLabel = new java.awt.Color(0xFFFF00);
                    columnBorder = new java.awt.Color(0xFFFFFF);
                    columnBackground = new java.awt.Color(0x000000);
                    startLine = new java.awt.Color(0xFFFFFF);
                    startLabel = new java.awt.Color(0xFFFF00);
                    
                    activityBorder = new java.awt.Color(0xFFFFFF);
                    activityBackground = new java.awt.Color(0x333333);
                    activityLabel = new java.awt.Color(0xFFFF00);
                    activityLabelBackground = new java.awt.Color(0x333333);
                }
                
            }
        ));
        
        return colorSchemes;
    }
    
}

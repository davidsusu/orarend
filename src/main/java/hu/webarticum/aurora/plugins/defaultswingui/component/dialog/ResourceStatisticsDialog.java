package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import javax.swing.JFrame;
import javax.swing.JLabel;

import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Resource;


public class ResourceStatisticsDialog extends EditDialog {
    
    private static final long serialVersionUID = 1L;


    private final Document document;
    
    private final Resource resource;
    
    private int blockCount = 0;
    
    private int activityCount = 0;

    private JLabel blockCountLabel;

    private JLabel activityCountLabel;
    
    
    public ResourceStatisticsDialog(JFrame parent, Document document, Resource resource) {
        super(parent);
        this.title = text("ui.swing.dialog.resourcestatistics.title");
        this.formWidth = 320;
        this.document = document;
        this.resource = resource;
        init();
    }

    
    @Override
    protected void build() {
        blockCountLabel = new JLabel();
        addRow(text("ui.swing.dialog.resourcestatistics.blocks"), blockCountLabel);

        activityCountLabel = new JLabel();
        addRow(text("ui.swing.dialog.resourcestatistics.activities"), activityCountLabel);
        
        // FIXME
        okButton.getParent().remove(okButton);
    }

    @Override
    protected void load() {
        ActivityFilter activityFilter = new ActivityFilter.HasResource(resource);
        
        blockCount = document.getBlockStore().getAll().filter(new BlockFilter.ActivityBlockFilter(activityFilter)).size();
        activityCount = document.getBlockStore().getAll().getActivities().filter(activityFilter).size();
        
        refreshLabels();
    }
    
    private void refreshLabels() {
        blockCountLabel.setText(String.format(text("ui.swing.dialog.resourcestatistics.n_blocks"), blockCount));
        activityCountLabel.setText(String.format(text("ui.swing.dialog.resourcestatistics.n_activities"), activityCount));
    }


    @Override
    protected void save() {
        // nothing to do
    }

}

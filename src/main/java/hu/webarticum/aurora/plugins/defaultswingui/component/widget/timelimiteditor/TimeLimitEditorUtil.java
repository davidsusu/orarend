package hu.webarticum.aurora.plugins.defaultswingui.component.widget.timelimiteditor;

import java.util.Collections;
import java.util.List;

import hu.webarticum.aurora.core.model.time.CustomTimeLimit;
import hu.webarticum.aurora.core.model.time.Time;

public class TimeLimitEditorUtil {

    public static Time getPreviousSwitch(CustomTimeLimit customTimeLimit, Time baseTime, boolean inclusive) {
        Time previousTime = null;
        for (Time currentTime: customTimeLimit.getTimes()) {
            int cmp = baseTime.compareTo(currentTime);
            if (cmp > 0) {
                previousTime = currentTime;
            } else if (inclusive && cmp == 0) {
                return currentTime;
            }
        }
        return previousTime;
    }

    public static Time getNextSwitch(CustomTimeLimit customTimeLimit, Time baseTime, boolean inclusive) {
        for (Time currentTime: customTimeLimit.getTimes()) {
            int cmp = baseTime.compareTo(currentTime);
            if (cmp < 0 || (inclusive && cmp == 0)) {
                return currentTime;
            }
        }
        return null;
    }

    public static CustomTimeLimit changeTimeLimit(CustomTimeLimit originalTimeLimit, Time originalTime, Time newTime) {
        boolean startState = originalTimeLimit.getStartState();
        List<Time> switches = originalTimeLimit.getTimes();
        switches.remove(originalTime);
        switches.add(newTime);
        Collections.sort(switches);
        return new CustomTimeLimit(startState, switches);
    }
    
    public static CustomTimeLimit extendSwitch(CustomTimeLimit customTimeLimit, Time baseTime, boolean direction) {
        Time otherTime = direction ? getNextSwitch(customTimeLimit, baseTime, false) : getPreviousSwitch(customTimeLimit, baseTime, false);
        boolean newStartState = customTimeLimit.getStartState();
        if (!direction && otherTime == null) {
            newStartState = !newStartState;
        }
        
        List<Time> newTimes = customTimeLimit.getTimes();
        newTimes.remove(baseTime);
        if (otherTime != null) {
            newTimes.remove(otherTime);
        }
        
        return new CustomTimeLimit(newStartState, newTimes);
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Insets;
import java.io.File;

import javax.swing.JComponent;
import javax.swing.Painter;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.CommonExtensions;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.util.i18n.CombinedMultiLanguageTextRepository;
import hu.webarticum.aurora.app.util.i18n.PropertiesLanguageTextRepository;
import hu.webarticum.aurora.app.util.i18n.TreeMultiLanguageTextRepository;
import hu.webarticum.aurora.plugins.defaultswingui.component.main.MainWindow;
import hu.webarticum.aurora.x.announcements.AnnouncementClient;

public class DefaultSwingUiRunner implements CommonExtensions.Runner {

    private static final int ANNOUNCEMENT_FETCHING_INTERVAL = 60000;
    
    @Override
    public Type getType() {
        return Type.UI;
    }
    
    @Override
    public boolean isApplicable() {
        try {
            Class.forName("javax.swing.JComponent");
        } catch (ClassNotFoundException e) {
            return false;
        }
        return true;
    }

    @Override
    public void run() {
        initLanguages();
        initLookAndFeel();
        startAnnouncementFetching();
        showMainWindow();
    }

    private void initLanguages() {
        TreeMultiLanguageTextRepository textRepository = Application.instance().getTextRepository();
        String[] topics = {"main", "dialog", "util", "widget"};
        for (String topic: topics) {
            CombinedMultiLanguageTextRepository langRepository = new CombinedMultiLanguageTextRepository();
            langRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/plugins/defaultswingui/lang/hu_HU/"+topic+".hu_HU.properties", "hu_HU"));
            langRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/plugins/defaultswingui/lang/en_US/"+topic+".en_US.properties", "en_US"));
            textRepository.mount("ui.swing."+topic, langRepository);
        }
        CombinedMultiLanguageTextRepository javaSwingLangRepository = new CombinedMultiLanguageTextRepository();
        javaSwingLangRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/plugins/defaultswingui/lang/hu_HU/swing.hu_HU.properties", "hu_HU"));
        javaSwingLangRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/plugins/defaultswingui/lang/en_US/swing.en_US.properties", "en_US"));
        textRepository.mount("java.swing", javaSwingLangRepository);
    }
    
    private void startAnnouncementFetching() {
        Timer timer = new Timer(ANNOUNCEMENT_FETCHING_INTERVAL, ev -> {
            if (Application.instance().getSettings().get("enable_fetching_announcements", "1").equals("1")) {
                new AnnouncementClient().run(true);
            }
        });
        timer.setInitialDelay(0);
        timer.start();
    }
    
    private void showMainWindow() {
        SwingUtilities.invokeLater(() -> {
            MainWindow mainWindow = new MainWindow();
            String inputFilePath = Application.instance().getArgumentMap().get("input-file");
            if (inputFilePath != null && !inputFilePath.isEmpty()) {
                mainWindow.run(new File(inputFilePath));
            } else {
                mainWindow.run();
            }
        });
    }
    
    public static void initLookAndFeel() {
        for (UIManager.LookAndFeelInfo laf: UIManager.getInstalledLookAndFeels()) {
            if (laf.getName().equals("Nimbus")) {
                try {
                    UIManager.setLookAndFeel(laf.getClassName());
                    UIManager.put("nimbusBase", new Color(170, 160, 150));
                    UIManager.put("nimbusBlueGrey", new Color(200, 200, 190));
                    UIManager.put("control", new Color(225, 220, 210));
                    UIManager.put("MenuBar[Enabled].backgroundPainter", (Painter<JComponent>) (g, component, width, height) -> {
                        GradientPaint gradient = new GradientPaint(0, 0, new Color(235, 230, 220), 0, height, new Color(245, 240, 240));
                        g.setPaint(gradient);
                        g.fillRect(0, 0, width, height);
                    });
                    UIManager.put("PopupMenuSeparator.contentMargins", new Insets(7, 0, 7, 0));
                } catch (Exception e) { }
            }
        }
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Cursor;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragSource;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.TransferHandler;

/**
 * Based on: https://stackoverflow.com/a/4769575/3948862
 */

public class TableRowTransferHandler extends TransferHandler {
    
    private static final long serialVersionUID = 1L;
    
    private final DataFlavor localObjectFlavor = new ActivationDataFlavor(
        Integer.class, "application/x-java-Integer;class=java.lang.Integer", "Table Row Index"
    );
    
    private final JTable table;

    public TableRowTransferHandler(JTable table) {
        this.table = table;
    }

    @Override
    protected Transferable createTransferable(JComponent component) {
        return new DataHandler(Integer.valueOf(table.getSelectedRow()), localObjectFlavor.getMimeType());
    }

    @Override
    public boolean canImport(TransferHandler.TransferSupport transferSupport) {
       boolean can = (
           transferSupport.getComponent() == table &&
           transferSupport.isDrop() &&
           transferSupport.isDataFlavorSupported(localObjectFlavor)
       );
       table.setCursor(can ? DragSource.DefaultMoveDrop : DragSource.DefaultMoveNoDrop);
       return can;
    }

    @Override
    public int getSourceActions(JComponent component) {
        return TransferHandler.MOVE;
    }

    @Override
    public boolean importData(TransferHandler.TransferSupport transferSupport) {
        JTable targetTable = (JTable)transferSupport.getComponent();
        JTable.DropLocation dropLocation = (JTable.DropLocation)transferSupport.getDropLocation();
        int toIndex = dropLocation.getRow();
        int max = table.getModel().getRowCount();
        if (toIndex < 0 || toIndex > max) {
            toIndex = max;
        }
        targetTable.setCursor(Cursor.getDefaultCursor());
        try {
            Integer rowFrom = (Integer)transferSupport.getTransferable().getTransferData(localObjectFlavor);
            if (rowFrom != -1 && rowFrom != toIndex) {
                ((Reorderable)table.getModel()).move(rowFrom, toIndex);
                if (toIndex > rowFrom) {
                    toIndex--;
                }
                targetTable.getSelectionModel().addSelectionInterval(toIndex, toIndex);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void exportDone(JComponent c, Transferable t, int act) {
        if (act == TransferHandler.MOVE || act == TransferHandler.NONE) {
            table.setCursor(Cursor.getDefaultCursor());
        }
    }
    
    public interface Reorderable {
        
        public void move(int fromIndex, int toIndex);
        
    }
 }
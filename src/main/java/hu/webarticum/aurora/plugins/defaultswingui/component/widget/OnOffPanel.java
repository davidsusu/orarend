package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class OnOffPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    JComponent titleComponent;
    
    JCheckBox onOffCheckbox;
    
    JComponent innerComponent;
    
    public OnOffPanel(String title, String onOffLabel, JComponent innerComponent) {
        this(new JLabel(title), onOffLabel, innerComponent);
    }

    public OnOffPanel(JComponent titleComponent, String onOffLabel, JComponent innerComponent) {
        this.titleComponent = titleComponent;
        this.onOffCheckbox = new JCheckBox(onOffLabel, false);
        this.innerComponent = innerComponent;
        build();
    }
    
    protected void build() {
        setBorder(new CompoundBorder(
            new LineBorder(new java.awt.Color(150, 150, 150)),
            new EmptyBorder(12, 12, 12, 12)
        ));
        setLayout(new BorderLayout());
        
        JPanel topPanel = new JPanel();
        topPanel.setOpaque(false);
        topPanel.setBorder(new EmptyBorder(0, 0, 10, 0));
        topPanel.setLayout(new BorderLayout());
        add(topPanel, BorderLayout.PAGE_START);

        topPanel.add(titleComponent, BorderLayout.LINE_START);
        
        onOffCheckbox.addItemListener(new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                refreshState();
            }
            
        });
        topPanel.add(onOffCheckbox, BorderLayout.LINE_END);
        
        add(innerComponent, BorderLayout.CENTER);
        
        refreshState();
    }
    
    public void setOn(boolean on) {
        onOffCheckbox.setSelected(on);
    }
    
    public boolean isOn() {
        return onOffCheckbox.isSelected();
    }
    
    public void addItemListener(ItemListener listener) {
        onOffCheckbox.addItemListener(listener);
    }

    public void removeItemListener(ItemListener listener) {
        onOffCheckbox.removeItemListener(listener);
    }
    
    public JComponent getInnerComponent() {
        return innerComponent;
    }
    
    protected void refreshState() {
        if (isOn()) {
            setBackground(new java.awt.Color(230, 240, 200));
        } else {
            setBackground(new java.awt.Color(255, 200, 200));
        }
    }
    
}

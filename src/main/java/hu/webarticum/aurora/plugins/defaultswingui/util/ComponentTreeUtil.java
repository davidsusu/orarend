package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JComponent;

// TODO: disable panel?

public class ComponentTreeUtil {
    
    public static JComponent findByProperty(
        Component component,
        Object key,
        Object value
    ) {
        if (component instanceof JComponent) {
            JComponent jComponent = (JComponent)component;
            Object currentValue = jComponent.getClientProperty(key);
            if (currentValue != null && currentValue.equals(value)) {
                return jComponent;
            }
        }
        if (component instanceof Container) {
            Container container = (Container)component;
            for (Component subComponent: container.getComponents()) {
                if (subComponent instanceof JComponent) {
                    JComponent foundComponent = findByProperty((JComponent)subComponent, key, value);
                    if (foundComponent != null) {
                        return foundComponent;
                    }
                }
            }
        }
        return null;
    }

    public static JButton getFirstButton(Container container) {
        for (Component subComponent: container.getComponents()) {
            if (subComponent instanceof JButton) {
                return (JButton)subComponent;
            } else if (subComponent instanceof Container) {
                JButton subResult = getFirstButton((Container)subComponent);
                if (subResult != null) {
                    return subResult;
                }
            }
        }
        return null;
    }
    
}

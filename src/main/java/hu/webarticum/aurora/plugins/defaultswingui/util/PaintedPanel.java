package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.Painter;

public class PaintedPanel extends JPanel {
    
    private static final long serialVersionUID = 1L;
    
    protected Painter<Component> painter = null;
    
    public void setPainter(Painter<Component> painter) {
        this.painter = painter;
    }

    public Painter<Component> getPainter() {
        return painter;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        if (painter == null) {
            super.paintComponent(g);
        } else {
            painter.paint((Graphics2D)g, this, this.getWidth(), this.getHeight());
        }
    }
    
}

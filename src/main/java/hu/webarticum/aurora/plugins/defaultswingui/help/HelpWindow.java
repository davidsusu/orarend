package hu.webarticum.aurora.plugins.defaultswingui.help;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelpWindow {
    
    private JFrame frame;
    
    public HelpWindow() {
        frame = new JFrame();
        frame.add(new JLabel("..."));
    }
    
    public void run() {
        frame.pack();
        frame.setVisible(true);
    }
    
}

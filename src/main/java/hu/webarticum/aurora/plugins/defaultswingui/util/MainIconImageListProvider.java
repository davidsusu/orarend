package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

public class MainIconImageListProvider {
    
    public static List<Image> getImageList() {
        List<Image> icons = new ArrayList<Image>();
        icons.add(IconLoader.loadImage("main_16"));
        icons.add(IconLoader.loadImage("main_32"));
        icons.add(IconLoader.loadImage("main_64"));
        icons.add(IconLoader.loadImage("main_128"));
        return icons;
    }
    
}

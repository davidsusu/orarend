package hu.webarticum.aurora.plugins.defaultswingui.component.widget.timelimiteditor;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import org.apache.commons.collections4.iterators.PeekingIterator;

import hu.webarticum.aurora.app.util.slots.Slot;
import hu.webarticum.aurora.core.model.time.CustomTimeLimit;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;

public class LayoutProvider {
    
    
    private final Color BACKGROUND_COLOR = Color.WHITE;
    
    private final long TOOGLE_SLOT_MAX_PAD = 15 * Time.MINUTE;
    
    private final long DEFAULT_START_DAY = Time.MONDAY;
    private final long DEFAULT_END_DAY = Time.FRIDAY;
    private final long DEFAULT_START_TIME = 7 * Time.HOUR;
    private final long DEFAULT_END_TIME = 14 * Time.HOUR;
    private final double SECONDS_PER_PIXEL = Time.MINUTE;
    private final int OUTER_PAD = 10;
    private final int COLUMN_WIDTH = 150;
    private final int COLUMN_GAP = 20;
    private final int COLUMN_PAD = 10;
    private final int TOP_PAD = 7;
    private final int SWITCH_HEIGHT = 4;
    private final int SWITCH_OUT = 7;
    private final int SLOT_OVERLAP_XDIFF = 5;
    
    private final int HEADER_HEIGHT = 50;
    private final Color HEADER_MIXED_COLOR = new Color(0xCCBB77);
    private final int HEADER_TITLE_FONTSIZE = 15;
    private final Color HEADER_TITLE_COLOR = Color.WHITE;
    
    private final int LEFT_WIDTH = 70;
    private final long LEFT_SLOTGROUP_MAXDIFFSECONDS = 20 * Time.MINUTE;
    
    private final Color COLUMN_FREE_COLOR = new Color(0x77CC55);
    private final Color COLUMN_BANNED_COLOR = new Color(0xFFBBAA);
    private final Color COLUMN_EXTENSION_COLOR = new Color(0xDDDDDD);
    private final Color SLOT_FREE_COLOR = new Color(40, 100, 0, 200);
    private final Color SLOT_BANNED_COLOR = new Color(130, 40, 0, 200);
    private final Color SWITCH_COLOR = new Color(120, 120, 120, 150);

    private final int TIMELABEL_FONTSIZE = 10;
    private final Color SLOT_TIMELABEL_COLOR = Color.WHITE;
    private final int SLOT_TIMELABEL_MARGIN = 4;
    private final int SWITCH_MIDNIGHT_TIMELABEL_MARGIN = 10;
    private final int SWITCH_MIDNIGHT_TIMELABEL_XPAD = 4;
    private final int SWITCH_MIDNIGHT_TIMELABEL_YPAD = 0;
    private final int SWITCH_TIMELABEL_XPAD = 4;
    private final int SWITCH_TIMELABEL_YPAD = 2;
    
    private final int TRAVERSE_LEFT_PAD = 30;

    private final boolean editable;
    
    
    private long startDay = 0;
    
    private long endDay = 0;
    
    private long startTime = 0;
    
    private long endTime = 0;
    
    private int width = 0;
    
    private int height = 0;
    
    private boolean hasTopPad = false;

    private List<SlotEntry> slotEntries = new ArrayList<SlotEntry>();

    private List<SwitchEntry> switchEntries = new ArrayList<SwitchEntry>();
    
    private List<SlotsEntry> slotsEntries = new ArrayList<SlotsEntry>();
    
    private BufferedImage image = null;
    
    private BufferedImage headerImage = null;
    
    private BufferedImage leftImage = null;
    
    
    public LayoutProvider(boolean editable /* TODO settings */) {
        this.editable = editable;
    }
    

    public Color getBackgroundColor() {
        return BACKGROUND_COLOR;
    }

    public long getToggleSlotMaxPad() {
        return TOOGLE_SLOT_MAX_PAD;
    }
    
    public double getSecondsPerPixel() {
        return SECONDS_PER_PIXEL;
    }
    
    public int getOuterPad() {
        return OUTER_PAD;
    }
    
    public boolean isEditable() {
        return editable;
    }
    
    public int getColumnWidth() {
        return COLUMN_WIDTH;
    }
    
    public int getColumnGap() {
        return COLUMN_GAP;
    }
    
    public int getColumnPad() {
        return COLUMN_PAD;
    }
    
    public int getTopPad() {
        return TOP_PAD;
    }
    
    public int getHeaderHeight() {
        return HEADER_HEIGHT;
    }
    
    public int getLeftWidth() {
        return LEFT_WIDTH;
    }
    
    public int getTraverseLeftPad() {
        return TRAVERSE_LEFT_PAD;
    }
    
    public long getStartDay() {
        return startDay;
    }

    public long getEndDay() {
        return endDay;
    }

    public int getDayCount() {
        long startDayNo = startDay / Time.DAY;
        long endDayNo = endDay / Time.DAY;
        return (int)(endDayNo - startDayNo + 1);
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }
    
    public boolean hasTopPad() {
        return hasTopPad;
    }
    
    public int getWidth() {
        return width;
    }
    
    public int getHeight() {
        return height;
    }

    public BufferedImage getImage() {
        return image;
    }

    public BufferedImage getHeaderImage() {
        return headerImage;
    }

    public BufferedImage getLeftImage() {
        return leftImage;
    }
    
    public SlotEntry getSlotEntryAt(Point point) {
        for (int i = slotEntries.size() - 1; i >= 0; i--) {
            SlotEntry slotEntry = slotEntries.get(i);
            if (slotEntry.getRectangle().contains(point)) {
                return slotEntry;
            }
        }
        return null;
    }

    public SwitchEntry getSwitchEntryAt(Point point) {
        for (int i = switchEntries.size() - 1; i >= 0; i--) {
            SwitchEntry switchEntry = switchEntries.get(i);
            if (switchEntry.getRectangle().contains(point)) {
                return switchEntry;
            }
        }
        return null;
    }

    public SwitchEntry getSwitchEntryAt(Time time) {
        for (int i = switchEntries.size() - 1; i >= 0; i--) {
            SwitchEntry switchEntry = switchEntries.get(i);
            if (switchEntry.getTime().equals(time)) {
                return switchEntry;
            }
        }
        return null;
    }

    public Time getDayAt(int x) {
        int innerX = x - OUTER_PAD;
        int columnPeriod = COLUMN_WIDTH + COLUMN_GAP;
        int dayIndex = innerX / columnPeriod;
        int dayCount = (int)((endDay - startDay) / Time.DAY) + 1;
        
        if (innerX < 0 || dayIndex >= dayCount) {
            return null;
        }

        int dayX = innerX % columnPeriod;
        
        if (dayX >= COLUMN_WIDTH) {
            return null;
        }
        
        return new Time(startDay + (dayIndex * Time.DAY));
    }
    
    public Time getTimeAt(Point point) {
        int innerX = point.x - OUTER_PAD;
        int columnPeriod = COLUMN_WIDTH + COLUMN_GAP;
        int dayIndex = innerX / columnPeriod;
        int dayCount = (int)((endDay - startDay) / Time.DAY) + 1;
        
        if (innerX < 0 || dayIndex >= dayCount) {
            return null;
        }
        
        int dayX = innerX % columnPeriod;
        
        if (dayX >= COLUMN_WIDTH) {
            return null;
        }
        
        int innerY = point.y - OUTER_PAD - TOP_PAD;
        int dayHeight = (int)Math.round((getEndTime() - getStartTime()) / SECONDS_PER_PIXEL);
        
        if (innerY < 0 || innerY >= dayHeight) {
            if (startTime > 0 && innerY < 0) {
                SwitchEntry midnightSwitchEntry = getSwitchEntryAt(point);
                if (midnightSwitchEntry != null) {
                    return midnightSwitchEntry.getTime();
                }
            }
            return null;
        }
        
        return new Time(startDay + (dayIndex * Time.DAY) + startTime + (long)Math.round(innerY * SECONDS_PER_PIXEL));
    }

    public Point getPointFor(Time time, int leftPad) {
        if (time == null) {
            return null;
        }
        long seconds = time.getSeconds();
        long dayIndex = (seconds - startDay) / Time.DAY;
        long daySeconds = seconds % Time.DAY;
        int x = OUTER_PAD + ((COLUMN_WIDTH + COLUMN_GAP) * (int)dayIndex);
        int y;
        if (time.getPart(Time.PART.SECONDS_OF_DAY) == 0 && startTime > 0) {
            y = OUTER_PAD + (SWITCH_HEIGHT / 2);
        } else {
            y = OUTER_PAD + TOP_PAD + (int)((daySeconds - startTime) / SECONDS_PER_PIXEL);
        }
        return new Point(x + leftPad, y);
    }
    
    public SlotsEntry getSlotsEntryAt(int y) {
        for (SlotsEntry slotsEntry: slotsEntries) {
            if (y >= slotsEntry.getStartY() && y < slotsEntry.getEndY()) {
                return slotsEntry;
            }
        }
        return null;
    }

    public Point getNextTraversalPoint(Point point, boolean cyclic, boolean forward) {
        Time time = (point == null) ? null : getTimeAt(point);
        return getPointFor(getNextTraversalTime(time, cyclic, forward), TRAVERSE_LEFT_PAD);
    }
    
    public Time getNextTraversalTime(Time time, boolean cyclic, boolean forward) {
        SlotEntry slotEntry = getNextSlotEntryAt(time, forward);
        SwitchEntry switchEntry = getNextSwitchEntryAt(time, forward);
        if (slotEntry == null) {
            if (switchEntry == null) {
                if (time == null || !cyclic) {
                    return null;
                } else {
                    return getNextTraversalTime((Time)null, cyclic, forward);
                }
            } else {
                return switchEntry.getTime();
            }
        } else {
            if (switchEntry == null) {
                return slotEntry.getSlot().getInterval().getStart();
            } else {
                int cmp = slotEntry.getSlot().getInterval().getStart().compareTo(switchEntry.getTime());
                boolean chooseSwitchEntry = forward ? cmp > 0 : cmp < 0;
                if (chooseSwitchEntry) {
                    return switchEntry.getTime();
                } else {
                    return slotEntry.getSlot().getInterval().getStart();
                }
            }
        }
    }

    private SlotEntry getNextSlotEntryAt(Time time, boolean forward) {
        if (time == null) {
            if (slotEntries.isEmpty()) {
                return null;
            } else {
                return slotEntries.get(forward ? 0 : slotEntries.size() - 1);
            }
        }
        if (forward) {
            for (SlotEntry slotEntry: slotEntries) {
                if (slotEntry.getSlot().getInterval().getStart().compareTo(time) > 0) {
                    return slotEntry;
                }
            }
        } else {
            for (int i = slotEntries.size() - 1; i >= 0; i--) {
                SlotEntry slotEntry = slotEntries.get(i);
                if (slotEntry.getSlot().getInterval().getStart().compareTo(time) < 0) {
                    return slotEntry;
                }
            }
        }
        return null;
    }

    private SwitchEntry getNextSwitchEntryAt(Time time, boolean forward) {
        if (time == null) {
            if (switchEntries.isEmpty()) {
                return null;
            } else {
                return switchEntries.get(forward ? 0 : switchEntries.size() - 1);
            }
        }
        if (forward) {
            for (SwitchEntry switchEntry: switchEntries) {
                if (switchEntry.getTime().compareTo(time) > 0) {
                    return switchEntry;
                }
            }
        } else {
            for (int i = switchEntries.size() - 1; i >= 0; i--) {
                SwitchEntry switchEntry = switchEntries.get(i);
                if (switchEntry.getTime().compareTo(time) < 0) {
                    return switchEntry;
                }
            }
        }
        return null;
    }

    public void load(CustomTimeLimit timeLimit, Collection<Slot> slots) {
        load(timeLimit, slots, AdditionalState.createEmpty());
    }
    
    public void load(CustomTimeLimit timeLimit, Collection<Slot> slots, AdditionalState additionalState) {
        startDay = DEFAULT_START_DAY;
        endDay = DEFAULT_END_DAY;
        startTime = DEFAULT_START_TIME;
        endTime = DEFAULT_END_TIME;
        
        boolean slotsChanged = checkSlotsChanged(slots);
        
        String[] weekDayLabels = new String[]{
            text("core.day.monday"),
            text("core.day.tuesday"),
            text("core.day.wednesday"),
            text("core.day.thursday"),
            text("core.day.friday"),
            text("core.day.saturday"),
            text("core.day.sunday"),
        };
        
        List<Time> timeLimitTimes = timeLimit.getTimes();
        List<Time> allTimes = new ArrayList<Time>(timeLimitTimes);
        
        for (Slot slot: slots) {
            Interval interval = slot.getInterval();
            allTimes.add(interval.getStart());
            allTimes.add(interval.getEnd());
        }
        
        hasTopPad = false;
        
        for (Time time: allTimes) {
            long daySeconds = time.getSeconds() / Time.DAY * Time.DAY;
            if (daySeconds < startDay) {
                startDay = daySeconds;
            } else if (daySeconds > endDay) {
                endDay = daySeconds;
            }

            long timeSeconds = time.getPart(Time.PART.SECONDS_OF_DAY);
            if (timeSeconds == 0) {
                hasTopPad = true;
            } else if (timeSeconds < startTime) {
                startTime = timeSeconds;
            } else if (timeSeconds > endTime) {
                endTime = timeSeconds;
            }
        }
        
        int dayCount = getDayCount();
        
        int columnHeight = (int)((endTime - startTime) / SECONDS_PER_PIXEL);
        
        width = dayCount * (COLUMN_WIDTH + COLUMN_GAP) - COLUMN_GAP + (2 * OUTER_PAD);
        height = columnHeight + TOP_PAD + (2 * OUTER_PAD);
        
        slotEntries.clear();
        switchEntries.clear();
        
        
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = image.getGraphics();
        
        graphics.setColor(BACKGROUND_COLOR);
        graphics.fillRect(0, 0, width, height);
        
        
        headerImage = new BufferedImage(width, HEADER_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics headerGraphics = headerImage.getGraphics();
        
        headerGraphics.setColor(BACKGROUND_COLOR);
        headerGraphics.fillRect(0, 0, width, HEADER_HEIGHT);
        headerGraphics.setFont(new Font(Font.SANS_SERIF, Font.BOLD, HEADER_TITLE_FONTSIZE));
        FontMetrics headerFontMetrics = headerGraphics.getFontMetrics();
        
        
        leftImage = new BufferedImage(LEFT_WIDTH, height, BufferedImage.TYPE_INT_RGB);
        Graphics leftGraphics = leftImage.getGraphics();
        
        leftGraphics.setColor(BACKGROUND_COLOR);
        leftGraphics.fillRect(0, 0, LEFT_WIDTH, height);
        
        
        for (long day = startDay; day <= endDay; day += Time.DAY) {
            int dayIndex = (int)((day - startDay) / Time.DAY);
            long nextDay = day + Time.DAY;
            Interval dayInterval = new Interval(day, nextDay);
            int weekIndex = (int)(day / Time.WEEK);
            int weekDayIndex = dayIndex % 7;
            String dayLabel = weekDayLabels[weekDayIndex].toUpperCase();
            if (weekIndex != 0) {
                int weekNumber = weekIndex;
                if (weekNumber > 0) {
                    weekNumber++;
                }
                dayLabel += " (" + weekNumber + ")";
            }
            
            int columnLeft = (COLUMN_WIDTH + COLUMN_GAP) * dayIndex + OUTER_PAD;
            int columnTop = TOP_PAD + OUTER_PAD;

            boolean moveOverlap = false;
            Interval previousInterval = null;
            
            for (Slot slot: slots) {
                Interval interval = slot.getInterval();
                if (interval.intersects(dayInterval)) {
                    if (previousInterval != null && previousInterval.intersects(interval)) {
                        moveOverlap = !moveOverlap;
                    } else {
                        moveOverlap = false;
                    }
                    
                    long slotStartTime = interval.getStart().getSeconds() - day;
                    long slotEndTime = interval.getEnd().getSeconds() - day;
                    
                    int left = columnLeft + COLUMN_PAD + (moveOverlap ? SLOT_OVERLAP_XDIFF : 0);
                    int top = columnTop + (int)((slotStartTime - startTime) / SECONDS_PER_PIXEL);
                    int width = COLUMN_WIDTH - (2 * COLUMN_PAD);
                    int height = (int)((slotEndTime - slotStartTime) / SECONDS_PER_PIXEL);
                    Rectangle rectangle = new Rectangle(left, top, width, height);
                    slotEntries.add(new SlotEntry(slot, rectangle));
                    
                    previousInterval = interval;
                }
            }
            
            Time lastTime = null;

            boolean state = timeLimit.getStartState();
            
            for (Time time: timeLimitTimes) {
                long timeSeconds = time.getSeconds();
                
                if (timeSeconds >= nextDay) {
                    break;
                } else if (timeSeconds >= day) {
                    int timeTop;
                    if (timeSeconds - day == 0) {
                        timeTop = OUTER_PAD + (SWITCH_HEIGHT / 2);
                    } else {
                        timeTop = columnTop + (int)((timeSeconds - day - startTime) / SECONDS_PER_PIXEL);
                        
                        long dayLatestTimeSeconds = (lastTime != null && lastTime.getSeconds() > day + startTime) ? lastTime.getSeconds() : day + startTime;
                        graphics.setColor(state ? COLUMN_FREE_COLOR : COLUMN_BANNED_COLOR);
                        
                        if (additionalState.extendAreaTime != null) {
                            if (additionalState.extendAreaDirection) {
                                Time previousSwitch = TimeLimitEditorUtil.getPreviousSwitch(timeLimit, time, false);
                                if (previousSwitch != null && previousSwitch.equals(additionalState.extendAreaTime)) {
                                    graphics.setColor(COLUMN_EXTENSION_COLOR);
                                }
                            } else {
                                Time nextSwitch = TimeLimitEditorUtil.getNextSwitch(timeLimit, new Time(dayLatestTimeSeconds), false);
                                if (nextSwitch != null && nextSwitch.equals(additionalState.extendAreaTime)) {
                                    graphics.setColor(COLUMN_EXTENSION_COLOR);
                                }
                            }
                        }
                        
                        graphics.fillRect(
                            columnLeft,
                            columnTop + (int)((dayLatestTimeSeconds - day - startTime) / SECONDS_PER_PIXEL),
                            COLUMN_WIDTH,
                            (int)((timeSeconds - dayLatestTimeSeconds) / SECONDS_PER_PIXEL)
                        );
                    }
                    
                    int left = columnLeft - SWITCH_OUT;
                    int top = timeTop - (SWITCH_HEIGHT / 2);
                    int width = COLUMN_WIDTH + (2 * SWITCH_OUT);
                    int height = SWITCH_HEIGHT;
                    Rectangle rectangle = new Rectangle(left, top, width, height);
                    switchEntries.add(new SwitchEntry(time, rectangle));
                    lastTime = time;
                }
                
                state = ! state;
            }
            
            long dayLastTimeSeconds = (lastTime != null && lastTime.getSeconds() > day + startTime) ? lastTime.getSeconds() : day + startTime;
            if (day + endTime > dayLastTimeSeconds) {
                graphics.setColor(state ? COLUMN_FREE_COLOR : COLUMN_BANNED_COLOR);
                
                if (additionalState.extendAreaTime != null) {
                    if (additionalState.extendAreaDirection) {
                        Time previousSwitch = TimeLimitEditorUtil.getPreviousSwitch(timeLimit, new Time(day + endTime), false);
                        if (previousSwitch != null && previousSwitch.equals(additionalState.extendAreaTime)) {
                            graphics.setColor(COLUMN_EXTENSION_COLOR);
                        }
                    } else {
                        Time nextSwitch = TimeLimitEditorUtil.getNextSwitch(timeLimit, new Time(dayLastTimeSeconds), false);
                        if (nextSwitch != null && nextSwitch.equals(additionalState.extendAreaTime)) {
                            graphics.setColor(COLUMN_EXTENSION_COLOR);
                        }
                    }
                }
                
                graphics.fillRect(
                    columnLeft,
                    columnTop + (int)((dayLastTimeSeconds - day - startTime) / SECONDS_PER_PIXEL),
                    COLUMN_WIDTH,
                    (int)((day + endTime - dayLastTimeSeconds) / SECONDS_PER_PIXEL)
                );
            }
            
            if (timeLimit.contains(dayInterval)) {
                headerGraphics.setColor(COLUMN_FREE_COLOR);
            } else if (timeLimit.intersects(dayInterval)) {
                headerGraphics.setColor(HEADER_MIXED_COLOR);
            } else {
                headerGraphics.setColor(COLUMN_BANNED_COLOR);
            }
            
            headerGraphics.fillRect(
                columnLeft,
                0,
                COLUMN_WIDTH,
                HEADER_HEIGHT
            );

            int titleWidth = headerFontMetrics.stringWidth(dayLabel);
            int titleHeight = headerFontMetrics.getHeight();
            int titleDescent = headerFontMetrics.getDescent();
            
            headerGraphics.setColor(HEADER_TITLE_COLOR);
            headerGraphics.drawString(
                dayLabel,
                columnLeft + ((COLUMN_WIDTH - titleWidth) / 2),
                HEADER_HEIGHT - ((HEADER_HEIGHT - titleHeight) / 2) - titleDescent
            );
        }
        
        graphics.setFont(new Font(Font.MONOSPACED, Font.PLAIN, TIMELABEL_FONTSIZE));
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int timeLabelFullHeight = fontMetrics.getHeight();
        int timeLabelWidth = fontMetrics.stringWidth("00:00");
        int timeLabelAscent = fontMetrics.getDescent();
        int timeLabelDescent = fontMetrics.getDescent();
        int timeLabelHeight = timeLabelFullHeight - timeLabelDescent - timeLabelAscent;

        for (SlotEntry slotEntry: slotEntries) {
            Rectangle rectangle = slotEntry.getRectangle();
            Slot slot = slotEntry.getSlot();
            Interval interval = slot.getInterval();
            
            graphics.setColor(timeLimit.contains(interval) ? SLOT_FREE_COLOR : SLOT_BANNED_COLOR);
            graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            
            String label = slot.getLabel();
            if (label.isEmpty()) {
                label = interval.getStart().toTimeString();
            }
            graphics.setColor(SLOT_TIMELABEL_COLOR);
            graphics.drawString(
                label,
                rectangle.x + SLOT_TIMELABEL_MARGIN,
                rectangle.y + SLOT_TIMELABEL_MARGIN + timeLabelHeight
            );
        }
        
        if (editable) {
            for (SwitchEntry switchEntry: switchEntries) {
                Time time = switchEntry.getTime();
                Rectangle rectangle = switchEntry.getRectangle();
                
                graphics.setColor(SWITCH_COLOR);
                graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                
                if (additionalState.moveToTime != null && time.equals(additionalState.moveToTime)) {
                    long moveTimeDiff = additionalState.moveToTime.getSeconds() - additionalState.moveFromTime.getSeconds();
                    int pixelDiff = (int)(moveTimeDiff / SECONDS_PER_PIXEL);
                    int movedX = rectangle.x + (rectangle.width / 2);
                    int movedY = rectangle.y + (rectangle.height / 2);
                    if (moveTimeDiff < 0) {
                        graphics.setColor(new Color(255, 255, 255, 170)); // XXX
                        graphics.drawLine(movedX, movedY, movedX - 7, movedY + 13);
                        graphics.drawLine(movedX, movedY, movedX + 7, movedY + 13);
                        graphics.drawLine(movedX, movedY, movedX, movedY - pixelDiff);
                    } else if (moveTimeDiff > 0) {
                        graphics.setColor(new Color(255, 255, 255, 170)); // XXX
                        graphics.drawLine(movedX, movedY, movedX - 7, movedY - 13);
                        graphics.drawLine(movedX, movedY, movedX + 7, movedY - 13);
                        graphics.drawLine(movedX, movedY, movedX, movedY - pixelDiff);
                    }
                }
                
                if (time.getPart(Time.PART.SECONDS_OF_DAY) == 0) {
                    int middle = rectangle.y + (rectangle.height / 2);
                    int boxWidth = timeLabelWidth + (SWITCH_MIDNIGHT_TIMELABEL_XPAD * 2);
                    int boxHeight = timeLabelHeight + (SWITCH_MIDNIGHT_TIMELABEL_YPAD * 2);
                    
                    graphics.setColor(BACKGROUND_COLOR);
                    graphics.fillRect(
                        rectangle.x + rectangle.width - boxWidth - SWITCH_MIDNIGHT_TIMELABEL_MARGIN,
                        middle + (boxHeight / 2) - boxHeight,
                        boxWidth,
                        boxHeight
                    );
                    
                    graphics.setColor(SWITCH_COLOR);
                    graphics.drawString(
                        "00:00",
                        rectangle.x + rectangle.width - boxWidth - SWITCH_MIDNIGHT_TIMELABEL_MARGIN + SWITCH_MIDNIGHT_TIMELABEL_XPAD,
                        middle + (boxHeight / 2) - SWITCH_MIDNIGHT_TIMELABEL_YPAD
                    );
                } else {
                    graphics.setColor(SWITCH_COLOR);
                    graphics.fillRect(
                        rectangle.x + rectangle.width - timeLabelWidth - (SWITCH_TIMELABEL_XPAD * 2),
                        rectangle.y - timeLabelHeight - (SWITCH_TIMELABEL_YPAD * 2),
                        timeLabelWidth + (SWITCH_TIMELABEL_XPAD * 2),
                        timeLabelHeight + (SWITCH_TIMELABEL_YPAD * 2)
                    );
                    graphics.setColor(Color.WHITE);
                    graphics.drawString(
                        time.toTimeString(),
                        rectangle.x + rectangle.width - timeLabelWidth - SWITCH_TIMELABEL_XPAD,
                        rectangle.y - SWITCH_TIMELABEL_YPAD
                    );
                }
            }
            
        }
        
        if (slotsChanged) {
            slotsEntries.clear();
            for (SlotGroup basicSlotsEntry: generateSlotGroups(slots)) {
                int startY = (int)(((basicSlotsEntry.startTime.getSeconds() - startTime) / SECONDS_PER_PIXEL) + OUTER_PAD + TOP_PAD);
                int endY = (int)(((basicSlotsEntry.endTime.getSeconds() - startTime) / SECONDS_PER_PIXEL) + OUTER_PAD + TOP_PAD);
                slotsEntries.add(new SlotsEntry(basicSlotsEntry.slots, basicSlotsEntry.startTime, basicSlotsEntry.endTime, startY, endY));
            }
        }
        
        for (SlotsEntry slotsEntry: slotsEntries) {
            boolean isAllFree = true;
            boolean isAllBanned = true;
            for (Slot slot: slotsEntry.getSlots()) {
                if (timeLimit.contains(slot.getInterval())) {
                    isAllBanned = false;
                } else {
                    isAllFree = false;
                }
                if (!isAllBanned && !isAllFree) {
                    break;
                }
            }
            int startY = slotsEntry.getStartY();
            int endY = slotsEntry.getEndY();
            if (isAllFree) {
                leftGraphics.setColor(COLUMN_FREE_COLOR);
            } else if (isAllBanned) {
                leftGraphics.setColor(COLUMN_BANNED_COLOR);
            } else {
                leftGraphics.setColor(HEADER_MIXED_COLOR);
            }
            leftGraphics.fillRect(0, startY, LEFT_WIDTH, endY - startY - 1);
        }
        
    }
    
    private boolean checkSlotsChanged(Collection<Slot> slots) {
        Iterator<Slot> slotIterator = slots.iterator();
        for (SlotEntry slotEntry: slotEntries) {
            if (!slotIterator.hasNext()) {
                return false;
            }
            Slot slot = slotIterator.next();
            if (!slotEntry.getSlot().equals(slot)) {
                return false;
            }
        }
        return true;
    }
    
    private List<SlotGroup> generateSlotGroups(Collection<Slot> slots) {
        TreeSet<Time> endDayTimes = new TreeSet<Time>();
        for (Slot slot: slots) {
            Time slotStartDayTime = slot.getInterval().getStart().getTimeOnly();
            Time slotEndDayTime = slot.getInterval().getEnd().getTimeOnly();
            if (slotEndDayTime.getSeconds() > slotStartDayTime.getSeconds()) {
                endDayTimes.add(slotEndDayTime);
            }
        }
        List<Slot> dayTimeSortedSlots = new ArrayList<Slot>(slots);
        Collections.sort(dayTimeSortedSlots, new Comparator<Slot>() {

            @Override
            public int compare(Slot slot1, Slot slot2) {
                Time dayTime1 = slot1.getInterval().getStart().getTimeOnly();
                Time dayTime2 = slot2.getInterval().getStart().getTimeOnly();
                return dayTime1.compareTo(dayTime2);
            }
            
        });
        PeekingIterator<Slot> peekingSlotIterator = new PeekingIterator<Slot>(dayTimeSortedSlots.iterator());
        List<SlotGroup> slotGroups = new ArrayList<SlotGroup>();
        SlotGroup currentSlotGroup = new SlotGroup();
        while (peekingSlotIterator.hasNext()) {
            Slot currentSlot = peekingSlotIterator.next();
            Time currentStartDayTime = currentSlot.getInterval().getStart().getTimeOnly();
            Time currentEndDayTime = currentSlot.getInterval().getEnd().getTimeOnly();
            Slot nextSlot = peekingSlotIterator.peek();
            
            currentSlotGroup.slots.add(currentSlot);
            if (currentSlotGroup.startTime == null) {
                currentSlotGroup.startTime = currentStartDayTime;
            }
            if (currentSlotGroup.endTime == null || currentEndDayTime.getSeconds() > currentSlotGroup.endTime.getSeconds()) {
                currentSlotGroup.endTime = currentEndDayTime;
            }
            
            boolean closeGroup;
            if (nextSlot == null) {
                closeGroup = true;
            } else {
                Time nextStartDayTime = nextSlot.getInterval().getStart().getTimeOnly();
                if (nextStartDayTime.getSeconds() - currentStartDayTime.getSeconds() > LEFT_SLOTGROUP_MAXDIFFSECONDS) {
                    closeGroup = true;
                } else if (!endDayTimes.subSet(currentStartDayTime, false, nextStartDayTime, false).isEmpty()) {
                    closeGroup = true;
                } else {
                    closeGroup = false;
                }
            }
            
            if (closeGroup) {
                currentSlotGroup.startTime = new Time(Math.max(startTime, currentSlotGroup.startTime.getSeconds() - TOOGLE_SLOT_MAX_PAD));
                currentSlotGroup.endTime = new Time(Math.min(endTime, currentSlotGroup.endTime.getSeconds() + TOOGLE_SLOT_MAX_PAD));
                slotGroups.add(currentSlotGroup);
                currentSlotGroup = new SlotGroup();
            }
        }
        
        SlotGroup previousSlotGroup = null;
        for (SlotGroup slotGroup: slotGroups) {
            if (previousSlotGroup != null) {
                long startSeconds = slotGroup.startTime.getSeconds();
                long endSeconds = slotGroup.endTime.getSeconds();
                long previousEndSeconds = previousSlotGroup.endTime.getSeconds();
                if (previousEndSeconds > startSeconds) {
                    long newBorderSeconds;
                    if (endSeconds < previousEndSeconds) {
                        newBorderSeconds = startSeconds;
                    } else {
                        newBorderSeconds = (startSeconds + previousEndSeconds) / 2;
                    }
                    Time newBorderTime = new Time(newBorderSeconds);
                    previousSlotGroup.endTime = newBorderTime;
                    slotGroup.startTime = newBorderTime;
                }
            }
            previousSlotGroup = slotGroup;
        }
        
        return slotGroups;
    }
    
    public abstract class RectangleEntry {

        private final Rectangle rectangle;
        
        public RectangleEntry(Rectangle rectangle) {
            this.rectangle = rectangle;
        }

        public Rectangle getRectangle() {
            return rectangle;
        }
        
    }
    
    public class SlotEntry extends RectangleEntry {
        
        private final Slot slot;
        
        public SlotEntry(Slot slot, Rectangle rectangle) {
            super(rectangle);
            this.slot = slot;
        }
        
        public Slot getSlot() {
            return slot;
        }
        
    }
    
    public class SwitchEntry extends RectangleEntry {
        
        private final Time time;
        
        public SwitchEntry(Time time, Rectangle rectangle) {
            super(rectangle);
            this.time = time;
        }
        
        public Time getTime() {
            return time;
        }
        
    }
    
    private class SlotGroup {
        
        final List<Slot> slots = new ArrayList<Slot>();
        
        Time startTime = null;
        
        Time endTime = null;
        
    }
    
    public class SlotsEntry  {

        private final List<Slot> slots;
        
        private final Time startTime;
        
        private final Time endTime;
        
        private final int startY;
        
        private final int endY;
        
        public SlotsEntry(Collection<Slot> slots, Time startTime, Time endTime, int startY, int endY) {
            this.startTime = startTime;
            this.endTime = endTime;
            this.slots = new ArrayList<Slot>(slots);
            this.startY = startY;
            this.endY = endY;
        }

        public List<Slot> getSlots() {
            return new ArrayList<Slot>(slots);
        }
        
        public Time getStartTime() {
            return startTime;
        }
        
        public Time getEndTime() {
            return endTime;
        }
        
        public int getStartY() {
            return startY;
        }
        
        public int getEndY() {
            return endY;
        }
        
    }
    
    public static class AdditionalState {

        private final Time moveFromTime;

        private final Time moveToTime;
        
        private final Time extendAreaTime;
        
        private final Boolean extendAreaDirection;
        
        AdditionalState(Time moveFromTime, Time moveToTime, Time extendAreaTime, Boolean extendAreaDirection) {
            this.moveFromTime = moveFromTime;
            this.moveToTime = moveToTime;
            this.extendAreaTime = extendAreaTime;
            this.extendAreaDirection = extendAreaDirection;
        }

        public static AdditionalState createEmpty() {
            return new AdditionalState(null, null, null, null);
        }

        public static AdditionalState createMove(Time moveFromTime, Time moveToTime) {
            return new AdditionalState(moveFromTime, moveToTime, null, null);
        }

        public static AdditionalState createExtend(Time extendAreaTime, Boolean extendAreaDirection) {
            return new AdditionalState(null, null, extendAreaTime, extendAreaDirection);
        }
        
    }

}


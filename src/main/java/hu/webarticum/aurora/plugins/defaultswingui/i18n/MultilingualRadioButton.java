package hu.webarticum.aurora.plugins.defaultswingui.i18n;

import javax.swing.Icon;
import javax.swing.JRadioButton;

public class MultilingualRadioButton extends JRadioButton implements MultilingualComponent {

    private static final long serialVersionUID = 1L;

    protected MultilingualContent labelContent;

    protected MultilingualContent tooltipContent;

    public MultilingualRadioButton() {
        setLanguagePath("");
    }

    public MultilingualRadioButton(String labelPath) {
        setLanguagePath(labelPath);
    }

    public MultilingualRadioButton(String labelPath, String tooltipPath) {
        setLanguagePath(labelPath, tooltipPath);
    }

    public MultilingualRadioButton(String labelPath, String tooltipPath, Icon icon) {
        setLanguagePath(labelPath, tooltipPath);
        setIcon(icon);
    }

    public MultilingualRadioButton(Icon icon, String tooltipPath) {
        setLanguagePath("", tooltipPath);
        setIcon(icon);
    }

    public void setLanguagePath(String labelPath) {
        setLanguagePath(labelPath, "");
    }
    
    public void setLanguagePath(String labelPath, String tooltipPath) {
        setLanguagePath(new PathMultilingualContent(labelPath), new PathMultilingualContent(tooltipPath));
        reloadLanguageTexts();
    }

    public void setLanguagePath(MultilingualContent labelContent) {
        setLanguagePath(labelContent, new PathMultilingualContent(""));
    }
    
    public void setLanguagePath(MultilingualContent labelContent, MultilingualContent tooltipContent) {
        this.labelContent = labelContent;
        this.tooltipContent = tooltipContent;
        reloadLanguageTexts();
    }
    
    @Override
    public void reloadLanguageTexts() {
        setText(labelContent.toString());
        setToolTipText(tooltipContent.toStringOrNull());
    }

}

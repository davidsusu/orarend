package hu.webarticum.aurora.plugins.defaultswingui.component.widget.timelimiteditor;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.border.MatteBorder;

import hu.webarticum.aurora.app.util.slots.AspectSlotCollector;
import hu.webarticum.aurora.app.util.slots.Slot;
import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.time.CustomTimeLimit;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.core.model.time.TimeLimit;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.EditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.TimeEditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.chm.AbstractCommand;
import hu.webarticum.chm.LinearHistory;


public class TimeLimitEditorPanel extends JPanel {
    
    private static final long serialVersionUID = 1L;
    
    
    private TimeLimitHistory history = null;
    
    private final JFrame parent;
    
    private final LayoutProvider layoutProvider;
    
    private final boolean editable;
    
    private CustomTimeLimit rawTimeLimit;
    
    private Rectangle highlightedRectangle = null;
    
    private DraggingSwitchContainer draggingSwitchContainer = null;
    
    private List<Slot> slots;

    private JPanel headerPanel;

    private JPanel leftPanel;

    private JPanel mainPanel;
    
    private JScrollPane mainScrollPane;
    
    private boolean keyboardNavigationEnabled = false;
    
    private Point keyboardPosition = null;
    
    private Image keyboardCursorImage;
    
    private LayoutProvider.SwitchEntry keyboardSwitchEntryToDelete = null;
    
    
    
    public TimeLimitEditorPanel(JFrame parent, boolean editable) {
        this.parent = parent;
        this.editable = editable;
        this.layoutProvider = new LayoutProvider(editable);
        keyboardCursorImage = IconLoader.loadImage("cursor_hand");
        build();
    }
    
    private void build() {
        setLayout(new BorderLayout());
        setFocusable(true);
        
        JPanel topPanel = new JPanel(new BorderLayout());
        add(BorderLayout.PAGE_START, topPanel);
        
        JPanel topLeftPanel = new JPanel();
        topLeftPanel.setPreferredSize(new Dimension(70, 50));
        topLeftPanel.setBackground(layoutProvider.getBackgroundColor());
        topPanel.add(topLeftPanel, BorderLayout.LINE_START);
        
        headerPanel = new JPanel() {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected void paintComponent(Graphics g) {
                int xScroll = mainScrollPane.getHorizontalScrollBar().getValue();
                g.setColor(layoutProvider.getBackgroundColor());
                g.fillRect(0, 0, headerPanel.getWidth(), headerPanel.getHeight());
                Image headerImage = layoutProvider.getHeaderImage();
                if (headerImage != null) {
                    g.drawImage(headerImage, -xScroll, 0, null);
                }
            }
            
        };
        headerPanel.setPreferredSize(new Dimension(200, layoutProvider.getHeaderHeight()));
        headerPanel.setBorder(new MatteBorder(0, 0, 1, 0, layoutProvider.getBackgroundColor()));
        headerPanel.setFocusable(true);
        
        headerPanel.addMouseMotionListener(new MouseMotionAdapter() {
            
            @Override
            public void mouseMoved(MouseEvent ev) {
                int x = ev.getX() + mainScrollPane.getHorizontalScrollBar().getValue();
                Time dayTime = layoutProvider.getDayAt(x);
                headerPanel.setCursor(
                    dayTime == null ?
                    Cursor.getDefaultCursor() :
                    Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)
                );
            }
            
        });
        
        headerPanel.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                int x = ev.getX() + mainScrollPane.getHorizontalScrollBar().getValue();
                toggleDay(layoutProvider.getDayAt(x));
            }
            
        });
        
        topPanel.add(BorderLayout.CENTER, headerPanel);

        leftPanel = new JPanel() {
            
            private static final long serialVersionUID = 1L;
            
            @Override
            protected void paintComponent(Graphics g) {
                g.setColor(layoutProvider.getBackgroundColor());
                g.fillRect(0, 0, leftPanel.getWidth(), leftPanel.getHeight());
                
                Image leftImage = layoutProvider.getLeftImage();
                if (leftImage != null) {
                    int yScroll = mainScrollPane.getVerticalScrollBar().getValue();
                    g.drawImage(leftImage, 0, -yScroll, null);
                }
            }
            
        };
        leftPanel.setPreferredSize(new Dimension(layoutProvider.getLeftWidth(), 200));
        leftPanel.setBorder(new MatteBorder(0, 0, 0, 1, layoutProvider.getBackgroundColor()));
        
        leftPanel.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                int y = ev.getY() + mainScrollPane.getVerticalScrollBar().getValue();
                LayoutProvider.SlotsEntry slotsEntry = layoutProvider.getSlotsEntryAt(y);
                toggleSlotsEntry(slotsEntry);
            }
        });
        
        leftPanel.addMouseMotionListener(new MouseMotionAdapter() {
            
            @Override
            public void mouseMoved(MouseEvent ev) {
                int y = ev.getY() + mainScrollPane.getVerticalScrollBar().getValue();
                LayoutProvider.SlotsEntry slotsEntry = layoutProvider.getSlotsEntryAt(y);
                leftPanel.setCursor(
                    slotsEntry == null ?
                    Cursor.getDefaultCursor() :
                    Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)
                );
            }
            
        });
        
        add(BorderLayout.LINE_START, leftPanel);
        
        mainPanel = new JPanel() {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected void paintComponent(Graphics g) {
                Image image = layoutProvider.getImage();
                g.setColor(layoutProvider.getBackgroundColor());
                g.fillRect(0, 0, mainPanel.getWidth(), mainPanel.getHeight());
                if (image != null) {
                    g.drawImage(image, 0, 0, null);
                    if (highlightedRectangle != null) {
                        g.setColor(new Color(255, 255, 255, 50));
                        g.fillRect(
                            highlightedRectangle.x,
                            highlightedRectangle.y,
                            highlightedRectangle.width,
                            highlightedRectangle.height
                        );
                    }
                    if (keyboardNavigationEnabled) {
                        if (keyboardPosition != null) {
                            g.drawImage(keyboardCursorImage, keyboardPosition.x, keyboardPosition.y, null);
                        }
                        if (keyboardSwitchEntryToDelete != null) {
                            Rectangle rectangle = keyboardSwitchEntryToDelete.getRectangle();
                            int centerX = (int)rectangle.getCenterX();
                            int centerY = (int)rectangle.getCenterY();
                            
                            Graphics2D g2 = (Graphics2D)g;
                            
                            Stroke originalStroke = g2.getStroke();
                            Object originalAntialiasing = g2.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
                            
                            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                            
                            g2.setColor(Color.WHITE);
                            g2.setStroke(new BasicStroke(7, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
                            g2.drawLine(centerX - 6, centerY - 6, centerX + 6, centerY + 6);
                            g2.drawLine(centerX + 6, centerY - 6, centerX - 6, centerY + 6);
                            
                            g2.setColor(new Color(0xCC3300));
                            g2.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
                            g2.drawLine(centerX - 6, centerY - 6, centerX + 6, centerY + 6);
                            g2.drawLine(centerX + 6, centerY - 6, centerX - 6, centerY + 6);

                            g2.setColor(new Color(0x113399));
                            
                            int[] dirs = new int[] {-1, 1};
                            for (int dir: dirs) {
                                g2.setStroke(new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
                                g2.drawArc(centerX - 18, centerY - 18, 37, 37, 30 * dir, 115 * dir);

                                g2.setStroke(new BasicStroke(2));
                                g2.drawLine(centerX, centerY + (18 * dir), centerX, centerY + (40 * dir));
                                g2.drawLine(centerX, centerY + (40 * dir), centerX - 7, centerY + (30 * dir));
                                g2.drawLine(centerX, centerY + (40 * dir), centerX + 7, centerY + (30 * dir));
                            }
                            
                            g2.setStroke(originalStroke);
                            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, originalAntialiasing);
                        }
                    }
                }
            }
            
        };
        
        final MouseMotionListener highlightListener = new MouseMotionAdapter() {
            
            @Override
            public void mouseMoved(MouseEvent ev) {
                if (!editable) {
                    return;
                }
                
                Point point = ev.getPoint();
                
                LayoutProvider.SwitchEntry switchEntry = layoutProvider.getSwitchEntryAt(point);
                if (switchEntry != null) {
                    highlightedRectangle = switchEntry.getRectangle();
                    mainPanel.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                } else {
                    LayoutProvider.SlotEntry slotEntry = layoutProvider.getSlotEntryAt(point);
                    if (slotEntry != null) {
                        highlightedRectangle = slotEntry.getRectangle();
                        mainPanel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                    } else {
                        highlightedRectangle = null;
                        mainPanel.setCursor(Cursor.getDefaultCursor());
                    }
                }
                
                stopKeyboardNavigation();
                mainPanel.repaint();
            }
            
            @Override
            public void mouseDragged(MouseEvent ev) {
                if (!editable) {
                    return;
                }
                
                Point point = ev.getPoint();
                
                if (draggingSwitchContainer != null) {
                    int yDiff = point.y - draggingSwitchContainer.startPoint.y;
                    
                    if (draggingSwitchContainer.mouseButton == 1) {
                        long timeDiff = (long)(yDiff * layoutProvider.getSecondsPerPixel());
                        Time startTime = draggingSwitchContainer.startTime;
                        long startTimeSeconds = startTime.getSeconds();
                        long minTimeSeconds = startTimeSeconds / Time.DAY * Time.DAY;
                        long maxTimeSeconds = minTimeSeconds + Time.DAY - 1;
                        CustomTimeLimit originalTimeLimit = draggingSwitchContainer.originalTimeLimit;
                        long newTimeSeconds = startTimeSeconds + timeDiff;
                        if (ev.isControlDown()) {
                            newTimeSeconds += 8 * Time.MINUTE;
                            newTimeSeconds -= newTimeSeconds % (15 * Time.MINUTE);
                        }
                        newTimeSeconds = Math.max(minTimeSeconds, Math.min(maxTimeSeconds, newTimeSeconds));
                        Time newTime = new Time(newTimeSeconds);
                        rawTimeLimit = TimeLimitEditorUtil.changeTimeLimit(originalTimeLimit, startTime, newTime);
                        layoutProvider.load(
                            rawTimeLimit, slots,
                            LayoutProvider.AdditionalState.createMove(startTime, newTime)
                        );
                        mainPanel.repaint();
                        headerPanel.repaint();
                        leftPanel.repaint();

                        LayoutProvider.SwitchEntry switchEntry = layoutProvider.getSwitchEntryAt(newTime);
                        if (switchEntry != null) {
                            highlightedRectangle = switchEntry.getRectangle();
                            mainPanel.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                        }
                    } else if (draggingSwitchContainer.mouseButton == 3) {
                        layoutProvider.load(
                            rawTimeLimit, slots,
                            LayoutProvider.AdditionalState.createExtend(draggingSwitchContainer.startTime, yDiff > 0)
                        );
                        mainPanel.repaint();
                        headerPanel.repaint();
                        leftPanel.repaint();
                    }
                }
            }
            
        };
        
        mainPanel.addMouseMotionListener(highlightListener);
        mainPanel.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (!editable) {
                    return;
                }
                
                Point point = ev.getPoint();
                
                LayoutProvider.SwitchEntry switchEntry = layoutProvider.getSwitchEntryAt(point);
                if (switchEntry != null) {
                    if (ev.getButton() == 1 && ev.getClickCount() % 2 == 0) {
                        highlightedRectangle = null;
                        mainPanel.repaint();
                        
                        Time originalTime = switchEntry.getTime();
                        GeneralWrapper<Time> timeWrapper = new GeneralWrapper<Time>(originalTime);
                        TimeEditDialog timeEditDialog = new TimeEditDialog(parent, timeWrapper);
                        timeEditDialog.run();
                        
                        if (timeEditDialog.getResult() == EditDialog.RESULT_OK) {
                            Time newTime = timeWrapper.get();
                            if (!newTime.equals(originalTime)) {
                                rawTimeLimit = TimeLimitEditorUtil.changeTimeLimit(rawTimeLimit, originalTime, newTime);
                                layoutProvider.load(rawTimeLimit, slots);
                                mainPanel.repaint();
                                headerPanel.repaint();
                                leftPanel.repaint();
                                updateSizes();
                                highlightListener.mouseMoved(ev);
                                history.commit();
                            }
                        }
                    }
                    return;
                }
                
                LayoutProvider.SlotEntry slotEntry = layoutProvider.getSlotEntryAt(point);
                if (slotEntry != null) {
                    toggleSlot(slotEntry.getSlot(), null);
                    
                    layoutProvider.load(rawTimeLimit, slots);
                    mainPanel.repaint();
                    headerPanel.repaint();
                    leftPanel.repaint();
                    updateSizes();
                    highlightListener.mouseMoved(ev);
                    history.commit();
                    
                    return;
                }

                if (ev.getButton() == 1 && ev.getClickCount() % 2 == 0) {
                    Time pickedTime = layoutProvider.getTimeAt(point);
                    if (pickedTime != null) {
                        insertInterval(pickedTime);
                        
                        layoutProvider.load(rawTimeLimit, slots);
                        mainPanel.repaint();
                        headerPanel.repaint();
                        leftPanel.repaint();
                        updateSizes();
                        highlightListener.mouseMoved(ev);
                        history.commit();
                    }
                }
                
            }
            
            @Override
            public void mousePressed(MouseEvent ev) {
                if (!editable) {
                    return;
                }
                
                mainPanel.requestFocus();
                
                Point point = ev.getPoint();
                
                LayoutProvider.SwitchEntry switchEntry = layoutProvider.getSwitchEntryAt(point);
                if (switchEntry != null) {
                    Time switchTime = switchEntry.getTime();
                    long dayTime = switchTime.getPart(Time.PART.SECONDS_OF_DAY);
                    if (dayTime != 0 || ev.getButton() != 1) {
                        draggingSwitchContainer = new DraggingSwitchContainer(rawTimeLimit, switchEntry.getTime(), point, ev.getButton());
                    }
                }
            }
            
            @Override
            public void mouseReleased(MouseEvent ev) {
                if (!editable) {
                    return;
                }
                
                if (draggingSwitchContainer != null) {
                    if (draggingSwitchContainer.mouseButton == 3) {
                        int yDiff = ev.getY() - draggingSwitchContainer.startPoint.y;
                        rawTimeLimit = TimeLimitEditorUtil.extendSwitch(rawTimeLimit, draggingSwitchContainer.startTime, yDiff > 0);
                    }
                }
                draggingSwitchContainer = null;
                layoutProvider.load(rawTimeLimit, slots);
                headerPanel.repaint();
                leftPanel.repaint();
                highlightListener.mouseMoved(ev);
                history.commit();
            }

            @Override
            public void mouseEntered(MouseEvent ev) {
                highlightListener.mouseMoved(ev);
            }
            
            @Override
            public void mouseExited(MouseEvent ev) {
                highlightListener.mouseMoved(ev);
            }
            
        });
        
        mainPanel.addKeyListener(new KeyAdapter() {
            
            @Override
            public void keyPressed(KeyEvent ev) {
                if (!editable) {
                    return;
                }
                int keyCode = ev.getKeyCode();
                if (keyCode == KeyEvent.VK_ESCAPE) {
                    if (draggingSwitchContainer != null) {
                        ev.consume();
                        abortAction();
                    } else if (keyboardNavigationEnabled) {
                        if (keyboardSwitchEntryToDelete != null) {
                            ev.consume();
                            keyboardSwitchEntryToDelete = null;
                            mainPanel.repaint();
                        } else {
                            ev.consume();
                            stopKeyboardNavigation();
                            mainPanel.repaint();
                            mainPanel.transferFocus();
                        }
                    }
                } else if (keyCode == KeyEvent.VK_TAB) {
                    Point FALLBACK_POSITION = new Point(
                        layoutProvider.getOuterPad() + layoutProvider.getTraverseLeftPad(),
                        layoutProvider.getOuterPad() + layoutProvider.getTopPad()
                    );
                    
                    Point targetPoint = null;
                    if (keyboardPosition != null) {
                        if (keyboardNavigationEnabled) {
                            targetPoint = layoutProvider.getNextTraversalPoint(keyboardPosition, true, !ev.isShiftDown());
                        }
                    } else {
                        targetPoint = layoutProvider.getNextTraversalPoint((Point)null, true, !ev.isShiftDown());
                    }
                    Point newPosition = keyboardPosition;
                    if (targetPoint != null) {
                        newPosition = targetPoint;
                    } else if (keyboardPosition == null) {
                        newPosition = FALLBACK_POSITION;
                    }
                    setKeyboardPosition(newPosition);
                }
                
                if (keyboardNavigationEnabled && keyboardPosition != null) {
                    if (keyboardSwitchEntryToDelete != null) {
                        ev.consume();
                        
                        if (keyCode == KeyEvent.VK_UP || keyCode == KeyEvent.VK_DOWN) {
                            boolean direction = false;
    
                            Time switchTime = keyboardSwitchEntryToDelete.getTime();
                            
                            if (keyCode == KeyEvent.VK_UP) {
                                keyboardSwitchEntryToDelete = null;
                                direction = false;
                            } else {
                                keyboardSwitchEntryToDelete = null;
                                direction = true;
                            }
                            
                            rawTimeLimit = TimeLimitEditorUtil.extendSwitch(rawTimeLimit, switchTime, direction);
    
                            layoutProvider.load(rawTimeLimit, slots);
                            mainPanel.repaint();
                            headerPanel.repaint();
                            leftPanel.repaint();
                            updateSizes();
                            history.commit();
                        }
                    } else {
                        if (keyCode >= KeyEvent.VK_LEFT && keyCode <= KeyEvent.VK_DOWN) {
                            ev.consume();
                            int x = keyboardPosition.x;
                            int y = keyboardPosition.y;
                            int xDiff = ev.isControlDown() ? layoutProvider.getColumnWidth() + layoutProvider.getColumnGap() : 1;
                            int yDiff = ev.isControlDown() ? (int)((15 * Time.MINUTE) / layoutProvider.getSecondsPerPixel()) : 1; 
                            if (keyCode == KeyEvent.VK_LEFT) {
                                x = Math.max(0, x - xDiff);
                            } else if (keyCode == KeyEvent.VK_UP) {
                                y = Math.max(0, y - yDiff);
                            } else if (keyCode == KeyEvent.VK_RIGHT) {
                                x = Math.min(mainPanel.getWidth() - 1, x + xDiff);
                            } else if (keyCode == KeyEvent.VK_DOWN) {
                                y = Math.min(mainPanel.getHeight() - 1, y + yDiff);
                            }
                            setKeyboardPosition(new Point(x, y));
                        } else if (keyCode == KeyEvent.VK_SPACE) {
                            boolean changed = false;
                            if (ev.isControlDown()) {
                                toggleSlotsEntry(layoutProvider.getSlotsEntryAt(keyboardPosition.y));
                            } else if (ev.isShiftDown()) {
                                Time time = layoutProvider.getTimeAt(keyboardPosition);
                                if (time != null) {
                                    toggleDay(time);
                                    changed = true;
                                }
                            } else {
                                LayoutProvider.SlotEntry slotEntry = layoutProvider.getSlotEntryAt(keyboardPosition);
                                if (slotEntry != null) {
                                    toggleSlot(slotEntry.getSlot(), null);
                                    changed = true;
                                }
                            }
                            
                            if (changed) {
                                layoutProvider.load(rawTimeLimit, slots);
                                mainPanel.repaint();
                                headerPanel.repaint();
                                leftPanel.repaint();
                                updateSizes();
                                history.commit();
                            }
                        } else if (keyCode == KeyEvent.VK_ENTER) {
                            LayoutProvider.SwitchEntry switchEntry = layoutProvider.getSwitchEntryAt(keyboardPosition);
                            if (switchEntry != null) {
                                Time originalTime = switchEntry.getTime();
                                GeneralWrapper<Time> timeWrapper = new GeneralWrapper<Time>(originalTime);
                                TimeEditDialog timeEditDialog = new TimeEditDialog(parent, timeWrapper);
                                timeEditDialog.run();
                                
                                if (timeEditDialog.getResult() == EditDialog.RESULT_OK) {
                                    Time newTime = timeWrapper.get();
                                    if (!newTime.equals(originalTime)) {
                                        rawTimeLimit = TimeLimitEditorUtil.changeTimeLimit(rawTimeLimit, originalTime, newTime);
                                        
                                        layoutProvider.load(rawTimeLimit, slots);
                                        mainPanel.repaint();
                                        headerPanel.repaint();
                                        leftPanel.repaint();
                                        updateSizes();
                                        history.commit();
                                    }
                                }
                            }
                        } else if (keyCode == KeyEvent.VK_INSERT) {
                            Time time = layoutProvider.getTimeAt(keyboardPosition);
                            if (time != null) {
                                insertInterval(time);
                                
                                layoutProvider.load(rawTimeLimit, slots);
                                mainPanel.repaint();
                                headerPanel.repaint();
                                leftPanel.repaint();
                                updateSizes();
                                history.commit();
                            }
                        } else if (keyCode == KeyEvent.VK_DELETE) {
                            keyboardSwitchEntryToDelete = layoutProvider.getSwitchEntryAt(keyboardPosition);
                            mainPanel.repaint();
                        }
                    }
                }
            }
        });
        
        mainPanel.setFocusable(true);
        mainPanel.setFocusTraversalKeysEnabled(false);
        
        mainScrollPane = new JScrollPane(mainPanel);
        mainScrollPane.setBorder(null);
        
        mainScrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            
            @Override
            public void adjustmentValueChanged(AdjustmentEvent ev) {
                leftPanel.repaint();
            }
            
        });
        
        mainScrollPane.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            
            @Override
            public void adjustmentValueChanged(AdjustmentEvent ev) {
                headerPanel.repaint();
            }
            
        });
        
        add(mainScrollPane, BorderLayout.CENTER);
    }
    
    private void stopKeyboardNavigation() {
        keyboardNavigationEnabled = false;
        keyboardSwitchEntryToDelete = null;
        mainPanel.repaint();
    }
    
    private void setKeyboardPosition(Point newPoisition) {
        keyboardPosition = newPoisition;
        keyboardNavigationEnabled = true;
        mainPanel.repaint();
        scrollToKeyboard();
    }
    
    private void scrollToKeyboard() {
        if (!keyboardNavigationEnabled || keyboardPosition == null) {
            return;
        }
        
        JViewport viewport = mainScrollPane.getViewport();
        JScrollBar horizontalScrollBar = mainScrollPane.getHorizontalScrollBar();
        JScrollBar verticalScrollBar = mainScrollPane.getVerticalScrollBar();
        
        int iconWidth = keyboardCursorImage.getWidth(null);
        int iconHeight = keyboardCursorImage.getHeight(null);

        int fullWidth = mainPanel.getWidth();
        int fullHeight = mainPanel.getHeight();
        int innerWidth = viewport.getWidth();
        int innerHeight = viewport.getHeight();
        int scrollX = horizontalScrollBar.getValue();
        int scrollY = verticalScrollBar.getValue();
        
        if (keyboardPosition.x < scrollX) {
            horizontalScrollBar.setValue(Math.max(0, keyboardPosition.x));
        } else if (keyboardPosition.x + iconWidth > scrollX + innerWidth) {
            horizontalScrollBar.setValue(Math.min(fullWidth - innerWidth, keyboardPosition.x + iconWidth - innerWidth));
        }
        
        if (keyboardPosition.y < scrollY) {
            verticalScrollBar.setValue(Math.max(0, keyboardPosition.y));
        } else if (keyboardPosition.y + iconHeight > scrollY + innerHeight) {
            verticalScrollBar.setValue(Math.min(fullHeight - innerHeight, keyboardPosition.y + iconHeight - innerHeight));
        }
    }
    
    private void toggleSlot(Slot slot, Boolean newState) {
        long TOOGLE_SLOT_MAX_PAD = layoutProvider.getToggleSlotMaxPad();
        
        Interval slotInterval = slot.getInterval();
        Time slotStartTime = slotInterval.getStart();
        Time slotEndTime = slotInterval.getEnd();
        long slotStartTimeSeconds = slotStartTime.getSeconds();
        long slotEndTimeSeconds = slotEndTime.getSeconds();
        long previousTimeSeconds = Long.MIN_VALUE;
        long nextTimeSeconds = Long.MAX_VALUE;
        boolean startEmpty = true;
        boolean endEmpty = true;
        for (Slot _slot: slots) {
            Interval _slotInterval = _slot.getInterval();
            if (_slotInterval.equals(slotInterval)) {
                continue;
            }
            long _slotStartTimeSeconds = _slotInterval.getStart().getSeconds();
            long _slotEndTimeSeconds = _slotInterval.getEnd().getSeconds();
            if (_slotStartTimeSeconds < slotStartTimeSeconds) {
                if (_slotEndTimeSeconds <= slotStartTimeSeconds) {
                    previousTimeSeconds = Math.max(previousTimeSeconds, _slotEndTimeSeconds);
                } else {
                    previousTimeSeconds = Math.max(previousTimeSeconds, _slotStartTimeSeconds);
                    startEmpty = false;
                }
            }
            if (_slotEndTimeSeconds > slotEndTimeSeconds) {
                if (_slotStartTimeSeconds >= slotEndTimeSeconds) {
                    nextTimeSeconds = Math.min(nextTimeSeconds, _slotStartTimeSeconds);
                    break; // sorted
                } else {
                    nextTimeSeconds = Math.min(nextTimeSeconds, _slotEndTimeSeconds);
                    endEmpty = false;
                }
            }
        }
        
        long padAreaStartTimeSeconds = slotStartTimeSeconds;
        if (startEmpty) {
            long _half = (previousTimeSeconds / 2) + (slotStartTimeSeconds / 2);
            _half = _half / Time.MINUTE * Time.MINUTE;
            long _pad = slotStartTimeSeconds - TOOGLE_SLOT_MAX_PAD;
            padAreaStartTimeSeconds = Math.max(_half, _pad);
        }
        
        long padAreaEndTimeSeconds = slotEndTimeSeconds;
        if (endEmpty) {
            long _half = (slotEndTimeSeconds / 2) + (nextTimeSeconds / 2);
            _half = (long)Math.ceil(((double)_half / Time.MINUTE)) * Time.MINUTE;
            long _pad = slotEndTimeSeconds + TOOGLE_SLOT_MAX_PAD;
            padAreaEndTimeSeconds = Math.min(_half, _pad);
        }
        
        Long previousSwitchSeconds = null;
        Long nextSwitchSeconds = null;
        for (Time _switch: rawTimeLimit.getTimes()) {
            long _switchSeconds = _switch.getSeconds();
            if (_switchSeconds < padAreaStartTimeSeconds) {
                previousSwitchSeconds = _switch.getSeconds();
            } else if (_switchSeconds > padAreaEndTimeSeconds) {
                nextSwitchSeconds = _switch.getSeconds();
                break;
            }
        }

        long previousDayStartSeconds = padAreaStartTimeSeconds / Time.DAY * Time.DAY;
        long nextDayStartSeconds = (long)Math.ceil((double)padAreaEndTimeSeconds / Time.DAY) * Time.DAY;

        long previousAreaStartSeconds = (previousSwitchSeconds == null) ?
            previousDayStartSeconds :
            Math.max(previousDayStartSeconds, previousSwitchSeconds)
        ;

        long nextAreaEndSeconds = (nextSwitchSeconds == null) ?
            nextDayStartSeconds :
            Math.min(nextDayStartSeconds, nextSwitchSeconds)
        ;
        
        TimeLimit previousArea;
        if (previousAreaStartSeconds <= layoutProvider.getStartDay()) {
            previousArea = new CustomTimeLimit(true, new Time(padAreaStartTimeSeconds));
        } else {
            previousArea = new Interval(previousAreaStartSeconds, padAreaStartTimeSeconds);
        }
        
        TimeLimit nextArea;
        if (nextAreaEndSeconds >= layoutProvider.getEndDay() + Time.DAY) {
            nextArea = new CustomTimeLimit(false, new Time(padAreaEndTimeSeconds));
        } else {
            nextArea = new Interval(nextAreaEndSeconds, padAreaEndTimeSeconds);
        }
        
        boolean previousAreaEmpty = true;
        for (Slot _slot: slots) {
            if (previousArea.intersects(_slot.getInterval())) {
                previousAreaEmpty = false;
                break;
            }
        }

        boolean nextAreaEmpty = true;
        for (Slot _slot: slots) {
            if (nextArea.intersects(_slot.getInterval())) {
                nextAreaEmpty = false;
                break;
            }
        }
        
        boolean isFree = rawTimeLimit.contains(slotInterval);
        
        if (newState != null && newState == isFree) {
            return;
        }
        
        if (isFree) {
            
            NavigableSet<Time> partitionEndTimes = new TreeSet<Time>();
            for (Slot _slot: slots) {
                Interval interval = _slot.getInterval();
                Time _slotStartTime = interval.getStart();
                Time _slotEndTime = interval.getEnd();
                if (interval.intersects(slotInterval)) {
                    if (_slotStartTime.compareTo(slotStartTime) > 0) {
                        partitionEndTimes.add(_slotStartTime);
                    }
                    if (_slotEndTime.compareTo(slotEndTime) < 0) {
                        partitionEndTimes.add(_slotEndTime);
                    }
                }
            }
            partitionEndTimes.add(slotEndTime);
            
            List<Interval> partitions = new ArrayList<Interval>();
            Time partitionStartTime = slotStartTime;
            for (Time partitionEndTime: partitionEndTimes) {
                partitions.add(new Interval(partitionStartTime, partitionEndTime));
                partitionStartTime = partitionEndTime;
            }
            int partitionCount = partitions.size();
            
            Interval winnerPartition = null;
            int winnerPartitionIndex = -1;
            int winnerPartitionFreeSlotCount = Integer.MAX_VALUE;
            int winnerPartitionSlotCount = Integer.MAX_VALUE;
            long winnerPartitionLength = -1;
            Map<Interval, Boolean> partitionBannableMap = new HashMap<Interval, Boolean>(); 
            for (int i = 0; i < partitionCount; i++) {
                Interval partition = partitions.get(i);
                int partitionFreeSlotCount = 0;
                int partitionSlotCount = 0;
                long partitionLength = partition.getLength();
                for (Slot _slot: slots) {
                    Interval interval = _slot.getInterval();
                    if (interval.intersects(partition)) {
                        partitionSlotCount++;
                        if (rawTimeLimit.contains(interval)) {
                            partitionFreeSlotCount++;
                        }
                    }
                    if (interval.getStart().compareTo(partition.getEnd()) >= 0) {
                        break;
                    }
                }
                partitionBannableMap.put(partition, partitionFreeSlotCount == 1);
                if (
                    partitionFreeSlotCount < winnerPartitionFreeSlotCount || (
                        partitionFreeSlotCount == winnerPartitionFreeSlotCount && (

                            partitionSlotCount < winnerPartitionSlotCount || (
                                partitionSlotCount == winnerPartitionSlotCount && (

                                    partitionLength < winnerPartitionLength
                                    
                                )
                            )
                            
                        )
                    )
                ) {
                    winnerPartition = partition;
                    winnerPartitionIndex = i;
                    winnerPartitionFreeSlotCount = partitionFreeSlotCount;
                    winnerPartitionSlotCount = partitionSlotCount;
                    winnerPartitionLength = partitionLength;
                }
            }
            
            rawTimeLimit = rawTimeLimit.differenceWith(winnerPartition);
            
            if (winnerPartition.getStart().equals(slotStartTime)) {
                rawTimeLimit = rawTimeLimit.differenceWith(new Interval(padAreaStartTimeSeconds, slotStartTimeSeconds));
                if (previousAreaEmpty) {
                    rawTimeLimit = rawTimeLimit.differenceWith(previousArea);
                }
            } else {
                for (int i = winnerPartitionIndex - 1; i >= 0; i--) {
                    Interval partition = partitions.get(i);
                    if (partitionBannableMap.get(partition)) {
                        rawTimeLimit = rawTimeLimit.differenceWith(partition);
                    } else {
                        break;
                    }
                }
            }
            
            if (winnerPartition.getEnd().equals(slotEndTime)) {
                rawTimeLimit = rawTimeLimit.differenceWith(new Interval(slotEndTimeSeconds, padAreaEndTimeSeconds));
                if (nextAreaEmpty) {
                    rawTimeLimit = rawTimeLimit.differenceWith(nextArea);
                }
            } else {
                for (int i = winnerPartitionIndex + 1; i < partitionCount; i++) {
                    Interval partition = partitions.get(i);
                    if (partitionBannableMap.get(partition)) {
                        rawTimeLimit = rawTimeLimit.differenceWith(partition);
                    } else {
                        break;
                    }
                }
            }
            
        } else {
            rawTimeLimit = rawTimeLimit.unionWith(new CustomTimeLimit(
                new Interval(padAreaStartTimeSeconds, padAreaEndTimeSeconds)
            ));
            if (previousAreaEmpty) {
                rawTimeLimit = rawTimeLimit.unionWith(previousArea);
            }
            if (nextAreaEmpty) {
                rawTimeLimit = rawTimeLimit.unionWith(nextArea);
            }
        }
    }
    
    private void toggleDay(Time time) {
        if (time == null) {
            return;
        }
        
        long seconds = time.getSeconds();
        long daySeconds = seconds - (seconds % Time.DAY);
        Time dayTime = new Time(daySeconds);
        Interval dayInterval = new Interval(dayTime, Time.DAY);

        long startDaySeconds = layoutProvider.getStartDay();
        long endDaySeconds = layoutProvider.getEndDay();
        
        TimeLimit toggleTimeLimit;
        if (daySeconds == startDaySeconds) {
            toggleTimeLimit = new CustomTimeLimit(true, dayInterval.getEnd());
        } else if (daySeconds == endDaySeconds) {
            toggleTimeLimit = new CustomTimeLimit(false, dayTime);
        } else {
            toggleTimeLimit = dayInterval;
        }
        
        if (rawTimeLimit.contains(dayInterval)) {
            rawTimeLimit = rawTimeLimit.differenceWith(toggleTimeLimit);
        } else {
            rawTimeLimit = rawTimeLimit.unionWith(toggleTimeLimit);
        }
        
        refreshView();
        history.commit();
    }
    
    private void toggleSlotsEntry(LayoutProvider.SlotsEntry slotsEntry) {
        if (slotsEntry == null) {
            return;
        }
        
        List<Slot> slotsToToggle = slotsEntry.getSlots();
        
        if (slotsToToggle.isEmpty()) {
            return;
        }
        
        boolean isAllFree = true;
        for (Slot slot : slotsToToggle) {
            if (!rawTimeLimit.contains(slot.getInterval())) {
                isAllFree = false;
                break;
            }
        }
        for (Slot slot : slotsToToggle) {
            toggleSlot(slot, !isAllFree);
        }

        refreshView();
        history.commit();
    }
    
    private void insertInterval(Time pickedTime) {
        long MIN_PAD = 5 * Time.MINUTE;
        long DIFF_BETWEEN = 10 * Time.MINUTE;
        
        long pickedTimeSeconds = pickedTime.getSeconds();
        
        Time previousTime = TimeLimitEditorUtil.getPreviousSwitch(rawTimeLimit, pickedTime, false);
        long previousTimeSeconds = (previousTime == null) ? Long.MIN_VALUE : previousTime.getSeconds();
        
        Time nextTime = TimeLimitEditorUtil.getNextSwitch(rawTimeLimit, pickedTime, true);
        long nextTimeSeconds = (nextTime == null) ? Long.MAX_VALUE : nextTime.getSeconds();
        
        long time1Seconds, time2Seconds;
        
        if (previousTime == null || nextTime == null || nextTimeSeconds - previousTimeSeconds > MIN_PAD * 2 + DIFF_BETWEEN) {
            time1Seconds = Math.max(previousTimeSeconds + MIN_PAD, pickedTimeSeconds - (DIFF_BETWEEN / 2));
            time2Seconds = time1Seconds + DIFF_BETWEEN;
        } else {
            long diffSeconds = nextTimeSeconds - previousTimeSeconds;
            long thirdDiffSeconds = diffSeconds / 3;
            time1Seconds = previousTimeSeconds + thirdDiffSeconds;
            time2Seconds = nextTimeSeconds - thirdDiffSeconds;
        }
        
        List<Time> newTimes = rawTimeLimit.getTimes();
        newTimes.add(new Time(time1Seconds));
        newTimes.add(new Time(time2Seconds));
        Collections.sort(newTimes);
        
        rawTimeLimit = new CustomTimeLimit(rawTimeLimit.getStartState(), newTimes);
    }
    
    public void load(TimeLimit timeLimit) {
        load(timeLimit, null, null);
    }
    
    public void load(TimeLimit timeLimit, Document document, Labeled contextObject) {
        this.slots = new ArrayList<Slot>();
        if (contextObject instanceof Aspect) {
            Aspect aspect = (Aspect) contextObject;
            this.slots.addAll(new AspectSlotCollector().collectOrDefault(document, aspect));
        }
        this.rawTimeLimit = new CustomTimeLimit(timeLimit);
        this.history = new TimeLimitHistory(rawTimeLimit);
        
        refreshView();
    }
    
    public void abortAction() {
        if (draggingSwitchContainer != null) {
            rawTimeLimit = draggingSwitchContainer.originalTimeLimit;
        }
        draggingSwitchContainer = null;
        highlightedRectangle = null;
        layoutProvider.load(rawTimeLimit, slots);
        mainPanel.setCursor(Cursor.getDefaultCursor());
        mainPanel.repaint();
        headerPanel.repaint();
        leftPanel.repaint();
    }

    public void set(TimeLimit timeLimit) {
        this.rawTimeLimit = new CustomTimeLimit(timeLimit);
        history.commit();
        refreshView();
    }
    
    public boolean undo() {
        abortAction();
        if (!history.rollBackPrevious()) {
            return false;
        }
        refreshView();
        return true;
    }
    
    public boolean redo() {
        abortAction();
        if (!history.executeNext()) {
            return false;
        }
        refreshView();
        return true;
    }
    
    public void focusMain() {
        mainPanel.requestFocusInWindow();
    }
    
    public TimeLimit getTimeLimit() {
        return rawTimeLimit.getSimplified();
    }
    
    private void refreshView() {
        layoutProvider.load(rawTimeLimit, slots);
        mainPanel.repaint();
        headerPanel.repaint();
        leftPanel.repaint();
        updateSizes();
    }
    
    private void updateSizes() {
        int width = layoutProvider.getWidth();
        int height = layoutProvider.getHeight();
        mainPanel.setPreferredSize(new Dimension(width, height));
        mainPanel.setMinimumSize(new Dimension(width, height));
        mainScrollPane.revalidate();
    }
    
    private class DraggingSwitchContainer {

        final CustomTimeLimit originalTimeLimit;

        final Time startTime;

        final Point startPoint;

        final int mouseButton;

        DraggingSwitchContainer(CustomTimeLimit originalTimeLimit, Time startTime, Point startPoint, int mouseButton) {
            this.originalTimeLimit = originalTimeLimit;
            this.startTime = startTime;
            this.startPoint = startPoint;
            this.mouseButton = mouseButton;
        }
        
    }
    
    private class TimeLimitHistory extends LinearHistory {
        
        CustomTimeLimit committedTimeLimit = null;
        
        TimeLimitHistory(CustomTimeLimit initialTimeLimit) {
            this.committedTimeLimit = initialTimeLimit;
        }
        
        boolean commit() {
            if (!rawTimeLimit.equals(committedTimeLimit)) {
                return addAndExecute(new TimeLimitCommand(committedTimeLimit, rawTimeLimit));
            } else {
                return false;
            }
        }
        
        class TimeLimitCommand extends AbstractCommand {
            
            final CustomTimeLimit previous;
            
            final CustomTimeLimit next;
            
            TimeLimitCommand(CustomTimeLimit previous, CustomTimeLimit next) {
                this.previous = previous;
                this.next = next;
            }
            
            @Override
            protected boolean _execute() {
                rawTimeLimit = committedTimeLimit = next;
                return true;
            }

            @Override
            protected boolean _rollBack() {
                rawTimeLimit = committedTimeLimit = previous;
                return true;
            }

        }
        
    }

}

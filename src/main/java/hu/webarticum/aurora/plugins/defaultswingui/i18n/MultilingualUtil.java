package hu.webarticum.aurora.plugins.defaultswingui.i18n;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JMenu;

public class MultilingualUtil {

    public static void reloadComponent(Component component) {
        if (component instanceof JMenu) {
            JMenu menu = ((JMenu)component);
            int itemCount = menu.getItemCount();
            for (int i=0; i<itemCount; i++) {
                reloadComponent(menu.getItem(i));
            }
        } else if (component instanceof Container) {
            for (Component childComponent: ((Container)component).getComponents()) {
                reloadComponent(childComponent);
            }
        }
        if (component instanceof MultilingualComponent) {
            ((MultilingualComponent)component).reloadLanguageTexts();
        }
    }

}

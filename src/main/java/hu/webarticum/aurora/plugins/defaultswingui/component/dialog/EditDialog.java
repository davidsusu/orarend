package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.text.JTextComponent;

import hu.webarticum.aurora.core.model.Store;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.ColorButton;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.OnOffPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.TimeLimitManagerPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.TimingSetManagerPanel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.DocumentChangeListener;
import hu.webarticum.chm.AbstractCommand;
import hu.webarticum.chm.Memento;



public abstract class EditDialog extends Dialog {
    
    private static final long serialVersionUID = 1L;

    
    public static final int RESULT_OK = 0;
    
    public static final int RESULT_CANCEL = 1;
    
    
    protected JPanel contentPanel;
    
    protected JScrollPane contentScrollPane;
    
    protected JPanel mainPanel;
    
    protected MultilingualButton okButton;
    
    protected MultilingualButton cancelButton;

    protected int formWidth = 400;

    protected int leftWidth = 150;

    protected boolean modified = false;
    
    protected JPanel returnButtonPanel;

    protected int result = RESULT_CANCEL;

    
    protected ModifyDocumentListener modifyDocumentListener = new ModifyDocumentListener();
    
    protected ModifyItemListener modifyItemListener = new ModifyItemListener();
    
    protected ModifyChangeListener modifyChangeListener = new ModifyChangeListener();

    
    public EditDialog(JFrame parent) {
        super(parent);
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            
            @Override
            public void windowClosing(WindowEvent ev) {
                tryCancel();
            }
            
        });
    }

    public void fireOk() {
        result = RESULT_OK;
        if (trySave()) {
            setVisible(false);
            dispose();
        }
    }
    
    protected abstract void load();

    protected abstract void save();

    protected String generateTitle() {
        return title;
    }
    
    @Override
    protected void init() {
        init(true, true);
    }

    protected void init(boolean addContentPanel, boolean addButtonPanel) {
        init(addContentPanel, addButtonPanel, true);
    }
    
    protected void init(boolean addContentPanel, boolean addButtonPanel, boolean addContentToPageStart) {
        setTitle(title);
        
        if (addContentPanel) {
            contentPanel = new JPanel(new BorderLayout());
            contentScrollPane = new JScrollPane(contentPanel);
            contentScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            contentScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
            contentScrollPane.setBorder(BorderFactory.createEmptyBorder());
            containerPanel.add(contentScrollPane, BorderLayout.CENTER);
            
            mainPanel = new JPanel();
            mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
            mainPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
            contentPanel.add(mainPanel, addContentToPageStart ? BorderLayout.PAGE_START : BorderLayout.CENTER);
        }
        
        if (addButtonPanel) {
            returnButtonPanel = new JPanel();
    
            cancelButton = new MultilingualButton("ui.swing.dialog.edit.cancel");
            returnButtonPanel.add(cancelButton);
            cancelButton.addActionListener(new ActionListener() {
    
                @Override
                public void actionPerformed(ActionEvent e) {
                    EditDialog.this.tryCancel();
                }
              
            });
            getContentPane().add(returnButtonPanel, BorderLayout.SOUTH);
    
            okButton = new MultilingualButton("ui.swing.dialog.edit.ok", getPurpose());
            returnButtonPanel.add(okButton);
            okButton.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent e) {
                    EditDialog.this.fireOk();
                }
                
            });
            getContentPane().add(returnButtonPanel, BorderLayout.SOUTH);
        }

        build();
        load();
        afterLoad();
        loaded = true;
    }
    
    protected ButtonPurpose getPurpose() {
        return ButtonPurpose.SAVE;
    }

    protected void afterLoad() {
        refreshTitle();
    }
    
    protected void refreshTitle() {
        setTitle(generateTitle());
    }

    protected void setSaveable(boolean saveable) {
        okButton.setVisible(saveable);
        cancelButton.setLanguagePath(saveable?"ui.swing.dialog.edit.cancel":"ui.swing.dialog.edit.close");
    }

    protected JPanel addSeparator() {
        return addSeparatorTo(mainPanel);
    }

    protected JPanel addSpace() {
        return addSpaceTo(mainPanel);
    }

    protected JPanel addRow(String label) {
        return addRow(new JLabel(label));
    }

    protected JPanel addRow(JComponent component) {
        return addRowTo(mainPanel, component, formWidth);
    }

    protected JPanel addRow(String label, JComponent content) {
        JLabel leftLabel = new JLabel(label);
        return addRow(leftLabel, content);
    }

    protected JPanel addRow(JComponent leftComponent, JComponent rightComponent) {
        return addRowTo(mainPanel, leftComponent, rightComponent, formWidth, leftWidth);
    }

    protected JPanel addSeparatorTo(JComponent parentComponent) {
        JPanel separatorPanel = new JPanel(new BorderLayout());
        separatorPanel.setOpaque(false);
        JSeparator separator = new JSeparator();
        separator.setPreferredSize(new Dimension(100, 5));
        separatorPanel.add(separator, BorderLayout.CENTER);
        separatorPanel.setBorder(new EmptyBorder(7, 0, 7, 0));
        parentComponent.add(separatorPanel);
        return separatorPanel;
    }

    protected JPanel addSpaceTo(JComponent parentComponent) {
        JPanel spacePanel = new JPanel(new BorderLayout());
        spacePanel.setOpaque(false);
        spacePanel.setPreferredSize(new Dimension(12, 12));
        parentComponent.add(spacePanel);
        return spacePanel;
    }
    
    protected JPanel addRowTo(JComponent parentComponent, String label, int fullwidth) {
        return addRowTo(parentComponent, new JLabel(label), fullwidth);
    }

    protected JPanel addRowTo(JComponent parentComponent, JComponent component, int fullwidth) {
        return addRowTo(parentComponent, component, fullwidth, component instanceof JPanel);
    }
    
    protected JPanel addRowTo(JComponent parentComponent, JComponent component, int fullwidth, boolean stretch) {
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(new BorderLayout());
        if (stretch) {
            rowPanel.add(component, BorderLayout.CENTER);
        } else {
            rowPanel.setPreferredSize(new Dimension(fullwidth, 30));
            rowPanel.add(component, BorderLayout.LINE_START);
        }
        parentComponent.add(rowPanel);
        return rowPanel;
    }

    protected JPanel addRowTo(
        JComponent parentComponent, String label, JComponent rightComponent,
        int fullwidth, int leftwidth
    ) {
        JLabel leftLabel = new JLabel(label);
        return addRowTo(parentComponent, leftLabel, rightComponent, fullwidth, leftwidth);
    }

    protected JPanel addRowTo(
        JComponent parentComponent, JComponent leftComponent, JComponent rightComponent,
        int fullwidth, int leftwidth
    ) {
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(new BorderLayout());
        rowPanel.setOpaque(false);
        JPanel leftPanel = new JPanel();
        leftPanel.setOpaque(false);
        leftPanel.setBorder(new EmptyBorder(8, 0, 0, 0));
        leftComponent.setAlignmentY(0);
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.LINE_AXIS));
        leftPanel.setPreferredSize(new Dimension(leftwidth, 30));
        leftPanel.add(leftComponent);
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.LINE_AXIS));
        rightPanel.setOpaque(false);
        rightPanel.add(rightComponent);
        rightPanel.setPreferredSize(new Dimension(fullwidth-leftwidth, rightComponent.getPreferredSize().height));
        rowPanel.add(leftPanel, BorderLayout.LINE_START);
        rowPanel.add(rightPanel, BorderLayout.CENTER);
        parentComponent.add(rowPanel);
        return rowPanel;
    }

    public int getResult() {
        return result;
    }
    
    protected boolean canSave() {
        return true;
    }
    
    protected boolean trySave() {
        if (canSave()) {
            save();
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    protected void tryCancel() {
        if (modified) {
            // FIXME
            int result = OptionPaneUtil.showConfirmDialog(
                parent,
                text("ui.swing.dialog.edit.unsaved.message"),
                text("ui.swing.dialog.edit.unsaved.title"),
                ButtonPurpose.ABORT
            );
            if (result!=JOptionPane.OK_OPTION) {
                return;
            }
        }
        result = RESULT_CANCEL;
        super.tryCancel();
    }
    
    
    
    
    protected void bindModifyListener(JTextComponent textComponent) {
        textComponent.getDocument().addDocumentListener(modifyDocumentListener);
    }

    protected void bindModifyListener(JCheckBox checkBox) {
        checkBox.addItemListener(modifyItemListener);
    }

    protected void bindModifyListener(JComboBox<?> comboBox) {
        comboBox.addItemListener(modifyItemListener);
    }

    protected void bindModifyListener(ColorButton colorButton) {
        colorButton.addItemListener(modifyItemListener);
    }

    protected void bindModifyListener(OnOffPanel onOffPanel) {
        onOffPanel.addItemListener(modifyItemListener);
    }

    protected void bindModifyListener(TimingSetManagerPanel panel) {
        panel.addChangeListener(modifyChangeListener);
    }

    protected void bindModifyListener(TimeLimitManagerPanel panel) {
        panel.addChangeListener(modifyChangeListener);
    }

    protected void bindModifyListener(JSpinner spinner) {
        spinner.addChangeListener(modifyChangeListener);
    }

    
    protected class ModifyDocumentListener extends DocumentChangeListener {
        
        @Override
        public void changed(DocumentEvent ev) {
            EditDialog.this.modified = true;
        }
        
    }
    
    protected class ModifyItemListener implements ItemListener {
        
        @Override
        public void itemStateChanged(ItemEvent e) {
            EditDialog.this.modified = true;
        }
        
    }
    
    protected class ModifyChangeListener implements ChangeListener {
        
        @Override
        public void stateChanged(ChangeEvent ev) {
            EditDialog.this.modified = true;
        }
        
    }

    public static class StoreItemEditCommand<T> extends AbstractCommand {

        final String labelPath;
        final Store<T> store;
        final T object;
        final Memento<T> oldMemento;
        final Memento<T> newMemento;
        
        public StoreItemEditCommand(String labelPath, Store<T> store, T object, Memento<T> oldMemento, Memento<T> newMemento) {
            this.labelPath = labelPath;
            this.store = store;
            this.object = object;
            this.oldMemento = oldMemento;
            this.newMemento = newMemento;
        }
        
        @Override
        protected boolean _execute() {
            newMemento.apply(object);
            store.refresh(object);
            return true;
        }

        @Override
        protected boolean _rollBack() {
            oldMemento.apply(object);
            store.refresh(object);
            return true;
        }

        @Override
        public String toString() {
            return text(labelPath);
        }

    }

    public static class StoreItemRegisterCommand<T> extends AbstractCommand {

        final String labelPath;
        final Store<T> store;
        final T object;
        
        public StoreItemRegisterCommand(String labelPath, Store<T> store, T object) {
            this.labelPath = labelPath;
            this.store = store;
            this.object = object;
        }
        
        @Override
        protected boolean _execute() {
            store.register(object);
            return true;
        }

        @Override
        protected boolean _rollBack() {
            store.remove(object);
            return true;
        }
        
        @Override
        public String toString() {
            return text(labelPath);
        }

    }
    
}
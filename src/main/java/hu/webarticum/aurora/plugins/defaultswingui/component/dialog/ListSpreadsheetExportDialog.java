package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.FileUtil;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.ResourceSubset;
import hu.webarticum.aurora.core.model.ResourceSubsetList;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.BlockFilterPanel;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.FileChooserField;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.AutoExtensionFileChooser;
import hu.webarticum.simplespreadsheetwriter.HssfApachePoiSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.HtmlSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.OdfToolkitSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.SerializeSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.Sheet;
import hu.webarticum.simplespreadsheetwriter.Spreadsheet;
import hu.webarticum.simplespreadsheetwriter.SpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.XssfApachePoiSpreadsheetDumper;


public class ListSpreadsheetExportDialog extends AbstractExportDialog {

    private static final long serialVersionUID = 1L;
    
    private static final Font ICON_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 20);

    
    private final Board board;
    
    private final Map<Pair<TimingSet, Long>, SortedMap<Time, Integer>> slotMap =
        new HashMap<Pair<TimingSet, Long>, SortedMap<Time, Integer>>()
    ;

    private final JTextField titleTextField = new JTextField();

    private final BlockFilterPanel blockFilterPanel;

    private final JComboBox<Template> templateComboBox = new JComboBox<Template>();
    
    private final DefaultComboBoxModel<Template> templateComboBoxModel = new DefaultComboBoxModel<Template>();
    
    private final JCheckBox extractPeriodsCheckBox = new JCheckBox();
    
    private final JCheckBox extractGroupsCheckBox = new JCheckBox();

    private final JList<ColumnDefinition> availableColumnsList = new JList<ColumnDefinition>();
    
    private final DefaultListModel<ColumnDefinition> availableColumnsListModel = new DefaultListModel<ColumnDefinition>();

    private final JList<ColumnDefinition> selectedColumnsList = new JList<ColumnDefinition>();
    
    private final DefaultListModel<ColumnDefinition> selectedColumnsListModel = new DefaultListModel<ColumnDefinition>();

    private final AutoExtensionFileChooser saveFileChooser = new AutoExtensionFileChooser();
    
    private final FileChooserField fileChooserField = new FileChooserField();

    private final JCheckBox openCheckBox = new JCheckBox();
    
    private final Map<String, SpreadsheetFormat> formatMap = new LinkedHashMap<String, SpreadsheetFormat>();
    
    
    public ListSpreadsheetExportDialog(JFrame parent, Document document, Board board) {
        super(parent);
        this.title = text("ui.swing.dialog.listspreadsheetexport.title");
        this.board = board;
        this.blockFilterPanel = new BlockFilterPanel(parent, document);
        this.formWidth = 500;
        init();
    }

    
    @Override
    protected void build() {
        {
            formatMap.put("ods", new SpreadsheetFormat("ods", text("ui.swing.dialog.listspreadsheetexport.format.ods"), new OdfToolkitSpreadsheetDumper()));
            formatMap.put("xls", new SpreadsheetFormat("xls", text("ui.swing.dialog.listspreadsheetexport.format.xls"), new HssfApachePoiSpreadsheetDumper()));
            formatMap.put("xlsx", new SpreadsheetFormat("xlsx", text("ui.swing.dialog.listspreadsheetexport.format.xlsx"), new XssfApachePoiSpreadsheetDumper()));
            formatMap.put("html", new SpreadsheetFormat("html", text("ui.swing.dialog.listspreadsheetexport.format.html"), new HtmlSpreadsheetDumper()));
            formatMap.put("obj", new SpreadsheetFormat("obj", text("ui.swing.dialog.listspreadsheetexport.format.obj"), new SerializeSpreadsheetDumper()));
            
            saveFileChooser.setAppendExtension(true);
            saveFileChooser.setMultiSelectionEnabled(false);
            saveFileChooser.setAcceptAllFileFilterUsed(true);
            for (Map.Entry<String, SpreadsheetFormat> entry: formatMap.entrySet()) {
                saveFileChooser.addChoosableFileFilter(entry.getValue().fileFilter);
            }
        }
        
        addRow(text("ui.swing.dialog.listspreadsheetexport.sheettitle"), titleTextField);

        addSeparator();

        blockFilterPanel.setColorAspectVisible(false);
        addRow(blockFilterPanel);

        addSeparator();

        JPanel templatePanel = new JPanel(new BorderLayout());
        
        JLabel templateLabel = new MultilingualLabel("ui.swing.dialog.listspreadsheetexport.template");
        templateLabel.setPreferredSize(new Dimension(leftWidth, 30));
        templatePanel.add(templateLabel, BorderLayout.LINE_START);
        
        for (Template template : Template.values()) {
            templateComboBoxModel.addElement(template);
        }
        templateComboBox.setModel(templateComboBoxModel);
        templatePanel.add(templateComboBox, BorderLayout.CENTER);

        JButton loadTemplateButton = new MultilingualButton("ui.swing.dialog.listspreadsheetexport.load_template");
        loadTemplateButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                loadTemplate((Template) templateComboBox.getSelectedItem());
            }
            
        });
        templatePanel.add(loadTemplateButton, BorderLayout.LINE_END);

        addRow(templatePanel);

        JPanel checkBoxesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        extractPeriodsCheckBox.setText(text("ui.swing.dialog.listspreadsheetexport.extract_periods"));
        checkBoxesPanel.add(extractPeriodsCheckBox);
        extractGroupsCheckBox.setText(text("ui.swing.dialog.listspreadsheetexport.extract_groups"));
        extractGroupsCheckBox.setBorder(new EmptyBorder(0, 25, 0, 0));
        checkBoxesPanel.add(extractGroupsCheckBox);
        addRow(checkBoxesPanel);
        
        JPanel columnsPanel = new JPanel(new BorderLayout());
        columnsPanel.setBorder(new EmptyBorder(5, 0, 5, 0));

        JPanel columnsLeftPanel = new JPanel(new BorderLayout());
        columnsPanel.add(columnsLeftPanel, BorderLayout.LINE_START);
        
        availableColumnsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        availableColumnsList.setModel(availableColumnsListModel);
        JScrollPane availabelColumnsScrollPane = new JScrollPane(
            availableColumnsList,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );
        availabelColumnsScrollPane.setPreferredSize(new Dimension(leftWidth + 50, 150));
        columnsLeftPanel.add(availabelColumnsScrollPane, BorderLayout.CENTER);
        
        JPanel middleButtonPanel = new JPanel();
        middleButtonPanel.setBorder(new EmptyBorder(40, 0, 0, 0));
        middleButtonPanel.setLayout(new BoxLayout(middleButtonPanel, BoxLayout.PAGE_AXIS));
        columnsLeftPanel.add(middleButtonPanel, BorderLayout.LINE_END);

        JButton selectColumnsButton = new JButton("\u21D2");
        selectColumnsButton.setFont(ICON_FONT);
        selectColumnsButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                selectColumns();
            }
            
        });
        middleButtonPanel.add(selectColumnsButton);
        
        JButton unselectColumnsButton = new JButton("\u21D0");
        unselectColumnsButton.setFont(ICON_FONT);
        unselectColumnsButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                unselectColumns();
            }
            
        });
        middleButtonPanel.add(unselectColumnsButton);
        
        JPanel columnsRightPanel = new JPanel(new BorderLayout());
        columnsPanel.add(columnsRightPanel);

        columnsRightPanel.add(
            new MultilingualLabel("ui.swing.dialog.listspreadsheetexport.columns"),
            BorderLayout.PAGE_START
        );
        
        selectedColumnsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        selectedColumnsList.setModel(selectedColumnsListModel);
        JScrollPane selectedColumnsScrollPane = new JScrollPane(
            selectedColumnsList,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );
        columnsRightPanel.add(selectedColumnsScrollPane, BorderLayout.CENTER);

        JPanel bottomButtonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        columnsRightPanel.add(bottomButtonPanel, BorderLayout.PAGE_END);
        
        JButton moveSelectedUpButton = new JButton("\u2191");
        moveSelectedUpButton.setFont(ICON_FONT);
        moveSelectedUpButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                moveSelectedUp();
            }
            
        });
        bottomButtonPanel.add(moveSelectedUpButton);

        JButton moveSelectedDownButton = new JButton("\u2193");
        moveSelectedDownButton.setFont(ICON_FONT);
        moveSelectedDownButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                moveSelectedDown();
            }
            
        });
        bottomButtonPanel.add(moveSelectedDownButton);
        
        addRow(columnsPanel);
        
        addSeparator();
        
        fileChooserField.setDialogType(JFileChooser.SAVE_DIALOG);
        fileChooserField.setFileChooser(saveFileChooser);
        addRow(text("ui.swing.dialog.listspreadsheetexport.targetfile"), fileChooserField);

        openCheckBox.setText(text("ui.swing.dialog.listspreadsheetexport.open_now"));
        addRow(openCheckBox);
        
        pack();
    }

    @Override
    protected void load() {
        titleTextField.setText(board.getLabel());
        templateComboBox.setSelectedItem(Template.KRETA);
        loadTemplate(Template.KRETA);
    }
    
    private void loadTemplate(Template template) {
        extractPeriodsCheckBox.setSelected(template.extractPeriods());
        extractGroupsCheckBox.setSelected(template.extractGroups());
        
        availableColumnsListModel.clear();
        List<ColumnDefinition> templateColumnDefinitions = template.getColumnDefinitions();
        
        for (ColumnDefinition columnDefinition : ColumnDefinition.values()) {
            if (!templateColumnDefinitions.contains(columnDefinition)) {
                availableColumnsListModel.addElement(columnDefinition);
            }
        }
        
        selectedColumnsListModel.clear();
        for (ColumnDefinition columnDefinition : templateColumnDefinitions) {
            selectedColumnsListModel.addElement(columnDefinition);
        }
    }
    
    private void selectColumns() {
        for (ColumnDefinition columnDefinition : availableColumnsList.getSelectedValuesList()) {
            int selectedSize = selectedColumnsListModel.size();
            availableColumnsListModel.removeElement(columnDefinition);
            selectedColumnsListModel.addElement(columnDefinition);
            selectedColumnsList.getSelectionModel().addSelectionInterval(selectedSize, selectedSize);
        }
    }
    
    private void unselectColumns() {
        for (ColumnDefinition columnDefinition : selectedColumnsList.getSelectedValuesList()) {
            selectedColumnsListModel.removeElement(columnDefinition);
        }
        Set<ColumnDefinition> selectedColumns = new HashSet<ColumnDefinition>();
        int selectedCount = selectedColumnsListModel.size();
        for (int i = 0; i < selectedCount; i++) {
            ColumnDefinition columnDefinition = selectedColumnsListModel.getElementAt(i);
            selectedColumns.add(columnDefinition);
        }
        availableColumnsListModel.clear();
        for (ColumnDefinition columnDefinition : ColumnDefinition.values()) {
            if (!selectedColumns.contains(columnDefinition)) {
                availableColumnsListModel.addElement(columnDefinition);
            }
        }
    }

    private void moveSelectedUp() {
        int selectedIndex = selectedColumnsList.getSelectedIndex();
        if (selectedIndex <= 0) {
            return;
        }
        
        ColumnDefinition columnDefinition = selectedColumnsListModel.getElementAt(selectedIndex);
        selectedColumnsListModel.removeElementAt(selectedIndex);
        selectedColumnsListModel.add(selectedIndex - 1, columnDefinition);
        selectedColumnsList.setSelectedIndex(selectedIndex - 1);
    }

    private void moveSelectedDown() {
        int selectedIndex = selectedColumnsList.getSelectedIndex();
        if (selectedIndex < 0 || selectedIndex >= selectedColumnsListModel.size()) {
            return;
        }
        
        ColumnDefinition columnDefinition = selectedColumnsListModel.getElementAt(selectedIndex);
        selectedColumnsListModel.removeElementAt(selectedIndex);
        selectedColumnsListModel.add(selectedIndex + 1, columnDefinition);
        selectedColumnsList.setSelectedIndex(selectedIndex + 1);
    }
    
    @Override
    protected void afterLoad() {
    }

    @Override
    protected void save() {
    }

    @Override
    protected boolean trySave() {
        final GeneralWrapper<File> resultWrapper = new GeneralWrapper<File>();
        SwingWorkerProcessDialog processDialog = new SwingWorkerProcessDialog(parent, text("ui.swing.dialog.listspreadsheetexport.saving"), true) {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected Worker createWorker() {
                return new Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        resultWrapper.set(trySaveUnwrapped());
                    }

                    @Override
                    public void _rollBack() {
                    }

                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        if (command instanceof TerminatedCommand) {
                            closeDialog();
                        }
                    }
                    
                };
            }
            
        };
        processDialog.run();
        
        File choosenFile = resultWrapper.get();
        
        if (choosenFile == null) {
            return false;
        }

        if (openCheckBox.isSelected()) {
            try {
                Desktop.getDesktop().open(choosenFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return true;
    }

    protected File trySaveUnwrapped() {
        String defaultExtension = "ods";
        Set<String> allExtensions = formatMap.keySet();
        
        final File choosenFile;
        final String choosenExtension;
        if (fileChooserField.isEmpty()) {
            if (saveFileChooser.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
                return null;
            }
            choosenFile = saveFileChooser.getAutoSelectedFile();
            choosenExtension = saveFileChooser.getAutoSelectedExtension();
        } else {
            choosenFile = fileChooserField.getFile();
            String choosenFileExtension = FileUtil.getExtension(choosenFile);
            choosenExtension = allExtensions.contains(choosenFileExtension) ? choosenFileExtension : defaultExtension;
        }
        
        if (!checkFile(parent, choosenFile)) {
            return null;
        }
        
        Spreadsheet spreadsheet = buildSpreadsheet();
        
        SpreadsheetFormat choosenFormat = formatMap.get(choosenExtension);
        try {
            choosenFormat.dumper.dump(spreadsheet, choosenFile);
        } catch (IOException e) {
            OptionPaneUtil.showMessageDialog(
                parent,
                text("ui.swing.dialog.listspreadsheetexport.saveprocessdialog.error.message"),
                text("ui.swing.dialog.listspreadsheetexport.saveprocessdialog.error.title"),
                JOptionPane.ERROR_MESSAGE
            );
            return null;
        }
        
        return choosenFile;
    }

    private Spreadsheet buildSpreadsheet() {
        Spreadsheet spreadsheet = new Spreadsheet();
        
        Sheet sheet = spreadsheet.add(titleTextField.getText());
        int titleColumnIndex = 0;
        for (ColumnDefinition columnDefinition : getSelectedColumnDefinitions()) {
            Sheet.Cell cell = sheet.getCell(0, titleColumnIndex, new Sheet.Cell());
            cell.format.put("font-weight", "bold");
            cell.text = columnDefinition.getLabel();
            titleColumnIndex++;
        }
        
        int rowIndex = 1;
        
        for (ActivityEntry entry : getActivityEntries()) {
            int columnIndex = 0;
            for (ColumnDefinition columnDefinition : getSelectedColumnDefinitions()) {
                sheet.write(rowIndex, columnIndex, columnDefinition.formatEntry(entry));
                columnIndex++;
            }
            rowIndex++;
        }
        
        return spreadsheet;
    }
    
    private List<ActivityEntry> getActivityEntries() {
        List<ActivityEntry> result = new ArrayList<ActivityEntry>();
        ActivityFilter activityFilter = blockFilterPanel.getActivityFilter();
        Period filterPeriod = blockFilterPanel.getPeriod();
        for (Board.Entry boardEntry : board) {
            Block block = boardEntry.getBlock();
            Interval interval = boardEntry.getInterval();
            
            long day = interval.getStart().getPart(Time.PART.FULLDAYS);
            Block.ActivityManager activityManager = block.getActivityManager();
            ActivityList periodActivities =
                filterPeriod != null ?
                activityManager.getActivities(filterPeriod) :
                activityManager.getActivities()
            ;
            ActivityList filteredActivities = periodActivities.filter(activityFilter);
            for (Activity activity : filteredActivities) {
                PeriodSet activityPeriods = new PeriodSet();
                if (filterPeriod != null) {
                    activityPeriods.add(filterPeriod);
                } else {
                    activityPeriods.addAll(activityManager.getActivityPeriods(activity));
                }
                
                ResourceSubsetList groups =
                    activity.getResourceManager().getResourceSubsets(Resource.Type.CLASS)
                ;
                if (activityPeriods.isEmpty() || groups.isEmpty()) {
                    continue;
                }
                
                List<List<Period>> periodPortions = new ArrayList<List<Period>>();
                if (extractPeriodsCheckBox.isSelected()) {
                    for (Period activityPeriod : activityPeriods) {
                        periodPortions.add(new ArrayList<Period>(Arrays.asList(activityPeriod)));
                    }
                } else {
                    periodPortions.add(new ArrayList<Period>(activityPeriods));
                }
                
                List<ResourceSubsetList> groupPortions = new ArrayList<ResourceSubsetList>();
                if (extractGroupsCheckBox.isSelected()) {
                    for (ResourceSubset group : groups) {
                        groupPortions.add(new ResourceSubsetList(group));
                    }
                } else {
                    groupPortions.add(groups);
                }
                
                for (List<Period> periodPortion : periodPortions) {
                    for (ResourceSubsetList groupPortion : groupPortions) {
                        Period samplePeriod = periodPortion.get(0);
                        ResourceSubset sampleGroup = groupPortion.get(0);
                        Time startTime = interval.getStart();
                        int slot = calculateSlot(block, activity, sampleGroup, samplePeriod, startTime);
                        result.add(new ActivityEntry(
                            activity, periodPortion, groupPortion, interval, day, slot
                        ));
                    }
                }
            }
        }
        return result;
    }
    
    private int calculateSlot(Block block, Activity activity, ResourceSubset group, Period period, Time startTime) {
        TimingSet timingSet = getTimingSet(block, activity, group, period);
        SortedMap<Time, Integer> timeToSlotMap = getTimeToSlotMap(timingSet, block.getLength());
        Time dayStartTime = startTime.getDateOnly();
        Time dayEndTime = dayStartTime.getMoved(Time.DAY);
        SortedMap<Time, Integer> dayTimeToSlotMap = timeToSlotMap.subMap(dayStartTime, dayEndTime);
        if (dayTimeToSlotMap.isEmpty()) {
            return 0;
        }
        long minimumDiff = Long.MAX_VALUE;
        int slot = -1;
        for (Map.Entry<Time, Integer> entry: dayTimeToSlotMap.entrySet()) {
            Time slotTime = entry.getKey();
            slot = entry.getValue();
            long diff = Math.abs(slotTime.getSeconds() - startTime.getSeconds());
            if (diff < minimumDiff) {
                minimumDiff = diff;
            } else {
                slot--;
                break;
            }
        }
        return slot;
    }
    
    private TimingSet getTimingSet(Block block, Activity activity, ResourceSubset group, Period period) {
        TimingSet resourceTimingSet = getTimingSet(group.getResource(), period);
        if (resourceTimingSet != null) {
            return resourceTimingSet;
        }
        
        TimingSet activityTimingSet = getActivityLongestTimingSet(activity, period);
        if (activityTimingSet != null) {
            return activityTimingSet;
        }
        
        return getBlockLongestTimingSet(block, period);
    }

    private TimingSet getBlockLongestTimingSet(Block block, Period period) {
        TimingSet longestTimingSet = null;
        int maximumTimingSetLength = -1;
        for (Activity activity: block.getActivityManager().getActivities(period)) {
            TimingSet activityTimingSet = getActivityLongestTimingSet(activity, period);
            if (activityTimingSet != null) {
                int activityTimingSetLength = activityTimingSet.size();
                if (activityTimingSetLength > maximumTimingSetLength) {
                    longestTimingSet = activityTimingSet;
                    maximumTimingSetLength = activityTimingSetLength;
                }
            }
        }
        return longestTimingSet;
    }
    
    private TimingSet getActivityLongestTimingSet(Activity activity, Period period) {
        TimingSet longestTimingSet = null;
        int maximumTimingSetLength = -1;
        for (Resource resource: activity.getResourceManager().getResources()) {
            TimingSet resourceTimingSet = getTimingSet(resource, period);
            if (resourceTimingSet != null) {
                int resourceTimingSetLength = resourceTimingSet.size();
                if (resourceTimingSetLength > maximumTimingSetLength) {
                    longestTimingSet = resourceTimingSet;
                    maximumTimingSetLength = resourceTimingSetLength;
                }
            }
        }
        return longestTimingSet;
    }
    
    private TimingSet getTimingSet(Resource resource, Period period) {
        Resource.TimingSetManager timingSetManager = resource.getTimingSetManager();
        
        TimingSet periodTimingSet = timingSetManager.getPeriodTimingSet(period);
        if (periodTimingSet != null) {
            return periodTimingSet;
        }
        
        TimingSet defaultTimingSet = timingSetManager.getDefaultTimingSet();
        if (defaultTimingSet != null) {
            return defaultTimingSet;
        }
        
        return null;
    }
    
    private SortedMap<Time, Integer> getTimeToSlotMap(TimingSet timingSet, long length) {
        Pair<TimingSet, Long> key = new Pair<TimingSet, Long>(timingSet, length);
        SortedMap<Time, Integer> result = slotMap.get(key);
        if (result == null) {
            result = generateTimeToSlotMap(timingSet, length);
            slotMap.put(key, result);
        }
        return result;
    }

    private SortedMap<Time, Integer> generateTimeToSlotMap(TimingSet timingSet, long length) {
        SortedMap<Time, Integer> result = new TreeMap<Time, Integer>();
        for (Map.Entry<Time, TimingSet> entry: timingSet.split(Time.DAY).entrySet()) {
            TimingSet dayTimingSet = entry.getValue();
            int slotIndex = -1;
            Time lastSlotTime = null;
            for (TimingSet.TimeEntry timeEntry: dayTimingSet) {
                Time time = timeEntry.getTime();
                if (lastSlotTime == null || time.getSeconds() - lastSlotTime.getSeconds() >= length) {
                    slotIndex++;
                }
                result.put(time, slotIndex);
                lastSlotTime = time;
            }
        }
        return result;
    }
    
    private List<ColumnDefinition> getSelectedColumnDefinitions() {
        List<ColumnDefinition> result = new ArrayList<ColumnDefinition>();
        int count = selectedColumnsListModel.getSize();
        for (int i = 0; i < count; i++) {
            result.add(selectedColumnsListModel.getElementAt(i));
        }
        return result;
    }
    
    
    private class SpreadsheetFormat {

        final SpreadsheetDumper dumper;
        
        final FileFilter fileFilter;
        
        SpreadsheetFormat(String extension, String description, SpreadsheetDumper dumper) {
            this.dumper = dumper;
            this.fileFilter = new AutoExtensionFileChooser.ExtensionFilter(description, extension);
        }
        
    }
    
    
    private class ActivityEntry {
        
        final Activity activity;
        
        final List<Period> periods;
        
        final ResourceSubsetList groups;
        
        final Interval interval;
        
        final long day;
        
        final int slot;
        
        ActivityEntry(
            Activity activity, Collection<Period> periods, Collection<ResourceSubset> groups,
            Interval interval, long day, int slot
        ) {
            this.activity = activity;
            PeriodSet periodSet = new PeriodSet();
            periodSet.addAll(periods);
            this.periods = new ArrayList<Period>(periodSet);
            this.groups = new ResourceSubsetList(groups);
            this.interval = interval;
            this.day = day;
            this.slot = slot;
        }
        
    }
    
    

    private enum ColumnDefinition implements Labeled {

        LABEL {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.label");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return entry.activity.getLabel();
            }
            
        },

        PERIOD {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.period");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return join(entry.periods);
            }
            
        },

        DAY {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.day");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                StringBuilder resultBuilder = new StringBuilder();
                resultBuilder.append(Time.getDayName((int) (entry.day % 7)));
                if (entry.day >= 7) {
                    long weekNo = (entry.day / 7) + 1;
                    resultBuilder.append(" (" + weekNo + ")");
                }
                return resultBuilder.toString();
            }
            
        },
        
        DAYNO {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.dayno");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return "" + (entry.day >= 0 ? entry.day + 1 : entry.day);
            }
            
        },

        DAYINDEX {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.daynindex");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return "" + entry.day;
            }
            
        },
        
        SLOT {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.slot");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return "" + (entry.slot + 1);
            }
            
        },

        SLOTINDEX {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.slotindex");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return "" + entry.slot;
            }
            
        },
        
        STARTTIME {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.starttime");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return "" + entry.interval.getStart().toTimeString();
            }
            
        },

        ENDTIME {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.endtime");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return "" + entry.interval.getEnd().toTimeString();
            }
            
        },
        
        CLASS {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.class");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                StringBuilder resultBuilder = new StringBuilder();
                boolean first = true;
                for (ResourceSubset group : entry.groups) {
                    if (first) {
                        first = false;
                    } else {
                        resultBuilder.append(", ");
                    }
                    String label = getClassPart(group);
                    resultBuilder.append(label.replace(",", ""));
                }
                return resultBuilder.toString();
            }
            
        },

        GROUP {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.group");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                StringBuilder resultBuilder = new StringBuilder();
                boolean first = true;
                boolean more = entry.groups.size() > 1;
                for (ResourceSubset group : entry.groups) {
                    if (first) {
                        first = false;
                    } else {
                        resultBuilder.append(", ");
                    }
                    String label = getGroupPart(group);
                    if (label == null) {
                        label = more ? "-" : "";
                    }
                    resultBuilder.append(label.replace(",", ""));
                }
                return resultBuilder.toString();
            }
            
        },

        GROUPS {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.groups");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                StringBuilder resultBuilder = new StringBuilder();
                boolean first = true;
                for (ResourceSubset group : entry.groups) {
                    if (first) {
                        first = false;
                    } else {
                        resultBuilder.append(", ");
                    }
                    String label = getFullName(group);
                    resultBuilder.append(label.replace(",", ""));
                }
                return resultBuilder.toString();
            }
            
        },

        SUBJECT {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.subject");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return join(entry.activity.getTagManager().getTags(Tag.Type.SUBJECT));
            }
            
        },

        TEACHER {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.teacher");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return join(entry.activity.getResourceManager().getResources(Resource.Type.PERSON));
            }
            
        },

        ROOM {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.column.room");
            }
            
            @Override
            public String formatEntry(ActivityEntry entry) {
                return join(entry.activity.getResourceManager().getResources(Resource.Type.LOCALE));
            }
            
        };
        

        public abstract String formatEntry(ActivityEntry entry);
        
        @Override
        public String toString() {
            return getLabel();
        }
        
        protected String join(Collection<? extends Labeled> items) {
            StringBuilder resultBuilder = new StringBuilder();
            boolean first = true;
            for (Labeled item : items) {
                if (first) {
                    first = false;
                } else {
                    resultBuilder.append(", ");
                }
                resultBuilder.append(item.getLabel().replace(",", ""));
            }
            return resultBuilder.toString();
        }
        
        protected String getClassPart(ResourceSubset group) {
            return group.getResource().getLabel();
        }
        
        protected String getGroupPart(ResourceSubset group) {
            if (group.isWhole()) {
                return null;
            } else if (group.isEmpty()) {
                return "X";
            } else if (group instanceof ResourceSubset.SplittingPart) {
                ResourceSubset.SplittingPart splittingPartSubset = (ResourceSubset.SplittingPart) group;
                Resource.Splitting.Part splittingPart = splittingPartSubset.getSplittingPart();
                String splittingLabel = splittingPart.getSplitting().getLabel();
                String splittingPartLabel = splittingPart.getLabel();
                return splittingLabel + "-" + splittingPartLabel;
            } else {
                return "complex";
            }
        }
        
        protected String getFullName(ResourceSubset group) {
            StringBuilder resultBuilder = new StringBuilder();
            resultBuilder.append(getClassPart(group));
            String groupPart = getGroupPart(group);
            if (groupPart != null) {
                resultBuilder.append(" / ");
                resultBuilder.append(groupPart);
            }
            return resultBuilder.toString();
        }
        
    }
    
    
    private enum Template implements Labeled {
        
        KRETA {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.template.kreta");
            }
            
            @Override
            public boolean extractPeriods() {
                return true;
            }

            @Override
            public boolean extractGroups() {
                return true;
            }

            @Override
            public List<ColumnDefinition> getColumnDefinitions() {
                List<ColumnDefinition> result = new ArrayList<ColumnDefinition>();
                result.add(ColumnDefinition.PERIOD);
                result.add(ColumnDefinition.DAY);
                result.add(ColumnDefinition.SLOT);
                result.add(ColumnDefinition.CLASS);
                result.add(ColumnDefinition.GROUP);
                result.add(ColumnDefinition.SUBJECT);
                result.add(ColumnDefinition.TEACHER);
                result.add(ColumnDefinition.ROOM);
                return result;
            }
            
        },
        
        FULL {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.template.full");
            }
            
            @Override
            public boolean extractPeriods() {
                return false;
            }

            @Override
            public boolean extractGroups() {
                return false;
            }

            @Override
            public List<ColumnDefinition> getColumnDefinitions() {
                List<ColumnDefinition> result = new ArrayList<ColumnDefinition>();
                result.addAll(Arrays.asList(ColumnDefinition.values()));
                return result;
            }
            
        },
        
        EMPTY {

            @Override
            public String getLabel() {
                return text("ui.swing.dialog.listspreadsheetexport.template.empty");
            }
            
            @Override
            public boolean extractPeriods() {
                return false;
            }

            @Override
            public boolean extractGroups() {
                return false;
            }

            @Override
            public List<ColumnDefinition> getColumnDefinitions() {
                return new ArrayList<ColumnDefinition>();
            }
            
        };
        
        
        public abstract boolean extractPeriods();
        
        public abstract boolean extractGroups();
        
        public abstract List<ColumnDefinition> getColumnDefinitions();

        @Override
        public String toString() {
            return getLabel();
        }
        
    }
    
}

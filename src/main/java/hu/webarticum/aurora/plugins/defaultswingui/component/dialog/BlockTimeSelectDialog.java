package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualRadioButton;

public class BlockTimeSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Block block;
    
    private GeneralWrapper<Time> timeWrapper;
    
    private Time time;
    
    private JRadioButton timingSetTimeRadioButton;
    
    private JComboBox<Labeled.PairWrapper<Time>> timingSetComboBox;
    
    private JRadioButton otherTimeRadioButton;
    
    private JButton otherTimeButton;
    
    public BlockTimeSelectDialog(JFrame parent, Block block, GeneralWrapper<Time> timeWrapper) {
        super(parent);
        this.title = text("ui.swing.dialog.blocktimeselect.title");
        this.block = block;
        this.timeWrapper = timeWrapper;
        this.time = timeWrapper.get();
        init();
    }
    
    @Override
    protected void build() {
        ButtonGroup buttonGroup = new ButtonGroup();
        
        
        timingSetTimeRadioButton = new MultilingualRadioButton("ui.swing.dialog.blocktimeselect.from_timingset");
        buttonGroup.add(timingSetTimeRadioButton);
        
        timingSetComboBox = new JComboBox<Labeled.PairWrapper<Time>>();

        DefaultComboBoxModel<Labeled.PairWrapper<Time>> model = new DefaultComboBoxModel<Labeled.PairWrapper<Time>>();

        timingSetComboBox.setModel(model);
        
        if (block!=null) {
            for (TimingSet.TimeEntry timeEntry: block.getCalculatedTimingSet()) {
                model.addElement(new Labeled.PairWrapper<Time>(timeEntry.toReadableString(), timeEntry.getTime()));
            }
        }

        otherTimeRadioButton = new MultilingualRadioButton("ui.swing.dialog.blocktimeselect.custom_time");
        buttonGroup.add(otherTimeRadioButton);
        
        if (model.getSize()==0) {
            otherTimeRadioButton.setSelected(true);
        } else {
            timingSetTimeRadioButton.addActionListener(new ActionListener() {

                @SuppressWarnings("unchecked")
                @Override
                public void actionPerformed(ActionEvent ev) {
                    BlockTimeSelectDialog.this.time = ((Labeled.PairWrapper<Time>)timingSetComboBox.getSelectedItem()).get();
                    reloadTime();
                }
            });
            timingSetComboBox.addActionListener(new ActionListener() {
                
                @SuppressWarnings("unchecked")
                @Override
                public void actionPerformed(ActionEvent ev) {
                    timingSetTimeRadioButton.setSelected(true);
                    BlockTimeSelectDialog.this.time = ((Labeled.PairWrapper<Time>)timingSetComboBox.getSelectedItem()).get();
                    reloadTime();
                }
                
            });
            addRow(timingSetTimeRadioButton, timingSetComboBox);
        }

        
        otherTimeButton = new JButton("?");
        otherTimeButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                GeneralWrapper<Time> wrapper = new GeneralWrapper<Time>(BlockTimeSelectDialog.this.time);
                TimeEditDialog dialog = new TimeEditDialog(parent, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    BlockTimeSelectDialog.this.time = wrapper.get();
                    BlockTimeSelectDialog.this.reloadTime();
                }
            }
            
        });
        addRow(otherTimeRadioButton, otherTimeButton);
    }

    @Override
    protected void load() {
        reloadTime();
    }
    
    @Override
    protected void save() {
        timeWrapper.set(this.time);
    }

    private void reloadTime() {
        otherTimeRadioButton.setSelected(true);
        DefaultComboBoxModel<Labeled.PairWrapper<Time>> model = (DefaultComboBoxModel<Labeled.PairWrapper<Time>>)timingSetComboBox.getModel();
        int size = model.getSize();
        for (int i=0; i<size; i++) {
            if (model.getElementAt(i).get().equals(this.time)) {
                timingSetComboBox.setSelectedIndex(i);
                timingSetTimeRadioButton.setSelected(true);
                break;
            }
        }
        otherTimeButton.setText((this.time==null)?" ":this.time.toReadableString());
    }

}

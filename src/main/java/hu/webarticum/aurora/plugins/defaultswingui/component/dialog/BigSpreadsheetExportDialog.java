package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.FileUtil;
import hu.webarticum.aurora.app.util.TraditionalTable;
import hu.webarticum.aurora.app.util.common.StringUtil;
import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.MultiComparator;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.FileChooserField;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualComboBox;
import hu.webarticum.aurora.plugins.defaultswingui.util.AutoExtensionFileChooser;
import hu.webarticum.simplespreadsheetwriter.HssfApachePoiSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.HtmlSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.OdfToolkitSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.SerializeSpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.Sheet;
import hu.webarticum.simplespreadsheetwriter.Spreadsheet;
import hu.webarticum.simplespreadsheetwriter.SpreadsheetDumper;
import hu.webarticum.simplespreadsheetwriter.XssfApachePoiSpreadsheetDumper;


public class BigSpreadsheetExportDialog extends AbstractExportDialog {

    private static final long serialVersionUID = 1L;

    
    private Board board;

    private AutoExtensionFileChooser saveFileChooser = new AutoExtensionFileChooser();
    
    private FileChooserField fileChooserField = new FileChooserField();

    private JSpinner headerRepeatSpinner = new JSpinner();

    private JTextField tabLabelTextField = new JTextField();

    private JTextField titleTextField = new JTextField();

    private MultilingualComboBox<Resource.Type> typeSelectComboBox;
    
    private JCheckBox openCheckBox = new JCheckBox();
    
    private Map<String, SpreadsheetFormat> formatMap = new LinkedHashMap<String, SpreadsheetFormat>();
    
    
    public BigSpreadsheetExportDialog(JFrame parent, Board board) {
        super(parent);
        this.title = text("ui.swing.dialog.bigspreadsheetexport.title");
        this.board = board;
        this.formWidth = 500;
        init();
    }

    
    @Override
    protected void build() {
        {
            formatMap.put("ods", new SpreadsheetFormat("ods", text("ui.swing.dialog.bigspreadsheetexport.format.ods"), new OdfToolkitSpreadsheetDumper()));
            formatMap.put("xls", new SpreadsheetFormat("xls", text("ui.swing.dialog.bigspreadsheetexport.format.xls"), new HssfApachePoiSpreadsheetDumper()));
            formatMap.put("xlsx", new SpreadsheetFormat("xlsx", text("ui.swing.dialog.bigspreadsheetexport.format.xlsx"), new XssfApachePoiSpreadsheetDumper()));
            formatMap.put("html", new SpreadsheetFormat("html", text("ui.swing.dialog.bigspreadsheetexport.format.html"), new HtmlSpreadsheetDumper()));
            formatMap.put("obj", new SpreadsheetFormat("obj", text("ui.swing.dialog.bigspreadsheetexport.format.obj"), new SerializeSpreadsheetDumper()));
            
            saveFileChooser.setAppendExtension(true);
            saveFileChooser.setMultiSelectionEnabled(false);
            saveFileChooser.setAcceptAllFileFilterUsed(true);
            for (Map.Entry<String, SpreadsheetFormat> entry: formatMap.entrySet()) {
                saveFileChooser.addChoosableFileFilter(entry.getValue().fileFilter);
            }
        }
        
        typeSelectComboBox = new MultilingualComboBox<Resource.Type>();
        typeSelectComboBox.addMultilingualItem("core.resource.type.class", Resource.Type.CLASS);
        typeSelectComboBox.addMultilingualItem("core.resource.type.person", Resource.Type.PERSON);
        typeSelectComboBox.addMultilingualItem("core.resource.type.locale", Resource.Type.LOCALE);
        typeSelectComboBox.addMultilingualItem("core.resource.type.object", Resource.Type.OBJECT);
        typeSelectComboBox.addMultilingualItem("core.resource.type.other", Resource.Type.OTHER);
        
        addRow(text("ui.swing.dialog.bigspreadsheetexport.resource_type"), typeSelectComboBox);
        
        final NumberFormatter headerRepeatSpinnerFormatter = new NumberFormatter(){
            
            private static final long serialVersionUID = 1L;

            {
                setValueClass(Integer.class);
            }
            
            @Override
            public Object stringToValue(String text) throws ParseException {
                if (text.equals(text("ui.swing.dialog.bigspreadsheetexport.header_repeat.start_end"))) {
                    return (Integer)(-1);
                } else if (text.equals(text("ui.swing.dialog.bigspreadsheetexport.header_repeat.start_only"))) {
                    return (Integer)0;
                } else {
                    return super.stringToValue(text);
                }
            }
            
            @Override
            public String valueToString(Object value) throws ParseException {
                int number = ((Number)value).intValue();
                if (number==(-1)) {
                    return text("ui.swing.dialog.bigspreadsheetexport.header_repeat.start_end");
                } else if (number==0) {
                    return text("ui.swing.dialog.bigspreadsheetexport.header_repeat.start_only");
                } else {
                    return super.valueToString(value);
                }
            }
            
        };
        SpinnerModel headerRepeatSpinnerModel = new SpinnerNumberModel(5, -1, Integer.MAX_VALUE, 1);
        headerRepeatSpinner.setModel(headerRepeatSpinnerModel);
        DefaultEditor headerRepeatSpinnerEditor = new DefaultEditor(headerRepeatSpinner){
            
            private static final long serialVersionUID = 1L;

            {
                getTextField().setFormatterFactory(new DefaultFormatterFactory(headerRepeatSpinnerFormatter));
            }
            
        };
        headerRepeatSpinner.setEditor(headerRepeatSpinnerEditor);
        addRow(text("ui.swing.dialog.bigspreadsheetexport.header_repeat"), headerRepeatSpinner);
        
        addRow(text("ui.swing.dialog.bigspreadsheetexport.tabname"), tabLabelTextField);
        
        addRow(text("ui.swing.dialog.bigspreadsheetexport.sheettitle"), titleTextField);

        fileChooserField.setDialogType(JFileChooser.SAVE_DIALOG);
        fileChooserField.setFileChooser(saveFileChooser);
        addRow(text("ui.swing.dialog.bigspreadsheetexport.targetfile"), fileChooserField);

        openCheckBox.setText(text("ui.swing.dialog.bigspreadsheetexport.open_now"));
        addRow(openCheckBox);
        
        pack();
    }

    @Override
    protected void load() {
        tabLabelTextField.setText(board.getLabel());
        titleTextField.setText(board.getLabel());
    }

    @Override
    protected void afterLoad() {
    }

    @Override
    protected void save() {
    }

    @Override
    protected boolean trySave() {
        final GeneralWrapper<File> resultWrapper = new GeneralWrapper<File>();
        SwingWorkerProcessDialog processDialog = new SwingWorkerProcessDialog(parent, text("ui.swing.dialog.bigspreadsheetexport.saving"), true) {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected Worker createWorker() {
                return new Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        resultWrapper.set(trySaveUnwrapped());
                    }

                    @Override
                    public void _rollBack() {
                    }

                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        if (command instanceof TerminatedCommand) {
                            closeDialog();
                        }
                    }
                    
                };
            }
            
        };
        processDialog.run();
        
        File choosenFile = resultWrapper.get();
        
        if (choosenFile == null) {
            return false;
        }
        
        if (openCheckBox.isSelected()) {
            try {
                Desktop.getDesktop().open(choosenFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return true;
    }

    protected File trySaveUnwrapped() {
        String defaultExtension = "ods";
        Set<String> allExtensions = formatMap.keySet();
        
        final File choosenFile;
        final String choosenExtension;
        if (fileChooserField.isEmpty()) {
            if (saveFileChooser.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
                return null;
            }
            choosenFile = saveFileChooser.getAutoSelectedFile();
            choosenExtension = saveFileChooser.getAutoSelectedExtension();
        } else {
            choosenFile = fileChooserField.getFile();
            String choosenFileExtension = FileUtil.getExtension(choosenFile);
            choosenExtension = allExtensions.contains(choosenFileExtension) ? choosenFileExtension : defaultExtension;
        }
        
        if (!checkFile(parent, choosenFile)) {
            return null;
        }
        
        Spreadsheet spreadsheet = buildSpreadsheet();
        
        SpreadsheetFormat choosenFormat = formatMap.get(choosenExtension);
        try {
            choosenFormat.dumper.dump(spreadsheet, choosenFile);
        } catch (IOException e) {
            OptionPaneUtil.showMessageDialog(
                parent,
                text("ui.swing.dialog.bigspreadsheetexport.saveprocessdialog.error.message"),
                text("ui.swing.dialog.bigspreadsheetexport.saveprocessdialog.error.title"),
                JOptionPane.ERROR_MESSAGE
            );
            return null;
        }
        
        return choosenFile;
    }

    private Spreadsheet buildSpreadsheet() {
        SpreadsheetData data = getData();
        
        
        Spreadsheet spreadsheet = new Spreadsheet();
        
        Sheet sheet = spreadsheet.add(tabLabelTextField.getText());
        
        Sheet.Format headerFormat = new Sheet.Format(new String[]{
            "background-color", "#CCCCCC",
            "text-align", "center",
            "vertical-align", "middle",
            "font-weight", "bold",
        });

        Sheet.Format leftFormat = new Sheet.Format(new String[]{
            "text-align", "center",
            "vertical-align", "middle",
        });
        
        Sheet.Format itemFormat = new Sheet.Format(new String[]{
            "white-space", "pre",
            "text-align", "center",
            "vertical-align", "middle",
            "font-weight", "bold",
        });

        Sheet.Format delayArrowFormat = new Sheet.Format(new String[]{
            "white-space", "pre",
            "text-align", "center",
            "vertical-align", "middle",
            "color", "#777777",
        });
        
        
        
        
        // XXX
        int itemCount = data.itemDatas.size();
        int dayCount = data.daySizes.size();
        
        int headerRepeat = (Integer)headerRepeatSpinner.getValue();
        
        int daySepCellWidth = 3;
        int itemCellWidth = 10;

        int row = 0;
        
        row++;
        
        {
            int col = 0;
            
            sheet.getColumn(col, new Sheet.Column()).width = (-1);
            col++;
            
            sheet.getColumn(col, new Sheet.Column()).width = 30;
            col++;
            
            for (int day = 0; day < dayCount; day++) {
                String dayName = data.dayNames.get(day);
                int daySize = data.daySizes.get(day);
                
                sheet.getColumn(col, new Sheet.Column()).width = daySepCellWidth;
                col++;
                
                sheet.getRow(row, new Sheet.Row()).height = 10;
                sheet.merges.add(new Sheet.Range(row, col, row, col + (daySize * 2) + 3));
                sheet.write(row, col, dayName, new Sheet.Format(new String[]{
                    "font-size", "14pt",
                    "font-weight", "bold",
                    "text-align", "center",
                    "vertical-align", "middle",
                }));
                
                sheet.getColumn(col, new Sheet.Column()).width = itemCellWidth / 2;
                col++;
                sheet.getColumn(col, new Sheet.Column()).width = itemCellWidth - (itemCellWidth / 2);
                col++;
                
                for (int slot = 0; slot < daySize; slot++) {
                    sheet.getColumn(col, new Sheet.Column()).width = itemCellWidth / 2;
                    col++;
                    sheet.getColumn(col, new Sheet.Column()).width = itemCellWidth - (itemCellWidth / 2);
                    col++;
                }
                
                sheet.getColumn(col, new Sheet.Column()).width = itemCellWidth / 2;
                col++;
                sheet.getColumn(col, new Sheet.Column()).width = itemCellWidth - (itemCellWidth / 2);
                col++;
            }

            sheet.getColumn(col, new Sheet.Column()).width = 1;
            col++;
        }
        
        row++;
        
        for (int itemIndex = 0; itemIndex < itemCount; itemIndex++) {
            if (itemIndex % headerRepeat == 0) {
                int col = 0;

                col++;
                
                col++;
                
                for (int daySize: data.daySizes) {
                    sheet.write(row, col, "", headerFormat);
                    col++;
                    sheet.merges.add(new Sheet.Range(row, col, row, col + 1));
                    sheet.write(row, col, "", headerFormat);
                    col++;
                    col++;
                    for (int slot = 0; slot < daySize; slot++) {
                        sheet.merges.add(new Sheet.Range(row, col, row, col + 1));
                        sheet.write(row, col, (slot + 1) + ".", headerFormat);
                        col++;
                        col++;
                    }
                    sheet.merges.add(new Sheet.Range(row, col, row, col + 1));
                    sheet.write(row, col, "", headerFormat);
                    col++;
                    col++;
                }
                row++;
            }
            
            {
                int col = 0;
                sheet.write(row, col, data.itemNames.get(itemIndex), leftFormat);
                
                col++;
                
                sheet.write(row, col, "", leftFormat);
                
                col++;

                TreeMap<Long, TreeMap<Integer, SlotData>> itemData = data.itemDatas.get(itemIndex);
                
                for (long day = 0; day < dayCount; day++) {
                    int daySize = data.daySizes.get((int)day);
                    
                    TreeMap<Integer, SlotData> slotMap = (itemData == null) ? null : itemData.get(day);
                    
                    col++;

                    sheet.merges.add(new Sheet.Range(row, col, row, col + 1));
                    col++;
                    col++;
                    boolean isPreviousDelayed = false;
                    for (int slot = 0; slot < daySize; slot++) {
                        boolean isPresent = (slotMap != null && slotMap.containsKey(slot));
                        boolean isDelayed = (isPresent && slotMap.get(slot).isDelayed);
                        boolean isNextPresent = (slotMap != null && slotMap.containsKey(slot + 1));
                        boolean isNextDelayed = (isNextPresent && slotMap.get(slot + 1).isDelayed);
                        if (isDelayed) {
                            if (!isPreviousDelayed) {
                                sheet.write(row, col, "\u2192", delayArrowFormat);
                            }
                            col++;
                        }
                        boolean doMerge;
                        if (isPresent) {
                            doMerge = (!isDelayed || isNextDelayed || !isNextPresent);
                        } else {
                            doMerge = !isPreviousDelayed;
                        }
                        if (doMerge) {
                            sheet.merges.add(new Sheet.Range(row, col, row, col + 1));
                        }
                        if (isPresent) {
                            SlotData slotData = slotMap.get(slot);
                            sheet.write(row, col, slotData.content, itemFormat);
                        }
                        if (!isDelayed) {
                            col++;
                        }
                        col++;
                        isPreviousDelayed = isDelayed;
                    }
                    if (!isPreviousDelayed) {
                        sheet.merges.add(new Sheet.Range(row, col, row, col + 1));
                    }
                    col++;
                    col++;
                }
            }
            
            sheet.getRow(row, new Sheet.Row()).height = 10;
            row++;
        }

        int minCol = sheet.getMinColumnIndex();
        int maxCol = sheet.getMaxColumnIndex();

        sheet.merges.add(new Sheet.Range(0, minCol, 0, maxCol));
        sheet.getRow(0, new Sheet.Row()).height = 20;
        sheet.write(0, 0, titleTextField.getText(), new Sheet.Format(new String[]{
            "font-size", "24pt",
            "font-weight", "bold",
            "text-align", "center",
            "vertical-align", "middle",
        }));
        
        int minRow = sheet.getMinRowIndex();
        int maxRow = sheet.getMaxRowIndex();
        
        sheet.areas.add(new Sheet.Area(new int[]{
            minRow, minCol, maxRow, maxCol,
        }, new Sheet.Format(new String[]{
            "border-top", "0.5pt solid #999999",
            "border-left", "0.5pt solid #999999",
            "border-right", "0.5pt solid #999999",
            "border-bottom", "0.5pt solid #999999",
        })));

        sheet.areas.add(new Sheet.Area(new int[]{
            minRow, minCol, minRow, maxCol,
        }, new Sheet.Format("border-top", "2pt solid #333333")));

        sheet.areas.add(new Sheet.Area(new int[]{
            minRow, minCol, maxRow, minCol,
        }, new Sheet.Format("border-left", "2pt solid #333333")));

        sheet.areas.add(new Sheet.Area(new int[]{
            minRow, maxCol, maxRow, maxCol,
        }, new Sheet.Format("border-right", "2pt solid #333333")));

        sheet.areas.add(new Sheet.Area(new int[]{
            maxRow, minCol, maxRow, maxCol,
        }, new Sheet.Format("border-bottom", "2pt solid #333333")));
        
        return spreadsheet;
    }
    
    private SpreadsheetData getData() {
        SpreadsheetData data = new SpreadsheetData();

        String[] weekDayLabels = new String[]{
            text("core.day.monday"),
            text("core.day.tuesday"),
            text("core.day.wednesday"),
            text("core.day.thursday"),
            text("core.day.friday"),
            text("core.day.saturday"),
            text("core.day.sunday"),
        };
        
        Resource.Type resourceType = typeSelectComboBox.getSelectedData();
        List<Resource> resources = new ArrayList<Resource>(
            board.getBlocks().getActivities().getResources(resourceType)
        );
        Collections.sort(resources, new MultiComparator<Resource>(new Labeled.LabeledComparator()));
        
        int dayCount = (int)board.getEndTime().getPart(Time.PART.FULLDAYS) + 1;
        
        for (long day = 0; day < dayCount; day++) {
            int dayIndex = (int)(day % (Time.WEEK / Time.DAY));
            int weekIndex = (int)(day * Time.DAY / Time.WEEK);
            String dayLabel = weekDayLabels[dayIndex].toUpperCase();
            if (weekIndex != 0) {
                int weekNumber = weekIndex;
                if (weekNumber > 0) {
                    weekNumber++;
                }
                dayLabel += " (" + weekNumber + ")";
            }
            data.dayNames.add(dayLabel);
            data.daySizes.add(0);
        }

        TraditionalTable fullTraditionalTable = new TraditionalTable(board);
        
        for (Resource resource: resources) {
            Board resourceBoard = board.filter(new BlockFilter.ActivityBlockFilter(new ActivityFilter.HasResource(resource)));
            TraditionalTable resourceTraditionalTable = new TraditionalTable(resourceBoard);
            
            data.itemNames.add(resource.getLabel());
            
            TreeMap<Long, TreeMap<Integer, SlotData>> itemDatas = new TreeMap<Long, TreeMap<Integer, SlotData>>();
            data.itemDatas.add(itemDatas);
            
            for (long day = 0; day < dayCount; day++) {
                TreeMap<Integer, Board.EntryList> resourceDayBlocksMap = resourceTraditionalTable.get(day);
                if (resourceDayBlocksMap != null && !resourceDayBlocksMap.isEmpty()) {
                    int resourceDaySize = resourceDayBlocksMap.lastKey() + 1;
                    
                    TreeMap<Integer, SlotData> dayItemDatas = new TreeMap<Integer, SlotData>();
                    itemDatas.put(day, dayItemDatas);
                    
                    data.daySizes.set((int)day, Math.max(data.daySizes.get((int)day), resourceDaySize));
                    for (Map.Entry<Integer, Board.EntryList> entry: resourceDayBlocksMap.entrySet()) {
                        int slot = entry.getKey();
                        Board.EntryList entryList = entry.getValue();
                        BlockList blocks = entryList.getBlocks();
                        SlotData slotData = new SlotData();
                        slotData.content = composeSlotCellText(resource, blocks);
                        slotData.isDelayed = detectDelayed(day, slot, entryList, fullTraditionalTable);
                        dayItemDatas.put(slot, slotData);
                    }
                }
            }
        }
        
        return data;
    }
    
    private boolean detectDelayed(long day, int slot, Board.EntryList entryList, TraditionalTable fullTraditionalTable) {
        if (entryList.isEmpty()) {
            return false;
        }
        
        TreeMap<Integer, Board.EntryList> daySlots = fullTraditionalTable.get(day);
        if (daySlots == null) {
            return false;
        }
        
        Board.EntryList slotEntryList = daySlots.get(slot);
        if (slotEntryList == null) {
            return false;
        }
        slotEntryList.sort();

        Time sampleTime = getMostUsedTime(entryList);
        Time refTime = getMostUsedTime(slotEntryList);
        long diffSeconds = sampleTime.getSeconds() - refTime.getSeconds();
        return diffSeconds >= (15 * Time.MINUTE);
    }
    
    private Time getMostUsedTime(Board.EntryList entryList) {
        Map<Time, Integer> countMap = new HashMap<Time, Integer>();
        for (Board.Entry entry: entryList) {
            Time time = entry.getTime();
            int count = 0;
            if (countMap.containsKey(time)) {
                count = countMap.get(time);
            }
            countMap.put(time, count + 1);
        }
        List<Map.Entry<Time, Integer>> countEntries = new ArrayList<Map.Entry<Time, Integer>>(countMap.entrySet());
        Collections.sort(countEntries, new Comparator<Map.Entry<Time, Integer>>() {
            
            @Override
            public int compare(Entry<Time, Integer> entry1, Entry<Time, Integer> entry2) {
                int cmp = -entry1.getValue().compareTo(entry2.getValue());
                if (cmp != 0) {
                    return cmp;
                }
                return entry1.getKey().compareTo(entry2.getKey());
            }
            
        });
        return countEntries.get(0).getKey();
    }
    
    private String composeSlotCellText(Resource resource, BlockList blocks) {
        ActivityList activities = blocks.getActivities().filter(new ActivityFilter.HasResource(resource));
        
        List<String> rows = new ArrayList<String>();

        if (!resource.getType().equals(Resource.Type.CLASS)) {
            addClassesRow(activities, rows);
        }
        
        addSubjectsRow(activities, rows);
        
        if (rows.isEmpty()) {
            return "?";
        } else {
            return StringUtil.join(rows, "\n");
        }
    }

    private void addClassesRow(ActivityList activities, List<String> rows) {
        Set<Resource> classes = activities.getResources(Resource.Type.CLASS);
        if (classes.isEmpty()) {
            return;
        }
        
        List<String> classShortNames = new ArrayList<String>();
        for (Resource _class: classes) {
            String classShortName = _class.getAcronym();
            if (classShortName.isEmpty()) {
                classShortName = _class.getLabel();
            }
            classShortNames.add(classShortName);
        }
        Collections.sort(classShortNames);
        rows.add(StringUtil.join(classShortNames, ","));
    }

    private void addSubjectsRow(ActivityList activities, List<String> rows) {
        Set<Tag> subjects = activities.getTags(Tag.Type.SUBJECT);
        if (subjects.isEmpty()) {
            return;
        }
        
        List<String> subjectShortNames = new ArrayList<String>();
        for (Tag subject: subjects) {
            String subjectShortName = subject.getAcronym();
            if (subjectShortName.isEmpty()) {
                subjectShortName = subject.getLabel();
            }
            subjectShortNames.add(subjectShortName);
        }
        Collections.sort(subjectShortNames);
        rows.add(StringUtil.join(subjectShortNames, ","));
    }
    
    
    private static class SpreadsheetData {
        
        List<String> dayNames = new ArrayList<String>();
        
        List<Integer> daySizes = new ArrayList<Integer>();
        
        List<String> itemNames = new ArrayList<String>();
        
        List<TreeMap<Long, TreeMap<Integer, SlotData>>> itemDatas = new ArrayList<TreeMap<Long, TreeMap<Integer, SlotData>>>();
        
    }
    
    
    private static class SlotData {
        
        boolean isDelayed = false;
        
        String content = "";
        
    }
    
    
    private static class SpreadsheetFormat {

        final SpreadsheetDumper dumper;
        
        final FileFilter fileFilter;
        
        SpreadsheetFormat(String extension, String description, SpreadsheetDumper dumper) {
            this.dumper = dumper;
            this.fileFilter = new AutoExtensionFileChooser.ExtensionFilter(description, extension);
        }
        
    }
    
}

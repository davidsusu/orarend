package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import hu.webarticum.aurora.app.check.DefaultBoardProblemCollector;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualRadioButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;

public class CheckBoardDialog extends EditDialog {
    
    private static final long serialVersionUID = 1L;

    private Document document;

    private Board board;
    
    private JRadioButton boardAllRadioButton;
    
    private JRadioButton boardSpecifiedRadioButton;
    
    private JComboBox<Labeled.GenericWrapper<Board>> boardComboBox;

    public CheckBoardDialog(JFrame parent, Document document) {
        this(parent, document, null);
    }

    public CheckBoardDialog(JFrame parent, Document document, Board board) {
        super(parent);
        this.title = text("ui.swing.dialog.checkboard.title");
        this.board = board;
        this.document = document;
        init();
    }
    
    @Override
    protected ButtonPurpose getPurpose() {
        return ButtonPurpose.ACTION;
    }

    @Override
    protected void build() {
        ButtonGroup boardTypeButtonGroup = new ButtonGroup();
        
        JPanel boardAllPanel = new JPanel();
        boardAllPanel.setLayout(new BorderLayout());
        
        boardAllRadioButton = new MultilingualRadioButton("ui.swing.dialog.checkboard.check_all");
        boardAllRadioButton.setSelected(true);
        boardAllPanel.add(boardAllRadioButton, BorderLayout.CENTER);
        boardTypeButtonGroup.add(boardAllRadioButton);
        
        addRow(new MultilingualLabel("ui.swing.dialog.checkboard.board"), boardAllPanel);

        
        JPanel boardSpecifiedPanel = new JPanel();
        boardSpecifiedPanel.setLayout(new BorderLayout());
        
        boardSpecifiedRadioButton = new JRadioButton();
        boardSpecifiedPanel.add(boardSpecifiedRadioButton, BorderLayout.LINE_START);
        boardTypeButtonGroup.add(boardSpecifiedRadioButton);
        
        boardComboBox = new JComboBox<Labeled.GenericWrapper<Board>>();
        for (Board board: document.getBoardStore()) {
            boardComboBox.addItem(new Labeled.GenericWrapper<Board>(board));
        }
        boardComboBox.addItemListener(new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                boardSpecifiedRadioButton.setSelected(true);
            }
            
        });
        boardSpecifiedPanel.add(boardComboBox, BorderLayout.CENTER);
        addRow("", boardSpecifiedPanel);
    }

    @Override
    protected void load() {
        if (board==null) {
            boardAllRadioButton.setSelected(true);
        } else {
            boardSpecifiedRadioButton.setSelected(true);
            int itemCount = boardComboBox.getItemCount();
            for (int i=0; i<itemCount; i++) {
                Labeled.GenericWrapper<Board> item = boardComboBox.getItemAt(i);
                if (item.get()==board) {
                    boardComboBox.setSelectedItem(item);
                }
            }
        }
    }

    @Override
    protected void save() {
        final boolean all = boardAllRadioButton.isSelected();
        
        final List<Board> boards = new ArrayList<Board>();
        
        if (all) {
            boards.addAll(document.getBoardStore().getAll());
        } else {
            int index = boardComboBox.getSelectedIndex();
            if (index>=0) {
                Labeled.GenericWrapper<Board> item = boardComboBox.getItemAt(index);
                boards.add(item.get());
            }
        }

        new MessageCollectorRunner(parent, document, new DefaultBoardProblemCollector(boards)).run();
    }

}

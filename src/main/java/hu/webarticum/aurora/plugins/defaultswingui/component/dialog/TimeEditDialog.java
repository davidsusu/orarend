package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.time.Time;



public class TimeEditDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private GeneralWrapper<Time> timeWrapper;
    
    private JSpinner weekSpinner;

    private JComboBox<Labeled.PairWrapper<Time>> dayCombo;
    
    private JSpinner hourSpinner;
    
    private JSpinner minuteSpinner;
    
    private JSpinner secondSpinner;
    
    @SuppressWarnings("unchecked")
    private Labeled.PairWrapper<Time>[] dayLabeledWrappers = new Labeled.PairWrapper[]{
        new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.monday")), new Time(Time.MONDAY)),
        new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.tuesday")), new Time(Time.TUESDAY)),
        new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.wednesday")), new Time(Time.WEDNESDAY)),
        new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.thursday")), new Time(Time.THURSDAY)),
        new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.friday")), new Time(Time.FRIDAY)),
        new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.saturday")), new Time(Time.SATURDAY)),
        new Labeled.PairWrapper<Time>(new Labeled.StringLabeled(text("core.day.sunday")), new Time(Time.SUNDAY)),
    };
    
    public TimeEditDialog(JFrame parent, GeneralWrapper<Time> timeWrapper) {
        super(parent);
        this.title = text("ui.swing.dialog.timeedit.title");
        this.timeWrapper = timeWrapper;
        setResizable(false);
        init();
    }
    
    @Override
    protected void build() {
        JPanel topPanel = new JPanel();
        mainPanel.add(topPanel);
        
        weekSpinner = new JSpinner();
        SpinnerNumberModel weekSpinnerModel = new SpinnerNumberModel();
        weekSpinnerModel.setMinimum(1);
        weekSpinner.setModel(weekSpinnerModel);
        JSpinner.NumberEditor weekSpinnerEditor = new JSpinner.NumberEditor(weekSpinner, text("ui.swing.dialog.timeedit.week_format"));
        weekSpinner.setEditor(weekSpinnerEditor);
        topPanel.add(weekSpinner);
        
        dayCombo = new JComboBox<Labeled.PairWrapper<Time>>(dayLabeledWrappers);
        topPanel.add(dayCombo);

        JPanel bottomPanel = new JPanel();
        mainPanel.add(bottomPanel);
        
        hourSpinner = new JSpinner();
        SpinnerNumberModel hourSpinnerModel = new SpinnerNumberModel();
        hourSpinnerModel.setMinimum(0);
        hourSpinnerModel.setMaximum(23);
        hourSpinner.setModel(hourSpinnerModel);
        JSpinner.NumberEditor hourSpinnerEditor = new JSpinner.NumberEditor(hourSpinner, "00");
        hourSpinner.setEditor(hourSpinnerEditor);
        bottomPanel.add(hourSpinner);

        bottomPanel.add(new JLabel(":"));
        
        minuteSpinner = new JSpinner();
        SpinnerNumberModel minuteSpinnerModel = new SpinnerNumberModel();
        minuteSpinnerModel.setMinimum(0);
        minuteSpinnerModel.setMaximum(59);
        minuteSpinner.setModel(minuteSpinnerModel);
        JSpinner.NumberEditor minuteSpinnerEditor = new JSpinner.NumberEditor(minuteSpinner, "00");
        minuteSpinner.setEditor(minuteSpinnerEditor);
        bottomPanel.add(minuteSpinner);

        bottomPanel.add(new JLabel(":"));
        
        secondSpinner = new JSpinner();
        SpinnerNumberModel secondSpinnerModel = new SpinnerNumberModel();
        secondSpinnerModel.setMinimum(0);
        secondSpinnerModel.setMaximum(59);
        secondSpinner.setModel(secondSpinnerModel);
        JSpinner.NumberEditor sedondSpinnerEditor = new JSpinner.NumberEditor(secondSpinner, "00");
        secondSpinner.setEditor(sedondSpinnerEditor);
        bottomPanel.add(secondSpinner);
    }

    @Override
    protected void load() {
        Time time = timeWrapper.get();
        if (time==null) {
            time = new Time(0);
        }
        weekSpinner.getModel().setValue((Long)(time.getPart(Time.PART.WEEKS)+1l));
        dayCombo.setSelectedIndex((int)time.getPart(Time.PART.DAYS));
        hourSpinner.getModel().setValue((Long)time.getPart(Time.PART.HOURS));
        minuteSpinner.getModel().setValue((Long)time.getPart(Time.PART.MINUTES));
        secondSpinner.getModel().setValue((Long)time.getPart(Time.PART.SECONDS));
    }

    @Override
    protected void save() {
        long seconds = 0;
        seconds += (((Number)weekSpinner.getValue()).longValue()-1)*Time.WEEK;
        seconds += dayCombo.getSelectedIndex()*Time.DAY;
        seconds += ((Number)hourSpinner.getValue()).longValue()*Time.HOUR;
        seconds += ((Number)minuteSpinner.getValue()).longValue()*Time.MINUTE;
        seconds += ((Number)secondSpinner.getValue()).longValue();
        timeWrapper.set(new Time(seconds));
    }

}

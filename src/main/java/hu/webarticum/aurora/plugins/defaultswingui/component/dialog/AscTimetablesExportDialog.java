package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.FileChooserField;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualComboBox;
import hu.webarticum.aurora.plugins.defaultswingui.util.AutoExtensionFileChooser;
import hu.webarticum.aurora.x.ascxmlexport.AscXmlExport;


public class AscTimetablesExportDialog extends AbstractExportDialog {

    private static final long serialVersionUID = 1L;

    
    private Board board;
    
    private Document document;

    private AutoExtensionFileChooser fileChooser = new AutoExtensionFileChooser();

    private FileChooserField fileField = new FileChooserField();

    private JTextField encodingField = new JTextField();

    private MultilingualComboBox<Integer> dayNumberingComboBox = new MultilingualComboBox<Integer>();

    private MultilingualComboBox<Character> decimalSeparatorComboBox = new MultilingualComboBox<Character>();
    
    
    public AscTimetablesExportDialog(JFrame parent, Board board, Document document) {
        super(parent);
        this.title = text("ui.swing.dialog.asctimetablesexport.title");
        this.board = board;
        this.document = document;
        this.formWidth = 500;
        init();
    }

    
    @Override
    protected void build() {
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));

        AutoExtensionFileChooser.ExtensionFilter zipFilter = new AutoExtensionFileChooser.ExtensionFilter(true, "ui.swing.dialog.asctimetablesexport.file.chooser.options.xml", "xml");
        fileChooser.addChoosableFileFilter(zipFilter);
        fileChooser.setFileFilter(zipFilter);
        fileField.setFileChooser(fileChooser);
        fileField.setDialogType(JFileChooser.SAVE_DIALOG);
        addRow(text("ui.swing.dialog.asctimetablesexport.file"), fileField);

        addRow(text("ui.swing.dialog.asctimetablesexport.encoding"), encodingField);

        dayNumberingComboBox.addMultilingualItem("ui.swing.dialog.asctimetablesexport.daynumbering.from_0", 0);
        dayNumberingComboBox.addMultilingualItem("ui.swing.dialog.asctimetablesexport.daynumbering.from_1", 1);
        addRow(text("ui.swing.dialog.asctimetablesexport.daynumbering"), dayNumberingComboBox);
        
        decimalSeparatorComboBox.addMultilingualItem("ui.swing.dialog.asctimetablesexport.decimalseparator.dot", '.');
        decimalSeparatorComboBox.addMultilingualItem("ui.swing.dialog.asctimetablesexport.decimalseparator.comma", ',');
        addRow(text("ui.swing.dialog.asctimetablesexport.decimalseparator"), decimalSeparatorComboBox);
        
        pack();
    }

    @Override
    protected void load() {
        encodingField.setText("UTF-8");
        dayNumberingComboBox.setSelectedData(0);
        decimalSeparatorComboBox.setSelectedData('.');
    }

    @Override
    protected void afterLoad() {
    }

    @Override
    protected void save() {
    }

    @Override
    protected boolean trySave() {
        final GeneralWrapper<Boolean> resultWrapper = new GeneralWrapper<Boolean>();
        SwingWorkerProcessDialog processDialog = new SwingWorkerProcessDialog(parent, text("ui.swing.dialog.asctimetablesexport.saving"), true) {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected Worker createWorker() {
                return new Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        resultWrapper.set(trySaveUnwrapped());
                    }

                    @Override
                    public void _rollBack() {
                    }

                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        if (command instanceof TerminatedCommand) {
                            closeDialog();
                        }
                    }
                    
                };
            }
            
        };
        processDialog.run();
        return resultWrapper.get();
    }

    protected boolean trySaveUnwrapped() {
        File file;
        if (fileField.isEmpty()) {
            if (fileChooser.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
                return false;
            }
            file = fileChooser.getAutoSelectedFile();
        } else {
            file = fileField.getFile();
        }

        if (!checkFile(parent, file)) {
            return false;
        }
        
        
        
        
        {
            String encoding = encodingField.getText();
            if (encoding.isEmpty()) {
                encoding = "UTF-8";
            }
            // TODO
        }
        
        
        
        Map<String, Object> settings = new HashMap<String, Object>();
        settings.put("dayNumberingFrom1", dayNumberingComboBox.getSelectedData() == 1);
        settings.put("decimalSeparatorComma", decimalSeparatorComboBox.getSelectedData() == ',');
        settings.put("encoding", encodingField.getText());
        
        AscXmlExport ascXmlExport = new AscXmlExport(settings);
        try {
            ascXmlExport.export(document, board, file);
        } catch (IOException e1) {
            OptionPaneUtil.showMessageDialog(
                parent,
                text("ui.swing.dialog.asctimetablesexport.saveerror.message"),
                text("ui.swing.dialog.asctimetablesexport.saveerror.title"),
                JOptionPane.ERROR_MESSAGE
            );
            return false;
        }
        
        return true;
    }
    
}

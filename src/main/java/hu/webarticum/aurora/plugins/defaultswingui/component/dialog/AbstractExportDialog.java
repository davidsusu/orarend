package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Dimension;
import java.awt.Window;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;

public abstract class AbstractExportDialog extends EditDialog {

    private static final long serialVersionUID = 1L;
    

    public AbstractExportDialog(JFrame parent) {
        super(parent);
    }

    
    protected boolean checkFile(JFrame parent, File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.abstractexport.saveerror.isdirectory.message"),
                    text("ui.swing.dialog.abstractexport.saveerror.isdirectory.title"),
                    JOptionPane.ERROR_MESSAGE
                );
                return false;
            } else if (!file.canWrite()) {
                OptionPaneUtil.showMessageDialog(
                    parent,
                    text("ui.swing.dialog.abstractexport.saveerror.cannotwrite.message"),
                    text("ui.swing.dialog.abstractexport.saveerror.cannotwrite.title"),
                    JOptionPane.ERROR_MESSAGE
                );
                return false;
            } else {
                int answer = OptionPaneUtil.showConfirmDialog(
                    parent,
                    text("ui.swing.dialog.abstractexport.overwritefile.sure"),
                    ButtonPurpose.DANGER
                );
                if (answer != JOptionPane.YES_OPTION) {
                    return false;
                }
            }
        }
        return true;
    }

    protected JPanel createSubPanel() {
        JPanel subPanel = new JPanel();
        subPanel.setLayout(new BoxLayout(subPanel, BoxLayout.PAGE_AXIS));
        subPanel.setBorder(new EmptyBorder(0, 20, 10, 0));
        return subPanel;
    }

    protected JSpinner createIntegerSpinner(int minimum, int maximum) {
        JSpinner spinner = new JSpinner();
        spinner.setPreferredSize(new Dimension(50, 30));
        
        SpinnerModel model = new SpinnerNumberModel(minimum, minimum, maximum, 1);
        spinner.setModel(model);
        
        return spinner;
    }

    protected JSpinner createDecimalSpinner(BigDecimal minimum, BigDecimal maximum, int decimals) {
        JSpinner spinner = new JSpinner();
        spinner.setPreferredSize(new Dimension(50, 30));
        
        BigDecimal step = new BigDecimal(new BigInteger("1"), decimals);
        SpinnerModel model = new SpinnerNumberModel(minimum, minimum, maximum, step) {
            
            private static final long serialVersionUID = 1L;
            
            @Override
            public Object getNextValue() {
                BigDecimal currentValue = (BigDecimal)getValue();
                BigDecimal maximumValue = (BigDecimal)getMaximum();
                BigDecimal step = (BigDecimal)getStepSize();
                BigDecimal newValue = currentValue.add(step);
                return (currentValue.compareTo(maximumValue) < 0) ? newValue : maximumValue;
            }

            @Override
            public Object getPreviousValue() {
                BigDecimal currentValue = (BigDecimal)getValue();
                BigDecimal minimumValue = (BigDecimal)getMinimum();
                BigDecimal step = (BigDecimal)getStepSize();
                BigDecimal newValue = currentValue.subtract(step);
                return (currentValue.compareTo(minimumValue) > 0) ? newValue : minimumValue;
            }
            
        };
        
        spinner.setModel(model);
        
        return spinner;
    }

    protected JSpinner createTimeSpinner(int minimum, int maximum) {
        JSpinner spinner = new JSpinner();
        spinner.setPreferredSize(new Dimension(50, 30));
        
        SpinnerModel model = new SpinnerNumberModel(minimum, minimum, maximum, 1);
        spinner.setModel(model);

        JSpinner.NumberEditor editor = new JSpinner.NumberEditor(spinner, "00");
        spinner.setEditor(editor);
        
        return spinner;
    }
    
}

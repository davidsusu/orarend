package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class TagSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<Tag> tagWrapper;

    private Tag.Type defaultType;

    private boolean forceType;

    private JComboBox<Labeled.PairWrapper<Tag.Type>> typeSelectComboBox;
    
    private JComboBox<Labeled.Wrapper> tagSelectComboBox;
    
    public TagSelectDialog(JFrame parent, Document document, GeneralWrapper<Tag> tagWrapper) {
        this(parent, document, tagWrapper, Tag.Type.SUBJECT, false);
    }

    public TagSelectDialog(JFrame parent, Document document, GeneralWrapper<Tag> tagWrapper, Tag.Type forcedType) {
        this(parent, document, tagWrapper, forcedType, true);
    }
    
    public TagSelectDialog(JFrame parent, Document document, GeneralWrapper<Tag> tagWrapper, Tag.Type defaultType, boolean forceType) {
        super(parent);
        this.title = text("ui.swing.dialog.tagselect.title");
        this.document = document;
        this.tagWrapper = tagWrapper;
        this.defaultType = defaultType;
        this.forceType = forceType;
        setResizable(false);
        init();
    }
    
    @Override
    protected void build() {
        JPanel addPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 5));
        mainPanel.add(addPanel);
        
        JButton addButton = new MultilingualButton("ui.swing.dialog.tagselect.new_tag");
        addButton.setIcon(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                @SuppressWarnings("unchecked")
                Tag.Type type = ((Labeled.PairWrapper<Tag.Type>)typeSelectComboBox.getSelectedItem()).get();
                Tag tag = new Tag(type);
                TagEditDialog dialog = new TagEditDialog(parent, document, tag);
                dialog.run();
                if (dialog.getResult() == EditDialog.RESULT_OK) {
                    document.getTagStore().register(tag);
                    refreshTagList(tag.getType(), tag);
                }
            }
            
        });
        addPanel.add(addButton);
        
        typeSelectComboBox = new JComboBox<Labeled.PairWrapper<Tag.Type>>();
        typeSelectComboBox.addItem(new Labeled.PairWrapper<Tag.Type>(text("core.tag.type.subject"), Tag.Type.SUBJECT));
        typeSelectComboBox.addItem(new Labeled.PairWrapper<Tag.Type>(text("core.tag.type.language"), Tag.Type.LANGUAGE));
        typeSelectComboBox.addItem(new Labeled.PairWrapper<Tag.Type>(text("core.tag.type.other"), Tag.Type.OTHER));
        addRow(new MultilingualLabel("ui.swing.dialog.tagselect.type"), typeSelectComboBox);
        
        tagSelectComboBox = new JComboBox<Labeled.Wrapper>();
        addRow(new MultilingualLabel("ui.swing.dialog.tagselect.tag"), tagSelectComboBox);
    }

    @Override
    protected void load() {
        Tag tag = tagWrapper.get();
        Tag.Type type = defaultType;
        if (tag != null) {
            type = tag.getType();
        }
        
        int typeCount = typeSelectComboBox.getItemCount();
        for (int i = 0; i < typeCount; i++) {
            if (typeSelectComboBox.getItemAt(i).get().equals(type)) {
                typeSelectComboBox.setSelectedIndex(i);
                break;
            }
        }

        if (forceType) {
            typeSelectComboBox.setEnabled(false);
        }
        
        refreshTagList(type, tag);
    }
    
    @Override
    protected void afterLoad() {
        typeSelectComboBox.addItemListener(new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                @SuppressWarnings("unchecked")
                Tag.Type type = ((Labeled.PairWrapper<Tag.Type>)typeSelectComboBox.getSelectedItem()).get();
                refreshTagList(type, null);
            }
            
        });
    }

    @Override
    protected void save() {
        Tag tag = (Tag)((Labeled.Wrapper)tagSelectComboBox.getSelectedItem()).get();
        tagWrapper.set(tag);
    }
    
    private void refreshTagList(Tag.Type type, Tag selectedTag) {
        DefaultComboBoxModel<Labeled.Wrapper> model = new DefaultComboBoxModel<Labeled.Wrapper>();
        tagSelectComboBox.setModel(model);
        List<Tag> filteredResources = new ArrayList<Tag>();
        for (Tag tag : document.getTagStore()) {
            if (tag.getType().equals(type)) {
                filteredResources.add(tag);
            }
        }
        if (filteredResources.isEmpty()) {
            okButton.setEnabled(false);
        } else {
            okButton.setEnabled(true);
            int pos = 0;
            int selectedPos = -1;
            for (Tag tag : filteredResources) {
                model.addElement(new Labeled.Wrapper(tag));
                if (tag.equals(selectedTag)) {
                    selectedPos = pos;
                }
                pos++;
            }
            if (selectedPos == (-1)) {
                selectedPos = 0;
            }
            tagSelectComboBox.setSelectedIndex(selectedPos);
        }
    }

}

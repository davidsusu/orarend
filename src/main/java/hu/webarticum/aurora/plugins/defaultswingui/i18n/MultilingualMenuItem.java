package hu.webarticum.aurora.plugins.defaultswingui.i18n;

import javax.swing.Icon;
import javax.swing.JMenuItem;

public class MultilingualMenuItem extends JMenuItem implements MultilingualComponent {

    private static final long serialVersionUID = 1L;

    protected MultilingualContent labelContent;

    protected MultilingualContent tooltipContent;

    public MultilingualMenuItem() {
        setLanguagePath("");
    }

    public MultilingualMenuItem(String labelPath) {
        setLanguagePath(labelPath);
    }

    public MultilingualMenuItem(String labelPath, String tooltipPath) {
        setLanguagePath(labelPath, tooltipPath);
    }

    public MultilingualMenuItem(String labelPath, String tooltipPath, Icon icon) {
        setLanguagePath(labelPath, tooltipPath);
        setIcon(icon);
    }

    public MultilingualMenuItem(Icon icon, String tooltipPath) {
        setLanguagePath("", tooltipPath);
        setIcon(icon);
    }

    public void setLanguagePath(String labelPath) {
        setLanguagePath(labelPath, "");
    }
    
    public void setLanguagePath(String labelPath, String tooltipPath) {
        setLanguagePath(new PathMultilingualContent(labelPath), new PathMultilingualContent(tooltipPath));
        reloadLanguageTexts();
    }

    public void setLanguagePath(MultilingualContent labelContent) {
        setLanguagePath(labelContent, new PathMultilingualContent(""));
    }
    
    public void setLanguagePath(MultilingualContent labelContent, MultilingualContent tooltipContent) {
        this.labelContent = labelContent;
        this.tooltipContent = tooltipContent;
        reloadLanguageTexts();
    }
    
    @Override
    public void reloadLanguageTexts() {
        setText(labelContent.toString());
        setToolTipText(tooltipContent.toStringOrNull());
    }

}

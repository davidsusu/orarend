package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import hu.webarticum.aurora.app.check.Message;
import hu.webarticum.aurora.app.check.MessageCollector;
import hu.webarticum.aurora.app.check.SolutionHint;
import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;

public class MessageCollectorRunner {

    private final JFrame parent;
    
    private final Document document;
    
    private final MessageCollector messageCollector;
    
    private final boolean showEmptyReport;
    

    public MessageCollectorRunner(JFrame parent, Document document, MessageCollector messageCollector) {
        this(parent, document, messageCollector, true);
    }
    
    public MessageCollectorRunner(JFrame parent, Document document, MessageCollector messageCollector, boolean showEmptyReport) {
        this.parent = parent;
        this.document = document;
        this.messageCollector = messageCollector;
        this.showEmptyReport = showEmptyReport;
    }
    
    
    public List<Message> run() {
        final List<Message> messages = new ArrayList<Message>();
        final ReportDialog reportDialog = new ReportDialog(parent);

        final GeneralWrapper<Boolean> isFinishedWrapper = new GeneralWrapper<Boolean>(false);
        
        SwingWorkerProcessDialog processDialog = new SwingWorkerProcessDialog(parent, text("ui.swing.dialog.messagecollectorrunner.progress.title"), true) {

            private static final long serialVersionUID = 1L;
            
            @Override
            protected Worker createWorker() {
                return new Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        reportDialog.registerGroup(Message.Type.ERROR, text("ui.swing.dialog.messagecollectorrunner.errors"));
                        reportDialog.registerGroup(Message.Type.WARNING, text("ui.swing.dialog.messagecollectorrunner.warnings"));
                        reportDialog.registerGroup(Message.Type.NOTICE, text("ui.swing.dialog.messagecollectorrunner.notices"));
                        
                        messages.addAll(messageCollector.collect());
                        
                        for (Message message : messages) {
                            Runnable action = null;
                            
                            SolutionHint solutionHint = message.getSolutionHint();
                            if (solutionHint != null) {
                                SolutionHint.Type solutionHintType = solutionHint.getType();
                                if (solutionHintType == SolutionHint.BasicType.EDIT_BLOCK) {
                                    final Block block = (Block)solutionHint.getData();
                                    action = new Runnable() {
                                        
                                        @Override
                                        public void run() {
                                            new BlockEditDialog(parent, document, block).run();
                                        }
                                        
                                    };
                                }
                            }
                            
                            reportDialog.addItem(
                                message.getType(),
                                message.getTitle().toString(),
                                message.getDescription().toString(),
                                action
                            );
                        }
                        isFinishedWrapper.set(true);
                    }
                    
                    @Override
                    public void _rollBack() {
                        // no rollback is necessary
                    }

                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        if (command instanceof TerminatedCommand) {
                            closeDialog();
                        }
                    }
                    
                };
            }
        };
        processDialog.run();

        if (isFinishedWrapper.get() && (showEmptyReport || !messages.isEmpty())) {
            reportDialog.refresh();
            reportDialog.run();
        }
        
        return messages;
    }
    
}

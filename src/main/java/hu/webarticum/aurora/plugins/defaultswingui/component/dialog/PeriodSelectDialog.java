package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class PeriodSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<Period> periodWrapper;

    private JComboBox<Labeled.Wrapper> periodSelectComboBox;
    
    public PeriodSelectDialog(JFrame parent, Document document, GeneralWrapper<Period> periodWrapper) {
        super(parent);
        this.title = text("ui.swing.dialog.periodselect.title");
        this.document = document;
        this.periodWrapper = periodWrapper;
        setResizable(false);
        init();
    }
    
    @Override
    protected void build() {
        JPanel addPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 5));
        mainPanel.add(addPanel);
        
        JButton addButton = new MultilingualButton("ui.swing.dialog.periodselect.new_period");
        addButton.setIcon(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Period period = new Period();
                PeriodEditDialog dialog = new PeriodEditDialog(parent, document, period);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    document.getPeriodStore().register(period);
                    refreshPeriodList(period);
                }
            }
            
        });
        addPanel.add(addButton);
        
        periodSelectComboBox = new JComboBox<Labeled.Wrapper>();
        addRow(new MultilingualLabel("ui.swing.dialog.periodselect.period"), periodSelectComboBox);
    }

    @Override
    protected void load() {
        refreshPeriodList(periodWrapper.get());
    }

    @Override
    protected void save() {
        Period period = (Period)((Labeled.Wrapper)periodSelectComboBox.getSelectedItem()).get();
        periodWrapper.set(period);
    }
    
    private void refreshPeriodList(Period selectedPeriod) {
        DefaultComboBoxModel<Labeled.Wrapper> model = new DefaultComboBoxModel<Labeled.Wrapper>();
        periodSelectComboBox.setModel(model);
        Document.PeriodStore periodStore = document.getPeriodStore();
        if (periodStore.size()==0) {
            okButton.setEnabled(false);
        } else {
            okButton.setEnabled(true);
            int pos = 0;
            int selectedPos = -1;
            for (Period period: periodStore) {
                model.addElement(new Labeled.Wrapper(period));
                if (period.equals(selectedPeriod)) {
                    selectedPos = pos;
                }
                pos++;
            }
            if (selectedPos!=(-1)) {
                periodSelectComboBox.setSelectedIndex(selectedPos);
            }
        }
    }

}

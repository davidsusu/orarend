package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.app.util.Settings;
import hu.webarticum.aurora.app.util.email.Attachment;
import hu.webarticum.aurora.app.util.email.SimpleMessage;
import hu.webarticum.aurora.app.util.email.SimpleMessageSender;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.DocumentChangeListener;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.aurora.plugins.defaultswingui.util.RunnableActionListener;
import hu.webarticum.aurora.plugins.defaultswingui.util.RunnableDocumentChangeListener;

public class EmailSenderDialog extends EditDialog {

    private static final long serialVersionUID = 1L;
    
    private final RecipientsTableModel model;
    
    private final List<Attachment> sharedAttachments = new ArrayList<Attachment>();
    
    private JPanel headerPanel;
    
    private JButton emailSettingsButton;

    private JPanel sidePanel;
    
    private JTable recipientsTable;

    private JButton addRecipientButton;
    
    private JButton removeRecipientButton;
    
    private JPanel formPanel;
    
    private JTextField emailField;
    
    private JTextField nameField;
    
    private JTextField subjectField;
    
    private JTextArea contentTextArea;
    
    private AttachmentsButton attachmentsButton;
    
    private SimpleMessage currentMessage;
    
    public EmailSenderDialog(JFrame parent, SimpleMessage... messages) {
        this(parent, Arrays.asList(messages));
    }

    public EmailSenderDialog(JFrame parent, Collection<SimpleMessage> messages) {
        this(parent, messages, Collections.emptyList());
    }
    
    public EmailSenderDialog(JFrame parent, Collection<SimpleMessage> messages, Collection<Attachment> sharedAttachments) {
        super(parent);
        this.title = text("ui.swing.dialog.emailsender.title");
        this.model = new RecipientsTableModel(
            messages.isEmpty() ?
            Collections.singletonList(new SimpleMessage()):
            messages
        );
        this.sharedAttachments.addAll(sharedAttachments);
        init();
    }

    @Override
    protected ButtonPurpose getPurpose() {
        return ButtonPurpose.ACTION;
    }

    @Override
    protected void build() {
        JPanel outerPanel = new JPanel(new BorderLayout());
        mainPanel.add(outerPanel);
        
        headerPanel = new JPanel();
        outerPanel.add(headerPanel, BorderLayout.PAGE_START);

        sidePanel = new JPanel();
        outerPanel.add(sidePanel, BorderLayout.LINE_START);

        formPanel = new JPanel();
        outerPanel.add(formPanel, BorderLayout.CENTER);
        
        buildHeaderPanel();
        buildSidePanel();
        buildFormPanel();
        
    }

    private void buildHeaderPanel() {
        headerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        emailSettingsButton = new MultilingualButton("ui.swing.dialog.emailsender.email_settings");
        headerPanel.add(emailSettingsButton);
    }
    
    private void buildSidePanel() {
        sidePanel.setLayout(new BorderLayout());
        
        JPanel sideInnerPanel = new JPanel();
        sideInnerPanel.setLayout(new BoxLayout(sideInnerPanel, BoxLayout.PAGE_AXIS));
        sidePanel.add(sideInnerPanel, BorderLayout.PAGE_START);
        
        recipientsTable = new JTable();
        recipientsTable.setFillsViewportHeight(true);
        recipientsTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane recipientsTableScrollPane = new JScrollPane(recipientsTable);
        recipientsTableScrollPane.setPreferredSize(new Dimension(400, 400));
        sideInnerPanel.add(recipientsTableScrollPane);

        JPanel recipientsButtonBar = new JPanel();
        recipientsButtonBar.setLayout(new FlowLayout(FlowLayout.RIGHT));
        sideInnerPanel.add(recipientsButtonBar);
        
        addRecipientButton = new MultilingualButton(null, "ui.swing.dialog.emailsender.add_recipient", IconLoader.loadIcon("add"));
        recipientsButtonBar.add(addRecipientButton);

        removeRecipientButton = new MultilingualButton(null, "ui.swing.dialog.emailsender.remove_recipient", IconLoader.loadIcon("trash"));
        recipientsButtonBar.add(removeRecipientButton);
    }

    private void buildFormPanel() {
        formPanel.setLayout(new BorderLayout());
        formPanel.setBorder(new EmptyBorder(0, 10, 0, 0));
        
        JPanel formInnerPanel = new JPanel();
        formInnerPanel.setLayout(new BoxLayout(formInnerPanel, BoxLayout.PAGE_AXIS));
        formPanel.add(formInnerPanel, BorderLayout.PAGE_START);
        
        addRowTo(formInnerPanel, text("ui.swing.dialog.emailsender.form.email"), 400);
        emailField = new JTextField();
        addRowTo(formInnerPanel, emailField, 400, true);
        
        addRowTo(formInnerPanel, createFormGap(), 400);
        
        addRowTo(formInnerPanel, text("ui.swing.dialog.emailsender.form.name"), 400);
        nameField = new JTextField();
        addRowTo(formInnerPanel, nameField, 400, true);
        
        addRowTo(formInnerPanel, createFormGap(), 400);
        
        addRowTo(formInnerPanel, text("ui.swing.dialog.emailsender.form.subject"), 400);
        subjectField = new JTextField();
        addRowTo(formInnerPanel, subjectField, 400, true);
        
        addRowTo(formInnerPanel, createFormGap(), 400);
        
        addRowTo(formInnerPanel, text("ui.swing.dialog.emailsender.form.content"), 400);
        contentTextArea = new JTextArea("", 12, 20);
        contentTextArea.setLineWrap(true);
        JScrollPane contentScrollPane = new JScrollPane(contentTextArea);
        addRowTo(formInnerPanel, contentScrollPane, 400, true);

        addRowTo(formInnerPanel, createFormGap(), 400);

        JPanel attachmentsButtonPanel = new JPanel();
        attachmentsButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        addRowTo(formInnerPanel, attachmentsButtonPanel, 400);
        
        attachmentsButton = new AttachmentsButton();
        attachmentsButtonPanel.add(attachmentsButton);
    }
    
    private JComponent createFormGap() {
        JPanel gapPanel = new JPanel();
        gapPanel.setPreferredSize(new Dimension(7, 7));
        return gapPanel;
    }

    @Override
    protected void load() {
        recipientsTable.setModel(model);
        recipientsTable.getSelectionModel().setSelectionInterval(0, 0);
    }
    
    @Override
    protected void afterLoad() {
        super.afterLoad();

        emailSettingsButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                new SettingsDialog(parent, SettingsDialog.TAB.EMAIL).run();
            }
            
        });
        
        GeneralWrapper<Boolean> programmaticFill = new GeneralWrapper<Boolean>(false);
        
        recipientsTable.getColumnModel().getColumn(0).setWidth(130);
        recipientsTable.getColumnModel().getColumn(1).setWidth(130);
        recipientsTable.getSelectionModel().clearSelection();
        recipientsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            
            @Override
            public void valueChanged(ListSelectionEvent ev) {
                if (!ev.getValueIsAdjusting()) {
                    ListSelectionModel selectionModel = recipientsTable.getSelectionModel();
                    int selectionIndex = selectionModel.getMinSelectionIndex();
                    if (selectionIndex == -1) {
                        selectionModel.setSelectionInterval(0, 0);
                        return;
                    }
                    
                    programmaticFill.set(true);
                    
                    if (currentMessage != null) {
                        storeMessage(currentMessage);
                    }
                    
                    SimpleMessage selectedMessage = model.getMessage(selectionIndex);
                    loadMessage(selectedMessage);
                    currentMessage = selectedMessage;

                    programmaticFill.set(false);
                }
            }
            
        });
        recipientsTable.getSelectionModel().setSelectionInterval(0, 0);
        setMessageFocus();
        
        recipientsTable.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getClickCount() == 2) {
                    setMessageFocus();
                }
            }
            
        });
        
        KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
        recipientsTable.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(enter, "EditMessage");
        recipientsTable.getActionMap().put("EditMessage", new AbstractAction() {
            
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setMessageFocus();
            }
        });
        
        Runnable messageChangeAction = new Runnable() {
            
            @Override
            public void run() {
                if (!programmaticFill.get()) {
                    storeMessage(currentMessage);
                }
            }
            
        };
        DocumentChangeListener messageChangeListener = new RunnableDocumentChangeListener(messageChangeAction);
        emailField.getDocument().addDocumentListener(messageChangeListener);
        nameField.getDocument().addDocumentListener(messageChangeListener);
        subjectField.getDocument().addDocumentListener(messageChangeListener);
        contentTextArea.getDocument().addDocumentListener(messageChangeListener);
        attachmentsButton.addAttachmentsListener(new AttachmentsListener() {
            
            @Override
            public void attachmentsChanged() {
                messageChangeAction.run();
            }
            
        });

        Runnable addRecipientAction = new Runnable() {
            
            @Override
            public void run() {
                model.append(new SimpleMessage());
                int lastIndex = model.getRowCount() - 1;
                recipientsTable.getSelectionModel().setSelectionInterval(lastIndex, lastIndex);
                emailField.requestFocusInWindow();
            }
            
        };
        addRecipientButton.addActionListener(new RunnableActionListener(addRecipientAction));
        
        Runnable removeRecipientAction = new Runnable() {
            
            @Override
            public void run() {
                if (model.getRowCount() < 2) {
                    return;
                }
                if (
                    OptionPaneUtil.showConfirmDialog(
                        parent,
                        text("ui.swing.dialog.emailsender.remove_recipient.confirm.message"),
                        text("ui.swing.dialog.emailsender.remove_recipient.confirm.title"),
                        JOptionPane.QUESTION_MESSAGE,
                        ButtonPurpose.DANGER
                    ) != JOptionPane.YES_OPTION
                ) {
                    return;
                }
                ListSelectionModel selectionModel = recipientsTable.getSelectionModel();
                int selectionIndex = selectionModel.getMinSelectionIndex();
                model.remove(selectionIndex);
                int rowCount = model.getRowCount();
                if (rowCount > 0) {
                    int newSelectionIndex = Math.min(rowCount - 1, selectionIndex);
                    selectionModel.setSelectionInterval(newSelectionIndex, newSelectionIndex);
                }
            }
            
        };
        removeRecipientButton.addActionListener(new RunnableActionListener(removeRecipientAction));
        
        recipientsTable.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {
                if (ev.getKeyCode() == KeyEvent.VK_DELETE) {
                    removeRecipientAction.run();
                } else if (ev.getKeyCode() == KeyEvent.VK_INSERT) {
                    addRecipientAction.run();
                }
            }
            
        });
    }
    
    private void loadMessage(SimpleMessage message) {
        emailField.setText(message.getEmail());
        nameField.setText(message.getName());
        subjectField.setText(message.getSubject());
        contentTextArea.setText(message.getText());
        attachmentsButton.setAttachments(message.getAttachments());
    }

    private void storeMessage(SimpleMessage message) {
        message.setEmail(emailField.getText());
        message.setName(nameField.getText());
        message.setSubject(subjectField.getText());
        message.setText(contentTextArea.getText());
        message.setAttachments(attachmentsButton.getAttachments());
        model.refresh(message);
    }
    
    private void setMessageFocus() {
        if (emailField.getText().isEmpty()) {
            emailField.requestFocusInWindow();
        } else if (nameField.getText().isEmpty()) {
            nameField.requestFocusInWindow();
        } else if (subjectField.getText().isEmpty()) {
            subjectField.requestFocusInWindow();
        } else {
            contentTextArea.requestFocusInWindow();
            contentTextArea.setSelectionStart(0);
            contentTextArea.setSelectionEnd(contentTextArea.getText().length());
        }
    }

    @Override
    protected void opened() {
        super.opened();
        
        setMessageFocus();
    }
    
    @Override
    protected boolean trySave() {
        GeneralWrapper<Boolean> doOpenEmailSettings = new GeneralWrapper<Boolean>(false);
        GeneralWrapper<Boolean> doClose = new GeneralWrapper<Boolean>(false);
        
        SwingWorkerProcessDialog dialog = new SwingWorkerProcessDialog(
            parent,
            text("ui.swing.dialog.emailsender.process.title"),
            false
        ) {
            
            private static final long serialVersionUID = 1L;

            @Override
            protected Worker createWorker() {
                return new Worker() {
                    
                    @Override
                    public void _run() throws CancelledException {
                        publish(new SetProgressCommand(null));
                        
                        Settings settings = Application.instance().getSettings();
                        String fromEmail = settings.get("email_from_email");
                        String fromName = settings.get("email_from_name");
                        String host = settings.get("email_host");
                        boolean useSsl = settings.get("email_use_ssl").equals("1");
                        String portString = settings.get("email_port");
                        boolean useAuthentication = settings.get("email_use_auth").equals("1");
                        String user = settings.get("email_user");
                        String password = settings.get("email_password");
                        int port = -1;
                        if (portString.isEmpty()) {
                            port =
                                useSsl ?
                                SimpleMessageSender.DEFAULT_PORT_SSL :
                                SimpleMessageSender.DEFAULT_PORT
                            ;
                        } else {
                            try {
                                port = Integer.parseInt(portString);
                            } catch (NumberFormatException e) {
                            }
                        }
                        
                        if (
                            host.isEmpty() || fromEmail.isEmpty() || fromName.isEmpty() ||
                            port < 1 || (useAuthentication && (user.isEmpty() || password.isEmpty()))
                        ) {
                            publish(new SetLabelCommand(text("ui.swing.dialog.emailsender.process.error.insufficient_email_settings")));
                            publish(new LogCommand(text("ui.swing.dialog.emailsender.process.error.insufficient_email_settings")));
                            doOpenEmailSettings.set(true);
                            publish(new FailedCommand());
                            return;
                        }
                        
                        SimpleMessageSender messageSender = new SimpleMessageSender();
                        messageSender.setFromName(fromName);
                        messageSender.setFromEmail(fromEmail);
                        messageSender.setHost(host);
                        messageSender.setUseSsl(useSsl);
                        messageSender.setPort(port);
                        messageSender.setUseAuthentication(useAuthentication);
                        messageSender.setUser(user);
                        messageSender.setPassword(password);
                        
                        try {
                            messageSender.test();
                        } catch (IOException e) {
                            publish(new SetLabelCommand(text("ui.swing.dialog.emailsender.process.error.server_test_failed")));
                            publish(new LogCommand(text("ui.swing.dialog.emailsender.process.error.server_test_failed")));
                            doOpenEmailSettings.set(true);
                            publish(new FailedCommand());
                            return;
                        }
                        
                        publish(new SetLabelCommand(text("ui.swing.dialog.emailsender.process.sending_emails")));
                        
                        List<SimpleMessage> messages = model.getMessages();
                        int messageCount = messages.size();

                        publish(new SetProgressCommand(0d));
                        
                        int successCount = 0;
                        for (int i = 0; i < messageCount; i++) {
                            SimpleMessage message = messages.get(i);
                            boolean success;
                            try {
                                messageSender.send(message);
                                success = true;
                                successCount++;
                            } catch (IOException e) {
                                success = false;
                            }
                            publish(new SetProgressCommand((double)i / messageCount));
                            publish(new LogCommand(String.format(
                                text("ui.swing.dialog.emailsender.process.recipient." + (success ? "success" : "fail")),
                                message.getEmail()
                            )));
                        }

                        publish(new SetLabelCommand(text("ui.swing.dialog.emailsender.process.error.sending_finished")));
                        publish(new LogCommand(String.format(
                            text("ui.swing.dialog.emailsender.process.success_rate"), successCount, messageCount
                        )));
                        
                        doClose.set(true);
                        publish(new SuccessCommand());
                    }
                    
                    @Override
                    public void _rollBack() {
                    }

                    @Override
                    public void processCommand(Command command) {
                        super.processCommand(command);
                        // TODO
                    }
                    
                };
            }
            
        };
        dialog.run();
        
        if (doOpenEmailSettings.get()) {
            new SettingsDialog(parent, SettingsDialog.TAB.EMAIL).run();
        }
        
        return doClose.get();
    }

    @Override
    protected void save() { 
        // moved to trySave()
    }
    
    // TODO: sort by name?
    private class RecipientsTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 1L;

        private final String[] columnNames = new String[] {
            text("ui.swing.dialog.emailsender.table.email"),
            text("ui.swing.dialog.emailsender.table.name"),
            text("ui.swing.dialog.emailsender.table.subject"),
        };
        
        private final List<SimpleMessage> messages = new ArrayList<SimpleMessage>();

        RecipientsTableModel(Collection<SimpleMessage> messages) {
            this.messages.addAll(messages);
        }
        
        @Override
        public int getRowCount() {
            return messages.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return columnNames[columnIndex];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            SimpleMessage message = messages.get(rowIndex);
            if (columnIndex == 0) {
                return message.getEmail();
            } else if (columnIndex == 1) {
                return message.getName();
            } else if (columnIndex == 2) {
                return message.getSubject();
            } else {
                return null;
            }
        }

        @Override
        public void setValueAt(Object value, int rowIndex, int columnIndex) {
            SimpleMessage message = messages.get(rowIndex);
            String stringValue = value.toString();
            if (columnIndex == 0) {
                message.setEmail(stringValue);
            } else if (columnIndex == 1) {
                message.setName(stringValue);
            } else if (columnIndex == 2) {
                message.setSubject(stringValue);
            }
            fireTableCellUpdated(rowIndex, columnIndex);
        }

        void refresh(SimpleMessage message) {
            int messageCount = messages.size();
            for (int i = 0; i < messageCount; i++) { 
                if (messages.get(i) == message) {
                    refresh(i);
                    break;
                }
            }
        }
        
        void refresh(int rowIndex) {
            for (int i = 0; i < columnNames.length; i++) {
                fireTableCellUpdated(rowIndex, i);
            }
        }

        void remove(int rowIndex) {
            messages.remove(rowIndex);
            fireTableRowsDeleted(rowIndex, rowIndex);
        }

        void append(SimpleMessage message) {
            int nextIndex = messages.size();
            messages.add(message);
            fireTableRowsInserted(nextIndex, nextIndex);
        }
        
        SimpleMessage getMessage(int rowIndex) {
            return messages.get(rowIndex);
        }
        
        List<SimpleMessage> getMessages() {
            return new ArrayList<SimpleMessage>(messages);
        }
        
    }
    
    private class AttachmentsButton extends JButton {

        private static final long serialVersionUID = 1L;
        
        private List<Attachment> attachments = new ArrayList<Attachment>();
        
        private List<AttachmentsListener> listeners = new ArrayList<AttachmentsListener>();
        
        AttachmentsButton() {
            refreshLabel();
            addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    openDialog();
                }
                
            });
        }
        
        List<Attachment> getAttachments() {
            return new ArrayList<Attachment>(attachments);
        }
        
        void setAttachments(Collection<Attachment> attachments) {
            this.attachments.clear();
            this.attachments.addAll(attachments);
            refreshLabel();
        }
        
        void refreshLabel() {
            setText(String.format(text("ui.swing.dialog.emailsender.form.attachments"), attachments.size()));
        }
        
        void openDialog() {
            new EmailAttachmentsDialog(parent, attachments, sharedAttachments).run();
            refreshLabel();
            for (AttachmentsListener listener: listeners) {
                listener.attachmentsChanged();
            }
        }
        
        void addAttachmentsListener(AttachmentsListener listener) {
            listeners.add(listener);
        }
        
    }

    interface AttachmentsListener {
        
        abstract void attachmentsChanged();
        
    }
    
}
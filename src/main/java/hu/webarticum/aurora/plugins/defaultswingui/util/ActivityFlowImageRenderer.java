package hu.webarticum.aurora.plugins.defaultswingui.util;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import hu.webarticum.aurora.app.util.IntervalFlowAligner;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.ActivityFlow;
import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.ActivityFlow.Entry;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;

public class ActivityFlowImageRenderer {
    
    protected Settings settings = new Settings();
    
    public void setTopPad(int topPad) {
        settings.topPad = topPad;
    }

    public void setBottomPad(int bottomPad) {
        settings.bottomPad = bottomPad;
    }

    public void setLeftPad(int leftPad) {
        settings.leftPad = leftPad;
    }

    public void setRightPad(int rightPad) {
        settings.rightPad = rightPad;
    }

    public void setLeftLabelsWidth(int leftLabelsWidth) {
        settings.leftLabelsWidth = leftLabelsWidth;
    }

    public void setHeaderHeight(int headerHeight) {
        settings.headerHeight = headerHeight;
    }

    public void setHeaderBelowSpace(int headerBelowSpace) {
        settings.headerBelowSpace = headerBelowSpace;
    }

    public void setColumnsSpace(int columnsSpace) {
        settings.columnsSpace = columnsSpace;
    }

    public void setColumnTopPad(int columnTopPad) {
        settings.columnTopPad = columnTopPad;
    }

    public void setColumnBottomPad(int columnBottomPad) {
        settings.columnBottomPad = columnBottomPad;
    }

    public void setColumnLeftPad(int columnLeftPad) {
        settings.columnLeftPad = columnLeftPad;
    }

    public void setColumnRightPad(int columnRightPad) {
        settings.columnRightPad = columnRightPad;
    }

    public void setColumnMinimumInnerWidth(int columnMinimumInnerWidth) {
        settings.columnMinimumInnerWidth = columnMinimumInnerWidth;
    }

    public void setColumnMaximumInnerWidth(int columnMaximumInnerWidth) {
        settings.columnMaximumInnerWidth = columnMaximumInnerWidth;
    }

    public void setColumnActivityOptimalWidth(int columnActivityCompactWidth) {
        settings.columnActivityCompactWidth = columnActivityCompactWidth;
    }

    public void setColumnActivitiesSpace(int columnActivitiesSpace) {
        settings.columnActivitiesSpace = columnActivitiesSpace;
    }

    public void setActivityTopPad(int activityTopPad) {
        settings.activityTopPad = activityTopPad;
    }

    public void setActivityLeftPad(int activityLeftPad) {
        settings.activityLeftPad = activityLeftPad;
    }

    public void setLeftLabelAboveSpace(int leftLabelAboveSpace) {
        settings.leftLabelAboveSpace = leftLabelAboveSpace;
    }
    
    public void setSecondsPerPixel(int secondsPerPixel) {
        settings.secondsPerPixel = secondsPerPixel;
    }

    public void setAlignerStrategy(IntervalFlowAligner.AlignerStrategy alignerStrategy) {
        settings.alignerStrategy = alignerStrategy;
    }

    public void setHeaderFont(Font headerFont) {
        settings.headerFont = headerFont;
    }

    public void setLeftLabelFont(Font leftLabelFont) {
        settings.leftLabelFont = leftLabelFont;
    }

    public void setActivityLabelFont(Font activityLabelFont) {
        settings.activityLabelFont = activityLabelFont;
    }

    public void setRequiredFirstDay(long requiredFirstDay) {
        settings.requiredFirstDay = requiredFirstDay;
    }

    public void setRequiredLastDay(long requiredLastDay) {
        settings.requiredLastDay = requiredLastDay;
    }

    public void setRequiredStartInDay(int requiredStartInDay) {
        settings.requiredStartInDay = requiredStartInDay;
    }

    public void setRequiredEndInDay(int requiredEndInDay) {
        settings.requiredEndInDay = requiredEndInDay;
    }

    public void setColorAspect(Object colorAspect) {
        settings.colorAspect = colorAspect;
    }

    public void setColorScheme(ColorScheme colorScheme) {
        settings.colorScheme = colorScheme;
    }

    public Result render(ActivityFlow activityFlow) {
        
        // FIXME
        activityFlow = activityFlow.filter(new ActivityFlow.EntryFilter() {
            
            @Override
            public boolean validate(Entry entry) {
                return !(
                    entry.getActivity().getResourceManager().getResources().size() == 1 &&
                    entry.getActivity().getTagManager().getTags(Tag.Type.SUBJECT).isEmpty() &&
                    entry.getActivity().getLabel().contains("lyuk")
                );
            }
            
        });
        
        List<PositionedActivityEntry> positionedActivityEntries = new ArrayList<PositionedActivityEntry>();
        
        String[] weekDayLabels = new String[]{
            text("core.day.monday"),
            text("core.day.tuesday"),
            text("core.day.wednesday"),
            text("core.day.thursday"),
            text("core.day.friday"),
            text("core.day.saturday"),
            text("core.day.sunday"),
        };
        
        Time firstTime = activityFlow.getStartTime();
        Time lastTime = activityFlow.getLastTime();
        
        long firstDay = Math.min(settings.requiredFirstDay, firstTime.getPart(Time.PART.FULLDAYS) * Time.DAY);
        long lastDay = Math.max(settings.requiredLastDay, lastTime.getPart(Time.PART.FULLDAYS) * Time.DAY);

        int startInDay = settings.requiredStartInDay;
        int endInDay = settings.requiredEndInDay;
        
        for (long day = firstDay; day <= lastDay; day += Time.DAY) {
            Interval dayInterval = new Interval(day, day + Time.DAY);
            ActivityFlow dayActivityFlow = activityFlow.getLimited(dayInterval);
            if (!dayActivityFlow.isEmpty()) {
                int startInCurrentDay = (int)(dayActivityFlow.getStartTime().getSeconds() - day);
                if (startInCurrentDay < startInDay) {
                    startInDay = startInCurrentDay;
                }
                int endInCurrentDay = (int)(dayActivityFlow.getEndTime().getSeconds() - day);
                if (endInCurrentDay > endInDay) {
                    endInDay = endInCurrentDay;
                }
            }
        }

        int columnStart = settings.topPad + settings.headerHeight + settings.headerBelowSpace + settings.columnTopPad;
        
        int left = settings.leftPad + settings.leftLabelsWidth;
        TreeMap<Integer, String> activityStarts = new TreeMap<Integer, String>();
        TreeMap<Long, String> dayLabels = new TreeMap<Long, String>();
        TreeMap<Long, Integer> dayColumnLefts = new TreeMap<Long, Integer>();
        TreeMap<Long, Integer> dayColumnWidths = new TreeMap<Long, Integer>();
        for (long day = firstDay; day <= lastDay; day += Time.DAY) {
            int dayIndex = (int)((day % Time.WEEK) / Time.DAY);
            int weekIndex = (int)(day / Time.WEEK);
            String dayLabel = weekDayLabels[dayIndex].toUpperCase();
            if (weekIndex != 0) {
                int weekNumber = weekIndex;
                if (weekNumber > 0) {
                    weekNumber++;
                }
                dayLabel += " (" + weekNumber + ")";
            }
            dayLabels.put(day, dayLabel);
            dayColumnLefts.put(day, left);
            
            left += settings.columnLeftPad;
            
            Interval dayInterval = new Interval(day, day + Time.DAY);
            ActivityFlow dayActivityFlow = activityFlow.getLimited(dayInterval);
            
            List<IntervalFlowAligner.Item> items = new ArrayList<IntervalFlowAligner.Item>();
            Map<IntervalFlowAligner.Item, Activity> itemActivityMap = new HashMap<IntervalFlowAligner.Item, Activity>();
            
            for (ActivityFlow.Entry entry: dayActivityFlow) {
                Activity activity = entry.getActivity();
                Interval interval = entry.getInterval();
                long longActivityStart = interval.getStart().getSeconds();
                long longActivityEnd = interval.getEnd().getSeconds();
                IntervalFlowAligner.Item item = new IntervalFlowAligner.Item(longActivityStart, longActivityEnd);
                
                int activityStart = (int)(longActivityStart - day);
                String activityStartLabel = (new Time(activityStart)).toTimeString();
                activityStarts.put(activityStart, activityStartLabel);
                
                items.add(item);
                itemActivityMap.put(item, activity);
            }

            IntervalFlowAligner aligner = new IntervalFlowAligner();
            aligner.setStrategy(settings.alignerStrategy);
            aligner.setOptimalItemWidth(settings.columnActivityCompactWidth);
            aligner.setMinimumWidth(settings.columnMinimumInnerWidth);
            aligner.setMaximumWidth(settings.columnMaximumInnerWidth);
            aligner.setInnerSpace(settings.columnActivitiesSpace);
            IntervalFlowAligner.Result result = aligner.alignItems(items);

            dayColumnWidths.put(day, result.width + settings.columnLeftPad + settings.columnRightPad);
            
            for (Map.Entry<IntervalFlowAligner.Item, IntervalFlowAligner.Align> entry: result.itemMap.entrySet()) {
                IntervalFlowAligner.Item item = entry.getKey();
                IntervalFlowAligner.Align align = entry.getValue();
                Activity activity = itemActivityMap.get(item);
                
                Rectangle activityRectangle = new Rectangle(
                    left + align.left,
                    (((int)(item.start - day) - startInDay) / settings.secondsPerPixel) + columnStart,
                    align.width,
                    ((int)(item.end - item.start) / settings.secondsPerPixel)
                );
                
                PositionedActivityEntry positionedActivityEntry = new PositionedActivityEntry(activity, activityRectangle);
                positionedActivityEntries.add(positionedActivityEntry);
            }

            left += result.width + settings.columnRightPad + settings.columnsSpace;
        }
        
        int width = left - settings.columnsSpace + settings.rightPad;
        int columnTop = settings.topPad + settings.headerHeight + settings.headerBelowSpace;
        int activitiesTop = columnTop + settings.columnTopPad;
        int activitiesHeight = (endInDay - startInDay) / settings.secondsPerPixel;
        int columnHeight = settings.columnTopPad + activitiesHeight + settings.columnBottomPad;
        int height =  columnTop + columnHeight + settings.columnBottomPad + settings.bottomPad;
        
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D)image.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(settings.colorScheme.background);
        g.fillRect(0, 0, width, height);

        g.setFont(settings.headerFont);
        FontMetrics headerFontMetrics = g.getFontMetrics();
        int headerLabelHeight = headerFontMetrics.getHeight();
        for (long day = firstDay; day <= lastDay; day += Time.DAY) {
            String dayLabel = dayLabels.get(day);
            
            int columnLeft = dayColumnLefts.get(day);
            int columnWidth = dayColumnWidths.get(day);
            
            g.setColor(settings.colorScheme.headerBackground);
            g.fillRect(columnLeft, settings.topPad, columnWidth, settings.headerHeight);
            g.setColor(settings.colorScheme.headerBorder);
            g.drawRect(columnLeft, settings.topPad, columnWidth, settings.headerHeight);
            
            g.setColor(settings.colorScheme.headerLabel);
            int headerLabelWidth = headerFontMetrics.stringWidth(dayLabel);
            int headerLabelLeft = columnLeft + ((columnWidth - headerLabelWidth) / 2);
            g.drawChars(
                dayLabel.toCharArray(), 0, dayLabel.length(), headerLabelLeft,
                settings.topPad + ((settings.headerHeight + headerLabelHeight) / 2)
            );
            
            g.setColor(settings.colorScheme.columnBackground);
            g.fillRect(columnLeft, columnTop, columnWidth, columnHeight);
            g.setColor(settings.colorScheme.columnBorder);
            g.drawRect(columnLeft, columnTop, columnWidth, columnHeight);
        }
        
        g.setFont(settings.leftLabelFont);
        FontMetrics leftLabelFontMetrics = g.getFontMetrics();
        int leftLabelHeight = leftLabelFontMetrics.getHeight();
        for (Map.Entry<Integer, String> entry: activityStarts.entrySet()) {
            int activityStart = entry.getKey();
            String label = entry.getValue();
            int top = activitiesTop + ((int)(activityStart - startInDay) / settings.secondsPerPixel);
            
            g.setColor(settings.colorScheme.startLine);
            g.drawLine(settings.leftPad, top, width - settings.rightPad, top);
            
            g.setColor(settings.colorScheme.startLabel);
            g.drawChars(
                label.toCharArray(), 0, label.length(), settings.leftPad,
                top + leftLabelHeight + settings.leftLabelAboveSpace
            );
        }
        
        for (PositionedActivityEntry positionedActivityEntry: positionedActivityEntries) {
            Rectangle activityRectangle = positionedActivityEntry.rectangle;
            Activity activity = positionedActivityEntry.activity;
            String activityLabel = activity.getLabel();
            
            java.awt.Color activityLabelBackgroundColor = settings.colorScheme.activityLabelBackground;
            java.awt.Color activityLabelColor = settings.colorScheme.activityLabel;
            
            if (!settings.colorScheme.applyActivityColors || settings.colorAspect == null) {
                g.setColor(settings.colorScheme.activityBackground);
                g.fillRect(activityRectangle.x, activityRectangle.y, activityRectangle.width, activityRectangle.height);
            } else {
                final List<Color> colors = new ArrayList<Color>();
                if (settings.colorAspect instanceof Tag.Type) {
                    Set<Tag> tags = activity.getTagManager().getTags((Tag.Type)settings.colorAspect);
                    for (Tag tag : tags) {
                        colors.add(tag.getColor());
                    }
                } else if (settings.colorAspect instanceof Resource.Type) {
                    Set<Resource> resources = new HashSet<Resource>(
                       activity.getResourceManager().getResources((Resource.Type)settings.colorAspect)
                    );
                    for (Resource resource : resources) {
                        colors.add(resource.getColor());
                    }
                }
                if (colors.isEmpty()) {
                    colors.add(Color.WHITE);
                }
                if (colors.size()==1) {
                    Color color = colors.get(0);
                    Color labelColor = color.getBlackWhiteContrastColor();
                    activityLabelBackgroundColor = new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue());
                    activityLabelColor = new java.awt.Color(labelColor.getRed(), labelColor.getGreen(), labelColor.getBlue());
                    g.setColor(activityLabelBackgroundColor);
                    g.fillRect(activityRectangle.x, activityRectangle.y, activityRectangle.width, activityRectangle.height);
                } else {
                    Color avgColor = Color.avg(colors);
                    Color labelColor = avgColor.getBlackWhiteContrastColor();
                    activityLabelBackgroundColor = new java.awt.Color(avgColor.getRed(), avgColor.getGreen(), avgColor.getBlue());
                    activityLabelColor = new java.awt.Color(labelColor.getRed(), labelColor.getGreen(), labelColor.getBlue());
                    List<java.awt.Color> awtColors = new ArrayList<java.awt.Color>();
                    for (Color color: colors) {
                        awtColors.add(new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue()));
                    }
                    new StripsPatternDrawer().drawStripes(g, activityRectangle, awtColors, 20);
                }
            }
            
            g.setColor(settings.colorScheme.activityBorder);
            g.drawRect(
                activityRectangle.x,
                activityRectangle.y,
                activityRectangle.width,
                activityRectangle.height
            );
            
            int activityLabelLeft = activityRectangle.x + settings.activityLeftPad;
            int activityLabelTop = activityRectangle.y + settings.activityTopPad;

            g.setFont(settings.activityLabelFont);
            drawTextWithBackground(
                g, activityLabel, activityLabelLeft, activityLabelTop - 1,
                activityLabelBackgroundColor, activityLabelColor
            );

            g.setFont(settings.activityLabelFont.deriveFont(Font.ITALIC)); // FIXME
            List<Resource> rooms = activity.getResourceManager().getResources(Resource.Type.LOCALE);
            if (!rooms.isEmpty()) {
                Resource room = rooms.get(0);
                drawTextWithBackground(
                    g, String.format("%s", room.getLabel()), activityLabelLeft + 2, activityLabelTop + 18,
                    activityLabelBackgroundColor, activityLabelColor
                );
            }
        }
        
        g.dispose();

        Map<Time, Pair<Integer, Integer>> dayColumns = new HashMap<Time, Pair<Integer, Integer>>();
        for (Map.Entry<Long, Integer> entry: dayColumnLefts.entrySet()) {
            long daySeconds = entry.getKey();
            int columnLeft = entry.getValue();
            int columnWidth = dayColumnWidths.get(daySeconds);
            dayColumns.put(new Time(daySeconds), new Pair<Integer, Integer>(columnLeft, columnWidth));
        }
        
        Result result = new Result();
        result.startInDay = startInDay;
        result.dayColumnTop = columnStart;
        result.secondsPerPixel = settings.secondsPerPixel;
        result.dayColumns = dayColumns;
        result.positionedActivityEntries = positionedActivityEntries;
        result.image = image;
        
        return result;
    }
    
    private void drawTextWithBackground(
        Graphics2D g, String text, int left, int top,
        java.awt.Color backgroundColor, java.awt.Color color
    ) {
        FontMetrics activityLabelFontMetrics = g.getFontMetrics();
        int activityLabelHeight = activityLabelFontMetrics.getHeight();
        int activityLabelDescent = activityLabelFontMetrics.getDescent();
        
        int activityLabelWidth = activityLabelFontMetrics.stringWidth(text);
        
        g.setColor(backgroundColor);
        g.fillRect(
            left,
            top,
            activityLabelWidth,
            activityLabelHeight
        );
        
        g.setColor(color);
        g.drawChars(
            text.toCharArray(), 0, text.length(),
            left, top + activityLabelHeight - activityLabelDescent
        );
    }
    

    public class PositionedActivityEntry {
        
        public final Activity activity;

        public final Rectangle rectangle;
        
        PositionedActivityEntry(Activity activity, Rectangle rectangle) {
            this.activity = activity;
            this.rectangle = rectangle;
        }
        
    }
    
    public class Result {

        public int startInDay;
        
        public int dayColumnTop;
        
        public int secondsPerPixel;
        
        public Map<Time, Pair<Integer, Integer>> dayColumns;
        
        public List<PositionedActivityEntry> positionedActivityEntries;

        public BufferedImage image;
        
    }

    public class Settings {

        int topPad = 30;
        int bottomPad = 30;
        int leftPad = 30;
        int rightPad = 30;
        int leftLabelsWidth = 30;
        int headerHeight = 30;
        int headerBelowSpace = 2;
        int columnsSpace = 5;
        int columnTopPad = 7;
        int columnBottomPad = 7;
        int columnLeftPad = 7;
        int columnRightPad = 7;
        int columnMinimumInnerWidth = 110;
        int columnMaximumInnerWidth = 0;
        int columnActivityCompactWidth = 60;
        int columnActivitiesSpace = 3;
        int activityTopPad = 3;
        int activityLeftPad = 3;
        int leftLabelAboveSpace = 1;
        int secondsPerPixel = 90;
        IntervalFlowAligner.AlignerStrategy alignerStrategy = IntervalFlowAligner.STRATEGY.COMPLEX;
        Font headerFont = new Font(Font.SERIF, Font.BOLD, 15);
        Font leftLabelFont = new Font(Font.SANS_SERIF, 0, 9);
        Font activityLabelFont = new Font(Font.SANS_SERIF, 0, 12);
        long requiredFirstDay = 0;
        long requiredLastDay = Time.FRIDAY;
        int requiredStartInDay = (int)Time.HOUR * 7;
        int requiredEndInDay = (int)Time.HOUR * 15;
        Object colorAspect = null;
        ColorScheme colorScheme = new ColorScheme();

    }
    
    public static class ColorScheme {
        
        public boolean applyActivityColors = true;

        public java.awt.Color background = java.awt.Color.WHITE;
        
        public java.awt.Color headerBorder = java.awt.Color.BLACK;
        public java.awt.Color headerBackground = java.awt.Color.WHITE;
        public java.awt.Color headerLabel = java.awt.Color.BLACK;
        public java.awt.Color columnBorder = java.awt.Color.BLACK;
        public java.awt.Color columnBackground = java.awt.Color.WHITE;
        public java.awt.Color startLine = java.awt.Color.GRAY;
        public java.awt.Color startLabel = java.awt.Color.GRAY;
        
        public java.awt.Color activityBorder = java.awt.Color.BLACK;
        public java.awt.Color activityBackground = java.awt.Color.WHITE;
        public java.awt.Color activityLabel = java.awt.Color.BLACK;
        public java.awt.Color activityLabelBackground = java.awt.Color.WHITE;
        
    }
    
}

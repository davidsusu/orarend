package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;


// FIXME/TODO: ezt es az ilyeneket inkabb widgetkent?
// XXX: meretezes?
public class ReportDialog extends EditDialog {
    
    private static final long serialVersionUID = 1L;

    private JTabbedPane tabbar;

    private JPanel nomessagePanel;
    
    List<Object> groupOrder = new ArrayList<Object>();
    
    Map<Object, Group> groups = new HashMap<Object, Group>();
    
    public ReportDialog(JFrame parent) {
        super(parent);
        this.title = text("ui.swing.dialog.report.title");
        init();
    }
    
    @Override
    protected void build() {
        tabbar = new JTabbedPane();
        mainPanel.add(tabbar);
        nomessagePanel = new JPanel();
        nomessagePanel.setBorder(BorderFactory.createCompoundBorder(new LineBorder(new java.awt.Color(70, 50, 30)), new EmptyBorder(10, 10, 10, 10)));
        nomessagePanel.setBackground(new java.awt.Color(255, 240, 180));
        mainPanel.add(nomessagePanel);
        JLabel nomessageLabel = new MultilingualLabel("ui.swing.dialog.report.no_message");
        nomessagePanel.add(nomessageLabel);
        
        setSaveable(false);
    }

    // FIXME/TODO: styles?
    public void registerGroup(Object groupId, String label) {
        if (groups.containsKey(groupId)) {
            groups.get(groupId).setLabel(label);
        } else {
            groups.put(groupId, new Group(groupId, label));
            groupOrder.add(groupId);
        }
    }
    
    public Group requireGroup(Object groupId) {
        Group group;
        if (groups.containsKey(groupId)) {
            group = groups.get(groupId);
        } else {
            group = new Group(groupId, groupId.toString());
            groups.put(groupId, group);
            groupOrder.add(groupId);
        }
        return group;
    }

    public void addItem(Object groupId, String message, String description, Runnable callback) {
        Group group = requireGroup(groupId);
        group.getItems().add(new Item(message, description, callback));
    }

    public void addItem(Object groupId, String message, String description) {
        addItem(groupId, message, description, null);
    }

    public void addItem(Object groupId, String message) {
        addItem(groupId, message, message, null);
    }
    
    @Override
    protected void load() {
        boolean hasItems = false;
        for (Object groupId: groupOrder) {
            Group group = groups.get(groupId);
            
            List<Item> items = group.getItems();
            
            if (items.isEmpty()) {
                continue;
            }
            
            hasItems = true;
            
            Item[] itemsArray = new Item[items.size()];
            items.toArray(itemsArray);
            
            JPanel tabPanel = new JPanel();
            
            tabPanel.setLayout(new BorderLayout());
            
            tabbar.addTab(group.getLabel()+" ("+items.size()+")", tabPanel);
            
            final JList<Item> list = new JList<Item>(itemsArray);
            final Runnable openAction = new Runnable() {
                
                @Override
                public void run() {
                    ItemDialog dialog = new ItemDialog(parent, list.getSelectedValue());
                    dialog.run();
                }
                
            };
            list.addMouseListener(new MouseListener() {
                
                @Override
                public void mouseReleased(MouseEvent ev) {
                }
                
                @Override
                public void mousePressed(MouseEvent ev) {
                }
                
                @Override
                public void mouseExited(MouseEvent ev) {
                }
                
                @Override
                public void mouseEntered(MouseEvent ev) {
                }
                
                @Override
                public void mouseClicked(MouseEvent ev) {
                    if (ev.getClickCount()==2) {
                        openAction.run();
                    }
                }
                
            });
            list.addKeyListener(new KeyListener() {
                
                @Override
                public void keyTyped(KeyEvent ev) {
                }
                
                @Override
                public void keyReleased(KeyEvent ev) {
                }
                
                @Override
                public void keyPressed(KeyEvent ev) {
                    if (ev.getKeyCode()==KeyEvent.VK_ENTER) {
                        openAction.run();
                    }
                }
                
            });
            JScrollPane scrollPane = new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setPreferredSize(new Dimension(500, 300));
            tabPanel.add(scrollPane, BorderLayout.CENTER);
        }
        
        tabbar.setVisible(hasItems);
        nomessagePanel.setVisible(!hasItems);
    }

    @Override
    protected void save() {
    }

    protected void reset() {
        tabbar.removeAll();
    }

    public void refresh() {
        reset();
        load();
    }
    
    protected class Group {
        
        final Object name;
        
        final List<Item> items = new ArrayList<Item>();

        String label;
        
        public Group(Object name, String label) {
            this.name = name;
            this.label = label;
        }
        
        public Object getName() {
            return name;
        }
        
        public String getLabel() {
            return label;
        }
        
        public List<Item> getItems() {
            return items;
        }

        public void setLabel(String label) {
            this.label = label;
        }
        
    }
    
    protected class Item {
        
        protected final String message;
        
        protected final String description;
        
        protected final Runnable callback;
        
        public Item(String message, String description, Runnable callback) {
            this.message = message;
            this.description = description;
            this.callback = callback;
        }

        public String getMessage() {
            return message;
        }

        public String getDescription() {
            return description;
        }

        public Runnable getCallback() {
            return callback;
        }
        
        @Override
        public String toString() {
            return message;
        }
        
    }
    
    public class ItemDialog extends EditDialog {
        
        private final Item item;
        
        private JTextField messageField;
        
        private JTextArea descriptionArea;
        
        private JScrollPane descriptionScrollPane;
        
        private JButton handleButton;
        
        public ItemDialog(JFrame parent, Item item) {
            super(parent);
            this.title = text("ui.swing.dialog.report.item.handle");
            this.item = item;
            init();
        }

        private static final long serialVersionUID = 1L;

        @Override
        protected void build() {
            messageField = new JTextField();
            messageField.setEditable(false);
            mainPanel.add(messageField);
            descriptionArea = new JTextArea();
            descriptionArea.setEditable(false);
            descriptionArea.setLineWrap(true);
            descriptionArea.setWrapStyleWord(true);
            descriptionScrollPane = new JScrollPane(descriptionArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            descriptionScrollPane.setPreferredSize(new Dimension(700, 400));
            mainPanel.add(descriptionScrollPane);
            JPanel handleButtonPanel = new JPanel();
            handleButtonPanel.setLayout(new BorderLayout());
            handleButton = new MultilingualButton("ui.swing.dialog.report.item.handle");
            handleButton.setPreferredSize(new Dimension(120, 40));
            handleButton.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    Runnable callback = item.getCallback();
                    if (callback!=null) {
                        callback.run();
                        // FIXME/TODO: close details, rerun check, refresh background display
                    }
                }
                
            });
            handleButtonPanel.add(handleButton, BorderLayout.LINE_END);
            mainPanel.add(handleButtonPanel);
            
            setSaveable(false);
        }

        @Override
        protected void load() {
            messageField.setText(item.getMessage());
            descriptionArea.setText(item.getDescription());
            
            if (item.getCallback()==null) {
                handleButton.setVisible(false);
            } else {
                handleButton.setVisible(true);
            }

            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                
                @Override
                public void run() {
                    descriptionScrollPane.getVerticalScrollBar().setValue(0);
                }
                
            });
            
        }

        @Override
        protected void save() {
        }
        
    }
    
}

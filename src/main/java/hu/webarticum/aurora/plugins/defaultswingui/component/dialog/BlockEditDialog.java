package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;

import hu.webarticum.aurora.app.memento.BlockMemento;
import hu.webarticum.aurora.app.memento.BlockMemento.ActivityMemento;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.app.util.common.StringUtil;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.MultiComparator;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.ResourceSubset;
import hu.webarticum.aurora.core.model.ResourceSubsetList;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.DocumentChangeListener;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.chm.Command;

public class BlockEditDialog extends LabeledEditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private Block block;
    
    private JSpinner lengthMinutesSpinner;
    
    private JSpinner lengthSecondsSpinner;
    
    private JTabbedPane activitiesTabbedPane;
    
    private List<ActivityEntry> activityEntries = new ArrayList<ActivityEntry>();
    
    private Integer emptyTop = null;
    
    public BlockEditDialog(JFrame parent, Document document, Block block) {
        super(parent);
        this.title = text("ui.swing.dialog.blockedit.title");
        this.block = block;
        this.document = document;
        this.formWidth = 470;
        init();
    }
    
    @Override
    protected void opened() {
        if (block.getActivityManager().getActivities().isEmpty()) {
            emptyTop = (int)getLocation().getY();
        }
    }
    
    @Override
    protected void build() {
        labelField = new JTextField();
        labelField.setMaximumSize(new Dimension(700, 25));
        addRow(new MultilingualLabel("ui.swing.dialog.blockedit.label"), labelField);

        JPanel lengthPanel = new JPanel();
        lengthPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 2));
        lengthMinutesSpinner = new JSpinner();
        lengthMinutesSpinner.setToolTipText(text("ui.swing.dialog.blockedit.length.minutes"));
        lengthMinutesSpinner.setPreferredSize(new Dimension(100, 25));
        lengthMinutesSpinner.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        lengthPanel.add(lengthMinutesSpinner);
        lengthPanel.add(new JLabel(":"));
        lengthSecondsSpinner = new JSpinner();
        lengthSecondsSpinner.setPreferredSize(new Dimension(70, 25));
        lengthSecondsSpinner.setToolTipText(text("ui.swing.dialog.blockedit.length.seconds"));
        lengthSecondsSpinner.setModel(new SpinnerNumberModel(0, 0, 59, 1));
        lengthPanel.add(lengthSecondsSpinner);
        addRow(new MultilingualLabel("ui.swing.dialog.blockedit.length"), lengthPanel);
        

        JPanel activitiesPanel = new JPanel();
        activitiesPanel.setLayout(new BoxLayout(activitiesPanel, BoxLayout.PAGE_AXIS));
        activitiesPanel.setBackground(new java.awt.Color(0xF7F5EE));
        activitiesPanel.setBorder(new CompoundBorder(
            new LineBorder(java.awt.Color.GRAY),
            new EmptyBorder(7, 7, 7, 7)
        ));
        mainPanel.add(activitiesPanel);
        
        JPanel activitiesHeaderPanel = new JPanel();
        activitiesHeaderPanel.setOpaque(false);
        activitiesHeaderPanel.setLayout(new BorderLayout());
        JLabel activitiesHeaderLabel = new MultilingualLabel("ui.swing.dialog.blockedit.activities");
        activitiesHeaderLabel.setFont(activitiesHeaderLabel.getFont().deriveFont(Font.BOLD));
        activitiesHeaderPanel.add(activitiesHeaderLabel, BorderLayout.LINE_START);
        JButton addActivityButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.dialog.blockedit.activities.add_activity");
        addActivityButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                modified = true;
                boolean isNew = activityEntries.isEmpty();
                Activity activity = new Activity(text("ui.swing.dialog.blockedit.activities.new_activity_label"));
                String blockLabel = labelField.getText();
                if (activityEntries.isEmpty() && !blockLabel.isEmpty()) {
                    activity.setLabel(blockLabel);
                    Activity.TagManager tagManager = activity.getTagManager();
                    for (Tag tag: document.getTagStore()) {
                        if (tag.getType()==Tag.Type.SUBJECT) {
                            if (containsNormalized(blockLabel, tag.getLabel())) {
                                tagManager.add(tag);
                            }
                        }
                    }
                    Activity.ResourceManager resourceManager = activity.getResourceManager();
                    for (Resource resource: document.getResourceStore()) {
                        if (containsNormalized(blockLabel, resource.getLabel())) {
                            resourceManager.add(resource);
                        }
                    }
                }
                Point oldLocation = BlockEditDialog.this.getLocation();
                int oldHeight = BlockEditDialog.this.getHeight();
                loadBlockActivityEntry(activity, document.getPeriodStore().getAll(), true);
                if (isNew) {
                    overruleDialogSize();
                    int newHeight = BlockEditDialog.this.getHeight();
                    int newY = (int)(oldLocation.getY()+((oldHeight-newHeight)/2));
                    BlockEditDialog.this.setLocation((int)oldLocation.getX(), newY);
                }
            }
            
            private boolean containsNormalized(String context, String subject) {
                String normalizedContext = StringUtil.normalize(context);
                String normalizedSubject = StringUtil.normalize(subject);
                return normalizedContext.matches(".*\\b"+Pattern.quote(normalizedSubject)+"\\b.*");
            }
            
        });
        activitiesHeaderPanel.add(addActivityButton, BorderLayout.LINE_END);
        activitiesPanel.add(activitiesHeaderPanel);
        
        activitiesTabbedPane = new JTabbedPane();
        activitiesPanel.add(activitiesTabbedPane);

        JButton timingSetButton = new MultilingualButton("ui.swing.dialog.blockedit.runtime_timingset.show");
        timingSetButton.setMaximumSize(new Dimension(700, 25));
        timingSetButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Block editedBlock = new Block();
                createBlockMemento().apply(editedBlock, false);
                TimingSet timingSet = editedBlock.getCalculatedTimingSet();
                TimingSetEditDialog dialog = new TimingSetEditDialog(parent, document, timingSet, block, false);
                dialog.run();
            }
            
        });
        addRowTo(mainPanel, new MultilingualLabel("ui.swing.dialog.blockedit.runtime_timingset"), timingSetButton, formWidth, 200);
    }

    @Override
    protected void load() {
        labelField.setText(block.getLabel());
        
        int length = (int)block.getLength();
        lengthMinutesSpinner.setValue(length/60);
        lengthSecondsSpinner.setValue(length%60);
        
        for (Block.ActivityManager.ActivityEntry blockActivityEntry: block.getActivityManager()) {
            loadBlockActivityEntry(blockActivityEntry.getActivity(), blockActivityEntry.getPeriods(), false);
        }
    }

    @Override
    protected void afterLoad() {
        super.afterLoad();

        bindModifyListener(lengthMinutesSpinner);
        bindModifyListener(lengthSecondsSpinner);
    }
    
    @Override
    protected boolean canSave() {
        if (!super.canSave()) {
            return false;
        }

        for (ActivityEntry activityEntry: activityEntries) {
            if (activityEntry.label.isEmpty()) {
                OptionPaneUtil.showMessageDialog(parent, text("ui.swing.dialog.blockedit.activities.specify_labels"));
                return false;
            }
        }
        
        return true;
    }

    @Override
    protected void save() {
        if (!modified) {
            return;
        }
        
        BlockMemento newMemento = createBlockMemento();
        
        Command command;
        Document.BlockStore blockStore = document.getBlockStore();
        
        if (blockStore.contains(block)) {
            BlockMemento oldMemento = new BlockMemento(block);
            command = new StoreItemEditCommand<Block>(
                "ui.swing.dialog.blockedit.command.edit", // TODO
                blockStore, block, oldMemento, newMemento
            );
        } else {
            newMemento.apply(block);
            command = new StoreItemRegisterCommand<Block>(
                "ui.swing.dialog.blockedit.command.register", // TODO
                blockStore, block
            );
        }
        
        history().addAndExecute(command);
    }

    private BlockMemento createBlockMemento() {
        long length = ((Integer)lengthMinutesSpinner.getValue() * 60) + ((Integer)lengthSecondsSpinner.getValue());
        
        Map<Activity, Pair<ActivityMemento, ? extends Collection<Period>>> activityDataMap = new HashMap<Activity, Pair<ActivityMemento, ? extends Collection<Period>>>();
        for (ActivityEntry activityEntry: activityEntries) {
            Activity activity = activityEntry.activity;
            ActivityMemento activityMemento = activityEntry.createActivityMemento();
            PeriodSet periods = activityEntry.periods;
            activityDataMap.put(activity, new Pair<BlockMemento.ActivityMemento, Collection<Period>>(activityMemento, periods));
        }
        
        return new BlockMemento(labelField.getText(), length, new BlockMemento.ActivityManagerMemento(activityDataMap));
    }
    
    private void loadBlockActivityEntry(final Activity activity, Collection<Period> periods, boolean focus) {
        int nextIndex = activityEntries.size();
        
        final ActivityEntry activityEntry = new ActivityEntry(activity, periods);
        activityEntries.add(activityEntry);
        JPanel activityEntryTabPanel = new JPanel();
        activityEntryTabPanel.setLayout(new BoxLayout(activityEntryTabPanel, BoxLayout.PAGE_AXIS));
        activityEntryTabPanel.setBackground(new java.awt.Color(0xFFFCF9));
        activityEntryTabPanel.setBorder(new CompoundBorder(
            new LineBorder(java.awt.Color.GRAY),
            new EmptyBorder(3, 3, 3, 3)
        ));
        activitiesTabbedPane.addTab(cutTabLabel(activityEntry.label), activityEntryTabPanel);
        
        
        JPanel labelPanel = new JPanel();
        labelPanel.setOpaque(false);
        labelPanel.setLayout(new BorderLayout());
        activityEntryTabPanel.add(labelPanel);
        
        JPanel labelLeftPanel = new JPanel();
        labelLeftPanel.setOpaque(false);
        labelLeftPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        labelLeftPanel.setPreferredSize(new Dimension(130, 30));
        labelPanel.add(labelLeftPanel, BorderLayout.LINE_START);
        
        labelLeftPanel.add(new MultilingualLabel("ui.swing.dialog.blockedit.activities.activity.label"));
        
        final JTextField activityLabelTextField = new JTextField(activityEntry.label);
        activityLabelTextField.getDocument().addDocumentListener(new DocumentChangeListener() {
            
            @Override
            public void changed(DocumentEvent ev) {
                modified = true;
                int entryIndex = activityEntries.indexOf(activityEntry);
                String label = activityLabelTextField.getText();
                activitiesTabbedPane.setTitleAt(entryIndex, cutTabLabel(label));
                activityEntry.label = label;
            }
            
        });
        labelPanel.add(activityLabelTextField, BorderLayout.CENTER);

        final JButton removeActivityButton = new MultilingualButton(IconLoader.loadIcon("trash"), "ui.swing.dialog.blockedit.activities.activity.delete_activity");
        removeActivityButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(parent,
                    text("ui.swing.dialog.blockedit.activities.activity.delete_activity.sure"),
                    text("ui.swing.dialog.blockedit.activities.activity.delete_activity.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.dialog.blockedit.activities.activity.delete_activity.cancel"),
                        text("ui.swing.dialog.blockedit.activities.activity.delete_activity.delete"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.DANGER,
                    },
                    text("ui.swing.dialog.blockedit.activities.activity.delete_activity.delete")
                );
                if (answer==1) {
                    Dimension originalSize = getSize();
                    
                    modified = true;
                    int entryIndex = activityEntries.indexOf(activityEntry);
                    activityEntries.remove(entryIndex);
                    activitiesTabbedPane.removeTabAt(entryIndex);
                    BlockEditDialog.this.pack();
                    
                    if (activityEntries.isEmpty()) {
                        Point currentLocation = getLocation();
                        if (emptyTop == null) {
                            Dimension currentSize = getSize();
                            int yDiff = (int)((originalSize.getHeight() - currentSize.getHeight()) / 2);
                            emptyTop = (int)currentLocation.getY() + yDiff;
                        }
                        setLocation((int)currentLocation.getX(), emptyTop);
                    }
                }
            }
            
        });
        labelPanel.add(removeActivityButton, BorderLayout.LINE_END);
        
        
        JPanel periodsAndTagsPanel = new JPanel();
        periodsAndTagsPanel.setLayout(new BoxLayout(periodsAndTagsPanel, BoxLayout.LINE_AXIS));
        activityEntryTabPanel.add(periodsAndTagsPanel);
        
        JPanel periodsPanel = new JPanel();
        periodsPanel.setLayout(new BorderLayout());
        periodsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        periodsAndTagsPanel.add(periodsPanel);
        
        JPanel periodsHeaderPanel = new JPanel();
        periodsHeaderPanel.setLayout(new BorderLayout());
        periodsPanel.add(periodsHeaderPanel, BorderLayout.PAGE_START);
        
        periodsHeaderPanel.add(new MultilingualLabel("ui.swing.dialog.blockedit.activities.activity.periods"), BorderLayout.LINE_START);
        
        final JList<Labeled.Wrapper> periodList = new JList<Labeled.Wrapper>();
        periodList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JButton addPeriodButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.dialog.blockedit.activities.activity.periods.assign_period");
        addPeriodButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Period> wrapper = new GeneralWrapper<Period>();
                PeriodSelectDialog dialog = new PeriodSelectDialog(parent, document, wrapper);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    modified = true;
                    activityEntry.periods.add(wrapper.get());
                    fillPeriodList(periodList, activityEntry);
                }
            }
            
        });
        periodsHeaderPanel.add(addPeriodButton, BorderLayout.LINE_END);
        
        JScrollPane periodListScrollPane = new JScrollPane(periodList);
        periodListScrollPane.setPreferredSize(new Dimension(100, 90));
        periodsPanel.add(periodListScrollPane, BorderLayout.CENTER);
        fillPeriodList(periodList, activityEntry);
        final Runnable removePeriodAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = periodList.getSelectedIndex();
                if (selectedIndex<0) {
                    return;
                }
                Period period = (Period)periodList.getModel().getElementAt(selectedIndex).get();
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(parent,
                    text("ui.swing.dialog.blockedit.activities.activity.periods.remove_period.sure"),
                    text("ui.swing.dialog.blockedit.activities.activity.periods.remove_period.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.dialog.blockedit.activities.activity.periods.remove_period.cancel"),
                        text("ui.swing.dialog.blockedit.activities.activity.periods.remove_period.remove"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.DANGER,
                    },
                    text("ui.swing.dialog.blockedit.activities.activity.periods.remove_period.remove")
                );
                if (answer==1) {
                    modified = true;
                    activityEntry.periods.remove(period);
                    fillPeriodList(periodList, activityEntry);
                }
            }
            
        };
        periodList.addMouseListener(new MouseListener() {
            
            @Override
            public void mouseReleased(MouseEvent ev) {}
            
            @Override
            public void mousePressed(MouseEvent ev) {}
            
            @Override
            public void mouseExited(MouseEvent ev) {}
            
            @Override
            public void mouseEntered(MouseEvent ev) {}
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getButton()==MouseEvent.BUTTON3) {
                    removePeriodAction.run();
                }
            }
        });
        periodList.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {
                if (ev.getKeyCode()==KeyEvent.VK_DELETE) {
                    removePeriodAction.run();
                }
            }
            
        });
        
        
        JPanel tagsPanel = new JPanel();
        tagsPanel.setLayout(new BorderLayout());
        tagsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        periodsAndTagsPanel.add(tagsPanel);
        
        JPanel tagsHeaderPanel = new JPanel();
        tagsHeaderPanel.setLayout(new BorderLayout());
        tagsPanel.add(tagsHeaderPanel, BorderLayout.PAGE_START);
        
        tagsHeaderPanel.add(new MultilingualLabel("ui.swing.dialog.blockedit.activities.activity.tags"), BorderLayout.LINE_START);

        final JList<Labeled.PairWrapper<Tag>> tagList = new JList<Labeled.PairWrapper<Tag>>();
        tagList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JButton addTagButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.dialog.blockedit.activities.activity.tags.assign_tag");
        addTagButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Tag> wrapper = new GeneralWrapper<Tag>();
                TagSelectDialog dialog = new TagSelectDialog(parent, document, wrapper);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    modified = true;
                    activityEntry.tags.add(wrapper.get());
                    fillTagList(tagList, activityEntry);
                }
            }
            
        });
        tagsHeaderPanel.add(addTagButton, BorderLayout.LINE_END);
        
        JScrollPane tagListScrollPane = new JScrollPane(tagList);
        tagListScrollPane.setPreferredSize(new Dimension(100, 90));
        tagsPanel.add(tagListScrollPane, BorderLayout.CENTER);
        fillTagList(tagList, activityEntry);
        final Runnable removeTagAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = tagList.getSelectedIndex();
                if (selectedIndex<0) {
                    return;
                }
                Tag tag = tagList.getModel().getElementAt(selectedIndex).get();
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(parent,
                    text("ui.swing.dialog.blockedit.activities.activity.tags.remove_tag.sure"),
                    text("ui.swing.dialog.blockedit.activities.activity.tags.remove_tag.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.dialog.blockedit.activities.activity.tags.remove_tag.cancel"),
                        text("ui.swing.dialog.blockedit.activities.activity.tags.remove_tag.remove"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.DANGER,
                    },
                    text("ui.swing.dialog.blockedit.activities.activity.tags.remove_tag.remove")
                );
                if (answer==1) {
                    modified = true;
                    activityEntry.tags.remove(tag);
                    fillTagList(tagList, activityEntry);
                }
            }
            
        };
        tagList.addMouseListener(new MouseListener() {
            
            @Override
            public void mouseReleased(MouseEvent ev) {}
            
            @Override
            public void mousePressed(MouseEvent ev) {}
            
            @Override
            public void mouseExited(MouseEvent ev) {}
            
            @Override
            public void mouseEntered(MouseEvent ev) {}
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getButton()==MouseEvent.BUTTON3) {
                    removeTagAction.run();
                }
            }
        });
        tagList.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {
                if (ev.getKeyCode()==KeyEvent.VK_DELETE) {
                    removeTagAction.run();
                }
            }
            
        });
        
        
        JPanel resourceSubsetsPanel = new JPanel();
        resourceSubsetsPanel.setLayout(new BorderLayout());
        resourceSubsetsPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        activityEntryTabPanel.add(resourceSubsetsPanel);
        
        JPanel resourceSubsetsHeaderPanel = new JPanel();
        resourceSubsetsHeaderPanel.setLayout(new BorderLayout());
        resourceSubsetsPanel.add(resourceSubsetsHeaderPanel, BorderLayout.PAGE_START);
        
        resourceSubsetsHeaderPanel.add(new MultilingualLabel("ui.swing.dialog.blockedit.activities.activity.resources"));

        final JList<Labeled.PairWrapper<ResourceSubset>> resourceSubsetList = new JList<Labeled.PairWrapper<ResourceSubset>>();
        resourceSubsetList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JPanel addResourcePanel = new JPanel();
        addResourcePanel.setLayout(new BoxLayout(addResourcePanel, BoxLayout.LINE_AXIS));
        resourceSubsetsHeaderPanel.add(addResourcePanel, BorderLayout.LINE_END);
        
        JButton addResourceSubsetButton = new MultilingualButton(IconLoader.loadIcon("add_split"), "ui.swing.dialog.blockedit.activities.activity.resources.assign_resourcesubset");
        addResourceSubsetButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Resource resource;
                {
                    GeneralWrapper<Resource> wrapper = new GeneralWrapper<Resource>();
                    ResourceSelectDialog dialog = new ResourceSelectDialog(parent, document, wrapper);
                    dialog.run();
                    if (dialog.getResult()==EditDialog.RESULT_OK) {
                        resource = wrapper.get();
                    } else {
                        return;
                    }
                }
                
                {
                    GeneralWrapper<ResourceSubset> wrapper = new GeneralWrapper<ResourceSubset>(new ResourceSubset.Whole(resource));
                    ResourceSubsetEditDialog dialog = new ResourceSubsetEditDialog(parent, document, wrapper);
                    dialog.run();
                    if (dialog.getResult()==EditDialog.RESULT_OK) {
                        modified = true;
                        activityEntry.resourceSubsets.add(wrapper.get());
                        fillResourceSubsetList(resourceSubsetList, activityEntry);
                    }
                }
            }
            
        });
        addResourcePanel.add(addResourceSubsetButton);
        
        JButton addResourceButton = new MultilingualButton(IconLoader.loadIcon("add"), "ui.swing.dialog.blockedit.activities.activity.resources.assign_resource");
        addResourceButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Resource resource;
                {
                    GeneralWrapper<Resource> wrapper = new GeneralWrapper<Resource>();
                    ResourceSelectDialog dialog = new ResourceSelectDialog(parent, document, wrapper);
                    dialog.run();
                    if (dialog.getResult()==EditDialog.RESULT_OK) {
                        resource = wrapper.get();
                    } else {
                        return;
                    }
                }
                
                modified = true;
                activityEntry.resourceSubsets.add(new ResourceSubset.Whole(resource));
                fillResourceSubsetList(resourceSubsetList, activityEntry);
            }
            
        });
        addResourcePanel.add(addResourceButton);

        JScrollPane resourceSubsetListScrollPane = new JScrollPane(resourceSubsetList);
        resourceSubsetListScrollPane.setPreferredSize(new Dimension(100, 90));
        resourceSubsetsPanel.add(resourceSubsetListScrollPane, BorderLayout.CENTER);
        fillResourceSubsetList(resourceSubsetList, activityEntry);
        final Runnable removeResourceSubsetAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = resourceSubsetList.getSelectedIndex();
                if (selectedIndex<0) {
                    return;
                }
                ResourceSubset resourceSubset = resourceSubsetList.getModel().getElementAt(selectedIndex).get();
                // TODO: convert to showConfirmDialog
                int answer = OptionPaneUtil.showOptionDialog(parent,
                    text("ui.swing.dialog.blockedit.activities.activity.resources.remove_resource.sure"),
                    text("ui.swing.dialog.blockedit.activities.activity.resources.remove_resource.confirm"),
                    JOptionPane.QUESTION_MESSAGE,
                    new String[]{
                        text("ui.swing.dialog.blockedit.activities.activity.resources.remove_resource.cancel"),
                        text("ui.swing.dialog.blockedit.activities.activity.resources.remove_resource.remove"),
                    },
                    new ButtonPurpose[] {
                        ButtonPurpose.NONE,
                        ButtonPurpose.DANGER,
                    },
                    text("ui.swing.dialog.blockedit.activities.activity.resources.remove_resource.remove")
                );
                if (answer==1) {
                    modified = true;
                    activityEntry.resourceSubsets.remove(resourceSubset);
                    fillResourceSubsetList(resourceSubsetList, activityEntry);
                }
            }
            
        };
        final Runnable editResourceSubsetAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = resourceSubsetList.getSelectedIndex();
                if (selectedIndex<0) {
                    return;
                }
                
                ResourceSubset resourceSubset = resourceSubsetList.getModel().getElementAt(selectedIndex).get();
                GeneralWrapper<ResourceSubset> wrapper = new GeneralWrapper<ResourceSubset>(resourceSubset);
                ResourceSubsetEditDialog dialog = new ResourceSubsetEditDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    modified = true;
                    activityEntry.resourceSubsets.remove(resourceSubset);
                    activityEntry.resourceSubsets.add(wrapper.get());
                    fillResourceSubsetList(resourceSubsetList, activityEntry);
                }
            }
            
        };
        resourceSubsetList.addMouseListener(new MouseListener() {
            
            @Override
            public void mouseReleased(MouseEvent ev) {}
            
            @Override
            public void mousePressed(MouseEvent ev) {}
            
            @Override
            public void mouseExited(MouseEvent ev) {}
            
            @Override
            public void mouseEntered(MouseEvent ev) {}
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getButton()==MouseEvent.BUTTON3) {
                    removeResourceSubsetAction.run();
                } else if (ev.getButton()==MouseEvent.BUTTON1 && ev.getClickCount()==2) {
                    editResourceSubsetAction.run();
                }
            }
        });
        resourceSubsetList.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {
                switch (ev.getKeyCode()) {
                    case KeyEvent.VK_DELETE:
                        removeResourceSubsetAction.run();
                    break;
                    case KeyEvent.VK_ENTER:
                        editResourceSubsetAction.run();
                    break;
                }
            }
            
        });
        
        
        if (focus) {
            activitiesTabbedPane.setSelectedIndex(nextIndex);
            activityLabelTextField.requestFocus();
            activityLabelTextField.selectAll();
        }
        
        pack();
    }
    
    private void fillPeriodList(JList<Labeled.Wrapper> periodList, ActivityEntry activityEntry) {
        DefaultListModel<Labeled.Wrapper> model = new DefaultListModel<Labeled.Wrapper>();
        for (Period period: activityEntry.periods) {
            model.addElement(new Labeled.Wrapper(period));
        }
        periodList.setModel(model);
    }
    
    private void fillTagList(JList<Labeled.PairWrapper<Tag>> tagList, ActivityEntry activityEntry) {
        DefaultListModel<Labeled.PairWrapper<Tag>> model = new DefaultListModel<Labeled.PairWrapper<Tag>>();
        for (Tag tag: activityEntry.tags) {
            model.addElement(new Labeled.PairWrapper<Tag>(getTagLabel(tag), tag));
        }
        tagList.setModel(model);
    }
    
    private void fillResourceSubsetList(JList<Labeled.PairWrapper<ResourceSubset>> resourceSubsetList, ActivityEntry activityEntry) {
        Collections.sort(activityEntry.resourceSubsets, new ResourceSubset.ResourceSubsetComparator());
        DefaultListModel<Labeled.PairWrapper<ResourceSubset>> model = new DefaultListModel<Labeled.PairWrapper<ResourceSubset>>();
        for (ResourceSubset resourceSubset: activityEntry.resourceSubsets) {
            model.addElement(new Labeled.PairWrapper<ResourceSubset>(getResourceSubsetLabel(resourceSubset), resourceSubset));
        }
        resourceSubsetList.setModel(model);
    }
    
    private String getTagLabel(Tag tag) {
        String typeLabel = "?";
        switch (tag.getType()) {
            case SUBJECT: typeLabel = text("core.tag.type.subject"); break;
            case LANGUAGE: typeLabel = text("core.tag.type.language"); break;
            case OTHER: typeLabel = text("core.tag.type.other"); break;
        }
        return typeLabel+": "+tag.getLabel();
    }
    
    private String getResourceSubsetLabel(ResourceSubset resourceSubset) {
        Resource resource = resourceSubset.getResource();
        String typeLabel = "?";
        switch (resource.getType()) {
            case CLASS: typeLabel = text("core.resource.type.class"); break;
            case PERSON: typeLabel = text("core.resource.type.person"); break;
            case LOCALE: typeLabel = text("core.resource.type.locale"); break;
            case OBJECT: typeLabel = text("core.resource.type.object"); break;
            case OTHER: typeLabel = text("core.resource.type.other"); break;
        }
        String resourceLabel = resource.getLabel();
        String subsetLabel = null;
        if (resourceSubset.isWhole()) {
            // nothing to do
        } else if (resourceSubset instanceof ResourceSubset.SplittingPart) {
            subsetLabel =
                text("ui.swing.dialog.blockedit.activities.activity.resources.subset")+": "+
                ((ResourceSubset.SplittingPart)resourceSubset).getSplittingPart().getLabel()
            ;
        } else {
            subsetLabel = text("ui.swing.dialog.blockedit.activities.activity.resources.complex");
        }
        String resourceLabelAsPostfix = ((subsetLabel==null)?"":" ("+subsetLabel+")");
        return typeLabel+": "+resourceLabel+resourceLabelAsPostfix;
    }
    
    private String cutTabLabel(String label) {
        if (label.length()==0) {
            label = "?";
        } else if (label.length()>20) {
            label = label.substring(0, 19)+"…";
        }
        return label;
    }
    
    private class ActivityEntry {
        
        Activity activity;
        
        String label;
        
        PeriodSet periods = new PeriodSet();
        
        Set<Tag> tags = new TreeSet<Tag>(new MultiComparator<Tag>(new Tag.TagComparator()));
        
        ResourceSubsetList resourceSubsets = new ResourceSubsetList();
        
        ActivityEntry(Activity activity, Collection<Period> periods) {
            this.activity = activity;
            this.label = activity.getLabel();
            this.periods.addAll(periods);
            this.tags.addAll(activity.getTagManager().getTags());
            this.resourceSubsets = activity.getResourceManager().getResourceSubsets();
        }
        
        ActivityMemento createActivityMemento() {
            return new ActivityMemento(label, new BlockMemento.ActivityMemento.TagManagerMemento(tags), new BlockMemento.ActivityMemento.ResourceManagerMemento(resourceSubsets));
        }
        
    }

}

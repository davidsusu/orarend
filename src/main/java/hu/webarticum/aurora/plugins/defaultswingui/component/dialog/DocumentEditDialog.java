package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;
import static hu.webarticum.aurora.app.Shortcut.trigger;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JTextField;

import hu.webarticum.aurora.app.memento.DocumentLightMemento;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.chm.AbstractCommand;


public class DocumentEditDialog extends LabeledEditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    public DocumentEditDialog(JFrame parent, Document document) {
        super(parent);
        this.title = text("ui.swing.dialog.documentedit.title");
        this.document = document;
        init();
    }
    
    @Override
    protected void build() {
        labelField = new JTextField();
        labelField.setMaximumSize(new Dimension(700, 25));
        addRow(new MultilingualLabel("ui.swing.dialog.documentedit.document_title"), labelField);
    }

    @Override
    protected void load() {
        labelField.setText(document.getLabel());
    }

    @Override
    protected void save() {
        if (!modified) {
            return;
        }
        
        DocumentLightMemento oldMemento = new DocumentLightMemento(document);
        DocumentLightMemento newMemento = new DocumentLightMemento(labelField.getText());
        history().addAndExecute(new DocumentEditCommand(document, oldMemento, newMemento));
    }

    protected static class DocumentEditCommand extends AbstractCommand {

        private final Document document;
        
        private final DocumentLightMemento oldMemento;

        private final DocumentLightMemento newMemento;
        
        public DocumentEditCommand(Document document, DocumentLightMemento oldMemento, DocumentLightMemento newMemento) {
            this.document = document;
            this.oldMemento = oldMemento;
            this.newMemento = newMemento;
        }
        
        @Override
        protected boolean _execute() {
            newMemento.apply(document);
            trigger("document-edited", document);
            return true;
        }

        @Override
        protected boolean _rollBack() {
            oldMemento.apply(document);
            trigger("document-edited", document);
            return true;
        }

        @Override
        public String toString() {
            return text("ui.swing.dialog.documentedit.command.edit");
        }
        
    }

}

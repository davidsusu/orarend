package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchReplacer {

    private String text;
    
    public MatchReplacer(String text) {
        this.text = text;
    }
    
    public String getText() {
        return text;
    }
    
    public String replaceAll(Pattern pattern, ReplaceCallback callback) {
        StringBuilder newTextBuilder = new StringBuilder();
        Matcher matcher = pattern.matcher(text);
        int length = text.length();
        int matchEndPos = 0;
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            if (start>matchEndPos) {
                newTextBuilder.append(text.substring(matchEndPos, start));
            }
            matchEndPos = end;
            String currentReplacement = callback.replace(matcher);
            newTextBuilder.append(currentReplacement);
        }
        if (matchEndPos<length) {
            newTextBuilder.append(text.substring(matchEndPos, length));
        }
        text = newTextBuilder.toString();
        return text;
    }
    
    public interface ReplaceCallback {
        
        public String replace(MatchResult result);
        
    }
    
}

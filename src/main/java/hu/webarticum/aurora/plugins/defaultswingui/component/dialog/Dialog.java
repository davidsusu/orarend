package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

import hu.webarticum.aurora.plugins.defaultswingui.global.GlobalUiObjects;

public abstract class Dialog extends JDialog {

    private static final long serialVersionUID = 1L;

    
    protected static Dimension cachedScreenSize = null;
    
    protected static Insets cachedScreenInsets = null;
    
    
    protected JFrame parent;

    protected JPanel containerPanel;
    
    protected boolean loaded = false;

    protected boolean cancelled = false;

    protected String title;
    
    
    public Dialog(JFrame parent) {
        super(fallbackParent(parent), "", true);
        
        this.parent = parent;
        
        title = text("ui.swing.dialog.dialog.title");
        
        containerPanel = new JPanel();
        containerPanel.setLayout(new BorderLayout());
        getContentPane().add(containerPanel);
        
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowOpened(WindowEvent ev) {
                opened();
            }
            
        });
    }

    @Override
    protected JRootPane createRootPane() {
        JRootPane rootPane = new JRootPane();
        rootPane.registerKeyboardAction(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Dialog.this.tryCancel();
            }
            
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        return rootPane;
    }

    protected abstract void build();
    
    protected void init() {
        setTitle(title);
        build();
    }
    
    public void run() {
        pack();
        
        if (parent==null) {
            parent = GlobalUiObjects.getMainFrame();
        }

        int contextLeft;
        int contextTop;
        int contextWidth;
        int contextHeight;
        
        if (parent==null) {
            Dimension screenSize = GlobalUiObjects.getScreenSize(this);
            Insets screenInsets = GlobalUiObjects.getScreenInsets(this);
            
            int desktopLeft = screenInsets.left;
            int desktopTop = screenInsets.top;
            int desktopWidth = screenSize.width - (screenInsets.left + screenInsets.right);
            int desktopHeight = screenSize.height - (screenInsets.top + screenInsets.bottom);
            
            contextLeft = desktopLeft;
            contextTop = desktopTop;
            contextWidth = desktopWidth;
            contextHeight = desktopHeight;
        } else {
            contextLeft = parent.getX();
            contextTop = parent.getY();
            contextWidth = parent.getWidth();
            contextHeight = parent.getHeight();
        }
        
        overruleDialogSize();
        
        int xCenter = contextLeft+(contextWidth/2);
        int yCenter = contextTop+(contextHeight/2);
        int x = xCenter-(getWidth()/2);
        int y = yCenter-(getHeight()/2);
        setLocation(x, y);
        setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
        setVisible(true);
        _dispose();
    }
    
    protected void opened() {
    }
    
    public void overruleDialogSize() {
        Dimension screenSize = GlobalUiObjects.getScreenSize(this);
        Insets screenInsets = GlobalUiObjects.getScreenInsets(this);
        
        int desktopHeight = screenSize.height - (screenInsets.top + screenInsets.bottom);
        
        Dimension currentSize = getSize();
        int currentWidth = (int)currentSize.getWidth();
        int currentHeight = (int)currentSize.getHeight();
        if (currentHeight > desktopHeight) {
            setSize(currentWidth, desktopHeight);
        }
    }
    
    @Override
    public void setVisible(boolean visible) {
        if (!visible) {
            super.setVisible(false);
        }
        if (parent == null) {
            JFrame fallbackFrame = (JFrame)getParent();
            fallbackFrame.setVisible(visible);
            if (visible) {
                fallbackFrame.setTitle(getTitle());
            }
        }
        if (visible) {
            super.setVisible(true);
        }
    }
    
    protected void _dispose() {
        dispose();
        if (parent == null) {
            JFrame fallbackFrame = (JFrame)getParent();
            fallbackFrame.setVisible(false);
            try {
                fallbackFrame.dispose();
            } catch (Throwable e) {
                System.out.println(e);
            }
        }
    }
    
    protected void closeDialog() {
        setVisible(false);
        _dispose();
    }
    
    protected void cancelDialog() {
        cancelled = true;
        closeDialog();
    }
    
    protected void tryCancel() {
        cancelDialog();
    }
    
    public boolean isCancelled() {
        return cancelled;
    }
    
    public static JFrame fallbackParent(JFrame frame) {
        if (frame!=null) {
            return frame;
        }
        
        JFrame fallbackFrame = new JFrame();
        fallbackFrame.setUndecorated(true);
        return fallbackFrame;
    }
    
}

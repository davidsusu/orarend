package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class BoardSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<Board> boardWrapper;

    private JComboBox<Labeled.Wrapper> boardSelectComboBox;
    
    public BoardSelectDialog(JFrame parent, Document document, GeneralWrapper<Board> boardWrapper) {
        super(parent);
        this.title = text("ui.swing.dialog.boardselect.title");
        this.document = document;
        this.boardWrapper = boardWrapper;
        setResizable(false);
        init();
    }
    
    @Override
    protected void build() {
        JPanel addPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 5));
        mainPanel.add(addPanel);
        
        JButton addButton = new MultilingualButton("ui.swing.dialog.boardselect.new_board");
        addButton.setIcon(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                Board board = new Board();
                BoardEditDialog dialog = new BoardEditDialog(parent, document, board);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    document.getBoardStore().register(board);
                    refreshBoardList(board);
                }
            }
            
        });
        addPanel.add(addButton);
        
        boardSelectComboBox = new JComboBox<Labeled.Wrapper>();
        addRow(new MultilingualLabel("ui.swing.dialog.boardselect.board"), boardSelectComboBox);
    }

    @Override
    protected void load() {
        refreshBoardList(boardWrapper.get());
    }

    @Override
    protected void save() {
        Board board = (Board)((Labeled.Wrapper)boardSelectComboBox.getSelectedItem()).get();
        boardWrapper.set(board);
    }
    
    private void refreshBoardList(Board selectedBoard) {
        DefaultComboBoxModel<Labeled.Wrapper> model = new DefaultComboBoxModel<Labeled.Wrapper>();
        boardSelectComboBox.setModel(model);
        Document.BoardStore boardStore = document.getBoardStore();
        if (boardStore.size()==0) {
            okButton.setEnabled(false);
        } else {
            okButton.setEnabled(true);
            int pos = 0;
            int selectedPos = -1;
            for (Board board : boardStore) {
                model.addElement(new Labeled.Wrapper(board));
                if (board.equals(selectedBoard)) {
                    selectedPos = pos;
                }
                pos++;
            }
            if (selectedPos!=(-1)) {
                boardSelectComboBox.setSelectedIndex(selectedPos);
            }
        }
    }

}

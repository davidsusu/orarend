package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class ResourceSelectDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<Resource> resourceWrapper;
    
    private Resource.Type defaultType;
    
    private boolean forceType;

    private JComboBox<Labeled.PairWrapper<Resource.Type>> typeSelectComboBox;
    
    private JComboBox<Labeled.Wrapper> resourceSelectComboBox;
    
    public ResourceSelectDialog(JFrame parent, Document document, GeneralWrapper<Resource> resourceWrapper) {
        this(parent, document, resourceWrapper, Resource.Type.CLASS, false);
    }

    public ResourceSelectDialog(JFrame parent, Document document, GeneralWrapper<Resource> resourceWrapper, Resource.Type forcedType) {
        this(parent, document, resourceWrapper, forcedType, true);
    }

    public ResourceSelectDialog(JFrame parent, Document document, GeneralWrapper<Resource> resourceWrapper, Resource.Type defaultType, boolean forceType) {
        super(parent);
        this.title = text("ui.swing.dialog.resourceselect.title");
        this.document = document;
        this.resourceWrapper = resourceWrapper;
        this.defaultType = defaultType;
        this.forceType = forceType;
        setResizable(false);
        init();
    }
    
    @Override
    protected void build() {
        JPanel addPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 5));
        mainPanel.add(addPanel);
        
        JButton addButton = new MultilingualButton("ui.swing.dialog.resourceselect.new_resource");
        addButton.setIcon(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                @SuppressWarnings("unchecked")
                Resource.Type type = ((Labeled.PairWrapper<Resource.Type>)typeSelectComboBox.getSelectedItem()).get();
                Resource resource = new Resource(type);
                ResourceEditDialog dialog = new ResourceEditDialog(parent, document, resource);
                dialog.run();
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    document.getResourceStore().register(resource);
                    refreshResourceList(resource.getType(), resource);
                }
            }
            
        });
        addPanel.add(addButton);
        
        typeSelectComboBox = new JComboBox<Labeled.PairWrapper<Resource.Type>>();
        typeSelectComboBox.addItem(new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.class"), Resource.Type.CLASS));
        typeSelectComboBox.addItem(new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.person"), Resource.Type.PERSON));
        typeSelectComboBox.addItem(new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.locale"), Resource.Type.LOCALE));
        typeSelectComboBox.addItem(new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.object"), Resource.Type.OBJECT));
        typeSelectComboBox.addItem(new Labeled.PairWrapper<Resource.Type>(text("core.resource.type.other"), Resource.Type.OTHER));
        addRow(new MultilingualLabel("ui.swing.dialog.resourceselect.type"), typeSelectComboBox);
        
        resourceSelectComboBox = new JComboBox<Labeled.Wrapper>();
        addRow(new MultilingualLabel("ui.swing.dialog.resourceselect.resource"), resourceSelectComboBox);
    }

    @Override
    protected void load() {
        Resource resource = resourceWrapper.get();
        Resource.Type type = defaultType;
        if (resource != null) {
            type = resource.getType();
        }

        int typeCount = typeSelectComboBox.getItemCount();
        for (int i = 0; i < typeCount; i++) {
            if (typeSelectComboBox.getItemAt(i).get().equals(type)) {
                typeSelectComboBox.setSelectedIndex(i);
                break;
            }
        }

        if (forceType) {
            typeSelectComboBox.setEnabled(false);
        }
        
        refreshResourceList(type, resource);
    }
    
    @Override
    protected void afterLoad() {
        typeSelectComboBox.addItemListener(new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent ev) {
                @SuppressWarnings("unchecked")
                Resource.Type type = ((Labeled.PairWrapper<Resource.Type>)typeSelectComboBox.getSelectedItem()).get();
                refreshResourceList(type, null);
            }
            
        });
    }

    @Override
    protected void save() {
        Resource resource = (Resource)((Labeled.Wrapper)resourceSelectComboBox.getSelectedItem()).get();
        resourceWrapper.set(resource);
    }
    
    private void refreshResourceList(Resource.Type type, Resource selectedResource) {
        DefaultComboBoxModel<Labeled.Wrapper> model = new DefaultComboBoxModel<Labeled.Wrapper>();
        resourceSelectComboBox.setModel(model);
        List<Resource> filteredResources = new ArrayList<Resource>();
        for (Resource resource: document.getResourceStore()) {
            if (resource.getType().equals(type)) {
                filteredResources.add(resource);
            }
        }
        if (filteredResources.isEmpty()) {
            okButton.setEnabled(false);
        } else {
            okButton.setEnabled(true);
            int pos = 0;
            int selectedPos = -1;
            for (Resource resource : filteredResources) {
                model.addElement(new Labeled.Wrapper(resource));
                if (resource.equals(selectedResource)) {
                    selectedPos = pos;
                }
                pos++;
            }
            if (selectedPos==(-1)) {
                selectedPos = 0;
            }
            resourceSelectComboBox.setSelectedIndex(selectedPos);
        }
    }

}

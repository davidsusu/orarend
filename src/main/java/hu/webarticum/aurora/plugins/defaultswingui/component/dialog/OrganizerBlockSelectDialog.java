package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;


import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualRadioButton;


public class OrganizerBlockSelectDialog extends EditDialog {
    
    private static final long serialVersionUID = 1L;

    private Document document;
    
    private GeneralWrapper<Block> block1Wrapper;
    
    private GeneralWrapper<Block> block2Wrapper;

    private Block block1;

    private Block block2;
    
    private JButton block1Button;
    
    private JButton block2Button;
    
    private JRadioButton block2NoneRadioButton;
    
    private JRadioButton block2SpecifiedRadioButton;
    
    public OrganizerBlockSelectDialog(JFrame parent, Document document, GeneralWrapper<Block> block1Wrapper, GeneralWrapper<Block> block2Wrapper) {
        super(parent);
        this.title = text("ui.swing.dialog.organizerblockselect.title");
        this.document = document;
        this.block1Wrapper = block1Wrapper;
        this.block2Wrapper = block2Wrapper;
        this.block1 = block1Wrapper.get();
        this.block2 = block2Wrapper.get();
        init();
    }
    
    @Override
    protected void build() {
        JPanel block1Panel = new JPanel();
        block1Panel.setLayout(new BorderLayout());
        block1Button = new JButton("?");
        block1Button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Block> wrapper = new GeneralWrapper<Block>(OrganizerBlockSelectDialog.this.block1);
                BlockSelectDialog dialog = new BlockSelectDialog(parent, document, wrapper);
                dialog.run();

                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    OrganizerBlockSelectDialog.this.block1 = wrapper.get();
                    reload();
                }
            }
            
        });
        block1Panel.add(block1Button, BorderLayout.CENTER);
        addRow(new MultilingualLabel("ui.swing.dialog.organizerblockselect.block1"), block1Panel);
        
        ButtonGroup block2TypeButtonGroup = new ButtonGroup();
        
        JPanel boardNonePanel = new JPanel();
        boardNonePanel.setLayout(new BorderLayout());
        
        block2NoneRadioButton = new MultilingualRadioButton("ui.swing.dialog.organizerblockselect.block2_new");
        block2NoneRadioButton.setSelected(true);
        boardNonePanel.add(block2NoneRadioButton, BorderLayout.CENTER);
        block2TypeButtonGroup.add(block2NoneRadioButton);
        
        addRow(new MultilingualLabel("ui.swing.dialog.organizerblockselect.block2"), boardNonePanel);

        
        JPanel boardSpecifiedPanel = new JPanel();
        boardSpecifiedPanel.setLayout(new BorderLayout());
        
        block2SpecifiedRadioButton = new JRadioButton();
        boardSpecifiedPanel.add(block2SpecifiedRadioButton, BorderLayout.LINE_START);
        block2TypeButtonGroup.add(block2SpecifiedRadioButton);
        
        block2Button = new JButton("");
        block2Button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Block> wrapper = new GeneralWrapper<Block>(OrganizerBlockSelectDialog.this.block2);
                BlockSelectDialog dialog = new BlockSelectDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult()==EditDialog.RESULT_OK) {
                    OrganizerBlockSelectDialog.this.block2 = wrapper.get();
                    reload();
                    if (OrganizerBlockSelectDialog.this.block2!=null) {
                        block2SpecifiedRadioButton.setSelected(true);
                    }
                } else if (OrganizerBlockSelectDialog.this.block2==null) {
                    block2NoneRadioButton.setSelected(true);
                }
            }
            
        });
        boardSpecifiedPanel.add(block2Button, BorderLayout.CENTER);
        addRow("", boardSpecifiedPanel);
    }

    @Override
    protected void load() {
        reload();
    }

    protected void reload() {
        if (block1 == null) {
            block1Button.setText("");
            okButton.setEnabled(false);
        } else {
            block1Button.setText(block1.getLabel());
            okButton.setEnabled(true);
        }
        
        if (block2 == null) {
            block2Button.setText("");
        } else {
            block2Button.setText(block2.getLabel());
        }
    }

    @Override
    protected void save() {
        block1Wrapper.set(block1);
        if (block2SpecifiedRadioButton.isSelected()) {
            block2Wrapper.set(block2);
        } else {
            block2Wrapper.set(null);
        }
    }

}

package hu.webarticum.aurora.plugins.defaultswingui.i18n;

import static hu.webarticum.aurora.app.Shortcut.text;

public class PathMultilingualContent implements MultilingualContent {

    private final String path;
    
    private final Object[] parameters;
    
    public PathMultilingualContent(String path, Object... parameters) {
        this.path = path;
        this.parameters = parameters;
    }
    
    @Override
    public String toString() {
        if (path == null || path.isEmpty()) {
            return "";
        } else if (parameters.length > 0) {
            return String.format(text(path), parameters);
        } else {
            return text(path);
        }
    }

    @Override
    public String toStringOrNull() {
        String text = toString();
        return text.isEmpty() ? null : text;
    }
    
}

package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RunnableActionListener implements ActionListener {

    private final Runnable runnable;
    
    public RunnableActionListener(Runnable runnable) {
        this.runnable = runnable;
    }
    
    @Override
    public void actionPerformed(ActionEvent ev) {
        runnable.run();
    }

}

package hu.webarticum.aurora.plugins.defaultswingui.util;

import javax.swing.event.DocumentEvent;

public class RunnableDocumentChangeListener extends DocumentChangeListener {

    private final Runnable runnable;
    
    public RunnableDocumentChangeListener(Runnable runnable) {
        this.runnable = runnable;
    }

    @Override
    protected void changed(DocumentEvent ev) {
        runnable.run();
    }
    
}

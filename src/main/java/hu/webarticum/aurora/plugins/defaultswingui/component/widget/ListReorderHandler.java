package hu.webarticum.aurora.plugins.defaultswingui.component.widget;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.EventListener;

import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;
import javax.swing.event.EventListenerList;

public class ListReorderHandler<E> {

    private static final IdentityHashDataFlavor HASH_DATA_FLAVOR = new IdentityHashDataFlavor();

    private static final IndexDataFlavor INDEX_DATA_FLAVOR = new IndexDataFlavor();
    
    private final JList<E> list;

    private final ReorderKeyListener keyListener = new ReorderKeyListener();
    
    private final EventListenerList listeners = new EventListenerList();
    
    private volatile boolean enabled = false;
    
    private TransferHandler previousTransferHandler = null;
    
    private boolean previousDragEnabled = false;
    
    private DropMode previousDropMode = null;
    
    
    public ListReorderHandler(JList<E> list) {
        this.list = list;
    }
    

    public void addListener(ReorderListener listener) {
        listeners.add(ReorderListener.class, listener);
    }

    public void removeListener(ReorderListener listener) {
        listeners.remove(ReorderListener.class, listener);
    }

    public synchronized void enable() {
        if (enabled) {
            return;
        }
        
        previousTransferHandler = list.getTransferHandler();
        previousDragEnabled = list.getDragEnabled();
        previousDropMode = list.getDropMode();
        
        list.setTransferHandler(new ListTransferHandler());
        list.setDragEnabled(true);
        list.setDropMode(DropMode.INSERT);
        
        list.addKeyListener(keyListener);
        
        enabled = true;
    }

    public synchronized void disable() {
        if (!enabled) {
            return;
        }
        
        list.removeKeyListener(keyListener);
        
        list.setDropMode(previousDropMode);
        list.setDragEnabled(previousDragEnabled);
        list.setTransferHandler(previousTransferHandler);
        
        enabled = false;
    }

    private void onMoved(int sourceIndex, int targetIndex) {
        for (ReorderListener reorderListener: listeners.getListeners(ReorderListener.class)) {
            reorderListener.onMoved(sourceIndex, targetIndex);
        }
    }

    
    public static interface ReorderListener extends EventListener {
        
        public void onMoved(int sourceIndex, int targetIndex);
        
    }
    
    
    // FIXME how else to detect source component?
    private class ListTransferHandler extends TransferHandler {
        
        private static final long serialVersionUID = 1L;

        
        @Override
        public boolean canImport(TransferHandler.TransferSupport info) {
            try {
                return (
                    info.isDataFlavorSupported(HASH_DATA_FLAVOR) &&
                    System.identityHashCode(list) == (Integer) info.getTransferable().getTransferData(HASH_DATA_FLAVOR)
                );
            } catch (Exception e) {
                return false;
            }
       }

        @Override
        protected Transferable createTransferable(JComponent component) {
            return new ListReorderTransferable(System.identityHashCode(list), list.getSelectedIndex());
        }
        
        @Override
        public int getSourceActions(JComponent component) {
            return TransferHandler.MOVE;
        }
        
        @Override
        public boolean importData(TransferHandler.TransferSupport info) {
            if (!info.isDrop()) {
                return false;
            }
            Transferable transferable = info.getTransferable();
            if (!transferable.isDataFlavorSupported(HASH_DATA_FLAVOR)) {
                return false;
            }
            int sourceIndex;
            try {
                sourceIndex = (Integer)transferable.getTransferData(INDEX_DATA_FLAVOR);
            } catch (Exception e) {
                return false;
            }
            if (sourceIndex < 0) {
                return false;
            }
            DefaultListModel<E> model = (DefaultListModel<E>)list.getModel();
            E value = model.elementAt(sourceIndex);
            JList.DropLocation dropLocation = (JList.DropLocation)info.getDropLocation();
            int dropIndex = dropLocation.getIndex();
            int targetIndex = dropIndex > sourceIndex ? dropIndex - 1 : dropIndex;
            if (sourceIndex == targetIndex) {
                return true;
            }
            
            model.remove(sourceIndex);
            model.add(targetIndex, value);
            onMoved(sourceIndex, targetIndex);
            
            return true;
        }
        
        @Override
        protected void exportDone(JComponent c, Transferable data, int action) {
            // nothing additional to do
        }
        
    }
    
    
    private static class IdentityHashDataFlavor extends DataFlavor implements Serializable {

        private static final long serialVersionUID = 1L;
        
        
        private IdentityHashDataFlavor() {
            super(Integer.class, Integer.class.getCanonicalName());
        }
        
    }

    
    private static class IndexDataFlavor extends DataFlavor implements Serializable {

        private static final long serialVersionUID = 1L;
        
        
        private IndexDataFlavor() {
            super(Integer.class, Integer.class.getCanonicalName());
        }
        
    }
    
    
    private static class ListReorderTransferable implements Transferable, Serializable {

        private static final long serialVersionUID = 1L;
        

        final int identityHash;
        
        final int sourceIndex;
        
        
        ListReorderTransferable(int identityHash, int sourceIndex) {
            this.identityHash = identityHash;
            this.sourceIndex = sourceIndex;
        }
        
        
        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (flavor instanceof IdentityHashDataFlavor) {
                return identityHash;
            } else if (flavor instanceof IndexDataFlavor) {
                return sourceIndex;
            } else {
                throw new UnsupportedFlavorException(flavor);
            }
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[] { HASH_DATA_FLAVOR, INDEX_DATA_FLAVOR };
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return ((flavor instanceof IdentityHashDataFlavor) || (flavor instanceof IndexDataFlavor));
        }
        
    }
    
    
    private class ReorderKeyListener implements KeyListener {

        @Override
        public void keyPressed(KeyEvent ev) {
            if (ev.isControlDown()) {
                int keyCode = ev.getKeyCode();
                int index = list.getSelectedIndex();
                DefaultListModel<E> model = (DefaultListModel<E>)list.getModel();
                int count = model.getSize();
                if (index >= 0) {
                    if (keyCode == KeyEvent.VK_UP) {
                        if (index > 0) {
                            E value = model.remove(index);
                            model.add(index - 1, value);
                            list.setSelectedIndex(index - 1);
                            onMoved(index, index - 1);
                            ev.consume();
                        }
                    } else if (keyCode == KeyEvent.VK_DOWN) {
                        if (index < count - 1) {
                            E value = model.remove(index);
                            model.add(index + 1, value);
                            list.setSelectedIndex(index + 1);
                            onMoved(index, index + 1);
                            ev.consume();
                        }
                    }
                }
            }
        }

        @Override
        public void keyReleased(KeyEvent ev) {
            // nothing to do
        }

        @Override
        public void keyTyped(KeyEvent ev) {
            // nothing to do
        }
        
    }
    
}

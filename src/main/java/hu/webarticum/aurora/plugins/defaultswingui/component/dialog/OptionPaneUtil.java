package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;

public class OptionPaneUtil {

    public static void showMessageDialog(JFrame parent, String message) {
        showMessageDialog(parent, message, null, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void showMessageDialog(JFrame parent, String message, String title, int messageType) {
        JOptionPane.showMessageDialog(
            parent,
            message,
            title != null? title : UIManager.getString("OptionPane.messageDialogTitle", Application.instance().getLocale()),
            messageType
        );
    }
    
    public static int showConfirmDialog(JFrame parent, String message, ButtonPurpose purpose) {
        return showConfirmDialog(parent, message, null, JOptionPane.QUESTION_MESSAGE, purpose);
    }

    public static int showConfirmDialog(JFrame parent, String message, String title, ButtonPurpose purpose) {
        return showConfirmDialog(parent, message, title, JOptionPane.QUESTION_MESSAGE, purpose);
    }

    public static int showConfirmDialog(JFrame parent, String message, String title, int messageType, ButtonPurpose purpose) {
        return showConfirmDialog(parent, message, title, messageType, purpose, "OK"); // FIXME
    }

    public static int showConfirmDialog(
        JFrame parent,
        String message,
        String title,
        int messageType,
        ButtonPurpose purpose,
        String okButtonLabel
    ) {
        return showConfirmDialog(parent, message, title, messageType, purpose, okButtonLabel, "Cancel"); // FIXME
    }

    public static int showConfirmDialog(
        JFrame parent,
        String message,
        String title,
        int messageType,
        ButtonPurpose purpose,
        String okButtonLabel,
        String cancelButtonLabel
    ) {
        Object[] options = new Object[] { cancelButtonLabel, okButtonLabel };
        ButtonPurpose[] purposes = new ButtonPurpose[] { ButtonPurpose.NONE, purpose };
        int answerIndex = showOptionDialog(parent, message, title, messageType, options, purposes, okButtonLabel);
        return answerIndex == 1 ? JOptionPane.YES_OPTION : JOptionPane.CANCEL_OPTION;
    }

    public static int showOptionDialog(
        JFrame parent,
        String message,
        String title,
        int messageType,
        Object[] options,
        Object initialValue
    ) {
        return showOptionDialog(parent, message, title, messageType, options, null, initialValue);
    }

    public static int showOptionDialog(
        JFrame parent,
        String message,
        String title,
        int messageType,
        Object[] options,
        ButtonPurpose[] optionPurposes,
        Object initialValue
    ) {
        ButtonOptionDialog dialog = new ButtonOptionDialog(parent, message, title, messageType, options, optionPurposes, initialValue);
        dialog.run();
        return dialog.getResult();
    }
    
    public static Object showInputDialog(
            JFrame parent,
            String message,
            String title,
            int messageType,
            Object[] option,
            Object initialValue,
            ButtonPurpose purpose
        ) {
        // TODO
        return JOptionPane.showInputDialog(parent, message, title, messageType, null, option, initialValue);
    }

    public static String showInputDialog(JFrame parent, String message, ButtonPurpose purpose) {
        // TODO
        return JOptionPane.showInputDialog(parent, message);
    }

    public static String showInputDialog(JFrame parent, String message, String oldValue, ButtonPurpose purpose) {
        // TODO
        return JOptionPane.showInputDialog(parent, message, oldValue);
    }
    
}

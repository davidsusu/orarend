package hu.webarticum.aurora.plugins.defaultswingui.util;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;


public class SizingPanel extends JPanel {
    
    private static final long serialVersionUID = 1L;

    
    private final Component component;
    
    
    public SizingPanel(Component component, int align) {
        this.component = component;
        setLayout(new FlowLayout(align, 0, 0));
        add(component);
    }
    
    @Override
    public Dimension getPreferredSize() {
        Dimension preferredSize = component.getPreferredSize();
        if (preferredSize == null) {
            preferredSize = new Dimension(25, 25);
        }
        Dimension minimumSize = getMinimumSize();
        if (minimumSize == null) {
            minimumSize = preferredSize;
        }
        Dimension maximumSize = getMaximumSize();
        if (maximumSize == null) {
            maximumSize = preferredSize;
        }
        int width = Math.min(maximumSize.width, Math.max(minimumSize.width, preferredSize.width));
        int height = Math.min(maximumSize.height, Math.max(minimumSize.height, preferredSize.height));
        return new Dimension(width, height);
    }
    
}

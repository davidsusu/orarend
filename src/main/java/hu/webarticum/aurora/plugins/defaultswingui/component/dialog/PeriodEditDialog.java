package hu.webarticum.aurora.plugins.defaultswingui.component.dialog;

import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.ParseException;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import hu.webarticum.aurora.app.memento.PeriodMemento;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.chm.Command;


public class PeriodEditDialog extends LabeledEditDialog {

    private static final long serialVersionUID = 1L;

    private Document document;

    private Period period;

    private JSpinner termSpinner = new JSpinner();

    private JSpinner positionSpinner = new JSpinner();
    
    public PeriodEditDialog(JFrame parent, Document document, Period period) {
        super(parent);
        this.title = text("ui.swing.dialog.periodedit.title");
        this.document = document;
        this.period = period;
        init();
    }
    
    @Override
    protected void build() {
        labelField = new JTextField();
        labelField.setMaximumSize(new Dimension(700, 25));
        
        addRow(new MultilingualLabel("ui.swing.dialog.periodedit.label"), labelField);
        
        final NumberFormatter termPositionFormatter = new NumberFormatter(){
            
            private static final long serialVersionUID = 1L;

            {
                setValueClass(Integer.class);
            }
            
            @Override
            public Object stringToValue(String text) throws ParseException {
                if (text.equals(text("ui.swing.dialog.periodedit.term.none"))) {
                    return (Integer)0;
                } else {
                    return super.stringToValue(text);
                }
            }
            
            @Override
            public String valueToString(Object value) throws ParseException {
                int number = ((Number)value).intValue();
                if (number==0) {
                    return text("ui.swing.dialog.periodedit.position.none");
                } else {
                    return super.valueToString(value);
                }
            }
            
        };
        
        termSpinner.setPreferredSize(new Dimension(80, 30));
        SpinnerModel termSpinnerModel = new SpinnerNumberModel(5, -1, Integer.MAX_VALUE, 1);
        termSpinner.setModel(termSpinnerModel);
        DefaultEditor termSpinnerEditor = new DefaultEditor(termSpinner){
            
            private static final long serialVersionUID = 1L;

            {
                JFormattedTextField textField = getTextField();
                textField.setFormatterFactory(new DefaultFormatterFactory(termPositionFormatter));
                textField.setHorizontalAlignment(JTextField.RIGHT);
            }
            
        };
        termSpinner.setEditor(termSpinnerEditor);
        JPanel termSpinnerPanel = new JPanel(new BorderLayout());
        termSpinnerPanel.add(termSpinner, BorderLayout.LINE_START);
        addRow(new MultilingualLabel("ui.swing.dialog.periodedit.term"), termSpinnerPanel);

        positionSpinner.setPreferredSize(new Dimension(80, 30));
        SpinnerModel positionSpinnerModel = new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1);
        positionSpinner.setModel(positionSpinnerModel);
        DefaultEditor positionSpinnerEditor = new DefaultEditor(positionSpinner){
            
            private static final long serialVersionUID = 1L;

            {
                JFormattedTextField textField = getTextField();
                textField.setFormatterFactory(new DefaultFormatterFactory(termPositionFormatter));
                textField.setHorizontalAlignment(JTextField.RIGHT);
            }
            
        };
        positionSpinner.setEditor(positionSpinnerEditor);
        JPanel positionSpinnerPanel = new JPanel(new BorderLayout());
        positionSpinnerPanel.add(positionSpinner, BorderLayout.LINE_START);
        addRow(new MultilingualLabel("ui.swing.dialog.periodedit.position"), positionSpinnerPanel);
    }

    @Override
    protected void load() {
        labelField.setText(period.getLabel());
        termSpinner.setValue(period.getTerm());
        positionSpinner.setValue(period.getPosition());
    }

    @Override
    protected void save() {
        if (!modified) {
            return;
        }
        
        PeriodMemento newMemento = new PeriodMemento(
            labelField.getText(),
            (Integer)termSpinner.getValue(),
            (Integer)positionSpinner.getValue()
        );
        
        Command command;
        Document.PeriodStore periodStore = document.getPeriodStore();
        
        if (periodStore.contains(period)) {
            PeriodMemento oldMemento = new PeriodMemento(period);
            command = new StoreItemEditCommand<Period>(
                "ui.swing.dialog.periodedit.command.edit",
                periodStore, period, oldMemento, newMemento
            );
        } else {
            newMemento.apply(period);
            command = new StoreItemRegisterCommand<Period>(
                "ui.swing.dialog.periodedit.command.register",
                periodStore, period
            );
        }
        
        history().addAndExecute(command);
    }

}

package hu.webarticum.aurora.app;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import hu.webarticum.aurora.app.event.EventManager;
import hu.webarticum.aurora.app.util.RecentFilesManager;
import hu.webarticum.aurora.app.util.Settings;
import hu.webarticum.aurora.app.util.i18n.TreeMultiLanguageTextRepository;
import hu.webarticum.aurora.core.io.DocumentIo.DocumentIoQueue;
import hu.webarticum.aurora.plugins.basekit.BaseKitPlugin;
import hu.webarticum.aurora.plugins.consoleui.ConsoleUiPlugin;
import hu.webarticum.aurora.plugins.defaultautoput.DefaultAutoPutPlugin;
import hu.webarticum.aurora.plugins.defaultswingui.DefaultSwingUiPlugin;
import hu.webarticum.chm.History;
import hu.webarticum.chm.LinearHistory;
import hu.webarticum.jpluginmanager.core.PluginContainer;
import hu.webarticum.jpluginmanager.core.PluginManager;
import hu.webarticum.jpluginmanager.core.PluginResolver;
import hu.webarticum.jpluginmanager.core.SimplePluginLoader;

public class Application implements Closeable {
    
    private static final String DEFAULT_LANGUAGE = "hu_HU";
    
    
    private static volatile Application _instance = null;
    
    
    private boolean closed = false;
    
    
    private Map<String, String> argumentMap = new HashMap<String, String>();
    
    private DocumentIoQueue documentIoQueue = null;
    
    private TreeMultiLanguageTextRepository multiLanguageTextRepository = null;

    private Settings settings = null;

    private RecentFilesManager recentFilesManager = null;

    private History history = null;
    
    private EventManager eventManager = null;
    
    private PluginManager pluginManager = null;
    
    
    private Application() {
    }
    
    public static synchronized Application instance() {
        if (_instance == null) {
            _instance = new Application();
        }
        return _instance;
    }

    public synchronized Map<String, String> getArgumentMap() {
        return argumentMap;
    }

    public synchronized DocumentIoQueue getDocumentIoQueue() {
        if (documentIoQueue == null) {
            documentIoQueue = new DocumentIoQueue();
        }
        return documentIoQueue;
    }
    
    public synchronized String getLanguage() {
        Settings settings = getSettings();
        String language = settings.get("language");
        if (language.isEmpty()) {
            language = DEFAULT_LANGUAGE;
            settings.set("language", language);
        }
        return language;
    }
    
    public synchronized Locale getLocale() {
        // FIXME
        return new Locale(getLanguage());
    }

    public synchronized TreeMultiLanguageTextRepository getTextRepository() {
        if (multiLanguageTextRepository == null) {
            
            multiLanguageTextRepository = new TreeMultiLanguageTextRepository();
            
            Settings settings = getSettings();
            
            // XXX
            String language = settings.get("language");
            if (language.isEmpty()) {
                language = "hu_HU";
            }
            settings.set("language", language);
            multiLanguageTextRepository.setLanguage(language);
        }
        return multiLanguageTextRepository;
    }
    
    public synchronized File getSettingsDirectory() {
        return new File(System.getProperty("user.home")+File.separator+"."+Info.CODENAME);
    }

    public synchronized File getSettingsFile() {
        return getUserFile("settings.properties");
    }

    public synchronized File getRecentFilesFile() {
        return getUserFile("recent-files.txt");
    }
    
    public synchronized File getUserFile(String filename) {
        File directory = getSettingsDirectory();
        if (directory.exists()) {
            if (!directory.isDirectory()) {
                try {
                    return File.createTempFile(Info.CODENAME, ""+System.identityHashCode(directory));
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        } else {
            directory.mkdir();
        }
        File file = new File(directory.getPath(), filename);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public synchronized Settings getSettings() {
        if (settings == null) {
            settings = new Settings(getSettingsFile());
        }
        return settings;
    }

    public synchronized RecentFilesManager getRecentFilesManager() {
        if (recentFilesManager == null) {
            recentFilesManager = new RecentFilesManager(getRecentFilesFile(), getRecentFilesCount());
        }
        return recentFilesManager;
    }

    public synchronized int getRecentFilesCount() {
        settings = getSettings();
        settings.load();
        String historyLengthStr = settings.get("recent_files_count", "10");
        int result = 10;
        try {
            result = Integer.parseInt(historyLengthStr);
        } catch (NumberFormatException e) {
            // nothing to do
        }
        return result;
    }

    public synchronized void resetHistory() {
        history = null;
    }

    public synchronized History getHistory() {
        if (history == null) {
            history = new LinearHistory(getHistoryLength());
        }
        return history;
    }

    public synchronized int getHistoryLength() {
        settings = getSettings();
        settings.load();
        String historyLengthStr = settings.get("history_length", "-1");
        int result = -1;
        try {
            result = Integer.parseInt(historyLengthStr);
        } catch (NumberFormatException e) {
            // nothing to do
        }
        if (result == 0) {
            result = -1;
        }
        return result;
    }

    public synchronized EventManager getEventManager() {
        if (eventManager == null) {
            eventManager = new EventManager();
        }
        return eventManager;
    }

    public synchronized PluginManager getPluginManager() {
        if (pluginManager == null) {
            // XXX
            pluginManager = new PluginManager(new SimplePluginLoader(
                DefaultSwingUiPlugin.class,
                DefaultAutoPutPlugin.class,
                ConsoleUiPlugin.class,
                
                // XXX old
                BaseKitPlugin.class
            ));
            new PluginResolver().startAll(pluginManager);
        }
        return pluginManager;
    }

    @Override
    public synchronized void close() {
        if (closed) {
            return;
        }
        
        for (PluginContainer pluginContainer: getPluginManager().getActivePluginContainers()) {
            pluginContainer.getPlugin().stop();
        }
    }
    
    @Override
    protected void finalize() throws Throwable { // NOSONAR
        close();
    }

}

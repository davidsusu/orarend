package hu.webarticum.aurora.app;

import java.util.List;

import hu.webarticum.aurora.app.event.Listener;
import hu.webarticum.aurora.app.event.Specification;
import hu.webarticum.aurora.app.util.i18n.TreeMultiLanguageTextRepository;
import hu.webarticum.chm.History;

public class Shortcut {
    
    private static final String FALLBACK_STRING = "???";
    
    public static TreeMultiLanguageTextRepository texts() {
        return Application.instance().getTextRepository();
    }

    @Deprecated
    public static String rawText(String text) {
        return text;
    }

    public static String text(String path) {
        return fallback(Application.instance().getTextRepository().getText(path));
    }

    public static String text(String path, String language) {
        return fallback(Application.instance().getTextRepository().getText(path, language));
    }

    public static Object textProvider(String path) {
        return new Object() {
            
            @Override
            public String toString() {
                return fallback(Application.instance().getTextRepository().getText(path));
            }
            
        };
    }

    public static <T> List<T> extensions(Class<T> type) {
        return Application.instance().getPluginManager().getExtensions(type);
    }
    
    public static History history() {
        return Application.instance().getHistory();
    }

    public static void events() {
        Application.instance().getEventManager();
    }

    public static void listen(String typeName, Listener listener) {
        Application.instance().getEventManager().register(typeName, listener);
    }

    public static void listen(String typeName, Object target, Listener listener) {
        Application.instance().getEventManager().register(typeName, target, listener);
    }

    public static void listen(Specification specification, Listener listener) {
        Application.instance().getEventManager().register(specification, listener);
    }

    public static void trigger(String typeName) {
        Application.instance().getEventManager().trigger(typeName);
    }

    public static void trigger(String typeName, Object target) {
        Application.instance().getEventManager().trigger(typeName, target);
    }

    public static void trigger(Specification specification) {
        Application.instance().getEventManager().trigger(specification);
    }

    private static String fallback(String string) {
        if (string != null && !string.isEmpty()) {
            return string;
        } else {
            return FALLBACK_STRING;
        }
    }
    
}

package hu.webarticum.aurora.app.util.common;

import java.io.Serializable;

public class Pair<T, U> implements Serializable {

    private static final long serialVersionUID = 1L;

    private final T left;

    private final U right;
    
    public Pair(T left, U right) {
        this.left = left;
        this.right = right;
    }

    public T getLeft() {
        return left;
    }

    public U getRight() {
        return right;
    }
    
    public Pair<U, T> getReversed() {
        return new Pair<U, T>(right, left);
    }
    
    @Override
    public boolean equals(Object other) {
        if (other instanceof Pair) {
            Pair<?, ?> otherPair = (Pair<?, ?>)other;
            return (equal(otherPair.getLeft(), left) && equal(otherPair.getRight(), right));
        } else {
            return false;
        }
    }
    
    private boolean equal(Object object1, Object object2) {
        if (object1 == null) {
            return (object2 == null);
        } else if (object2 == null) {
            return false;
        } else {
            return object1.equals(object2);
        }
    }
    
    @Override
    public int hashCode() {
        return left.hashCode() + (right.hashCode() * 31);
    }

    @Override
    public String toString() {
        return "Pair {" + left + ", " + right + "}";
    }
    
}

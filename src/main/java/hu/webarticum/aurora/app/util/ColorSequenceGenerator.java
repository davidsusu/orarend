package hu.webarticum.aurora.app.util;

import hu.webarticum.aurora.core.model.Color;

public class ColorSequenceGenerator {
    
    private int index = 0;
    
    private Color[] colors = {
        new Color(0xEE2222),
        new Color(0x339900),
        new Color(0xFFFF00),
        new Color(0x5555FF),
        new Color(0xEE33FF),
        new Color(0xFFCC33),
        new Color(0xBBDD77),
        new Color(0xFF9999),
        new Color(0x51BBD3),
        new Color(0xDD7700),
        new Color(0x0013CA),
        new Color(0xC5B42E),
        new Color(0xC1C1C1),
        new Color(0xFC099C),
        new Color(0xA97096),
        new Color(0xD6FF41),
        new Color(0x770000),
        new Color(0x09E8FC),
        new Color(0xCC9922),
        new Color(0xCCCCFF),
        new Color(0xF6C119),
        new Color(0xAA9999),
        new Color(0xFFDDDD),
        new Color(0x335577),
        new Color(0xCDBC83),
        new Color(0xAD00CA),
        new Color(0x997700),
        new Color(0xF67719),
        new Color(0x000099),
        new Color(0xE3D67F),
        new Color(0x77DD00),
        new Color(0xFBF1A0),
        new Color(0xBB0000),
        new Color(0x773399),
        new Color(0xFF7FCD),
        new Color(0xF6D888),
        new Color(0xD35186),
        new Color(0x94A552),
        new Color(0xF6A488),
        new Color(0x00AA89),
    };
    
    public Color next() {
        if (index>=colors.length) {
            index = 0;
        }
        Color color = colors[index];
        index++;
        return color;
    }
    
    public void reset() {
        index = 0;
    }
    
}

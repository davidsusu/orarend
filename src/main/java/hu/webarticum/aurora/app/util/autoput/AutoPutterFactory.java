package hu.webarticum.aurora.app.util.autoput;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Labeled;

public interface AutoPutterFactory extends Labeled {

    public enum Priority {
        PRIMARY, HIGH, LOW, MEDIUM
    }

    public String getName();
    
    public Priority getPriority();
    
    public AutoPutterAspectStructure collectAspectStructure(Document document, BlockList blocks, Board board);
    
    public AutoPutter createAutoPutter(Document document, BlockList blocks, Board board, List<String> enabledAspects);

    public static class AutoPutterFactoryComparator implements Comparator<AutoPutterFactory>, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public int compare(AutoPutterFactory factory1, AutoPutterFactory factory2) {
            int primaryResult = factory1.getPriority().compareTo(factory2.getPriority());
            if (primaryResult == 0) {
                return (new Labeled.LabeledComparator()).compare(factory1, factory2);
            } else {
                return primaryResult;
            }
        }
        
    }
    
}

package hu.webarticum.aurora.app.util.slots;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Interval;

public class AspectSlotCollector {
    
    public List<Slot> collectOrDefault(Document document, Aspect aspect) {
        List<Slot> slots = collect(document, aspect);
        if (slots.isEmpty()) {
            slots = collectDefault(aspect);
        }
        
        return slots;
    }
    
    public List<Slot> collect(Document document, Aspect aspect) {
        Set<Slot> slotSet = new TreeSet<Slot>(new Comparator<Slot>() {

            @Override
            public int compare(Slot slot1, Slot slot2) {
                return slot1.getInterval().compareTo(slot2.getInterval());
            }
            
        });
        for (Block block: document.getBlockStore()) {
            if (new BlockFilter.ActivityBlockFilter(new ActivityFilter.HasAspect(aspect)).validate(block)) {
                for (TimingSet.TimeEntry timeEntry: block.getCalculatedTimingSet(false)) {
                    Interval interval = new Interval(timeEntry.getTime(), block.getLength());
                    String label = timeEntry.getLabel();
                    slotSet.add(new Slot(interval, label));
                }
            }
        }
        return new ArrayList<Slot>(slotSet);
    }

    public List<Slot> collectDefault(Aspect aspect) {
        List<Slot> slots = new ArrayList<Slot>();
        if (aspect.isTimingSetEnabled()) {
            TimingSet defaultTimingSet = aspect.getTimingSetManager().getDefaultTimingSet();
            if (defaultTimingSet != null) {
                for (TimingSet.TimeEntry timeEntry : defaultTimingSet) {
                    Interval interval = new Interval(timeEntry.getTime(), Block.DEFAULT_LENGTH);
                    String label = timeEntry.getLabel();
                    slots.add(new Slot(interval, label));
                }
            }
        }
        
        return slots;
    }
    
}

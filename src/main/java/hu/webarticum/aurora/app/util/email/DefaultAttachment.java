package hu.webarticum.aurora.app.util.email;

import java.io.IOException;
import java.io.InputStream;

public class DefaultAttachment implements Attachment {
    
    private String filename;
    
    private String mimeType;
    
    private String description;
    
    private ContentProvider contentProvider;

    public DefaultAttachment(String filename, String mimeType, ContentProvider contentProvider) {
        this(filename, mimeType, filename, contentProvider);
    }
    
    public DefaultAttachment(String filename, String mimeType, String description, ContentProvider contentProvider) {
        this.filename = filename;
        this.mimeType = mimeType;
        this.description = description;
        this.contentProvider = contentProvider;
    }
    
    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }
    
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return contentProvider.getInputStream();
    }
    
    public ContentProvider getContentProvider() {
        return contentProvider;
    }
    
    public void setContentProvider(ContentProvider contentProvider) {
        this.contentProvider = contentProvider;
    }
    
    @Override
    public String toString() {
        return getDescription();
    }

    public interface ContentProvider {
        
        public InputStream getInputStream() throws IOException;
        
    }
    
}


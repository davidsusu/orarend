package hu.webarticum.aurora.app.util.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Intervalable;
import hu.webarticum.aurora.core.model.time.Time;

// TODO: extract public methods to separated classes
public class IntervalUtil {

    // TODO: use Intervalable instead
    public static List<Interval> getMaximalDisjoint(Collection<Interval> intervals) {
        List<Interval> result = new ArrayList<Interval>();
        if (intervals.isEmpty()) {
            return result;
        }
        List<Interval> sortedIntervals = new ArrayList<Interval>(intervals);
        Collections.sort(sortedIntervals);
        TreeSet<Interval> remainingIntervals = new TreeSet<Interval>(new Comparator<Interval>() {

            @Override
            public int compare(Interval interval1, Interval interval2) {
                int comp = interval1.getEnd().compareTo(interval2.getEnd());
                if (comp==0) {
                    return interval1.getStart().compareTo(interval2.getStart());
                } else {
                    return comp;
                }
            }
            
        });
        remainingIntervals.addAll(intervals);
        ListIterator<Interval> iterator = sortedIntervals.listIterator();
        while (!remainingIntervals.isEmpty()) {
            Interval nextInterval = remainingIntervals.pollFirst();
            Time breakTime = nextInterval.getEnd();
            result.add(nextInterval);
            while (iterator.hasNext()) {
                Interval interval = iterator.next();
                if (interval.getStart().compareTo(breakTime)<0) {
                    remainingIntervals.remove(interval);
                } else {
                    iterator.previous();
                    break;
                }
            }
        }
        return result;
    }

    public static <T extends Intervalable> SortedMap<Interval, List<T>> breakIntervalablesToSections(
        Iterable<T> intervalables
    ) {
        SortedMap<Time, Pair<List<T>, List<T>>> timingMap = new TreeMap<Time, Pair<List<T>, List<T>>>();
        for (T intervalable: intervalables) {
            Interval interval = intervalable.getInterval();
            
            Pair<List<T>, List<T>> startPair = timingMap.get(interval.getStart());
            if (startPair == null) {
                startPair = createTimingItem();
                timingMap.put(interval.getStart(), startPair);
            }
            startPair.getLeft().add(intervalable);

            Pair<List<T>, List<T>> endPair = timingMap.get(interval.getEnd());
            if (endPair == null) {
                endPair = createTimingItem();
                timingMap.put(interval.getEnd(), endPair);
            }
            endPair.getRight().add(intervalable);
        }
        
        SortedMap<Interval, List<T>> result = new TreeMap<Interval, List<T>>();
        
        List<T> currentElements = new ArrayList<T>();
        Time previousTime = new Time(Long.MIN_VALUE);
        for (Map.Entry<Time, Pair<List<T>, List<T>>> entry: timingMap.entrySet()) {
            Time time = entry.getKey();
            if (!currentElements.isEmpty()) {
                result.put(new Interval(previousTime, time), new ArrayList<T>(currentElements));
            }
            Pair<List<T>, List<T>> pair = entry.getValue();
            List<T> startings = pair.getLeft();
            Set<T> endings = new HashSet<T>(pair.getRight());
            List<T> zeroLengthIntervalables = new ArrayList<T>();
            for (T startingIntervalable: startings) {
                if (endings.contains(startingIntervalable)) {
                    zeroLengthIntervalables.add(startingIntervalable);
                    endings.remove(startingIntervalable);
                } else {
                    currentElements.add(startingIntervalable);
                }
            }
            currentElements.removeAll(endings);
            if (!zeroLengthIntervalables.isEmpty()) {
                result.put(new Interval(time, time), zeroLengthIntervalables);
            }
            previousTime = time;
        }
        
        return result;
    }
    
    private static <T> Pair<List<T>, List<T>> createTimingItem() {
        return new Pair<List<T>, List<T>>(
            new ArrayList<T>(),
            new ArrayList<T>()
        );
    }
    
}

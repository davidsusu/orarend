package hu.webarticum.aurora.app.util.autoput;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import hu.webarticum.aurora.core.model.Labeled;

public class AutoPutterAspectStructure {
    
    public static final AutoPutterAspectStructure EMPTY = builder().build();


    public static final int DEFAULT_LEVEL = 0;
    
    public static final Category DEFAULT_CATEGORY = new Category("", "");
    
    public static final SubCategory DEFAULT_SUBCATEGORY = new SubCategory(DEFAULT_CATEGORY, "", "");
    
    
    private final Map<String, AspectData> aspectDatas;

    private final Map<Category, Map<SubCategory, List<String>>> categoryStructure;
    
    private final Map<Integer, List<String>> aspectsByLevel;
    
    
    public AutoPutterAspectStructure(Builder builder) {
        aspectDatas = new LinkedHashMap<String, AspectData>(builder.aspectDatas);
        
        categoryStructure = new LinkedHashMap<AutoPutterAspectStructure.Category, Map<SubCategory,List<String>>>();
        
        Set<SubCategory> usedSubCategories = new LinkedHashSet<AutoPutterAspectStructure.SubCategory>();
        for (AspectData aspectData : aspectDatas.values()) {
            for (SubCategory subCategory : aspectData.subCategories) {
                usedSubCategories.add(subCategory);
            }
        }
        
        Set<SubCategory> orderedSubCategories = new LinkedHashSet<AutoPutterAspectStructure.SubCategory>(
            builder.orderedSubCategories
        );
        orderedSubCategories.retainAll(usedSubCategories);
        orderedSubCategories.addAll(usedSubCategories);
        
        for (SubCategory subCategory : orderedSubCategories) {
            addSubCategory(subCategory);
        }
        
        aspectsByLevel = new TreeMap<Integer, List<String>>();

        for (AspectData aspectData : aspectDatas.values()) {
            for (SubCategory subCategory : aspectData.subCategories) {
                categoryStructure.get(subCategory.category).get(subCategory).add(aspectData.name);
            }
            List<String> levelAspects = aspectsByLevel.get(aspectData.level);
            if (levelAspects == null) {
                levelAspects = new ArrayList<String>();
                aspectsByLevel.put(aspectData.level, levelAspects);
            }
            levelAspects.add(aspectData.name);
        }
    }
    
    private void addSubCategory(SubCategory subCategory) {
        Category category = subCategory.category;
        
        Map<SubCategory, List<String>> structureUnderCategory = categoryStructure.get(category);
        if (structureUnderCategory == null) {
            structureUnderCategory = new LinkedHashMap<SubCategory, List<String>>();
            categoryStructure.put(category, structureUnderCategory);
        }
        
        if (!structureUnderCategory.containsKey(subCategory)) {
            structureUnderCategory.put(subCategory, new ArrayList<String>());
        }
    }
    

    public static Builder builder() {
        return new Builder();
    }
    
    
    public boolean isUncategorized() {
        if (!categoryStructure.keySet().equals(Collections.singleton(DEFAULT_CATEGORY))) {
            return false;
        }
        
        return categoryStructure.get(DEFAULT_CATEGORY).keySet().equals(Collections.singleton(DEFAULT_SUBCATEGORY));
    }
    
    public List<Category> getCategories() {
        return new ArrayList<Category>(categoryStructure.keySet());
    }

    public List<SubCategory> getSubCategoriesOf(Category category) {
        return new ArrayList<SubCategory>(categoryStructure.get(category).keySet());
    }

    public List<String> getAspectsIn(SubCategory subCategory) {
        return new ArrayList<String>(categoryStructure.get(subCategory.category).get(subCategory));
    }
    
    public List<Integer> getLevels() {
        return new ArrayList<Integer>(aspectsByLevel.keySet());
    }
    
    public List<String> getAspectsAt(int level) {
        return aspectsByLevel.get(level);
    }
    
    public List<String> getAllAspects() {
        return new ArrayList<String>(aspectDatas.keySet());
    }

    public boolean hasAspect(String aspect) {
        return aspectDatas.containsKey(aspect);
    }
    
    public AspectData getAspectData(String aspect) {
        return aspectDatas.get(aspect);
    }
    
    public AutoPutterAspectStructure reduce(Collection<String> aspects) {
        Builder builder = AutoPutterAspectStructure.builder();
        List<String> commonAspects = getAllAspects();
        commonAspects.retainAll(aspects);
        for (String aspect : commonAspects) {
            builder.addAspect(getAspectData(aspect));
        }
        return builder.build();
    }
    

    public static class AspectData {
        
        private final String name;
        
        private final String label;
        
        private final int level;
        
        private final List<SubCategory> subCategories;
        
        
        AspectData(String name, String label, int level, Collection<SubCategory> subCategories) {
            this.name = name;
            this.label = label;
            this.level = level;
            this.subCategories = new ArrayList<SubCategory>(subCategories);
        }
        

        public String getName() {
            return name;
        }

        public String getLabel() {
            return label;
        }

        public int getLevel() {
            return level;
        }

        public List<SubCategory> getSubCategories() {
            return new ArrayList<SubCategory>(subCategories);
        }

    }
    
    
    public static class Builder {
        

        private Set<SubCategory> orderedSubCategories = new LinkedHashSet<SubCategory>();
        
        private Map<String, AspectData> aspectDatas = new LinkedHashMap<String, AspectData>();
        
        
        private Builder() {
            // use builder()
        }
        
        
        public void addSubCategory(SubCategory subCategory) {
            orderedSubCategories.add(subCategory);
        }

        public void addAspect(String aspect) {
            addAspect(aspect, aspect);
        }

        public void addAspect(String aspect, String label) {
            addAspect(aspect, label, DEFAULT_LEVEL);
        }


        public void addAspect(String aspect, String label, int level) {
            addAspect(aspect, label, level, DEFAULT_SUBCATEGORY);
        }

        
        public void addAspect(String aspect, String label, int level, SubCategory... subCategories) {
            addAspect(aspect, label, level, Arrays.asList(subCategories));
        }

        public void addAspect(String aspect, String label, int level, Collection<SubCategory> subCategories) {
            addAspect(new AspectData(aspect, label, level, subCategories));
        }

        public void addAspect(AspectData aspectData) {
            aspectDatas.put(aspectData.name, aspectData);
        }
        
        public AutoPutterAspectStructure build() {
            return new AutoPutterAspectStructure(this);
        }
        
    }
    
    
    public static class Category implements Labeled {
        
        private static final long serialVersionUID = 1L;

        
        private final String name;
        
        private final String label;
        
        
        public Category(String name, String label) {
            this.name = name;
            this.label = label;
        }
        
        
        public String getName() {
            return name;
        }

        @Override
        public String getLabel() {
            return label;
        }
        
        @Override
        public String toString() {
            return String.format("%s: %s", name, label);
        }
        
    }

    
    public static class SubCategory implements Labeled {
        
        private static final long serialVersionUID = 1L;

        
        private final Category category;
        
        private final String name;
        
        private final String label;
        
        
        public SubCategory(Category category, String name, String label) {
            this.category = category;
            this.name = name;
            this.label = label;
        }
        
        
        public Category getCategory() {
            return category;
        }
        
        public String getName() {
            return name;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public String toString() {
            return String.format("%s: %s", name, label);
        }
        
    }
    
}

package hu.webarticum.aurora.app.util;

import hu.webarticum.aurora.core.model.Document;

public interface DocumentFactory {
    
    public Document createDocument();
    
}

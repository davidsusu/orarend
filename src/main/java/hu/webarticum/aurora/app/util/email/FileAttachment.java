package hu.webarticum.aurora.app.util.email;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.activation.MimetypesFileTypeMap;

public class FileAttachment implements Attachment {
    
    private final File file;

    private String filename;

    private String description;

    public FileAttachment(File file) {
        this(file, file.getName());
    }

    public FileAttachment(File file, String filename) {
        this(file, filename, filename);
    }
    
    public FileAttachment(File file, String filename, String description) {
        this.file = file;
        this.filename = filename;
        this.description = description;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    @Override
    public String getMimeType() {
        return new MimetypesFileTypeMap().getContentType(filename);
    }

    @Override
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new FileInputStream(file);
    }

    @Override
    public String toString() {
        return getDescription();
    }

}

package hu.webarticum.aurora.app.util.common;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StringUtil {
    
    public static String normalize(String input) {
        return Normalizer.normalize(input, Normalizer.Form.NFD)
            .replaceAll("\\p{M}", "")
            .replaceAll("\\W+", " ")
            .toLowerCase()
            .trim()
        ;
    }

    public static String simplify(String input) {
        return normalize(input).replace(' ', '_');
    }

    public static String capitalize(String input) {
        if (input.isEmpty()) {
            return input;
        }
        char firstChar = input.charAt(0);
        String remainString = input.substring(1);
        return Character.toUpperCase(firstChar) + remainString.toLowerCase();
    }
    
    public static String setLength(String text, int length, char padChar) {
        if (text == null) {
            text = "";
        }
        int textLength = text.length();
        if (textLength >= length) {
            return text.substring(0, length);
        } else {
            StringBuilder builder = new StringBuilder(text);
            for (int i = textLength; i < length; i++) {
                builder.append(padChar);
            }
            return builder.toString();
        }
    }

    public static String random() {
        return random(10);
    }

    public static String random(int length) {
        return random(10, "abcdefghijklmnopqrstuvwxyz0123456789");
    }

    public static String random(int length, String abc) {
        int abcLength = abc.length();
        Random random = new Random();
        StringBuilder resultBuilder = new StringBuilder();
        for (int i=0; i<length; i++) {
            resultBuilder.append(abc.charAt(random.nextInt(abcLength)));
        }
        return resultBuilder.toString();
    }
    
    public static String join(Iterable<?> items, String separator) {
        boolean first = true;
        StringBuilder builder = new StringBuilder();
        for (Object object: items) {
            String string = object.toString();
            if (first) {
                first = false;
            } else {
                builder.append(separator);
            }
            builder.append(string);
        }
        return builder.toString();
    }
    
    public static String joinEscaped(Iterable<?> items, char separator, char escaper) {
        StringBuilder resultBuilder = new StringBuilder();
        boolean first = true;
        for (Object item: items) {
            if (first) {
                first = false;
            } else {
                resultBuilder.append(separator);
            }
            String str = item.toString();
            str = str.replace("" + escaper, "" + escaper + escaper);
            str = str.replace("" + separator, "" + escaper + separator);
            resultBuilder.append(str);
        }
        if (resultBuilder.length() == 0) {
            resultBuilder.append(escaper);
        }
        return resultBuilder.toString();
    }
    
    public static List<String> splitEscaped(String joined, char separator, char escaper, boolean unescape) {
        List<String> result = new ArrayList<String>();
        if (joined.equals(""+escaper)) {
            return result;
        }
        boolean escapedStatus = false;
        StringBuilder itemBuilder = new StringBuilder();
        int joinedLength = joined.length();
        for (int i=0; i<joinedLength; i++) {
            char c = joined.charAt(i);
            if (escapedStatus) {
                if (!unescape) {
                    itemBuilder.append(escaper);
                }
                itemBuilder.append(c);
                escapedStatus = false;
            } else {
                if (c==separator) {
                    result.add(itemBuilder.toString());
                    itemBuilder = new StringBuilder();
                } else if (c==escaper) {
                    escapedStatus = true;
                } else {
                    itemBuilder.append(c);
                }
            }
        }
        result.add(itemBuilder.toString());
        return result;
    }
    
    public static String toHtml(String text) {
        return text
            .replace("&", "&amp;")
            .replace("<", "&lt;")
            .replace(">", "&gt;")
            .replace("\"", "&quot;")
        ;
    }
    
}

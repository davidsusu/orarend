package hu.webarticum.aurora.app.util.autoput;

import java.util.ArrayList;
import java.util.List;

import hu.webarticum.aurora.app.lang.CancelledException;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;

public abstract class AbstractAutoPutter implements AutoPutter {

    protected Document document = new Document();
    
    protected BlockList blocks = new BlockList();
    
    protected Board board = new Board();
    
    protected Board originalBoard = new Board();
    
    protected List<String> enabledAspects;
    
    protected AutoPutOutput out = new AutoPutOutput.DefaultAutoPutOutput();
    
    protected volatile ResultState resultState = null; // NOSONAR
    

    protected void init(Document document, BlockList blocks, Board board, List<String> enabledAspects) {
        this.document = document;
        this.blocks = blocks.copy();
        this.blocks.removeAll(board.getBlocks());
        this.board = board;
        this.originalBoard = board.copy();
        this.enabledAspects = new ArrayList<String>(enabledAspects);
    }
    

    @Override
    public void setOutput(AutoPutOutput out) {
        this.out = out;
    }

    @Override
    public void run() {
        boolean rollback = false;
        try {
            _run();
        } catch (CancelledException e) {
            rollback = true;
        } catch (Exception e) {
            rollback = true;
            e.printStackTrace();
        }
        clean();
        if (rollback) {
            rollBack();
        }
    }
    
    @Override
    public void runThrows() throws CancelledException {
        try {
            _run();
        } catch (CancelledException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void rollBack() {
        board.applyFrom(originalBoard);
    }

    @Override
    public void clean() {
    }
    
    @Override
    public ResultState getResultState() {
        return resultState;
    }
    
    public void sleep(long millis) throws CancelledException {
        long size = 300;
        long remainer = millis%size;
        long iterations = (millis-remainer)/size;
        try {
            for (long i=0; i<iterations; i++) {
                out.tick();
                Thread.sleep(size);
            }
            if (remainer>0) {
                out.tick();
                Thread.sleep(remainer);
            }
        } catch (InterruptedException e) {
            throw new CancelledException();
        }
        out.tick();
    }
    
    protected abstract void _run() throws CancelledException;
    
}
package hu.webarticum.aurora.app.util;

import java.util.Comparator;

public class ToStringComparator implements Comparator<Object> {

    private final Comparator<String> stringComparator;
    
    public ToStringComparator() {
        this.stringComparator = null;
    }
    
    public ToStringComparator(Comparator<String> stringComparator) {
        this.stringComparator = stringComparator;
    }
    
    @Override
    public int compare(Object object1, Object object2) {
        if (stringComparator == null) {
            return object1.toString().compareTo(object2.toString());
        } else {
            return stringComparator.compare(object1.toString(), object2.toString());
        }
    }

}

package hu.webarticum.aurora.app.util.i18n;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesLanguageTextRepository implements LanguageTextRepository {

    private final String resourcePath;

    private final String language;
    
    private Properties properties = null;
    
    public PropertiesLanguageTextRepository(String resourcePath, String language) {
        this.resourcePath = resourcePath;
        this.language = language;
    }
    
    private void init() {
        properties = new Properties();
        InputStream inputStream = getClass().getResourceAsStream(resourcePath);
        try {
            properties.load(inputStream);
        } catch (Exception e) {
        }
    }

    @Override
    public String getText(String path) {
        if (properties==null) {
            init();
        }
        Object value = properties.get(path);
        if (value==null) {
            return "";
        }
        return (String)value;
    }

    @Override
    public String getLanguage() {
        return language;
    }
    
    
    
}

package hu.webarticum.aurora.app.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecentFilesManager {
    
    private static final Pattern ESCAPE_PATTERN = Pattern.compile("\\\\(.)");

    private final File file;
    
    private final int capacity;
    
    private final List<String> pathList = new CopyOnWriteArrayList<>();
    
    private volatile boolean loaded = false;
    
    public RecentFilesManager(File file, int capacity) {
        this.file = file;
        this.capacity = capacity;
    }
    
    public synchronized void register(String path) {
        reload();
        pathList.remove(path);
        pathList.add(0, path);
        while (pathList.size() > capacity) {
            pathList.remove(pathList.size() - 1);
        }
        save();
    }

    public int getCapacity() {
        return capacity;
    }
    
    public synchronized List<String> getPathList() {
        if (!loaded) {
            reload();
        }
        return pathList;
    }
    
    private synchronized void reload() {
        List<String> newPathList = new ArrayList<>();
        try (BufferedReader reader =
                new BufferedReader(new InputStreamReader(new FileInputStream(file)))
        ) {
            String line;
            while ((line = reader.readLine()) != null) {
                newPathList.add(decodePath(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        pathList.clear();
        pathList.addAll(newPathList);
        loaded = true;
    }

    private synchronized void save() {
        try (Writer writer =
                new OutputStreamWriter(new FileOutputStream(file))
        ) {
            for (String path : pathList) {
                writer.write(encodePath(path));
                writer.write('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private String decodePath(String encoded) {
        StringBuffer resultBuffer = new StringBuffer();
        Matcher matcher = ESCAPE_PATTERN.matcher(encoded);
        while (matcher.find()) {
            String replacement = matcher.group(1);
            if (replacement.equals("n")) {
                replacement = "\n";
            } else if (replacement.equals("r")) {
                replacement = "\r";
            }
            matcher.appendReplacement(
                resultBuffer,
                Matcher.quoteReplacement(replacement)
            );
        }
        matcher.appendTail(resultBuffer);
        return resultBuffer.toString();
    }

    private String encodePath(String path) {
        return path
            .replace("\\", "\\\\")
            .replace("\r", "\\r")
            .replace("\n", "\\n")
        ;
    }
    
}

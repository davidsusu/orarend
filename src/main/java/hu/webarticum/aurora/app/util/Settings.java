package hu.webarticum.aurora.app.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Settings {
    
    private final File file;
    
    private Properties properties = null;
    
    public Settings(File file) {
        this.file = file;
    }
    
    public boolean set(String key, String value) {
        return set(key, value, true);
    }

    public boolean set(String key, String value, boolean save) {
        load();
        properties.setProperty(key, value);
        if (save) {
            return save();
        } else {
            return true;
        }
    }

    public String get(String key) {
        return get(key, "");
    }

    public String get(String key, String fallbackValue) {
        load();
        return properties.getProperty(key, fallbackValue);
    }

    public void load() {
        load(false);
    }

    public boolean load(boolean force) {
        if (force || properties==null) {
            properties = new Properties();
            try {
                properties.load(new FileInputStream(file));
            } catch (FileNotFoundException e) {
                return false;
            } catch (IOException e) {
                return false;
            }
            return true;
        }
        return true;
    }
    
    public boolean save() {
        if (properties==null) {
            return true;
        }
        try {
            properties.store(new FileOutputStream(file), "Timetable global settings");
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return properties.toString();
    }
    
}

package hu.webarticum.aurora.app.util;

import java.net.URL;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.core.io.DocumentIo;
import hu.webarticum.aurora.core.model.Document;

public class UrlDocumentFactory implements DocumentFactory {
    
    private final URL url;
    
    public UrlDocumentFactory(URL url) {
        this.url = url;
    }
    
    @Override
    public Document createDocument() {
        if (url == null) {
            return null; // TODO: throw IllegalArgumentException instead
        }
        
        String extension = FileUtil.getExtension(url);
        if (extension.isEmpty()) {
            extension = Info.DEFAULT_FILE_EXTENSION;
        }
        
        DocumentIo documentIo = Application.instance().getDocumentIoQueue().getByExtension(extension);
        if (documentIo == null) {
            return null; // TODO: throw IllegalStateException instead
        }
        
        try {
            return documentIo.load(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return null;
    }

}

package hu.webarticum.aurora.app.util.i18n;

public interface MultiLanguageTextRepository extends LanguageTextRepository {

    public String getText(String path, String language);
    
    public void setLanguage(String language);
    
}

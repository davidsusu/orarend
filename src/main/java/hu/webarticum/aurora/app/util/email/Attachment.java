package hu.webarticum.aurora.app.util.email;

import java.io.IOException;
import java.io.InputStream;

public interface Attachment {

    public String getFilename();

    public void setFilename(String filename);

    public String getMimeType();

    public String getDescription();
    
    public InputStream getInputStream() throws IOException;

}
package hu.webarticum.aurora.app.util.autoput;

import hu.webarticum.aurora.app.lang.CancelledException;

public interface AutoPutter extends Runnable {
    
    
    public void setOutput(AutoPutOutput out);

    public void runThrows() throws CancelledException;
    
    public void rollBack();

    public void clean();

    public ResultState getResultState();

    public enum ResultState {
        SOLVED, UNSOLVED, ABORTED
    }

    
    public interface AutoPutOutput {
        
        public void setLabel(String label);
        
        public void setProgress(Double progress);

        public void log(String line);

        public void tick() throws CancelledException;

        public void trackTime();

        public void logTime(String comment);
        
        public void finish(boolean success);
        
        // TODO
        public static class DefaultAutoPutOutput implements AutoPutOutput {

            @Override
            public void setLabel(String label) {}

            @Override
            public void setProgress(Double progress) {}

            @Override
            public void log(String line) {}

            @Override
            public void tick() {}

            @Override
            public void trackTime() {}

            @Override
            public void logTime(String comment) {}
            
            @Override
            public void finish(boolean success) {}
            
        }
        
    }
    
    
}

package hu.webarticum.aurora.app.util.email;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SimpleMessage {
    
    private String name;

    private String email;
    
    private String subject;
    
    private String text;

    private final List<Attachment> attachments = new ArrayList<Attachment>();
    
    
    public SimpleMessage() {
        this("", "", "", "");
    }

    public SimpleMessage(String name, String email, String subject, String text, Attachment... attachments) {
        this(name, email, subject, text, Arrays.asList(attachments));
    }

    public SimpleMessage(String name, String email, String subject, String text, Collection<Attachment> attachments) {
        this.name = name;
        this.email = email;
        this.subject = subject;
        this.text = text;
        this.attachments.addAll(attachments);
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Attachment> getAttachments() {
        return new ArrayList<Attachment>(attachments);
    }

    public void setAttachments(Attachment... attachments) {
        setAttachments(Arrays.asList(attachments));
    }
    
    public void setAttachments(Collection<Attachment> attachments) {
        this.attachments.clear();
        addAttachments(attachments);
    }

    public void addAttachments(Attachment... attachments) {
        addAttachments(Arrays.asList(attachments));
    }
    
    public void addAttachments(Collection<Attachment> attachments) {
        this.attachments.addAll(attachments);
    }

    public void addAttachment(Attachment attachment) {
        this.attachments.add(attachment);
    }

}

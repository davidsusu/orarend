package hu.webarticum.aurora.app.util;

import java.util.Arrays;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.ResourceSubset;
import hu.webarticum.aurora.core.model.ResourceSubsetList;

public final class BlockUtil {

    private BlockUtil() {
        // utility class
    }
    
    
    // TODO: make a better implementation
    public static int getShrinkedCount(BlockList blocks, Resource resource) {
        int quantity = 0;
        for (Period period: blocks.getPeriods()) {
            int maximumCount = blocks.getActivities(period).getResourceSubsets(resource).getMaximumCount(resource);
            if (maximumCount > quantity) {
                quantity = maximumCount;
            }
        }
        return quantity;
    }

    public static long detectMedianBlockLength(BlockList blocks) {
        int size = blocks.size();
        if (size == 0) {
            return Block.DEFAULT_LENGTH;
        }
        
        long[] lengths = new long[size];
        for (int i = 0; i < size; i++) {
            lengths[i] = blocks.get(i).getLength();
        }
        Arrays.sort(lengths);
        return lengths[size / 2];
    }
    
    public static int getShrinkedCount_NG(BlockList blocks, Resource resource) {
        PeriodSet periods = blocks.getPeriods();
        if (periods.isEmpty()) {
            return 1;
        }
        
        int shrinkedCount = 0;
        
        BlockList partialBlocks = new BlockList();
        for (Block block : blocks) {
            if (isPartial(block, resource, periods)) {
                partialBlocks.add(block);
            } else {
                shrinkedCount++;
            }
        }
        
        shrinkedCount += getShrinkedCount(partialBlocks);
        
        return shrinkedCount;
    }
    
    private static boolean isPartial(
        Block block, Resource resource, PeriodSet periods
    ) {
        Block.ActivityManager activityManager = block.getActivityManager();
        boolean isAllWhole = true;
        boolean isAllEmpty = true;
        for (Period period : periods) {
            ResourceSubsetList resourceSubsets =
                activityManager.getActivities(period).getResourceSubsets(resource)
            ;
            ResourceSubset union = new ResourceSubset.Union(resourceSubsets);
            boolean isWhole = union.isWhole();
            boolean isEmpty = union.isEmpty();
            if (!isWhole) {
                isAllWhole = false;
            }
            if (!isEmpty) {
                isAllEmpty = false;
            }
            if (!isWhole && !isEmpty) {
                break;
            }
        }
        return !isAllWhole && !isAllEmpty;
    }

    public static int getShrinkedCount(BlockList blocks) {
        
        // FIXME / TODO: create a partial maxsat instance to enforce maximal gravity
        System.err.println("Complex getShrinkedCount");
        return blocks.size();
        
    }
    
}

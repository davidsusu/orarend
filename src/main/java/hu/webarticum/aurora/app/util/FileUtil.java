package hu.webarticum.aurora.app.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;

import hu.webarticum.aurora.app.util.common.Pair;

public final class FileUtil {
    
    public static final char EXTENSION_SEPARATOR = '.';
    
    
    private FileUtil() {
        // utility class
    }
    
    
    public static String readContent(File file) throws IOException {
        return readContent(file, "UTF-8");
    }
    
    public static String readContent(File file, String charset) throws IOException {
        return readContent(new InputStreamReader(new FileInputStream(file), charset));
    }

    public static String readContent(URL url) throws IOException {
        return readContent(url, "UTF-8");
    }
    
    public static String readContent(URL url, String charset) throws IOException {
        return readContent(new InputStreamReader(url.openStream(), charset));
    }
    
    public static String readContent(Reader reader) throws IOException {
        BufferedReader bufferedReader = null;
        
        String ln = System.lineSeparator();
        
        try {
            bufferedReader = new BufferedReader(reader);
            String line;
            StringBuilder contentBuilder = new StringBuilder();
            boolean firstLine = true;
            
            while ((line = bufferedReader.readLine()) != null) {
                if (firstLine) {
                    firstLine = false;
                } else {
                    contentBuilder.append(ln);
                }
                contentBuilder.append(line);
            }
            return contentBuilder.toString();
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (reader != null) {
                reader.close();
            }
        }
        
    }
    
    public static void writeContent(File file, String content) throws IOException {
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);
            writer.write(content);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    public static String getExtension(String path) {
        String extension = "";

        int dotIndex = path.lastIndexOf(EXTENSION_SEPARATOR);
        int separatorIndex = path.lastIndexOf(File.separatorChar);
        int perIndex = path.lastIndexOf('/');
        
        if (perIndex > separatorIndex) {
            separatorIndex = perIndex;
        }
        
        if (dotIndex > separatorIndex) {
            extension = path.substring(dotIndex + 1);
        }
        
        return extension;
    }

    public static String getExtension(File file) {
        return getNameExtension(file.getName());
    }

    public static String getExtension(URL url) {
        return getExtension(url.getPath());
    }
    
    public static String getNameExtension(String name) {
        int dotIndex = name.lastIndexOf(EXTENSION_SEPARATOR);
        if (dotIndex >= 0) {
            return name.substring(dotIndex + 1);
        } else {
            return "";
        }
    }
    
    public static Pair<String, String> splitFromExtension(String path) {
        String basePath = path;
        String extension = "";

        int dotIndex = path.lastIndexOf(EXTENSION_SEPARATOR);
        int separatorIndex = path.lastIndexOf(File.separatorChar);
        
        if (dotIndex > separatorIndex) {
            basePath = path.substring(0, dotIndex);
            extension = path.substring(dotIndex + 1);
        }
        
        return new Pair<String, String>(basePath, extension);
    }

    public static Pair<File, String> ensureExtension(
        File file, String fallbackExtension, String[] allowedExtensions, boolean appendFallbackExtension
    ) {
        return ensureExtension(file, fallbackExtension, Arrays.asList(allowedExtensions), appendFallbackExtension);
    }
    
    public static Pair<File, String> ensureExtension(
        File file, String fallbackExtension, Collection<String> allowedExtensions, boolean appendFallbackExtension
    ) {
        String path = file.getAbsolutePath();
        
        String extension = getExtension(path);
        
        for (String allowedExtension: allowedExtensions) {
            if (extension.equals(allowedExtension)) {
                return new Pair<File, String>(file, allowedExtension);
            }
        }
        
        if (appendFallbackExtension) {
            String extendedPath = path + EXTENSION_SEPARATOR + fallbackExtension;
            return new Pair<File, String>(new File(extendedPath), fallbackExtension);
        } else {
            return new Pair<File, String>(file, fallbackExtension);
        }
    }
    
    public static File directoryFile(File directory, String filename) {
        return directory.toPath().resolve(filename).toFile();
    }
    
    public static File ensureUnique(File file) {
        return ensureUnique(file.getParentFile(), file.getName());
    }
    
    public static File ensureUnique(File directory, String filename) {
        Path directoryPath = directory.toPath();
        File file = directoryPath.resolve(filename).toFile();
        if (!file.exists()) {
            return file;
        }
        Pair<String, String> filenameExtensionPair = splitFromExtension(filename);
        String baseFilename = filenameExtensionPair.getLeft();
        String extension = filenameExtensionPair.getRight();
        boolean hasExtension = !extension.isEmpty();
        int i = 2;
        do {
            String uniqueFilename = baseFilename + "_" + i;
            if (hasExtension) {
                uniqueFilename += EXTENSION_SEPARATOR + extension;
            }
            file = directoryPath.resolve(uniqueFilename).toFile();
            i++;
        } while (file.exists());
        return file;
    }
    
    public static boolean isSymlink(File file) throws IOException {
        if (File.separatorChar == '\\') {
            return false;
        }
        
        File canonicalFile = file;
        if (file.getParent() != null) {
            canonicalFile = new File(file.getParentFile().getCanonicalFile(), file.getName());
        }
        
        if (canonicalFile.getCanonicalFile().equals(canonicalFile.getAbsoluteFile())) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean deleteRecursively(File file) {
        if (file.isDirectory()) {
            File directory = file;
            
            if (!directory.exists()) {
                return false;
            }
            
            boolean symlink;
            try {
                symlink = isSymlink(directory);
            } catch (IOException e) {
                return false;
            }
            
            if (!symlink) {
                File[] files = directory.listFiles();
                if (files == null) {
                    return false;
                }
                
                boolean result = true;
                for (File subFile: files) {
                    result = (deleteRecursively(subFile) && result);
                }
                if (!result) {
                    return false;
                }
            }
            
            return directory.delete();
        } else {
            return file.delete();
        }
    }
    
}

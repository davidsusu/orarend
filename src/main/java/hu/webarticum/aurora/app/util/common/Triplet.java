package hu.webarticum.aurora.app.util.common;

import java.io.Serializable;

public class Triplet<T, U, V> implements Serializable {

    private static final long serialVersionUID = 1L;

    private final T left;

    private final U middle;

    private final V right;
    
    public Triplet(T left, U middle, V right) {
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

    public T getLeft() {
        return left;
    }


    public U getMiddle() {
        return middle;
    }

    public V getRight() {
        return right;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Triplet) {
            Triplet<?, ?, ?> otherTriplet = (Triplet<?, ?, ?>)other;
            return (equal(otherTriplet.getLeft(), left) && equal(otherTriplet.getMiddle(), middle) && equal(otherTriplet.getRight(), right));
        } else {
            return false;
        }
    }

    private boolean equal(Object object1, Object object2) {
        if (object1==null) {
            return (object2==null);
        } else if (object2==null) {
            return false;
        } else {
            return object1.equals(object2);
        }
    }

    @Override
    public int hashCode() {
        return left.hashCode()+middle.hashCode()+right.hashCode();
    }

    @Override
    public String toString() {
        return "Triplet {"+left+", "+middle+", "+right+"}";
    }
    
}

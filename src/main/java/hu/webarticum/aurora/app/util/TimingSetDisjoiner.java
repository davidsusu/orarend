package hu.webarticum.aurora.app.util;

import java.util.ArrayList;
import java.util.List;

import hu.webarticum.aurora.app.util.common.IntervalUtil;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.TimingSet.TimeEntry;
import hu.webarticum.aurora.core.model.time.Interval;

public class TimingSetDisjoiner {

    private final TimingSet timingSet;
    
    
    public TimingSetDisjoiner(TimingSet timingSet) {
        this.timingSet = timingSet;
    }
    

    public TimingSet calculateMaximalDisjoint(long length) {
        TimingSet result = new TimingSet();
        if (timingSet.isEmpty()) {
            return result;
        }
        List<Interval> intervals = new ArrayList<Interval>();
        for (TimeEntry entry: timingSet) {
            intervals.add(new Interval(entry.getTime(), length));
        }
        intervals = IntervalUtil.getMaximalDisjoint(intervals);
        for (Interval interval: intervals) {
            for (TimeEntry entry: timingSet) {
                if (entry.getTime().equals(interval.getStart())) {
                    result.add(entry);
                }
            }
        }
        
        return result;
    }
    
}

package hu.webarticum.aurora.app.util.i18n;

import java.util.Map;
import java.util.TreeMap;

public class TreeMultiLanguageTextRepository implements MultiLanguageTextRepository {

    protected String language = "";
    
    protected Map<String, MultiLanguageTextRepository> mountedRepositoryMap = new TreeMap<String, MultiLanguageTextRepository>();

    @Override
    public String getText(String path) {
        return getText(path, language);
    }
    
    @Override
    public String getText(String path, String language) {
        TreeMap<Integer, MultiLanguageTextRepository> sortedSubRepositoryMap = new TreeMap<Integer, MultiLanguageTextRepository>();
        for (Map.Entry<String, MultiLanguageTextRepository> entry: mountedRepositoryMap.entrySet()) {
            String key = entry.getKey();
            boolean equals = key.equals(path);
            if (equals || path.startsWith(key+".")) {
                MultiLanguageTextRepository subRepository = entry.getValue();
                int length = key.length();
                sortedSubRepositoryMap.put(0-(equals?length:length+1), subRepository);
            }
        }
        for (Map.Entry<Integer, MultiLanguageTextRepository> entry: sortedSubRepositoryMap.entrySet()) {
            int length = 0-entry.getKey();
            MultiLanguageTextRepository subRepository = entry.getValue();
            String subPath = path.substring(length);
            String languageText = subRepository.getText(subPath, language);
            if (!languageText.isEmpty()) {
                return languageText;
            }
        }
        return "";
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public void setLanguage(String language) {
        this.language = language;
    }

    public void mount(String path, MultiLanguageTextRepository repository) {
        mountedRepositoryMap.put(path, repository);
    }

    public void unmount(String path) {
        mountedRepositoryMap.remove(path);
    }
    
}

package hu.webarticum.aurora.app.util.slots;

import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.time.Interval;

public class Slot implements Labeled {
    
    private static final long serialVersionUID = 1L;

    private Interval interval;
    
    private String label;
    
    public Slot(Interval interval, String label) {
        this.interval = interval;
        this.label = label;
    }
    
    public Interval getInterval() {
        return interval;
    }
    
    @Override
    public String getLabel() {
        return label;
    }
    
    @Override
    public int hashCode() {
        return interval.hashCode();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other instanceof Slot) {
            Slot otherSlot = (Slot)other;
            return (
                otherSlot.interval.equals(this.interval) &&
                otherSlot.label.equals(this.label)
            );
        } else {
            return false;
        }
    }
    
}

package hu.webarticum.aurora.app.util.i18n;

import java.util.Map;
import java.util.TreeMap;

public class SimpleMutableLanguageTextRepository implements LanguageTextRepository {

    protected final String language;
    
    protected final Map<String, String> textMap = new TreeMap<String, String>();
    
    public SimpleMutableLanguageTextRepository(String language) {
        this.language = language;
    }

    public void setText(String path, String text) {
        textMap.put(path, text);
    }
    
    @Override
    public String getText(String path) {
        if (textMap.containsKey(path)) {
            return textMap.get(path);
        } else {
            return "";
        }
    }

    @Override
    public String getLanguage() {
        return language;
    }

}

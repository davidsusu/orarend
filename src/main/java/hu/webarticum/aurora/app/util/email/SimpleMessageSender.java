package hu.webarticum.aurora.app.util.email;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

public class SimpleMessageSender {


    public static final int DEFAULT_PORT = 25;
    
    public static final int DEFAULT_PORT_SSL = 465;
    
    
    private String fromName = "";
    
    private String fromEmail = "";

    private String host = "";

    private boolean useSsl = false;
    
    private int port = 0;

    private boolean useAuthentication = false;
    
    private String user = "";

    private String password = "";
    
    
    public String getFromName() {
        return fromName;
    }
    
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
    
    public String getFromEmail() {
        return fromEmail;
    }
    
    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }
    
    public String getHost() {
        return host;
    }
    
    public void setHost(String host) {
        this.host = host;
    }
    
    public boolean isUseSsl() {
        return useSsl;
    }
    
    public void setUseSsl(boolean useSsl) {
        this.useSsl = useSsl;
    }
    
    public int getPort() {
        return port;
    }
    
    public void setPort(int port) {
        this.port = port;
    }
    
    public boolean isUseAuthentication() {
        return useAuthentication;
    }
    
    public void setUseAuthentication(boolean useAuthentication) {
        this.useAuthentication = useAuthentication;
    }
    
    public String getUser() {
        return user;
    }
    
    public void setUser(String user) {
        this.user = user;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public void test() throws IOException {
        Session session = createSession();
        
        try {
            try (Transport transport = session.getTransport("smtp")) {
                if (useAuthentication) {
                    transport.connect(user, password);
                } else {
                    transport.connect();
                }
                
                if (!transport.isConnected()) {
                    throw new IOException("No connection");
                }
            }
        } catch (MessagingException e) {
            throw new IOException("Mailing error: " + e.getMessage(), e);
        }
    }
    
    public void send(SimpleMessage simpleMessage) throws IOException {
        Session session = createSession();
        
        String from = fromName.isEmpty() ? fromEmail : fromName + " <" + fromEmail + ">";
        
        String toName = simpleMessage.getName();
        String toEmail = simpleMessage.getEmail();
        String to = toName.isEmpty() ? toEmail : toName + " <" + toEmail + ">";
        
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(
                Message.RecipientType.TO,
                InternetAddress.parse(to)
            );
            message.setSubject(simpleMessage.getSubject());
            
            List<Attachment> attachments = simpleMessage.getAttachments();
            if (attachments.isEmpty()) {
                message.setText(simpleMessage.getText());
            } else {
                Multipart multipart = new MimeMultipart();
                
                BodyPart contentPart = new MimeBodyPart();
                contentPart.setText(simpleMessage.getText());
                multipart.addBodyPart(contentPart);
                
                for (Attachment attachment: attachments) {
                    BodyPart attachmentPart = new MimeBodyPart();
                    DataSource source;
                    source = new ByteArrayDataSource(
                        attachment.getInputStream(),
                        attachment.getMimeType()
                    );
                    attachmentPart.setDataHandler(new DataHandler(source));
                    attachmentPart.setFileName(attachment.getFilename());
                    multipart.addBodyPart(attachmentPart);
                }
                
                message.setContent(multipart);
            }
            
            Transport.send(message);

        } catch (MessagingException e) {
            throw new IOException("Mailing error: " + e.getMessage(), e);
        }
    }
    
    private Session createSession() {
        String smtpUser = useAuthentication ? user : "";
        String smtpPassword = useAuthentication ? password : "";
        
        int smtpPort = port > 0 ? port : (useSsl ? DEFAULT_PORT_SSL : DEFAULT_PORT);
        
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", useAuthentication ? "true" : "false");
        props.put("mail.smtp.port", smtpPort);
        
        if (useSsl) {
            props.put("mail.smtp.socketFactory.port", smtpPort);
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }
        
        Session session = Session.getInstance(
            props,
            
            new javax.mail.Authenticator() {
                
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpUser, smtpPassword);
                }
                
            }
            
        );
        
        return session;
    }
    
}

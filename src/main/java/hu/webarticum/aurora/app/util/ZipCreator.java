package hu.webarticum.aurora.app.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipCreator {
    
    private final int bufferSize;
    
    private TreeMap<String, Object> entries = new TreeMap<String, Object>();
    
    public ZipCreator() {
        this(2048);
    }

    public ZipCreator(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public void add(String name, Object content) {
        entries.put(name, content);
    }

    
    // supported sources (other objects will be converted to String):
    
    public void add(String name, File source) {
        entries.put(name, (Object)source);
    }

    public void add(String name, InputStream source) {
        entries.put(name, (Object)source);
    }

    public void add(String name, String content) {
        entries.put(name, (Object)content);
    }

    public void add(String name, URL source) {
        entries.put(name, (Object)source);
    }

    
    public void save(File file) throws IOException {
        try (ZipOutputStream zipOut = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            for (Map.Entry<String, Object> entry: entries.entrySet()) {
                String name = entry.getKey();
                Object content = entry.getValue();
                
                ZipEntry zipEntry = new ZipEntry(name);
                zipOut.putNextEntry(zipEntry);
                
                InputStream entryInputStream;

                if (content instanceof File) {
                    entryInputStream = new FileInputStream((File)content);
                } else if (content instanceof InputStream) {
                    entryInputStream = (InputStream)content;
                } else if (content instanceof URL) {
                    entryInputStream = ((URL)content).openStream();
                } else {
                    entryInputStream = new ByteArrayInputStream(content.toString().getBytes("UTF-8"));
                }
                
                try (BufferedInputStream bufferedInputStream = new BufferedInputStream(entryInputStream, bufferSize)) {
                    byte[] buffer = new byte[bufferSize];
                    int readBufferSize;
                    
                    while ((readBufferSize = bufferedInputStream.read(buffer, 0, bufferSize)) != (-1)) {
                        zipOut.write(buffer, 0, readBufferSize);
                    }
                }
            }
        }
    }
    
}

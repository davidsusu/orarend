package hu.webarticum.aurora.app.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

public class IntervalFlowAligner {
    
    private Settings settings = new Settings();
    
    private AlignerStrategy strategy = STRATEGY.SIMPLE;
    
    public enum STRATEGY implements AlignerStrategy {
        
        SIMPLE {

            @Override
            public Result alignItems(Collection<Item> items, Settings settings) {
                List<Item> sortedItems = new ArrayList<Item>(items);
                Collections.sort(sortedItems, new ItemIntervalComparator());
                
                Result result = new Result();
                int width = Util.calculateRestrictedSize(settings.optimalItemWidth, settings.minimumWidth, settings.maximumWidth);
                
                for (Item item: sortedItems) {
                    result.itemMap.put(item, new Align(0, width));
                }
                
                result.width = width;
                return result;
            }
            
        },
        
        COMPLEX {

            @Override
            public Result alignItems(Collection<Item> items, Settings settings) {
                List<Item> sortedItems = new ArrayList<Item>(items);
                Collections.sort(sortedItems, new ItemIntervalComparator());
                
                Result result = new Result();
                
                int maximumLevelCount = 0;
                List<List<List<Item>>> crowds = new ArrayList<List<List<Item>>>();
                {
                    List<List<Item>> crowd = null;
                    for (Item item: sortedItems) {
                        if (crowd == null) {
                            crowd = new ArrayList<List<Item>>();
                            crowds.add(crowd);
                            List<Item> levelItems = new ArrayList<Item>();
                            crowd.add(levelItems);
                            levelItems.add(item);
                            maximumLevelCount = 1;
                        } else {
                            boolean added = false;
                            int levelCount = crowd.size();
                            for (int level = 0; level < levelCount; level++) {
                                List<Item> levelItems = crowd.get(level);
                                Item previousItem = levelItems.get(levelItems.size() - 1);
                                if (item.start >= previousItem.end) {
                                    if (level == 0) {
                                        crowd = new ArrayList<List<Item>>();
                                        crowds.add(crowd);
                                        levelItems = new ArrayList<Item>();
                                        crowd.add(levelItems);
                                    }
                                    levelItems.add(item);
                                    added = true;
                                    break;
                                }
                            }
                            if (!added) {
                                List<Item> levelItems = new ArrayList<Item>();
                                crowd.add(levelItems);
                                levelItems.add(item);
                                if (levelCount + 1 > maximumLevelCount) {
                                    maximumLevelCount = levelCount + 1;
                                }
                            }
                        }
                    }
                }
                
                int optimalWidth = settings.optimalItemWidth * maximumLevelCount;
                int width = Util.calculateRestrictedSize(optimalWidth, settings.minimumWidth, settings.maximumWidth);
                
                for (List<List<Item>> crowd: crowds) {
                    int levelCount = crowd.size();
                    int itemWidth = Math.max(1, ((width + settings.innerSpace) / levelCount) - settings.innerSpace);
                    for (int level = 0; level < levelCount; level++) {
                        List<Item> levelItems = crowd.get(level);
                        for (Item item: levelItems) {
                            result.itemMap.put(item, new Align((itemWidth + settings.innerSpace) * level, itemWidth));
                        }
                    }
                }
                
                result.width = width;
                return result;
            }
            
        }
        
    }
    
    public void setStrategy(AlignerStrategy strategy) {
        this.strategy = strategy;
    }
    
    public void setOptimalItemWidth(int optimalItemWidth) {
        this.settings.optimalItemWidth = optimalItemWidth;
    }
    
    public void setMinimumWidth(int minimumWidth) {
        this.settings.minimumWidth = minimumWidth;
    }

    public void setMaximumWidth(int maximumWidth) {
        this.settings.maximumWidth = maximumWidth;
    }

    public void setWidth(int width) {
        this.settings.minimumWidth = width;
        this.settings.maximumWidth = width;
    }
    
    public void setInnerSpace(int innerSpace) {
        this.settings.innerSpace = innerSpace;
    }
    
    public Result alignItems(Collection<Item> items) {
        return strategy.alignItems(items, settings);
    }
    
    public static class Settings {

        public int optimalItemWidth = 0;
        
        public int minimumWidth = 0;

        public int maximumWidth = 0;
        
        public int innerSpace = 5;
        
    }
    
    public static class Item {
        
        public final long start;
        
        public final long end;
        
        public Item(long start, long end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return "{" + start + "; " + end+"}";
        }

    }
    
    public static class Result {
        
        public int width;
        
        // TreeMap(ItemIntervalComparator) ?
        public LinkedHashMap<Item, Align> itemMap = new LinkedHashMap<Item, Align>();

    }
    
    public static class Align {
        
        public int left = 0;
        
        public int width = 1;

        public Align() {
        }

        public Align(int left, int width) {
            this.left = left;
            this.width = width;
        }
        
        public int getRight() {
            return left + width;
        }
        
        @Override
        public String toString() {
            return "{ " + left + " + " + width+" }";
        }

    }
    
    public interface AlignerStrategy {
        
        public Result alignItems(Collection<Item> items, Settings settings);
        
    }
    
    public static class ItemIntervalComparator implements Comparator<Item> {

        @Override
        public int compare(Item item1, Item item2) {
            int result = Long.compare(item1.start, item2.start);
            if (result == 0) {
                result = Long.compare(item1.end, item2.end);
            }
            return result;
        }
        
    }
    
    public static class Util {
        
        public static int calculateRestrictedSize(int optimal, int minimum, int maximum) {
            optimal = Math.max(0, optimal);
            minimum = Math.max(0, minimum);
            maximum = Math.max(0, maximum);
            if (optimal == 0 && minimum == 0 && maximum == 0) {
                return 1;
            }
            if (maximum > 0 && maximum <= minimum) {
                maximum = minimum;
            }
            if (minimum == 0 && maximum == 0) {
                return optimal;
            } else if (minimum == 0) {
                if (optimal > 0) {
                    return Math.min(maximum, optimal);
                } else {
                    return 1;
                }
            } else if (maximum == 0) {
                if (optimal > 0) {
                    return Math.max(minimum, optimal);
                } else {
                    return minimum;
                }
            } else if (optimal < maximum) {
                return Math.max(minimum, optimal);
            } else {
                return maximum;
            }
        }
        
        public static int avg(int number1, int number2) {
            long sum = number1 + number2;
            return (int)(sum / 2);
        }
        
    }
    
}

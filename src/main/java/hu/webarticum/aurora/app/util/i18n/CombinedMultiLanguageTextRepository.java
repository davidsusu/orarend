package hu.webarticum.aurora.app.util.i18n;

import java.util.Map;
import java.util.TreeMap;

public class CombinedMultiLanguageTextRepository implements MultiLanguageTextRepository {

    protected String language = "";
    
    protected Map<String, LanguageTextRepository> repositoryMap = new TreeMap<String, LanguageTextRepository>();
    
    @Override
    public String getText(String path) {
        return getText(path, language);
    }

    @Override
    public String getText(String path, String language) {
        if (repositoryMap.containsKey(language)) {
            return repositoryMap.get(language).getText(path);
        } else {
            return "";
        }
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public void setLanguage(String language) {
        this.language = language;
    }

    public void put(LanguageTextRepository repository) {
        put(repository, repository.getLanguage());
    }

    public void put(LanguageTextRepository repository, String language) {
        repositoryMap.put(language, repository);
    }
    
}

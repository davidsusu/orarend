package hu.webarticum.aurora.app.util.email;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;

public class ImageAttachment implements Attachment {

    private final Image image;
    
    private String filename;
    
    private ImageType imageType;
    
    private String description;
    
    public enum ImageType {AUTO, PNG, JPEG};
    
    public ImageAttachment(Image image, String filename) {
        this(image, filename, ImageType.AUTO);
    }

    public ImageAttachment(Image image, String filename, ImageType imageType) {
        this(image, filename, imageType, filename);
    }
    
    public ImageAttachment(Image image, String filename, ImageType imageType, String description) {
        this.image = image;
        this.filename = filename;
        this.imageType = imageType;
        this.description = description;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Image getImage() {
        return image;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    @Override
    public String getMimeType() {
        switch (imageType) {
            case PNG:
                return "image/png";
            case JPEG:
                return "image/jpeg";
            case AUTO:
            default:
                String contentType = new MimetypesFileTypeMap().getContentType(filename);
                boolean isImageDetected = (contentType != null && contentType.startsWith("image/"));
                return isImageDetected ? contentType : "image/png";
        }
    }

    @Override
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        RenderedImage renderedImage;
        if (image instanceof RenderedImage) {
            renderedImage = (RenderedImage)image;
        } else {
            BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics = bufferedImage.createGraphics();
            graphics.drawImage(image, 0, 0, null);
            graphics.dispose();
            renderedImage = bufferedImage;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(renderedImage, getMimeType().replaceFirst("^image/", ""), outputStream);
        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    @Override
    public String toString() {
        return getDescription();
    }

}
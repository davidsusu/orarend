package hu.webarticum.aurora.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;


public class TraditionalTable extends TreeMap<Long, TreeMap<Integer, Board.EntryList>> {
    
    private static final long serialVersionUID = 1L;
    
    public TraditionalTable() {
        super();
    }
    
    public TraditionalTable(TreeMap<Long, TreeMap<Integer, Board.EntryList>> otherMap) {
        super();
        for (Map.Entry<Long, TreeMap<Integer, Board.EntryList>> entry: otherMap.entrySet()) {
            TreeMap<Integer, Board.EntryList> dayMapCopy = new TreeMap<Integer, Board.EntryList>();
            dayMapCopy.putAll(entry.getValue());
            put(entry.getKey(), dayMapCopy);
        }
    }
    
    public TraditionalTable(Board board) {
        super(FromBoardBuilder.create(board));
    }
    
    public long getDayCount() {
        if (isEmpty()) {
            return 0;
        } else {
            return lastKey() + 1;
        }
    }
    
    private static class FromBoardBuilder {
        
        private Board board;
        
        private Map<Aspect, TimingSet> aspectTimingSetMap = new HashMap<Aspect, TimingSet>();
        
        private FromBoardBuilder(Board board) {
            this.board = board;
        }
        
        static TreeMap<Long, TreeMap<Integer, Board.EntryList>> create(Board board) {
            return (new FromBoardBuilder(board)).build();
        }
        
        private TreeMap<Long, TreeMap<Integer, Board.EntryList>> build() {
            TreeMap<Long, TreeMap<Integer, Board.EntryList>> result = new TreeMap<Long, TreeMap<Integer, Board.EntryList>>();
            for (Board.Entry boardEntry: board) {
                Time time = boardEntry.getTime();
                Block block = boardEntry.getBlock();
                TimingSet blockTimingSet = getBlockTimingSet(block);
                
                Pair<Long, Integer> calculatedDaySlotPair = calculateDaySlotPair(blockTimingSet, time);
                long calculatedDay = calculatedDaySlotPair.getLeft();
                int calculatedSlot = calculatedDaySlotPair.getRight();
                
                TreeMap<Integer, Board.EntryList> dayData;
                if (result.containsKey(calculatedDay)) {
                    dayData = result.get(calculatedDay);
                } else {
                    dayData = new TreeMap<Integer, Board.EntryList>();
                    result.put(calculatedDay, dayData);
                }
                
                Board.EntryList slotEntries;
                if (dayData.containsKey(calculatedSlot)) {
                    slotEntries = dayData.get(calculatedSlot);
                } else {
                    slotEntries = new Board.EntryList();
                    dayData.put(calculatedSlot, slotEntries);
                }
                
                slotEntries.add(boardEntry);
            }
            return result;
        }

        private TimingSet getBlockTimingSet(Block block) {
            List<Aspect> aspects = new ArrayList<Aspect>();
            ActivityList activities = block.getActivityManager().getActivities();
            aspects.addAll(activities.getResources());
            aspects.addAll(activities.getTags());
            TimingSet longestTimingSet = new TimingSet();
            for (Aspect aspect: aspects) {
                TimingSet aspectTimingSet = getAspectTimingSet(aspect);
                if (aspectTimingSet.size() > longestTimingSet.size()) {
                    longestTimingSet = aspectTimingSet;
                }
            }
            return longestTimingSet;
        }
        
        private TimingSet getAspectTimingSet(Aspect aspect) {
            if (!aspectTimingSetMap.containsKey(aspect)) {
                TimingSet rawTimingSet;
                
                Aspect.TimingSetManager timingSetManager = aspect.getTimingSetManager();
                TimingSet defaultTimingSet = timingSetManager.getDefaultTimingSet();
                if (defaultTimingSet != null && !defaultTimingSet.isEmpty()) {
                    rawTimingSet = defaultTimingSet;
                } else {
                    TimingSet longestPeriodTimingSet = new TimingSet();
                    for (TimingSet periodTimingSet: timingSetManager.getPeriodTimingSets().values()) {
                        if (periodTimingSet != null && periodTimingSet.size() > longestPeriodTimingSet.size()) {
                            longestPeriodTimingSet = periodTimingSet;
                        }
                    }
                    rawTimingSet = longestPeriodTimingSet;
                }
                
                TimingSetDisjoiner disjoiner = new TimingSetDisjoiner(rawTimingSet);
                long blockLength = BlockUtil.detectMedianBlockLength(getAspectBlocks(aspect));
                TimingSet normalizedTimingSet = disjoiner.calculateMaximalDisjoint(blockLength);
                aspectTimingSetMap.put(aspect, normalizedTimingSet);
            }
            return aspectTimingSetMap.get(aspect);
        }
        
        private BlockList getAspectBlocks(Aspect aspect) {
            return board.getBlocks()
                .filter(new BlockFilter.ActivityBlockFilter(
                    new ActivityFilter.HasAspect(aspect)
                ))
            ;
        }
        
        private Pair<Long, Integer> calculateDaySlotPair(TimingSet timingSet, Time startTime) {
            long startSeconds = startTime.getSeconds();
            long day = startTime.getPart(Time.PART.FULLDAYS);
            Time dayTime = new Time(day * Time.DAY);
            TimingSet dayTimingSet = timingSet.getLimited(new Interval(dayTime, Time.DAY));
            TreeSet<Time> dayTimes = dayTimingSet.getTimes();
            List<Time> dayTimeList = new ArrayList<Time>(dayTimes);
            if (dayTimes.isEmpty()) {
                return new Pair<Long, Integer>(day, 0);
            } else {
                Time floorTime = dayTimes.floor(startTime);
                Time ceilingTime = dayTimes.ceiling(startTime);
                
                Time nearestTime;
                if (floorTime == null) {
                    nearestTime = ceilingTime;
                } else if (ceilingTime == null) {
                    nearestTime = floorTime;
                } else if (startSeconds - floorTime.getSeconds() < ceilingTime.getSeconds() - startSeconds) {
                    nearestTime = floorTime;
                } else {
                    nearestTime = ceilingTime;
                }
                
                int slot = dayTimeList.indexOf(nearestTime);
                
                return new Pair<Long, Integer>((long)day, slot);
            }
        }
        
    }
    
}

package hu.webarticum.aurora.app.util.i18n;

public interface LanguageTextRepository {

    public String getText(String path);
    
    public String getLanguage();
    
}

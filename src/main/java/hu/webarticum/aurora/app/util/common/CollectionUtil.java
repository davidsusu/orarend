package hu.webarticum.aurora.app.util.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CollectionUtil {

    public static <T> T getFirst(Collection<T> collection) {
        return getNth(collection, 0);
    }

    public static <T> T getNth(Collection<T> collection, int nth) {
        if (collection==null || collection.isEmpty()) {
            return null;
        }
        int i = 0;
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            T item = iterator.next();
            if (i==nth) {
                return item;
            }
            i++;
        }
        return null;
    }

    public static <T> void removeFirst(Collection<T> collection) {
        removeNth(collection, 0);
    }

    public static <T> void removeNth(Collection<T> collection, int nth) {
        if (collection==null || collection.isEmpty()) {
            return;
        }
        int i = 0;
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            if (i==nth) {
                iterator.remove();
                return;
            }
            i++;
        }
    }
    
    public static <T, U> List<Pair<T, U>> getPairListFromMap(Map<T, U> map) {
        List<Pair<T, U>> pairList = new ArrayList<Pair<T, U>>();
        for (Map.Entry<T, U> entry: map.entrySet()) {
            pairList.add(new Pair<T, U>(entry.getKey(), entry.getValue()));
        }
        return pairList;
    }
    
    public static <T> void moveElements(Collection<T> collection1, Collection<T> collection2, int moveCount) {
        Iterator<T> iterator = collection1.iterator();
        for (int i=0; i<moveCount&&iterator.hasNext(); i++) {
            T element = iterator.next();
            iterator.remove();
            collection2.add(element);
        }
    }
    
    public static <T> T containsOrFallback(T item, Collection<T> collection, T fallback) {
        if (collection.contains(item)) {
            return item;
        } else {
            return fallback;
        }
    }
    
    public static boolean containSame(List<?> list1, List<?> list2) {
        return toValueMap(list1).equals(toValueMap(list2));
    }
    
    public static <T> Map<T, Integer> toValueMap(List<T> list) {
        Map<T, Integer> result = new HashMap<T, Integer>();
        for (T item: list) {
            if (result.containsKey(item)) {
                result.put(item, result.get(item) + 1);
            } else {
                result.put(item, 1);
            }
        }
        return result;
    }
    
}

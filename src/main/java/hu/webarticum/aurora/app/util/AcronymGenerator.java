package hu.webarticum.aurora.app.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AcronymGenerator {

    public static String generateAcronym(String text) {
        String regex = "((\\p{L}\\p{M}*)+|\\p{Nd}+)";
        if (text.matches(regex)) {
            if (text.length()>3) {
                return text.substring(0, 3);
            } else {
                return text;
            }
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        StringBuilder acronymBuilder = new StringBuilder();
        while (matcher.find()) {
            String token = matcher.group();
            if (!token.isEmpty()) {
                char firstChar = token.charAt(0);
                if (Character.isDigit(firstChar)) {
                    acronymBuilder.append(token);
                } else {
                    acronymBuilder.append(firstChar);
                }
            }
        }
        return acronymBuilder.toString();
    }
    
}

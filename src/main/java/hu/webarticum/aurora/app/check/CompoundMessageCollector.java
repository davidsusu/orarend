package hu.webarticum.aurora.app.check;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CompoundMessageCollector extends AbstractMessageCollector {

    private final List<MessageCollector> collectors;
    
    
    public CompoundMessageCollector(MessageCollector... collectors) {
        this(Arrays.asList(collectors));
    }

    public CompoundMessageCollector(Collection<MessageCollector> collectors) {
        this.collectors = new ArrayList<MessageCollector>(collectors);
        for (MessageCollector collector : collectors) {
            collector.addMessageListener(new MessageCollector.MessageListener() {
                
                @Override
                public void onMessage(Message message) {
                    fireListeners(message);
                }
                
            });
        }
    }
    
    
    @Override
    protected void _collect() {
        for (MessageCollector collector : collectors) {
            messages.addAll(collector.collect());
        }
    }

}

package hu.webarticum.aurora.app.check;

import java.util.List;

public interface MessageCollector {
    
    public List<Message> collect();
    
    public void addMessageListener(MessageListener messageListener);
    
    
    public interface MessageListener {
        
        public void onMessage(Message message);
        
    }
}

package hu.webarticum.aurora.app.check;

public class SolutionHint {

    public enum BasicType implements Type {
        EDIT_BLOCK
    }
    
    
    private final Type type;
    
    private final Object data;
    
    
    public SolutionHint(Type type, Object data) {
        this.type = type;
        this.data = data;
    }
    

    public Type getType() {
        return type;
    }

    public Object getData() {
        return data;
    }
    
    
    public interface Type {
        
    }
    
}

package hu.webarticum.aurora.app.check;

public class Message {
    
    public enum Type { ERROR, WARNING, NOTICE }
    
    
    private final Type type;

    private final Object title;

    private final Object description;
    
    private final SolutionHint solutionHint;
    

    public Message(Type type, Object title) {
        this(type, title, "");
    }

    public Message(Type type, Object title, Object description) {
        this(type, title, description, null);
    }
    
    public Message(Type type, Object title, Object description, SolutionHint solutionHint) {
        this.type = type;
        this.title = title;
        this.description = description;
        this.solutionHint = solutionHint;
    }


    public Type getType() {
        return type;
    }

    public Object getTitle() {
        return title;
    }

    public Object getDescription() {
        return description;
    }

    public SolutionHint getSolutionHint() {
        return solutionHint;
    }
    
}

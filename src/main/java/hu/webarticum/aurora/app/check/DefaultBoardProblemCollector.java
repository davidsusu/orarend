package hu.webarticum.aurora.app.check;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.BlockIntersectionTable;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.ResourceSubsetList;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.core.model.time.TimeLimit;

public class DefaultBoardProblemCollector extends AbstractMessageCollector {

    private final List<Board> boards;
    

    public DefaultBoardProblemCollector(Board board) {
        this(Arrays.asList(board));
    }
    
    public DefaultBoardProblemCollector(Collection<Board> boards) {
        this.boards = new ArrayList<Board>(boards);
    }
    
    
    @Override
    protected void _collect() {
        for (final Board board: boards) {
            analyzeBoard(board);
        }
    }

    private void analyzeBoard(Board board) {
        BlockIntersectionTable conflictTable = board.getConflictTable();
        
        String prefix = boards.size() > 1 ? "\"" + board.getLabel() + "\": " : "";
        
        for (BlockIntersectionTable.Entry entry: conflictTable) {
        	Interval interval = entry.getInterval();
        	Time start = interval.getStart();
        	String startText = start.toReadableString();
        	Time end = interval.getEnd();
        	String endText = start.getDateOnly().equals(end.getDateOnly()) ? end.toTimeString() : end.toReadableString();
            String title = prefix + String.format(text("app.check.defaultboardproblemcollector.conflict"), startText, endText);
            StringBuilder descriptionBuilder = new StringBuilder();
            BlockList blocks = entry.getBlocks();
            ResourceSubsetList resourceSubsets = blocks.getActivities().getResourceSubsets();
            for (Resource resource : resourceSubsets.getResources()) {
            	int available = resource.getQuantity();
            	int used = resourceSubsets.getMaximumCount(resource);
            	if (used > available) {
                    descriptionBuilder.append(String.format(text(
                    		"app.check.defaultboardproblemcollector.overused.resource"), resource.getLabel(), used, available));
                    BlockList resourceBlocks = blocks.filter(new BlockFilter.ActivityBlockFilter(new ActivityFilter.HasResource(resource)));
            		for (Block block : resourceBlocks) {
            			descriptionBuilder.append("\n    " + block.getLabel());
            		}
            	}
            }
            String description = descriptionBuilder.toString();
            publish(Message.Type.ERROR, title, description);
        }
        
        for (Board.Entry entry: board) {
            final Block block = entry.getBlock();
            Time time = entry.getTime();
            TimeLimit timeLimit = block.getCalculatedTimeLimit();
            TimingSet timingSet = block.getCalculatedTimingSet(false);
            if (!timeLimit.contains(time)) {
                String title = prefix + text("app.check.defaultboardproblemcollector.out_of_timelimit.title") + ": " + block.getLabel();
                String description = String.format(text("app.check.defaultboardproblemcollector.out_of_timelimit.message"), time.toReadableString());
                publish(Message.Type.WARNING, title, description, SolutionHint.BasicType.EDIT_BLOCK, block);
            }
            if (!timingSet.containsTime(time)) {
                String title = prefix + text("app.check.defaultboardproblemcollector.out_of_timingset.title") + ": " + block.getLabel();
                String description = String.format(text("app.check.defaultboardproblemcollector.out_of_timingset.message"), time.toReadableString());
                publish(Message.Type.NOTICE, title, description);
            }
        }
    }
        
}

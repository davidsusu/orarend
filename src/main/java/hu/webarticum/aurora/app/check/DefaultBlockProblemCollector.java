package hu.webarticum.aurora.app.check;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.Arrays;
import java.util.Collection;

import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Block.ActivityManager;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.TimingSet;

public class DefaultBlockProblemCollector extends AbstractMessageCollector {

    private final BlockList blocks;

    private final Document document;
    

    public DefaultBlockProblemCollector(Block block, Document document) {
        this(Arrays.asList(block), document);
    }
    
    public DefaultBlockProblemCollector(Collection<Block> blocks, Document document) {
        this.blocks = new BlockList(blocks);
        this.document = document;
    }
    
    
    @Override
    protected void _collect() {
        for (final Block block: blocks) {
            analyzeBlock(block);
        }
    }

    private void analyzeBlock(Block block) {
        detectActivitiesWithNoPeriod(block);
        detectConflicts(block);
        checkPlaces(block);
    }
    
    private void detectActivitiesWithNoPeriod(Block block) {
        Block.ActivityManager activityManager = block.getActivityManager();
        for (ActivityManager.ActivityEntry entry: activityManager) {
            Activity activity = entry.getActivity();
            if (entry.getPeriods().isEmpty()) {
                publish(
                    Message.Type.NOTICE,
                    String.format("%s: %s", text("app.check.defaultblockproblemcollector.no_activity.title"), block.getLabel()),
                    String.format(text("app.check.defaultblockproblemcollector.no_activity.message"), activity.getLabel()),
                    SolutionHint.BasicType.EDIT_BLOCK, block
                );
            }
        }
    }
    
    private void detectConflicts(Block block) {
        Block.ActivityManager activityManager = block.getActivityManager();
        for (Period period: activityManager.getPeriods()) {
            if (block.hasConflicts(period)) {
                publish(
                    Message.Type.ERROR,
                    String.format("%s: %s", text("app.check.defaultblockproblemcollector.conflict.title"), block.getLabel()),
                    String.format(text("app.check.defaultblockproblemcollector.conflict.message"), period.getLabel()),
                    SolutionHint.BasicType.EDIT_BLOCK, block
                );
            }
        }
    }
    
    private void checkPlaces(Block block) {
        TimingSet runtimeTimingSet = block.getCalculatedTimingSet();
        TimingSet unlimitedTimingSet = block.getCalculatedTimingSet(false);
        if (runtimeTimingSet.isEmpty()) {
            String description;
            if (unlimitedTimingSet.isEmpty()) {
                description = text("app.check.defaultblockproblemcollector.empty_timingset.message.originally");
            } else {
                description = String.format(text("app.check.defaultblockproblemcollector.empty_timingset.message.with_timelimits"), unlimitedTimingSet.size());
            }
            publish(
                Message.Type.ERROR,
                String.format("%s: %s", text("app.check.defaultblockproblemcollector.empty_timingset.title"), block.getLabel()),
                description,
                SolutionHint.BasicType.EDIT_BLOCK, block
            );
        } else if (runtimeTimingSet.size() == 1) {
            publish(
                Message.Type.WARNING,
                String.format("%s: %s", text("app.check.defaultblockproblemcollector.only_place.title"), block.getLabel()),
                String.format(text("app.check.defaultblockproblemcollector.only_place.message"), runtimeTimingSet.first().toReadableString(), unlimitedTimingSet.size()),
                SolutionHint.BasicType.EDIT_BLOCK, block
            );
        }
    }
    
}

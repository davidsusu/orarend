package hu.webarticum.aurora.app.check;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public abstract class AbstractMessageCollector implements MessageCollector {
    
    private final ThreadPoolExecutor executor =
        new ThreadPoolExecutor(0, 1, 1, TimeUnit.SECONDS, new LinkedBlockingDeque<>())
    ;
    
    private final List<MessageListener> listeners = new ArrayList<MessageListener>(1);
    
    protected final List<Message> messages = Collections.synchronizedList(new ArrayList<>());
    
    
    @Override
    public void addMessageListener(MessageListener listener) {
        this.listeners.add(listener);
    }
    
    @Override
    public List<Message> collect() {
        _collect();
        return new ArrayList<>(messages);
    }

    protected void publish(Message.Type type, Object title, Object description) {
        messages.add(new Message(type, title, description));
    }

    protected void publish(
        Message.Type type, Object title, Object description,
        SolutionHint.Type solutionHintType, Object solutionHintData
    ) {
        messages.add(new Message(
            type, title, description,
            new SolutionHint(solutionHintType, solutionHintData)
        ));
    }
    
    protected void publish(final Message message) {
        messages.add(message);
        fireListeners(message);
    }

    protected void fireListeners(final Message message) {
        if (!listeners.isEmpty()) {
            executor.execute(new Runnable() {
                
                @Override
                public void run() {
                    for (MessageListener listener : listeners) {
                        listener.onMessage(message);
                    }
                }
                
            });
        }
    }
    
    protected abstract void _collect();
    
}

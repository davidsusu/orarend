package hu.webarticum.aurora.app.check;

import java.util.function.Predicate;

public class FilteringMessageCollector extends AbstractMessageCollector {

    private final MessageCollector collector;
    
    
    public static final Predicate<Message> ERROR_FILTER = new Predicate<Message>() {
        
        @Override
        public boolean test(Message message) {
            return message.getType() == Message.Type.ERROR;
        }
        
    };
    
    
    public FilteringMessageCollector(MessageCollector collector, final Predicate<Message> filter) {
        this.collector = collector;
        collector.addMessageListener(new MessageCollector.MessageListener() {
            
            @Override
            public void onMessage(Message message) {
                if (filter.test(message)) {
                    fireListeners(message);
                }
            }
            
        });
    }
    
    
    @Override
    protected void _collect() {
        messages.addAll(collector.collect());
    }

}

package hu.webarticum.aurora.app.check;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hu.webarticum.aurora.app.util.BlockUtil;
import hu.webarticum.aurora.app.util.common.IntervalUtil;
import hu.webarticum.aurora.core.model.ActivityFilter;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockFilter;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;

public class DefaultBlockListProblemCollector extends AbstractMessageCollector {

    private final BlockList blocks;
    
    private final Document document;
    
    private final Board board;
    

    public DefaultBlockListProblemCollector(Collection<Block> blocks, Document document) {
        this(blocks, document, null);
    }
    
    public DefaultBlockListProblemCollector(Collection<Block> blocks, Document document, Board board) {
        this.blocks = new BlockList(blocks);
        this.document = document;
        this.board = board;
    }
    
    
    @Override
    protected void _collect() {
        BlockList placedBlocks = board != null ? board.getBlocks() : new BlockList();
        
        Map<Block, TimingSet> blockTimingSetMap = new HashMap<Block, TimingSet>();
        Map<Period, Map<Resource, BlockList>> periodResourceBlocksMap = new HashMap<Period, Map<Resource, BlockList>>();
        
        for (Block block: blocks) {
            TimingSet blockTimingSet = block.getCalculatedTimingSet();
            TimingSet blockTableTimingSet;
            
            if (board == null) {
                blockTableTimingSet = blockTimingSet;
            } else if (!board.contains(block)) {
                blockTableTimingSet = block.getCalculatedTimingSet(board);
                if (blockTableTimingSet.isEmpty() && !blockTimingSet.isEmpty()) {
                    String title = text("app.check.defaultblocklistproblemcollector.block_conflicts_board.title") + ": " + block.getLabel();
                    StringBuilder descriptionBuilder = new StringBuilder();
                    descriptionBuilder.append(text("app.check.defaultblocklistproblemcollector.block_conflicts_board.message") + ":\n");
                    for (TimingSet.TimeEntry entry: blockTimingSet) {
                        descriptionBuilder.append("\n" + entry.getTime());
                        String timeEntryLabel = entry.getLabel();
                        if (!timeEntryLabel.isEmpty()) {
                            descriptionBuilder.append(": " + timeEntryLabel);
                        }
                    }
                    String description = descriptionBuilder.toString();
                    publish(Message.Type.ERROR, title, description);
                }
            } else {
                continue;
            }
            blockTimingSetMap.put(block, blockTableTimingSet);
            Block.ActivityManager activityManager = block.getActivityManager();
            for (Period period: activityManager.getPeriods()) {
                Map<Resource, BlockList> resourceBlocksMap;
                if (periodResourceBlocksMap.containsKey(period)) {
                    resourceBlocksMap = periodResourceBlocksMap.get(period);
                } else {
                    resourceBlocksMap = new HashMap<Resource, BlockList>();
                    periodResourceBlocksMap.put(period, resourceBlocksMap);
                }
                for (Resource resource: activityManager.getActivities(period).getResources()) {
                    BlockList resourceBlocks;
                    if (resourceBlocksMap.containsKey(resource)) {
                        resourceBlocks = resourceBlocksMap.get(resource);
                    } else {
                        resourceBlocks = new BlockList();
                        resourceBlocksMap.put(resource, resourceBlocks);
                    }
                    resourceBlocks.add(block);
                }
            }
        }
        
        for (Map.Entry<Period, Map<Resource, BlockList>> periodResourceBlocksEntry: periodResourceBlocksMap.entrySet()) {
            Period period = periodResourceBlocksEntry.getKey();
            Map<Resource, BlockList> resourceBlocksMap = periodResourceBlocksEntry.getValue();
            for (Map.Entry<Resource, BlockList> resourceBlocksEntry: resourceBlocksMap.entrySet()) {
                Resource resource = resourceBlocksEntry.getKey();
                BlockList resourceBlocks = resourceBlocksEntry.getValue();

                List<Interval> intervals = new ArrayList<Interval>();
                for (Block block: resourceBlocks) {
                    for (TimingSet.TimeEntry entry: blockTimingSetMap.get(block)) {
                        Time time = entry.getTime();
                        intervals.add(new Interval(time, block.getLength()));
                    }
                }
                int maximumPlaceCount = IntervalUtil.getMaximalDisjoint(intervals).size();
                int resourceQuantity = resource.getQuantity();
                int maximumCount = resourceBlocks.getActivities(period).getResourceSubsets(resource).getMaximumCount(resource);
                int placedCount = placedBlocks.getActivities(period).getResourceSubsets(resource).getMaximumCount(resource);
                int unplacedCount = maximumCount - placedCount;
                
                if ((unplacedCount / (double)resourceQuantity) > maximumPlaceCount) {
                    String title = "AAA " + text("app.check.defaultblocklistproblemcollector.lack_of_space.title") + ": " + resource.getLabel()+", "+period.getLabel();
                    String description = "BBB " + text("app.check.defaultblocklistproblemcollector.lack_of_space.message");
                    publish(Message.Type.ERROR, title, description);
                }
            }
        }

        for (Resource resource: blocks.getActivities().getResources(Resource.Type.CLASS)) {
            BlockList classBlocks = blocks.filter(new BlockFilter.ActivityBlockFilter(new ActivityFilter.HasResource(resource)));
            Map<Set<Time>, BlockList> blocksByTimingSet = new HashMap<Set<Time>, BlockList>();
            for (Block block: classBlocks) {
                TimingSet timingSet = block.getCalculatedTimingSet();
                Set<Time> times = timingSet.getTimes();
                BlockList timingSetBlocks;
                if (blocksByTimingSet.containsKey(times)) {
                    timingSetBlocks = blocksByTimingSet.get(times);
                } else {
                    timingSetBlocks = new BlockList();
                    blocksByTimingSet.put(times, timingSetBlocks);
                }
                timingSetBlocks.add(block);
            }
            if (resource.isTimingSetEnabled()) {
                Set<Time> globalTimes = null;
                for (Period period : document.getPeriodStore()) {
                    Set<Time> periodTimes =
                        resource.getTimingSetManager().getCalculatedPeriodTimingSet(period).getTimes()
                    ;
                    if (globalTimes == null) {
                        globalTimes = periodTimes;
                    } else {
                        globalTimes.retainAll(periodTimes);
                    }
                }

                blocksByTimingSet.put(globalTimes, classBlocks);
            }
            for (Map.Entry<Set<Time>, BlockList> entry: blocksByTimingSet.entrySet()) {
                BlockList timingSetBlocks = entry.getValue();

                List<Interval> intervals = new ArrayList<Interval>();
                for (Block block: timingSetBlocks) {
                    for (TimingSet.TimeEntry timeEntry: block.getCalculatedTimingSet()) {
                        Time time = timeEntry.getTime();
                        intervals.add(new Interval(time, block.getLength()));
                    }
                }
                int shrinkedCount = BlockUtil.getShrinkedCount(timingSetBlocks, resource);
                int maximumPlaceCount = IntervalUtil.getMaximalDisjoint(intervals).size();

                if (maximumPlaceCount < shrinkedCount) {
                    StringBuilder descriptionBuilder = new StringBuilder();
                    descriptionBuilder.append(String.format(
                        text("app.check.defaultblocklistproblemcollector.lack_of_space_timingset.message"),
                        timingSetBlocks.size(),
                        shrinkedCount,
                        maximumPlaceCount
                    ));
                    descriptionBuilder.append("\n");
                    for (Block block: timingSetBlocks) {
                        descriptionBuilder.append("\n" + block.getLabel());
                    }
                    String errorTitle = String.format(text("app.check.defaultblocklistproblemcollector.lack_of_space_timingset.title"), timingSetBlocks.size());
                    String description = descriptionBuilder.toString();
                    publish(Message.Type.ERROR, errorTitle, description);
                }
            }
            
        }
    }
    
}

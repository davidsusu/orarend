package hu.webarticum.aurora.app.event;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class EventManager {
    
    private Map<String, List<Entry>> typeEntryMap = new HashMap<String, List<Entry>>();
    
    public void register(String typeName, Listener listener) {
        register(new Specification(typeName), listener);
    }

    public void register(String typeName, Object target, Listener listener) {
        register(new Specification(typeName, target), listener);
    }

    public void register(Specification specification, Listener listener) {
        String typeName = specification.getType();
        List<Entry> typeEntries;
        if (typeEntryMap.containsKey(typeName)) {
            typeEntries = typeEntryMap.get(typeName);
        } else {
            typeEntries = new LinkedList<Entry>();
            typeEntryMap.put(typeName, typeEntries);
        }
        Entry entry = new Entry(specification, listener);
        typeEntries.add(entry);
    }
    
    public void remove(String typeName) {
        typeEntryMap.remove(typeName);
    }

    public void remove(Specification specification) {
        String typeName = specification.getType();
        if (typeEntryMap.containsKey(typeName)) {
            Iterator<Entry> entryterator = typeEntryMap.get(typeName).iterator();
            while (entryterator.hasNext()) {
                Entry entry = entryterator.next();
                if (entry.specification.accept(specification)) {
                    entryterator.remove();
                }
            }
        }
    }

    public void remove(Listener listener) {
        for (Map.Entry<String, List<Entry>> typeToListenersEntry: typeEntryMap.entrySet()) {
            Iterator<Entry> entryterator = typeToListenersEntry.getValue().iterator();
            while (entryterator.hasNext()) {
                Entry entry = entryterator.next();
                if (entry.listener == listener) {
                    entryterator.remove();
                }
            }
        }
    }

    public void trigger(String typeName) {
        trigger(new Specification(typeName));
    }

    public void trigger(String typeName, Object target) {
        trigger(new Specification(typeName, target));
    }
    
    public void trigger(Specification specification) {
        String typeName = specification.getType();
        if (typeEntryMap.containsKey(typeName)) {
            Event event = new Event(specification);
            List<Entry> typeEntries = typeEntryMap.get(typeName);
            for (Entry entry: typeEntries) {
                if (entry.specification.accept(specification)) {
                    entry.listener.invoke(event);
                }
            }
        }
    }
    
    private class Entry {
        
        final Specification specification;

        final Listener listener;
        
        Entry(Specification specification, Listener listener) {
            this.specification = specification;
            this.listener = listener;
        }
        
    }
    
}

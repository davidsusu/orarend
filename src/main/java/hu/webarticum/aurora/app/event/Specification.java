package hu.webarticum.aurora.app.event;

import java.util.HashMap;
import java.util.Map;

public class Specification {
    
    private final String type;
    
    private final Map<String, Object> attributes;

    public Specification(String type) {
        this.type = type;
        this.attributes = new HashMap<String, Object>();
    }

    public Specification(String type, Object target) {
        this.type = type;
        this.attributes = new HashMap<String, Object>();
        this.attributes.put("target", target);
    }
    
    public Specification(String type, Map<String, Object> attributes) {
        this.type = type;
        this.attributes = new HashMap<String, Object>(attributes);
    }
    
    public String getType() {
        return type;
    }
    
    public Map<String, Object> getAttributes() {
        return new HashMap<String, Object>(attributes);
    }
    
    public boolean accept(Specification other) {
        if (!type.equals(other.type)) {
            return false;
        }
        
        for (Map.Entry<String, Object> entry: attributes.entrySet()) {
            String attributeName = entry.getKey();
            Object attributeValue = entry.getValue();
            if (!other.attributes.containsKey(attributeName)) {
                return false;
            }
            if (!attributeValue.equals(other.attributes.get(attributeName))) {
                return false;
            }
        }
        
        return true;
    }
    
}

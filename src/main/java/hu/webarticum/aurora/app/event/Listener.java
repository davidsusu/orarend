package hu.webarticum.aurora.app.event;


public interface Listener {
    
    public void invoke(Event event);
    
}

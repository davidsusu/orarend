package hu.webarticum.aurora.app.event;


public class Event {
    
    private final Specification specification;
    
    public Event(Specification specification) {
        this.specification = specification;
    }
    
    public String getType() {
        return specification.getType();
    }
    
    public Specification getSpecification() {
        return specification;
    }
    
}

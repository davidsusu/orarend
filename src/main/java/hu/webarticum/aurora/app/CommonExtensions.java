package hu.webarticum.aurora.app;

import java.awt.Component;
import java.io.IOException;

import javax.swing.Icon;

import hu.webarticum.aurora.app.util.DocumentFactory;
import hu.webarticum.aurora.app.util.autoput.AutoPutterFactory;

// XXX: getLabel() in extensions

public class CommonExtensions {

    public interface Runner {
        
        public enum Type {
            OPERATION,
            UI,
        }
        
        public Type getType();
        
        public boolean isApplicable();
        
        public void run();
        
    }

    public interface Algorithm {
        
        public AutoPutterFactory getAutoPutterFactory();
        
    }
    
    public interface Template {
        
        public String getLabel();
        
        public DocumentFactory getDocumentFactory();
        
    }

    public interface Demo {

        public String getLabel();
        
        public DocumentFactory getDocumentFactory();
        
    }

    public interface BoardExport {
    
        public String getLabel();
        
        public Icon getIcon();
        
        public BoardExportHandlerFactory getBoardExportHandlerFactory();
        
        public interface BoardExportHandlerFactory {
            
            public BoardExportHandler createBoardExportHandler();
            
        }
        
        public interface BoardExportHandler {
            
            // TODO: make it swing-independent (use abstract-gui)
            public Component getComponent();
            
            public void initComponent();
            
            public void executeExport() throws IOException;
            
        }
        
    }
    
}

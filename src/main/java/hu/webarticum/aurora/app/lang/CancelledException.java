package hu.webarticum.aurora.app.lang;

public class CancelledException extends Exception {

    private static final long serialVersionUID = 1L;

    
    public CancelledException() {
        super();
    }

    public CancelledException(Throwable cause) {
        super(cause);
    }
    
}

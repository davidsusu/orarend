package hu.webarticum.aurora.app.memento;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.chm.Memento;

public class TimingSetMemento implements Memento<TimingSet> {

    private final String label;
    private final List<TimingSet.TimeEntry> entries;
    
    public TimingSetMemento(TimingSet timingSet) {
        this(timingSet.getLabel(), timingSet);
    }

    public TimingSetMemento(String label, Collection<TimingSet.TimeEntry> entries) {
        this.label = label;
        this.entries = new ArrayList<TimingSet.TimeEntry>(entries);
    }

    @Override
    public void apply(TimingSet timingSet) {
        timingSet.setLabel(label);
        timingSet.clear();
        timingSet.addAll(entries);
    }
    
}

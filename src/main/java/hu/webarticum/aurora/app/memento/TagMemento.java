package hu.webarticum.aurora.app.memento;

import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.chm.Memento;

public class TagMemento extends AspectMementoBase implements Memento<Tag> {

    private final Tag.Type type;
    
    public TagMemento(Tag tag) {
        super(tag);
        this.type = tag.getType();
    }

    public TagMemento(
        String label, String acronym, Color color,
        boolean timingSetEnabled, boolean timeLimitEnabled,
        TimingSetManagerMemento timingSetManagerMemento, TimeLimitManagerMemento timeLimitManagerMemento,
        Tag.Type type
    ) {
        super(
            label, acronym, color, timingSetEnabled, timeLimitEnabled,
            timingSetManagerMemento, timeLimitManagerMemento
        );
        this.type = type;
    }

    @Override
    public void apply(Tag tag) {
        apply((Aspect)tag);
        tag.setType(type);
    }
    
}

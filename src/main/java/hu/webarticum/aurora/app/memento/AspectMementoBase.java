package hu.webarticum.aurora.app.memento;

import java.util.HashMap;
import java.util.Map;

import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.TimeLimit;
import hu.webarticum.chm.Memento;

public abstract class AspectMementoBase {

    private final String label;
    private final String acronym;
    private final Color color;
    private final boolean timingSetEnabled;
    private final boolean timeLimitEnabled;
    private final TimingSetManagerMemento timingSetManagerMemento;
    private final TimeLimitManagerMemento timeLimitManagerMemento;
    
    public AspectMementoBase(Aspect aspect) {
        this(
            aspect.getLabel(), aspect.getAcronym(), aspect.getColor(),
            aspect.isTimingSetEnabled(), aspect.isTimeLimitEnabled(),
            new TimingSetManagerMemento(aspect.getTimingSetManager()),
            new TimeLimitManagerMemento(aspect.getTimeLimitManager())
        );
    }

    public AspectMementoBase(
        String label, String acronym, Color color,
        boolean timingSetEnabled, boolean timeLimitEnabled,
        TimingSetManagerMemento timingSetManagerMemento, TimeLimitManagerMemento timeLimitManagerMemento
    ) {
        this.label = label;
        this.acronym = acronym;
        this.color = color;
        this.timingSetEnabled = timingSetEnabled;
        this.timeLimitEnabled = timeLimitEnabled;
        this.timingSetManagerMemento = timingSetManagerMemento;
        this.timeLimitManagerMemento = timeLimitManagerMemento;
    }
    
    protected void apply(Aspect aspect) {
        aspect.setLabel(label);
        aspect.setAcronym(acronym);
        aspect.setColor(color);
        aspect.setTimingSetEnabled(timingSetEnabled);
        aspect.setTimeLimitEnabled(timeLimitEnabled);
        timingSetManagerMemento.apply(aspect.getTimingSetManager());
        timeLimitManagerMemento.apply(aspect.getTimeLimitManager());
    }

    public static class TimingSetManagerMemento implements Memento<Aspect.TimingSetManager> {
        
        private final TimingSet defaultTimingSet;
        private final Map<Period, TimingSet> periodTimingSetMap;
        
        public TimingSetManagerMemento(Aspect.TimingSetManager timingSetManager) {
            this(timingSetManager.getDefaultTimingSet(), timingSetManager.getPeriodTimingSets());
        }
        
        public TimingSetManagerMemento(TimingSet defaultTimingSet, Map<Period, TimingSet> periodTimingSetMap) {
            this.defaultTimingSet = defaultTimingSet;
            this.periodTimingSetMap = new HashMap<Period, TimingSet>(periodTimingSetMap);
        }

        @Override
        public void apply(Aspect.TimingSetManager timingSetManager) {
            timingSetManager.setDefaultTimingSet(defaultTimingSet);
            
            timingSetManager.removePeriodTimingSets();
            for (Map.Entry<Period, TimingSet> entry: periodTimingSetMap.entrySet()) {
                timingSetManager.setPeriodTimingSet(entry.getKey(), entry.getValue());
            }
        }
        
    }

    public static class TimeLimitManagerMemento implements Memento<Aspect.TimeLimitManager> {
        
        private final TimeLimit defaultTimeLimit;
        private final Map<Period, TimeLimit> periodTimeLimitMap;
        
        public TimeLimitManagerMemento(Aspect.TimeLimitManager timeLimitManager) {
            this(timeLimitManager.getDefaultTimeLimit(), timeLimitManager.getPeriodTimeLimits());
        }
        
        public TimeLimitManagerMemento(TimeLimit defaultTimeLimit, Map<Period, TimeLimit> periodTimeLimitMap) {
            this.defaultTimeLimit = defaultTimeLimit;
            this.periodTimeLimitMap = new HashMap<Period, TimeLimit>(periodTimeLimitMap);
        }

        @Override
        public void apply(Aspect.TimeLimitManager timeLimitManager) {
            timeLimitManager.setDefaultTimeLimit(defaultTimeLimit);
        
            timeLimitManager.removePeriodTimeLimits();
            for (Map.Entry<Period, TimeLimit> entry: periodTimeLimitMap.entrySet()) {
                timeLimitManager.setPeriodTimeLimit(entry.getKey(), entry.getValue());
            }
        }
        
    }
    
}

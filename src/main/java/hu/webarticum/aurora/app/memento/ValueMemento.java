package hu.webarticum.aurora.app.memento;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.core.model.Value.ValueMap;
import hu.webarticum.chm.Memento;

public class ValueMemento implements Memento<Value> {
    
    private final Value.Type type;
    
    private final Object innerValue;
    
    private final List<SubEntry> subEntries;
    
    public ValueMemento(Value value) {
        this.type = value.getType();
        this.innerValue = value.get();
        
        subEntries = new ArrayList<SubEntry>();
        
        if (this.type == Value.Type.MAP) {
            for (Map.Entry<Value, Value> entry: value.getAsMap().entrySet()) {
                Value keyValue = entry.getKey();
                Value valueValue = entry.getValue();
                subEntries.add(new SubEntry(
                    keyValue, new ValueMemento(keyValue),
                    valueValue, new ValueMemento(valueValue)
                ));
            }
        } else if (this.type == Value.Type.LIST || this.type == Value.Type.SET) {
            for (Value valueValue: value.getAsList()) {
                subEntries.add(new SubEntry(
                    null, null,
                    valueValue, new ValueMemento(valueValue)
                ));
            }
        }
    }
    
    @Override
    public void apply(Value value) {
        value.getAccess().set(innerValue);

        if (this.type == Value.Type.MAP) {
            ValueMap valueMap = (ValueMap)innerValue;
            valueMap.clear();
            for (SubEntry subEntry: subEntries) {
                subEntry.keyMemento.apply(subEntry.keyReference);
                subEntry.valueMemento.apply(subEntry.valueReference);
                valueMap.put(subEntry.keyReference, subEntry.valueReference);
            }
        } else if (this.type == Value.Type.LIST || this.type == Value.Type.SET) {
            @SuppressWarnings("unchecked")
            Collection<Value> valueCollection = (Collection<Value>)innerValue;
            valueCollection.clear();
            for (SubEntry subEntry: subEntries) {
                subEntry.valueMemento.apply(subEntry.valueReference);
                valueCollection.add(subEntry.valueReference);
            }
        }
    }
    
    private class SubEntry {
        
        final Value keyReference;
        
        final ValueMemento keyMemento;
        
        final Value valueReference;
        
        final ValueMemento valueMemento;
        
        SubEntry(
            Value keyReference, ValueMemento keyMemento,
            Value valueReference, ValueMemento valueMemento
        ) {
            this.keyReference = keyReference;
            this.keyMemento = keyMemento;
            this.valueReference = valueReference;
            this.valueMemento = valueMemento;
        }
        
    }
    
}

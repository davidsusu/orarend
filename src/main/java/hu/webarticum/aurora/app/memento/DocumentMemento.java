package hu.webarticum.aurora.app.memento;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.chm.Memento;

public class DocumentMemento extends DocumentLightMemento {

    private final StoreMemento<Period> periodStoreMemento;
    private final Map<Period, PeriodMemento> periodMementoMap;

    private final StoreMemento<TimingSet> timingSetStoreMemento;
    private final Map<TimingSet, TimingSetMemento> timingSetMementoMap;
    
    private final StoreMemento<Tag> tagStoreMemento;
    private final Map<Tag, TagMemento> tagMementoMap;

    private final StoreMemento<Resource> resourceStoreMemento;
    private final Map<Resource, ResourceMemento> resourceMementoMap;
    
    private final StoreMemento<Block> blockStoreMemento;
    private final Map<Block, BlockMemento> blockMementoMap;

    private final StoreMemento<Board> boardStoreMemento;
    private final Map<Board, BoardMemento> boardMementoMap;
    
    private final ValueMemento extraDataMemento;
    
    public DocumentMemento(Document document) {
        this(
            document.getLabel(),
            new StoreMemento<Period>(document.getPeriodStore()),
            createPeriodCollectionMementoMap(document.getPeriodStore().getAll()),
            new StoreMemento<TimingSet>(document.getTimingSetStore()),
            createTimingSetCollectionMementoMap(document.getTimingSetStore().getAll()),
            new StoreMemento<Tag>(document.getTagStore()),
            createTagCollectionMementoMap(document.getTagStore().getAll()),
            new StoreMemento<Resource>(document.getResourceStore()),
            createResourceCollectionMementoMap(document.getResourceStore().getAll()),
            new StoreMemento<Block>(document.getBlockStore()),
            createBlockCollectionMementoMap(document.getBlockStore().getAll()),
            new StoreMemento<Board>(document.getBoardStore()),
            createBoardCollectionMementoMap(document.getBoardStore().getAll()),
            new ValueMemento(document.getExtraData())
        );
    }
    
    public DocumentMemento(
        String label,
        StoreMemento<Period> periodStoreMemento,
        Map<Period, PeriodMemento> periodMementoMap,
        StoreMemento<TimingSet> timingSetStoreMemento,
        Map<TimingSet, TimingSetMemento> timingSetMementoMap,
        StoreMemento<Tag> tagStoreMemento,
        Map<Tag, TagMemento> tagMementoMap,
        StoreMemento<Resource> resourceStoreMemento,
        Map<Resource, ResourceMemento> resourceMementoMap,
        StoreMemento<Block> blockStoreMemento,
        Map<Block, BlockMemento> blockMementoMap,
        StoreMemento<Board> boardStoreMemento,
        Map<Board, BoardMemento> boardMementoMap,
        ValueMemento extraDataMemento
    ) {
        super(label);

        this.periodStoreMemento = periodStoreMemento;
        this.periodMementoMap = new HashMap<Period, PeriodMemento>(periodMementoMap);
        this.timingSetStoreMemento = timingSetStoreMemento;
        this.timingSetMementoMap = new HashMap<TimingSet, TimingSetMemento>(timingSetMementoMap);
        this.tagStoreMemento = tagStoreMemento;
        this.tagMementoMap = new HashMap<Tag, TagMemento>(tagMementoMap);
        this.resourceStoreMemento = resourceStoreMemento;
        this.resourceMementoMap = new HashMap<Resource, ResourceMemento>(resourceMementoMap);
        this.blockStoreMemento = blockStoreMemento;
        this.blockMementoMap = new HashMap<Block, BlockMemento>(blockMementoMap);
        this.boardStoreMemento = boardStoreMemento;
        this.boardMementoMap = new HashMap<Board, BoardMemento>(boardMementoMap);
        this.extraDataMemento = extraDataMemento;
    }
    
    @Override
    public void apply(Document document) {
        super.apply(document);
        
        periodStoreMemento.apply(document.getPeriodStore());
        applyMementoMap(periodMementoMap);
        timingSetStoreMemento.apply(document.getTimingSetStore());
        applyMementoMap(timingSetMementoMap);
        tagStoreMemento.apply(document.getTagStore());
        applyMementoMap(tagMementoMap);
        resourceStoreMemento.apply(document.getResourceStore());
        applyMementoMap(resourceMementoMap);
        blockStoreMemento.apply(document.getBlockStore());
        applyMementoMap(blockMementoMap);
        boardStoreMemento.apply(document.getBoardStore());
        applyMementoMap(boardMementoMap);
        extraDataMemento.apply(document.getExtraData());
    }
    
    private <T> void applyMementoMap(Map<T, ? extends Memento<T>> mementoMap) {
        for (Map.Entry<T, ? extends Memento<T>> entry: mementoMap.entrySet()) {
            entry.getValue().apply(entry.getKey());
        }
    }

    public static Map<Period, PeriodMemento> createPeriodCollectionMementoMap(Collection<Period> periods) {
        Map<Period, PeriodMemento> result = new HashMap<Period, PeriodMemento>();
        for (Period period: periods) {
            result.put(period, new PeriodMemento(period));
        }
        return result;
    }

    public static Map<TimingSet, TimingSetMemento> createTimingSetCollectionMementoMap(Collection<TimingSet> timingSets) {
        Map<TimingSet, TimingSetMemento> result = new HashMap<TimingSet, TimingSetMemento>();
        for (TimingSet timingSet: timingSets) {
            result.put(timingSet, new TimingSetMemento(timingSet));
        }
        return result;
    }

    public static Map<Tag, TagMemento> createTagCollectionMementoMap(Collection<Tag> tags) {
        Map<Tag, TagMemento> result = new HashMap<Tag, TagMemento>();
        for (Tag tag: tags) {
            result.put(tag, new TagMemento(tag));
        }
        return result;
    }

    public static Map<Resource, ResourceMemento> createResourceCollectionMementoMap(Collection<Resource> resources) {
        Map<Resource, ResourceMemento> result = new HashMap<Resource, ResourceMemento>();
        for (Resource resource: resources) {
            result.put(resource, new ResourceMemento(resource));
        }
        return result;
    }

    public static Map<Block, BlockMemento> createBlockCollectionMementoMap(Collection<Block> blocks) {
        Map<Block, BlockMemento> result = new HashMap<Block, BlockMemento>();
        for (Block block: blocks) {
            result.put(block, new BlockMemento(block));
        }
        return result;
    }

    public static Map<Board, BoardMemento> createBoardCollectionMementoMap(Collection<Board> boards) {
        Map<Board, BoardMemento> result = new HashMap<Board, BoardMemento>();
        for (Board board: boards) {
            result.put(board, new BoardMemento(board));
        }
        return result;
    }
    
}

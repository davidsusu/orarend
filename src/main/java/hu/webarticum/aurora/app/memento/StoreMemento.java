package hu.webarticum.aurora.app.memento;


import java.util.HashMap;
import java.util.Map;

import hu.webarticum.aurora.core.model.Store;
import hu.webarticum.chm.Memento;

public class StoreMemento<T> implements Memento<Store<T>> {
    
    final Map<T, String> itemIdMap;
    
    public StoreMemento(Store<T> store) {
        this(store.getItemIdMap());
    }

    public StoreMemento(Map<T, String> itemIdMap) {
        this.itemIdMap = new HashMap<T, String>(itemIdMap);
    }

    @Override
    public void apply(Store<T> store) {
        Map<T, String> currentItemIdMap = store.getItemIdMap();

        for (Map.Entry<T, String> entry: currentItemIdMap.entrySet()) {
            T item = entry.getKey();
            String id = entry.getValue();
            if (!itemIdMap.containsKey(item) || !itemIdMap.get(item).equals(id)) {
                store.remove(item);
            }
        }
        for (Map.Entry<T, String> entry: itemIdMap.entrySet()) {
            T item = entry.getKey();
            String id = entry.getValue();
            if (!store.contains(item)) {
                store.register(item, id);
            }
        }
    }
    
}

package hu.webarticum.aurora.app.memento;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.chm.Memento;

public class ResourceMemento extends AspectMementoBase implements Memento<Resource> {

    private final SplittingManagerMemento splittingManagerMemento;
    private final Resource.Type type;
    private final int quantity;
    private final String email;
    
    public ResourceMemento(Resource resource) {
        super(resource);
        this.splittingManagerMemento = new SplittingManagerMemento(resource.getSplittingManager());
        this.type = resource.getType();
        this.quantity = resource.getQuantity();
        this.email = resource.getEmail();
    }

    public ResourceMemento(
        String label, String acronym, String email, Color color,
        boolean timingSetEnabled, boolean timeLimitEnabled,
        SplittingManagerMemento splittingManagerMemento,
        TimingSetManagerMemento timingSetManagerMemento, TimeLimitManagerMemento timeLimitManagerMemento,
        Resource.Type type, int quantity
    ) {
        super(
            label, acronym, color, timingSetEnabled, timeLimitEnabled,
            timingSetManagerMemento, timeLimitManagerMemento
        );
        this.splittingManagerMemento = splittingManagerMemento;
        this.type = type;
        this.quantity = quantity;
        this.email = email;
    }

    @Override
    public void apply(Resource resource) {
        apply((Aspect)resource);
        splittingManagerMemento.apply(resource.getSplittingManager());
        resource.setType(type);
        resource.setQuantity(quantity);
        resource.setEmail(email);
    }
    
    public static class SplittingManagerMemento implements Memento<Resource.SplittingManager> {
        
        private final List<Pair<Resource.Splitting, SplittingMemento>> splittingMementoPairs;
        
        public SplittingManagerMemento(Resource.SplittingManager splittingManager) {
            splittingMementoPairs = new ArrayList<Pair<Resource.Splitting, SplittingMemento>>();
            for (Resource.Splitting splitting: splittingManager.getSplittings()) {
                splittingMementoPairs.add(new Pair<Resource.Splitting, SplittingMemento>(splitting, new SplittingMemento(splitting)));
            }
        }

        public SplittingManagerMemento(Collection<Pair<Resource.Splitting, SplittingMemento>> splittingMementoPairs) {
            this.splittingMementoPairs = new ArrayList<Pair<Resource.Splitting, SplittingMemento>>(splittingMementoPairs);
        }

        @Override
        public void apply(Resource.SplittingManager splittingManager) {
            List<Resource.Splitting> splittingListReference = splittingManager.getSplittings();
            splittingListReference.clear();
            for (Pair<Resource.Splitting, SplittingMemento> splittingMementoPair: splittingMementoPairs) {
                Resource.Splitting splitting = splittingMementoPair.getLeft();
                SplittingMemento splittingMemento = splittingMementoPair.getRight();
                splittingMemento.apply(splitting);
                splittingListReference.add(splitting);
            }
        }
        
    }
    
    public static class SplittingMemento implements Memento<Resource.Splitting> {

        private final String label;
        private final List<Pair<Resource.Splitting.Part, SplittingPartMemento>> splittingPartMementoPairs;
        
        public SplittingMemento(Resource.Splitting splitting) {
            this.label = splitting.getLabel();
            splittingPartMementoPairs = new ArrayList<Pair<Resource.Splitting.Part, SplittingPartMemento>>();
            for (Resource.Splitting.Part splittingPart: splitting.getParts()) {
                splittingPartMementoPairs.add(new Pair<Resource.Splitting.Part, SplittingPartMemento>(splittingPart, new SplittingPartMemento(splittingPart)));
            }
        }
        
        public SplittingMemento(String label, Collection<Pair<Resource.Splitting.Part, SplittingPartMemento>> splittingPartMementoPairs) {
            this.label = label;
            this.splittingPartMementoPairs = new ArrayList<Pair<Resource.Splitting.Part, SplittingPartMemento>>(splittingPartMementoPairs);
        }

        @Override
        public void apply(Resource.Splitting splitting) {
            splitting.setLabel(label);
            
            List<Resource.Splitting.Part> splittingPartListReference = splitting.getParts();
            splittingPartListReference.clear();
            for (Pair<Resource.Splitting.Part, SplittingPartMemento> splittingPartMementoPair: splittingPartMementoPairs) {
                Resource.Splitting.Part splittingPart = splittingPartMementoPair.getLeft();
                SplittingPartMemento splittingPartMemento = splittingPartMementoPair.getRight();
                splittingPartMemento.apply(splittingPart);
                splitting.addPart(splittingPart);
            }
        }
        
    }
    
    public static class SplittingPartMemento implements Memento<Resource.Splitting.Part> {
        
        private final String label;
        
        public SplittingPartMemento(Resource.Splitting.Part splittingPart) {
            this(splittingPart.getLabel());
        }

        public SplittingPartMemento(String label) {
            this.label = label;
        }
        
        @Override
        public void apply(Resource.Splitting.Part splittingPart) {
            splittingPart.setLabel(label);
        }
        
    }
    
}


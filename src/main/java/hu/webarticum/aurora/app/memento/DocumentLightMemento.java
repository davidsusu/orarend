package hu.webarticum.aurora.app.memento;

import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.chm.Memento;

public class DocumentLightMemento implements Memento<Document> {
    
    private final String label;
    
    public DocumentLightMemento(Document document) {
        this(document.getLabel());
    }
    
    public DocumentLightMemento(String label) {
        this.label = label;
    }
    
    @Override
    public void apply(Document document) {
        document.setLabel(label);
    }
    
}

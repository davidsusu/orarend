package hu.webarticum.aurora.app.memento;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Block.ActivityManager;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.ResourceSubset;
import hu.webarticum.aurora.core.model.ResourceSubsetList;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.chm.Memento;

public class BlockMemento implements Memento<Block> {
    
    private final String label;
    private final long length;
    private final ActivityManagerMemento activityManagerMemento;
    
    public BlockMemento(Block block) {
        this(block.getLabel(), block.getLength(), new ActivityManagerMemento(block.getActivityManager()));
    }
    
    public BlockMemento(String label, long length, ActivityManagerMemento activityManagerMemento) {
        this.label = label;
        this.length = length;
        this.activityManagerMemento = activityManagerMemento;
    }
    
    @Override
    public void apply(Block block) {
        apply(block, true);
    }
    
    public void apply(Block block, boolean keepActivityReferences) {
        block.setLabel(label);
        block.setLength(length);
        activityManagerMemento.apply(block.getActivityManager(), keepActivityReferences);
    }
    
    public static class ActivityManagerMemento implements Memento<Block.ActivityManager> {
        
        private final HashMap<Activity, Pair<ActivityMemento, PeriodSet>> activityDataMap;
        
        public ActivityManagerMemento(Block.ActivityManager activityManager) {
            this.activityDataMap = new HashMap<Activity, Pair<ActivityMemento, PeriodSet>>();
            for (Map.Entry<Activity, PeriodSet> entry: activityManager.toActivityToPeriodsMap().entrySet()) {
                Activity activity = entry.getKey();
                ActivityMemento activityMemento = new ActivityMemento(activity);
                Collection<Period> periods = entry.getValue();
                this.activityDataMap.put(activity, new Pair<ActivityMemento, PeriodSet>(activityMemento, new PeriodSet(periods)));
            }
        }
        
        public ActivityManagerMemento(Map<Activity, Pair<ActivityMemento, ? extends Collection<Period>>> activityDataMap) {
            this.activityDataMap = new HashMap<Activity, Pair<ActivityMemento, PeriodSet>>();
            for (Map.Entry<Activity, Pair<ActivityMemento, ? extends Collection<Period>>> entry: activityDataMap.entrySet()) {
                Activity activity = entry.getKey();
                Pair<ActivityMemento, ? extends Collection<Period>> activityMementoPeriodsPair = entry.getValue();
                ActivityMemento activityMemento = activityMementoPeriodsPair.getLeft();
                Collection<Period> periods = activityMementoPeriodsPair.getRight();
                this.activityDataMap.put(activity, new Pair<ActivityMemento, PeriodSet>(activityMemento, new PeriodSet(periods)));
            }
        }

        @Override
        public void apply(ActivityManager activityManager) {
            apply(activityManager, true);
        }
        
        public void apply(ActivityManager activityManager, boolean keepActivityReferences) {
            activityManager.clear();
            for (Map.Entry<Activity, Pair<ActivityMemento, PeriodSet>> entry: activityDataMap.entrySet()) {
                Activity activity;
                if (keepActivityReferences) {
                    activity = entry.getKey();
                } else {
                    activity = new Activity();
                }
                
                Pair<ActivityMemento, PeriodSet> activityMementoPeriodsPair = entry.getValue();
                ActivityMemento activityMemento = activityMementoPeriodsPair.getLeft();
                PeriodSet periods = activityMementoPeriodsPair.getRight();

                activityMemento.apply(activity);
                activityManager.add(activity, periods);
            }
        }
        
    }
    
    public static class ActivityMemento implements Memento<Activity> {

        private final String label;
        private final TagManagerMemento tagManagerMemento;
        private final ResourceManagerMemento resourceManagerMemento;
        
        public ActivityMemento(Activity activity) {
            this(activity.getLabel(), new TagManagerMemento(activity.getTagManager()), new ResourceManagerMemento(activity.getResourceManager()));
        }

        public ActivityMemento(String label, TagManagerMemento tagManagerMemento, ResourceManagerMemento resourceManagerMemento) {
            this.label = label;
            this.tagManagerMemento = tagManagerMemento;
            this.resourceManagerMemento = resourceManagerMemento;
        }

        @Override
        public void apply(Activity activity) {
            activity.setLabel(label);
            tagManagerMemento.apply(activity.getTagManager());
            resourceManagerMemento.apply(activity.getResourceManager());
        }

        public static class TagManagerMemento implements Memento<Activity.TagManager> {
            
            private final Set<Tag> tags;
            
            public TagManagerMemento(Activity.TagManager tagManager) {
                this(tagManager.getTags());
            }

            public TagManagerMemento(Collection<Tag> tags) {
                this.tags = new HashSet<Tag>(tags);
            }

            @Override
            public void apply(Activity.TagManager tagManager) {
                tagManager.clear();
                for (Tag tag: tags) {
                    tagManager.add(tag);
                }
            }
            
        }

        public static class ResourceManagerMemento implements Memento<Activity.ResourceManager> {
            
            private final ResourceSubsetList resourceSubsets;
            
            public ResourceManagerMemento(Activity.ResourceManager resourceManager) {
                this(resourceManager.getResourceSubsets());
            }

            public ResourceManagerMemento(ResourceSubsetList resourceSubsets) {
                this.resourceSubsets = new ResourceSubsetList(resourceSubsets);
            }

            @Override
            public void apply(Activity.ResourceManager resourceManager) {
                resourceManager.clear();
                for (ResourceSubset resourceSubset: resourceSubsets) {
                    resourceManager.add(resourceSubset);
                }
            }
            
        }
        
    }

}

package hu.webarticum.aurora.app.memento;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.IteratorUtils;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.util.common.Pair;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.Aspect;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.ResourceSubset;
import hu.webarticum.aurora.core.model.ResourceSubsetList;
import hu.webarticum.aurora.core.model.Store;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.core.model.time.TimeLimit;
import hu.webarticum.chm.AbstractCommand;
import hu.webarticum.chm.Command;

public class RemovalHelper {
    
    
    private final Document document;
    
    
    public RemovalHelper(Document document) {
        this.document = document;
    }
    
    public Command createRemoveCommandFor(final Period period, String commandLabelPath) {
        return new AbstractRemovalCommand<Period>(period, document.getPeriodStore(), commandLabelPath) {
            
            Map<Block, ActivityList> blockAssociatedActivitiesMap = new HashMap<Block, ActivityList>();
            
            Map<Aspect, TimingSet> aspectTimingSetMap = new HashMap<Aspect, TimingSet>();
            
            Map<Aspect, TimeLimit> aspectTimeLimitMap = new HashMap<Aspect, TimeLimit>();
            
            {
                for (Block block: document.getBlockStore()) {
                    ActivityList activities = block.getActivityManager().getActivities(period);
                    if (!activities.isEmpty()) {
                        blockAssociatedActivitiesMap.put(block, activities);
                    }
                }
                
                Iterator<Aspect> iterator = IteratorUtils.chainedIterator(
                    document.getTagStore().iterator(), document.getResourceStore().iterator()
                );
                while (iterator.hasNext()) {
                    Aspect aspect = iterator.next();
                    
                    Aspect.TimingSetManager timingSetManager = aspect.getTimingSetManager();
                    TimingSet timingSet = timingSetManager.getPeriodTimingSet(period);
                    if (timingSet != null) {
                        aspectTimingSetMap.put(aspect, timingSet);
                    }

                    Aspect.TimeLimitManager timeLimitManager = aspect.getTimeLimitManager();
                    TimeLimit timeLimit = timeLimitManager.getPeriodTimeLimit(period);
                    if (timeLimit != null) {
                        aspectTimeLimitMap.put(aspect, timeLimit);
                    }
                }
            }
            
            @Override
            protected boolean _execute() {
                super._execute();
                
                for (Map.Entry<Block, ActivityList> entry: blockAssociatedActivitiesMap.entrySet()) {
                    entry.getKey().getActivityManager().removePeriod(period);
                }
                
                for (Map.Entry<Aspect, TimingSet> entry: aspectTimingSetMap.entrySet()) {
                    entry.getKey().getTimingSetManager().removePeriodTimingSet(period);
                }
                
                for (Map.Entry<Aspect, TimeLimit> entry: aspectTimeLimitMap.entrySet()) {
                    entry.getKey().getTimeLimitManager().removePeriodTimeLimit(period);
                }
                
                return true;
            }

            @Override
            protected boolean _rollBack() {
                super._rollBack();
                
                for (Map.Entry<Block, ActivityList> entry: blockAssociatedActivitiesMap.entrySet()) {
                    Block.ActivityManager activityManager = entry.getKey().getActivityManager();
                    ActivityList activities = entry.getValue();
                    for (Activity activity: activities) {
                        Block.ActivityManager.ActivityEntry activityEntry = activityManager.getActivityEntry(activity);
                        if (activityEntry != null) {
                            activityEntry.addPeriod(period);
                        } else {
                            activityManager.add(activity, period);
                        }
                    }
                }

                for (Map.Entry<Aspect, TimingSet> entry: aspectTimingSetMap.entrySet()) {
                    Aspect aspect = entry.getKey();
                    TimingSet timingSet = entry.getValue();
                    aspect.getTimingSetManager().setPeriodTimingSet(period, timingSet);
                }
                
                for (Map.Entry<Aspect, TimeLimit> entry: aspectTimeLimitMap.entrySet()) {
                    Aspect aspect = entry.getKey();
                    TimeLimit timeLimit = entry.getValue();
                    aspect.getTimeLimitManager().setPeriodTimeLimit(period, timeLimit);
                }
                
                return true;
            }
            
        };
    }
    
    public Command createRemoveCommandFor(final TimingSet timingSet, String commandLabelPath) {
        return new AbstractRemovalCommand<TimingSet>(timingSet, document.getTimingSetStore(), commandLabelPath) {
            
            List<Aspect> defaultAssociationAspects = new ArrayList<Aspect>();
            
            List<Pair<Aspect, Period>> aspectPeriodAssociations = new ArrayList<Pair<Aspect, Period>>();
            
            {
                Iterator<Aspect> iterator = IteratorUtils.chainedIterator(
                    document.getTagStore().iterator(), document.getResourceStore().iterator()
                );
                while (iterator.hasNext()) {
                    Aspect aspect = iterator.next();
                    Aspect.TimingSetManager timingSetManager = aspect.getTimingSetManager();
                    
                    TimingSet defaultTimingSet = timingSetManager.getDefaultTimingSet();
                    if (defaultTimingSet == timingSet) {
                        defaultAssociationAspects.add(aspect);
                    }
                    
                    for (Period period: document.getPeriodStore()) {
                        if (timingSetManager.getPeriodTimingSet(period) == timingSet) {
                            aspectPeriodAssociations.add(new Pair<Aspect, Period>(aspect, period));
                        }
                    }
                    
                    aspect.getTimingSetManager();
                }
            }
            
            @Override
            protected boolean _execute() {
                super._execute();
                
                for (Aspect aspect: defaultAssociationAspects) {
                    aspect.getTimingSetManager().removeDefaultTimingSet();
                }
                
                for (Pair<Aspect, Period> aspectPeriodPair: aspectPeriodAssociations) {
                    Aspect aspect = aspectPeriodPair.getLeft();
                    Period period = aspectPeriodPair.getRight();
                    aspect.getTimingSetManager().removePeriodTimingSet(period);
                }
                
                return true;
            }
            
            @Override
            protected boolean _rollBack() {
                super._rollBack();

                for (Aspect aspect: defaultAssociationAspects) {
                    aspect.getTimingSetManager().setDefaultTimingSet(timingSet);
                }
                
                for (Pair<Aspect, Period> aspectPeriodPair: aspectPeriodAssociations) {
                    Aspect aspect = aspectPeriodPair.getLeft();
                    Period period = aspectPeriodPair.getRight();
                    aspect.getTimingSetManager().setPeriodTimingSet(period, timingSet);
                }
                
                return true;
            }
            
        };
    }
    
    public Command createRemoveCommandFor(final Tag tag, String commandLabelPath) {
        return new AbstractRemovalCommand<Tag>(tag, document.getTagStore(), commandLabelPath) {

            final List<Activity> containingActivities = new ArrayList<Activity>();
            
            {
                for (Block block: document.getBlockStore()) {
                    for (Activity activity: block.getActivityManager().getActivities()) {
                        if (activity.getTagManager().has(tag)) {
                            containingActivities.add(activity);
                        }
                    }
                }
            }
            
            @Override
            protected boolean _execute() {
                super._execute();
                
                for (Activity activity: containingActivities) {
                    activity.getTagManager().remove(tag);
                }
                
                document.getBlockStore().refresh(false);
                
                return true;
            }
            
            @Override
            protected boolean _rollBack() {
                super._rollBack();

                for (Activity activity: containingActivities) {
                    activity.getTagManager().add(tag);
                }

                document.getBlockStore().refresh(false);
                
                return true;
            }
            
        };
    }
    
    public Command createRemoveCommandFor(final Resource resource, String commandLabelPath) {
        return new AbstractRemovalCommand<Resource>(resource, document.getResourceStore(), commandLabelPath) {

            final Map<Activity, ResourceSubsetList> activityResourceSubsetsMap = new HashMap<Activity, ResourceSubsetList>();
            
            {
                for (Block block: document.getBlockStore()) {
                    for (Activity activity: block.getActivityManager().getActivities()) {
                        ResourceSubsetList resourceSubsets = activity.getResourceManager().getResourceSubsets(resource);
                        if (!resourceSubsets.isEmpty()) {
                            activityResourceSubsetsMap.put(activity, resourceSubsets);
                        }
                    }
                }
            }
            
            @Override
            protected boolean _execute() {
                super._execute();

                for (Map.Entry<Activity, ResourceSubsetList> entry: activityResourceSubsetsMap.entrySet()) {
                    entry.getKey().getResourceManager().removeResource(resource);
                }

                document.getBlockStore().refresh(false);
                
                return true;
            }
            
            @Override
            protected boolean _rollBack() {
                super._rollBack();

                for (Map.Entry<Activity, ResourceSubsetList> entry: activityResourceSubsetsMap.entrySet()) {
                    Activity.ResourceManager resourceManager = entry.getKey().getResourceManager();
                    for (ResourceSubset resourceSubset: entry.getValue()) {
                        resourceManager.add(resourceSubset);
                    }
                }

                document.getBlockStore().refresh(false);
                
                return true;
            }
            
            @Override
            boolean validateValue(Object innerValue, Value value, Value parentValue, Value[] path) {
                if (super.validateValue(innerValue, value, parentValue, path)) {
                    return true;
                } else if (value.getType() == Value.Type.RESOURCE) {
                    return value.getAsResource().equals(resource);
                } else if (value.getType() == Value.Type.RESOURCESUBSET) {
                    return value.getAsResourceSubset().getResource().equals(resource);
                } else {
                    return false;
                }
            }
            
        };
    }

    public Command createRemoveCommandFor(final Block block, String commandLabelPath) {
        return new AbstractRemovalCommand<Block>(block, document.getBlockStore(), commandLabelPath) {
            
            List<Pair<Board, Board.Entry>> boardEntries = new ArrayList<Pair<Board, Board.Entry>>();
            
            {
                for (Board board: document.getBoardStore()) {
                    Board.Entry entry = board.search(block);
                    if (entry != null) {
                        boardEntries.add(new Pair<Board, Board.Entry>(board, entry));
                    }
                }
            }
            
            @Override
            protected boolean _execute() {
                super._execute();
                for (Pair<Board, Board.Entry> boardEntryPair: boardEntries) {
                    boardEntryPair.getLeft().remove(block);
                }
                document.getBoardStore().refresh(false);
                return true;
            }
            
            @Override
            protected boolean _rollBack() {
                super._rollBack();
                for (Pair<Board, Board.Entry> boardEntryPair: boardEntries) {
                    boardEntryPair.getLeft().add(block, boardEntryPair.getRight().getTime());
                }
                document.getBoardStore().refresh(false);
                return true;
            }
            
            @Override
            boolean validateValue(Object innerValue, Value value, Value parentValue, Value[] path) {
                if (super.validateValue(innerValue, value, parentValue, path)) {
                    return true;
                } else if (value.getType() == Value.Type.ACTIVITY) {
                    return block.getActivityManager().getActivities().contains(innerValue);
                } else {
                    return false;
                }
            }
            
        };
    }
    
    public Command createRemoveCommandFor(final Board board, String commandLabelPath) {
        return new AbstractRemovalCommand<Board>(board, document.getBoardStore(), commandLabelPath) {
            
            @Override
            protected boolean _execute() {
                super._execute();
                document.getBlockStore().refresh(false);
                return true;
            }
            
            @Override
            protected boolean _rollBack() {
                super._rollBack();
                document.getBlockStore().refresh(false);
                return true;
            }
            
        };
    }
    
    
    private abstract class AbstractRemovalCommand<T> extends AbstractCommand {
        
        final T item;
        
        final Store<T> store;
        
        final String idInStore;
        
        final String commandLabelPath;
        
        final List<ValueRestoreEntry> valueRestoreEntries;
        
        AbstractRemovalCommand(T item, Store<T> store, String commandLabelPath) {
            this.item = item;
            this.store = store;
            this.idInStore = (store == null) ? null : store.getId(item);
            this.commandLabelPath = commandLabelPath;
            this.valueRestoreEntries = new ArrayList<ValueRestoreEntry>();

            final Value extraData = document.getExtraData();
            extraData.walk(new Value.WalkCallback() {
                
                @Override
                public void touch(Value value, Value parentValue, Value[] path) {
                    Object innerValue = value.get();
                    if (validateValue(innerValue, value, parentValue, path)) {
                        valueRestoreEntries.add(new ValueRestoreEntry(innerValue, value, parentValue, extraData, path));
                    }
                }
                
            });
            
        }
        
        @Override
        protected boolean _execute() {
            if (store != null) {
                store.remove(item);
            }
            
            for (ValueRestoreEntry valueRestoreEntry: valueRestoreEntries) {
                valueRestoreEntry.remove();
            }
            
            return true;
        }
        
        @Override
        protected boolean _rollBack() {
            if (store != null) {
                store.register(item, idInStore);
            }
            
            for (ValueRestoreEntry valueRestoreEntry: valueRestoreEntries) {
                valueRestoreEntry.restore();
            }
            
            return true;
        }
        
        @Override
        public String toString() {
            return Application.instance().getTextRepository().getText(commandLabelPath);
        }

        boolean validateValue(Object innerValue, Value value, Value parentValue, Value[] path) {
            if (innerValue == null || item == null) {
                return innerValue == item;
            } else {
                return innerValue.equals(item);
            }
        }

    }
    
    private class ValueRestoreEntry {
        
        final Object innerValue;
        
        final Value value;
        
        final Value parentValue;
        
        final Value rootValue;
        
        final Value[] path;
        
        ValueRestoreEntry(Object innerValue, Value value, Value parentValue, Value rootValue, Value[] path) {
            this.innerValue = innerValue;
            this.value = value;
            this.parentValue = parentValue;
            this.rootValue = rootValue;
            this.path = path;
        }
        
        void remove() {
            if (parentValue == null) {
                value.getAccess().set(Value.Type.NULL);
                return;
            }
            
            Value.Type parentType = parentValue.getType();
            
            if (parentType == Value.Type.SET) {
                parentValue.getAsSet().remove(value);
            } else if (parentType == Value.Type.LIST || parentType == Value.Type.MAP) {
                rootValue.getAccess(path).remove();
            }
        }
        
        void restore() {
            value.getAccess().set(innerValue);
            
            if (parentValue == null) {
                return;
            }
            
            Value.Type parentType = parentValue.getType();

            if (parentType == Value.Type.SET) {
                parentValue.getAsSet().add(value);
            } else if (parentType == Value.Type.LIST || parentType == Value.Type.MAP) {
                rootValue.getAccess(path).set(value);
            }
        }
        
    }
    
}

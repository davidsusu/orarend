package hu.webarticum.aurora.app.memento;

import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.chm.Memento;

public class PeriodMemento implements Memento<Period> {
    
    private final String label;
    private final int term;
    private final int position;
    
    public PeriodMemento(Period period) {
        this(period.getLabel(), period.getTerm(), period.getPosition());
    }

    public PeriodMemento(String label, int term, int position) {
        this.label = label;
        this.term = term;
        this.position = position;
    }

    @Override
    public void apply(Period period) {
        period.setLabel(label);
        period.setTerm(term);
        period.setPosition(position);
    }
    
}

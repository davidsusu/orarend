package hu.webarticum.aurora.app.memento;

import java.util.HashMap;
import java.util.Map;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.time.Time;

public class BoardMemento extends BoardLightMemento {
    
    private final Map<Block, Time> blockTimeMap;
    
    public BoardMemento(Board board) {
        super(board.getLabel());
        this.blockTimeMap = board.toBlockTimeMap();
    }

    public BoardMemento(String label, Map<Block, Time> blockTimeMap) {
        super(label);
        this.blockTimeMap = new HashMap<Block, Time>(blockTimeMap);
    }

    @Override
    public void apply(Board board) {
        super.apply(board);
        
        board.clear();
        for (Map.Entry<Block, Time> entry: blockTimeMap.entrySet()) {
            board.add(entry.getKey(), entry.getValue());
        }
    }
    
}

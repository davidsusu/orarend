package hu.webarticum.aurora.app.memento;

import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.chm.Memento;

public class BoardLightMemento implements Memento<Board> {

    private final String label;
    
    public BoardLightMemento(Board board) {
        this(board.getLabel());
    }

    public BoardLightMemento(String label) {
        this.label = label;
    }

    @Override
    public void apply(Board board) {
        board.setLabel(label);
    }
    
}

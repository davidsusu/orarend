package hu.webarticum.aurora.app;

import static hu.webarticum.aurora.app.Shortcut.extensions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import hu.webarticum.aurora.app.util.i18n.CombinedMultiLanguageTextRepository;
import hu.webarticum.aurora.app.util.i18n.PropertiesLanguageTextRepository;
import hu.webarticum.aurora.core.io.DocumentIo.DocumentIoQueue;
import hu.webarticum.aurora.core.io.bundled.CompressedXmlDocumentIo;
import hu.webarticum.aurora.core.io.bundled.SerializeDocumentIo;
import hu.webarticum.aurora.core.io.bundled.XmlDocumentIo;
import hu.webarticum.aurora.core.text.CoreText;
import hu.webarticum.aurora.core.text.CoreTextSupplier;

public class Main {

    public static void main(String[] args) {
        initBasics(args);
        
        CommonExtensions.Runner uiRunner = null;
        
        for (CommonExtensions.Runner runner: extensions(CommonExtensions.Runner.class)) {
            if (runner.isApplicable()) {
                if (runner.getType() != CommonExtensions.Runner.Type.UI) {
                    runner.run();
                    break;
                } else if (uiRunner == null) {
                    uiRunner = runner;
                }
            }
        }
        
        if (uiRunner != null) {
            uiRunner.run();
        }
    }
    
    private static void initBasics(String[] args) {
        final Application application = Application.instance();
        
        // Arguments
        application.getArgumentMap().putAll(parseArguments(args, "input-file"));
        
        // Languages
        CombinedMultiLanguageTextRepository coreLangRepository = new CombinedMultiLanguageTextRepository();
        coreLangRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/app/lang/core.hu_HU.properties", "hu_HU"));
        coreLangRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/app/lang/core.en_US.properties", "en_US"));
        application.getTextRepository().mount("core", coreLangRepository);
        CoreText.setTextSupplier(new CoreTextSupplier() {
            
            @Override
            public String getText(String coreTextPath) {
                return application.getTextRepository().getText("core." + coreTextPath);
            }
            
        });
        CombinedMultiLanguageTextRepository appLangRepository = new CombinedMultiLanguageTextRepository();
        appLangRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/app/lang/app.hu_HU.properties", "hu_HU"));
        appLangRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/app/lang/app.en_US.properties", "en_US"));
        application.getTextRepository().mount("app", appLangRepository);
        
        // Document IOs
        DocumentIoQueue documentIoQueue = application.getDocumentIoQueue();
        documentIoQueue.add(new CompressedXmlDocumentIo());
        documentIoQueue.add(new XmlDocumentIo());
        documentIoQueue.add(new SerializeDocumentIo());
        
        Runtime.getRuntime().addShutdownHook(new Thread() {
            
            @Override
            public void run() {
                application.close();
            }
            
        });
    }

    private static Map<String, String> parseArguments(String[] args, String... unnamedFields) {
        Map<String, String> arguments = new HashMap<String, String>();
        Iterator<String> unnamedFieldsIterator = Arrays.asList(unnamedFields).iterator();
        for (String argumentStr: args) {
            String name = null;
            String value = null;
            if (argumentStr.matches("\\-\\-[^ =\"\\\\]+=.+")) {
                int esPos = argumentStr.indexOf('=');
                name = argumentStr.substring(2, esPos);
                value = argumentStr.substring(esPos+1);
            } else if (unnamedFieldsIterator.hasNext()) {
                name = unnamedFieldsIterator.next();
                value = argumentStr;
            }
            if (name!=null) {
                arguments.put(name, value);
            }
        }
        return arguments;
    }
    
}

package hu.webarticum.aurora.app;

public final class Info {
    
    private Info() {}
    
    public static final String NAME = "Aurora";

    public static final String CODENAME = "aurora-timetable";

    public static final int MAJOR_VERSION = 0;

    public static final int MINOR_VERSION = 14;

    public static final int PATCH_VERSION = 1;
    
    public static final String AUTHOR = "Horváth Dávid";
    
    public static final String WEBSITE = "https://orarend-program.hu/";
    
    public static final String WEBSITE_NAME = "orarend-program.hu";

    public static final String SERVICE_URL = "https://orarend-program.hu/service";

    public static final String RESOURCE_PATH = "/hu/webarticum/aurora";
    
    public static final String DEFAULT_FILE_EXTENSION = "aur";
    
}

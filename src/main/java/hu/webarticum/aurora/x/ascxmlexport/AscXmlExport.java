package hu.webarticum.aurora.x.ascxmlexport;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;

import hu.webarticum.aurora.app.util.TraditionalTable;
import hu.webarticum.aurora.app.util.common.CollectionUtil;
import hu.webarticum.aurora.app.util.common.StringUtil;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.ActivityList;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Resource.Splitting;
import hu.webarticum.aurora.core.model.ResourceSubset;
import hu.webarticum.aurora.core.model.ResourceSubsetList;
import hu.webarticum.aurora.core.model.Tag;

// TODO: termsdef, weeksdef, daysdef

public class AscXmlExport {

    private static Map<String, Object> defaultSettings = new HashMap<String, Object>();
    static {
        defaultSettings.put("xxx", false);
    }
    
    private Map<String, Object> settings;
    
    public AscXmlExport(Map<String, Object> settings) {
        this.settings = new HashMap<String, Object>(defaultSettings);
        this.settings.putAll(settings);
    }
    
    public void export(Document document, Board board, File outFile) throws IOException {
        (new AscXmlExportRunner(document, board, outFile)).run();
    }
    
    private class AscXmlExportRunner {

        final Document document;
        
        final Board board;
        
        final File outFile;
        
        AscXmlExportRunner(Document document, Board board, File outFile) {
            this.document = document;
            this.board = board;
            this.outFile = outFile;
        }
        
        void run() throws IOException {
            DocumentBuilderFactory xmlFactory = DocumentBuilderFactory.newInstance();
            
            org.w3c.dom.Document xmlDocument;
            try {
                xmlDocument = xmlFactory.newDocumentBuilder().newDocument();
            } catch (ParserConfigurationException e) {
                return;
            }

            boolean decimalSeparatorComma = (boolean)(Boolean)settings.get("decimalSeparatorComma");
            boolean dayNumberingFrom1 = (boolean)(Boolean)settings.get("dayNumberingFrom1");
            
            List<String> options = new ArrayList<String>();
            options.add("export:idprefix:%CHRID");
            options.add("import:idprefix:%TEMPID");
            options.add("groupstype1");
            options.add("lessonsincludeclasseswithoutstudents");
            options.add("handlestudentsafterlessons");
            options.add(decimalSeparatorComma ? "decimalseparatorcomma" : "decimalseparatordot");
            options.add(dayNumberingFrom1 ? "daynumbering1" : "daynumbering0");
            
            Element rootElement = xmlDocument.createElement("timetable");
            rootElement.setAttribute("ascttversion", "2017.6.10");
            rootElement.setAttribute("importtype", "database");
            rootElement.setAttribute("options", StringUtil.join(options, ","));
            rootElement.setAttribute("defaultexport", "1");
            rootElement.setAttribute("displayname", "aSc Timetables 2012 XML");
            xmlDocument.appendChild(rootElement);

            Document.PeriodStore periodStore = document.getPeriodStore();
            Document.TagStore tagStore = document.getTagStore();
            Document.ResourceStore resourceStore = document.getResourceStore();
            
            {
                Element subjectsElement = xmlDocument.createElement("subjects");
                subjectsElement.setAttribute("options", "canadd,export:silent");
                subjectsElement.setAttribute("columns", "id,name,short");
                rootElement.appendChild(subjectsElement);

                for (Tag subject: tagStore.getAll(Tag.Type.SUBJECT)) {
                    Element subjectElement = xmlDocument.createElement("subject");
                    subjectElement.setAttribute("id", tagStore.getId(subject));
                    subjectElement.setAttribute("name", subject.getLabel());
                    subjectElement.setAttribute("short", subject.getAcronym());
                    subjectsElement.appendChild(subjectElement);
                }
            }
            
            {
                Element teachersElement = xmlDocument.createElement("teachers");
                teachersElement.setAttribute("options", "canadd,export:silent");
                teachersElement.setAttribute("columns", "id,name,short,color,email");
                rootElement.appendChild(teachersElement);
                
                for (Resource person: resourceStore.getAll(Resource.Type.PERSON)) {
                    Element teacherElement = xmlDocument.createElement("teacher");
                    teacherElement.setAttribute("id", resourceStore.getId(person));
                    teacherElement.setAttribute("name", person.getLabel());
                    teacherElement.setAttribute("short", person.getAcronym());
                    teacherElement.setAttribute("color", person.getColor().getHexa());
                    teacherElement.setAttribute("email", ""); // TODO
                    teachersElement.appendChild(teacherElement);
                }
            }
            
            {
                Element classroomsElement = xmlDocument.createElement("classrooms");
                classroomsElement.setAttribute("options", "canadd,export:silent");
                classroomsElement.setAttribute("columns", "id,name,short,capacity");
                rootElement.appendChild(classroomsElement);
                
                for (Resource locale: resourceStore.getAll(Resource.Type.LOCALE)) {
                    Element classroomElement = xmlDocument.createElement("classroom");
                    classroomElement.setAttribute("id", resourceStore.getId(locale));
                    classroomElement.setAttribute("name", locale.getLabel());
                    classroomElement.setAttribute("short", locale.getAcronym());
                    classroomElement.setAttribute("capacity", "*");
                    classroomsElement.appendChild(classroomElement);
                }
            }

            Labeled.LabeledStore<Resource.Splitting.Part> splittingPartStore = new Labeled.LabeledStore<Resource.Splitting.Part>();
            Map<Resource, Resource.Splitting.Part> classFakeResourceMap = new HashMap<Resource, Resource.Splitting.Part>();
            {
                Element classesElement = xmlDocument.createElement("classes");
                classesElement.setAttribute("options", "canadd,export:silent");
                classesElement.setAttribute("columns", "id,name,short");
                rootElement.appendChild(classesElement);

                Element groupsElement = xmlDocument.createElement("groups");
                groupsElement.setAttribute("options", "canadd,export:silent");
                groupsElement.setAttribute("columns", "id,name,classid,entireclass,divisiontag,ascttdivision");
                rootElement.appendChild(groupsElement);

                Resource fakeResource = new Resource();
                Resource.Splitting fakeSplitting = fakeResource.getSplittingManager().add("[FAKE]");
                
                for (Resource _class: resourceStore.getAll(Resource.Type.CLASS)) {
                    String classid = document.getResourceStore().getId(_class);
                    
                    Element classElement = xmlDocument.createElement("class");
                    classElement.setAttribute("id", resourceStore.getId(_class));
                    classElement.setAttribute("name", _class.getLabel());
                    classElement.setAttribute("short", _class.getAcronym());
                    classesElement.appendChild(classElement);
                    
                    int splittingIndex = 0;
                    for (Resource.Splitting splitting: _class.getSplittingManager().getSplittings()) {
                        splittingIndex++;
                        String splittingLabel = splitting.getLabel();
                        for (Resource.Splitting.Part splittingPart: splitting.getParts()) {
                            String groupId = splittingPartStore.register(splittingPart, resourceStore.getId(_class) + "_" + splittingLabel + "_" + splittingPart.getLabel());
                            Element groupElement = xmlDocument.createElement("group");
                            groupElement.setAttribute("id", groupId);
                            groupElement.setAttribute("name", splittingLabel + ": " +splittingPart.getLabel());
                            groupElement.setAttribute("classid", classid);
                            groupElement.setAttribute("entireclass", "0");
                            groupElement.setAttribute("divisiontag", "" + splittingIndex);
                            groupElement.setAttribute("ascttdivision", "" + splittingIndex); // XXX
                            groupsElement.appendChild(groupElement);
                        }
                    }

                    {
                        Splitting.Part fakeSplittingPart = fakeSplitting.addPart(_class.getLabel());
                        classFakeResourceMap.put(_class, fakeSplittingPart);
                        String wholeGroupId = splittingPartStore.register(fakeSplittingPart);
                        
                        Element groupElement = xmlDocument.createElement("group");
                        groupElement.setAttribute("id", wholeGroupId);
                        groupElement.setAttribute("name", "Egész osztály"); // XXX
                        groupElement.setAttribute("classid", classid);
                        groupElement.setAttribute("entireclass", "1");
                        groupElement.setAttribute("divisiontag", "0");
                        groupElement.setAttribute("ascttdivision", "0"); // XXX
                        groupsElement.appendChild(groupElement);
                    }
                    
                }

            }
            
            {
                Element lessonsElement = xmlDocument.createElement("lessons");
                lessonsElement.setAttribute("options", "canadd,export:silent");
                lessonsElement.setAttribute("columns", "id,subjectid,classids,groupids,teacherids,classroomids,durationperiods,periodsperweek,capacity");
                rootElement.appendChild(lessonsElement);
                
                Map<Set<Object>, Element> subsetsLessonMap = new HashMap<Set<Object>, Element>();
                Map<Activity, String> activityLessonIdMap = new HashMap<Activity, String>();
                Map<Element, ActivityList> lessonElementActivitiesMap = new HashMap<Element, ActivityList>();
                
                int lessonIndex = 0;
                for (Activity activity: board.getBlocks().getActivities()) {
                    Set<Object> activityMajorAspects = getMajorAspects(activity);
                    String lessonId;
                    Element lessonElement;
                    ActivityList lessonActivityList;
                    if (subsetsLessonMap.containsKey(activityMajorAspects)) {
                        lessonElement = subsetsLessonMap.get(activityMajorAspects);
                        lessonId = lessonElement.getAttribute("id");
                        lessonActivityList = lessonElementActivitiesMap.get(lessonElement);
                    } else {
                        lessonId = "lesson_" + (++lessonIndex);
                        
                        lessonActivityList = new ActivityList();

                        Activity.TagManager activityTagManager = activity.getTagManager();
                        Activity.ResourceManager activityResourceManager = activity.getResourceManager();
                        
                        Set<Tag> activitySubjects = activityTagManager.getTags(Tag.Type.SUBJECT);
                        String subjectidAttr = activitySubjects.isEmpty() ? "" : tagStore.getId(CollectionUtil.getFirst(activitySubjects));
                        
                        List<Resource> activityClasses = activityResourceManager.getResources(Resource.Type.CLASS);
                        String classidsAttr = StringUtil.join(resourceStore.getIds(activityClasses), ",");
                        
                        List<Resource> activityPersons = activityResourceManager.getResources(Resource.Type.PERSON);
                        String teacheridsAttr = StringUtil.join(resourceStore.getIds(activityPersons), ",");
                        
                        List<Resource> activityLocales = activityResourceManager.getResources(Resource.Type.LOCALE);
                        String classroomidsAttr = StringUtil.join(resourceStore.getIds(activityLocales), ",");

                        ResourceSubsetList activityResourceSubsets = activityResourceManager.getResourceSubsets(Resource.Type.CLASS);
                        Set<String> groupidSet = new TreeSet<String>();
                        for (ResourceSubset activityResourceSubset: activityResourceSubsets) {
                            if (activityResourceSubset.isWhole()) {
                                Resource.Splitting.Part splittingPart = classFakeResourceMap.get(activityResourceSubset.getResource());
                                groupidSet.add(splittingPartStore.getId(splittingPart));
                            } else if (activityResourceSubset instanceof ResourceSubset.SplittingPart) {
                                Resource.Splitting.Part splittingPart = ((ResourceSubset.SplittingPart)activityResourceSubset).getSplittingPart();
                                groupidSet.add(splittingPartStore.getId(splittingPart));
                            } else {
                                // TODO (???, kulon <group> es divisiontag="..." mindegyiknek?)
                            }
                        }
                        String groupidsAttr = StringUtil.join(groupidSet, ",");
                        
                        lessonElement = xmlDocument.createElement("lesson");
                        lessonElement.setAttribute("id", lessonId);
                        lessonElement.setAttribute("subjectid", subjectidAttr);
                        lessonElement.setAttribute("classids", classidsAttr);
                        lessonElement.setAttribute("groupids", groupidsAttr);
                        lessonElement.setAttribute("teacherids", teacheridsAttr);
                        lessonElement.setAttribute("classroomids", classroomidsAttr);
                        lessonElement.setAttribute("durationperiods", "1");
                        lessonElement.setAttribute("capacity", "*");
                        lessonsElement.appendChild(lessonElement);
                        lessonElementActivitiesMap.put(lessonElement, lessonActivityList);
                        
                        subsetsLessonMap.put(activityMajorAspects, lessonElement);
                    }
                    activityLessonIdMap.put(activity, lessonId);
                    lessonActivityList.add(activity);
                }
                
                for (Map.Entry<Element, ActivityList> entry: lessonElementActivitiesMap.entrySet()) {
                    Element lessonElement = entry.getKey();
                    ActivityList lessonActivities = entry.getValue();
                    
                    String periodsperweekAttr = "" + ((float)lessonActivities.size() / (float)periodStore.size());
                    if (decimalSeparatorComma) {
                        periodsperweekAttr = periodsperweekAttr.replace('.', ',');
                    }
                    
                    lessonElement.setAttribute("periodsperweek", periodsperweekAttr); // XXX
                }
                
                TraditionalTable traditionalTable = new TraditionalTable(board);
                long dayCount = traditionalTable.getDayCount();
                
                Element cardsElement = xmlDocument.createElement("cards");
                cardsElement.setAttribute("options", "canadd,export:silent");
                cardsElement.setAttribute("columns", "lessonid,subjectid,classids,groupids,teacherids,classroomids,terms,weeks,day,period");
                rootElement.appendChild(cardsElement);
                
                String periodsperweekAttr = "" + (1 / (float)periodStore.size());
                if (decimalSeparatorComma) {
                    periodsperweekAttr = periodsperweekAttr.replace('.', ',');
                }
                
                for (Map.Entry<Long, TreeMap<Integer, Board.EntryList>> dayEntry: traditionalTable.entrySet()) {
                    long day = dayEntry.getKey();
                    
                    String dayAttr = "" + (day + (dayNumberingFrom1 ? 1 : 0));
                    String daysAttr = createDaysAttr(day, dayCount);
                    
                    for (Map.Entry<Integer, Board.EntryList> slotEntry: dayEntry.getValue().entrySet()) {
                        int slot = slotEntry.getKey();
                        
                        String periodAttr = "" + (slot + 1);
                        
                        for (Block block: slotEntry.getValue().getBlocks()) {
                            Block.ActivityManager blockActivityManager = block.getActivityManager();
                            for (Activity activity: blockActivityManager.getActivities()) {
                                String lessonid = activityLessonIdMap.get(activity);
                                
                                Activity.TagManager activityTagManager = activity.getTagManager();
                                Activity.ResourceManager activityResourceManager = activity.getResourceManager();
                                
                                Set<Tag> activitySubjects = activityTagManager.getTags(Tag.Type.SUBJECT);
                                String subjectidAttr = activitySubjects.isEmpty() ? "" : tagStore.getId(CollectionUtil.getFirst(activitySubjects));
                                
                                List<Resource> activityClasses = activityResourceManager.getResources(Resource.Type.CLASS);
                                String classidsAttr = StringUtil.join(resourceStore.getIds(activityClasses), ",");
                                
                                List<Resource> activityPersons = activityResourceManager.getResources(Resource.Type.PERSON);
                                String teacheridsAttr = StringUtil.join(resourceStore.getIds(activityPersons), ",");
                                
                                List<Resource> activityLocales = activityResourceManager.getResources(Resource.Type.LOCALE);
                                String classroomidsAttr = StringUtil.join(resourceStore.getIds(activityLocales), ",");
                                
                                ResourceSubsetList activityResourceSubsets = activityResourceManager.getResourceSubsets(Resource.Type.CLASS);
                                Set<String> groupidSet = new TreeSet<String>();
                                for (ResourceSubset activityResourceSubset: activityResourceSubsets) {
                                    if (activityResourceSubset.isWhole()) {
                                        Resource.Splitting.Part splittingPart = classFakeResourceMap.get(activityResourceSubset.getResource());
                                        groupidSet.add(splittingPartStore.getId(splittingPart));
                                    } else if (activityResourceSubset instanceof ResourceSubset.SplittingPart) {
                                        Resource.Splitting.Part splittingPart = ((ResourceSubset.SplittingPart)activityResourceSubset).getSplittingPart();
                                        groupidSet.add(splittingPartStore.getId(splittingPart));
                                    } else {
                                        // TODO (???, kulon <group> es divisiontag="..." mindegyiknek?)
                                    }
                                }
                                String groupidsAttr = StringUtil.join(groupidSet, ",");
                                
                                for (Period period: blockActivityManager.getActivityPeriods(activity)) {
                                    int term = period.getTerm();
                                    int position = period.getPosition();

                                    String termAttr = (term == 0) ? "" : "" + term;
                                    String positionAttr = (position == 0) ? "" : "" + position;
                                    
                                    Element cardElement = xmlDocument.createElement("card");
                                    cardElement.setAttribute("lessonid", lessonid);
                                    cardElement.setAttribute("subjectid", subjectidAttr);
                                    cardElement.setAttribute("classids", classidsAttr);
                                    cardElement.setAttribute("groupids", groupidsAttr);
                                    cardElement.setAttribute("teacherids", teacheridsAttr);
                                    cardElement.setAttribute("classroomids", classroomidsAttr);
                                    cardElement.setAttribute("terms", termAttr);
                                    cardElement.setAttribute("weeks", positionAttr);
                                    cardElement.setAttribute("day", dayAttr);
                                    cardElement.setAttribute("days", daysAttr);
                                    cardElement.setAttribute("period", periodAttr);
                                    cardsElement.appendChild(cardElement);
                                }
                            }
                        }
                    }
                }
            }

            String encoding = (String)settings.get("encoding");
            if (encoding.isEmpty()) {
                encoding = "UTF-8";
            }
            
            try {
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
                transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                transformer.transform(new DOMSource(xmlDocument), new StreamResult(outFile));
            } catch (TransformerException e) {
                throw new IOException(e);
            }
        }
        
        private Set<Object> getMajorAspects(Activity activity) {
            Set<Object> aspects = new HashSet<Object>();
            
            Activity.TagManager tagManager = activity.getTagManager();
            Activity.ResourceManager resourceManager = activity.getResourceManager();
            
            aspects.addAll(tagManager.getTags(Tag.Type.SUBJECT));
            aspects.addAll(resourceManager.getResourceSubsets(Resource.Type.CLASS));
            aspects.addAll(resourceManager.getResourceSubsets(Resource.Type.PERSON));
            
            return aspects;
        }
        
        private String createDaysAttr(long day, long dayCount) {
            StringBuilder resultBuilder = new StringBuilder();
            for (long i = 0; i < dayCount; i++) {
                if (i == day) {
                    resultBuilder.append('1');
                } else {
                    resultBuilder.append('0');
                }
            }
            return resultBuilder.toString();
        }
        
    }
    
}

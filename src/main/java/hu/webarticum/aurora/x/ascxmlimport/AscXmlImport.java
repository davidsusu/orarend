package hu.webarticum.aurora.x.ascxmlimport;

import static hu.webarticum.aurora.app.Shortcut.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import hu.webarticum.aurora.app.util.AcronymGenerator;
import hu.webarticum.aurora.app.util.BlockUtil;
import hu.webarticum.aurora.app.util.ColorSequenceGenerator;
import hu.webarticum.aurora.app.util.TimingSetDisjoiner;
import hu.webarticum.aurora.core.model.Activity;
import hu.webarticum.aurora.core.model.Activity.ResourceManager;
import hu.webarticum.aurora.core.model.Activity.TagManager;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.BlockList;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Document.BlockStore;
import hu.webarticum.aurora.core.model.Document.PeriodStore;
import hu.webarticum.aurora.core.model.Document.ResourceStore;
import hu.webarticum.aurora.core.model.Document.TagStore;
import hu.webarticum.aurora.core.model.Document.TimingSetStore;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.PeriodSet;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Resource.Splitting;
import hu.webarticum.aurora.core.model.Resource.SplittingManager;
import hu.webarticum.aurora.core.model.Value.ValueList;
import hu.webarticum.aurora.core.model.ResourceSubset;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Time;

public class AscXmlImport {
    
    private static final Pattern SINGLE_BIT_PATTERN = Pattern.compile("^(0*)10*$");
    
    private static final Pattern HEXA_COLOR_PATTERN = Pattern.compile("^#[0-9a-fA-F]{6}$");
    
    private static final List<String> DAY_KEYS;
    static {
        List<String> dayKeys = new ArrayList<>();
        dayKeys.add("monday");
        dayKeys.add("tuesday");
        dayKeys.add("wednesday");
        dayKeys.add("thursday");
        dayKeys.add("friday");
        dayKeys.add("saturday");
        dayKeys.add("sunday");
        DAY_KEYS = Collections.unmodifiableList(dayKeys);
    }
    

    public Document load(URL url) throws IOException {
        try (InputStream urlInputStream = url.openStream()) {
            return load(urlInputStream);
        }
    }

    public Document load(File file) throws IOException {
        try (InputStream fileInputStream = new FileInputStream(file)) {
            return load(fileInputStream);
        }
    }

    public Document load(InputStream inputStream) throws IOException {
        org.w3c.dom.Document xmlDocument;
        try {
            xmlDocument = loadXmlDocument(inputStream);
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw new IOException("Unable to process XML", e);
        }
        return load(xmlDocument);
    }

    private org.w3c.dom.Document loadXmlDocument(InputStream inputStream) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory xmlFactory = createXmlFactory();
        DocumentBuilder documentBuilder = xmlFactory.newDocumentBuilder();
        return documentBuilder.parse(inputStream);
    }
    
    private DocumentBuilderFactory createXmlFactory() throws SAXException, ParserConfigurationException {
        DocumentBuilderFactory xmlFactory = DocumentBuilderFactory.newInstance();
        xmlFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        xmlFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        xmlFactory.setValidating(false);
        xmlFactory.setNamespaceAware(true);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source schemaSource = new StreamSource(getClass().getClassLoader().getResourceAsStream(
            "hu/webarticum/aurora/core/io/xsd/document.xsd"
        ));
        Schema schema = schemaFactory.newSchema(schemaSource);
        xmlFactory.setSchema(schema);
        return xmlFactory;
    }

    public Document load(org.w3c.dom.Document xmlDocument) {
        Map<String, ResourceSubset> groupMap = new HashMap<>();
        
        Document document = new Document("Importált órarend");
        loadPeriods(document, xmlDocument);
        loadSubjects(document, xmlDocument);
        loadClasses(document, xmlDocument, groupMap);
        loadLocations(document, xmlDocument);
        loadTimingSets(document, xmlDocument);
        loadPersons(document, xmlDocument);
        loadBoardsAndBlocks(document, xmlDocument, groupMap);
        
        // TODO tables, placements
        
        return document;
    }
    
    private void loadPeriods(Document document, org.w3c.dom.Document xmlDocument) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element termsdefsElement = findFirstByTagName(rootElement, "termsdefs");
        if (termsdefsElement == null) {
            return;
        }
        
        Element weeksdefsElement = findFirstByTagName(rootElement, "weeksdefs");
        if (weeksdefsElement == null) {
            return;
        }

        PeriodStore periodStore = document.getPeriodStore();
        for (Element termsdefElement : findAllByTagName(termsdefsElement, "termsdef")) {
            for (Element weeksdefElement : findAllByTagName(weeksdefsElement, "weeksdef")) {
                loadPeriod(periodStore, termsdefElement, weeksdefElement);
            }
        }
    }
    
    private void loadPeriod(PeriodStore periodStore, Element termsdefElement, Element weeksdefElement) {
        int termIndex = getBitIndex(termsdefElement.getAttribute("terms"));
        if (termIndex == -1) {
            return;
        }

        int weekIndex = getBitIndex(weeksdefElement.getAttribute("weeks"));
        if (weekIndex == -1) {
            return;
        }
        
        String label = termsdefElement.getAttribute("name") + " / " + weeksdefElement.getAttribute("name");
        Period period = new Period(label, termIndex + 1, weekIndex + 1);
        String termId = getAttributeOrDefault(termsdefElement, "id", "XXX");
        String weekId = getAttributeOrDefault(weeksdefElement, "id", "XXX");
        String id = termId + "_" + weekId;
        
        periodStore.register(period, id);
    }

    private void loadSubjects(Document document, org.w3c.dom.Document xmlDocument) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element subjectsElement = findFirstByTagName(rootElement, "subjects");
        if (subjectsElement == null) {
            return;
        }

        TagStore tagStore = document.getTagStore();
        ColorSequenceGenerator colorGenerator = new ColorSequenceGenerator();
        for (Element subjectElement : findAllByTagName(subjectsElement, "subject")) {
            loadSubject(tagStore, subjectElement, colorGenerator.next());
        }
    }

    private void loadSubject(TagStore tagStore, Element subjectElement, Color color) {
        String label = subjectElement.getAttribute("name");
        String acronym = getAttributeOrDefault(subjectElement, "short", AcronymGenerator.generateAcronym(label));
        Tag subject = new Tag(Tag.Type.SUBJECT, label, acronym, color);
        String id = getAttributeOrDefault(subjectElement, "id", () -> tagStore.getDefaultIdFor(subject));
        tagStore.register(subject, id);
    }

    private void loadClasses(Document document, org.w3c.dom.Document xmlDocument, Map<String, ResourceSubset> groupMap) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element classesElement = findFirstByTagName(rootElement, "classes");
        if (classesElement == null) {
            return;
        }

        for (Element classElement : findAllByTagName(classesElement, "class")) {
            loadClass(document, xmlDocument, classElement, groupMap);
        }
    }
    
    private void loadClass(Document document, org.w3c.dom.Document xmlDocument, Element classElement, Map<String, ResourceSubset> groupMap) {
        ResourceStore resourceStore = document.getResourceStore();
        String label = classElement.getAttribute("name");
        String acronym = getAttributeOrDefault(classElement, "short", AcronymGenerator.generateAcronym(label));
        Color color = Color.WHITE; // XXX
        Resource resource = new Resource(Resource.Type.CLASS, label, acronym, color);
        
        // TODO: time limit <-- timeoff
        
        String id = getAttributeOrDefault(classElement, "id", () -> resourceStore.getDefaultIdFor(resource));
        loadClassGroups(resource, id, xmlDocument, groupMap);
        resourceStore.register(resource, id);
    }
    
    private void loadClassGroups(Resource resource, String id, org.w3c.dom.Document xmlDocument, Map<String, ResourceSubset> groupMap) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element groupsElement = findFirstByTagName(rootElement, "groups");
        if (groupsElement == null) {
            return;
        }

        SortedMap<Integer, LinkedHashMap<String, String>> groupStructure = new TreeMap<>();
        for (Element groupElement : findAllByTagName(groupsElement, "group")) {
            registerGroupInStructures(resource, id, groupElement, groupStructure, groupMap);
        }
        
        SplittingManager splittingManager = resource.getSplittingManager();
        for (Map.Entry<Integer, LinkedHashMap<String, String>> entry : groupStructure.entrySet()) {
            int splittingNo = entry.getKey();
            String splittingLabel = "Bontás " + splittingNo; // FIXME
            Splitting splitting = splittingManager.add(splittingLabel);
            for (Map.Entry<String, String> subEntry : entry.getValue().entrySet()) {
                String groupId = subEntry.getKey();
                String groupLabel = subEntry.getValue();
                Splitting.Part splittingPart = splitting.addPart(groupLabel);
                groupMap.put(groupId, new ResourceSubset.SplittingPart(splittingPart));
            }
        }
    }
    
    private void registerGroupInStructures(
            Resource resource,
            String id,
            Element groupElement,
            SortedMap<Integer, LinkedHashMap<String, String>> groupStructure,
            Map<String, ResourceSubset> groupMap) {
        String groupId = groupElement.getAttribute("id");
        String classid = groupElement.getAttribute("classid");
        if (!classid.equals(id)) {
            return;
        }
        
        String entireclassStr = groupElement.getAttribute("entireclass");
        if (entireclassStr.equals("1")) {
            groupMap.put(groupId, new ResourceSubset.Whole(resource));
            return;
        }
        
        int divisionNo;
        try {
            divisionNo = Integer.parseInt(groupElement.getAttribute("divisiontag"));
        } catch (NumberFormatException e) {
            return;
        }
        
        LinkedHashMap<String, String> groups = groupStructure.computeIfAbsent(divisionNo, k -> new LinkedHashMap<>());
        String label = getAttributeOrDefault(groupElement, "name", "" + groups.size());
        groups.put(groupId, label);
    }

    private void loadLocations(Document document, org.w3c.dom.Document xmlDocument) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element classroomsElement = findFirstByTagName(rootElement, "classrooms");
        if (classroomsElement == null) {
            return;
        }

        ResourceStore resourceStore = document.getResourceStore();
        for (Element classroomElement : findAllByTagName(classroomsElement, "classroom")) {
            loadLocation(resourceStore, classroomElement);
        }
    }

    private void loadLocation(ResourceStore resourceStore, Element classroomElement) {
        String label = classroomElement.getAttribute("name");
        String acronym = getAttributeOrDefault(classroomElement, "short", AcronymGenerator.generateAcronym(label));
        Color color = Color.WHITE;
        Resource resource = new Resource(Resource.Type.LOCALE, label, acronym, color);
        String id = getAttributeOrDefault(classroomElement, "id", () -> resourceStore.getDefaultIdFor(resource));

        // TODO: time limit <-- timeoff
        
        resourceStore.register(resource, id);
    }
    
    private void loadTimingSets(Document document, org.w3c.dom.Document xmlDocument) {
        TimingSet defaultTimingSet = extractDefaultTimingSet(xmlDocument);
        if (defaultTimingSet == null) {
            return;
        }
        
        TimingSetStore timingSetStore = document.getTimingSetStore();
        timingSetStore.register(defaultTimingSet);
        
        // TODO: custom timing sets (dayperiods, custombells, classtimetables)
        
        for (Resource clazz : document.getResourceStore().getAll(Resource.Type.CLASS)) {
            clazz.setTimingSetEnabled(true);
            clazz.getTimingSetManager().setDefaultTimingSet(defaultTimingSet);
        }
    }
    
    private TimingSet extractDefaultTimingSet(org.w3c.dom.Document xmlDocument) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element periodsElement = findFirstByTagName(rootElement, "periods");
        if (periodsElement == null) {
            return null;
        }
        
        TimingSet defaultTimingSet = new TimingSet("Alapértelmezett időbeosztás"); // XXX
        SortedMap<Integer, String> dayShortNames = extractDayShortNames(xmlDocument);
        for (Map.Entry<Integer, String> entry : dayShortNames.entrySet()) {
            int dayIndex = entry.getKey();
            String dayShortName = entry.getValue();
            for (Element periodElement : findAllByTagName(periodsElement, "period")) {
                String slotIndexStr = periodElement.getAttribute("period");
                int slotIndex = Integer.parseInt(slotIndexStr);
                
                String timeString = periodElement.getAttribute("starttime");
                long timeSeconds;
                if (!timeString.isEmpty()) {
                    timeSeconds = Time.parseTimeString(timeString + ":00");
                } else {
                    timeSeconds = (8L + slotIndex) * Time.HOUR;
                }
                long daySeconds = dayIndex * Time.DAY;
                Time time = new Time(daySeconds + timeSeconds);
                
                String slotName = periodElement.getAttribute("name");
                if (slotName.isEmpty()) {
                    slotName = slotIndexStr;
                }
                String slotLabel = dayShortName + " " + slotName;
                defaultTimingSet.add(time, slotLabel);
            }
        }
        return defaultTimingSet;
    }
    
    private SortedMap<Integer, String> extractDayShortNames(org.w3c.dom.Document xmlDocument) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element daysElement = findFirstByTagName(rootElement, "days");
        if (daysElement != null) {
            return extractDayShortNamesFromDaysElement(daysElement);
        }

        Element daysdefsElement = findFirstByTagName(rootElement, "daysdefs");
        if (daysdefsElement != null) {
            return extractDayShortNamesFromDaysdefsElement(daysdefsElement);
        }
        
        return new TreeMap<>();
    }

    private SortedMap<Integer, String> extractDayShortNamesFromDaysElement(Element daysElement) {
        SortedMap<Integer, String> dayNames = new TreeMap<>();
        for (Element dayElement : findAllByTagName(daysElement, "day")) {
            String dayIndexStr = dayElement.getAttribute("short");
            if (dayIndexStr.isEmpty()) {
                continue;
            }
            
            int dayIndex = Integer.parseInt(dayIndexStr); // FIXME: from 0 or 1?
            String dayShortName = extractShortName(dayElement, dayIndexStr); // FIXME: from 0 or 1?
            dayNames.put(dayIndex, dayShortName);
        }
        return dayNames;
    }

    private SortedMap<Integer, String> extractDayShortNamesFromDaysdefsElement(Element daysdefsElement) {
        SortedMap<Integer, String> dayNames = new TreeMap<>();
        int dayCount = 1;
        for (Element daysdefElement : findAllByTagName(daysdefsElement, "daysdef")) {
            String dayBits = daysdefElement.getAttribute("days");
            if (dayBits.isEmpty()) {
                continue;
            }
            
            int bitIndex = getBitIndex(dayBits);
            if (bitIndex == -1) {
                continue;
            }

            int bitCount = dayBits.length();
            if (bitCount > dayCount) {
                dayCount = bitCount;
            }
            
            String dayShortName = extractShortName(daysdefElement, "" + bitIndex); // FIXME: from 0 or 1?
            dayNames.put(bitIndex, dayShortName);
        }
        for (int i = 0; i < dayCount; i++) {
            if (!dayNames.containsKey(i)) {
                dayNames.put(i, getBuildInDayShortName(i));
            }
        }
        return dayNames;
    }
    
    private String getBuildInDayShortName(int dayIndex) {
        int weekIndex = dayIndex / 7;
        int weekDayIndex = dayIndex % 7;
        if (weekDayIndex < 0) {
            weekDayIndex = weekDayIndex + 7;
        }
        
        String dayShortName = text("core.day." + DAY_KEYS.get(weekDayIndex) + ".abbr");
        if (weekIndex != 0) {
            dayShortName = (weekIndex + 1) + "/" + dayShortName;
        }
        return dayShortName;
    }
    
    private String extractShortName(Element element, String defaultValue) {
        String shortName = element.getAttribute("short");
        if (!shortName.isEmpty()) {
            return shortName;
        }
        
        String name = element.getAttribute("name");
        if (name.isEmpty()) {
            return name;
        }
        
        return defaultValue;
    }
    
    private void loadPersons(Document document, org.w3c.dom.Document xmlDocument) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element teachersElement = findFirstByTagName(rootElement, "teachers");
        if (teachersElement == null) {
            return;
        }

        ResourceStore resourceStore = document.getResourceStore();
        for (Element teacherElement : findAllByTagName(teachersElement, "teacher")) {
            loadPerson(resourceStore, teacherElement);
        }
    }
    
    private void loadPerson(ResourceStore resourceStore, Element teacherElement) {
        String label = teacherElement.getAttribute("name");
        String acronym = getAttributeOrDefault(teacherElement, "short", AcronymGenerator.generateAcronym(label));
        Color color = parseColor(teacherElement.getAttribute("color"));
        String email = teacherElement.getAttribute("email");
        Resource resource = new Resource(Resource.Type.PERSON, label, acronym, color);
        resource.setEmail(email);
        String id = getAttributeOrDefault(teacherElement, "id", () -> resourceStore.getDefaultIdFor(resource));

        // TODO: time limit <-- timeoff
        
        resourceStore.register(resource, id);
    }

    private void loadBoardsAndBlocks(Document document, org.w3c.dom.Document xmlDocument, Map<String, ResourceSubset> groupMap) {
        Element rootElement = xmlDocument.getDocumentElement();
        Element lessonsElement = findFirstByTagName(rootElement, "lessons");
        if (lessonsElement == null) {
            return;
        }

        Board boardForFixedBlocks = new Board("Fix órák"); // XXX
        Board boardForPlacedBlocks = new Board("Importált órarend"); // XXX
        for (Element lessonElement : findAllByTagName(lessonsElement, "lesson")) {
            loadBlocksFromLesson(document, xmlDocument, lessonElement, groupMap, boardForFixedBlocks, boardForPlacedBlocks);
        }
        
        if (!boardForFixedBlocks.isEmpty()) {
            document.getBoardStore().register(boardForFixedBlocks);
        }
        if (!boardForPlacedBlocks.isEmpty()) {
            document.getBoardStore().register(boardForPlacedBlocks);
        }
    }

    private void loadBlocksFromLesson(
            Document document,
            org.w3c.dom.Document xmlDocument,
            Element lessonElement,
            Map<String, ResourceSubset> groupMap,
            Board boardForFixedBlocks,
            Board boardForPlacedBlocks) {
        String lessonId = lessonElement.getAttribute("id");
        
        int durationPeriods = Integer.parseInt(getAnyAttributeOrDefault(lessonElement, new String[] { "durationperiods", "periodspercard" }, "1"));
        float periodsPerWeek = Float.parseFloat(getAttributeOrDefault(lessonElement, "periodsperweek", "1.0"));
        
        int blockCount = Math.round(periodsPerWeek); // TODO
        int fullBlockStackCount = blockCount / durationPeriods;

        Block sampleBlock = createSampleBlock(document, lessonElement, groupMap);
        TimingSet timingSet = extractTimingSet(document, sampleBlock);
        List<Block> unhandledBlocks = registerAndGetBlocksFromSample(sampleBlock, blockCount, lessonId, document.getBlockStore());
        List<BlockList> stacks = createAndGetStacks(document, unhandledBlocks, fullBlockStackCount);
        SortedMap<Integer, SortedMap<Integer, Element>> placedCards = collectCards(xmlDocument, lessonId);
        SortedMap<Time, Boolean> unfilledSlotTimes = new TreeMap<>();

        Iterator<BlockList> stackIterator = stacks.iterator();
        for (Map.Entry<Integer, SortedMap<Integer, Element>> dayEntry : placedCards.entrySet()) {
            int day = dayEntry.getKey();
            boolean isProperlyStacked = false; // TODO
            BlockList stackBlocks = null;
            if (isProperlyStacked && stackIterator.hasNext()) {
                stackBlocks = stackIterator.next();
            }
            int i = 0;
            for (Map.Entry<Integer, Element> entry : dayEntry.getValue().entrySet()) {
                int slot = entry.getKey();
                Element cardElement = entry.getValue();
                boolean locked = cardElement.getAttribute("locked").equals("1");
                Time time = extractSlotTime(sampleBlock, timingSet, day, slot);
                if (stackBlocks != null) {
                    Block block = stackBlocks.get(i);
                    boardForPlacedBlocks.add(block, time);
                    if (locked) {
                        boardForFixedBlocks.add(block, time);
                    }
                    unhandledBlocks.remove(block);
                } else {
                    unfilledSlotTimes.put(time, locked);
                }
                i++;
            }
        }
        
        Iterator<Block> blockIterator = unhandledBlocks.iterator();
        for (Map.Entry<Time, Boolean> entry : unfilledSlotTimes.entrySet()) {
            if (!blockIterator.hasNext()) {
                break;
            }
            
            Time time = entry.getKey();
            boolean locked = entry.getValue();
            Block block = blockIterator.next();
            boardForPlacedBlocks.add(block, time);
            if (locked) {
                boardForFixedBlocks.add(block, time);
            }
        }
    }
    
    private Block createSampleBlock(Document document, Element lessonElement, Map<String, ResourceSubset> groupMap) {
        Activity activity = new Activity();
        
        TagStore tagStore = document.getTagStore();
        TagManager tagManager = activity.getTagManager();
        List<Tag> subjects = findItems(lessonElement, "subjectid", null, tagStore::get);
        subjects.forEach(tagManager::add);

        ResourceStore resourceStore = document.getResourceStore();
        ResourceManager resourceManager = activity.getResourceManager();
        //findItems(lessonElement, "classid", "classids", resourceStore::get).forEach(resourceManager::add); // XXX
        findItems(lessonElement, "teacherid", "teacherids", resourceStore::get).forEach(resourceManager::add);
        findItems(lessonElement, null, "classroomids", resourceStore::get).forEach(resourceManager::add);
        List<ResourceSubset> groups = findItems(lessonElement, "groupid", "groupids", groupMap::get);
        groups.forEach(resourceManager::add);

        List<String> labelTokens = new ArrayList<>();
        for (ResourceSubset group : groups) {
            if (group.isWhole()) {
                labelTokens.add(group.getResource().getLabel());
            } else if (group instanceof ResourceSubset.SplittingPart) {
                ResourceSubset.SplittingPart splittingPart = (ResourceSubset.SplittingPart) group;
                Resource resource = group.getResource();
                Splitting.Part part = splittingPart.getSplittingPart();
                labelTokens.add(resource.getLabel() + "/" + part.getLabel());
            }
        }
        if (!subjects.isEmpty()) {
            labelTokens.add(subjects.get(0).getLabel());
        }
        String label = "Tevékenység"; // FIXME
        if (!labelTokens.isEmpty()) {
            label = String.join(" ", labelTokens);
        }
        
        Period samplePeriod = document.getPeriodStore().getAll().iterator().next(); // TODO: more periods...
        PeriodSet periods = new PeriodSet(samplePeriod); // FIXME

        
        activity.setLabel(label);
        Block sampleBlock = new Block(label);
        sampleBlock.getActivityManager().add(activity, periods);
        
        return sampleBlock;
    }
    
    private List<Block> registerAndGetBlocksFromSample(Block sampleBlock, int count, String baseId, BlockStore blockStore) {
        List<Block> unhandledBlocks = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Block block = new Block(sampleBlock);
            blockStore.register(block, baseId);
            unhandledBlocks.add(block);
        }
        return unhandledBlocks;
    }
    
    private List<BlockList> createAndGetStacks(Document document, List<Block> blocks, int stackSize) {
        List<BlockList> stacks = new ArrayList<>();
        if (stackSize <= 1) {
            return stacks;
        }

        ValueList followingValues = document.getExtraData().getAccess("autoput.school.setup.followings").getReferenced(Value.Type.LIST).getAsList();
        
        int stackCount = blocks.size() / stackSize;
        for (int i = 0; i < stackCount; i++) {
            BlockList stack = new BlockList();
            Block previousBlock = null;
            for (int j = 0; j < stackSize; j++) {
                int blockNo = (i * stackSize) + j;
                Block block = blocks.get(blockNo);
                stack.add(block);
                if (previousBlock != null) {
                    Value followingData = new Value(Value.Type.MAP);
                    Value.ValueMap followingDataMap = followingData.getAsMap();
                    followingDataMap.putRaw("ordered", true);
                    followingDataMap.putRaw("block1", previousBlock);
                    followingDataMap.putRaw("block2", block);
                    followingDataMap.putRaw("comment", "");
                    followingValues.add(followingData);
                }
                previousBlock = block;
            }
            stacks.add(stack);
        }
        
        return stacks;
    }

    private TimingSet extractTimingSet(Document document, Block block) {
        TimingSet rawTimingSet = extractRawTimingSet(document, block);
        TimingSetDisjoiner disjoiner = new TimingSetDisjoiner(rawTimingSet);
        long blockLength = BlockUtil.detectMedianBlockLength(document.getBlockStore().getAll());
        return disjoiner.calculateMaximalDisjoint(blockLength);
    }
    
    private TimingSet extractRawTimingSet(Document document, Block block) {
        for (Activity activity : block.getActivityManager().getActivities()) {
            for (Resource clazz : activity.getResourceManager().getResources(Resource.Type.CLASS)) {
                if (clazz.isTimingSetEnabled()) {
                    TimingSet timingSet = clazz.getTimingSetManager().getDefaultTimingSet();
                    if (timingSet != null) {
                        return timingSet;
                    }
                }
            }
        }
        
        return document.getTimingSetStore().iterator().next();
    }
    
    private Time extractSlotTime(Block block, TimingSet timingSet, int day, int slot) {
        TimingSet dayTimingSet = timingSet.getLimited(new Interval(new Time(Time.DAY * day), Time.DAY), block.getLength());
        List<Time> dayTimes = new ArrayList<>(dayTimingSet.getTimes());
        if (dayTimes.isEmpty()) {
            return new Time(Time.DAY + (Time.HOUR * 8)); // XXX
        } else if (slot >= dayTimes.size()) {
            return dayTimes.get(dayTimes.size() - 1).getMoved(block.getLength() + (Time.MINUTE * 10)); // XXX
        }
        
        return dayTimes.get(slot);
    }

    private SortedMap<Integer, SortedMap<Integer, Element>> collectCards(org.w3c.dom.Document xmlDocument, String lessonId) {
        SortedMap<Integer, SortedMap<Integer, Element>> result = new TreeMap<>();
        
        Element cardsElement = findFirstByTagName(xmlDocument.getDocumentElement(), "cards");
        if (cardsElement == null) {
            return result;
        }
        
        for (Element cardElement : findAllByTagName(cardsElement, "card")) {
            if (!cardElement.getAttribute("lessonid").equals(lessonId)) {
                continue;
            }
            
            int day;
            String dayAttr = cardElement.getAttribute("day");
            if (!dayAttr.isEmpty()) {
                day = Integer.parseInt(dayAttr);
            } else {
                day = getBitIndex(cardElement.getAttribute("days"));
            }

            int slot = Integer.parseInt(cardElement.getAttribute("period"));
            
            result.computeIfAbsent(day, d -> new TreeMap<>()).put(slot, cardElement);
        }
        
        return result;
    }
    
    private <T> List<T> findItems(Element element, String singleIdAttr, String idsAttr, Function<String, T> finder) {
        Set<String> classIds = new HashSet<>();
        if (singleIdAttr != null) {
            String idValue = element.getAttribute(singleIdAttr);
            if (!idValue.isEmpty()) {
                classIds.add(idValue);
            }
        }
        if (idsAttr != null) {
            String idsValue = element.getAttribute(idsAttr);
            if (!idsValue.isEmpty()) {
                classIds.addAll(Arrays.asList(idsValue.split(",")));
            }
        }
        
        List<T> result = new ArrayList<>();
        for (String classId : classIds) {
            T resource = finder.apply(classId);
            if (resource != null) {
                result.add(resource);
            }
        }
        return result;
    }
    
    private int getBitIndex(String bitStr) {
        Matcher matcher = SINGLE_BIT_PATTERN.matcher(bitStr);
        if (!matcher.find()) {
            return -1;
        }
        
        return matcher.group(1).length();
    }
    
    private Color parseColor(String colorAttribute) {
        if (!HEXA_COLOR_PATTERN.matcher(colorAttribute).matches()) {
            return Color.WHITE;
        }
        
        return new Color(colorAttribute);
    }

    private Element findFirstByTagName(Element element, String tagName) {
        boolean matchAll = (tagName.equals("*"));
        NodeList nodeList = element.getChildNodes();
        int size = nodeList.getLength();
        for (int i = 0; i < size; i++) {
            Node childNode = nodeList.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                Element childElement = (Element)childNode;
                if (matchAll || childElement.getTagName().equals(tagName)) {
                    return childElement;
                }
            }
        }
        return null;
    }
    
    private List<Element> findAllByTagName(Element element, String tagName) {
        List<Element> elements = new ArrayList<Element>();
        boolean matchAll = (tagName.equals("*"));
        NodeList nodeList = element.getChildNodes();
        int size = nodeList.getLength();
        for (int i = 0; i < size; i++) {
            Node childNode = nodeList.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                Element childElement = (Element)childNode;
                if (matchAll || childElement.getTagName().equals(tagName)) {
                    elements.add(childElement);
                }
            }
        }
        return elements;
    }

    private String getAttributeOrDefault(Element element, String attributeName, String defaultValue) {
        return getAttributeOrDefault(element, attributeName, () -> defaultValue);
    }
    
    private String getAttributeOrDefault(Element element, String attributeName, Supplier<String> fallbackSupplier) {
        return getAnyAttributeOrDefault(element, new String[] { attributeName }, fallbackSupplier);
    }

    private String getAnyAttributeOrDefault(Element element, String[] attributeNames, String defaultValue) {
        return getAnyAttributeOrDefault(element, attributeNames, () -> defaultValue);
    }
    
    private String getAnyAttributeOrDefault(Element element, String[] attributeNames, Supplier<String> fallbackSupplier) {
        for (String attributeName : attributeNames) {
            String value = element.getAttribute(attributeName);
            if (!value.isEmpty()) {
                return value;
            }
        }
        return fallbackSupplier.get();
    }
    
}

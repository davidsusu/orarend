package hu.webarticum.aurora.x.documentwizard;

import static hu.webarticum.aurora.app.Shortcut.text;
import static hu.webarticum.aurora.app.Shortcut.texts;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.util.i18n.CombinedMultiLanguageTextRepository;
import hu.webarticum.aurora.app.util.i18n.PropertiesLanguageTextRepository;
import hu.webarticum.aurora.core.model.Board;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.Period;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.TimingSet;
import hu.webarticum.aurora.core.model.time.Time;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.Dialog;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualButton;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualCheckBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualComboBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualTabbedPane;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;

public class NewDocumentWizardDialog extends Dialog {
    
    private static final long serialVersionUID = 1L;
    
    private Document resultDocument;
    
    private MultilingualTabbedPane wizardPane;

    private JButton cancelButton;
    
    private JButton previousButton;
    
    private JButton nextButton;
    
    private JButton finishButton;
    
    private JPanel commonsPanel;
    
    private JTextField documentLabelField;
    
    private MultilingualComboBox<String> weeksComboBox;
    
    private JCheckBox addTableCheckBox;
    
    private JPanel teachersPanel;
    
    private LabelAcronymColorTableComponent teachersTable;
    
    private JPanel subjectsPanel;

    private LabelAcronymColorTableComponent subjectsTable;
    
    private JPanel classesPanel;

    private LabelAcronymColorTableComponent classesTable;
    
    private JPanel timingsetPanel;
    
    private JCheckBox enableTimingSetCheckBox;
    
    private JSpinner numberOfDaysSpinner;

    private JSpinner numberOfStudiesSpinner;
    
    private JSpinner[] studyStartSpinners;
    
    private int[][] studyStartTimes = new int[][]{
        {8, 0},
        {8, 55},
        {9, 55},
        {10, 55},
        {11, 50},
        {12, 45},
        {13, 40},
        {14, 35},
        {15, 30},
        {16, 25},
    };
    
    private boolean langInited = false;
    
    
    
    
    public NewDocumentWizardDialog(JFrame parent) {
        super(parent);
        initLanguages();
        this.title = text("x.documentwizard.title");
        init();
    }

    
    protected void initLanguages() {
        if (!langInited) {

            CombinedMultiLanguageTextRepository langRepository = new CombinedMultiLanguageTextRepository();
            langRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/x/documentwizard/lang/documentwizard.hu_HU.properties", "hu_HU"));
            langRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/x/documentwizard/lang/documentwizard.en_US.properties", "en_US"));
            texts().mount("x.documentwizard", langRepository);
            
            langInited = true;
        }
    }
    
    
    @Override
    protected void build() {
        wizardPane = new MultilingualTabbedPane();
        wizardPane.setTabPlacement(JTabbedPane.LEFT);
        wizardPane.setPreferredSize(new Dimension(650, 500));
        containerPanel.add(wizardPane, BorderLayout.CENTER);

        buildCommonsPanel();
        buildTeachersPanel();
        buildSubjectsPanel();
        buildClassesPanel();
        buildTimingsetPanel();

        addTab(IconLoader.loadIcon("wizard_commons"), "x.documentwizard.tab.commons.tabtext", commonsPanel);
        addTab(IconLoader.loadIcon("wizard_teachers"), "x.documentwizard.tab.teachers.tabtext", teachersPanel);
        addTab(IconLoader.loadIcon("wizard_subjects"), "x.documentwizard.tab.subjects.tabtext", subjectsPanel);
        addTab(IconLoader.loadIcon("wizard_classes"), "x.documentwizard.tab.classes.tabtext", classesPanel);
        addTab(IconLoader.loadIcon("wizard_timingsets"), "x.documentwizard.tab.timingsets.tabtext", timingsetPanel);
        
        JPanel buttonOuterPanel = new JPanel();
        buttonOuterPanel.setLayout(new BorderLayout());
        buttonOuterPanel.setBorder(new EmptyBorder(5, 10, 5, 10));
        containerPanel.add(buttonOuterPanel, BorderLayout.PAGE_END);

        JLabel buttonDummyLeftLabel = new JLabel();
        buttonDummyLeftLabel.setPreferredSize(new Dimension(220, 25));
        buttonOuterPanel.add(buttonDummyLeftLabel, BorderLayout.LINE_START);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        buttonOuterPanel.add(buttonPanel, BorderLayout.CENTER);
        
        JPanel buttonLeftPanel = new JPanel();
        buttonLeftPanel.setLayout(new BoxLayout(buttonLeftPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(buttonLeftPanel, BorderLayout.LINE_START);
        
        JPanel buttonRightPanel = new JPanel();
        buttonRightPanel.setLayout(new BoxLayout(buttonRightPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(buttonRightPanel, BorderLayout.LINE_END);

        cancelButton = new MultilingualButton("x.documentwizard.navigation.cancel");
        cancelButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                cancelDialog();
            }
            
        });
        buttonLeftPanel.add(cancelButton);
        
        previousButton = new MultilingualButton("x.documentwizard.navigation.back_with_arrow");
        previousButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                int currentTabIndex = wizardPane.getSelectedIndex();
                if (currentTabIndex>0) {
                    wizardPane.setSelectedIndex(currentTabIndex-1);
                }
            }
            
        });
        buttonLeftPanel.add(previousButton);
        
        nextButton = new MultilingualButton("x.documentwizard.navigation.next_with_arrow");
        nextButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                int tabCount = wizardPane.getTabCount();
                int currentTabIndex = wizardPane.getSelectedIndex();
                if (currentTabIndex<tabCount-1) {
                    wizardPane.setSelectedIndex(currentTabIndex+1);
                }
            }
            
        });
        buttonRightPanel.add(nextButton);
        
        finishButton = new MultilingualButton("x.documentwizard.navigation.finish");
        finishButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                finishWizard();
            }
            
        });
        buttonRightPanel.add(finishButton);

        wizardPane.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent ev) {
                refreshButtons();
            }
            
        });
        refreshTimingSetsControls();
        refreshButtons();
        
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                documentLabelField.selectAll();
                documentLabelField.requestFocusInWindow();
            }
            
        });
    }
    
    
    public void refreshButtons() {
        int tabCount = wizardPane.getTabCount();
        int currentTabIndex = wizardPane.getSelectedIndex();
        previousButton.setEnabled(currentTabIndex>0);
        nextButton.setEnabled(currentTabIndex<tabCount-1);
    }
    
    
    private JPanel createTabPanelWithLabelAcronymColorTable(final LabelAcronymColorTableComponent tablePanel, ImageIcon icon, String titlePath, String sortInfoPath, String addInfoPath) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        
        JPanel innerPanel = new JPanel();
        BoxLayout panelLayout = new BoxLayout(innerPanel, BoxLayout.PAGE_AXIS);
        innerPanel.setLayout(panelLayout);
        panel.add(innerPanel, BorderLayout.PAGE_START);
        
        JPanel topPanel = new JPanel(new BorderLayout());
        topPanel.setBorder(new EmptyBorder(7, 3, 7, 3));
        innerPanel.add(topPanel);
        
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.LINE_AXIS));
        topPanel.add(buttonsPanel, BorderLayout.LINE_END);
        
        JPanel topLeftPanel = new JPanel();
        topLeftPanel.setLayout(new BoxLayout(topLeftPanel, BoxLayout.LINE_AXIS));
        topPanel.add(topLeftPanel, BorderLayout.LINE_START);
        
        JLabel titleLabel = createTitleLabelWithIcon(titlePath, icon);
        topLeftPanel.add(titleLabel);
        
        JButton sortButton = new MultilingualButton(IconLoader.loadIcon("sort"), sortInfoPath);
        sortButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tablePanel.sort();
            }
            
        });
        buttonsPanel.add(sortButton);

        JButton addButton = new MultilingualButton(IconLoader.loadIcon("add"), addInfoPath);
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                tablePanel.addRow();
            }
            
        });
        buttonsPanel.add(addButton);

        innerPanel.add(tablePanel);
        
        return panel;
    }
    
    private JLabel createTitleLabelWithIcon(String titlePath, ImageIcon icon) {
        JLabel titleLabel = new MultilingualLabel(titlePath);
        titleLabel.setIcon(icon);
        titleLabel.setIconTextGap(10);
        titleLabel.setBorder(new EmptyBorder(5, 10, 5, 10));
        {
            Map<TextAttribute, Object> fontAttributes = new HashMap<TextAttribute, Object>();
            fontAttributes.put(TextAttribute.FAMILY, Font.DIALOG);
            fontAttributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
            fontAttributes.put(TextAttribute.SIZE, 12);
            titleLabel.setFont(new Font(fontAttributes));
        }
        return titleLabel;
    }
    

    private void buildCommonsPanel() {
        commonsPanel = new JPanel();
        commonsPanel.setLayout(new BorderLayout());
        
        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.PAGE_AXIS));
        commonsPanel.add(innerPanel, BorderLayout.PAGE_START);

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        topPanel.setBorder(new EmptyBorder(7, 3, 7, 3));
        
        innerPanel.add(topPanel);

        JLabel titleLabel = createTitleLabelWithIcon("x.documentwizard.tab.commons.title", IconLoader.loadIcon("wizard_commons"));
        topPanel.add(titleLabel, BorderLayout.LINE_START);
        
        JPanel dataPanel = new JPanel();
        dataPanel.setLayout(new BoxLayout(dataPanel, BoxLayout.PAGE_AXIS));
        dataPanel.setBorder(new EmptyBorder(7, 7, 7, 7));
        innerPanel.add(dataPanel);
        
        documentLabelField = new JTextField();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int nextYear = year+1;
        String schoolYearName = String.format(
            text("x.documentwizard.tab.commons.default_document_label"),
            year, nextYear
        );
        documentLabelField.setText(schoolYearName);
        documentLabelField.setPreferredSize(new Dimension(250, 30));
        dataPanel.add(createRow(new MultilingualLabel("x.documentwizard.tab.commons.document_label"), documentLabelField, 150));
        
        weeksComboBox = new MultilingualComboBox<String>();
        weeksComboBox.addMultilingualItem("x.documentwizard.tab.commons.weeks.single", "single");
        weeksComboBox.addMultilingualItem("x.documentwizard.tab.commons.weeks.ab", "ab");
        weeksComboBox.addMultilingualItem("x.documentwizard.tab.commons.weeks.12", "12");
        weeksComboBox.addMultilingualItem("x.documentwizard.tab.commons.weeks.12ab", "12ab");
        weeksComboBox.setSelectedIndex(0);
        weeksComboBox.setPreferredSize(new Dimension(180, 30));
        dataPanel.add(createRow(new MultilingualLabel("x.documentwizard.tab.commons.weeks"), weeksComboBox, 150));
        
        addTableCheckBox = new MultilingualCheckBox("x.documentwizard.tab.commons.add_empty_board");
        addTableCheckBox.setSelected(true);
        dataPanel.add(createRow(new MultilingualLabel("x.documentwizard.tab.commons.boards"), addTableCheckBox, 150));
    }
    

    private void buildTeachersPanel() {
        teachersTable = new LabelAcronymColorTableComponent(
            new String[]{
                text("x.documentwizard.tab.teachers.table.label"),
                text("x.documentwizard.tab.teachers.table.acronym"),
                text("x.documentwizard.tab.teachers.table.color"),
            },
            text("x.documentwizard.tab.teachers.table.add")
        );
        teachersPanel = createTabPanelWithLabelAcronymColorTable(
            teachersTable, IconLoader.loadIcon("wizard_teachers"),
            "x.documentwizard.tab.teachers.title",
            "x.documentwizard.tab.teachers.sort",
            "x.documentwizard.tab.teachers.add"
        );
    }
    
    
    private void buildSubjectsPanel() {
        subjectsTable = new LabelAcronymColorTableComponent(
            new String[]{
                text("x.documentwizard.tab.subjects.table.label"),
                text("x.documentwizard.tab.subjects.table.acronym"),
                text("x.documentwizard.tab.subjects.table.color"),
            },
            text("x.documentwizard.tab.subjects.table.add")
        );
        subjectsPanel = createTabPanelWithLabelAcronymColorTable(
            subjectsTable, IconLoader.loadIcon("wizard_subjects"),
            "x.documentwizard.tab.subjects.title",
            "x.documentwizard.tab.subjects.sort",
            "x.documentwizard.tab.subjects.add"
        );
    }
    
    
    private void buildClassesPanel() {
        classesTable = new LabelAcronymColorTableComponent(
            new String[]{
                text("x.documentwizard.tab.classes.table.label"),
                text("x.documentwizard.tab.classes.table.acronym"),
                text("x.documentwizard.tab.classes.table.color"),
            },
            text("x.documentwizard.tab.classes.table.add")
        );
        classesPanel = createTabPanelWithLabelAcronymColorTable(
            classesTable, IconLoader.loadIcon("wizard_classes"),
            "x.documentwizard.tab.classes.title",
            "x.documentwizard.tab.classes.sort",
            "x.documentwizard.tab.classes.add"
        );
    }
    
    
    private void buildTimingsetPanel() {
        timingsetPanel = new JPanel();
        timingsetPanel.setLayout(new BorderLayout());
        
        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.PAGE_AXIS));
        timingsetPanel.add(innerPanel, BorderLayout.PAGE_START);

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        topPanel.setBorder(new EmptyBorder(7, 3, 7, 3));
        
        innerPanel.add(topPanel);
        
        JLabel titleLabel = createTitleLabelWithIcon("x.documentwizard.tab.timingsets.title", IconLoader.loadIcon("wizard_timingsets"));
        topPanel.add(titleLabel, BorderLayout.LINE_START);

        enableTimingSetCheckBox = new MultilingualCheckBox("x.documentwizard.tab.timingsets.enabled");
        enableTimingSetCheckBox.setSelected(true);
        enableTimingSetCheckBox.setBorder(new EmptyBorder(20, 10, 20, 10));
        enableTimingSetCheckBox.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent ev) {
                refreshTimingSetsControls();
            }
            
        });
        innerPanel.add(createSimpleRow(enableTimingSetCheckBox));

        
        JPanel dataOuterPanel = new JPanel();
        dataOuterPanel.setLayout(new BoxLayout(dataOuterPanel, BoxLayout.LINE_AXIS));
        dataOuterPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
        innerPanel.add(dataOuterPanel);

        
        JPanel leftOuterPanel = new JPanel();
        leftOuterPanel.setLayout(new BorderLayout());
        dataOuterPanel.add(leftOuterPanel);

        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS));
        leftOuterPanel.add(leftPanel, BorderLayout.PAGE_START);
        
        numberOfDaysSpinner = createNaturalSpinner(5, 1, 99, 1);
        numberOfDaysSpinner.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent ev) {
                refreshTimingSetsControls();
            }
            
        });
        leftPanel.add(createRow(new MultilingualLabel("x.documentwizard.tab.timingsets.number_of_days"), numberOfDaysSpinner, 100));

        numberOfStudiesSpinner = createNaturalSpinner(8, 1, studyStartTimes.length, 1);
        numberOfStudiesSpinner.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent ev) {
                refreshTimingSetsControls();
            }
            
        });
        leftPanel.add(createRow(new MultilingualLabel("x.documentwizard.tab.timingsets.number_of_studies"), numberOfStudiesSpinner, 100));
        

        JPanel rightOuterPanel = new JPanel();
        rightOuterPanel.setLayout(new BorderLayout());
        dataOuterPanel.add(rightOuterPanel);

        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
        rightOuterPanel.add(rightPanel, BorderLayout.PAGE_START);
        
        studyStartSpinners = new JSpinner[studyStartTimes.length];
        
        for (int i=0; i<studyStartTimes.length; i++) {
            studyStartSpinners[i] = createTimeSpinner(studyStartTimes[i][0], studyStartTimes[i][1]);
            String studyName = String.format(text(
                "x.documentwizard.nth_study"), i+1
            );
            rightPanel.add(createRow(new JLabel(studyName), studyStartSpinners[i], 100));
        }
    }
    

    private JComponent createSimpleRow(JComponent component) {
        JPanel outer = new JPanel();
        outer.setLayout(new BorderLayout());
        
        outer.add(component, BorderLayout.LINE_START);
        
        return outer;
    }
    
    
    private JComponent createRow(JComponent leftComponent, JComponent rightComponent, int leftWidth) {
        JPanel outer = new JPanel();
        outer.setLayout(new BorderLayout());
        outer.setPreferredSize(new Dimension(leftWidth+rightComponent.getPreferredSize().width, 30));
        
        JPanel inner = new JPanel();
        inner.setLayout(new BoxLayout(inner, BoxLayout.LINE_AXIS));
        outer.add(inner, BorderLayout.LINE_START);
        
        Dimension leftComponentSize = leftComponent.getSize();
        JPanel left = new JPanel();
        left.setLayout(new BorderLayout());
        left.setPreferredSize(new Dimension(leftWidth, leftComponentSize.height));
        left.add(leftComponent, BorderLayout.LINE_START);
        
        inner.add(left);
        inner.add(rightComponent);
        
        return outer;
    }
    
    
    private JSpinner createNaturalSpinner(int value, int minimum, int maximum, int step) {
        JSpinner spinner = new JSpinner();
        
        SpinnerNumberModel model = new SpinnerNumberModel(value, minimum, maximum, step);
        spinner.setModel(model);
        
        return spinner;
    }
    
    
    private JSpinner createTimeSpinner(int hours, int minutes) {
        JSpinner spinner = new JSpinner();
        
        SpinnerDateModel model = new SpinnerDateModel();
        model.setCalendarField(Calendar.MINUTE); // XXX
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hours);
        calendar.set(Calendar.MINUTE, minutes);
        model.setValue(calendar.getTime());
        spinner.setModel(model);
        
        JSpinner.DateEditor editor = new JSpinner.DateEditor(spinner, "HH:mm");
        spinner.setEditor(editor);
        
        return spinner;
    }
    
    
    private void refreshTimingSetsControls() {
        boolean enabled = enableTimingSetCheckBox.isSelected();
        numberOfDaysSpinner.setEnabled(enabled);
        numberOfStudiesSpinner.setEnabled(enabled);
        int studyNumber = (int)(Integer)numberOfStudiesSpinner.getValue();
        for (int i=0; i<studyStartSpinners.length; i++) {
            JSpinner spinner = studyStartSpinners[i];
            spinner.setEnabled(enabled && i<studyNumber);
        }
    }

    
    private void addTab(ImageIcon icon, String titlePath, Component component) {
        int index = wizardPane.getTabCount();
        wizardPane.addMultilingualTab(titlePath, component);
        JPanel tabPanel = new JPanel(new BorderLayout());
        tabPanel.setOpaque(false);
        tabPanel.setPreferredSize(new Dimension(200, 50));
        JLabel label = new MultilingualLabel(titlePath);
        label.setIcon(icon);
        label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        tabPanel.add(label, BorderLayout.CENTER);
        wizardPane.setTabComponentAt(index, tabPanel);
    }
    
    
    private void finishWizard() {
        teachersTable.close();
        subjectsTable.close();
        classesTable.close();
        
        Document document = new Document();
        Document.PeriodStore periodStore = document.getPeriodStore();
        Document.BoardStore boardStore = document.getBoardStore();
        Document.ResourceStore resourceStore = document.getResourceStore();
        Document.TagStore tagStore = document.getTagStore();
        
        document.setLabel(documentLabelField.getText());
        
        String weeksType = weeksComboBox.getSelectedData();
        if (weeksType.equals("single")) {
            periodStore.register(new Period(text("x.documentwizard.finish.week_single_name"), 0, 0));
        } else if (weeksType.equals("ab")) {
            periodStore.register(new Period(text("x.documentwizard.finish.week_a_name"), 0, 1));
            periodStore.register(new Period(text("x.documentwizard.finish.week_b_name"), 0, 2));
        } else if (weeksType.equals("12")) {
            periodStore.register(new Period(text("x.documentwizard.finish.week_1_name"), 1, 0));
            periodStore.register(new Period(text("x.documentwizard.finish.week_2_name"), 2, 0));
        } else if (weeksType.equals("12ab")) {
            periodStore.register(new Period(text("x.documentwizard.finish.week_1a_name"), 1, 1));
            periodStore.register(new Period(text("x.documentwizard.finish.week_1b_name"), 1, 2));
            periodStore.register(new Period(text("x.documentwizard.finish.week_2a_name"), 2, 1));
            periodStore.register(new Period(text("x.documentwizard.finish.week_2b_name"), 2, 2));
        }
        
        if (addTableCheckBox.isSelected()) {
            boardStore.register(new Board(text("x.documentwizard.finish.board_name")));
        }
        
        for (LabelAcronymColorTableComponent.Row teacherRow: teachersTable.getRows()) {
            if (!teacherRow.label.isEmpty()) {
                Resource teacher = new Resource(Resource.Type.PERSON);
                teacher.setLabel(teacherRow.label);
                teacher.setAcronym(teacherRow.acronym);
                teacher.setColor(teacherRow.color);
                resourceStore.register(teacher);
            }
        }
        for (LabelAcronymColorTableComponent.Row subjectRow: subjectsTable.getRows()) {
            if (!subjectRow.label.isEmpty()) {
                Tag subject = new Tag(Tag.Type.SUBJECT);
                subject.setLabel(subjectRow.label);
                subject.setAcronym(subjectRow.acronym);
                subject.setColor(subjectRow.color);
                tagStore.register(subject);
            }
        }
        
        for (LabelAcronymColorTableComponent.Row classRow: classesTable.getRows()) {
            if (!classRow.label.isEmpty()) {
                Resource _class = new Resource(Resource.Type.CLASS);
                _class.setLabel(classRow.label);
                _class.setAcronym(classRow.acronym);
                _class.setColor(classRow.color);
                resourceStore.register(_class);
            }
        }
        
        if (enableTimingSetCheckBox.isSelected()) {
            TimingSet timingSet = new TimingSet(text("x.documentwizard.finish.timingset_name"));
            int numberOfDays = (int)(Integer)numberOfDaysSpinner.getValue();
            int numberOfStudies = (int)(Integer)numberOfStudiesSpinner.getValue();
            for (int i=0; i<numberOfDays; i++) {
                for (int j=0; j<numberOfStudies; j++) {
                    Date timeDate = (Date)studyStartSpinners[j].getValue();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(timeDate);
                    int hours = calendar.get(Calendar.HOUR_OF_DAY);
                    int minutes = calendar.get(Calendar.MINUTE);
                    Time time = new Time((i*Time.DAY)+(hours*Time.HOUR)+(minutes*Time.MINUTE));
                    String dayString = getDayAbbr(i);
                    int weekIndex = (int)Math.floor(i / 7d);
                    if (weekIndex != 0) {
                        int weekNumber = (weekIndex < 0) ? weekIndex : weekIndex + 1;
                        dayString = weekNumber + " / " + dayString;
                    }
                    String studyName = String.format(text(
                        "x.documentwizard.day_nth_study"), dayString, j+1
                    );
                    timingSet.add(time, studyName);
                }
            }
            document.getTimingSetStore().register(timingSet);
            for (Resource _class: document.getResourceStore()) {
                if (_class.getType().equals(Resource.Type.CLASS)) {
                    _class.setTimingSetEnabled(true);
                    _class.getTimingSetManager().setDefaultTimingSet(timingSet);
                }
            }
        }
        
        resultDocument = document;
        closeDialog();
    }
    
    private String getDayAbbr(long day) {
        int dayIndex = (int)(day % 7);
        String[] dayNames = new String[]{
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday",
        };
        return text("core.day." + dayNames[dayIndex] + ".abbr");
    }
    
    
    public Document getResultDocument() {
        return resultDocument;
    }
    
}

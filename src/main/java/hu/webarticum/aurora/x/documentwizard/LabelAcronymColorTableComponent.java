package hu.webarticum.aurora.x.documentwizard;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.CellEditor;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import hu.webarticum.aurora.app.util.AcronymGenerator;
import hu.webarticum.aurora.app.util.ColorSequenceGenerator;
import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.plugins.defaultswingui.util.ColorUtil;

class LabelAcronymColorTableComponent extends JLayeredPane {
    
    
    private static final long serialVersionUID = 1L;


    private Dimension dimension = new Dimension(420, 440);
    
    private String[] columnNames;

    private String addInfo;
    
    private ColorSequenceGenerator colorSequenceGenerator = new ColorSequenceGenerator();
    
    private JTable table;
    
    private JButton bigAddButton;

    private Dimension bigAddButtonDimension = new Dimension(70, 70);
    
    
    LabelAcronymColorTableComponent(String[] columnNames, String addInfo) {
        super();
        this.columnNames = columnNames;
        this.addInfo = addInfo;
        this.table = new JTable();
        this.bigAddButton = new JButton();
        init();
    }
    
    private void init() {
        table.setModel(new DefaultTableModel(new Object[][]{}, columnNames));
        table.setFillsViewportHeight(true);
        table.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {
                int code = ev.getKeyCode();
                if (!ev.isAltDown() && !ev.isControlDown() && !ev.isShiftDown()) {
                    switch (code) {
                        case KeyEvent.VK_TAB:
                            int rowCount = table.getRowCount();
                            if (
                                rowCount==0 || (
                                    table.getSelectedRow()==rowCount-1 &&
                                    table.getSelectedColumn()==table.getColumnCount()-1
                                )
                            ) {
                                ev.consume();
                                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
                            }
                            break;
                        case KeyEvent.VK_ENTER:
                            if (table.getSelectedRow()==table.getRowCount()-1) {
                                ev.consume();
                                addRow();
                            }
                            break;
                        case KeyEvent.VK_DELETE:
                            int selectedIndex = table.getSelectedRow();
                            if (selectedIndex>=0) {
                                ev.consume();
                                ((DefaultTableModel)(table.getModel())).removeRow(selectedIndex);
                                CellEditor cellEditor = table.getCellEditor();
                                if (cellEditor!=null) {
                                    cellEditor.cancelCellEditing();
                                }
                                int count = table.getRowCount();
                                if (selectedIndex<count) {
                                    table.setRowSelectionInterval(selectedIndex, selectedIndex);
                                } else if (selectedIndex>0) {
                                    table.setRowSelectionInterval(selectedIndex-1, selectedIndex-1);
                                }
                                if (table.getRowCount()==0) {
                                    colorSequenceGenerator.reset();
                                    bigAddButton.setVisible(true);
                                }
                            }
                            break;
                    }
                } else if (!ev.isAltDown() && !ev.isControlDown() && ev.isShiftDown()) {
                    switch (code) {
                        case KeyEvent.VK_TAB:
                            if (table.getRowCount()==0 || (table.getSelectedRow()==0 && table.getSelectedColumn()==0)) {
                                ev.consume();
                                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
                            }
                            break;
                    }
                }
            }
            
        });
        
        setColumns();
        
        table.setPreferredScrollableViewportSize(dimension);
        
        JScrollPane tableScrollPane = new JScrollPane(table);
        tableScrollPane.setPreferredSize(dimension);
        
        setPreferredSize(dimension);
        
        tableScrollPane.setBounds(0, 0, (int)dimension.getWidth(), (int)dimension.getHeight());
        this.add(tableScrollPane, Integer.valueOf(0));
        
        bigAddButton.setText("+");
        bigAddButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bigAddButton.setToolTipText(addInfo);
        if (UIManager.getLookAndFeel().getName().equals("Nimbus")) {
            bigAddButton.setOpaque(false);
            bigAddButton.setBackground(new java.awt.Color(0x009703));
            bigAddButton.setForeground(java.awt.Color.WHITE);
        }
        {
            Map<TextAttribute, Object> fontAttributes = new HashMap<TextAttribute, Object>();
            fontAttributes.put(TextAttribute.FAMILY, Font.DIALOG);
            fontAttributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
            fontAttributes.put(TextAttribute.SIZE, 30);
            bigAddButton.setFont(new Font(fontAttributes));
        }
        int buttonWidth = (int)bigAddButtonDimension.getWidth();
        int buttonHeight = (int)bigAddButtonDimension.getHeight();
        int buttonLeft = ((int)dimension.getWidth()-buttonWidth)/2;
        int buttonTop = 120;
        bigAddButton.setBounds(buttonLeft, buttonTop, buttonWidth, buttonHeight);
        bigAddButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                addRow();
            }
            
        });
        this.add(bigAddButton, Integer.valueOf(1));
    }

    
    public void sort() {
        int count = table.getRowCount();
        Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>(count);
        for (Object item: ((DefaultTableModel)(table.getModel())).getDataVector()) {
            @SuppressWarnings("unchecked")
            Vector<Object> rowVector = (Vector<Object>)item;
            if (!rowVector.get(0).equals("")) {
                dataVector.add(rowVector);
            }
        }
        Collections.sort(dataVector, new Comparator<Vector<Object>>() {

            @Override
            public int compare(Vector<Object> item1, Vector<Object> item2) {
                String name1 = (String)item1.get(0);
                String name2 = (String)item2.get(0);
                Labeled.StringComparator comparator = new Labeled.StringComparator();
                return comparator.compare(name1, name2);
            }
            
        });
        ((DefaultTableModel)(table.getModel())).setDataVector(dataVector, new Vector<Object>(Arrays.asList(columnNames)));
        setColumns();
        if (table.getRowCount()==0) {
            colorSequenceGenerator.reset();
            bigAddButton.setVisible(true);
        }
    }
    
    
    public void addRow() {
        int index = table.getRowCount();
        bigAddButton.setVisible(false);
        table.requestFocus();
        ((DefaultTableModel)(table.getModel())).addRow(new Object[]{"", "", colorSequenceGenerator.next()});
        table.setRowSelectionInterval(index, index);
        table.editCellAt(index, 0);
        Component editorComponent = table.getEditorComponent();
        editorComponent.requestFocusInWindow();
        
        KeyListener tmpKeyListener = new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent ev) {
            }
            
            @Override
            public void keyReleased(KeyEvent ev) {
            }
            
            @Override
            public void keyPressed(KeyEvent ev) {
                KeyListener self = this;
                if (ev.getKeyCode() == KeyEvent.VK_ENTER) {
                    SwingUtilities.invokeLater(new Runnable() {
                        
                        @Override
                        public void run() {
                            editorComponent.removeKeyListener(self);
                            addRow();
                        }
                        
                    });
                }
            }
            
        };
        editorComponent.addKeyListener(tmpKeyListener);
    }
    
    
    public List<Row> getRows() {
        List<Row> rows = new ArrayList<Row>();
        TableModel model = table.getModel();
        int rowCount = table.getRowCount();
        for (int i=0; i<rowCount; i++) {
            String label = (String)model.getValueAt(i, 0);
            String acronym = (String)model.getValueAt(i, 1);
            Color color = (Color)model.getValueAt(i, 2);
            rows.add(new Row(label, acronym, color));
        }
        return rows;
    }
    
    
    private void setColumns() {
        TableColumn acrColumn = table.getColumnModel().getColumn(1);
        acrColumn.setWidth(100);
        acrColumn.setMaxWidth(100);
        
        TableColumn colorColumn = table.getColumnModel().getColumn(2);
        colorColumn.setWidth(50);
        colorColumn.setMaxWidth(50);
        TableCellRenderer colorRenderer = new ColorRenderer();
        colorColumn.setCellRenderer(colorRenderer);
        TableCellEditor colorEditor = new ColorEditor();
        colorColumn.setCellEditor(colorEditor);

        table.getModel().addTableModelListener(new TableModelListener() {
            
            @Override
            public void tableChanged(TableModelEvent ev) {
                int firstRow = ev.getFirstRow();
                int lastRow = ev.getLastRow();
                int column = ev.getColumn();
                int type = ev.getType();
                if (type!=TableModelEvent.UPDATE || firstRow!=lastRow) {
                    return;
                }
                switch (column) {
                    case 0:
                        TableModel model = ((TableModel)ev.getSource());
                        String newValue = model.getValueAt(firstRow, column).toString();
                        model.setValueAt(AcronymGenerator.generateAcronym(newValue), firstRow, column+1);
                        break;
                }
            }
        });
    }
    
    
    public void close() {
        if (table.isEditing()) {
            table.getCellEditor().stopCellEditing();
        }
    }
    
    
    class ColorRenderer implements TableCellRenderer {

        JPanel colorPanel;
        
        public ColorRenderer() {
            colorPanel = new JPanel();
            colorPanel.setPreferredSize(new Dimension(25, 25));
        }
        
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (value == null) {
                return null;
            }
            colorPanel.setBackground(ColorUtil.toAwtColor((Color)value));
            return colorPanel;
        }
        
    }
    
    
    class ColorEditor extends AbstractCellEditor implements TableCellEditor {
        
        private static final long serialVersionUID = 1L;

        private JButton button;
        
        private Color color;
        
        public ColorEditor() {
            button = new JButton();
            button.setPreferredSize(new Dimension(25, 25));
            button.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent ev) {
                    java.awt.Color sourceAwtColor = ColorUtil.toAwtColor(color);
                    java.awt.Color newAwtColor = JColorChooser.showDialog(
                        button,
                        text("x.documentwizard.labelacronymcolortable.colorchooser_title"),
                        sourceAwtColor
                    );
                    if (newAwtColor!=null) {
                        Color newColor = ColorUtil.fromAwtColor(newAwtColor);
                        setColor(newColor);
                    }
                }
                
            });
            setColor(Color.WHITE);
        }
        
        @Override
        public Color getCellEditorValue() {
            return color;
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            setColor((Color)value);
            return button;
        }
        
        public void setColor(Color color) {
            this.color = color;
            button.setBackground(ColorUtil.toAwtColor(color));
        }
        
    }
    
    
    public class Row {

        public final String label;
        
        public final String acronym;
        
        public final Color color;
        
        private Row(String label, String acronym, Color color) {
            this.label = label;
            this.acronym = acronym;
            this.color = color;
        }
        
    }
    
}
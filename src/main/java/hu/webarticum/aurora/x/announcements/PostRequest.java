package hu.webarticum.aurora.x.announcements;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PostRequest {

    private final String url;

    private final String postContent;
    
    public PostRequest(String url, String postContent) {
        this.url = url;
        this.postContent = postContent;
    }
    
    public String send() throws IOException {
        String response = "";
        
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection)(new URL(url).openConnection());
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", Integer.toString(postContent.getBytes().length));
            connection.setUseCaches(false);
            connection.setDoOutput(true);

            DataOutputStream outputStream = new DataOutputStream (connection.getOutputStream());
            outputStream.writeBytes(postContent);
            outputStream.close();
            
            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder responseBuilder = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                responseBuilder.append(line);
                responseBuilder.append('\n');
            }
            reader.close();
            response = responseBuilder.toString();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        
        return response;
    }
    
}

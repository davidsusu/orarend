package hu.webarticum.aurora.x.announcements;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.Dialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.widget.HtmlView;

public class AnnouncementDialog extends Dialog {
    
    private static final long serialVersionUID = 1L;
    
    private final Announcement announcement;
    
    public AnnouncementDialog(Announcement announcement) {
        super(null);
        this.title = announcement.getTitle();
        this.announcement = announcement;
        init();
    }
    
    @Override
    protected void build() {
        JPanel contentPanel = new JPanel();
        containerPanel.add(contentPanel, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();
        containerPanel.add(buttonPanel, BorderLayout.PAGE_END);
        String htmlContentWithTitle =
            "<h1>" + HtmlView.htmlencode(announcement.getTitle()) + "</h1>" +
            "<h2>(" + HtmlView.htmlencode(announcement.getPublishDateTime()) + ")</h2>" +
            announcement.getHtmlContent()
        ;
        HtmlView contentView = new HtmlView(htmlContentWithTitle);
        JScrollPane contentScrollPane = new JScrollPane(contentView);
        contentScrollPane.setPreferredSize(new Dimension(450, 500));
        contentPanel.add(contentScrollPane);
        JButton closeButton = new JButton("OK");
        closeButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
            
        });
        buttonPanel.add(closeButton);
    }

}

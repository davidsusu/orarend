package hu.webarticum.aurora.x.announcements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

public class AnnouncementFetcher {
    
    private final String url;
    
    public AnnouncementFetcher(String url) {
        this.url = url;
    }
    
    @SuppressWarnings("unchecked")
    public FetchResult fetchSince(String dateTime) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("since", dateTime == null ? "" : dateTime);
        String jsonString = jsonObject.toJSONString();
        
        String postContent;
        try {
            postContent = "request=" + URLEncoder.encode(jsonString, "utf-8");
        } catch (UnsupportedEncodingException e) {
            return new FetchResult(false, null, null);
        }
        
        String response = "";
        
        PostRequest request = new PostRequest(url, postContent);
        try {
            response = request.send();
        } catch (IOException e) {
            return new FetchResult(false, null, null);
        }

        Object data;
        try {
            data = JSONValue.parseWithException(response);
        } catch (ParseException e) {
            return new FetchResult(false, null, null);
        }
        if (!(data instanceof JSONObject)) {
            return new FetchResult(false, null, null);
        }
        
        JSONObject responseJsonObject = (JSONObject)data;

        String fetchDateTime = "";
        Object fetchDateTimeObject = responseJsonObject.get("dateTime");
        if (fetchDateTimeObject instanceof String) {
            fetchDateTime = (String)fetchDateTimeObject;
        }
        
        List<Announcement> announcements = new ArrayList<Announcement>();
        Object announcementsObject = responseJsonObject.get("announcements");
        if (announcementsObject instanceof JSONArray) {
            JSONArray announcementsArray = (JSONArray)announcementsObject;
            for (Object announcementObject: announcementsArray) {
                if (announcementObject instanceof JSONObject) {
                    JSONObject announcementRecord = (JSONObject)announcementObject;
                    String publishDateTime = announcementRecord.getOrDefault("datetime_publish", "").toString();
                    String title = announcementRecord.getOrDefault("title", "").toString();
                    String htmlContent = announcementRecord.getOrDefault("html_content", "").toString();
                    announcements.add(new Announcement(publishDateTime, title, htmlContent));
                }
            }
        }
        
        return new FetchResult(true, fetchDateTime, announcements);
    }
    
    public class FetchResult {

        public final boolean success;
        
        public final String fetchDateTime;
        
        public final List<Announcement> announcements;

        private FetchResult(boolean success, String fetchDateTime, List<Announcement> announcements) {
            this.success = success;
            this.fetchDateTime = fetchDateTime;
            this.announcements = announcements;
        }
        
    }
    
}

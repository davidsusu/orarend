package hu.webarticum.aurora.x.announcements;


public class Announcement {
    
    private final String publishDateTime;
    private final String title;
    private final String htmlContent;
    
    public Announcement(String publishDateTime, String title, String htmlContent ) {
        this.publishDateTime = publishDateTime;
        this.title = title;
        this.htmlContent = htmlContent;
    }

    public String getPublishDateTime() {
        return publishDateTime;
    }

    public String getTitle() {
        return title;
    }

    public String getHtmlContent() {
        return htmlContent;
    }
    
}

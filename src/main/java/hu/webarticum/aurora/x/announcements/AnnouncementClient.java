package hu.webarticum.aurora.x.announcements;

import static hu.webarticum.aurora.app.Shortcut.text;
import static hu.webarticum.aurora.app.Shortcut.texts;

import javax.swing.SwingUtilities;

import hu.webarticum.aurora.app.Application;
import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.util.i18n.CombinedMultiLanguageTextRepository;
import hu.webarticum.aurora.app.util.i18n.PropertiesLanguageTextRepository;
import hu.webarticum.aurora.plugins.defaultswingui.DefaultSwingUiRunner;
import hu.webarticum.aurora.plugins.defaultswingui.global.GlobalUiObjects;
import hu.webarticum.aurora.plugins.defaultswingui.util.NotificationQueue;


public class AnnouncementClient {

    protected static final String ANNOUNCEMENTS_URL = Info.SERVICE_URL + "/announcements";
    
    private static boolean guiInited = false;
    
    public void run(boolean enablePopup) {
        (new Thread() {

            @Override
            public void run() {
                final AnnouncementFetcher.FetchResult fetchResult =
                    new AnnouncementFetcher(ANNOUNCEMENTS_URL).fetchSince(getLastFetchDateTime())
                ;
                
                initGui();
                
                SwingUtilities.invokeLater(new Runnable() {
                    
                    @Override
                    public void run() {
                        if (!fetchResult.success) {
                            return;
                        }
                        
                        int announcementCount = fetchResult.announcements.size();
                        
                        int to = enablePopup ? announcementCount - 1 : announcementCount;
                        for (int i = 0; i < to; i++) {
                            Announcement announcement = fetchResult.announcements.get(i);
                            GlobalUiObjects.getNotificationQueue().send(
                                    text("x.announcements.announcement"),
                                announcement.getTitle(),
                                NotificationQueue.Notification.TYPE.INFO
                            );
                        }
                        
                        if (enablePopup && to >= 0) {
                            new AnnouncementDialog(fetchResult.announcements.get(to)).run();
                        }
                        
                        setLastFetchDateTime(fetchResult.fetchDateTime);
                    }
                    
                });
            }
            
        }).start();
    }

    public String getLastFetchDateTime() {
        return Application.instance().getSettings().get("lastdatetime_announcements_fetched");
    }

    public void setLastFetchDateTime(String dateTime) {
        Application.instance().getSettings().set("lastdatetime_announcements_fetched", dateTime);
    }

    public static void initGui() {
        if (!guiInited) {
            DefaultSwingUiRunner.initLookAndFeel();

            CombinedMultiLanguageTextRepository langRepository = new CombinedMultiLanguageTextRepository();
            langRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/x/announcements/lang/announcements.hu_HU.properties", "hu_HU"));
            langRepository.put(new PropertiesLanguageTextRepository(Info.RESOURCE_PATH+"/x/announcements/lang/announcements.en_US.properties", "en_US"));
            texts().mount("x.announcements", langRepository);
            
            guiInited = true;
        }
    }
    
}

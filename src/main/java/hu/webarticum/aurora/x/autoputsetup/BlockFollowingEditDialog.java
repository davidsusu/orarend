package hu.webarticum.aurora.x.autoputsetup;

import static hu.webarticum.aurora.app.Shortcut.text;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.plugins.defaultautoput.UserBlockFollowingDefinition;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.BlockSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.EditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualCheckBox;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;

public class BlockFollowingEditDialog extends EditDialog {

    private static final long serialVersionUID = 1L;

    
    private final Document document;
    
    private final GeneralWrapper<UserBlockFollowingDefinition> followingDefinitionWrapper;


    private Block block1 = null;
    
    private Block block2 = null;
    
    private JCheckBox orderedCheckBox;
    
    private JButton block1Button;
    
    private JButton block2Button;
    
    private JTextArea commentTextArea;
    
    
    public BlockFollowingEditDialog(
        JFrame parent, Document document,
        GeneralWrapper<UserBlockFollowingDefinition> followingDefinitionWrapper
    ) {
        super(parent);
        SchoolAutoputSetupDialog.initLanguages();
        this.title = text("x.schoolautoputsetup.blockfollowingedit.title");
        this.document = document;
        this.followingDefinitionWrapper = followingDefinitionWrapper;
        init();
    }
    
    @Override
    protected void build() {
        orderedCheckBox = new MultilingualCheckBox("x.schoolautoputsetup.blockfollowingedit.ordered");
        addRow(orderedCheckBox);
        
        JPanel block1Panel = new JPanel(new BorderLayout());
        block1Button = new JButton();
        block1Button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Block> blockWrapper = new GeneralWrapper<>(block1);
                BlockSelectDialog dialog = new BlockSelectDialog(
                    parent, document, blockWrapper,
                    block2 == null ? null : Arrays.asList(block2)
                );
                dialog.run();
                
                if (dialog.getResult() == EditDialog.RESULT_OK) {
                    block1 = blockWrapper.get();
                    refreshBlockButton(block1Button, block1);
                    refreshOkButton();
                }
            }
            
        });
        block1Panel.add(block1Button, BorderLayout.CENTER);
        addRow(new MultilingualLabel("x.schoolautoputsetup.blockfollowingedit.block1"), block1Panel);

        JPanel block2Panel = new JPanel(new BorderLayout());
        block2Button = new JButton();
        block2Button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                GeneralWrapper<Block> blockWrapper = new GeneralWrapper<>(block2);
                BlockSelectDialog dialog = new BlockSelectDialog(
                    parent, document, blockWrapper,
                    block1 == null ? null : Arrays.asList(block1)
                );
                dialog.run();
                
                if (dialog.getResult() == EditDialog.RESULT_OK) {
                    block2 = blockWrapper.get();
                    refreshBlockButton(block2Button, block2);
                    refreshOkButton();
                }
            }
            
        });
        block2Panel.add(block2Button, BorderLayout.CENTER);
        addRow(new MultilingualLabel("x.schoolautoputsetup.blockfollowingedit.block2"), block2Panel);

        commentTextArea = new JTextArea();
        JScrollPane commentScrollPane = new JScrollPane(commentTextArea);
        commentScrollPane.setPreferredSize(new Dimension(200, 90));
        addRow(new MultilingualLabel("x.schoolautoputsetup.blockfollowingedit.comment"), commentScrollPane);
    }

    @Override
    protected void load() {
        UserBlockFollowingDefinition followingDefinition = followingDefinitionWrapper.get();
        if (followingDefinition != null) {
            block1 = followingDefinition.getBlock1();
            block2 = followingDefinition.getBlock2();
            
            orderedCheckBox.setSelected(followingDefinition.isOrdered());
            commentTextArea.setText(followingDefinition.getComment());
        }

        refreshBlockButton(block1Button, block1);
        refreshBlockButton(block2Button, block2);
        refreshOkButton();
        
    }

    private void refreshBlockButton(JButton button, Block block) {
        button.setText(block == null ? "- - -" : block.getLabel());
    }
    
    private void refreshOkButton() {
        okButton.setEnabled(canSave());
    }

    @Override
    protected boolean canSave() {
        return (
            block1 != null &&
            block2 != null &&
            !block1.equals(block2)
        );
    }
    
    @Override
    protected void save() {
        UserBlockFollowingDefinition followingDefinition = new UserBlockFollowingDefinition(
            orderedCheckBox.isSelected(),
            block1,
            block2,
            commentTextArea.getText()
        );
        followingDefinitionWrapper.set(followingDefinition);
    }

}

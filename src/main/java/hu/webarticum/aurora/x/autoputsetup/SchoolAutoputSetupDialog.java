package hu.webarticum.aurora.x.autoputsetup;

import static hu.webarticum.aurora.app.Shortcut.history;
import static hu.webarticum.aurora.app.Shortcut.text;
import static hu.webarticum.aurora.app.Shortcut.textProvider;
import static hu.webarticum.aurora.app.Shortcut.texts;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.Painter;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import hu.webarticum.aurora.app.Info;
import hu.webarticum.aurora.app.memento.ValueMemento;
import hu.webarticum.aurora.app.util.common.StringUtil;
import hu.webarticum.aurora.app.util.i18n.CombinedMultiLanguageTextRepository;
import hu.webarticum.aurora.app.util.i18n.PropertiesLanguageTextRepository;
import hu.webarticum.aurora.core.model.Block;
import hu.webarticum.aurora.core.model.Color;
import hu.webarticum.aurora.core.model.Document;
import hu.webarticum.aurora.core.model.GeneralWrapper;
import hu.webarticum.aurora.core.model.Labeled;
import hu.webarticum.aurora.core.model.MultiComparator;
import hu.webarticum.aurora.core.model.Resource;
import hu.webarticum.aurora.core.model.Tag;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.core.model.Value.ValueList;
import hu.webarticum.aurora.plugins.defaultautoput.UserBlockFollowingDefinition;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.EditDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.OptionPaneUtil;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.ResourceSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.component.dialog.TagSelectDialog;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualLabel;
import hu.webarticum.aurora.plugins.defaultswingui.i18n.MultilingualTabbedPane;
import hu.webarticum.aurora.plugins.defaultswingui.util.ButtonPurpose;
import hu.webarticum.aurora.plugins.defaultswingui.util.ColorUtil;
import hu.webarticum.aurora.plugins.defaultswingui.util.IconLoader;
import hu.webarticum.aurora.plugins.defaultswingui.util.PaintedPanel;
import hu.webarticum.chm.Command;
import hu.webarticum.chm.Memento;
import hu.webarticum.chm.MementoCommand;

public class SchoolAutoputSetupDialog extends EditDialog {

    public enum TAB { TEACHERS, FOLLOWINGS }
    
    
    private static final long serialVersionUID = 1L;
    
    
    private Document document;

    private MultilingualTabbedPane mainTabbedPane;
    
    private JList<Labeled.Wrapper> teacherDistributorTagsList;
    
    private JList<Labeled.Wrapper> noHoleTeachersList;
    
    private JList<Labeled.Wrapper> noDoubleHoleTeachersList;
    
    private JList<Labeled.Wrapper> reqDay2TeachersList;
    
    private JTable followingsTable;
    
    private List<UserBlockFollowingDefinition> followings = new ArrayList<UserBlockFollowingDefinition>();

    private static boolean langInited = false;
    
    public SchoolAutoputSetupDialog(JFrame parent, Document document) {
        super(parent);
        initLanguages();
        this.title = text("x.schoolautoputsetup.title");
        this.document = document;
        leftWidth = 200;
        init();
    }
    

    public static void initLanguages() {
        if (!langInited) {

            CombinedMultiLanguageTextRepository langRepository = new CombinedMultiLanguageTextRepository();
            langRepository.put(new PropertiesLanguageTextRepository(
                Info.RESOURCE_PATH + "/plugins/basekit/schoolautoputsetup/lang/schoolautoputsetup.hu_HU.properties", "hu_HU"
            ));
            langRepository.put(new PropertiesLanguageTextRepository(
                Info.RESOURCE_PATH + "/plugins/basekit/schoolautoputsetup/lang/schoolautoputsetup.en_US.properties", "en_US"
            ));
            texts().mount("x.schoolautoputsetup", langRepository);
            
            langInited = true;
        }
    }
    

    
    @Override
    protected void build() {
        mainTabbedPane = new MultilingualTabbedPane();
        mainTabbedPane.setBorder(new EmptyBorder(15, 15, 15, 15));
        mainTabbedPane.setPreferredSize(new Dimension(600, 400));
        containerPanel.add(mainTabbedPane, BorderLayout.CENTER);
        
        addTeachersTab();
        addFollowingsTab();
    }
    
    @Override
    protected void load() {
        Value extraData = document.getExtraData();
        
        loadTeachers(extraData);
        loadFollowings(extraData);
    }
    
    @Override
    protected void save() {
        if (!modified) {
            return;
        }
        
        final Value extraData = document.getExtraData();
        
        Command changeExtraDataCommand = new MementoCommand<Value>(
            textProvider("x.schoolautoputsetup.command.change"),
            extraData,
            new Function<Value, Memento<Value>>() {
    
                @Override
                public Memento<Value> apply(Value value) {
                    return new ValueMemento(value);
                }
                
            }, new Consumer<Value>() {
    
                @Override
                public void accept(Value value) {
                    saveTeachers(value);
                    saveFollowings(value);
                }
                
            }
        );
        
        history().addAndExecute(changeExtraDataCommand);
    }

    
    // TEACHERS
    
    private void addTeachersTab() {
        JPanel teachersTabOuterPanel = new JPanel(new BorderLayout());
        mainTabbedPane.addMultilingualTab("x.schoolautoputsetup.tabs.teachers", teachersTabOuterPanel);

        JPanel teachersPanel = new JPanel();
        teachersPanel.setLayout(new GridLayout(2, 2, 10, 10));
        teachersTabOuterPanel.add(teachersPanel, BorderLayout.PAGE_START);
        
        teacherDistributorTagsList = new JList<Labeled.Wrapper>();
        JLabel teacherDistributorLabel = new JLabel(String.format(
            "<html>%s</html>",
            StringUtil.toHtml(text("x.schoolautoputsetup.tabs.teachers.teacher_distributor_tags"))
        ));
        JPanel distributorTagsPanel = teachersCreateListPanel(
            teacherDistributorLabel, teacherDistributorTagsList, 120, new TeachersItemChooser() {
                
                @Override
                public Labeled chooseItem() {
                    GeneralWrapper<Tag> tagWrapper = new GeneralWrapper<Tag>();
                    TagSelectDialog dialog = new TagSelectDialog(parent, document, tagWrapper, Tag.Type.OTHER, false);
                    dialog.run();
                    return tagWrapper.get();
                }
                
            }
        );
        teachersPanel.add(distributorTagsPanel);
        
        noHoleTeachersList = new JList<Labeled.Wrapper>();
        JLabel noHoleTeachersLabel = new JLabel(String.format(
            "<html>%s</html>",
            StringUtil.toHtml(text("x.schoolautoputsetup.tabs.teachers.no_hole_teachers"))
        ));
        JPanel noHoleTeachersPanel = teachersCreateListPanel(
            noHoleTeachersLabel, noHoleTeachersList, 120, new TeachersItemChooser() {
                
                @Override
                public Labeled chooseItem() {
                    GeneralWrapper<Resource> resourceWrapper = new GeneralWrapper<Resource>();
                    ResourceSelectDialog dialog = new ResourceSelectDialog(parent, document, resourceWrapper, Resource.Type.PERSON);
                    dialog.run();
                    return resourceWrapper.get();
                }
                
            }
        );
        teachersPanel.add(noHoleTeachersPanel);
        
        noDoubleHoleTeachersList = new JList<Labeled.Wrapper>();
        JLabel noDoubleHoleTeachersLabel = new JLabel(String.format(
            "<html>%s</html>",
            StringUtil.toHtml(text("x.schoolautoputsetup.tabs.teachers.no_double_hole_teachers"))
        ));
        JPanel noDoubleHoleTeachersPanel = teachersCreateListPanel(
            noDoubleHoleTeachersLabel, noDoubleHoleTeachersList, 120, new TeachersItemChooser() {
                
                @Override
                public Labeled chooseItem() {
                    GeneralWrapper<Resource> resourceWrapper = new GeneralWrapper<Resource>();
                    ResourceSelectDialog dialog = new ResourceSelectDialog(parent, document, resourceWrapper, Resource.Type.PERSON);
                    dialog.run();
                    return resourceWrapper.get();
                }
                
            }
        );
        teachersPanel.add(noDoubleHoleTeachersPanel);
        
        reqDay2TeachersList = new JList<Labeled.Wrapper>();
        JLabel reqDay2TeachersLabel = new JLabel(String.format(
            "<html>%s</html>",
            StringUtil.toHtml(text("x.schoolautoputsetup.tabs.teachers.req_day_2_teachers"))
        ));
        JPanel reqDay2TeachersPanel = teachersCreateListPanel(
            reqDay2TeachersLabel, reqDay2TeachersList, 120, new TeachersItemChooser() {
                
                @Override
                public Labeled chooseItem() {
                    GeneralWrapper<Resource> resourceWrapper = new GeneralWrapper<Resource>();
                    ResourceSelectDialog dialog = new ResourceSelectDialog(parent, document, resourceWrapper, Resource.Type.PERSON);
                    dialog.run();
                    return resourceWrapper.get();
                }
                
            }
        );
        teachersPanel.add(reqDay2TeachersPanel);
    }
    
    private void loadTeachers(Value extraData) {
        Value.ValueSet teacherDistributorTagsData = extraData.getAccess("autoput.school.setup.teacher_distributor_tags").get().getAsSet();
        TreeSet<Labeled> teacherDistributorTags = new TreeSet<Labeled>(new MultiComparator<Labeled>(new Labeled.LabeledComparator()));
        for (Value value: teacherDistributorTagsData) {
            Tag tag = value.getAsTag();
            teacherDistributorTags.add(tag);
        }
        setTeachersListItems(teacherDistributorTagsList, teacherDistributorTags);
        
        Value.ValueSet noHoleTeachersData = extraData.getAccess("autoput.school.setup.no_hole_teachers").get().getAsSet();
        TreeSet<Labeled> noHoleTeachers = new TreeSet<Labeled>(new MultiComparator<Labeled>(new Labeled.LabeledComparator()));
        for (Value value: noHoleTeachersData) {
            Resource teacher = value.getAsResource();
            noHoleTeachers.add(teacher);
        }
        setTeachersListItems(noHoleTeachersList, noHoleTeachers);
        
        Value.ValueSet noDoubleHoleTeachersData = extraData.getAccess("autoput.school.setup.no_double_hole_teachers").get().getAsSet();
        TreeSet<Labeled> noDoubleHoleTeachers = new TreeSet<Labeled>(new MultiComparator<Labeled>(new Labeled.LabeledComparator()));
        for (Value value: noDoubleHoleTeachersData) {
            Resource teacher = value.getAsResource();
            noDoubleHoleTeachers.add(teacher);
        }
        setTeachersListItems(noDoubleHoleTeachersList, noDoubleHoleTeachers);
        
        Value.ValueSet reqDay2TeachersData = extraData.getAccess("autoput.school.setup.req_day_2_teachers").get().getAsSet();
        TreeSet<Labeled> reqDay2Teachers = new TreeSet<Labeled>(new MultiComparator<Labeled>(new Labeled.LabeledComparator()));
        for (Value value: reqDay2TeachersData) {
            Resource teacher = value.getAsResource();
            reqDay2Teachers.add(teacher);
        }
        setTeachersListItems(reqDay2TeachersList, reqDay2Teachers);
    }
    
    private void saveTeachers(Value extraData) {
        TreeSet<Labeled> teacherDistributorTags = getTeachersListItems(teacherDistributorTagsList);
        Value.ValueSet teacherDistributorTagsData = extraData.getAccess("autoput.school.setup.teacher_distributor_tags").getReferenced(Value.Type.SET).getAsSet();
        teacherDistributorTagsData.clear();
        teacherDistributorTagsData.addAllRaw(teacherDistributorTags);
        
        TreeSet<Labeled> noHoleTeachers = getTeachersListItems(noHoleTeachersList);
        Value.ValueSet noHoleTeachersData = extraData.getAccess("autoput.school.setup.no_hole_teachers").getReferenced(Value.Type.SET).getAsSet();
        noHoleTeachersData.clear();
        noHoleTeachersData.addAllRaw(noHoleTeachers);
        
        TreeSet<Labeled> noDoubleHoleTeachers = getTeachersListItems(noDoubleHoleTeachersList);
        Value.ValueSet noDoubleHoleTeachersData = extraData.getAccess("autoput.school.setup.no_double_hole_teachers").getReferenced(Value.Type.SET).getAsSet();
        noDoubleHoleTeachersData.clear();
        noDoubleHoleTeachersData.addAllRaw(noDoubleHoleTeachers);
        
        TreeSet<Labeled> reqDay2Teachers = getTeachersListItems(reqDay2TeachersList);
        Value.ValueSet reqDay2TeachersData = extraData.getAccess("autoput.school.setup.req_day_2_teachers").getReferenced(Value.Type.SET).getAsSet();
        reqDay2TeachersData.clear();
        reqDay2TeachersData.addAllRaw(reqDay2Teachers);
    }

    private JPanel teachersCreateListPanel(
        JLabel topLabel, final JList<Labeled.Wrapper> list, final int height,
        final TeachersItemChooser chooser
    ) {
        final Runnable addAction = new Runnable() {

            @Override
            public void run() {
                Labeled newItem = chooser.chooseItem();
                if (newItem != null) {
                    addTeachersItemToList(list, newItem);
                    modified = true;
                }
            }
            
        };
        
        final Runnable removeAction = new Runnable() {
            
            @Override
            public void run() {
                Object value = list.getSelectedValue();
                if (value != null) {
                    int answer = OptionPaneUtil.showConfirmDialog(
                        parent,
                        text("x.schoolautoputsetup.tabs.teachers.list.remove.confirm"),
                        text("x.schoolautoputsetup.tabs.teachers.list.remove.title"),
                        ButtonPurpose.DANGER
                    );
                    if (answer == JOptionPane.YES_OPTION) {
                        Labeled item = ((Labeled.Wrapper)value).get();
                        TreeSet<Labeled> items = getTeachersListItems(list);
                        items.remove(item);
                        setTeachersListItems(list, items);
                        modified = true;
                    }
                }
            }
            
        };
        
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(new EmptyBorder(5, 0, 0, 0));
        
        JPanel topPanel = new JPanel(new BorderLayout());
        panel.add(topPanel, BorderLayout.PAGE_START);
        
        topPanel.add(topLabel, BorderLayout.CENTER);
        
        JPanel addButtonPanel = new JPanel(new BorderLayout());
        topPanel.add(addButtonPanel, BorderLayout.LINE_END);
        
        JButton addButton = new JButton();
        addButton.setIcon(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                addAction.run();
            }
            
        });
        addButtonPanel.add(addButton, BorderLayout.PAGE_START);
        
        JScrollPane scrollPane = new JScrollPane(list);

        list.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent ev) {
                if (ev.getButton() == MouseEvent.BUTTON3) {
                    removeAction.run();
                }
            }
            
        });
        
        list.addKeyListener(new KeyAdapter() {
            
            @Override
            public void keyPressed(KeyEvent ev) {
                if (ev.getKeyCode() == KeyEvent.VK_DELETE) {
                    removeAction.run();
                }
            }
            
        });
        
        scrollPane.setPreferredSize(new Dimension(200, height));
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        panel.add(scrollPane, BorderLayout.CENTER);
        
        return panel;
    }

    private TreeSet<Labeled> getTeachersListItems(JList<Labeled.Wrapper> list) {
        TreeSet<Labeled> items = new TreeSet<Labeled>(new MultiComparator<Labeled>(new Labeled.LabeledComparator()));
        ListModel<Labeled.Wrapper> listModel = list.getModel();
        int itemCount = listModel.getSize();
        for (int i=0; i<itemCount; i++) {
            items.add(listModel.getElementAt(i).get());
        }
        return items;
    }
    
    private void addTeachersItemToList(JList<Labeled.Wrapper> list, Labeled item) {
        TreeSet<Labeled> items = getTeachersListItems(list);
        items.add(item);
        setTeachersListItems(list, items);
    }
    
    private void setTeachersListItems(JList<Labeled.Wrapper> list, Collection<Labeled> items) {
        DefaultListModel<Labeled.Wrapper> listModel = new DefaultListModel<Labeled.Wrapper>();
        for (Labeled item: items) {
            listModel.addElement(new Labeled.Wrapper(item));
        }
        list.setModel(listModel);
    }
    
    
    // FOLLOWINGS
    
    private void addFollowingsTab() {
        final Runnable addAction = new Runnable() {
            
            @Override
            public void run() {
                GeneralWrapper<UserBlockFollowingDefinition> wrapper =
                    new GeneralWrapper<UserBlockFollowingDefinition>(null)
                ;
                BlockFollowingEditDialog dialog = new BlockFollowingEditDialog(parent, document, wrapper);
                dialog.run();
                
                if (dialog.getResult() == EditDialog.RESULT_OK) {
                    UserBlockFollowingDefinition followingDefinition = wrapper.get();
                    followings.add(followingDefinition);
                    reloadFollowingsTable();
                    modified = true;
                }
            }
            
        };
        
        final Runnable editAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = followingsTable.getSelectedRow();
                if (selectedIndex >= 0) {
                    UserBlockFollowingDefinition oldFollowingDefinition = followings.get(selectedIndex);
                    GeneralWrapper<UserBlockFollowingDefinition> wrapper =
                        new GeneralWrapper<UserBlockFollowingDefinition>(oldFollowingDefinition)
                    ;
                    BlockFollowingEditDialog dialog = new BlockFollowingEditDialog(parent, document, wrapper);
                    dialog.run();
                    
                    if (dialog.getResult() == EditDialog.RESULT_OK) {
                        UserBlockFollowingDefinition newFollowingDefinition = wrapper.get();
                        followings.set(selectedIndex, newFollowingDefinition);
                        reloadFollowingsTable();
                        modified = true;
                    }
                }
            }
            
        };
        
        final Runnable removeAction = new Runnable() {
            
            @Override
            public void run() {
                int selectedIndex = followingsTable.getSelectedRow();
                if (selectedIndex >= 0) {
                    int answer = OptionPaneUtil.showConfirmDialog(
                        parent,
                        text("x.schoolautoputsetup.tabs.followings.table.remove.confirm"),
                        text("x.schoolautoputsetup.tabs.followings.table.remove.title"),
                        ButtonPurpose.DANGER
                    );
                    if (answer == JOptionPane.YES_OPTION) {
                        followings.remove(selectedIndex);
                        reloadFollowingsTable();
                        modified = true;
                    }
                }
            }
            
        };
        
        JPanel followingsPanel = new JPanel(new BorderLayout());
        mainTabbedPane.addMultilingualTab("x.schoolautoputsetup.tabs.followings", followingsPanel);

        JPanel topPanel = new JPanel(new BorderLayout());
        followingsPanel.add(topPanel, BorderLayout.PAGE_START);
        
        JLabel topLabel = new MultilingualLabel("x.schoolautoputsetup.tabs.followings.description");
        topPanel.add(topLabel, BorderLayout.CENTER);
        
        JButton addButton = new JButton(IconLoader.loadIcon("add"));
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ev) {
                addAction.run();
            }
            
        });
        topPanel.add(addButton, BorderLayout.LINE_END);
        
        followingsTable = new JTable() {

            private static final long serialVersionUID = 1L;
            
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            
        };
        followingsTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        followingsTable.setRowHeight(50);
        followingsTable.setCellEditor(null);
        followingsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        followingsTable.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent ev) {
                int button = ev.getButton();
                if (button == MouseEvent.BUTTON3) {
                    removeAction.run();
                } else if (button == MouseEvent.BUTTON1 && ev.getClickCount() == 2) {
                    editAction.run();
                }
            }
            
        });
        
        followingsTable.addKeyListener(new KeyAdapter() {
            
            @Override
            public void keyPressed(KeyEvent ev) {
                int keyCode = ev.getKeyCode();
                if (keyCode == KeyEvent.VK_DELETE) {
                    removeAction.run();
                } else if (keyCode == KeyEvent.VK_ENTER) {
                    editAction.run();
                }
            }
            
        });
        
        JScrollPane followingsTableScrollPane = new JScrollPane(followingsTable);
        followingsTableScrollPane.setPreferredSize(new Dimension(450, 400));
        followingsPanel.add(followingsTableScrollPane, BorderLayout.CENTER);
    }
    
    private void loadFollowings(Value extraData) {
        Value.ValueList followingData = extraData.getAccess("autoput.school.setup.followings").get().getAsList();

        followings.clear();
        for (Value followingItem : followingData) {
            UserBlockFollowingDefinition followingDefinition = new UserBlockFollowingDefinition(
                followingItem.getAccess("ordered").get().getAsBoolean(),
                followingItem.getAccess("block1").get().getAsBlock(),
                followingItem.getAccess("block2").get().getAsBlock(),
                followingItem.getAccess("comment").get().getAsString()
            );
            followings.add(followingDefinition);
        }
        
        reloadFollowingsTable();
    }
    
    private void reloadFollowingsTable() {
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn(text("x.schoolautoputsetup.tabs.followings.table.column.block1"));
        tableModel.addColumn(text("x.schoolautoputsetup.tabs.followings.table.column.block2"));
        tableModel.addColumn(text("x.schoolautoputsetup.tabs.followings.table.column.comment"));
        followingsTable.setModel(tableModel);

        TableColumnModel columnModel = followingsTable.getColumnModel();
        TableColumn column1 = columnModel.getColumn(0);
        column1.setPreferredWidth(150);
        column1.setMinWidth(150);
        column1.setMaxWidth(150);
        column1.setCellRenderer(new BlockCellRenderer(true));

        TableColumn column2 = columnModel.getColumn(1);
        column2.setPreferredWidth(150);
        column2.setMinWidth(150);
        column2.setMaxWidth(150);
        column2.setCellRenderer(new BlockCellRenderer(false));
        
        TableColumn column3 = columnModel.getColumn(2);
        column3.setCellRenderer(new CommentCellRenderer());
        
        for (UserBlockFollowingDefinition followingDefinition : followings) {
            tableModel.addRow(new Object[] { followingDefinition, followingDefinition, followingDefinition });
        }
    }

    private void saveFollowings(Value extraData) {
        ValueList followingValues = extraData.getAccess("autoput.school.setup.followings").getReferenced(Value.Type.LIST).getAsList();
        followingValues.clear();
        for (UserBlockFollowingDefinition followingDefinition : followings) {
            Value followingData = new Value(Value.Type.MAP);
            Value.ValueMap followingDataMap = followingData.getAsMap();
            followingDataMap.putRaw("ordered", followingDefinition.isOrdered());
            followingDataMap.putRaw("block1", followingDefinition.getBlock1());
            followingDataMap.putRaw("block2", followingDefinition.getBlock2());
            followingDataMap.putRaw("comment", followingDefinition.getComment());
            followingValues.add(followingData);
        }
    }
    
    
    private interface TeachersItemChooser {
        
        public Labeled chooseItem();
        
    }
    
    
    private static class BlockCellRenderer implements TableCellRenderer {

        final boolean first;
        
        
        BlockCellRenderer(boolean first) {
            this.first = first;
        }
        
        
        @Override
        public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col
        ) {
            if (value == null) {
                return new JPanel();
            }

            UIDefaults uiDefaults = UIManager.getLookAndFeel().getDefaults();

            String backgroundColorName = isSelected ? "nimbusSelectionBackground" : "nimbusLightBackground";
            java.awt.Color backgroundColor = new java.awt.Color(uiDefaults.getColor(backgroundColorName).getRGB());

            UserBlockFollowingDefinition followingDefinition = (UserBlockFollowingDefinition)value;
            Block block = first ? followingDefinition.getBlock1() : followingDefinition.getBlock2();
            boolean ordered = followingDefinition.isOrdered();
            
            Set<Color> colors = new LinkedHashSet<Color>();
            for (Tag tag : block.getActivityManager().getActivities().getTags(Tag.Type.SUBJECT)) {
                colors.add(tag.getColor());
            }
            Color avgColor = Color.avg(colors);
            
            JPanel cellOuterPanel = new JPanel();
            cellOuterPanel.setLayout(new BorderLayout());
            cellOuterPanel.setBorder(new EmptyBorder(2, 2, 2, 0));
            cellOuterPanel.setBackground(backgroundColor);
            
            PaintedPanel paintedPanel = new PaintedPanel();
            paintedPanel.setLayout(new BorderLayout());
            cellOuterPanel.add(paintedPanel);

            Painter<Component> painter = ColorUtil.createStripPainter(colors);
            paintedPanel.setPainter(painter);
            
            JPanel cellPanel = new JPanel() {

                private static final long serialVersionUID = 1L;

                @Override
                protected void paintComponent(java.awt.Graphics g) {
                    super.paintComponent(g);
                    
                    Graphics2D g2 = (Graphics2D)g;
                    
                    int width = getWidth();
                    int height = getHeight();
                    
                    java.awt.Color lineColor = java.awt.Color.BLACK;
                    java.awt.Color fillColor = new java.awt.Color(ordered ? 0xCF4B1C : 0x1C3BCF);

                    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                    Polygon polygon = new Polygon();
                    if (first) {
                        if (ordered) {
                            polygon.addPoint(width + 10, height - 10);
                            polygon.addPoint(width - 45, height - 10);
                            polygon.addPoint(width - 45, height - 20);
                            polygon.addPoint(width + 10, height - 20);
                        } else {
                            polygon.addPoint(width + 10, height - 10);
                            polygon.addPoint(width - 40, height - 10);
                            polygon.addPoint(width - 40, height - 5);
                            polygon.addPoint(width - 50, height - 15);
                            polygon.addPoint(width - 40, height - 25);
                            polygon.addPoint(width - 40, height - 20);
                            polygon.addPoint(width + 10, height - 20);
                        }
                    } else {
                        polygon.addPoint(-10, height - 10);
                        polygon.addPoint(40, height - 10);
                        polygon.addPoint(40, height - 5);
                        polygon.addPoint(50, height - 15);
                        polygon.addPoint(40, height - 25);
                        polygon.addPoint(40, height - 20);
                        polygon.addPoint(-10, height - 20);
                        
                        g2.drawPolygon(polygon);
                    }
                    
                    g2.setColor(fillColor);
                    g2.fillPolygon(polygon);
                    
                    g2.setColor(lineColor);
                    g2.drawPolygon(polygon);
                }
                
            };
            cellPanel.setOpaque(false);
            cellPanel.setLayout(new BorderLayout());
            paintedPanel.add(cellPanel);
            
            JLabel blockLabel = new JLabel(block.getLabel());
            blockLabel.setBorder(new EmptyBorder(3, 3, 3, 1));
            blockLabel.setBackground(ColorUtil.toAwtColor(avgColor));
            blockLabel.setForeground(ColorUtil.toAwtColor(avgColor.getBlackWhiteContrastColor()));
            cellPanel.add(blockLabel, BorderLayout.PAGE_START);
            
            return cellOuterPanel;
        }
        
    }

    private static class CommentCellRenderer implements TableCellRenderer {
        
        @Override
        public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col
        ) {
            if (value == null) {
                return new JPanel();
            }
            
            UIDefaults uiDefaults = UIManager.getLookAndFeel().getDefaults();

            String backgroundColorName = isSelected ? "nimbusSelectionBackground" : "nimbusLightBackground";
            java.awt.Color backgroundColor = new java.awt.Color(uiDefaults.getColor(backgroundColorName).getRGB());

            String foregroundColorName = isSelected ? "nimbusSelectedText" : "text";
            java.awt.Color foregroundColor = new java.awt.Color(uiDefaults.getColor(foregroundColorName).getRGB());
            
            UserBlockFollowingDefinition followingDefinition = (UserBlockFollowingDefinition)value;
            
            JPanel cellPanel = new JPanel(new BorderLayout());
            cellPanel.setBackground(backgroundColor);
            
            JTextArea textArea = new JTextArea();
            textArea.setText(followingDefinition.getComment());
            textArea.setBorder(new EmptyBorder(4, 4, 4, 4));
            textArea.setEditable(false);
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            textArea.setBackground(backgroundColor);
            textArea.setForeground(foregroundColor);
            cellPanel.add(textArea, BorderLayout.CENTER);
            
            return cellPanel;
        }
    }
    
}

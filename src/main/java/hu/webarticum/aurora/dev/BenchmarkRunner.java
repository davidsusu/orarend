package hu.webarticum.aurora.dev;

public class BenchmarkRunner {

    public void runBenchmark(Runnable task, int warmup, int runs) {
        for (int i = 0; i < warmup; i++) {
            task.run();
        }
        long startTime = System.currentTimeMillis();
        long latestTime = startTime;
        long shortestTime = Integer.MAX_VALUE;
        long longestTime = Integer.MIN_VALUE;
        for (int i = 0; i < runs; i++) {
            task.run();
            long nextTime = System.currentTimeMillis();
            long taskTime = nextTime - latestTime;
            shortestTime = Math.min(shortestTime, taskTime);
            longestTime = Math.max(longestTime, taskTime);
            latestTime = nextTime;
        }
        long endTime = System.currentTimeMillis();
        long elapsedTime = endTime - startTime;
        long averageTime = elapsedTime / runs;
        System.out.println(String.format("avg: %d (min: %d, max: %d)",
            averageTime, shortestTime, longestTime
        ));
    }
    
}

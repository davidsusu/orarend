package hu.webarticum.aurora.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import hu.webarticum.aurora.app.memento.ValueMemento;
import hu.webarticum.aurora.core.model.Value;
import hu.webarticum.aurora.core.model.Value.ValueList;
import hu.webarticum.aurora.core.model.Value.ValueMap;


public class ValueMementoTest {

    @Test
    public void testSimpleInteger() {
        Value value = new Value(10);
        ValueMemento memento = new ValueMemento(value);
        
        value.getAccess().set(15);
        
        assertThat(value).extracting(Value::getAsInt).isEqualTo(15);
        
        memento.apply(value);

        assertThat(value).extracting(Value::getAsInt).isEqualTo(10);
    }

    @Test
    public void testComplexStructure() {
        Value structureValue = new Value(Value.Type.MAP);
        ValueMap structureMap = structureValue.getAsMap();

        structureMap.put(new Value("apple"), new Value("banana"));
        
        Value listValue = new Value(Value.Type.LIST);
        structureMap.put(new Value("orange"), listValue);
        
        ValueList list = listValue.getAsList();
        list.add(new Value("first"));
        list.add(new Value("second"));
        list.add(new Value("third"));
        
        ValueMemento memento = new ValueMemento(structureValue);
        
        structureValue.getAccess("apple").set("benene");
        structureValue.getAccess("orange.0").set("1st");
        structureValue.getAccess("orange.1").set("2nd");
        structureValue.getAccess("orange.2").set("3rd");
        structureValue.getAccess("orange.3").set("4th");

        assertThat(structureValue.getAccess("apple").get().get()).isEqualTo("benene");
        assertThat(structureValue.getAccess("orange.0").get().get()).isEqualTo("1st");
        assertThat(structureValue.getAccess("orange.1").get().get()).isEqualTo("2nd");
        assertThat(structureValue.getAccess("orange.2").get().get()).isEqualTo("3rd");
        assertThat(structureValue.getAccess("orange").get().getAsList().size()).isEqualTo(4);
        
        memento.apply(structureValue);

        assertThat(structureValue.getAccess("apple").get().get()).isEqualTo("banana");
        assertThat(structureValue.getAccess("orange.0").get().get()).isEqualTo("first");
        assertThat(structureValue.getAccess("orange.1").get().get()).isEqualTo("second");
        assertThat(structureValue.getAccess("orange.2").get().get()).isEqualTo("third");
        assertThat(structureValue.getAccess("orange").get().getAsList().size()).isEqualTo(3);
    }
    
}

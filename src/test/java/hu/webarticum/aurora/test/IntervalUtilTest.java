package hu.webarticum.aurora.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import hu.webarticum.aurora.app.util.common.IntervalUtil;
import hu.webarticum.aurora.core.model.time.Interval;
import hu.webarticum.aurora.core.model.time.Intervalable;


public class IntervalUtilTest {

    @Test
    public void test() {
        TestIntervalable inA = new TestIntervalable("A", new Interval(0, 5));
        TestIntervalable inB = new TestIntervalable("B", new Interval(1, 3));
        TestIntervalable inC = new TestIntervalable("C", new Interval(2, 4));
        TestIntervalable inD = new TestIntervalable("D", new Interval(2, 7));
        TestIntervalable inE = new TestIntervalable("E", new Interval(4, 6));
        TestIntervalable inF = new TestIntervalable("F", new Interval(5, 8));
        TestIntervalable inG = new TestIntervalable("G", new Interval(6, 6));
        TestIntervalable inH = new TestIntervalable("H", new Interval(7, 9));
        
        List<TestIntervalable> intervalables = new ArrayList<TestIntervalable>();
        intervalables.add(inA);
        intervalables.add(inB);
        intervalables.add(inC);
        intervalables.add(inD);
        intervalables.add(inE);
        intervalables.add(inF);
        intervalables.add(inG);
        intervalables.add(inH);
        
        SortedMap<Interval, List<TestIntervalable>> actualSections =
            IntervalUtil.breakIntervalablesToSections(intervalables)
        ;
        
        SortedMap<Interval, List<TestIntervalable>> expectedSections =
            new TreeMap<Interval, List<TestIntervalable>>()
        ;
        expectedSections.put(new Interval(0, 1), Arrays.asList(inA));
        expectedSections.put(new Interval(1, 2), Arrays.asList(inA, inB));
        expectedSections.put(new Interval(2, 3), Arrays.asList(inA, inB, inC, inD));
        expectedSections.put(new Interval(3, 4), Arrays.asList(inA, inC, inD));
        expectedSections.put(new Interval(4, 5), Arrays.asList(inA, inD, inE));
        expectedSections.put(new Interval(5, 6), Arrays.asList(inD, inE, inF));
        expectedSections.put(new Interval(6, 6), Arrays.asList(inG));
        expectedSections.put(new Interval(6, 7), Arrays.asList(inD, inF));
        expectedSections.put(new Interval(7, 8), Arrays.asList(inF, inH));
        expectedSections.put(new Interval(8, 9), Arrays.asList(inH));
        
        assertThat(actualSections).isEqualTo(expectedSections);
    }
    
    
    public static class TestIntervalable implements Intervalable {

        private final String label;
        
        private final Interval interval;
        
        
        public TestIntervalable(String label, Interval interval) {
            this.label = label;
            this.interval = interval;
        }
        
        
        @Override
        public Interval getInterval() {
            return interval;
        }
        
        @Override
        public String toString() {
            return label;
        }
        
    }

}

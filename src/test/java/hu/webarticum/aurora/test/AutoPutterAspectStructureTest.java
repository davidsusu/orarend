package hu.webarticum.aurora.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import hu.webarticum.aurora.app.util.autoput.AutoPutterAspectStructure;

public class AutoPutterAspectStructureTest {

    @Test
    public void testCategoryStructure() {
        AutoPutterAspectStructure structure = buildStructure();
        
        assertThat(dumpStructure(structure)).isEqualTo(
            "[main1: [sub1.1: [hello, ], sub1.2: [world, xxx, ], ], " +
            "main2: [sub2.1: [lorem, ipsum, ], sub2.3: [hello, ipsum, ], ], ]"
        );
    }

    @Test
    public void testLevels() {
        AutoPutterAspectStructure structure = buildStructure();
        
        assertThat(dumpLevels(structure)).isEqualTo(
            "[1: [hello, lorem, xxx, ], 2: [world, ], 3: [ipsum, ], ]"
        );
    }
    
    private AutoPutterAspectStructure buildStructure() {
        AutoPutterAspectStructure.Builder builder = AutoPutterAspectStructure.builder();
        
        AutoPutterAspectStructure.Category main1 = new AutoPutterAspectStructure.Category("main1", "Fő 1");
        AutoPutterAspectStructure.SubCategory sub11 = new AutoPutterAspectStructure.SubCategory(main1, "sub1.1", "Al 1.1");
        AutoPutterAspectStructure.Category main2 = new AutoPutterAspectStructure.Category("main2", "Fő 2");
        AutoPutterAspectStructure.SubCategory sub21 = new AutoPutterAspectStructure.SubCategory(main2, "sub2.1", "Al 2.1");
        AutoPutterAspectStructure.SubCategory sub22 = new AutoPutterAspectStructure.SubCategory(main2, "sub2.2", "Al 2.2");
        AutoPutterAspectStructure.SubCategory sub23 = new AutoPutterAspectStructure.SubCategory(main2, "sub2.3", "Al 2.3");

        AutoPutterAspectStructure.SubCategory sub12 = new AutoPutterAspectStructure.SubCategory(main1, "sub1.2", "Al 1.2");

        builder.addSubCategory(sub11);
        builder.addSubCategory(sub12);
        builder.addSubCategory(sub21);
        builder.addSubCategory(sub22);

        builder.addAspect("hello", "Hello", 1, sub11, sub23);
        builder.addAspect("world", "World", 2, sub12);
        builder.addAspect("lorem", "Lorem", 1, sub21);
        builder.addAspect("ipsum", "Ipsum", 3, sub21, sub23);
        builder.addAspect("xxx", "XXX", 1, sub12);
        
        return builder.build();
    }
    
    private String dumpStructure(AutoPutterAspectStructure structure) {
        StringBuilder resultBuilder = new StringBuilder("[");
        
        for (AutoPutterAspectStructure.Category category : structure.getCategories()) {
            resultBuilder.append(category.getName() + ": [");
            for (AutoPutterAspectStructure.SubCategory subCategory : structure.getSubCategoriesOf(category)) {
                resultBuilder.append(subCategory.getName() + ": [");
                for (String aspect : structure.getAspectsIn(subCategory)) {
                    resultBuilder.append(aspect + ", ");
                }
                resultBuilder.append("], ");
            }
            resultBuilder.append("], ");
        }

        resultBuilder.append("]");
        
        return resultBuilder.toString();
    }

    private String dumpLevels(AutoPutterAspectStructure structure) {
        StringBuilder resultBuilder = new StringBuilder("[");

        for (int level : structure.getLevels()) {
            resultBuilder.append(level + ": [");
            for (String aspect : structure.getAspectsAt(level)) {
                resultBuilder.append(aspect + ", ");
            }
            resultBuilder.append("], ");
        }

        resultBuilder.append("]");
        
        return resultBuilder.toString();
    }
    
}

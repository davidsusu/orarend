package hu.webarticum.aurora.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import hu.webarticum.aurora.app.event.Event;
import hu.webarticum.aurora.app.event.EventManager;
import hu.webarticum.aurora.app.event.Listener;


public class EventManagerTest {

    @Test
    public void test() {
        // TODO: more test
        
        final List<String> messages = new ArrayList<String>();
        
        EventManager eventManager = new EventManager();
        eventManager.register("some-event", new Listener() {
            
            @Override
            public void invoke(Event event) {
                messages.add("General listener");
            }
            
        });
        eventManager.register("some-event", 1, new Listener() {
            
            @Override
            public void invoke(Event event) {
                messages.add("One specific listener");
            }
            
        });
        eventManager.register("some-event", 2, new Listener() {
            
            @Override
            public void invoke(Event event) {
                messages.add("Other specific listener");
            }
            
        });
        eventManager.register("x-event", new Listener() {
            
            @Override
            public void invoke(Event event) {
                messages.add("X listener");
            }
            
        });
        
        eventManager.trigger("some-event");
        eventManager.trigger("some-event", 1);
        eventManager.trigger("some-event", 2);
        eventManager.trigger("some-event", 3);
        eventManager.trigger("other-event");
        eventManager.trigger("other-event", 1);
        
        
        List<String> expectedMessages = new ArrayList<String>();
        
        expectedMessages.add("General listener");
        
        expectedMessages.add("General listener");
        expectedMessages.add("One specific listener");
        
        expectedMessages.add("General listener");
        expectedMessages.add("Other specific listener");
        
        expectedMessages.add("General listener");

        assertThat(messages).isEqualTo(expectedMessages);
    }

}
